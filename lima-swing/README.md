Présentation :

Acronyme de Lutin Invoice Monitoring and Accounting,
l'application de comptabilité Lima est un logiciel libre pensé pour être le plus ergonomique possible et facile d'accès à tout utilisateur,
quelque soit son niveau en comptabilité : débutant comme confirmé.
La particularité de Lima est qu'il s'agit d'un produit évolutif permettant de répondre aux besoins spécifiques de toute entreprise ou organisation,
tout en garantissant le maintien des données comptables.

Le logiciel est écrit en Java ce qui assure une compatibilité multiplateforme : Windows, Mac OS X, Linux.
Il peut-être installé en fonctionnement client monoposte, ou en configuration client <–> serveur.
Lors d'une installation en client serveur, le moteur de persistence de données est installé coté serveur,
et l'interface est installée sur autant de postes client que désiré.

Pour lancer l'application :

    Sous Linux : via le script lima ou en ligne de commande java -jar lima.jar ;

    Sous Mac OS X : en double-cliquant sur le jar lima.jar ;

    Sous Windows : en double-cliquant sur l'éxécutable lima.exe.


Pour configurer le client pour se connecter au serveur, éditez le fichier de configuration (lima-swing.config) qui se trouve ici :
Unix-like : $HOME/.config/lima-swing.config
Windows 7 et + : C:\Users\USER_NAME\AppData\Roaming\lima-swing.config
Mac OS : $HOME/Library/Application Support/lima-swing.config

Ajoutez la ligne suivantes sur en remplaçant l'IP en exemple (192.168.1.37) par celle du serveur :
lima.host.address=192.168.1.37