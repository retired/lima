/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima;

import com.google.common.collect.ImmutableSet;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.actions.MiscAction;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.OptionsService;
import org.chorem.lima.business.config.LimaConfigOptionDef;
import org.chorem.lima.business.utils.BigDecimalToString;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.converter.ConverterUtil;
import org.nuiton.util.version.Version;
import org.nuiton.util.version.VersionBuilder;

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * La configuration de l'application.
 *
 * @author chemit
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class LimaSwingConfig extends ApplicationConfig implements BigDecimalToString.Config {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(LimaSwingConfig.class);

    public static final Set<Character> NUMBER_SEPARATOR =  ImmutableSet.of(' ', ',', '.', ';');
    public static final Set<Integer> NUMBER_DECIMALS =  ImmutableSet.of(0, 1, 2, 3, 4, 5, 6);

    private static LimaSwingConfig instance;

    protected OptionsService optionsService;

    /** La version du logiciel. */
    protected Version version;
    
    /**
     * Get copyright text (include version).
     *
     * @return copyright text
     */
    public String getCopyrightText() {
        return "Version " + getVersion() + " Codelutin @ 2008-" + getOption("application.year");
    }

    /**
     * Version as string.
     *
     * @return le nombre global ex: 3.2.0.0
     */
    public String getVersion() {
        return version.toString();
    }

    /**
     * Lima config constructor.
     * <p/>
     * Define all default options and action alias.
     */
    public LimaSwingConfig() {

        // set defaut option (included configuration file name : important)
        loadDefaultOptions(Option.values());

        // set action alias
        for (Action a : Action.values()) {
            for (String alias : a.aliases) {
                addActionAlias(alias, a.action);
            }
        }

        // ajout des alias (can be set in option enum ?)
        addAlias("--disableui", "--launchui false");
    }

    public synchronized static LimaSwingConfig getInstance() {
        if (instance == null) {
            instance = new LimaSwingConfig();
            instance.loadConfiguration(Option.CONFIG_FILE.getDefaultValue());
        }
        return instance;
    }

    protected void loadConfiguration(String configFileName) {

        instance.setConfigFileName(configFileName);
        try {
            instance.parse();
        } catch (ArgumentsParserException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't read configuration", ex);
            }
        }
    }

    @Override
    public ApplicationConfig parse(String... args) throws ArgumentsParserException {

        // super parse, read config file etc...
        super.parse(args);

        // on ne peut pas connaitre la version avant la lecture du fichier
        VersionBuilder nb = new VersionBuilder();
        nb.setVersion(getOption("application.version"));
        version = nb.build();
        return this;

    }

    /**
     * Get application locale.
     *
     * @return configuration application locale
     */
    public Locale getLocale() {
        String local = getOption(Option.LOCALE.key);
        Locale result = ConverterUtil.convert(Locale.class, local);
        return result;
    }

    /**
     * Locale setter for command line parameters.
     *
     * @param locale new locale
     */
    public void setLocale(String locale) {
        setOption(Option.LOCALE.key, locale);
    }

    /**
     * Change locale (not command line version).
     * Save user file.
     *
     * @param newLocale new locale
     */
    public void setLocale(Locale newLocale) {
        setOption(Option.LOCALE.key, newLocale.toString());
        optionsService.setLocal(newLocale);
        saveForUser();
        firePropertyChange("locale", null, newLocale);
    }

    /**
     * Get application decimal separator
     *
     * @return configuration application decimal separator
     */
    public char getDecimalSeparator() {
        return optionsService.getDecimalSeparator();
    }

    /**
     * Change decimal separator
     * Save user file.
     *
     * @param decimalSeparator new DecimalSeparator
     */
    public void setDecimalSeparator(String decimalSeparator) {
        optionsService.setDecimalSeparator(decimalSeparator);
        firePropertyChange("decimalSeparator", null, decimalSeparator);
    }

    /**
     * Get application scale
     *
     * @return configuration application scale
     */
    public int getScale() {
        return optionsService.getScale();
    }

    /**
     * Change scale
     * Save user file.
     *
     * @param scale new Scale
     */
    public void setScale(String scale) {
        optionsService.setScale(scale);
        firePropertyChange("scale", null, scale);
        if (log.isInfoEnabled()) {
            log.info("new scale" + scale);
        }
    }

    /**
     * Get application thousand separator
     *
     * @return configuration application thousand separator
     */
    public char getThousandSeparator() {
        return optionsService.getThousandSeparator();
    }

    /**
     * Change the thousand separator
     * Save user file.
     *
     * @param thousandSeparator new thousandSeparator
     */
    public void setThousandSeparator(String thousandSeparator) {
        optionsService.setThousandSeparator(thousandSeparator);
        firePropertyChange("thousandSeparator", null, thousandSeparator);
    }

    /**
     * currency configuration boolean
     *
     * @return {@code true} if the currency must be displayed
     */
    public boolean getCurrency() {
        return optionsService.getCurrency();
    }

    /**
     * Change the currency displaying
     *
     * @param currency the new currency to set in configuration
     */
    public void setCurrency(boolean currency) {
        optionsService.setCurrency(currency);
        firePropertyChange("currency", null, currency);
    }

    /**
     * Launch ui configuration value.
     *
     * @return {@code true} if ui must be displayed
     */
    public boolean isLaunchui() {
        boolean launchUI = getOptionAsBoolean(Option.LAUNCH_UI.key);
        return launchUI;
    }

    /**
     * Launch ui setter for command line parameters.
     *
     * @param launchui new lauch ui value
     */
    public void setLaunchui(String launchui) {
        setOption(Option.LAUNCH_UI.key, launchui);
    }

    /**
     * Get support email address.
     *
     * @return support email
     */
    public String getSupportEmail() {
        return getOption(Option.SUPPORT_EMAIL.key);
    }

    /**
     * Return true if ejb mode is configured as remote.
     *
     * @return {@code true} if remote mode should be used
     */
    public boolean isEJBRemoteMode() {
        boolean result = getOptionAsBoolean(Option.OPEN_EJB_REMOTE_MODE.key);
        return result;
    }

    public void setEJBRemoteMode(boolean isRemotable) {
        setOption(Option.OPEN_EJB_REMOTE_MODE.key, String.valueOf(isRemotable));
        saveForUser();
    }

    public File getDataDirectory() {
        File result = getOptionAsFile(Option.DATA_DIR.key);
        return result;
    }

    public File getLimaStateFile() {
        File result = getOptionAsFile(Option.LIMA_STATE_FILE.key);
        return result;
    }

    public File getResourcesDirectory() {
        File result = getOptionAsFile(Option.RESOURCES_DIRECTORY.key);
        return result;
    }

    public File getI18nDirectory() {
        File result = getOptionAsFile(Option.I18N_DIRECTORY.key);
        return result;
    }


    public void setColorSelectionFocus(String color) {
        setOption(Option.COLOR_SELECTION_FOCUS.key, color);
    }

    public Color getColorSelectionFocus() {
        return getOptionAsColor((Option.COLOR_SELECTION_FOCUS.key));
    }

    public void setSelectAllEditingCell(boolean selectAllEditingCell) {
        setOption(Option.SELECT_ALL_EDITING_CELL.key, Boolean.toString(selectAllEditingCell));
    }

    public boolean isSelectAllEditingCell() {
        return getOptionAsBoolean((Option.SELECT_ALL_EDITING_CELL.key));
    }

    /** Used in ???? */
    protected static final String[] DEFAULT_JAXX_PCS = {"fullScreen", "locale", "decimalSeparator", "scale", "thousandSeparator", "currency"};

    /** Used in ???? */
    public void removeJaxxPropertyChangeListener() {
        PropertyChangeListener[] toRemove = JAXXUtil.findJaxxPropertyChangeListener(DEFAULT_JAXX_PCS, getPropertyChangeListeners());
        if (toRemove == null || toRemove.length == 0) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("before remove : " + getPropertyChangeListeners().length);
            log.debug("toRemove : " + toRemove.length);

        }
        for (PropertyChangeListener listener : toRemove) {
            removePropertyChangeListener(listener);
        }
        if (log.isDebugEnabled()) {
            log.debug("after remove : " + getPropertyChangeListeners().length);
        }
    }

    /**
     * Lima option definition.
     * <p/>
     * Contains all lima configuration key, with defaut value and
     * information for jaxx configuration frame ({@link #type},
     * {@link #transientState}, {@link #finalState}...)
     */
    public enum Option implements LimaConfigOptionDef {

        CONFIG_FILE(ApplicationConfig.CONFIG_FILE_NAME,
                t("lima.config.configFileName.label"),
                n("lima.config.configFileName.description"),
                "lima-swing.config",
                String.class, true, true),

        DATA_DIR("lima.data.dir",
                t("lima.config.data.dir.label"),
                n("lima.config.data.dir.description"),
                "${user.home}/.lima",
                File.class, false, false),

        RESOURCES_DIRECTORY("lima.resources.dir",
                t("lima.config.resources.dir.label"),
                n("lima.config.resources.dir.description"),
                "${lima.data.dir}/resources-${application.version}",
                String.class, false, false),

        I18N_DIRECTORY("lima.i18n.dir",
                t("lima.config.i18n.dir.label"),
                n("lima.config.i18n.dir.description"),
                "${lima.resources.dir}/i18n",
                String.class, false, false),

        LOCALE("lima.ui.locale",
                t("lima.config.locale.label"),
                n("lima.config.locale.description"),
                "fr_FR",
                Locale.class, false, false),

        LAUNCH_UI("lima.ui.launchui",
                t("lima.config.ui.launchUi.label"),
                n("lima.config.ui.launchUi.description"),
                "true", Boolean.class, true, true),

        SUPPORT_EMAIL("lima.email.support",
                t("lima.email.support.label"),
                n("lima.email.support.description"),
                "support@codelutin.com",
                String.class, false, false),

        OPEN_EJB_REMOTE_MODE("openejb.embedded.remotable",
                t("lima.openEjb.remote.mode.label"),
                n("lima.openEjb.remote.mode.description"),
                "false",
                String.class, false, false),

        LIMA_STATE_FILE("lima.ui.state.file",
                t("lima.config.state.file.label"),
                n("lima.config.state.file.description"),
                "${lima.data.dir}/limaState.xml",
                String.class, false, false),

        COLOR_SELECTION_FOCUS("lima.ui.table.cell.colorSelectionFocus",
                t("lima.config.color.selection.focus.label"),
                n("lima.config.color.selection.focus.description"),
                "#000000",
                Color.class, false, false),

        SELECT_ALL_EDITING_CELL("lima.ui.table.cell.selectAllEditingCell",
                t("lima.config.selectAllEditingCell.label"),
                n("lima.config.selectAllEditingCell.description"),
                "true",
                Boolean.class, false, false),

        TABLE_CELL_BACKGROUND("lima.ui.table.cell.background",
                t("lima.config.cell.background.label"),
                n("lima.config.cell.background.description"),
                "#FFFFFF",
                Color.class, false, false),

        TABLE_CELL_FOREGROUND("lima.ui.table.cell.foreground",
                t("lima.config.cell.foreground.label"),
                n("lima.config.cell.foreground.description"),
                "#000000",
                Color.class, false, false),

        TABLE_CELL_PAIR_BACKGROUND("lima.ui.table.cell.pair.background",
                t("lima.config.cell.pair.background.label"),
                n("lima.config.cell.pair.background.description"),
                "#EEEEEE",
                Color.class, false, false),

        TABLE_CELL_PAIR_FOREGROUND("lima.ui.table.cell.pair.foreground",
                t("lima.config.cell.pair.foreground.label"),
                n("lima.config.cell.pair.foreground.description"),
                "#000000",
                Color.class, false, false),

        TABLE_CELL_SELECTED_BACKGROUND("lima.ui.table.cell.selected.background",
                t("lima.config.cell.selected.background.label"),
                n("lima.config.cell.selected.background.description"),
                "#0066CC",
                Color.class, false, false),

        TABLE_CELL_SELECTED_FOREGROUND("lima.ui.table.cell.selected.foreground",
                t("lima.config.cell.selected.foreground.label"),
                n("lima.config.cell.selected.foreground.description"),
                "#FFFFFF",
                Color.class, false, false),

        TABLE_CELL_PAIR_SELECTED_BACKGROUND("lima.ui.table.cell.pair.selected.background",
                t("lima.config.cell.pair.selected.background.label"),
                n("lima.config.cell.pair.selected.background.description"),
                "#006699",
                Color.class, false, false),

        TABLE_CELL_PAIR_SELECTED_FOREGROUND("lima.ui.table.cell.pair.selected.foreground",
                t("lima.config.cell.pair.selected.foreground.label"),
                n("lima.config.cell.pair.selected.foreground.description"),
                "#FFFFFF",
                Color.class, false, false),

        TABLE_CELL_ERROR_BACKGROUND("lima.ui.table.cell.error.background",
                t("lima.config.cell.error.background.label"),
                n("lima.config.cell.error.background.description"),
                "#FFFFFF",
                Color.class, false, false),

        TABLE_CELL_ERROR_FOREGROUND("lima.ui.table.cell.error.foreground",
                t("lima.config.cell.error.foreground.label"),
                n("lima.config.cell.error.foreground.description"),
                "#FF0936",
                Color.class, false, false),

        TABLE_CELL_PAIR_ERROR_BACKGROUND("lima.ui.table.cell.pair.error.background",
                t("lima.config.cell.pair.error.background.label"),
                n("lima.config.cell.pair.error.background.description"),
                "#EEEEEE",
                Color.class, false, false),

        TABLE_CELL_PAIR_ERROR_FOREGROUND("lima.ui.table.cell.pair.error.foreground",
                t("lima.config.cell.pair.error.foreground.label"),
                n("lima.config.cell.pair.error.foreground.description"),
                "#FC0625",
                Color.class, false, false),

        TABLE_CELL_SELECTED_ERROR_BACKGROUND("lima.ui.table.cell.selected.error.background",
                t("lima.config.cell.selected.error.background.label"),
                n("lima.config.cell.selected.error.background.description"),
                "#0066CC",
                Color.class, false, false),

        TABLE_CELL_SELECTED_ERROR_FOREGROUND("lima.ui.table.cell.selected.error.foreground",
                t("lima.config.cell.selected.error.foreground.label"),
                n("lima.config.cell.selected.error.foreground.description"),
                "#C998C1",
                Color.class, false, false),

        TABLE_CELL_PAIR_SELECTED_ERROR_BACKGROUND("lima.ui.table.cell.pair.selected.error.background",
                t("lima.config.cell.pair.selected.error.background.label"),
                n("lima.config.cell.pair.selected.error.background.description"),
                "#006699",
                Color.class, false, false),

        TABLE_CELL_PAIR_SELECTED_ERROR_FOREGROUND("lima.ui.table.cell.pair.selected.error.foreground",
                t("lima.config.cell.pair.selected.error.foreground.label"),
                n("lima.config.cell.pair.selected.error.foreground.description"),
                "#C96678",
                Color.class, false, false),

        TABLE_CELL_MANDATORY_BACKGROUND("lima.ui.table.cell.mandatory.background",
                t("lima.config.cell.mandatory.background.label"),
                n("lima.config.cell.mandatory.background.description"),
                "#FFCCCC",
                Color.class, false, false),

        TABLE_CELL_MANDATORY_FOREGROUND("lima.ui.table.cell.mandatory.foreground",
                t("lima.config.cell.mandatory.foreground.label"),
                n("lima.config.cell.mandatory.foreground.description"),
                "#000000",
                Color.class, false, false),

        TABLE_CELL_PAIR_MANDATORY_BACKGROUND("lima.ui.table.cell.pair.mandatory.background",
                t("lima.config.cell.pair.mandatory.background.label"),
                n("lima.config.cell.pair.mandatory.background.description"),
                "#FF99CC",
                Color.class, false, false),

        TABLE_CELL_PAIR_MANDATORY_FOREGROUND("lima.ui.table.cell.pair.mandatory.foreground",
                t("lima.config.cell.pair.mandatory.foreground.label"),
                n("lima.config.cell.pair.mandatory.foreground.description"),
                "#000000",
                Color.class, false, false),

        TABLE_CELL_SELECTED_MANDATORY_BACKGROUND("lima.ui.table.cell.selected.mandatory.background",
                t("lima.config.cell.selected.mandatory.background.label"),
                n("lima.config.cell.selected.mandatory.background.description"),
                "#FF0000",
                Color.class, false, false),

        TABLE_CELL_SELECTED_MANDATORY_FOREGROUND("lima.ui.table.cell.selected.mandatory.foreground",
                t("lima.config.cell.selected.mandatory.foreground.label"),
                n("lima.config.cell.selected.mandatory.foreground.description"),
                "#FFFFFF",
                Color.class, false, false),

        TABLE_CELL_PAIR_SELECTED_MANDATORY_BACKGROUND("lima.ui.table.cell.pair.selected.mandatory.background",
                t("lima.config.cell.pair.selected.mandatory.background.label"),
                n("lima.config.cell.pair.selected.mandatory.background.description"),
                "#990000",
                Color.class, false, false),

        TABLE_CELL_PAIR_SELECTED_MANDATORY_FOREGROUND("lima.ui.table.cell.pair.selected.mandatory.foreground",
                t("lima.config.cell.pair.selected.mandatory.foreground.label"),
                n("lima.config.cell.pair.selected.mandatory.foreground.description"),
                "#000000",
                Color.class, false, false);

        protected enum ComportmentEditingCellEnum {ALL, NOTHING}

        protected final String key;

        protected final String label;

        protected final String description;

        protected String defaultValue;

        protected final Class<?> type;

        protected boolean transientState;

        protected boolean finalState;

        Option(String key, String label, String description, String defaultValue,
               Class<?> type, boolean transientState, boolean finalState) {
            this.key = key;
            this.label = label;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.finalState = finalState;
            this.transientState = transientState;
        }

        @Override
        public boolean isFinal() {
            return finalState;
        }

        @Override
        public void setFinal(boolean _final) {
            this.finalState = _final;
        }

        @Override
        public boolean isTransient() {
            return transientState;
        }

        @Override
        public void setTransient(boolean _transient) {
            this.transientState = _transient;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public String getLabel () {
            return label;
        }

        @Override
        public String getDescription() {
            return t(description);
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }
    }

    /** Lima action definition. */
    public enum Action {

        HELP(n("lima.action.commandline.help"), MiscAction.class.getName() + "#help", "-h", "--help");

        /** Before init action step. */
        public static final int BEFORE_EXIT_STEP = 0;

        /** After init action step. */
        public static final int AFTER_INIT_STEP = 1;

        public String description;

        public String action;

        public String[] aliases;

        Action(String description, String action, String... aliases) {
            this.description = description;
            this.action = action;
            this.aliases = aliases;
        }

        public String getDescription() {
            return t(description);
        }
    }

    /**
     * Override save action to propagate some option to server.
     */
    @Override
    public void save(File file,
                     boolean forceAll,
                     String... excludeKeys) throws IOException {

        super.save(file, forceAll, excludeKeys);

        // propagate scale option to serveur option
        optionsService = LimaServiceFactory.getService(OptionsService.class);

        // Need to push again the values to server
        String scale = getOption(optionsService.getScaleOption().getKey());
        optionsService.setScale(scale);
        boolean currency = getOptionAsBoolean(optionsService.getCurrencyOption().getKey());
        optionsService.setCurrency(currency);
        String decimalSeparator = getOption(optionsService.getDecimalSeparatorOption().getKey());
        optionsService.setDecimalSeparator(decimalSeparator);
        String throusandSeparator = getOption(optionsService.getThousandSeparatorOption().getKey());
        optionsService.setThousandSeparator(throusandSeparator);

    }
}
