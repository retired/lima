/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.chorem.lima.LimaSwingConfig;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.error.ErrorReporter;

import javax.swing.*;
import java.awt.*;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Error helper.
 * <p/>
 * Used to easily switch real exception interface. Currently used : swingx.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class ErrorHelper implements ErrorReporter {

    /** Log. */
    private static final Log log = LogFactory.getLog(ErrorHelper.class);

    protected LimaSwingConfig config;

    public ErrorHelper(LimaSwingConfig config) {
        this.config = config;
    }

    /**
     * Display a user friendly error frame.
     *
     * @param parent  parent component
     * @param message message for user
     * @param cause   exception cause
     */
    public void showErrorDialog(Component parent, String message,
                                       Throwable cause) {
        JXErrorPane pane = new JXErrorPane();
        ErrorInfo info = new ErrorInfo(t("lima.error"),
                                       t("lima.error.message", message), null, null,
                                       cause, null, null);
        pane.setErrorInfo(info);
        pane.setErrorReporter(this);
        JXErrorPane.showDialog(parent, pane);
    }

    /**
     * Display a user friendly error frame.
     *
     * @param message message for user
     */
    public void showErrorMessage(String message) {
        JOptionPane.showMessageDialog(null, message,
                t("lima.error"), JOptionPane.ERROR_MESSAGE);
    }


    /*
     * @see org.jdesktop.swingx.error.ErrorReporter#reportError(org.jdesktop.swingx.error.ErrorInfo)
     */
    @Override
    public void reportError(ErrorInfo errorInfo) throws NullPointerException {

        try {
            String emailTo = config.getSupportEmail();

            MultiPartEmail email = new MultiPartEmail();
            // smtp
            email.setHostName("smtp");
            // to
            email.addTo(emailTo, "Support");
            // from
            email.setFrom("no-reply@nuiton.org", "Lima");
            // subject
            email.setSubject("Project error notification : Lima");

            // message description
            StringBuilder message = new StringBuilder();
            message.append(formatMessage("Project", "Lima " + config.getVersion()));
            message.append(formatMessage("Date", new Date().toString()));
            message.append(formatMessage("Title", errorInfo.getTitle()));
            message.append(formatMessage("Description", errorInfo.getBasicErrorMessage().replaceAll("<[^>]+>", "")));

            // message configuration
            message.append(formatMessage("Configuration", null));
            List<String> propertiesNames = new ArrayList<>(config.getOptions().stringPropertyNames());
            Collections.sort(propertiesNames);
            for (String propertyName : propertiesNames) {
                // security, don't send string containing password :
                if (!propertyName.contains("pass")) {
                    message.append("\t").append(propertyName).append(" : ").append(config.getOptions().getProperty(propertyName)).append("\n");
                }
            }

            // message exception
            StringWriter out = new StringWriter();
            PrintWriter writer = new PrintWriter(out);
            errorInfo.getErrorException().printStackTrace(writer);
            message.append(formatMessage("Exception", out.toString()));

            // send mail
            email.setMsg(message.toString());
            email.send();

            JOptionPane.showMessageDialog(null, "A report message has been sent to " + emailTo);
        } catch (EmailException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't send report email", ex);
            }
        }
    }

    protected String formatMessage(String category, String content) {
        String formatted = category + " :\n";
        if (StringUtils.isNotEmpty(content)) {
            formatted += "\t" + content + "\n";
        }
        return formatted;
    }
}
