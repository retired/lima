/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.util;

import org.chorem.lima.entity.Account;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;

public class AccountToString extends ObjectToStringConverter {

    private static AccountToString converter;

    @Override
    public String getPreferredStringForItem(Object item) {
        String result = null;
        if (item != null) {
            if (item instanceof Account) {
                Account account = (Account) item;
                result = account.getAccountNumber() + " - " + account.getLabel();
            }
        }
        return result;
    }

    public static AccountToString getInstance() {
        if (converter == null) {
            converter = new AccountToString();
        }
        return converter;
    }

}
