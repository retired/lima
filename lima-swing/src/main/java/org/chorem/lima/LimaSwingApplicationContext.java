/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima;

import jaxx.runtime.JAXXUtil;
import jaxx.runtime.context.DefaultApplicationContext;
import jaxx.runtime.context.JAXXContextEntryDef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.ui.LimaDecoratorProvider;
import org.chorem.lima.ui.MainView;
import org.chorem.lima.ui.MainViewHandler;
import org.nuiton.decorator.DecoratorProvider;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.i18n.init.I18nInitializer;
import org.nuiton.util.StringUtil;

import java.util.Locale;

/** @author chemit */
public class LimaSwingApplicationContext extends DefaultApplicationContext {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(LimaSwingApplicationContext.class);

    /**
     * l'intance partagée accessible après un appel à la méthode
     * {@link #init()}
     */
    protected static LimaSwingApplicationContext instance;

    public static final JAXXContextEntryDef<MainView> MAIN_UI_ENTRY_DEF = JAXXUtil.newContextEntryDef("mainUI", MainView.class);

    public static final JAXXContextEntryDef<DecoratorProvider> DECORATOR_PROVIDER_DEF = JAXXUtil.newContextEntryDef("decoratorProvider", DecoratorProvider.class);

    public static final JAXXContextEntryDef<LimaSwingConfig> CONFIG_DEF = JAXXUtil.newContextEntryDef("limaConfig", LimaSwingConfig.class);

    /**
     * @return <code>true</code> si le context a été initialisé via la méthode
     *         {@link #init()}, <ocde>false</code> autrement.
     */
    public static boolean isInit() {
        return instance != null;
    }

    /**
     * Permet l'initialisation du contexte applicatif et positionne
     * l'instance partagée.
     * <p/>
     * Note : Cette méthode ne peut être appelée qu'une seule fois.
     *
     * @return l'instance partagée
     * @throws IllegalStateException si un contexte applicatif a déja été positionné.
     */
    public static LimaSwingApplicationContext init() throws IllegalStateException {
        if (isInit()) {
            throw new IllegalStateException("there is already a application context registred.");
        }
        instance = new LimaSwingApplicationContext();
        instance.setContextValue(new MainViewHandler());
        CONFIG_DEF.setContextValue(instance, LimaSwingConfig.getInstance());
        DECORATOR_PROVIDER_DEF.setContextValue(instance, new LimaDecoratorProvider());
        return instance;
    }

    /**
     * Récupération du contexte applicatif.
     *
     * @return l'instance partagé du contexte.
     * @throws IllegalStateException si le contexte n'a pas été initialisé via
     *                               la méthode {@link #init()}
     */
    public static LimaSwingApplicationContext get() throws IllegalStateException {
        if (!isInit()) {
            throw new IllegalStateException("no application context registred.");
        }
        return instance;
    }

    public LimaSwingConfig getConfig() {
        return CONFIG_DEF.getContextValue(this);
    }

    public DecoratorProvider getDecoratorProvider() {
        return DECORATOR_PROVIDER_DEF.getContextValue(this);
    }

    public void initI18n(LimaSwingConfig config) {

        I18n.close();

        I18nInitializer i18nInitializer = new DefaultI18nInitializer("lima");
        Locale locale = config.getLocale();
        I18n.init(i18nInitializer, locale);

        // Default Locale for DatePicker
        Locale.setDefault(locale);

        long t00 = System.nanoTime();

        if (log.isDebugEnabled()) {
            log.debug("i18n language : " + locale);
            log.debug("i18n loading time : " +
                      StringUtil.convertTime(t00, System.nanoTime()));
        }
    }

    public MainView getMainUI() {
        return MAIN_UI_ENTRY_DEF.getContextValue(this);
    }

    /**
     * close the application's context.
     *
     */
    public void close() {
        if (log.isDebugEnabled()) {
            log.debug("closing context " + this);
        }

        // fermeture du context principal
        MainView mainUI = getMainUI();
        if (mainUI != null && mainUI.isVisible()) {
            mainUI.setVisible(false);
            mainUI.dispose();
        }

        if (log.isDebugEnabled()) {
            log.debug("context closed " + this);
        }
        //System.exit(0);
    }

    public static LimaSwingApplicationContext getContext() {
        return get();
    }
}
