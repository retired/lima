/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.fiscalperiod;

import org.chorem.lima.business.utils.FiscalPeriodComparator;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.AbstractLimaTableModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Modele de Fiscal period (rendu de list)
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class FiscalPeriodTableModel extends AbstractLimaTableModel<FiscalPeriod> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 77027335135838258L;

    public FiscalPeriodTableModel() {
        comparator = new FiscalPeriodComparator();
    }

    @Override
    protected void initColumn() {
        addColumn(new AbstractColumn<FiscalPeriodTableModel>(Date.class, t("lima.fiscalPeriod.begin"), false) {
            @Override
            public Object getValueAt(int row) {
                FiscalPeriod fiscalPeriod = tableModel.get(row);
                return fiscalPeriod.getBeginDate();
            }
        });

        addColumn(new AbstractColumn<FiscalPeriodTableModel>(Date.class, t("lima.fiscalPeriod.end"), false) {
            @Override
            public Object getValueAt(int row) {
                FiscalPeriod fiscalPeriod = tableModel.get(row);
                return fiscalPeriod.getEndDate();
            }
        });

        addColumn(new AbstractColumn<FiscalPeriodTableModel>(String.class, t("lima.fiscalPeriod.status"), false) {
            @Override
            public Object getValueAt(int row) {
                FiscalPeriod fiscalPeriod = tableModel.get(row);
                String status = t("lima.fiscalPeriod.open");
                if (fiscalPeriod.isLocked()) {
                    status =  t("lima.fiscalPeriod.closed");
                }
                return status;
            }

        });
    }
}
