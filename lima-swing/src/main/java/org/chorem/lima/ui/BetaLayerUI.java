/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;

import javax.swing.*;
import java.awt.*;

/**
 * Layer qui affiche "beta version".
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class BetaLayerUI extends AbstractLayerUI<JComponent> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 309245880711380974L;

    @Override
    protected void paintLayer(Graphics2D g2, JXLayer<? extends JComponent> l) {
        super.paintLayer(g2, l);

        // position
        //g2.translate(-35, 90); // top left
        g2.translate(l.getBounds().getMaxX() - 130, 0); // top right

        // yellow backgroung 
        g2.setColor(Color.YELLOW);
        g2.fillOval(0, -35, 130, 70);
        g2.fillRect(65, 0, 65, 35);

        // text
        g2.setFont(new Font("Dialog", Font.BOLD, 16));
        g2.setColor(Color.BLACK);
        g2.drawString("Beta version",12, 14);
        
        // for test only
        g2.setFont(new Font("Dialog", Font.ITALIC, 12));
        g2.setColor(Color.BLACK);
        g2.drawString("(for test only)",40,29);
    }
}
