/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.fiscalperiod;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.exceptions.AlreadyLockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.LastUnlockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoEmptyFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookImpl;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodImpl;
import org.chorem.lima.enums.MainUiRefreshComponent;
import org.chorem.lima.ui.MainView;
import org.chorem.lima.util.BigDecimalToString;
import org.chorem.lima.util.ErrorHelper;
import org.nuiton.util.DateUtil;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler pour la gestion des exercices (creation/fermeture).
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FiscalPeriodViewHandler implements ServiceListener {

    private static final Log log = LogFactory.getLog(FiscalPeriodViewHandler.class);

    protected FiscalPeriodView view;

    /** Service. */
    protected FiscalPeriodService fiscalPeriodService;

    protected FinancialTransactionService financialTransactionService;

    protected ErrorHelper errorHelper;

    /**
     * Constructor.
     * 
     * @param view managed view
     */
    public FiscalPeriodViewHandler(FiscalPeriodView view) {
        this.view = view;

        fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);
        financialTransactionService = LimaServiceFactory.getService(FinancialTransactionService.class);

        LimaServiceFactory.addServiceListener(FiscalPeriodService.class, this);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
    }

    public void init() {

        MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);

        InputMap inputMap = view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();

        // add action on Ctrl + N
        String binding = "new-FiscalPeriod";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 3174253799898553603L;

            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FISCAL_YEAR_PANE);
                addFiscalPeriod();
            }
        });

        // add action on Ctrl + M
        binding = "update-FiscalPeriod";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FISCAL_YEAR_PANE);
                updateFiscalPeriod();
            }
        });

        // add action on Delete
        binding = "remove-FiscalPeriod";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FISCAL_YEAR_PANE);
                deleteFiscalPeriod();
            }
        });

        // add action on Ctrl + B
        binding = "close-FiscalPeriod";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FISCAL_YEAR_PANE);
                blockFiscalPeriod();
            }
        });

        loadAllFiscalPeriod();
    }

    /**
     * Reload fiscal period list from service and update ui.
     */
    public void loadAllFiscalPeriod() {
        List<FiscalPeriod> periods = fiscalPeriodService.getAllFiscalPeriods();

        FiscalPeriodTableModel tableModel = view.getFiscalPeriodTableModel();
        tableModel.setValues(periods);
    }

    /**
     * Display add period form and perform add.
     */
    public void addFiscalPeriod() {

        FiscalPeriodTableModel tableModel = view.getFiscalPeriodTableModel();

        // set begin date picker
        Date beginDate = null;
        Date endDate;
        FiscalPeriod result = fiscalPeriodService.getLastFiscalPeriod();
        if (result != null){
            beginDate = DateUtils.addDays(result.getEndDate(), 1);
        }
        if (beginDate == null) {
            Calendar calendarBegin = Calendar.getInstance();
            // set begindate to JAN 1 - 0:00.000 of this years
            beginDate = calendarBegin.getTime();
            beginDate = DateUtils.truncate(beginDate, Calendar.YEAR);
        }

        // get end date
        endDate = DateUtils.addYears(beginDate, 1);
        endDate = DateUtils.addDays(endDate, -1);


        AddPeriod addPeriodDialog = new AddPeriod(view);
        addPeriodDialog.setTitle(t("lima.fiscalPeriod.add.form"));
        addPeriodDialog.getBeginDatePicker().setDate(beginDate);
        addPeriodDialog.getEndDatePicker().setDate(endDate);
        addPeriodDialog.setLocationRelativeTo(view);
        addPeriodDialog.setVisible(true);

        if (checkFiscalPeriod(addPeriodDialog)) {
            try {
                FiscalPeriod fiscalPeriod = new FiscalPeriodImpl();
                fiscalPeriod.setBeginDate(addPeriodDialog.getBeginDatePicker().getDate());
                fiscalPeriod.setEndDate(addPeriodDialog.getEndDatePicker().getDate());
                fiscalPeriod = fiscalPeriodService.createFiscalPeriod(fiscalPeriod);
                tableModel.addValue(fiscalPeriod);
            } catch (BeginAfterEndFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.fiscalPeriod.add.error.beginAfterEndFiscalPeriod",
                        e.getFiscalPeriod().getBeginDate(),
                        e.getFiscalPeriod().getEndDate()));
            } catch (MoreOneUnlockFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.fiscalPeriod.add.error.moreOneUnlockFiscalPeriod",
                        e.getCountUnlockFiscalPeriod()));
            } catch (NotBeginNextDayOfLastFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.fiscalPeriod.add.error.notBeginNextDayOfLastFiscalPeriod",
                        e.getFiscalPeriod().getEndDate()));
            }
        }
    }

    protected boolean checkFiscalPeriod(AddPeriod addPeriodDialog) {
        Date beginDate = addPeriodDialog.getBeginDatePicker().getDate();
        Date endDate = addPeriodDialog.getEndDatePicker().getDate();

        String title = addPeriodDialog.getTitle();

        if (!addPeriodDialog.isValidate()) {
            return false;
        } else if (endDate.before(beginDate)) {
            errorHelper.showErrorMessage(t("lima.fiscalPeriod.add.error.beginAfterEndFiscalPeriod",
                    beginDate,
                    endDate));
            return false;
        } else {
            //check if fiscalperiod have 12 months, ask a confirmation
            int nbMonth = DateUtil.getDifferenceInMonths(beginDate, endDate);
            int response = JOptionPane.YES_OPTION;
            if (nbMonth != 12) {
                response = JOptionPane.showConfirmDialog(view,
                        t("lima.fiscalPeriod.add.confirm.moreThan12"),
                        title,
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            }
            return (response == JOptionPane.YES_OPTION);


        }


    }

    /**
     * Display add period form and perform add.
     */
    public void updateFiscalPeriod() {

        FiscalPeriodTable table = view.getFiscalPeriodTable();
        FiscalPeriodTableModel tableModel = view.getFiscalPeriodTableModel();

        int selectedRow = table.getSelectedRow();

        if (selectedRow >=0 ) {

            FiscalPeriod selectedFiscalPeriod = tableModel.get(selectedRow);

            if (!selectedFiscalPeriod.isLocked()) {

                AddPeriod addPeriodDialog = new AddPeriod(view);
                addPeriodDialog.setTitle(t("lima.fiscalPeriod.update.form"));
                addPeriodDialog.getBeginDatePicker().setDate(selectedFiscalPeriod.getBeginDate());
                addPeriodDialog.getEndDatePicker().setDate(selectedFiscalPeriod.getEndDate());
                addPeriodDialog.setModifyPeriod(true);
                addPeriodDialog.setLocationRelativeTo(view);
                addPeriodDialog.setVisible(true);

                if (checkFiscalPeriod(addPeriodDialog)) {
                    selectedFiscalPeriod.setEndDate(addPeriodDialog.getEndDatePicker().getDate());
                    fiscalPeriodService.updateEndDate(selectedFiscalPeriod);
                    tableModel.fireTableRowsUpdated(selectedRow, selectedRow);
                }
            }
        }
    }

    /**
     * Undisplay period and perform delete.
     */
    public void deleteFiscalPeriod() {

        FiscalPeriodTable table = view.getFiscalPeriodTable();
        FiscalPeriodTableModel tableModel = view.getFiscalPeriodTableModel();

        int selectedRow = table.getSelectedRow();

        if (selectedRow >=0 ) {

            FiscalPeriod selectedFiscalPeriod = tableModel.get(selectedRow);

            if (!selectedFiscalPeriod.isLocked()
                    && financialTransactionService.getAllFinancialTransactions(selectedFiscalPeriod).isEmpty()) {

                //check if the user want to delete the fiscal year
                int response = JOptionPane.showConfirmDialog(view,
                        t("lima.fiscalPeriod.delete.confirmation"),
                        t("lima.fiscalPeriod.delete.title"), JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);

                if (response == JOptionPane.YES_OPTION) {
                    try {
                        fiscalPeriodService.deleteFiscalPeriod(selectedFiscalPeriod);

                        tableModel.remove(selectedFiscalPeriod);
                        view.setBlockEnabled(false);
                        view.setDeleteEnabled(false);
                    } catch (NoEmptyFiscalPeriodException e) {
                        errorHelper.showErrorMessage(t("lima.fiscalPeriod.delete.error.noEmptyFiscalPeriod",
                                e.getFinancialTransactions().size()));
                    }
                }
            }
        }
    }

    /**
     * Block selected fiscal period,
     * and make new fiscal period and retained earnings
     * after user confirmations
     */
    public void blockFiscalPeriod() {
        FiscalPeriodTable table = view.getFiscalPeriodTable();
        final FiscalPeriodTableModel tableModel = view.getFiscalPeriodTableModel();

        final int selectedRow = table.getSelectedRow();

        if (selectedRow >= 0) {

            final FiscalPeriod selectedFiscalPeriod = tableModel.get(selectedRow);

            if (!selectedFiscalPeriod.isLocked()) {

                //check if the user want to block the fiscal year
                int answerBlock = JOptionPane.showConfirmDialog(view,
                        t("lima.fiscalPeriod.block.confirmation"),
                        t("lima.fiscalPeriod.block.title"), JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);

                if (answerBlock == JOptionPane.YES_OPTION) {

                    //report if they are at least one transaction to be report
                    if (fiscalPeriodService.isRetainedEarnings(selectedFiscalPeriod)) {

                        // find next fiscal year
                        FiscalPeriod lastFiscalPeriod = fiscalPeriodService.getLastFiscalPeriod();
                        if (selectedFiscalPeriod.equals(lastFiscalPeriod)) {

                            //check if the user wants to create a new fiscal year
                            int answerCreate = JOptionPane.showConfirmDialog(view,
                                    t("lima.fiscalPeriod.block.newYear"),
                                    t("lima.fiscalPeriod.block.title"), JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE);

                            // create a new fiscal year
                            if (answerCreate == JOptionPane.YES_OPTION) {
                                addFiscalPeriod();
                            }
                        }


                        lastFiscalPeriod = fiscalPeriodService.getLastFiscalPeriod();
                        // check if next fiscal year don't exist
                        if (selectedFiscalPeriod.equals(lastFiscalPeriod)) {

                            JOptionPane.showMessageDialog(view,
                                    t("lima.fiscalPeriod.block.cantBalanceIfNextPeriodNotExist"),
                                    t("lima.fiscalPeriod.block.title"),
                                    JOptionPane.ERROR_MESSAGE);

                        } else {

                            //check if the user wants to report datas
                            int answerRetainedEarnings = JOptionPane.showConfirmDialog(view,
                                    t("lima.fiscalPeriod.block.addRetainedEarnings"),
                                    t("lima.fiscalPeriod.block.title"), JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE);

                            if (answerRetainedEarnings == JOptionPane.YES_OPTION) {
                                //Sets EntryBook
                                RetainedEarningsEntryBookForm entryBookForm =
                                        new RetainedEarningsEntryBookForm(view);

                                // jaxx constructor don't call super() ?
                                entryBookForm.setLocationRelativeTo(view);
                                entryBookForm.setVisible(true);

                                if (entryBookForm.getValidate()) {
                                    final EntryBook entryBook;

                                    if (entryBookForm.getNewEntryBookCheckBox().isSelected()) {
                                        entryBook = new EntryBookImpl();
                                        entryBook.setCode(entryBookForm.getCodeField().getText());
                                        entryBook.setLabel(entryBookForm.getLabelField().getText());
                                    } else {
                                        entryBook = (EntryBook) entryBookForm.getEntryBookComboBox().getSelectedItem();
                                    }

                                    if (entryBook != null && StringUtils.isNotBlank(entryBook.getCode())) {

                                        final RetainedEarningsWait retainedEarningsWait = new RetainedEarningsWait(view);
                                        retainedEarningsWait.setLocationRelativeTo(view);

                                        new SwingWorker<FiscalPeriod, Void>() {
                                            @Override
                                            protected FiscalPeriod doInBackground() throws Exception {
                                                FiscalPeriod fiscalPeriodBlocked = fiscalPeriodService.retainedEarningsAndBlockFiscalPeriod(selectedFiscalPeriod, entryBook);
                                                tableModel.setValue(selectedRow, fiscalPeriodBlocked);

                                                view.setBlockEnabled(false);
                                                view.setDeleteEnabled(false);

                                                return fiscalPeriodBlocked;
                                            }

                                            @Override
                                            protected void done() {
                                                retainedEarningsWait.setVisible(false);
                                                try {
                                                    get();
                                                } catch (InterruptedException e) {
                                                    catchExceptionDuringBlock(e);
                                                } catch (ExecutionException e) {
                                                    catchExceptionDuringBlock(e.getCause());
                                                }


                                            }
                                        }.execute();
                                        retainedEarningsWait.setVisible(true);

                                    } else {

                                        JOptionPane.showMessageDialog(view,
                                                t("lima.fiscalPeriod.block.cantBalanceNotBook"),
                                                t("lima.fiscalPeriod.block.title"),
                                                JOptionPane.ERROR_MESSAGE);
                                    }
                                }

                            } else {

                                JOptionPane.showMessageDialog(view,
                                        t("lima.fiscalPeriod.block.cantBlockNotBalance"),
                                        t("lima.fiscalPeriod.block.title"),
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        }

                    } else {
                        try {
                            FiscalPeriod fiscalPeriodBlocked = fiscalPeriodService.blockFiscalPeriod(selectedFiscalPeriod);
                            tableModel.setValue(selectedRow, fiscalPeriodBlocked);

                            view.setBlockEnabled(false);
                            view.setDeleteEnabled(false);
                        } catch (Exception e) {
                            catchExceptionDuringBlock(e);
                        }
                    }
                }
            }
        }
    }

    protected void catchExceptionDuringBlock(Throwable e) {

        if (e instanceof LastUnlockedFiscalPeriodException) {
            errorHelper.showErrorMessage(t("lima.fiscalPeriod.block.error.lastUnlockedFiscalPeriod",
                    ((LastUnlockedFiscalPeriodException) e).getFiscalPeriod().getBeginDate(),
                    ((LastUnlockedFiscalPeriodException) e).getFiscalPeriod().getEndDate()));
        } else if (e instanceof AlreadyLockedFiscalPeriodException) {
            errorHelper.showErrorMessage(t("lima.fiscalPeriod.block.error.AlreadyLockedFiscalPeriod"));
        } else if (e instanceof UnbalancedFinancialTransactionsException) {
            StringBuilder message = new StringBuilder(t("lima.fiscalPeriod.block.error.unbalanced.main"));
            for (FinancialTransaction transaction : ((UnbalancedFinancialTransactionsException) e).getFinancialTransactions()) {
                message.append("\n    - ");
                message.append(t("lima.fiscalPeriod.block.error.unbalanced.transaction",
                        transaction.getTransactionDate(),
                        BigDecimalToString.format(transaction.getAmountDebit()),
                        BigDecimalToString.format(transaction.getAmountCredit())));
            }
            errorHelper.showErrorMessage(message.toString());
        } else if (e instanceof UnfilledEntriesException) {
            StringBuilder message = new StringBuilder(t("lima.fiscalPeriod.block.error.unfilled.main"));
            for (Entry entry : ((UnfilledEntriesException) e).getEntries()) {
                message.append("\n    - ");
                message.append(t("lima.fiscalPeriod.block.error.unfilled.entry",
                        entry.getFinancialTransaction().getTransactionDate(),
                        BigDecimalToString.format(entry.getAmount())));
                if (entry.getAccount() == null) {
                    message.append("\n        - ");
                    message.append(t("lima.fiscalPeriod.block.error.unfilled.noAccount"));
                }
                if (org.apache.commons.lang3.StringUtils.isBlank(entry.getVoucher())) {
                    message.append("\n        - " );
                    message.append(t("lima.fiscalPeriod.block.error.unfilled.noVoucher"));
                }
                if (org.apache.commons.lang3.StringUtils.isBlank(entry.getDescription())) {
                    message.append("\n        - ");
                    message.append(t("lima.fiscalPeriod.block.error.unfilled.noDescription"));
                }
            }
            errorHelper.showErrorMessage(message.toString());
        } else if (e instanceof WithoutEntryBookFinancialTransactionsException) {
            StringBuilder message = new StringBuilder(t("lima.fiscalPeriod.block.error.withoutEntryBook.main"));
            for (FinancialTransaction transaction : ((WithoutEntryBookFinancialTransactionsException) e).getFinancialTransactions()) {
                message.append("\n    - ");
                message.append(t("lima.fiscalPeriod.block.error.withoutEntryBook.transaction",
                        transaction.getTransactionDate(),
                        BigDecimalToString.format(transaction.getAmountCredit())));
            }
            errorHelper.showErrorMessage(message.toString());
        } else  {
            errorHelper.showErrorMessage(e.getMessage());
        }






    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        //refresh on import datas
        if (methodName.contains("importAll")) {
            loadAllFiscalPeriod();
        }
    }
    
    /**
     * Manage the differents buttons for fiscal period
     * */
    protected void onSelectionChanged(ListSelectionEvent listSelectionEvent){
        if(!listSelectionEvent.getValueIsAdjusting()) {
            DefaultListSelectionModel listSelectionModel = (DefaultListSelectionModel)listSelectionEvent.getSource();

            int selectedRow = listSelectionEvent.getFirstIndex();
            if (!listSelectionModel.isSelectedIndex(selectedRow)) {
                selectedRow = listSelectionEvent.getLastIndex();
            }

            FiscalPeriodTableModel tableModel = view.getFiscalPeriodTableModel();
            if (tableModel.getRowCount() != selectedRow) {
                FiscalPeriod fiscalPeriodAt = tableModel.get(selectedRow);

                List<FinancialTransaction> financialTransactionList = financialTransactionService.getAllFinancialTransactions(fiscalPeriodAt);

                boolean enableBlock = false;
                boolean enableDelete = false;

                if (log.isDebugEnabled()) {
                    log.debug("reloadEnablingButton");
                }

              if (selectedRow != -1){
                  if (log.isDebugEnabled()) {
                      log.debug("selectedFiscalPeriod != null");
                  }

                  //no action possible for fiscal period closed
                  if (!fiscalPeriodAt.isLocked()){
                      if (log.isDebugEnabled()) {
                          log.debug("selectedFiscalPeriod not locked");
                      }
                      enableBlock = true;

                      //only an empty and open fiscal period may be deleted
                      enableDelete = financialTransactionList.size() <= 0;
                   }
                }
                view.setBlockEnabled(enableBlock);
                view.setDeleteEnabled(enableDelete);
            }
        }
    }  
        
}
