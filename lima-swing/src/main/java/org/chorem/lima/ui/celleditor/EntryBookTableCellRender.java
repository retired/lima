package org.chorem.lima.ui.celleditor;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.EntryBook;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class EntryBookTableCellRender extends DefaultLimaTableCellRenderer {

    @Override
    public void setValue(Object value) {
        Object result = value;
        if (value != null && value instanceof EntryBook) {
            EntryBook entryBook= (EntryBook) value;
            String label = entryBook.getCode();
            if (entryBook.getLabel() != null) {
                label += " - " + entryBook.getLabel();
            }
            result = label;
        }
        super.setValue(result);
    }

}
