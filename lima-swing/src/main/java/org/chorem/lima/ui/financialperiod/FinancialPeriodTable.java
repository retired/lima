/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialperiod;

import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.ui.celleditor.DateLimaTableCellRenderer;
import org.chorem.lima.ui.celleditor.DefaultLimaTableCellRenderer;
import org.chorem.lima.ui.celleditor.EntryBookTableCellRender;
import org.chorem.lima.ui.celleditor.TableCellErrorDetector;
import org.chorem.lima.ui.common.AbstractLimaTable;

import java.util.Date;

/**
 * Financial period table adding color hidhlighter and key management.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialPeriodTable extends AbstractLimaTable<FinancialPeriodViewHandler> {

    /** serialVersionUID. */
    private static final long serialVersionUID = -1960326844433064178L;

    /**
     * Constructor, install highlighter.
     * 
     * @param handler
     */
    public FinancialPeriodTable(FinancialPeriodViewHandler handler) {
        super(handler);

        // renderer
        TableCellErrorDetector errorDetector = new FinancialPeriodErrorDetector();

        DefaultLimaTableCellRenderer renderer = new DefaultLimaTableCellRenderer();
        renderer.setErrorDetector(errorDetector);
        setDefaultRenderer(Object.class, renderer);

        renderer = new DateLimaTableCellRenderer();
        renderer.setErrorDetector(errorDetector);
        setDefaultRenderer(Date.class, renderer);

        renderer = new EntryBookTableCellRender();
        renderer.setErrorDetector(errorDetector);
        setDefaultRenderer(EntryBook.class, renderer);

    }

}
