/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialstatementchart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.FinancialStatementService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.FinancialStatement;
import org.chorem.lima.entity.FinancialStatementImpl;
import org.chorem.lima.enums.FinancialStatementsChartEnum;
import org.chorem.lima.enums.ImportExportEnum;
import org.chorem.lima.ui.importexport.ImportExport;
import org.chorem.lima.util.ReportDialogView;
import org.jdesktop.swingx.JXTreeTable;
import org.nuiton.util.Resource;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.tree.TreePath;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler associated with account view.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class FinancialStatementChartViewHandler implements ServiceListener {

    /** log. */
    private static final Log log = LogFactory.getLog(FinancialStatementChartViewHandler.class);

    protected FinancialStatementService financialStatementService;

    protected FinancialStatementChartView view;

    protected FinancialStatementChartViewHandler(FinancialStatementChartView view) {
        this.view = view;

        financialStatementService =
                LimaServiceFactory.getService(
                        FinancialStatementService.class);
        LimaServiceFactory.addServiceListener(ImportService.class, this);

        init();
    }

    public void init() {

        InputMap inputMap = view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();

        // add action on Ctrl + Maj + N
        String binding = "add-FinancialStatementHeader";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK + InputEvent.SHIFT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -7788186392450704736L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addFinancialStatementHeader();
            }
        });

        // add action on Ctrl + N
        binding = "add-FinancialStatementMovement";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -2024337652917520114L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addFinancialStatementMovement();
            }
        });

        // add action on Ctrl + M
        binding = "update-FinancialStatement";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 38900830693943289L;

            @Override
            public void actionPerformed(ActionEvent e) {
                updateFinancialStatement();
            }
        });

        // add action on Delete
        binding = "remove-FinancialStatement";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 5281128699359433458L;

            @Override
            public void actionPerformed(ActionEvent e) {
                removeFinancialStatement();
            }
        });

        // add action on Ctrl + I
        binding = "import-FinancialStatement";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -8953401784332356894L;

            @Override
            public void actionPerformed(ActionEvent e) {
                importFinancialStatementChart();
            }
        });

        // add action on Ctrl + K
        binding = "check-FinancialStatement";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_K, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 4940041938090472686L;

            @Override
            public void actionPerformed(ActionEvent e) {
                financialStatementChartCheck();
            }
        });
    }

    /** Add new account with account form. */
    public void addFinancialStatementHeader() {

        JXTreeTable treeTable = view.getTreeTable();
        FinancialStatementChartTreeTableModel treeTableModel =
                (FinancialStatementChartTreeTableModel) treeTable.getTreeTableModel();

        FinancialStatement newFinancialStatementHeader =
                new FinancialStatementImpl();
        final FinancialStatementHeaderForm financialStatementHeaderForm =
                new FinancialStatementHeaderForm(view);

        financialStatementHeaderForm.setFinancialStatement(newFinancialStatementHeader);

        InputMap inputMap = financialStatementHeaderForm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = financialStatementHeaderForm.getRootPane().getActionMap();
        String binding = "dispose";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 743244863218861378L;

            @Override
            public void actionPerformed(ActionEvent e) {
                financialStatementHeaderForm.performCancel();
            }
        });

        // jaxx constructor don't call super() ?
        financialStatementHeaderForm.setLocationRelativeTo(view);
        financialStatementHeaderForm.setVisible(true);

        // null == cancel action
        newFinancialStatementHeader = financialStatementHeaderForm.getFinancialStatement();
        if (newFinancialStatementHeader != null) {
            newFinancialStatementHeader.setHeader(true);
            // get current selection path
            TreePath treePath;
            int selectedRow = treeTable.getSelectedRow();
            if (selectedRow != -1) {
                treePath = treeTable.getPathForRow(selectedRow);
            } else {
                treePath = new TreePath(treeTableModel.getRoot());
            }

            // add it
            treeTableModel.addFinancialStatement(treePath, newFinancialStatementHeader);
        }
    }

    public void addFinancialStatementMovement() {

        JXTreeTable treeTable = view.getTreeTable();
        FinancialStatementChartTreeTableModel treeTableModel =
                (FinancialStatementChartTreeTableModel) treeTable.getTreeTableModel();

        FinancialStatement newFinancialStatementMovement =
                new FinancialStatementImpl();
        final FinancialStatementMovementForm financialStatementMovementForm =
                new FinancialStatementMovementForm(view);
        financialStatementMovementForm.setFinancialStatement(newFinancialStatementMovement);

        InputMap inputMap = financialStatementMovementForm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = financialStatementMovementForm.getRootPane().getActionMap();
        String binding = "dispose";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 4247772767239546785L;

            @Override
            public void actionPerformed(ActionEvent e) {
                financialStatementMovementForm.performCancel();
            }
        });

        // jaxx constructor don't call super() ?
        financialStatementMovementForm.setLocationRelativeTo(view);
        financialStatementMovementForm.setVisible(true);
        newFinancialStatementMovement = financialStatementMovementForm.getFinancialStatement();

        // null == cancel action
        if (newFinancialStatementMovement != null) {
            // get current selection path
            TreePath treePath;
            int selectedRow = view.treeTable.getSelectedRow();
            treePath = view.treeTable.getPathForRow(selectedRow);
            // add it
            treeTableModel.addFinancialStatement(treePath, newFinancialStatementMovement);
        }
    }

    /**
     * Open account form with selected account.
     * Verifiy if it's an account or a subledger
     */
    public void updateFinancialStatement() {

        JXTreeTable treeTable = view.getTreeTable();
        FinancialStatementChartTreeTableModel treeTableModel =
                (FinancialStatementChartTreeTableModel) treeTable.getTreeTableModel();

        // get selected account
        int selectedRow = treeTable.getSelectedRow();
        TreePath treePath = treeTable.getPathForRow(selectedRow); // not null
        FinancialStatement financialStatement =
                (FinancialStatement) treePath.getLastPathComponent();
        //update Account or update SubLedger
        if (financialStatement != null) {

            //test if selectedrow is account or ledger
            log.debug(financialStatement.isHeader());
            if (financialStatement.isHeader()) {
                final FinancialStatementHeaderForm financialStatementHeaderForm =
                        new FinancialStatementHeaderForm(view);
                financialStatementHeaderForm.setFinancialStatement(financialStatement);

                InputMap inputMap = financialStatementHeaderForm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
                ActionMap actionMap = financialStatementHeaderForm.getRootPane().getActionMap();
                String binding = "dispose";
                inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
                actionMap.put(binding, new AbstractAction() {
                    private static final long serialVersionUID = 3175340724715264379L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        financialStatementHeaderForm.performCancel();
                    }
                });
                // jaxx constructor don't call super() ?
                financialStatementHeaderForm.setLocationRelativeTo(view);
                financialStatementHeaderForm.setVisible(true);
                // null == cancel action
                financialStatement = financialStatementHeaderForm.getFinancialStatement();
            }
            // else is a movement
            else {
                final FinancialStatementMovementForm financialStatementMovementForm =
                        new FinancialStatementMovementForm(view);
                financialStatementMovementForm.setFinancialStatement(financialStatement);

                InputMap inputMap = financialStatementMovementForm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
                ActionMap actionMap = financialStatementMovementForm.getRootPane().getActionMap();
                String binding = "dispose";
                inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
                actionMap.put(binding, new AbstractAction() {
                    private static final long serialVersionUID = -3722767598494135172L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        financialStatementMovementForm.performCancel();
                    }
                });

                // jaxx constructor don't call super() ?
                financialStatementMovementForm.setLocationRelativeTo(view);
                financialStatementMovementForm.setVisible(true);
                // null == cancel action
                financialStatement =
                        financialStatementMovementForm.getFinancialStatement();
            }
            //if action confirmed
            if (financialStatement != null) {
                // update it
                treeTableModel.updateFinancialStatement(treePath, financialStatement);
            }
        }
    }

    /** Ask for user to remove for selected account, and remove it if confirmed. */
    public void removeFinancialStatement() {
        JXTreeTable treeTable = view.getTreeTable();
        FinancialStatementChartTreeTableModel treeTableModel =
                (FinancialStatementChartTreeTableModel) treeTable.getTreeTableModel();

        // Any row selected
        int selectedRow = view.treeTable.getSelectedRow();
        if (selectedRow != -1) {
            int n = JOptionPane.showConfirmDialog(view,
                                                  t("lima.financialStatement.remove.confirm"),
                                                  t("lima.confirmation"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE);
            if (n == JOptionPane.YES_OPTION) {
                // update view of treetable
                TreePath treePath = view.treeTable.getPathForRow(selectedRow);
                FinancialStatement financialStatement =
                        (FinancialStatement) treePath.getLastPathComponent();

                treeTableModel.removeFinancialStatementObject(treePath, financialStatement);
            }
        }
    }

    public void refresh() {
        JXTreeTable treeTable = view.getTreeTable();
        FinancialStatementChartTreeTableModel treeTableModel =
                (FinancialStatementChartTreeTableModel) treeTable.getTreeTableModel();
        treeTableModel.refreshTree();
        //refresh view
        view.repaint();
    }

    public void importFinancialStatementChart() {
        final FinancialStatementImportForm form =
                new FinancialStatementImportForm();

        InputMap inputMap = form.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = form.getRootPane().getActionMap();
        String binding = "dispose";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -8291208693591479867L;

            @Override
            public void actionPerformed(ActionEvent e) {
                form.performCancel();
            }
        });

        form.setLocationRelativeTo(view);
        form.setVisible(true);

        Object value = form.getChartFinancialStatementCombo().getSelectedItem();
        // if action confirmed
        if (value != null) {
            if (form.getDeleteChartFinancialStatement().isSelected()) {
                financialStatementService.removeAllFinancialStatement();
            }
            FinancialStatementsChartEnum financialStatementsEnum =
                    (FinancialStatementsChartEnum) value;
            ImportExport importExport = new ImportExport(view);
            importExport.importExport(ImportExportEnum.CSV_FINANCIALSTATEMENTS_IMPORT,
                    null, financialStatementsEnum.getDefaultFileUrl(), true);

            FinancialStatementChartTreeTableModel treeTableModel = (FinancialStatementChartTreeTableModel) view.getTreeTable().getTreeTableModel();

            treeTableModel.refreshTree();
        }
    }


    public void financialStatementChartCheck() {
        java.util.List<Account> unusedAccounts = financialStatementService.checkFinancialStatementChart();

        StringBuilder result = new StringBuilder(t("lima.financialStatements.check.warn")  + "\n\n");

        for (Account account : unusedAccounts) {

            result.append(t("lima.financialStatements.check.nothing",
                    account.getAccountNumber(), account.getLabel()) + "\n");
        }

        showReportDialog(result.toString(), t("lima.financialStatements.check"), view);
    }
    
    /**
     * Permet d'afficher une boite de dialogue avec rapport
     *
     */
    public void showReportDialog(String message, String title, Component parent) {
        ReportDialogView reportDialogView = new ReportDialogView();
        reportDialogView.setIconImage(Resource.getIcon("icons/lima.png").getImage());
        JTextArea textArea = reportDialogView.getTextArea();
        textArea.setText(message);
        reportDialogView.setSize(600, 400);
        reportDialogView.setTitle(title);
        reportDialogView.setLocationRelativeTo(parent);
        reportDialogView.setVisible(true);
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {

        if (methodName.contains("FinancialStatements")
            || methodName.contains("importAll")
            || methodName.contains("importAs")) {
            refresh();
        }
    }

}
