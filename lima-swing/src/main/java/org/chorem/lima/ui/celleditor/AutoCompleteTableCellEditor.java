package org.chorem.lima.ui.celleditor;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import java.util.List;
import java.util.Map;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class AutoCompleteTableCellEditor extends StringTableCellEditor {

    protected static final Map<String, List<String>> PRECEDING_VALUES_BY_ID = Maps.newHashMap();

    List<String> precedingValues;

    public AutoCompleteTableCellEditor(String id) {
        precedingValues = PRECEDING_VALUES_BY_ID.computeIfAbsent(id, k -> Lists.newLinkedList());

        AutoCompleteDecorator.decorate(getComponent(), precedingValues, false);
    }

    @Override
    public String getCellEditorValue() {
        String stringValue = super.getCellEditorValue().toString();
        if (!precedingValues.contains(stringValue)) {
            precedingValues.add(stringValue);
        }
        return stringValue;
    }
}
