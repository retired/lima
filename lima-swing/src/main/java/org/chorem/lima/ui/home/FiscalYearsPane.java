/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.home;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.ui.MainView;

import javax.swing.event.HyperlinkEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Panenl de la home view de lima qui affiche les statistiques de
 * l'exercice en cours.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FiscalYearsPane extends AbstractHomePane implements ServiceListener {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(FiscalYearsPane.class);

    protected FiscalPeriodService fiscalPeriodService;

    /**
     * Constructors (init services).
     * 
     * @param view home view
     */
    public FiscalYearsPane(HomeView view) {
        super(view);

        fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);
        LimaServiceFactory.addServiceListener(FiscalPeriodService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);

        refresh();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType())) {
            if (e.getDescription().equals("#fiscalperiodschart")) {
                MainView ui = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                ui.getHandler().showFiscalPeriodView(ui);
            }
        }
    }

    public void refresh() {

        log.debug("Rafraîchissement fiscal periods pane");
        
        String htmlBegin = "<font face='sans-serif' size=3>"
                           + "<p style=vertical-align:'bottom', horizontal-align:'center'>";
        String htmlEnd = "</p></font>";

        //FISCAL PERIOD
        List<FiscalPeriod> fiscalPeriods =
                fiscalPeriodService.getAllFiscalPeriods();
        List<FiscalPeriod> unblockedFiscalPeriods =
                fiscalPeriodService.getAllUnblockedFiscalPeriods();
        if (unblockedFiscalPeriods.size() > 0) {
            setBackground(GREEN_BACKGROUND);
            String fiscalString = t("lima.home.fiscalYears.opened", unblockedFiscalPeriods.size()) + "<br/>"
                                  + t("lima.home.fiscalYears.closed", fiscalPeriods.size() - unblockedFiscalPeriods.size())
                                  + "<br/><br/><a href='#fiscalperiodschart'>"
                                  + t("lima.home.fiscalYears.modify") + "</a>";
            //set Text
            setText(htmlBegin + fiscalString + htmlEnd);


        } else {
            setBackground(RED_BACKGROUND);
            String fiscalString = t("lima.home.fiscalYears.noOpen");
            if (fiscalPeriods.size() > 0) {
                fiscalString += "<br/>" + t("lima.home.fiscalYears.closed", fiscalPeriods.size());
            }
            fiscalString += "<br/><br/><a href='#fiscalperiodschart'>"
                            + t("lima.home.fiscalYears.create") + "</a>";
            //set Text
            setText(htmlBegin + fiscalString + htmlEnd);

        }
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        log.debug("Nom de la méthode : " + methodName);
        if (methodName.contains("FiscalPeriod") ||
            methodName.contains("importAll") ||
            methodName.contains("importAs")) {
            refresh();
        }
    }

}
