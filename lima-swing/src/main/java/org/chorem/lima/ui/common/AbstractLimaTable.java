package org.chorem.lima.ui.common;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.ui.celleditor.BigDecimalTableCellEditor;
import org.chorem.lima.ui.celleditor.BigDecimalTableCellRenderer;
import org.chorem.lima.ui.celleditor.DateLimaTableCellRenderer;
import org.chorem.lima.ui.celleditor.DefaultLimaTableCellRenderer;
import org.chorem.lima.ui.celleditor.StringTableCellEditor;
import org.jdesktop.swingx.JXTable;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;
import java.awt.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public abstract class AbstractLimaTable<H> extends JXTable {

    protected static final Log log = LogFactory.getLog(AbstractLimaTable.class);

    private static final long serialVersionUID = -4195941654485008887L;

    protected H handler;

    public AbstractLimaTable(H handler) {
        this.handler = handler;

        setShowHorizontalLines(true);
        setShowVerticalLines(true);
        setGridColor(new Color(232, 232, 246));

        initNavigation();

        // renderer
        setDefaultRenderer(Object.class, new DefaultLimaTableCellRenderer());
        setDefaultRenderer(Date.class, new DateLimaTableCellRenderer());
        setDefaultRenderer(BigDecimal.class, new BigDecimalTableCellRenderer());
//        setDefaultRenderer(Account.class, new AccountTableCellRenderer());
//        setDefaultRenderer(EntryBook.class, new EntryBookTableCellRender());

        // editor
        setDefaultEditor(BigDecimal.class, new BigDecimalTableCellEditor());
//        setDefaultEditor(Account.class, new AccountTableCellEditor());
//        setDefaultEditor(EntryBook.class, new EntryBookTableCellEditor());
        setDefaultEditor(String.class, new StringTableCellEditor());

        setShowHorizontalLines(false);
    }

    @Override
    public void setModel(TableModel model) {
        super.setModel(model);
        if (model instanceof AbstractLimaTableModel) {
            for (int columnIndex = 0; columnIndex < getColumnModel().getColumnCount(); columnIndex++) {
                Column column = ((AbstractLimaTableModel) model).getColumn(columnIndex);
                TableCellEditor cellEditor = column.getCellEditor();
                if (cellEditor != null) {
                    getColumnModel().getColumn(columnIndex).setCellEditor(cellEditor);
                }
            }
        }
    }

    protected void initNavigation() {}

    public H getHandler() {
        return handler;
    }

    public void exit() {
        if (isEditing()) {
            TableCellEditor editor = getCellEditor();
            editor.stopCellEditing();
        }
    }

}
