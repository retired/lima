package org.chorem.lima.ui.financialtransaction;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.ui.celleditor.AccountTableCellRenderer;
import org.chorem.lima.ui.celleditor.BigDecimalTableCellRenderer;
import org.chorem.lima.ui.celleditor.DateLimaTableCellRenderer;
import org.chorem.lima.ui.celleditor.DefaultLimaTableCellRenderer;
import org.chorem.lima.ui.celleditor.EntryBookTableCellRender;
import org.chorem.lima.ui.celleditor.TableCellErrorDetector;
import org.chorem.lima.ui.common.AbstractLimaTable;
import org.chorem.lima.ui.common.CellRouteHorizontalAction;
import org.chorem.lima.ui.common.CellRouteVerticalAction;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FinancialTransactionDefaultTable<H> extends AbstractLimaTable<H> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 3133690382049594727L;

    public FinancialTransactionDefaultTable(H handler) {
        super(handler);
        setSortable(false);

        // renderer
        TableCellErrorDetector errorDetector = new FinancialTransactionErrorDetector();

        DefaultLimaTableCellRenderer renderer = new DefaultLimaTableCellRenderer();
        renderer.setMandatory(true);
        setDefaultRenderer(Object.class, renderer);

        renderer.setMandatory(true);
        setDefaultRenderer(Integer.class, renderer);

        renderer = new DateLimaTableCellRenderer();
        renderer.setMandatory(true);
        setDefaultRenderer(Date.class, renderer);

        renderer = new BigDecimalTableCellRenderer();
        renderer.setErrorDetector(errorDetector);
        setDefaultRenderer(BigDecimal.class, renderer);

        renderer = new AccountTableCellRenderer();
        renderer.setMandatory(true);
        setDefaultRenderer(Account.class, renderer);

        renderer = new EntryBookTableCellRender();
        renderer.setMandatory(true);
        setDefaultRenderer(EntryBook.class, renderer);
    }

    @Override
    protected void initNavigation() {
        super.initNavigation();
        InputMap inputMap= getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = getActionMap();

        // action on Tab
        Object binding = inputMap.get(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0));
        actionMap.put(binding, new CellRouteHorizontalAction(this, true));

        // action on Shift + Tab
        binding = inputMap.get(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, KeyEvent.SHIFT_DOWN_MASK));
        actionMap.put(binding, new CellRouteHorizontalAction(this, false));

        // action on Enter
        binding = inputMap.get(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
        actionMap.put(binding, new CellRouteVerticalAction(this, true));

        // action on Shift + Enter
        binding = inputMap.get(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.SHIFT_DOWN_MASK));
        actionMap.put(binding, new CellRouteVerticalAction(this, false));

    }
}
