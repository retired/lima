/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.entrybook;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.exceptions.AlreadyExistEntryBookException;
import org.chorem.lima.business.exceptions.UsedEntryBookException;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookImpl;
import org.chorem.lima.enums.EntryBooksChartEnum;
import org.chorem.lima.enums.ImportExportEnum;
import org.chorem.lima.enums.MainUiRefreshComponent;
import org.chorem.lima.ui.MainView;
import org.chorem.lima.ui.importexport.ImportExport;
import org.chorem.lima.util.ErrorHelper;
import org.jdesktop.swingx.JXTable;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for entry book view.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class EntryBookViewHandler implements ServiceListener {

    private static final Log log = LogFactory.getLog(EntryBookViewHandler.class);

    protected EntryBookView view;

    protected EntryBookService entryBookService;

    protected ErrorHelper errorHelper;

    public EntryBookViewHandler(EntryBookView view) {
        this.view = view;
        entryBookService = LimaServiceFactory.getService(EntryBookService.class);
        LimaServiceFactory.addServiceListener(ImportService.class, this);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
    }

    public void init() {

        InputMap inputMap = view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();

        // add action on Ctrl + N
        String binding = "new-entryBook";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -2195732334248213712L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addEntryBook();
            }
        });

        // add action on Delete
        binding = "remove-entryBook";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -4932713296274387513L;

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteEntryBook();
            }
        });

        // add action on Ctrl + M
        binding = "modify-entryBook";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -761841876399286500L;

            @Override
            public void actionPerformed(ActionEvent e) {
                updateEntryBook();
            }
        });

        // add action on Ctrl + I
        binding = "import-entryBook";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -8953401784332356894L;

            @Override
            public void actionPerformed(ActionEvent e) {
                importEntryBooks();
            }
        });

        EntryBookTable table = view.getEntryBooksTable();

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 ) {
                    updateEntryBook();
                }
            }
        });

        loadAllEntryBooks();
    }

    /**
     * Load all available entry book into table.
     */
    protected void loadAllEntryBooks() {

        List<EntryBook> entryBooks = entryBookService.getAllEntryBooks();
        if (log.isDebugEnabled()) {
            log.debug(String.format("Loaded %d entry books from service", entryBooks.size()));
        }

        EntryBookTableModel model = view.getEntryBookTableModel();
        model.setValues(entryBooks);
    }

    /**
     * Add new entry book (display blocking dialog and perform add).
     */
    public void addEntryBook() {

        EntryBook newEntryBook = new EntryBookImpl();
        final EntryBookForm entryBookForm = new EntryBookForm(view);

        InputMap inputMap = entryBookForm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = entryBookForm.getRootPane().getActionMap();
        String binding = "dispose";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -8206425963136985592L;

            @Override
            public void actionPerformed(ActionEvent e) {
                entryBookForm.performCancel();
            }
        });

        entryBookForm.setEntryBook(newEntryBook);
        entryBookForm.setLocationRelativeTo(view);
        entryBookForm.setVisible(true);

        newEntryBook = entryBookForm.getEntryBook();
        // null == cancel action
        if (newEntryBook != null) {

            // service call
            try {
                newEntryBook = entryBookService.createEntryBook(newEntryBook);

                MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ENTRY_BOOK_PANE);

            } catch (AlreadyExistEntryBookException e) {
                errorHelper.showErrorMessage(t("lima.entryBook.alreadyExistEntryBook", e.getEntryBook().getCode()));
            }

            // ui refresh
            EntryBookTableModel model = view.getEntryBookTableModel();
            model.addValue(newEntryBook);
        }
    }

    /**
     * Update existing entry book.
     */
    public void updateEntryBook() {

        JXTable entryBookTable = view.getEntryBooksTable();
        EntryBookTableModel entryBookTableModel = view.getEntryBookTableModel();
        int selectedRow = entryBookTable.getSelectedRow();

        if (selectedRow >= 0) {
            EntryBook selectedEntryBook = entryBookTableModel.get(selectedRow);
            final EntryBookForm entryBookForm = new EntryBookForm(view);

            InputMap inputMap = entryBookForm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            ActionMap actionMap = entryBookForm.getRootPane().getActionMap();
            String binding = "dispose";
            inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
            actionMap.put(binding, new AbstractAction() {
                private static final long serialVersionUID = 4869740774851185380L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    entryBookForm.performCancel();
                }
            });

            entryBookForm.setAddState(false);
            entryBookForm.setEntryBook(selectedEntryBook);
            entryBookForm.setLocationRelativeTo(view);
            entryBookForm.setVisible(true);

            // null == cancel action
            selectedEntryBook = entryBookForm.getEntryBook();
            if (selectedEntryBook != null) {
                // service call
                selectedEntryBook = entryBookService.updateEntryBook(selectedEntryBook);

                // ui refresh
                entryBookTableModel.updateEntryBook(selectedEntryBook);

                MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ENTRY_BOOK_PANE);

            }
        }
    }

    /**
     * Delete selected entry book.
     */
    public void deleteEntryBook() {
        JXTable entryBookTable = view.getEntryBooksTable();
        int selectedRow = entryBookTable.getSelectedRow();

        if (selectedRow >= 0) {
            EntryBookTableModel entryBookTableModel = view.getEntryBookTableModel();

            EntryBook selectedEntryBook = entryBookTableModel.get(selectedRow);

            int response = JOptionPane.showConfirmDialog(view,
                t("lima.entryBook.remove.confirm", selectedEntryBook.getLabel()),
                t("lima.entryBook.remove.title"), JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

            if (response == JOptionPane.YES_OPTION) {
                try {
                    entryBookService.removeEntryBook(selectedEntryBook);
                    entryBookTableModel.remove(selectedEntryBook);

                    MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                    mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ENTRY_BOOK_PANE);

                } catch (UsedEntryBookException e) {
                    errorHelper.showErrorMessage(t("lima.entryBook.delete.used.error", e.getEntryBook().getCode(), e.getEntryBook().getLabel()));
                }
            }
        }
    }

    /**
     * Import entry book.
     * 
     * UI will be refreshed by {@link #notifyMethod(String, String)} called
     * by service listener.
     */
    public void importEntryBooks() {

        final EntryBookImportForm form = new EntryBookImportForm(view);

        InputMap inputMap = form.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = form.getRootPane().getActionMap();
        String binding = "dispose";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -1659538823979223871L;

            @Override
            public void actionPerformed(ActionEvent e) {
                form.performCancel();
            }
        });

        form.setLocationRelativeTo(view);
        form.setVisible(true);

        Object value = form.getButtonGroup().getSelectedValue();
        EntryBooksChartEnum entryBooksChartEnum = (EntryBooksChartEnum) value;
        // if action confirmed
        if (entryBooksChartEnum != null) {
            ImportExport importExport = new ImportExport(view);
            switch (entryBooksChartEnum) {
                case IMPORT_CSV:
                    importExport.importExport(ImportExportEnum.CSV_ENTRYBOOKS_IMPORT,
                            null, EntryBooksChartEnum.IMPORT_CSV.getDefaultFileURL(), true);
                    break;
                case IMPORT_EBP:
                    importExport.importExport(ImportExportEnum.EBP_ENTRYBOOKS_IMPORT,
                            null, EntryBooksChartEnum.IMPORT_EBP.getDefaultFileURL(), true);
                    break;

                default:
                    importExport.importExport(ImportExportEnum.CSV_ENTRYBOOKS_IMPORT,
                            null, entryBooksChartEnum.getDefaultFileURL(), true);
                    break;
            }
            loadAllEntryBooks();

            MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
            mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ENTRY_BOOK_PANE);

        }
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {

        //refresh on import datas
        if (methodName.contains("importEntryBooks")
            || methodName.contains("importAll")
            || methodName.contains("importAs")) {
            loadAllEntryBooks();
        }
    }
}
