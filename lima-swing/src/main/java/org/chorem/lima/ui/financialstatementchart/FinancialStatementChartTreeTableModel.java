/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialstatementchart;

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.FinancialStatementService;
import org.chorem.lima.business.exceptions.AlreadyExistFinancialStatementException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.entity.FinancialStatement;
import org.chorem.lima.entity.FinancialStatementImpl;
import org.chorem.lima.util.ErrorHelper;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.swing.tree.TreePath;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Tree table model for account edition.
 *
 * @author ore
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class FinancialStatementChartTreeTableModel extends AbstractTreeTableModel {

    /** log. */
    private static final Log log = LogFactory.getLog(FinancialStatementChartViewHandler.class);

    /** Services. */
    protected final FinancialStatementService financialStatementService;

    Binder<FinancialStatement, FinancialStatement> binderFinancialStatement;

    protected ErrorHelper errorHelper;

    /** Model constructor. Init account service used here. */
    public FinancialStatementChartTreeTableModel() {

        //create root for the tree
        super(new FinancialStatementImpl());

        // Gets factory service
        financialStatementService =
                LimaServiceFactory.getService(FinancialStatementService.class);

        FinancialStatement master = (FinancialStatement) root;
        master.addAllSubFinancialStatements(financialStatementService.getRootFinancialStatements());

        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());

        binderFinancialStatement = BinderFactory.newBinder(FinancialStatement.class);

    }


    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        String res = null;
        switch (column) {
            case 0:
                res = t("lima.table.label");
                break;
            case 1:
                res = t("lima.table.debitCredit");
                break;
            case 2:
                res = t("lima.table.debit");
                break;
            case 3:
                res = t("lima.table.credit");
                break;
            case 4:
                res = t("lima.table.provisionDeprecation");
                break;
        }
        return res;
    }

    @Override
    public int getChildCount(Object node) {
        FinancialStatement parentFinancialStatementHeader =
                    (FinancialStatement) node;
        int result = parentFinancialStatementHeader.sizeSubFinancialStatements();
        return result;
    }

    @Override
    public Object getChild(Object parent, int index) {
        FinancialStatement parentFinancialStatement =
            (FinancialStatement) parent;

        List<FinancialStatement> financialStatements =
                Lists.newArrayList(parentFinancialStatement.getSubFinancialStatements());

        FinancialStatement result = financialStatements.get(index);
        return result;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        FinancialStatement parentFinancialStatement =
                (FinancialStatement) parent;

        List<FinancialStatement> financialStatements =
            Lists.newArrayList(parentFinancialStatement.getSubFinancialStatements());

        int result = financialStatements.indexOf(child);
        return result;
    }

    @Override
    public Object getValueAt(Object node, int column) {
        Object result = "n/a";
        FinancialStatement financialStatement = (FinancialStatement) node;
        switch (column) {
            case 0:
                result = financialStatement.getLabel();
                break;
            case 1:
                result = financialStatement.getAccounts();
                break;
            case 2:
                result = financialStatement.getDebitAccounts();
                break;
            case 3:
                result = financialStatement.getCreditAccounts();
                break;
            case 4:
                result = financialStatement.getProvisionDeprecationAccounts();
                break;
        }
        return result;
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        return false;
    }

    @Override
    public boolean isLeaf(Object node) {
        FinancialStatement financialStatement = (FinancialStatement) node;
        return financialStatement != getRoot() && !financialStatement.isHeader();
    }


    /** Refresh FinancialStatementChart. */
    public void refreshTree() {

        FinancialStatement master = (FinancialStatement) root;
        master.setSubFinancialStatements(financialStatementService.getRootFinancialStatements());

        modelSupport.fireNewRoot();
    }


    /**
     * Add FinancialStatement(path can be null).
     *
     * @param path
     * @param financialStatement
     */
    public void addFinancialStatement(TreePath path, FinancialStatement financialStatement) {
        // Calling account service
        FinancialStatement parentFinancialStatementHeader =
                (FinancialStatement) path.getLastPathComponent();

        try {

            if (parentFinancialStatementHeader == getRoot()) {
                financialStatement = financialStatementService.createMasterFinacialStatements(
                        financialStatement);
                parentFinancialStatementHeader.addSubFinancialStatements(financialStatement);
            } else {
                financialStatement = financialStatementService.createFinancialStatement(
                        parentFinancialStatementHeader, financialStatement);
                binderFinancialStatement.copy(financialStatement.getMasterFinancialStatement(), parentFinancialStatementHeader);
            }

            modelSupport.fireTreeStructureChanged(path);


        } catch (AlreadyExistFinancialStatementException alreadyExistFinancialStatement) {
            errorHelper.showErrorMessage(t("lima.financialStatement.error.alreadyExistFinancialStatement",
                    alreadyExistFinancialStatement.getFinancialStatementLabel(), alreadyExistFinancialStatement.getMasterLabel()));
        } catch (NotAllowedLabelException notAllowedLabel) {
            errorHelper.showErrorMessage(t("lima.error.notAllowedLabel",
                    notAllowedLabel.getLabel()));
        }
    }


    /**
     * Update financialStatement
     *
     * @param path
     * @param financialStatement
     */
    public void updateFinancialStatement(TreePath path, FinancialStatement financialStatement) {

        FinancialStatement updateFinancialStatement = financialStatementService.updateFinancialStatement(financialStatement);
        binderFinancialStatement.copy(updateFinancialStatement, financialStatement);
        modelSupport.firePathChanged(path);
    }


    /**
     * Remove financialStatement
     *
     * @param path
     * @param financialStatement
     */
    public void removeFinancialStatementObject(TreePath path, FinancialStatement financialStatement) {
        // Calling account service
        int index = getIndexOfChild(
                path.getParentPath().getLastPathComponent(), financialStatement);
        financialStatementService.removeFinancialStatement(financialStatement);
        modelSupport.fireChildRemoved(path.getParentPath(), index, financialStatement);
    }
}
