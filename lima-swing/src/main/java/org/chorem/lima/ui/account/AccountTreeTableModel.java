/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.account;

import org.chorem.lima.entity.Account;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;

/**
 * Account tree table model redefining get value at for column.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class AccountTreeTableModel extends DefaultTreeTableModel {

    /**
     * Creates a new {@code DefaultTreeTableModel} with the specified
     * {@code root}.
     * 
     * @param root the root node of the tree
     */
    public AccountTreeTableModel(TreeTableNode root) {
        super(root);
    }

    @Override
    public Object getValueAt(Object node, int column) {
        MutableTreeTableNode treenode = (MutableTreeTableNode)node;
        Account account = (Account)treenode.getUserObject();
        
        Object result = null;
        if (account != null && column == 1) {
            result = account.getLabel();
        }
        
        return result;
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        return false;
    }
}
