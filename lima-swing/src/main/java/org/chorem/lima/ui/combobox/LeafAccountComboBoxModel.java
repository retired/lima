/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.combobox;

import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.utils.AccountComparator;
import org.chorem.lima.entity.Account;

import javax.swing.*;
import java.util.List;

/**
 * Account combo box model containing only leaf account (without sub accounts).
 */
public class LeafAccountComboBoxModel extends AbstractListModel implements ComboBoxModel, ServiceListener {

    private static final long serialVersionUID = 1L;

    protected Object selectedAccount;

    protected List<Account> datasCache;

    protected AccountService accountService;

    public LeafAccountComboBoxModel() {
        accountService =
                LimaServiceFactory.getService(AccountService.class);
        LimaServiceFactory.addServiceListener(AccountService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);
        datasCache = getDataList();
    }

    @Override
    public Object getSelectedItem() {
        return selectedAccount;
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedAccount = anItem;
        fireContentsChanged(this, -1, -1);
    }

    @Override
    public Object getElementAt(int index) {
        return datasCache.get(index);
    }

    @Override
    public int getSize() {
        return datasCache.size();
    }

    public List<Account> getDataList() {
        List<Account> result = accountService.getAllLeafAccounts();
        result.sort(new AccountComparator());
        return result;

    }

    public void refresh() {
        datasCache = getDataList();
        fireContentsChanged(this, 0, datasCache.size());
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        if (serviceName.contains("Account") ||
            methodName.contains("importAll")) {
            refresh();
        }
    }

}
