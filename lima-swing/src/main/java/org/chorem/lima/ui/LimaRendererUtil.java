/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.ui;

import jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import org.chorem.lima.LimaSwingApplicationContext;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorProvider;

import javax.swing.ListCellRenderer;

/**
 * Helper class to deal with renderers.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5.2
 */
public class LimaRendererUtil {

    public static ListCellRenderer newDecoratorListCellRenderer(Class<?> type) {

        DecoratorProvider decoratorProvider = LimaSwingApplicationContext.get().getDecoratorProvider();
        Decorator<?> decorator = decoratorProvider.getDecoratorByType(type);
        return new DecoratorListCellRenderer(decorator);
    }

}
