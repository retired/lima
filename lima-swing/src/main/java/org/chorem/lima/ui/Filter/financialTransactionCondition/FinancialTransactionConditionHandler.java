package org.chorem.lima.ui.Filter.financialTransactionCondition;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.beans.Condition;
import org.chorem.lima.beans.FinancialTransactionCondition;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.financialtransactionsearch.FinancialTransactionSearchViewHandler;

import javax.swing.*;
import java.beans.PropertyVetoException;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FinancialTransactionConditionHandler {

    /** log. */
    private static final Log log =
            LogFactory.getLog(FinancialTransactionSearchViewHandler.class);

    protected FinancialTransactionConditionView view;

    protected FinancialTransactionCondition filter;

    public FinancialTransactionConditionHandler(FinancialTransactionConditionView view) {
        this.view = view;
        this.filter = new FinancialTransactionCondition();
    }

    public void setAllConditions(boolean all) {
        filter.setAllConditions(all);
    }

    public boolean isAllConditions() {
        return filter.isAllConditions();
    }

    public FinancialTransactionCondition getFilter() {
        return filter;
    }

    public void addCondition(ConditionHandler conditionHandler) {
        // add model
        List<Condition> conditions = filter.getConditions();
        Condition condition = conditionHandler.getCondition();
        boolean add = conditions.add(condition);

        // add view
        if (add) {
            JPanel conditionPanel = view.getConditionPanel();
            JInternalFrame conditionView = conditionHandler.getView();
            conditionPanel.add(conditionView);
            try {
                conditionView.setSelected(true);
            } catch (PropertyVetoException e) {
                log.error(e);
            }
            conditionPanel.validate();
            conditionPanel.repaint();
        }
        conditionHandler.setFilterHandler(this);
    }

    public void removeCondition(ConditionHandler conditionHandler) {
        // remove model
        List<Condition> conditions = filter.getConditions();
        Condition condition = conditionHandler.getCondition();
        boolean remove = conditions.remove(condition);

        // remove view
        if (remove) {
            JPanel conditionPanel = view.getConditionPanel();
            JComponent conditionView = conditionHandler.getView();
            conditionPanel.remove(conditionView);
            view.getParent().validate();
            view.getParent().repaint();
        }
    }
}
