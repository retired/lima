/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.ui.accountViewer;

import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FiscalPeriod;

import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author sletellier <letellier@codelutin.com>
 */
public class AccountViewerEditModel implements Serializable {

    public static final String PROPERTY_DEBIT = "debit";
    public static final String PROPERTY_CREDIT = "credit";
    public static final String PROPERTY_SOLD = "sold";

    protected PeriodChoice periodChoice = PeriodChoice.FINANCIAL_PERIOD;
    protected FinancialPeriod financialPeriod;
    protected FiscalPeriod fiscalPeriod;
    protected Account account;
    protected Date dateStart;
    protected Date dateEnd;

    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    protected AccountViewerTableModel model;

    protected BigDecimal debit = BigDecimal.ZERO;
    protected BigDecimal credit = BigDecimal.ZERO;
    protected BigDecimal sold = BigDecimal.ZERO;

    public BigDecimal getDebit() {
        return debit;
    }

    public void setDebit(BigDecimal debit) {
        BigDecimal oldDebit = getDebit();

        if (!BigDecimal.ZERO.equals(debit)){
            this.debit = debit.add(oldDebit);
        }else{
            this.debit = BigDecimal.ZERO;
        }

        firePropertyChange(PROPERTY_DEBIT, oldDebit, this.debit);
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        BigDecimal oldCredit = getCredit();

        if (!BigDecimal.ZERO.equals(credit)){
            this.credit = credit.add(oldCredit);
        }else{
            this.credit=BigDecimal.ZERO;
        }

        firePropertyChange(PROPERTY_CREDIT, oldCredit, this.credit);
    }

    public BigDecimal getSold() {
        return sold;
    }

    public void setSold(BigDecimal sold, boolean credit) {
        BigDecimal oldSold = getSold();

        if (!BigDecimal.ZERO.equals(sold)){
            if (credit){
                this.sold = oldSold.subtract(sold);
            }else{
                this.sold = oldSold.add(sold);
            }
        }else{
            this.sold =BigDecimal.ZERO;
        }

        firePropertyChange(PROPERTY_SOLD, oldSold, this.sold);
    }


    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    public PeriodChoice getPeriodChoice() {
        return periodChoice;
    }

    public void setPeriodChoice(PeriodChoice periodChoice) {
        this.periodChoice = periodChoice;
    }

    public FinancialPeriod getFinancialPeriod() {
        return financialPeriod;
    }

    public void setFinancialPeriod(FinancialPeriod financialPeriod) {
        this.financialPeriod = financialPeriod;
    }

    public FiscalPeriod getFiscalPeriod() {
        return fiscalPeriod;
    }

    public void setFiscalPeriod(FiscalPeriod fiscalPeriod) {
        this.fiscalPeriod = fiscalPeriod;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }
}

