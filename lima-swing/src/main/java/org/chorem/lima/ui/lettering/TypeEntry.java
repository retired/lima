package org.chorem.lima.ui.lettering;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public enum  TypeEntry {

    ALL(true, true),

    LETTERED(true, false),

    NO_LETTERED(false, true);

    private boolean lettered;

    private boolean noLettered;

    TypeEntry(boolean lettered, boolean noLettered) {
        this.lettered = lettered;
        this.noLettered = noLettered;
    }


    @Override
    public String toString() {

        String result = "";

        switch (this) {
            case LETTERED:
                result = t("lima.lettering.checkLettredEntry");
                break;
            case NO_LETTERED:
                result = t("lima.lettering.checkNoLettredEntry");
                break;
            case ALL:
                result = t("lima.lettering.checkAll");
                break;
        }

        return  result;

    }

    public boolean isLettered() {
        return lettered;
    }

    public boolean isNoLettered() {
        return noLettered;
    }
}
