/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.opening;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodImpl;
import org.chorem.lima.enums.AccountsChartEnum;
import org.chorem.lima.enums.EntryBooksChartEnum;
import org.chorem.lima.enums.FinancialStatementsChartEnum;
import org.chorem.lima.enums.ImportExportEnum;
import org.chorem.lima.enums.VatStatementsChartEnum;
import org.chorem.lima.ui.importexport.ImportExport;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;

import static org.nuiton.i18n.I18n.t;

public class OpeningViewHandler {

    private static final Log log = LogFactory.getLog(OpeningViewHandler.class);

    private static Border noBorder = new EmptyBorder(0, 0, 0, 0);

    private static Color green = new Color(0x66, 0xcc, 0x00);

    protected int step;

    protected OpeningView view;


    protected BackupPanel backupPanel;
    //Panels
    protected CreateAccountsPanel caPanel;

    protected CreateIdentityPanel idPanel;

    protected CreateEntryBookPanel ebPanel;

    protected CreateFiscalPeriodPanel fsPanel;

    public OpeningViewHandler(OpeningView view) {
        this.view = view;
        backupPanel = new BackupPanel();
        idPanel = new CreateIdentityPanel();
        caPanel = new CreateAccountsPanel();
        ebPanel = new CreateEntryBookPanel();
        fsPanel = new CreateFiscalPeriodPanel();
    }

    public void init() {
        step = 0;
        next();
    }

    public void previous() {
        JPanel panel = view.getPanel();
        panel.removeAll();
        step = step - 2;
        switch (step) {
            case 0:
                view.getPrevious().setEnabled(false);
                view.getIdentityIcon().setBorder(noBorder);
                view.getBackupIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                panel.add(backupPanel);
                panel.validate();
                view.repaint();
                step++;
                break;
            case 1:
                //refresh UI
                view.getAccountsIcon().setBorder(noBorder);
                view.getIdentityIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                panel.add(idPanel);
                panel.validate();
                view.repaint();
                step++;
                break;
            case 2:
                //refresh UI
                // remove all accounts
                view.getEntrybooksIcon().setBorder(noBorder);
                view.getAccountsIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                caPanel.getButtonGroup().setSelectedValue(null);
                panel.add(caPanel);
                panel.validate();
                view.repaint();
                step++;
                break;
            case 3:
                view.getNext().setText(t("lima.opening.next"));
                // remove all entry books
                view.getFiscalperiodsIcon().setBorder(noBorder);
                view.getEntrybooksIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                panel.add(ebPanel);
                panel.validate();
                view.repaint();
                step++;
                break;
            default:
                break;
        }

    }

    public void next() {
        ImportExport importExport = new ImportExport(view);
        JPanel panel = view.getPanel();
        panel.removeAll();
        switch (step) {
            case 0:
                view.getPrevious().setEnabled(false);
                //refresh UI
                view.getBackupIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                panel.add(backupPanel);
                panel.validate();
                view.repaint();
                step++;
                break;
            case 1:
                // backup was done, exit first start config
                if (backupPanel.getDone()) {
                    view.dispose();
                    break;
                }
                view.getPrevious().setEnabled(true);
                //refresh UI
                view.getBackupIcon().setBorder(noBorder);
                view.getIdentityIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                panel.add(idPanel);
                panel.validate();
                view.repaint();
                step++;
                break;
            case 2:
                idPanel.getHandler().updateIdentity();
                //refresh UI
                view.getIdentityIcon().setBorder(noBorder);
                view.getAccountsIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                panel.add(caPanel);
                panel.validate();
                view.repaint();
                step++;
                break;

            case 3:
                importAccountabilityLayouts(importExport);

                view.getAccountsIcon().setBorder(noBorder);
                view.getEntrybooksIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                panel.add(ebPanel);
                panel.validate();
                view.repaint();
                step++;
                break;

            case 4:
                importEntryBookLayouts(importExport);

                view.getEntrybooksIcon().setBorder(noBorder);
                view.getFiscalperiodsIcon().setBorder(BorderFactory.createLineBorder(green, 2));
                panel.add(fsPanel);
                panel.validate();
                view.getNext().setText(t("lima.opening.end"));
                view.repaint();
                step++;
                break;
            case 5:
                try {
                    FiscalPeriod fiscalPeriod = new FiscalPeriodImpl();
                    fiscalPeriod.setBeginDate(fsPanel.getBeginDatePicker().getDate());
                    fiscalPeriod.setEndDate(fsPanel.getEndDatePicker().getDate());

                    LimaServiceFactory.getService(
                            FiscalPeriodService.class).
                            createFiscalPeriod(fiscalPeriod);

                } catch (LimaException ex) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't create fiscal period", ex);
                    }
                }
                view.dispose();
                break;
            default:
                view.dispose();
                break;
        }
    }

    private void importEntryBookLayouts(ImportExport importExport) {

        Object value = ebPanel.getButtonGroup().getSelectedValue();
        // if action confirmed
        if (value != null) {
            // reset previous imported values
            importExport.resetInitialImportedEntryBook();

            EntryBooksChartEnum entryBooksChartEnum = (EntryBooksChartEnum) value;

            switch (entryBooksChartEnum) {
                case IMPORT_CSV:
                    importExport.importExport(ImportExportEnum.CSV_ENTRYBOOKS_IMPORT,
                            null, EntryBooksChartEnum.IMPORT_CSV.getDefaultFileURL(), false);
                    break;
                case IMPORT_EBP:
                    importExport.importExport(ImportExportEnum.EBP_ENTRYBOOKS_IMPORT,
                            null, EntryBooksChartEnum.IMPORT_EBP.getDefaultFileURL(), false);
                    break;

                default:
                    importExport.importExport(ImportExportEnum.CSV_ENTRYBOOKS_IMPORT,
                            null, entryBooksChartEnum.getDefaultFileURL(), false);
                    break;
            }
        }
    }

    protected void importAccountabilityLayouts(ImportExport importExport) {
        Object value = caPanel.getButtonGroup().getSelectedValue();
        // if action confirmed
        if (value != null) {
            // reset previous imported values
            importExport.resetInitialAccountabilityLayoutsImport();

            AccountsChartEnum defaultAccountsChartEnum = (AccountsChartEnum) value;
            //Import accounts chart
            switch (defaultAccountsChartEnum) {
                case IMPORT_EBP:
                    importExport.importExport(ImportExportEnum.EBP_ACCOUNTCHARTS_IMPORT,
                            null, defaultAccountsChartEnum.getDefaultFileURL(), false);
                    break;

                default:
                    importExport.importExport(ImportExportEnum.CSV_ACCOUNTCHARTS_IMPORT,
                            null, defaultAccountsChartEnum.getDefaultFileURL(), false);
                    break;
            }
            //Import financialstatement
            switch (defaultAccountsChartEnum) {
                case SHORTENED:
                    importExport.importExport(ImportExportEnum.CSV_FINANCIALSTATEMENTS_IMPORT,
                            null, defaultAccountsChartEnum.getDefaultFileURL(), false);
                    break;
                case DEVELOPED:
                    importExport.importExport(ImportExportEnum.CSV_FINANCIALSTATEMENTS_IMPORT,
                            null, FinancialStatementsChartEnum.DEVELOPED.getDefaultFileUrl() , false);
                    break;
                default:
                    importExport.importExport(ImportExportEnum.CSV_FINANCIALSTATEMENTS_IMPORT,
                            null, FinancialStatementsChartEnum.BASE.getDefaultFileUrl(), false);
                    break;
            }
            //Import vatstatement
            importExport.importExport(ImportExportEnum.CSV_VAT_IMPORT,
                    null, VatStatementsChartEnum.DEFAULT.getDefaultFileUrl(), false);
        }
    }
}
