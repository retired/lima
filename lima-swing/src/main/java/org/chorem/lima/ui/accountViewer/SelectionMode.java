package org.chorem.lima.ui.accountViewer;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 15/09/15.
 */
public enum SelectionMode {

    LETTERED, MANUAL;

    @Override
    public String toString() {

        String result = "";

        switch (this) {
            case MANUAL:
                result = t("lima.filter.condition.selectionMode.manual");
                break;
            case LETTERED:
                result = t("lima.filter.condition.selectionMode.lettered");
                break;
        }

        return  result;

    }
}
