/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.lettering;

import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.AbstractLimaTableModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Basic transaction table model.
 * <p/>
 *
 * @author ore
 * @author chatellier
 */
public class LetteringTableModel extends AbstractLimaTableModel<Entry> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;
    protected ImageIcon lockedImageIcon;
    protected List<Entry> unlockedEntries;

    @Override
    protected void initColumn() {
        addColumn(new AbstractColumn<LetteringTableModel>(ImageIcon.class, t("lima.table.locked"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                ImageIcon imageIcon = unlockedEntries.contains(entry) ? null: getLockedImageIcon();
                return imageIcon;
            }
        });

        addColumn(new AbstractColumn<LetteringTableModel>(Date.class, t("lima.table.date"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getFinancialTransaction().getTransactionDate();
            }
        });

        addColumn(new AbstractColumn<LetteringTableModel>(EntryBook.class, t("lima.table.entryBook"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getFinancialTransaction().getEntryBook();
            }
        });

        addColumn(new AbstractColumn<LetteringTableModel>(Account.class, t("lima.table.account"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getAccount();
            }
        });

        addColumn(new AbstractColumn<LetteringTableModel>(String.class, t("lima.table.voucher"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getVoucher();
            }
        });

        addColumn(new AbstractColumn<LetteringTableModel>(String.class, t("lima.table.description"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getDescription();
            }
        });

        addColumn(new AbstractColumn<LetteringTableModel>(String.class, t("lima.table.letter"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getLettering();
            }
        });

        addColumn(new AbstractColumn<LetteringTableModel>(BigDecimal.class, t("lima.table.debit"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.isDebit() ? entry.getAmount() : BigDecimal.ZERO;
            }
        });

        addColumn(new AbstractColumn<LetteringTableModel>(BigDecimal.class, t("lima.table.credit"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.isDebit() ? BigDecimal.ZERO : entry.getAmount();
            }
        });

    }

    public List<Entry> getUnlockedEntries() {
        return unlockedEntries;
    }

    public void setUnlockedEntries(List<Entry> unlockedEntries) {
        this.unlockedEntries = unlockedEntries;
    }

    protected ImageIcon getLockedImageIcon() {
        if (lockedImageIcon == null) {
            InputStream stream = LetteringTableModel.class.getResourceAsStream("/icons/action-financialPeriod-close.png");
            Image lockedIcon;
            try {
                lockedIcon = ImageIO.read(stream);
                lockedImageIcon = new ImageIcon(lockedIcon);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return lockedImageIcon;
    }
}
