/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.ui.celleditor;

import org.chorem.lima.util.BigDecimalToString;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;

public class BigDecimalTableCellRenderer extends DefaultLimaTableCellRenderer {

    private static final long serialVersionUID = -2499433026151065390L;

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel myCell = (JLabel) super.getTableCellRendererComponent(
                table, value, isSelected, hasFocus, row, column);
        myCell.setHorizontalAlignment(SwingConstants.RIGHT);

        return myCell;
    }

    public void setValue(Object aValue) {
        Object result = aValue;
        if (aValue != null && aValue instanceof BigDecimal) {
            result = BigDecimalToString.format((BigDecimal)result);
        }
        super.setValue(result);
    }

}
