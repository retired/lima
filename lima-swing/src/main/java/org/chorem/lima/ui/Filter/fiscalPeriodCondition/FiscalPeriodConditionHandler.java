package org.chorem.lima.ui.Filter.fiscalPeriodCondition;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.Condition;
import org.chorem.lima.beans.FiscalPeriodCondition;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;

import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import java.util.List;


/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FiscalPeriodConditionHandler implements ConditionHandler {

    protected FiscalPeriodConditionView view;

    protected FiscalPeriodCondition condition;

    protected FinancialTransactionConditionHandler filterHandler;

    public FiscalPeriodConditionHandler(FiscalPeriodConditionView view) {
        this.view = view;
        this.condition = new FiscalPeriodCondition();
    }

    @Override
    public Condition getCondition() {
        return condition;
    }

    @Override
    public FiscalPeriodConditionView getView() {
        return view;
    }

    @Override
    public void setFilterHandler(FinancialTransactionConditionHandler filterHandler) {
        this.filterHandler = filterHandler;
    }

    public Object[] getFiscalPeriodList() {
        FiscalPeriodService fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);
        List<FiscalPeriod> periods = fiscalPeriodService.getAllFiscalPeriods();
        Object[] result = periods.toArray();
        return result;
    }

    public void setFiscalPeriod(ListSelectionEvent event) {
        JList fiscalPeriodList = view.getFiscalPeriodList();
        List<FiscalPeriod> selectedValuesList = fiscalPeriodList.getSelectedValuesList();
        condition.setFiscalPeriods(selectedValuesList);
    }

    public void delete() {
        filterHandler.removeCondition(this);
    }
}
