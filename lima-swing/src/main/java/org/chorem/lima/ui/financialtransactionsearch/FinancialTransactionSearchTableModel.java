/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialtransactionsearch;

import org.chorem.lima.beans.FinancialTransactionCondition;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;
import org.chorem.lima.ui.financialtransaction.AccountColumn;
import org.chorem.lima.ui.financialtransaction.BalanceColumn;
import org.chorem.lima.ui.financialtransaction.CreditColumn;
import org.chorem.lima.ui.financialtransaction.DateColumn;
import org.chorem.lima.ui.financialtransaction.DebitColumn;
import org.chorem.lima.ui.financialtransaction.DescriptionColumn;
import org.chorem.lima.ui.financialtransaction.EntryBookColumn;
import org.chorem.lima.ui.financialtransaction.LetterColumn;
import org.chorem.lima.ui.financialtransaction.VoucherColumn;

import java.util.List;

/**
 * Basic transaction table model.
 * <p/>
 * Le modele est filtré sur {@link #selectedFiscalPeriod} (montée en charge !).
 *
 * @author ore
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class FinancialTransactionSearchTableModel extends FinancialTransactionTableModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Financial service */
    protected final FinancialPeriodService financialPeriodService;

    /** selected financial period */
    protected FiscalPeriod selectedFiscalPeriod;

    /** data cache */
    protected FinancialTransactionCondition financialTransactionCondition;

    /**
     * Model constructor.
     * <p/>
     * Just init service proxies.
     */
    public FinancialTransactionSearchTableModel() {
        super();
        financialPeriodService =
                LimaServiceFactory.getService(
                        FinancialPeriodService.class);
    }

    protected void refresh(FinancialTransactionCondition financialTransactionCondition) {
        this.financialTransactionCondition = financialTransactionCondition;
        List<FinancialTransaction> transactions = financialTransactionService.searchFinancialTransaction(this.financialTransactionCondition);
        setTransactions(transactions);
    }

    @Override
    protected void initColumn() {
        addColumn(new DateColumn());
        addColumn(new EntryBookColumn());
        addColumn(new VoucherColumn());
        addColumn(new AccountColumn());
        addColumn(new DescriptionColumn());
        addColumn(new LetterColumn());
        addColumn(new DebitColumn());
        addColumn(new CreditColumn());
        addColumn(new BalanceColumn());
    }

    public void setFiscalPeriod(FiscalPeriod fiscalPeriod) {
        selectedFiscalPeriod = fiscalPeriod;
    }
    
    public void notifyMethod(String serviceName, String methodeName) {
        super.notifyMethod(serviceName, methodeName);
        if ((serviceName.contains("FinancialTransaction") || methodeName.contains("importEntries")
             || methodeName.contains("importAll")) && !methodeName.contains("search")) {
            //on recharge la liste
            refresh(financialTransactionCondition);
        }
    }

    public void setTransactions(List<FinancialTransaction> transactions) {
        clear();
        for (FinancialTransaction transaction : transactions) {
            addAll(transaction.getEntry());
        }
    }

}
