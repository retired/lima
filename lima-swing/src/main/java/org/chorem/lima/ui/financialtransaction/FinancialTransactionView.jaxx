<!--
  #%L
  Lima :: Swing
  %%
  Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel layout="{new BorderLayout()}">

  <import>
    org.chorem.lima.entity.EntryBook
    org.chorem.lima.entity.FinancialPeriod
    org.chorem.lima.entity.FiscalPeriod
    org.chorem.lima.ui.common.EntryBookListRenderer
    org.chorem.lima.ui.common.FinancialTransactionTableModel
    java.awt.event.KeyEvent
    javax.swing.BorderFactory
    javax.swing.DefaultListSelectionModel
    javax.swing.KeyStroke
    javax.swing.ListSelectionModel
  </import>

  <FinancialTransactionViewHandler id="handler" constructorParams="this"/>

  <ListSelectionModel
      id='selectionModel'
      initializer="new DefaultListSelectionModel()"
      selectionMode='{ListSelectionModel.SINGLE_SELECTION}'
      onValueChanged="handler.selectionChanged()"/>

  <Boolean id="selectedRow" javaBean="false"/>
  <Boolean id="addEntryAllowed" javaBean="true"/>
  <Boolean id="transactionInClipBoard" javaBean="false"/>
  <Boolean id="entryInClipBoard" javaBean="false"/>
  <Boolean id="balance" javaBean="true"/>
  <Boolean id="assignableInAllEntries" javaBean="false"/>

  <script>
    <![CDATA[
    void $afterCompleteSetup() {
        handler.init();
    }
    ]]>
  </script>
  <JToolBar id="toolbar"
            constraints="BorderLayout.PAGE_START">

    <JButton id="addTransaction"
             onActionPerformed="handler.addFinancialTransaction()" />

    <JButton id="removeTransaction"
             onActionPerformed="handler.deleteSelectedTransaction()" />

    <JButton id="copyTransaction"
             onActionPerformed="handler.copyTransaction()" />

    <JButton id="pastTransaction"
             onActionPerformed="handler.pasteTransaction()" />

    <JToolBar.Separator/>

    <JButton id="addEntry"
             onActionPerformed="handler.addEntry()" />

    <JButton id="removeEntry"
             onActionPerformed="handler.deleteSelectedEntry()" />

    <JButton id="copyEntry"
             onActionPerformed="handler.copyEntry()" />

    <JButton id="pasteEntry"
             onActionPerformed="handler.pasteEntry()" />

    <JButton id="assignEntries"
             onActionPerformed="handler.assignAllEntries()" />

    <JToolBar.Separator/>

    <JButton id="balanceButton"
             onActionPerformed="handler.balanceTransaction()" />

    <JToolBar.Separator/>

    <JLabel id="fiscalPeriodLabel"/>

    <org.chorem.lima.ui.common.FiscalPeriodComboBoxModel id="fiscalPeriodComboBoxModel"/>

    <JComboBox id="fiscalPeriodComboBox"
               model="{fiscalPeriodComboBoxModel}"
               onItemStateChanged="handler.fiscalPeriodSelected(event)"/>

    <JLabel id="financialPeriodLabel"/>

    <org.chorem.lima.ui.common.FinancialPeriodComboBoxModel id="financialPeriodComboBoxModel"/>

    <JComboBox id="financialPeriodComboBox"
               model="{financialPeriodComboBoxModel}"
               onItemStateChanged="handler.financialPeriodSelected(event)"/>

    <JButton id="back"
             onActionPerformed="handler.back(financialPeriodComboBox)"/>

    <JButton id="next"
             onActionPerformed="handler.next(financialPeriodComboBox)"/>

    <JToolBar.Separator/>

    <JLabel id="entryBookLabel" />

    <org.chorem.lima.ui.common.EntryBookComboBoxModel id="entryBookComboBoxModel"/>

    <JComboBox id="entryBookComboBox"
               model="{entryBookComboBoxModel}"
               onItemStateChanged="handler.entryBookSelected(event)"/>
  </JToolBar>

  <JScrollPane constraints="BorderLayout.CENTER">
    <FinancialTransactionTableModel id="financialTransactionTableModel"/>

    <FinancialTransactionTable id="financialTransactionTable"
                               constructorParams='handler'
                               selectionModel='{selectionModel}'
                               model='{financialTransactionTableModel}'/>
  </JScrollPane>

  <JPanel constraints="BorderLayout.SOUTH" layout="{new BorderLayout()}">
    <JLabel id='periodStatusLabel' constraints="BorderLayout.WEST"/>
    <JLabel id='balanceStatusLabel' constraints="BorderLayout.EAST"/>
  </JPanel>

</JPanel>
