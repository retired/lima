package org.chorem.lima.ui.combobox;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.utils.AccountComparator;
import org.chorem.lima.entity.Account;
import org.nuiton.decorator.DecoratorUtil;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class AccountComboBox extends BeanFilterableComboBox<Account>  implements ServiceListener {

    private static final Log log = LogFactory.getLog(AccountComboBox.class);

    protected AccountService accountService;

    protected boolean leafAccounts;

    public AccountComboBox() {
        initializeAccountComboBox();
    }

    public AccountComboBox(JAXXContext parentContext) {
        super(parentContext);
        initializeAccountComboBox();
    }

    private void initializeAccountComboBox() {
        leafAccounts = false;
        accountService = LimaServiceFactory.getService(AccountService.class);
        LimaServiceFactory.addServiceListener(AccountService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);
        init(DecoratorUtil.newMultiJXPathDecorator(Account.class, "${" + Account.PROPERTY_ACCOUNT_NUMBER + "}$s##${" + Account.PROPERTY_LABEL + "}$s", "##", " - "), getDataList());
    }

    public boolean isLeafAccounts() {
        return leafAccounts;
    }

    public void setLeafAccounts(boolean leafAccounts) {
        this.leafAccounts = leafAccounts;
    }

    public List<Account> getDataList() {
        List<Account> result = leafAccounts ? accountService.getAllLeafAccounts() : accountService.getAllAccounts();
        result.sort(new AccountComparator());
        return result;

    }

    public void refresh() {
        setData(getDataList());
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        if (serviceName.contains("Account") ||
                methodName.contains("importAll")) {
            refresh();
        }
    }

}
