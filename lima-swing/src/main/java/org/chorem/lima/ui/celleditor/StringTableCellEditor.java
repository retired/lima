package org.chorem.lima.ui.celleditor;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingConfig;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class StringTableCellEditor extends DefaultCellEditor {

    private static final Log log = LogFactory.getLog(BigDecimalTableCellEditor.class);

    public StringTableCellEditor() {
        super(new JTextField());
        setClickCountToStart(1);
        Border border = BorderFactory.createLineBorder(new Color(123, 165, 205), 2);

        getComponent().setBorder(border);
        getComponent().addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent event) {
                runEdition();
            }

            @Override
            public void focusLost(FocusEvent event) {
                // nothing to do
            }
        });
        getComponent().addAncestorListener(new AncestorListener() {

            @Override
            public void ancestorAdded(AncestorEvent event) {
                runEdition();
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                // nothing to do
            }

            @Override
            public void ancestorMoved(AncestorEvent event) {
                // nothing to do
            }
        });
        getComponent().addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                runEdition();
            }
        });

        // remove input Ctrl + Del
        InputMap inputMap= getComponent().getInputMap(JComponent.WHEN_FOCUSED);
        while (inputMap != null) {
            inputMap.remove(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, KeyEvent.CTRL_DOWN_MASK));
            inputMap = inputMap.getParent();
        }

    }

    @Override
    public JTextField getComponent() {
        return (JTextField) super.getComponent();
    }

    /**
     * Two kinds of edition, with LimaConfig value
     * */
    public void runEdition() {
        LimaSwingConfig config = LimaSwingConfig.getInstance();
        if (config.isSelectAllEditingCell()) {
            getComponent().selectAll();
        } else {
            int textFieldSize = getComponent().getText().length();
            getComponent().select(textFieldSize, textFieldSize);
        }
    }

    /**
     * Split decimal with scale config
     * @return bigDecimal value
     * */
    @Override
    public Object getCellEditorValue() {
        String stringValue = super.getCellEditorValue().toString();
        return stringValue;
    }

}
