package org.chorem.lima.ui.Filter.dateCondition;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.beans.Condition;
import org.chorem.lima.beans.DateCondition;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;

import java.awt.event.ItemEvent;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class DateConditionHandler implements ConditionHandler {

    protected DateConditionView view;

    protected DateCondition condition;

    protected FinancialTransactionConditionHandler filterHandler;

    public DateConditionHandler( DateConditionView view) {
        this.view = view;
        this.condition = new DateCondition();
    }

    public Date getDate() {
        return condition.getDate();
    }

    public void setDate(Date date) {
        condition.setDate(date);
    }

    public void setOperand(DateCondition.Operand operand) {
        condition.setOperand(operand);
    }

    public DateCondition.Operand getOperand() {
        return condition.getOperand();
    }

    public void delete() {
       filterHandler.removeCondition(this);
    }

    public void operandSelected(ItemEvent event) {
        condition.setOperand((DateCondition.Operand) event.getItem());
    }

    @Override
    public Condition getCondition() {
        return condition;
    }

    @Override
    public DateConditionView getView() {
        return view;
    }

    @Override
    public void setFilterHandler(FinancialTransactionConditionHandler filterHandler) {
        this.filterHandler = filterHandler;
    }

    protected DateFormat getDateFormat() {
        Locale locale = LimaSwingConfig.getInstance().getLocale();
        DateFormat result = DateFormat.getDateInstance(DateFormat.SHORT, locale);
        return result;
    }

    protected Locale getLocale() {
        Locale locale = LimaSwingConfig.getInstance().getLocale();
        return locale;
    }
}
