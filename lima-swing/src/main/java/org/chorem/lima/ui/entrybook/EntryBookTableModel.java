/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.entrybook;

import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.AbstractLimaTableModel;

import static org.nuiton.i18n.I18n.t;

/**
 * Entry book table model only rendering entry book list.
 *
 * @author ore
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class EntryBookTableModel extends AbstractLimaTableModel<EntryBook> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7578692417919755647L;

    @Override
    protected void initColumn() {
        addColumn(new AbstractColumn<EntryBookTableModel>(String.class, t("lima.entryBook.code"), false) {
            @Override
            public Object getValueAt(int row) {
                return tableModel.get(row).getCode();
            }
        });
        addColumn(new AbstractColumn<EntryBookTableModel>(String.class, t("lima.entryBook.label"), false) {
            @Override
            public Object getValueAt(int row) {
                return tableModel.get(row).getLabel();
            }
        });
    }

    public void updateEntryBook(EntryBook entryBook) {
        int row = values.indexOf(entryBook);
        fireTableRowsUpdated(row, row);
    }

}
