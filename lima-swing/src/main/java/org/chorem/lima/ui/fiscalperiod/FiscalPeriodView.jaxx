<!--
  #%L
  Lima :: Swing
  %%
  Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<JPanel layout="{new BorderLayout()}">

  <import>
    javax.swing.ListSelectionModel
    javax.swing.DefaultListSelectionModel
    static org.nuiton.i18n.I18n.t
  </import>

  <FiscalPeriodViewHandler id="handler" constructorParams="this"/>

  <Boolean id="blockEnabled" javaBean="false"/>
  <Boolean id="deleteEnabled" javaBean="false"/>

  <script><![CDATA[  
    void $afterCompleteSetup() {
        getHandler().init();
    }
  ]]></script>


  <JToolBar id="toolBar"
            constraints="BorderLayout.PAGE_START">

    <JButton id="addButton"
             onActionPerformed="handler.addFiscalPeriod()"/>

    <JButton id="updateButton"
             onActionPerformed="handler.updateFiscalPeriod()"/>

    <JButton id="blockButton"
             onActionPerformed="handler.blockFiscalPeriod()"/>

    <JButton id="removeButton"
             onActionPerformed="handler.deleteFiscalPeriod()"/>

  </JToolBar>
  <JScrollPane constraints="BorderLayout.CENTER">
    <FiscalPeriodTableModel id="fiscalPeriodTableModel" />
    <FiscalPeriodTable id="fiscalPeriodTable"
                       constructorParams='handler'
                       model="{fiscalPeriodTableModel}"
                       selectionModel='{selectionModel}'/>
    <ListSelectionModel id="selectionModel"
                        initializer="new DefaultListSelectionModel()"
                        onValueChanged="handler.onSelectionChanged(event)"/>

  </JScrollPane>
</JPanel>
