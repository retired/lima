package org.chorem.lima.ui.Filter;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.Condition;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;

import javax.swing.*;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public interface ConditionHandler {

    Condition getCondition();

    JInternalFrame getView();

    void setFilterHandler(FinancialTransactionConditionHandler filterHandler);
}
