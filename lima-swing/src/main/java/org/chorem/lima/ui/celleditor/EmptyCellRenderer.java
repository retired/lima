/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.ui.celleditor;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Renderer qui affiche la valeur de la cellule (toString()) en permettant de
 * conserver les cases rouges (entre invalides) meme lors de la selection
 * de la ligne.
 * 
 * TODO echatellier 20120412 reprise de code herité du jdk, pas très propre
 * mais en essayant de l'enlevr, on perd le coloration des lignes non
 * selectionnées.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class EmptyCellRenderer extends DefaultTableCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -7834417406160620726L;

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (table.isCellEditable(row, column)
                && (value == null
                    || (value instanceof String) && ((String) value).isEmpty()
                    )
                ) {
            setBackground(new Color(255, 198, 209));
        } else {
            if (isSelected) {
                setForeground(new Color(0, 0, 0));
            }
        }

        if (table.isCellEditable(row, column)) {
            // true for voucher and description
            // false for Account Class
            // if empty or null, colour background in red
            // else setText
            if (value instanceof String) {
                if (String.valueOf(value) == null || String.valueOf(value).isEmpty()) {
                    component.setBackground(new Color(255, 198, 209));
                }
            } else {
                if (value == null) {
                    component.setBackground(new Color(255, 198, 209));
                }
            }
        }
        return this;
    }

}
