/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.common;

import javax.swing.*;
import java.util.List;

/**
 * Model generique pour les objects lima.
 * 
 * @param <E> type d'object géré
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class GenericComboBoxModel<E> extends DefaultComboBoxModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 4778917695588663498L;

    protected List<E> objects;
    
    @Override
    public int getSize() {
        int result = 0;
        if (objects != null) {
            result = objects.size();
        }
        return result;
    }

    @Override
    public Object getElementAt(int index) {
        return objects.get(index);
    }
    
    public void setObjects(List<E> objects) {
        this.objects = objects;
        fireContentsChanged(this, 0, objects.size());
    }
}
