/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.common;

import org.chorem.lima.entity.FinancialPeriod;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;

/**
 * Financial period list renderer.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialPeriodListRenderer extends DefaultListCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -9089182547408397051L;

    // afiche le mois en lettre et le numero de la période, pour eviter les confusions entre deux mois identique pour des exercice de plus de 12 mois

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        FinancialPeriod financialPeriod = (FinancialPeriod)value;
        Object newValue = financialPeriod;
        if (financialPeriod != null) {
            // index = -1 pour la valeur sectionné
            if (index < 0) {
                index = list.getSelectedIndex();
            }
            newValue = String.format("%1$tB (%2$d)", financialPeriod.getBeginDate(), index + 1);
        }
        return super.getListCellRendererComponent(list, newValue, index, isSelected,
                cellHasFocus);
    }

}
