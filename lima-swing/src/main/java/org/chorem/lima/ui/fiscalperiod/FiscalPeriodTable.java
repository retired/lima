/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.fiscalperiod;

import org.chorem.lima.ui.celleditor.DateLimaTableCellRenderer;
import org.chorem.lima.ui.celleditor.DefaultLimaTableCellRenderer;
import org.chorem.lima.ui.celleditor.TableCellErrorDetector;
import org.chorem.lima.ui.common.AbstractLimaTable;

import java.util.Date;

/**
 * Fiscal period table add support for JXTable.
 * 
 * Including:
 * <ul>
 * <li>Locked period red highlight
 * <li>
 * </ul>
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FiscalPeriodTable extends AbstractLimaTable<FiscalPeriodViewHandler> {

    private static final long serialVersionUID = -8462838870024505659L;

    public FiscalPeriodTable(FiscalPeriodViewHandler handler) {
        super(handler);

        // renderer
        TableCellErrorDetector errorDetector = new FiscalPeriodErrorDetector();

        DefaultLimaTableCellRenderer renderer = new DefaultLimaTableCellRenderer();
        renderer.setErrorDetector(errorDetector);
        setDefaultRenderer(Object.class, renderer);

        renderer = new DateLimaTableCellRenderer();
        renderer.setErrorDetector(errorDetector);
        setDefaultRenderer(Date.class, renderer);
    }

}
