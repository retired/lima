/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin,Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialtransaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;

/**
 * Table des transaction qui ajoute des comportement (keys).
 *
 * <ul>
 * <li>Auto creation des entrees si la transaction est vide (tab)
 * <li>positionnement automatique sur les cellules editables
 * </ul>
 *
 * @author jpepin
 * @author echatellier
 */
public class FinancialTransactionTable extends FinancialTransactionDefaultTable<FinancialTransactionViewHandler> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 3133690382049594727L;

    private static final Log log = LogFactory.getLog(FinancialTransactionTable.class);

    public FinancialTransactionTable(FinancialTransactionViewHandler handler) {
        super(handler);
    }

    @Override
    protected void initNavigation() {
        super.initNavigation();
        InputMap inputMap= getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = getActionMap();

        // action on Tab
        Object binding = inputMap.get(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0));
        actionMap.put(binding, new NextCellAction());

    }

    private class NextCellAction extends AbstractAction {

        FinancialTransactionTable table = FinancialTransactionTable.this;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (log.isDebugEnabled()) {
                log.debug("Key tab used");
            }

            exit();

            int nbColumn = getColumnCount();
            int nbRows = getRowCount();
            int column = getSelectedColumn();
            int row = getSelectedRow();

            if (column >= 0 && row >= 0) {

                boolean end = false;

                FinancialTransactionTableModel tableModel = (FinancialTransactionTableModel) getModel();
                FinancialTransaction transaction = tableModel.getTransactionAt(row);
                do {
                    if (column >= nbColumn - 1) {
                        if (row >= nbRows - 1 || transaction != tableModel.getTransactionAt(row + 1)) {
                            BigDecimal credit = transaction.getAmountCredit();
                            BigDecimal debit = transaction.getAmountDebit();
                            if (credit.compareTo(debit) != 0) {
                                table.handler.addEntry();
                                // addEntry entry change selection
                                end = true;
                            } else if (row >= nbRows - 1) {
                                table.handler.addFinancialTransaction();
                                // addFinancialTransaction change selection
                                end = true;
                            } else {
                                column = 0;
                                row++;
                            }
                        } else {
                            column = 0;
                            row++;
                        }
                    } else {
                        column++;
                    }
                } while (!end && !tableModel.isCellEditable(row,column));

                if (!end) {
                    changeSelection(row, column, false, false);
                }
            }
        }
    }

}
