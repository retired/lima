/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.accountViewer;

import org.chorem.lima.entity.Entry;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.AbstractLimaTableModel;

import java.math.BigDecimal;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Basic transaction table model.
 * <p/>
 *
 * @author ore
 * @author chatellier
 */
public class AccountViewerTableModel extends AbstractLimaTableModel<Entry> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Override
    protected void initColumn() {
        addColumn(new AbstractColumn<AccountViewerTableModel>(Date.class, t("lima.table.date"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getFinancialTransaction().getTransactionDate();
            }
        });

        addColumn(new AbstractColumn<AccountViewerTableModel>(String.class, t("lima.table.entryBook"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getFinancialTransaction().getEntryBook().getCode();
            }
        });

        addColumn(new AbstractColumn<AccountViewerTableModel>(String.class, t("lima.table.voucher"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getVoucher();
            }
        });

        addColumn(new AbstractColumn<AccountViewerTableModel>(String.class, t("lima.table.description"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getDescription();
            }
        });

        addColumn(new AbstractColumn<AccountViewerTableModel>(String.class, t("lima.table.letter"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.getLettering();
            }
        });

        addColumn(new AbstractColumn<AccountViewerTableModel>(BigDecimal.class, t("lima.table.debit"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.isDebit() ? entry.getAmount() : BigDecimal.ZERO;
            }
        });

        addColumn(new AbstractColumn<AccountViewerTableModel>(BigDecimal.class, t("lima.table.credit"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry entry = tableModel.get(row);
                return entry.isDebit() ? BigDecimal.ZERO : entry.getAmount();
            }
        });

        addColumn(new AbstractColumn<AccountViewerTableModel>(BigDecimal.class, t("lima.table.balance"), false) {
            @Override
            public Object getValueAt(int row) {
                Entry currentEntry = tableModel.get(row);
                BigDecimal result = BigDecimal.ZERO;
                result = currentEntry.isDebit() ? result.add(currentEntry.getAmount()) : result.subtract(currentEntry.getAmount());
                if (row > 0) {
                    int i = 1;
                        while (row - i >= 0) {
                            Entry prevEntry = tableModel.get(row - i);
                            result = prevEntry.isDebit() ? result.add(prevEntry.getAmount()) : result.subtract(prevEntry.getAmount());
                            i++;
                        }

                }
                return result;
            }
        });

    }
}
