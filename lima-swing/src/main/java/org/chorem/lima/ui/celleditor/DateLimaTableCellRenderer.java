package org.chorem.lima.ui.celleditor;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.LimaSwingConfig;

import java.text.DateFormat;
import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class DateLimaTableCellRenderer extends DefaultLimaTableCellRenderer {

    @Override
    protected void setValue(Object value) {
        if (value instanceof Date) {
            DateFormat format = DateFormat.getDateInstance(DateFormat.MEDIUM, LimaSwingConfig.getInstance().getLocale());
            super.setValue(format.format((Date) value));
        } else {
            super.setValue(value);
        }
    }
}
