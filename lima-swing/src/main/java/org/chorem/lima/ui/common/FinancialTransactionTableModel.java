package org.chorem.lima.ui.common;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.ClosedPeriodicEntryBookService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.utils.EntryComparator;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.ui.financialtransaction.AccountColumn;
import org.chorem.lima.ui.financialtransaction.CreditColumn;
import org.chorem.lima.ui.financialtransaction.DayColumn;
import org.chorem.lima.ui.financialtransaction.DebitColumn;
import org.chorem.lima.ui.financialtransaction.DescriptionColumn;
import org.chorem.lima.ui.financialtransaction.VoucherColumn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FinancialTransactionTableModel extends TableModelWithGroup<Entry> implements ServiceListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = -7495388454688562991L;

    protected static final Log log = LogFactory.getLog(FinancialTransactionTableModel.class);

    /** Service (just to update setValueAt(). */
    protected FinancialTransactionService financialTransactionService;

    protected Map<FinancialTransaction, Boolean> lockedByTransaction;

    protected ClosedPeriodicEntryBookService closedPeriodicEntryBookService;

    protected HashMap<FinancialTransaction, ClosedPeriodicEntryBook> closedPeriodicEntryBookForTransaction;

    protected Map<EntryBook, List<ClosedPeriodicEntryBook>> closedPeriodicEntryBookByEntryBook;

    protected Boolean isGlobalyLocked;

    public FinancialTransactionTableModel() {
        setComparator(new EntryComparator());

        financialTransactionService = LimaServiceFactory.getService(FinancialTransactionService.class);
        closedPeriodicEntryBookService =  LimaServiceFactory.getService(ClosedPeriodicEntryBookService.class);
        LimaServiceFactory.addServiceListener(FinancialPeriodService.class, this);
        lockedByTransaction = Maps.newHashMap();
    }

    @Override
    protected void initColumn() {
        addColumn(new DayColumn());
        addColumn(new VoucherColumn());
        addColumn(new AccountColumn());
        addColumn(new DescriptionColumn());
        addColumn(new DebitColumn());
        addColumn(new CreditColumn());
    }

    protected Ordering<Entry> ordering = Ordering.from(new EntryComparator());

    public void setTransactions(List<FinancialTransaction> transactions) {
        clear();
        List<Entry> orderedEntries = new ArrayList<>();
        for (FinancialTransaction transaction : transactions) {
            //addAll(transaction.getEntry());
            // Normal entry order is by create date
            // But it can append that entries with same voucher or not grouped
            // and make transaction reading difficult.
            // The following method keep entries ordered by date but group them by voucher to.
            List<Entry> result = getOrderedTransactionEntries(transaction);
            orderedEntries.addAll(result);
        }
        addAll(orderedEntries);
    }

    //Entries should be ordered from Model and data base.
    // FIXME ref #1291
    protected List<Entry> getOrderedTransactionEntries(FinancialTransaction transaction) {
        Collection<Entry> entries = transaction.getEntry();
        List<Entry> orderedEntries = ordering.sortedCopy(entries);
        LinkedHashMap<String, List<Entry>> orderedEntriesByVoucher = getEntriesByVoucherByDate(orderedEntries);
        List<Entry> result = getGroupedOrderedEntries(orderedEntriesByVoucher);
        return result;
    }

    private List<Entry> getGroupedOrderedEntries(LinkedHashMap<String, List<Entry>> orderedEntriesByVoucher) {
        List<Entry> result = Lists.newArrayList();
        for (Map.Entry<String, List<Entry>> stringListEntry : orderedEntriesByVoucher.entrySet()) {
            result.addAll(stringListEntry.getValue());
        }
        return result;
    }

    protected LinkedHashMap<String, List<Entry>> getEntriesByVoucherByDate(List<Entry> orderedEntries) {
        LinkedHashMap<String, List<Entry>> orderedEntriesByVoucher = new LinkedHashMap<>();
        for (Entry orderedEntry : orderedEntries) {
            List<Entry> entriesForVoucher = orderedEntriesByVoucher.computeIfAbsent(orderedEntry.getVoucher(), k -> Lists.newArrayList());
            entriesForVoucher.add(orderedEntry);
        }
        return orderedEntriesByVoucher;
    }

    public void addAll(List<Entry> values) {

        if (values != null && !values.isEmpty()) {

            setTransactionLocked(values);

            int row = this.values.size();
            this.values.addAll(values);
            fireTableRowsInserted(row, row + values.size() - 1);
        }
    }

    protected void setTransactionLocked(List<Entry> values) {
        if (isGlobalyLocked == null) {

            setClosedPeriodicEntryBookByEntryBook(values);

            setClosedPeriodicEntryBookForTransaction(values);
        }
    }

    protected void setClosedPeriodicEntryBookForTransaction(List<Entry> values) {
        closedPeriodicEntryBookForTransaction = new HashMap<>();
        for (Entry value : values) {
            FinancialTransaction etr = value.getFinancialTransaction();
            List<ClosedPeriodicEntryBook> cpebs = closedPeriodicEntryBookByEntryBook.get(etr.getEntryBook());
            if (CollectionUtils.isNotEmpty(cpebs)) {
                for (ClosedPeriodicEntryBook cpeb : cpebs) {
                    FinancialPeriod fp = cpeb.getFinancialPeriod();
                    if (cpeb.getFinancialPeriod().getBeginDate().compareTo(fp.getBeginDate()) <= 0 &&
                            cpeb.getFinancialPeriod().getEndDate().compareTo(fp.getEndDate()) >= 0) {
                        closedPeriodicEntryBookForTransaction.put(etr, cpeb);
                        break;
                    }
                }
            }
        }
    }

    protected void setClosedPeriodicEntryBookByEntryBook(List<Entry> values) {
        closedPeriodicEntryBookByEntryBook = new HashMap<>();
        List<Entry> orderedEntries = ordering.sortedCopy(values);

        Entry first = orderedEntries.get(0);
        Entry last = orderedEntries.get(orderedEntries.size() - 1);
        Date from = first.getFinancialTransaction().getTransactionDate();
        Date to = last.getFinancialTransaction().getTransactionDate();
        Collection<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = closedPeriodicEntryBookService.getForDates(from, to);

        for (ClosedPeriodicEntryBook closedPeriodicEntryBook : closedPeriodicEntryBooks) {
            EntryBook entryBook = closedPeriodicEntryBook.getEntryBook();
            List<ClosedPeriodicEntryBook> cpeb = closedPeriodicEntryBookByEntryBook.computeIfAbsent(entryBook, k -> new ArrayList<>());
            cpeb.add(closedPeriodicEntryBook);
        }
    }

    public FinancialTransaction getTransactionAt(int row) {
        Entry entry = get(row);
        return entry.getFinancialTransaction();
    }

    public boolean isLocked(int row) {

        FinancialTransaction transaction = getTransactionAt(row);

        Boolean locked = isGlobalyLocked == null ? lockedByTransaction.get(transaction) : isGlobalyLocked;

        if (locked == null) {
            ClosedPeriodicEntryBook closedPeriodicEntryBook = closedPeriodicEntryBookForTransaction.computeIfAbsent(transaction, t -> closedPeriodicEntryBookService.getByEntryBookAndFinancialPeriod(
                    t.getEntryBook(),
                    t.getTransactionDate()));

            if (log.isDebugEnabled()) {
                log.debug("transaction " + transaction.getTransactionDate() + " : " + closedPeriodicEntryBook.isLocked());
            }
            locked = closedPeriodicEntryBook.isLocked();
            lockedByTransaction.put(transaction, locked);
        }

        return locked;
    }

    public boolean isCellNavigable(int row, int column) {
        return super.isCellEditable(row, column);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return super.isCellEditable(row, column) && !isLocked(row);
    }

    public int indexOf(FinancialTransaction transaction) {
        int index = 0;
        Collection<Entry> entriesTransaction = transaction.getEntry();
        for (Entry entry : values) {
            if (!entriesTransaction.contains(entry)) {
                index++;
            } else {
                break;
            }
        }
        return index;
    }

    public boolean contains(FinancialTransaction transaction) {
        boolean result = false;
        for (Entry entry : transaction.getEntry()) {
            if (contains(entry)) {
                result = true;
                break;
            }
        }
        return  result;
    }

    /**
     * Insert new entry.
     *
     * @param entry entry to insert
     */
    public Entry addEntry(Entry entry) throws BeforeFirstFiscalPeriodException, AfterLastFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException {
        Entry newEntry = null;
        FinancialTransaction transaction = entry.getFinancialTransaction();
        if (contains(transaction)) {
            newEntry = new EntryImpl();
            newEntry.setFinancialTransaction(transaction);
            newEntry.setVoucher(entry.getVoucher());
            newEntry.setAccount(entry.getAccount());
            newEntry.setDescription(entry.getDescription());
            newEntry.setAmount(entry.getAmount());
            newEntry.setDebit(entry.isDebit());
            newEntry = financialTransactionService.createEntry(newEntry);

            transaction.addEntry(newEntry);
            financialTransactionService.updateFinancialTransaction(transaction);
            addValue(newEntry);
        }
        return newEntry;
    }

    /**
     * Delete selected row in table (could be transaction or entry).
     * <p/>
     * Called by model.
     *
     * @param row
     */
    public void removeTransaction(int row) throws LockedFinancialPeriodException, LockedEntryBookException {
        FinancialTransaction transaction = getTransactionAt(row);
        removeAll(transaction.getEntry());
        financialTransactionService.removeFinancialTransaction(transaction);
    }

    public void removeEntry(int row) throws LockedFinancialPeriodException, LockedEntryBookException {
        Entry entry = get(row);
        FinancialTransaction transaction = entry.getFinancialTransaction();
        if (transaction.sizeEntry() > 1) {
            financialTransactionService.removeEntry(entry);
            transaction.removeEntry(entry);
            remove(entry);
        } else {
            financialTransactionService.removeFinancialTransaction(transaction);
            removeAll(transaction.getEntry());
        }
    }

    public FinancialTransaction addTransaction(FinancialTransaction transaction) throws LockedFinancialPeriodException, LockedEntryBookException, AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException {

        FinancialTransaction newTransaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());
        addAll(newTransaction.getEntry());

        return newTransaction;
    }

    public BigDecimal getBalanceTransactionInRow(int row) {
        FinancialTransaction transaction = getTransactionAt(row);
        BigDecimal debit = transaction.getAmountDebit();
        BigDecimal credit = transaction.getAmountCredit();
        BigDecimal balance = debit.subtract(credit);
        return balance;
    }

    @Override
    public boolean sameGroup(Entry e1, Entry e2) {
        return e1.getFinancialTransaction().equals(e2.getFinancialTransaction());
    }

    public boolean updateEntry(Entry entry) {
        boolean updated = false;

        if (log.isDebugEnabled()) {
            log.debug("Update Entry");
        }
        try {
            financialTransactionService.updateEntry(entry);
            updated = true;
        } catch (LockedEntryBookException e) {
            errorHelper.showErrorMessage(t("lima.entry.update.error.closedEntryBook",
                    e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                    e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                    e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                    e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
        } catch (LockedFinancialPeriodException e) {
            errorHelper.showErrorMessage(t("lima.entry.update.error.lockedFinancialPeriod",
                    e.getFinancialPeriod().getBeginDate(),
                    e.getFinancialPeriod().getEndDate()));
        }
        return updated;
    }

    public boolean updateTransaction(FinancialTransaction transaction) {
        boolean update = false;

        if (log.isDebugEnabled()) {
            log.debug("Update transaction");
        }
        try {
            financialTransactionService.updateFinancialTransaction(transaction);
            update = true;

        } catch (LockedFinancialPeriodException e) {
            errorHelper.showErrorMessage(t("lima.financialTransaction.update.error.lockedFinancialPeriod",
                    e.getFinancialPeriod().getBeginDate(),
                    e.getFinancialPeriod().getEndDate()));
        } catch (AfterLastFiscalPeriodException e) {
            errorHelper.showErrorMessage(t("lima.financialTransaction.update.error.afterLastFiscalPeriod",
                    e.getDate()));
        } catch (BeforeFirstFiscalPeriodException e) {
            errorHelper.showErrorMessage(t("lima.financialTransaction.update.error.beforeFirstFiscalPeriod",
                    e.getDate()));
        } catch (LockedEntryBookException e) {
            errorHelper.showErrorMessage(t("lima.financialTransaction.update.error.lockedEntryBook",
                    e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                    e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                    e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                    e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
        }
        return update;
    }

    public void fireTransaction(FinancialTransaction transaction) {
        int firstRow = indexOf(transaction);
        int lastRow = firstRow + transaction.sizeEntry() - 1;
        fireTableRowsUpdated(firstRow, lastRow);
    }

    @Override
    public void notifyMethod(String serviceName, String methodeName) {
        log.debug("Nom de la méthode : " + methodeName);
        if (methodeName.contains("blockClosedPeriodicEntryBook")) {
            lockedByTransaction.clear();
        }
    }

    public void setGlobalyLocked(Boolean globalyLocked) {
        isGlobalyLocked = globalyLocked;
    }
}
