/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.account;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.exceptions.AlreadyExistAccountException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnexistingAccount;
import org.chorem.lima.business.exceptions.UsedAccountException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.enums.AccountsChartEnum;
import org.chorem.lima.enums.ImportExportEnum;
import org.chorem.lima.enums.MainUiRefreshComponent;
import org.chorem.lima.ui.MainView;
import org.chorem.lima.ui.importexport.ImportExport;
import org.chorem.lima.util.ErrorHelper;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler associated with account view.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class AccountViewHandler implements ServiceListener {

    /** log. */
    private static final Log log = LogFactory.getLog(AccountViewHandler.class);

    protected AccountService accountService;

    protected AccountView view;

    protected ErrorHelper errorHelper;

    /**
     * Sort account with label length.
     */
    protected static final Comparator<Account> ACCOUNT_LENGTH_COMPARATOR = new Comparator<Account>() {
        @Override
        public int compare(Account o1, Account o2) {
            int result = o1.getAccountNumber().length() - o2.getAccountNumber().length();
            if (result == 0) {
                // same length, compare accountNumber
                result = o1.getAccountNumber().compareTo(o2.getAccountNumber());
            }
            return result;
        }
    };
    /**
     * Sort Account number by lenght in reverse order.
     */
    protected static final Comparator<String> REVERSE_ACCOUNT_LENGTH_COMPARATOR = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            int result = o2.length() - o1.length();
            if (result == 0) {
                // same length, compare accountNumber
                result = o2.compareTo(o1);
            }
            return result;
        }
    };

    public AccountViewHandler(AccountView view) {
        this.view = view;
        // Gets factory service        
        LimaServiceFactory.addServiceListener(ImportService.class, this);
        accountService = LimaServiceFactory.getService(AccountService.class);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
    }

    /**
     * Init initialized view by loading account data from service.
     */
    public void init() {
        JXTreeTable table = view.getAccountsTreeTable();

        //To block reaction of the dual key 'ctrl+a' (Selection of all lines)
        InputMap inputMap = view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK), "none");

        // add action on Ctrl + N
        String binding = "new-account";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -2627651616597622057L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addAccount();
            }
        });

        // add action on Delete
        binding = "remove-account";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 8244969229065970041L;

            @Override
            public void actionPerformed(ActionEvent e) {
                removeAccount();
            }
        });

        // add action on Ctrl + M
        binding = "modify-account";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -1287526862158662714L;

            @Override
            public void actionPerformed(ActionEvent e) {
                updateAccount();
            }
        });

        // add action on Ctrl + I
        binding = "import-account";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -8953401784332356894L;

            @Override
            public void actionPerformed(ActionEvent e) {
                importAccountsChart();
            }
        });

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                JXTreeTable source = (JXTreeTable) e.getSource();
                if (source.rowAtPoint(e.getPoint()) == -1) {
                    source.clearSelection();
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                JXTreeTable source = (JXTreeTable) e.getSource();
                if (source.rowAtPoint(e.getPoint()) >= 0 && e.getClickCount() == 2 ) {
                    updateAccount();
                }
            }
        });
        
        loadAllAccounts();
    }

    /**
     * Load all accounts from service and display it into tree table.
     */
    protected void loadAllAccounts() {
        // default data load
        List<Account> accounts = accountService.getAllAccounts();
        accounts.sort(ACCOUNT_LENGTH_COMPARATOR);
        if (log.isDebugEnabled()) {
            log.debug(String.format("Loaded %d accounts from service", accounts.size()));
        }

        // render in tree node hierarchy for DefaultTreeTableModel
        SortedMap<String, DefaultMutableTreeTableNode> nodeCache = new TreeMap<>(REVERSE_ACCOUNT_LENGTH_COMPARATOR);
        DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode(null);
        for (Account account : accounts) {
            // find parent
            DefaultMutableTreeTableNode parentNode = root;
            for (Map.Entry<String, DefaultMutableTreeTableNode> entry : nodeCache.entrySet()) {
                String accountNumber = entry.getKey();
                if (account.getAccountNumber().startsWith(accountNumber)) {
                    parentNode = entry.getValue();
                    break;
                }
            }
            
            // make current node
            DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(account);
            parentNode.add(node);

            nodeCache.put(account.getAccountNumber(), node);
        }
        
        // refreshing tree's model
        DefaultTreeTableModel model = new AccountTreeTableModel(root);
        model.setColumnIdentifiers(Arrays.asList(t("lima.table.number"), t("lima.table.label")));
        JXTreeTable table = view.getAccountsTreeTable();
        table.setTreeTableModel(model);
    }

    /**
     * Display add account view
     */
    public void addAccount() {
        final AccountForm accountForm = new AccountForm(view);

        InputMap inputMap = accountForm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = accountForm.getRootPane().getActionMap();
        String binding = "dispose";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -4829115532909638962L;

            @Override
            public void actionPerformed(ActionEvent e) {
                accountForm.dispose();
            }
        });

        accountForm.setLocationRelativeTo(view);
        accountForm.setVisible(true);
    }

    /**
     * Add new account with account form.
     *
     * @param dialog the account form
     */
    public void addAccount(AccountForm dialog) {

        try {
            Account newAccount = dialog.getAccount();
            newAccount = accountService.createAccount(newAccount);

            // update tree
            JXTreeTable treeTable = view.getAccountsTreeTable();
            DefaultTreeTableModel model = (DefaultTreeTableModel)treeTable.getTreeTableModel();
            DefaultMutableTreeTableNode node = (DefaultMutableTreeTableNode)findParentNode(model.getRoot(), newAccount.getAccountNumber());
            DefaultMutableTreeTableNode newNode = new DefaultMutableTreeTableNode(newAccount);

            List<MutableTreeTableNode> nodesToMove = findSubNodes(node, newAccount.getAccountNumber());
            for (MutableTreeTableNode nodeToMove : nodesToMove) {
                model.removeNodeFromParent(nodeToMove);
                newNode.add(nodeToMove);
            }

            model.insertNodeInto(newNode, node, node.getChildCount());
            treeTable.expandPath(new TreePath(model.getPathToRoot(node)));

            MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
            mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ACCOUNT_PANE);

        } catch (AlreadyExistAccountException e) {
            errorHelper.showErrorMessage(t("lima.account.add.error.alreadyExist", e.getAccountNumber()));
        } catch (InvalidAccountNumberException e) {
            errorHelper.showErrorMessage(t("lima.account.add.error.InvalidAccountNumber", e.getAccountNumber()));
        } catch (NotNumberAccountNumberException e) {
            errorHelper.showErrorMessage(t("lima.account.error.notNumberAccountNumber", e.getAccountNumber()));
        } finally {
            dialog.dispose();
        }
    }

    /**
     * Find potential parent node for account number.
     * 
     * @param currentNode node 
     * @param accountNumber node label to search parent
     * @return found parent (can't be null)
     */
    protected TreeTableNode findParentNode(TreeTableNode currentNode,
            String accountNumber) {

        TreeTableNode result = null;
        Account account = (Account)currentNode.getUserObject();

        if (account == null || accountNumber.startsWith(account.getAccountNumber())) {
            for (int childIndex = 0; childIndex < currentNode.getChildCount() && result == null; childIndex++) {
                TreeTableNode child = currentNode.getChildAt(childIndex);
                result = findParentNode(child, accountNumber);
            }

            if (result == null) {
                result = currentNode;
            }
        }

        return result;
    }

    /**
     * Find all subnodes in currentNode with account label starting with accountNumber.
     * 
     * @param currentNode currentNode to search into
     * @param accountNumber accountNumber number to search
     * @return node list
     */
    protected List<MutableTreeTableNode> findSubNodes(TreeTableNode currentNode, String accountNumber) {
        List<MutableTreeTableNode> nodes = new ArrayList<>();
        for (int childIndex = 0; childIndex < currentNode.getChildCount(); childIndex++) {
            MutableTreeTableNode child = (MutableTreeTableNode)currentNode.getChildAt(childIndex);
            Account account = (Account)child.getUserObject();
            if (account.getAccountNumber().startsWith(accountNumber)) {
                nodes.add(child);
            }
        }
        return nodes;
    }

    /**
     * Open update account (or subledger) form with selected account
     * from the tree.
     */
    public void updateAccount() {
        JXTreeTable treeTable = view.getAccountsTreeTable();

        // get selected account
        int selectedRow = treeTable.getSelectedRow();
        TreePath treePath = treeTable.getPathForRow(selectedRow);
        if (treePath != null) {
            TreeTableNode lastPathComponent = (TreeTableNode) treePath.getLastPathComponent();
            Account selectedAccount = (Account)lastPathComponent.getUserObject();

            // display edit form
            final UpdateAccountForm accountForm = new UpdateAccountForm(view);

            InputMap inputMap = accountForm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            ActionMap actionMap = accountForm.getRootPane().getActionMap();
            String binding = "dispose";
            inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
            actionMap.put(binding, new AbstractAction() {
                private static final long serialVersionUID = -2123621815991309210L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    accountForm.dispose();
                }
            });


            accountForm.setAccount(selectedAccount);
            accountForm.setLocationRelativeTo(view);
            accountForm.setVisible(true);

            MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
            mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ACCOUNT_PANE);
        }
    }

    /**
     * Perform update account to service.
     * 
     * @param dialog dialog containing account
     */
    public void updateAccount(UpdateAccountForm dialog) {

        try {
            Account account = dialog.getAccount();
            account = accountService.updateAccount(account);
    
            // update tree
            JXTreeTable treeTable = view.getAccountsTreeTable();
            DefaultTreeTableModel model = (DefaultTreeTableModel)treeTable.getTreeTableModel();
            int selectedRow = treeTable.getSelectedRow();
            TreePath treePath = treeTable.getPathForRow(selectedRow);
            //TreeTableNode lastPathComponent = (TreeTableNode) treePath.getLastPathComponent();
            //lastPathComponent.setUserObject(account);
            model.valueForPathChanged(treePath, account);

        } catch (InvalidAccountNumberException e) {
            errorHelper.showErrorMessage(t("lima.account.update.error.invalidAccountNumber", e.getAccountNumber()));
        } catch (NotNumberAccountNumberException e) {
            errorHelper.showErrorMessage(t("lima.account.error.notNumberAccountNumber", e.getAccountNumber()));
        } finally {
            // close dialog
            dialog.dispose();
        }
    }

    /**
     * Ask for user to remove for selected account, and remove it if confirmed.
     */
    public void removeAccount() {

        // maybe this code can be factorised
        JXTreeTable treeTable = view.getAccountsTreeTable();
        int selectedRow = treeTable.getSelectedRow();
        TreePath treePath = treeTable.getPathForRow(selectedRow);
        MutableTreeTableNode lastNode = (MutableTreeTableNode)treePath.getLastPathComponent();
        Account account = (Account)lastNode.getUserObject();

        int response = JOptionPane.showConfirmDialog(view,
             t("lima.account.remove.confirm", account.getAccountNumber()),
             t("lima.account.remove.confirm.title"),
             JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (response == JOptionPane.YES_OPTION) {
            try {
                accountService.removeAccount(account);

                // add all sub accounts to parent
                DefaultTreeTableModel model = (DefaultTreeTableModel) treeTable.getTreeTableModel();
                MutableTreeTableNode parent = (MutableTreeTableNode) lastNode.getParent();
                for (int childIndex = lastNode.getChildCount() - 1; childIndex >= 0; childIndex--) {
                    MutableTreeTableNode child = (MutableTreeTableNode) lastNode.getChildAt(childIndex);
                    model.insertNodeInto(child, parent, parent.getChildCount());
                }

                // remove node
                model.removeNodeFromParent(lastNode);

                MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ACCOUNT_PANE);

            } catch (UsedAccountException e) {
                errorHelper.showErrorMessage(t("lima.account.remove.error.usedAccount", e.getAccountNumber()));
            } catch (UnexistingAccount unexistingAccount) {
                errorHelper.showErrorMessage(t("lima.account.remove.error.unexistingAcount", unexistingAccount.getAccountNumber()));
            }
        }
    }

    public void importAccountsChart() {

        final AccountImportForm form = new AccountImportForm(view);

        InputMap inputMap = form.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = form.getRootPane().getActionMap();
        String binding = "dispose";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -1756820018546487410L;

            @Override
            public void actionPerformed(ActionEvent e) {
                form.performCancel();
            }
        });

        form.setLocationRelativeTo(view);
        form.setVisible(true);

        Object value = form.getChartAccountCombo().getSelectedItem();
        // if action confirmed
        if (value != null) {
            ImportExport importExport = new ImportExport(view);
            AccountsChartEnum defaultAccountsChartEnum = (AccountsChartEnum) value;
            //Import accounts chart
            switch (defaultAccountsChartEnum) {
                case IMPORT_EBP:
                    importExport.importExport(ImportExportEnum.EBP_ACCOUNTCHARTS_IMPORT,
                            null,
                            defaultAccountsChartEnum.getDefaultFileURL(),
                            false);
                    break;

                default:
                    importExport.importExport(ImportExportEnum.CSV_ACCOUNTCHARTS_IMPORT,
                            null,
                            defaultAccountsChartEnum.getDefaultFileURL(),
                            false);
                    break;
            }
            loadAllAccounts();

            MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
            mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ACCOUNT_PANE);

        }
    }

    /**
     * Called when import methods are called on services.
     */
    @Override
    public void notifyMethod(String serviceName, String methodName) {

        if (methodName.contains("importAccounts") ||
                methodName.contains("importAll") ||
                methodName.contains("importAsCSV")) {
            
            if (log.isInfoEnabled()) {
                log.info(String.format("Service notification %s, reloading accounts", methodName));
            }

            loadAllAccounts();
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Ignoring service notification " + serviceName + ":" + methodName);
            }
        }
    }
}
