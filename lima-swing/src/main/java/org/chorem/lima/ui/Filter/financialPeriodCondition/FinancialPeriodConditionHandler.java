package org.chorem.lima.ui.Filter.financialPeriodCondition;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.Condition;
import org.chorem.lima.beans.FinancialPeriodCondition;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;

import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FinancialPeriodConditionHandler implements ConditionHandler {

    protected FinancialPeriodConditionView view;

    protected FinancialPeriodCondition condition;

    protected FinancialTransactionConditionHandler filterHandler;

    public FinancialPeriodConditionHandler(FinancialPeriodConditionView view) {
        this.view = view;
        this.condition = new FinancialPeriodCondition();
    }

    @Override
    public Condition getCondition() {
        return condition;
    }

    @Override
    public FinancialPeriodConditionView getView() {
        return view;
    }

    @Override
    public void setFilterHandler(FinancialTransactionConditionHandler filterHandler) {
        this.filterHandler = filterHandler;
    }

    public Object[] getFinancialPeriodList() {
        FinancialPeriodService financialPeriodService = LimaServiceFactory.getService(FinancialPeriodService.class);
        List<FinancialPeriod> periods = financialPeriodService.getAllFinancialPeriods();
        Object[] result = periods.toArray();
        return result;
    }

    public void setFinancialPeriod(ListSelectionEvent event) {
        JList financialPeriodList = view.getFinancialPeriodList();
        List<FinancialPeriod> selectedValuesList = financialPeriodList.getSelectedValuesList();
        condition.setFinancialPeriods(selectedValuesList);
    }

    public void delete() {
        filterHandler.removeCondition(this);
    }

}
