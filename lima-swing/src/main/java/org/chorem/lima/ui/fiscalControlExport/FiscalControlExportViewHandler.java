package org.chorem.lima.ui.fiscalControlExport;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.ExportResult;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.ExportService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.ui.importexport.ImportExportWaitView;
import org.chorem.lima.util.ErrorHelper;
import org.jdesktop.swingx.painter.BusyPainter;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.rmi.server.ExportException;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FiscalControlExportViewHandler {

    private static final Log log = LogFactory.getLog(FiscalControlExportViewHandler.class);

    protected FiscalControlExportView view;

    protected FiscalPeriodService fiscalPeriodService;

    protected EntryBookService entryBookService;

    protected IdentityService identityService;

    protected ExportService exportService;

    private ImportExportWaitView waitView;


    public FiscalControlExportViewHandler(FiscalControlExportView view) {
        this.view = view;
        fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);
        entryBookService = LimaServiceFactory.getService(EntryBookService.class);
        identityService = LimaServiceFactory.getService(IdentityService.class);
        exportService = LimaServiceFactory.getService(ExportService.class);
    }

    public void init() {
        List<FiscalPeriod> fiscalPeriods = fiscalPeriodService.getAllBlockedFiscalPeriods();
        view.getFiscalPeriodComboBoxModel().setObjects(fiscalPeriods);
        if (!fiscalPeriods.isEmpty()) {
            view.getFiscalPeriodComboBoxModel().setSelectedItem(fiscalPeriods.get(fiscalPeriods.size() - 1));
        }
        List<EntryBook> entryBooks = entryBookService.getAllEntryBooks();
        view.getEntryBookAtNewComboBoxModel().setObjects(entryBooks);
        Identity identity = identityService.getIdentity();
        view.getSirenWarnLabel().setVisible(identity != null && StringUtils.isBlank(identity.getBusinessNumber()));
    }

    public void export() {
        FiscalPeriod fiscalPeriod = (FiscalPeriod) view.getFiscalPeriodComboBoxModel().getSelectedItem();
        EntryBook entryBookAtNew = (EntryBook) view.getEntryBookAtNewComboBoxModel().getSelectedItem();
        if (fiscalPeriod != null && entryBookAtNew != null) {
            String fileName = exportService.getFiscalControlFileName(fiscalPeriod);
            File homeDirectory = new File(System.getProperty("user.home"));
            File exportFile = new File(homeDirectory, fileName);

            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setSelectedFile(exportFile);
            String approveButtonText = t("lima.importExport.export");
            chooser.setDialogTitle(approveButtonText);
            chooser.setApproveButtonText(approveButtonText);

            if (chooser.showOpenDialog(view) == JFileChooser.APPROVE_OPTION) {
                exportFile = chooser.getSelectedFile();

                boolean continueExport = true;

                if (!exportFile.getName().equals(fileName)) {

                    String[] options = {t("lima.fiscalControlExport.file.nonConformanceName.continue"), t("lima.fiscalControlExport.file.nonConformanceName.cancel")};
                    continueExport = 0 == JOptionPane.showOptionDialog(
                            view,
                            t("lima.fiscalControlExport.file.nonConformanceName", exportFile.getName(), fileName),
                            t("lima.fiscalControlExport.file.nonConformanceName.title"),
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[0]);

                }

                if (continueExport && exportFile.exists()) {

                    String[] options = {t("lima.fiscalControlExport.file.overwrite.continue"), t("lima.fiscalControlExport.file.overwrite.cancel")};
                    continueExport = 0 == JOptionPane.showOptionDialog(
                            view,
                            t("lima.fiscalControlExport.file.overwrite", exportFile.getName()),
                            t("lima.fiscalControlExport.file.overwrite.title"),
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[0]);
                }

                if (continueExport) {

                    //create the wait dialog panel
                    waitView = new ImportExportWaitView();
                    waitView.setLocationRelativeTo(view);
                    BusyPainter busyPainter = waitView.getBusylabel().getBusyPainter();
                    busyPainter.setHighlightColor(new Color(44, 61, 146).darker());
                    busyPainter.setBaseColor(new Color(168, 204, 241).brighter());

                    new ExportWorker(fiscalPeriod, entryBookAtNew, exportFile).execute();

                    waitView.setVisible(true);
                    view.dispose();
                }
            }
        }
    }


    protected class ExportWorker extends SwingWorker<ExportResult, Void> {

        protected FiscalPeriod fiscalPeriod;
        protected EntryBook entryBookAtNew;
        protected File exportFile;

        public ExportWorker(FiscalPeriod fiscalPeriod, EntryBook entryBookAtNew, File exportFile) {
            this.fiscalPeriod = fiscalPeriod;
            this.entryBookAtNew = entryBookAtNew;
            this.exportFile = exportFile;
        }

        @Override
        protected ExportResult doInBackground() throws Exception {
            ExportResult exportResult = exportService.exportFiscalControl(fiscalPeriod, entryBookAtNew, Charsets.UTF_8.toString());
            List<ExportException> exportExceptions = exportResult.getExportExceptions();
            if (exportExceptions == null || exportExceptions.isEmpty()) {
                BufferedWriter out = null;
                try {
                    out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(exportFile), Charset.forName("UTF-8")));
                    out.write(exportResult.getExportData());
                    out.flush();
                    out.close();
                } catch (IOException eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't write file " + exportFile, eee);
                    }
                } finally {
                    IOUtils.closeQuietly(out);
                }
            } else {
                throw exportExceptions.get(0);
            }
            return exportResult;
        }

        @Override
        protected void done() {
            try {

                //hidde wait dialog panel
                waitView.setVisible(false);

                // display result dialog
                ExportResult exportResult = get();
                JOptionPane.showMessageDialog(
                        view,
                        t("lima.fiscalControlExport.success"),
                        t("lima.fiscalControlExport.success.title"),
                        JOptionPane.INFORMATION_MESSAGE);


            } catch (Exception ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't get result message", ex);
                }
                ErrorHelper errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
                errorHelper.showErrorDialog(null, ex.getMessage(), ex);
            }
        }

    }

}
