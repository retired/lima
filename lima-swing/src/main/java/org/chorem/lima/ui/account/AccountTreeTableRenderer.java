/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.account;

import org.chorem.lima.entity.Account;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

/**
 * Account renderer in account tree.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class AccountTreeTableRenderer extends DefaultTreeCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -3249368726501873583L;

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean selected, boolean expanded, boolean leaf, int row,
            boolean hasFocus) {

        MutableTreeTableNode node = (MutableTreeTableNode)value;
        Account account = (Account)node.getUserObject();
        Object localValue = value;
        if (account != null) {
            localValue = account.getAccountNumber();
        }
        return super.getTreeCellRendererComponent(tree, localValue, selected, expanded,
            leaf, row, hasFocus);
    }
 
}
