package org.chorem.lima.ui.common;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.event.ActionEvent;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CellRouteHorizontalAction extends AbstractAction{

    private static final long serialVersionUID = 6178936082032645524L;
    JTable table;

    protected int step;
    protected int route1;
    protected int route2;
    protected int nbCell1;
    protected int nbCell2;

    public CellRouteHorizontalAction(JTable table, boolean nextCell) {
        this.table = table;
        step = nextCell ? +1 : -1;
    }

    protected void init() {
        route1 = table.getSelectedColumn();
        route2 = table.getSelectedRow();
        nbCell1 = table.getColumnCount();
        nbCell2 = table.getRowCount();
    }

    protected int getRow() {
        return route2;
    }

    protected int getColumn() {
        return route1;
    }

    protected boolean isEditable() {
        return table.getModel().isCellEditable(getRow(), getColumn());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        init();
        if (route1 >= 0 && route2 >= 0) {
            do {
                route1 = route1 + step;
                if (route1 < 0 || route1 >= nbCell1) {
                    route1 = route1 % nbCell1;
                    if (route1 < 0) {
                        route1 = route1 + nbCell1;
                    }
                    route2 = route2 + step;
                    if (route2 < 0 || route2 >= nbCell2) {
                        route2 = route2 % nbCell2;
                        if (route2 < 0) {
                            route2 = route2 + nbCell2;
                        }
                    }
                }
            } while (!isEditable());
            if (table.isEditing()) {
                TableCellEditor editor = table.getCellEditor();
                editor.stopCellEditing();
            }
            table.changeSelection(getRow(), getColumn(), false, false);
        }
    }
}
