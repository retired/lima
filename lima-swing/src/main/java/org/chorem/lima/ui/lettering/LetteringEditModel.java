/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.ui.lettering;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author sletellier <letellier@codelutin.com>
 */
public class LetteringEditModel implements Serializable {

    public static final String PROPERTY_EDITABLE = "editable";

    public static final String PROPERTY_DEBIT = "debit";

    public static final String PROPERTY_CREDIT = "credit";

    public static final String PROPERTY_SOLD = "sold";

    public static final String PROPERTY_LETTERED = "lettered";

    public static final String PROPERTY_UNLETTERED = "unLettered";

    public static final String PROPERTY_EQUALIZED = "equalized";

    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    protected LetteringTableModel model;
    protected boolean lettered;
    protected boolean unLettered;
    protected boolean equalized;
    protected boolean editable;
    protected BigDecimal debit = BigDecimal.ZERO;
    protected BigDecimal credit = BigDecimal.ZERO;
    protected BigDecimal sold = BigDecimal.ZERO;

    protected BigDecimal tableDebit = BigDecimal.ZERO;
    protected BigDecimal tableCredit = BigDecimal.ZERO;
    protected BigDecimal tableSold = BigDecimal.ZERO;

    public boolean isEqualized() {
        return equalized;
    }

    public void setEqualized(boolean equalized) {
        boolean oldEqualized = isEqualized();
        this.equalized = equalized;
        firePropertyChange(PROPERTY_EQUALIZED, oldEqualized, this.equalized);
    }

    public boolean isLettered() {
        return lettered;
    }

    public void setLettered(boolean lettered) {
        boolean oldLettrer = isLettered();
        this.lettered = lettered && (BigDecimal.ZERO.equals(sold) || sold.doubleValue() == 0);
        firePropertyChange(PROPERTY_LETTERED, oldLettrer, this.lettered);
    }

    public boolean isUnLettered() {
        return unLettered;
    }

    public void setUnLettered(boolean unLettered) {
        boolean oldDeletterer = isUnLettered();
        this.unLettered = unLettered;
        firePropertyChange(PROPERTY_UNLETTERED, oldDeletterer, this.unLettered);
    }

    public BigDecimal getDebit() {
        return debit;
    }

    public void setDebit(BigDecimal debit) {
        BigDecimal oldDebit = getDebit();

        if (!BigDecimal.ZERO.equals(debit)){
            this.debit = debit.add(oldDebit);
        } else {
            this.debit = BigDecimal.ZERO;
        }

        firePropertyChange(PROPERTY_DEBIT, oldDebit, this.debit);
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        BigDecimal oldCredit = getCredit();

        if (!BigDecimal.ZERO.equals(credit)){
            this.credit = credit.add(oldCredit);
        } else {
            this.credit=BigDecimal.ZERO;
        }

        firePropertyChange(PROPERTY_CREDIT, oldCredit, this.credit);
    }

    public BigDecimal getSold() {
        return sold;
    }

    public void setSold(BigDecimal solde, boolean credit) {
        BigDecimal oldSold = getSold();

        if (!BigDecimal.ZERO.equals(solde)){
            if (credit){
                this.sold = oldSold.subtract(solde);
            } else {
                this.sold = oldSold.add(solde);
            }
        } else {
            this.sold =BigDecimal.ZERO;
        }

        firePropertyChange(PROPERTY_SOLD, oldSold, this.sold);
    }

    public BigDecimal getTableDebit() {
        return tableDebit;
    }

    public void setTableDebit(BigDecimal tableDebit) {
        this.tableDebit = tableDebit;
    }

    public BigDecimal getTableCredit() {
        return tableCredit;
    }

    public void setTableCredit(BigDecimal tableCredit) {
        this.tableCredit = tableCredit;
    }

    public BigDecimal getTableSold() {
        return tableSold;
    }

    public void setTableSold(BigDecimal tableSold) {
        this.tableSold = tableSold;
    }

    public void resetDebitCreditBalance(){
        setDebit(BigDecimal.ZERO);
        setCredit(BigDecimal.ZERO);
        setSold(BigDecimal.ZERO, false);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        boolean oldEditable = this.editable;
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldEditable, this.editable);
    }
}
