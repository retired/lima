package org.chorem.lima.ui.celleditor;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.ui.common.TableModelWithGroup;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class DefaultLimaTableCellRenderer implements TableCellRenderer {

    private String text = null;

    protected boolean mandatory;

    protected TableCellErrorDetector errorDetector;

    public DefaultLimaTableCellRenderer() {
        mandatory = false;
        errorDetector = new TableCellErrorDetector();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        //Create a new JLabel to avoid colouring only when row is selected
        JLabel myCell = new JLabel();
        myCell.setOpaque(true);

        myCell.setName("Table.entryCellRenderer");

        int line = row;

        if (table.getModel() instanceof TableModelWithGroup) {
            TableModelWithGroup model = (TableModelWithGroup) table.getModel();

            line = model.indexInGroup(row);
        }

        LimaSwingConfig limaSwingConfig = LimaSwingConfig.getInstance();

        // border
        Border border = BorderFactory.createEmptyBorder(1, 1, 1, 1);
        if (hasFocus) {
            Color color = limaSwingConfig.getOptionAsColor(LimaSwingConfig.Option.COLOR_SELECTION_FOCUS.getKey());
            border = BorderFactory.createLineBorder(color, 2);
        } else if (line == 0 && table.getModel() instanceof TableModelWithGroup) {
            border = BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK);
        }

        myCell.setBorder(border);

        // color

        LimaSwingConfig.Option backgroundOption;
        LimaSwingConfig.Option foreGroundOption;

        if ((line & 1) == 1 || line % 2 == 1) {
            if (isSelected) {
                if (errorDetector.isError(table, value, row, column)) {
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_ERROR_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_ERROR_FOREGROUND;
                } else if (isMandatory(table, value, row, column)){
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_MANDATORY_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_MANDATORY_FOREGROUND;
                } else {
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_FOREGROUND;
                }
            } else {
                if (errorDetector.isError(table, value, row, column)) {
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_ERROR_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_ERROR_FOREGROUND;
                } else if (isMandatory(table, value, row, column)){
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_MANDATORY_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_MANDATORY_FOREGROUND;
                } else {
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_PAIR_FOREGROUND;
                }
            }
        } else {
            if (isSelected) {
                if (errorDetector.isError(table, value, row, column)) {
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_SELECTED_ERROR_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_SELECTED_ERROR_FOREGROUND;
                } else if (isMandatory(table, value, row, column)){
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_SELECTED_MANDATORY_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_SELECTED_MANDATORY_FOREGROUND;
                } else {
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_SELECTED_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_SELECTED_FOREGROUND;
                }
            } else {
                if (errorDetector.isError(table, value, row, column)) {
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_ERROR_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_ERROR_FOREGROUND;
                } else if (isMandatory(table, value, row, column)){
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_MANDATORY_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_MANDATORY_FOREGROUND;
                } else {
                    backgroundOption = LimaSwingConfig.Option.TABLE_CELL_BACKGROUND;
                    foreGroundOption = LimaSwingConfig.Option.TABLE_CELL_FOREGROUND;
                }
            }
        }

        myCell.setBackground(limaSwingConfig.getOptionAsColor(backgroundOption.getKey()));
        myCell.setForeground(limaSwingConfig.getOptionAsColor(foreGroundOption.getKey()));
        setValue(value);

        myCell.setText(text);

        return myCell;
    }

    protected void setValue(Object value) {
        this.text = (value == null) ? "" : value.toString();
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    protected boolean isMandatory(JTable table, Object value, int row, int column) {
        return mandatory && table.isCellEditable(row, column)
                && (value == null
                || (value instanceof String) && ((String) value).isEmpty());
    }

    public TableCellErrorDetector getError() {
        return errorDetector;
    }

    public void setErrorDetector(TableCellErrorDetector errorDetector) {
        this.errorDetector = errorDetector;
    }



}
