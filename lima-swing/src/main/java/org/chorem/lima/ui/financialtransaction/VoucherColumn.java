package org.chorem.lima.ui.financialtransaction;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.Entry;
import org.chorem.lima.ui.celleditor.AutoCompleteTableCellEditor;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class VoucherColumn extends AbstractColumn<FinancialTransactionTableModel> {

    public VoucherColumn() {
        super(String.class, t("lima.financialTransaction.voucher"), true);
        setCellEditor(new AutoCompleteTableCellEditor("lima.financialTransaction.voucher"));
    }

    @Override
    public Object getValueAt(int row) {
        Entry entry = tableModel.get(row);
        return entry.getVoucher();
    }

    @Override
    public boolean setValueAt(Object value, int row) {
        Entry entry = tableModel.get(row);
        String voucher = ((String) value).trim();

        boolean update = (entry.getVoucher() == null || entry.getVoucher().compareTo(voucher) != 0);
        if (update) {
            String previousVoucher = entry.getVoucher();
            entry.setVoucher(voucher);
            if (!tableModel.updateEntry(entry)) {
                entry.setVoucher(previousVoucher);
            }
        }
        return update;
    }
}
