package org.chorem.lima.ui.Filter.BigDecimalCondition;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.AbstractBigDecimalCondition;
import org.chorem.lima.beans.Condition;
import org.chorem.lima.beans.DebitCondition;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;

import java.awt.event.ItemEvent;
import java.math.BigDecimal;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class BigDecimalConditionHandler implements ConditionHandler {

    protected BigDecimalConditionView view;

    protected AbstractBigDecimalCondition condition;

    protected FinancialTransactionConditionHandler filterHandler;

    public BigDecimalConditionHandler(BigDecimalConditionView view) {
        this.view = view;
        this.condition = new DebitCondition();
    }

    public BigDecimal getValue() {
        return condition.getValue();
    }

    public void setValue(BigDecimal value) {
        condition.setValue(value);
    }

    public void setOperand(AbstractBigDecimalCondition.Operand operand) {
        condition.setOperand(operand);
    }

    public AbstractBigDecimalCondition.Operand getOperand() {
        return condition.getOperand();
    }

    public void delete() {
        filterHandler.removeCondition(this);
    }

    public void operandSelected(ItemEvent event) {
        condition.setOperand((AbstractBigDecimalCondition.Operand) event.getItem());
    }

    @Override
    public Condition getCondition() {
        return condition;
    }

    @Override
    public BigDecimalConditionView getView() {
        return view;
    }

    @Override
    public void setFilterHandler(FinancialTransactionConditionHandler filterHandler) {
        this.filterHandler = filterHandler;
    }
}
