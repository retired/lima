/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialtransaction;

import com.google.common.collect.Maps;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.NoFiscalPeriodFoundException;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionImpl;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.enums.MainUiRefreshComponent;
import org.chorem.lima.ui.MainView;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;
import org.chorem.lima.util.BigDecimalToString;
import org.chorem.lima.util.ErrorHelper;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler associated with financial transaction view.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class FinancialTransactionViewHandler implements ServiceListener, TableModelListener, TableColumnModelListener {

    private static final Log log = LogFactory.getLog(FinancialTransactionViewHandler.class);

    /** Managed view. */
    protected FinancialTransactionView view;

    /** Cache object used to copy & paste. */
    protected Object clipBoard;

    protected EntryBookService entryBookService;
    protected FinancialPeriodService financialPeriodService;
    protected FiscalPeriodService fiscalPeriodService;
    protected FinancialTransactionService financialTransactionService;
    protected Date lastAssignDate;

    protected ErrorHelper errorHelper;

    protected boolean initializationComplete;

    protected boolean isLock = false;

    public FinancialTransactionViewHandler(FinancialTransactionView view) {
        this.view = view;
        initializationComplete = false;
        entryBookService = LimaServiceFactory.getService(EntryBookService.class);
        financialPeriodService = LimaServiceFactory.getService(FinancialPeriodService.class);
        fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);
        financialTransactionService = LimaServiceFactory.getService(FinancialTransactionService.class);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
        initShortCuts();
    }

    /**
     * Init all combo box in view.
     */
    public void init() {

        // fiscal periods
        List<FiscalPeriod> fiscalPeriods = fiscalPeriodService.getAllUnblockedFiscalPeriods();

        view.getFiscalPeriodComboBoxModel().setObjects(fiscalPeriods);

        if (log.isDebugEnabled()) {
            log.debug(String.format("Loaded %d fiscal periods", fiscalPeriods.size()));
        }
        if (!fiscalPeriods.isEmpty()) {
            view.getFiscalPeriodComboBoxModel().setSelectedItem(fiscalPeriods.get(0));
        }

        // entry books
        final List<EntryBook> allEntryBooks = entryBookService.getAllEntryBooks();
        view.getEntryBookComboBoxModel().setObjects(allEntryBooks);

        if (log.isDebugEnabled()) {
            log.debug(String.format("Loaded %d entry books", allEntryBooks.size()));
        }
        if (!allEntryBooks.isEmpty()) {
            view.getEntryBookComboBoxModel().setSelectedItem(allEntryBooks.get(0));
        }

        initializationComplete = true;

        updateFinancialTransactions();
        computePeriodStatusText();

        setAddEntryAllowed();

        addListeners();

    }

    protected void addListeners() {
        LimaServiceFactory.addServiceListener(FinancialPeriodService.class, this);
        LimaServiceFactory.addServiceListener(FiscalPeriodService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);

        FinancialTransactionTable table = view.getFinancialTransactionTable();
        table.getColumnModel().addColumnModelListener(this);
        SwingUtil.fixTableColumnWidth(table, 0, 40);

        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
        tableModel.addTableModelListener(this);

        MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);

        tableModel.addTableModelListener(e -> {

            boolean mustRecompute = e.getType() == TableModelEvent.DELETE
                    || e.getType() == TableModelEvent.INSERT
                    || (e.getType() == TableModelEvent.UPDATE &&
                    (e.getColumn() == TableModelEvent.ALL_COLUMNS || e.getColumn() == 4 || e.getColumn() == 5));

            if (mustRecompute) {
                computeBalanceStatusText();
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FINANCIAL_TRANSACTION_PANE);
            }

        });
    }

    protected void initShortCuts() {

        InputMap inputMap= view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();
        Object binding;

        // add action on Ctrl + Shift + N
        binding = "new-transaction";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -1664378625712266838L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addFinancialTransaction();
            }
        });

        // add action on Ctrl + Shift + Delete
        binding = "delete-transaction";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 7621389371928432410L;

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSelectedTransaction();
            }
        });

        // add action on Ctrl + Shift + C
        binding = "copy-transaction";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -982724612254422140L;

            @Override
            public void actionPerformed(ActionEvent e) {
                copyTransaction();
            }
        });

        // add action on Ctrl + Shift + V
        binding = "paste-transaction";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 3981745985282030673L;

            @Override
            public void actionPerformed(ActionEvent e) {
                pasteTransaction();
            }
        });

        // add action on Ctrl + N
        binding = "new-entry";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 6099897253355058899L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addEntry();
            }
        });

        // add action on Ctrl + Delete
        binding = "delete-entry";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 5137178343047269716L;

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSelectedEntry();
            }
        });

        // add action on Ctrl + Alt + C
        binding = "copy-entry";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK + KeyEvent.ALT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -1866038141590067940L;

            @Override
            public void actionPerformed(ActionEvent e) {
                copyEntry();
            }
        });

        // add action on Ctrl + Alt + V
        binding = "paste-entry";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK + KeyEvent.ALT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -3996645480884548643L;

            @Override
            public void actionPerformed(ActionEvent e) {
                pasteEntry();
            }
        });

        // add action on Ctrl + B
        binding = "balance";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 5070872946843797639L;

            @Override
            public void actionPerformed(ActionEvent e) {
                balanceTransaction();
            }
        });

        // add action on Ctrl + Alt + A
        binding = "assign-all";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK + KeyEvent.ALT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 4993025324310374283L;

            @Override
            public void actionPerformed(ActionEvent e) {
                assignAllEntries();
            }
        });
    }

    /**
     * New fiscal period selection.
     *
     * @param event event
     */
    public void fiscalPeriodSelected(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            FiscalPeriod selectedFiscalPeriod = (FiscalPeriod) event.getItem();

            List<FinancialPeriod> financialPeriods = financialPeriodService.getFinancialPeriodsWithBeginDateWithin(selectedFiscalPeriod.getBeginDate(),
                    selectedFiscalPeriod.getEndDate());

            if (log.isDebugEnabled()) {
                log.debug(String.format("Loaded %d financial periods", financialPeriods.size()));
            }
            view.getFinancialPeriodComboBoxModel().setObjects(financialPeriods);
            view.getFinancialPeriodComboBoxModel().setSelectedItem(financialPeriods.get(0));
            // this instruction triggers updateFinancialTransaction();
        }
    }

    /**
     * New financial period selection.
     *
     * @param event event
     */
    public void financialPeriodSelected(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            updateFinancialTransactions();
            computePeriodStatusText();
            setAddEntryAllowed();
        }
    }

    /**
     * New entry book selected.
     *
     * @param event event
     */
    public void entryBookSelected(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            updateFinancialTransactions();
            computePeriodStatusText();
            setAddEntryAllowed();
        }
    }

    /**
     * Fill transaction for selection fiscal period, financial perdiod
     * and entry book.
     */
    public void updateFinancialTransactions() {
        if (view.getEntryBookComboBoxModel().getSelectedItem() != null &&
                view.getFiscalPeriodComboBoxModel().getSelectedItem() != null &&
                view.getFinancialPeriodComboBox().getSelectedItem() != null &&
                initializationComplete) {

            FinancialTransactionTable table = view.getFinancialTransactionTable();
            table.exit();

            FinancialPeriod financialPeriod = (FinancialPeriod) view.getFinancialPeriodComboBox().getSelectedItem();
            EntryBook entryBook = (EntryBook) view.getEntryBookComboBox().getSelectedItem();

            List<FinancialTransaction> transactions = financialTransactionService.getAllFinancialTransactions(financialPeriod, entryBook);

            FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();

            isLock = isTableLock(financialPeriod, entryBook);
            tableModel.setGlobalyLocked(isLock);

            tableModel.setTransactions(transactions);

            if (table.isEditable() == isLock) {
                table.setEditable(!isLock);
            }

        }
    }

    protected boolean isTableLock(FinancialPeriod financialPeriod, EntryBook entryBook) {
        boolean result = false;
        if (financialPeriod != null && entryBook != null) {
            ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriod);
            result = closedPeriodicEntryBook == null || closedPeriodicEntryBook.isLocked();
            if (log.isDebugEnabled()) {
                log.debug("Locked:" + result);
            }
        }
        return result;
    }

    public void selectionChanged() {
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
        int selectedRow = table.getSelectedRow();
        view.setSelectedRow(selectedRow >= 0 && selectedRow < tableModel.size());
        if (selectedRow >= 0 && selectedRow < tableModel.size())  {
            FinancialTransaction transaction = tableModel.getTransactionAt(selectedRow);
            BigDecimal credit = transaction.getAmountCredit();
            BigDecimal debit = transaction.getAmountDebit();
            view.setBalance(credit.compareTo(debit) ==  0);
        } else {
            view.setBalance(true);
        }
    }

    public void balanceTransaction()  {
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
        FinancialTransactionTable table = view.getFinancialTransactionTable();

        int rowSelected = table.getSelectedRow();
        if (rowSelected != -1) {
                if (table.isEditing()) {
                TableCellEditor editor = table.getCellEditor();
                editor.stopCellEditing();
            }
            Entry entry = tableModel.get(rowSelected);
            FinancialTransaction transaction = entry.getFinancialTransaction();
            BigDecimal credit = transaction.getAmountCredit();
            BigDecimal debit = transaction.getAmountDebit();
            if (entry.isDebit()) {
                debit = debit.subtract(entry.getAmount());
            } else {
                credit = credit.subtract(entry.getAmount());
            }

            BigDecimal balance = credit.subtract(debit);

            BigDecimal previousAmount = entry.getAmount();
            boolean previousDebit = entry.isDebit();
            entry.setAmount(balance.abs());
            entry.setDebit(balance.signum() > 0);
            if (tableModel.updateEntry(entry)) {
                int firstRow = tableModel.indexOf(transaction);
                int lastRow = firstRow + transaction.sizeEntry() - 1;
                tableModel.fireTableRowsUpdated(firstRow, lastRow);
                view.setBalance(true);
            } else {
                entry.setAmount(previousAmount);
                entry.setDebit(previousDebit);
            }
        }
    }

    /**
     * Copy selected Transaction
     *
     */
    public void copyTransaction(){
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow != -1) {
            clipBoard = view.getFinancialTransactionTableModel().getTransactionAt(indexSelectedRow);
            view.setTransactionInClipBoard(true);
            view.setEntryInClipBoard(false);
        }
    }

    /**
     * Copy selected Entry
     *
     */
    public void copyEntry(){
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow != -1) {
        clipBoard = view.getFinancialTransactionTableModel().get(indexSelectedRow);
        view.setTransactionInClipBoard(false);
        view.setEntryInClipBoard(true);
        }
    }

    /**
     * Paste Transaction.
     *
     */
    public void pasteTransaction(){
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        //select the new line
        ListSelectionModel selectionModel = table.getSelectionModel();

        if (clipBoard != null && clipBoard instanceof FinancialTransaction) {
            try {
                FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
                FinancialTransaction transaction = (FinancialTransaction) clipBoard;

                EntryBook entryBook = (EntryBook) view.getEntryBookComboBox().getSelectedItem();

                transaction.setEntryBook(entryBook);
                transaction.setTransactionDate(getUseDate());

                tableModel.addTransaction(transaction);

                if (table.isEditing()) {
                    TableCellEditor editor = table.getCellEditor();
                    editor.stopCellEditing();
                }
                int index = tableModel.indexOf(transaction);
                selectionModel.setSelectionInterval(index, index);
                table.changeSelection(index, 1, false, false);
                table.requestFocus();
                table.scrollRowToVisible(Math.max(0, index - 5));
                table.scrollRowToVisible(Math.min(tableModel.size() - 1, index + transaction.sizeEntry() + 5));
            } catch (LockedFinancialPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.paste.transaction.error.lockedFinancialPeriod",
                        e.getFinancialPeriod().getBeginDate(),
                        e.getFinancialPeriod().getEndDate()));
            } catch (AfterLastFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.paste.transaction.error.afterLastFiscalPeriod",
                        e.getDate()));
            } catch (BeforeFirstFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.paste.transaction.error.beforeFirstFiscalPeriod",
                        e.getDate()));
            } catch (LockedEntryBookException e) {
                errorHelper.showErrorMessage(t("lima.entries.paste.transaction.error.lockedEntryBook",
                        e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                        e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
            } catch (NoFiscalPeriodFoundException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.transaction.error.noFiscalPeriodExistException"));
            }
        }
    }

    /**
     * Paste row.
     *
     */
    public void pasteEntry(){

        FinancialTransactionTable table = view.getFinancialTransactionTable();
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
        //select the new line
        ListSelectionModel selectionModel = table.getSelectionModel();
        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow != -1 && clipBoard != null && clipBoard instanceof Entry) {
            try {
                Entry entryTmp = (Entry) clipBoard;
                FinancialTransaction transaction = tableModel.getTransactionAt(indexSelectedRow);
                Entry entry = new EntryImpl();
                entry.setFinancialTransaction(transaction);
                entry.setVoucher(entryTmp.getVoucher());
                entry.setAccount(entryTmp.getAccount());
                entry.setDescription(entryTmp.getDescription());
                entry.setAmount(entryTmp.getAmount());
                entry.setDebit(entryTmp.isDebit());

                if (table.isEditing()) {
                    TableCellEditor editor = table.getCellEditor();
                    editor.stopCellEditing();
                }

                tableModel.addEntry(entry);

                int index = tableModel.indexOf(transaction) + transaction.sizeEntry() - 1;
                selectionModel.setSelectionInterval(index, index);
                //focus on second column
                table.changeSelection(index, 1, false, false);
                table.requestFocus();
            } catch (LockedFinancialPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.paste.entry.error.lockedFinancialPeriod",
                        e.getFinancialPeriod().getBeginDate(),
                        e.getFinancialPeriod().getEndDate()));
            } catch (AfterLastFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.paste.entry.error.afterLastFiscalPeriod",
                        e.getDate()));
            } catch (BeforeFirstFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.paste.entry.error.beforeFirstFiscalPeriod",
                        e.getDate()));
            } catch (LockedEntryBookException e) {
                errorHelper.showErrorMessage(t("lima.entries.paste.entry.error.lockedEntryBook",
                        e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                        e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
            }
        }
    }

    /**
     * Add new transaction.
     */
    public void addFinancialTransaction() {
        try {
            EntryBook entryBook = (EntryBook) view.getEntryBookComboBox().getSelectedItem();
            FinancialTransactionTable table = view.getFinancialTransactionTable();
            FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();

            FinancialTransaction financialTransaction = new FinancialTransactionImpl();
            // can be defined later by user
            if (entryBook != null) {
                financialTransaction.setEntryBook(entryBook);
            }

            // set date to the financial transaction
            financialTransaction.setTransactionDate(getUseDate());
            // create it
            financialTransaction = tableModel.addTransaction(financialTransaction);

            int addIndex = tableModel.indexOf(financialTransaction);

            //select the new line
            if (table.isEditing()) {
                TableCellEditor editor = table.getCellEditor();
                editor.stopCellEditing();
            }
            ListSelectionModel selectionModel = table.getSelectionModel();
            selectionModel.setSelectionInterval(addIndex, addIndex);
            // set focus on date
            table.changeSelection(addIndex, 0, false, false);
            table.requestFocus();
            table.scrollRowToVisible(Math.max(0, addIndex - 5));
            table.scrollRowToVisible(Math.min(tableModel.size() - 1, addIndex + 5));
        } catch (LockedFinancialPeriodException e) {
            errorHelper.showErrorMessage(t("lima.entries.add.transaction.error.lockedFinancialPeriod",
                    e.getFinancialPeriod().getBeginDate(),
                    e.getFinancialPeriod().getEndDate()));
        } catch (AfterLastFiscalPeriodException e) {
            errorHelper.showErrorMessage(t("lima.entries.add.transaction.error.afterLastFiscalPeriod",
                    e.getDate()));
        } catch (BeforeFirstFiscalPeriodException e) {
            errorHelper.showErrorMessage(t("lima.entries.add.transaction.error.beforeFirstFiscalPeriod",
                    e.getDate()));
        } catch (LockedEntryBookException e) {
            errorHelper.showErrorMessage(t("lima.entries.add.transaction.error.lockedEntryBook",
                    e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                    e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                    e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                    e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
        } catch (NoFiscalPeriodFoundException e) {
            errorHelper.showErrorMessage(t("lima.entries.add.transaction.error.noFiscalPeriodExistException"));
        }
    }

    /**
     * Add new entry to selected transaction.
     */
    public void addEntry() {

        FinancialTransactionTable table = view.getFinancialTransactionTable();
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();

        // FIXME echatellier 20120413 what is excatlty selected row here ?
        // real selected row or selected transaction row ?
        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow != -1) {
            TableCellEditor cellEditor = table.getCellEditor();
            if (cellEditor != null) {
                cellEditor.cancelCellEditing();
            }

            try {
                //First case, where line selected is an entry : take its transaction
                //to select the last entry of the transaction
                Entry entryAt = tableModel.get(indexSelectedRow);
                FinancialTransaction transaction = entryAt.getFinancialTransaction();
                int rowLasEntry = tableModel.indexOf(transaction) + transaction.sizeEntry() - 1;
                Entry lastEntry = tableModel.get(rowLasEntry);

                if (log.isDebugEnabled()) {
                    log.debug("Selected row : " + indexSelectedRow);
                }

                String defaultVoucher = null;
                String defaultDescription = null;

                if (lastEntry != null) {
                    defaultVoucher = lastEntry.getVoucher();
                    defaultDescription = lastEntry.getDescription();
                }

                // creates the new entry
                Entry entry = new EntryImpl();
                entry.setVoucher(defaultVoucher);
                if (log.isDebugEnabled()) {
                    log.debug("Description of precedent entry : " + defaultDescription);
                }
                entry.setDescription(defaultDescription);

                String defaultAccount;
                //TODO dcosse pas de valeur en dur
                //Actual (2017) tva percentage : 20%
                BigDecimal tvaPercentAdd = BigDecimal.valueOf(0.2);
                BigDecimal tvaTax = null;

                //Calculation of tva tax only if first entry is a sale
                if (lastEntry != null && lastEntry.getAccount() != null) {
                    defaultAccount = lastEntry.getAccount().getAccountNumber();
                    if ("410".equals(defaultAccount) || "418".equals(defaultAccount)) {
                        tvaTax = (lastEntry.getAmount()).multiply(tvaPercentAdd);
                    }
                }

                //Set tva tax on the debit of the new entry
                if (tvaTax != null) {
                    entry.setAmount(tvaTax);
                    entry.setDebit(true);
                }

                if (log.isInfoEnabled()) {
                    log.info("tva : " + tvaTax);
                }

                BigDecimal credit = transaction.getAmountCredit();
                BigDecimal debit = transaction.getAmountDebit();
                entry.setDebit(credit.compareTo(debit) > 0);
                entry.setAmount(credit.subtract(debit).abs());

                entry.setFinancialTransaction(transaction);

                // create it in service
                tableModel.addEntry(entry);

                //select the new line
                int row = rowLasEntry + 1;
                ListSelectionModel selectionModel = table.getSelectionModel();
                selectionModel.setSelectionInterval(
                        row, row);
                // set focus on voucher
                table.changeSelection(row, 1, false, false);
                table.requestFocus();
            } catch (LockedFinancialPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.lockedFinancialPeriod",
                        e.getFinancialPeriod().getBeginDate(),
                        e.getFinancialPeriod().getEndDate()));
            } catch (AfterLastFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.afterLastFiscalPeriod",
                        e.getDate()));
            } catch (BeforeFirstFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.beforeFirstFiscalPeriod",
                        e.getDate()));
            } catch (LockedEntryBookException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.lockedEntryBook",
                        e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                        e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Call addentry selected transaction without selection");
            }
        }
    }

    public void deleteSelectedTransaction() {
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
        ListSelectionModel selectionModel = table.getSelectionModel();

        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow >= 0) {
            TableCellEditor cellEditor = table.getCellEditor();
            if (cellEditor != null) {
                cellEditor.cancelCellEditing();
            }
            int response = JOptionPane.showConfirmDialog(
                    view, t("lima.entries.remove.transaction.confirm"),
                    t("lima.entries.remove.transaction.title"), JOptionPane.YES_NO_OPTION);

            if (response == JOptionPane.YES_OPTION) {
                try {
                    tableModel.removeTransaction(indexSelectedRow);

                    if (tableModel.size() > 0) {
                        if (indexSelectedRow >= tableModel.size()) {
                            indexSelectedRow = tableModel.size() - 1;
                        }
                        selectionModel.setSelectionInterval(
                                indexSelectedRow, indexSelectedRow);
                        table.changeSelection(indexSelectedRow, 1, false, false);
                        table.requestFocus();
                    }
                } catch (LockedFinancialPeriodException e) {
                    errorHelper.showErrorMessage(t("lima.entries.remove.transaction.error.lockedFinancialPeriod",
                            e.getFinancialPeriod().getBeginDate(),
                            e.getFinancialPeriod().getEndDate()));
                } catch (LockedEntryBookException e) {
                    errorHelper.showErrorMessage(t("lima.entries.remove.transaction.error.lockedEntryBook",
                            e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                            e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                            e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                            e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
                }

            }

        }
    }

    public void deleteSelectedEntry() {
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
        ListSelectionModel selectionModel = table.getSelectionModel();

        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow >= 0) {
            TableCellEditor cellEditor = table.getCellEditor();
            if (cellEditor != null) {
                cellEditor.cancelCellEditing();
            }
            int response = JOptionPane.showConfirmDialog(
                    view, t("lima.entries.remove.entry.confirm"),
                    t("lima.entries.remove.entry.title"), JOptionPane.YES_NO_OPTION);

            if (response == JOptionPane.YES_OPTION) {
                try {
                    tableModel.removeEntry(indexSelectedRow);

                    if (tableModel.size() > 0) {
                        if (indexSelectedRow >= tableModel.size()) {
                            indexSelectedRow = tableModel.size() - 1;
                        }
                        selectionModel.setSelectionInterval(
                                indexSelectedRow, indexSelectedRow);
                        table.changeSelection(indexSelectedRow, 1, false, false);
                        table.requestFocus();
                    }
                } catch (LockedFinancialPeriodException e) {
                    errorHelper.showErrorMessage(t("lima.entries.remove.entry.error.lockedFinancialPeriod",
                            e.getFinancialPeriod().getBeginDate(),
                            e.getFinancialPeriod().getEndDate()));
                } catch (LockedEntryBookException e) {
                    errorHelper.showErrorMessage(t("lima.entries.remove.entry.error.lockedEntryBook",
                            e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                            e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                            e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                            e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
                }
            }
        }
    }

    public void assignAllEntries() {
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();

        table.exit();

        int selectedRow = table.getSelectedRow();
        int selectedColumn = table.getSelectedColumn();
        if (selectedRow >= 0 && (selectedColumn == 1 || selectedColumn == 3)) {

            Entry entrySource = tableModel.get(selectedRow);
            FinancialTransaction transaction = entrySource.getFinancialTransaction();

            Map<Entry, String> previousValuesMap = Maps.newHashMap();
            try {
                for (Entry entryTarget : transaction.getEntry()) {
                    if (entrySource != entryTarget) {
                        switch (selectedColumn) {
                            case 1:
                                if (!StringUtils.equals(entryTarget.getVoucher(), entrySource.getVoucher())) {
                                    previousValuesMap.put(entryTarget, entryTarget.getVoucher());
                                    entryTarget.setVoucher(entrySource.getVoucher());
                                    financialTransactionService.updateEntry(entryTarget);
                                }
                                break;
                            case 3:
                                if (!StringUtils.equals(entryTarget.getDescription(), entrySource.getDescription())) {
                                    previousValuesMap.put(entryTarget, entryTarget.getDescription());
                                    entryTarget.setDescription(entrySource.getDescription());
                                    financialTransactionService.updateEntry(entryTarget);
                                }
                                break;
                        }
                    }
                }
                int firstRow = tableModel.indexOf(transaction);
                int lastRow = firstRow + transaction.sizeEntry() - 1;
                tableModel.fireTableRowsUpdated(firstRow, lastRow);
            } catch (LockedEntryBookException e) {
                errorHelper.showErrorMessage(t("lima.entries.assign.error.lockedEntryBook",
                        e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                        e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
                // restaure prévious values
                for (Map.Entry<Entry, String> mapEntry : previousValuesMap.entrySet()) {
                    Entry entry = mapEntry.getKey();
                    String previousValue = mapEntry.getValue();
                    switch (selectedColumn) {
                        case 1:
                            entry.setVoucher(previousValue);
                            break;
                        case 3:
                            entry.setDescription(previousValue);
                            break;
                    }
                }
            } catch (LockedFinancialPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.assign.error.lockedFinancialPeriod",
                        e.getFinancialPeriod().getBeginDate(),
                        e.getFinancialPeriod().getEndDate()));
                // restaure prévious values
                for (Map.Entry<Entry, String> mapEntry : previousValuesMap.entrySet()) {
                    Entry entry = mapEntry.getKey();
                    String previousValue = mapEntry.getValue();
                    switch (selectedColumn) {
                        case 1:
                            entry.setVoucher(previousValue);
                            break;
                        case 3:
                            entry.setDescription(previousValue);
                            break;
                    }
                }
            }
        }
    }

    /**
     * Select previous value in combo box.
     *
     * @param comboBox combo box
     */
    public void back(JComboBox comboBox) {
        int row = comboBox.getSelectedIndex();

        if (row > 0) {
            comboBox.setSelectedIndex(row - 1);
        }
    }

    /**
     * Select next value in combo box.
     *
     * @param comboBox combo box
     */
    public void next(JComboBox comboBox) {
        int size = comboBox.getModel().getSize();
        int row = comboBox.getSelectedIndex();

        if (row < size - 1) {
            comboBox.setSelectedIndex(row + 1);
        }
    }

    /*
     * @see org.chorem.lima.business.ServiceListener#notifyMethod(java.lang.String, java.lang.String)
     */
    @Override
    public void notifyMethod(String serviceName, String methodName) {

        if (log.isDebugEnabled()) {
            log.debug("Method notified " + serviceName + "#" + methodName);
        }

        if (serviceName.contains("FinancialTransaction") ||
            methodName.contains("importEntries") ||
            methodName.contains("importAll")) {
            updateFinancialTransactions();
        }
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE
                && e.getColumn() == 0
                && e.getFirstRow() >= 0) {
            if (log.isDebugEnabled()) {
                log.debug("Date changed");
            }

            FinancialTransactionTable table = view.getFinancialTransactionTable();
            FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
            Entry entry = tableModel.get(e.getFirstRow());
            FinancialTransaction financialTransaction = entry.getFinancialTransaction();
            lastAssignDate = financialTransaction.getTransactionDate();

            tableModel.sort();
            int newRow = tableModel.indexOf(entry);
            table.changeSelection(newRow, 0, false, false);
        }
    }

    @Override
    public void columnAdded(TableColumnModelEvent e) {
        //nothing
    }

    @Override
    public void columnRemoved(TableColumnModelEvent e) {
        //nothing
    }

    @Override
    public void columnMoved(TableColumnModelEvent e) {
        //nothing
    }

    @Override
    public void columnMarginChanged(ChangeEvent e) {
        //nothing
    }

    @Override
    public void columnSelectionChanged(ListSelectionEvent e) {
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();

        int selectedRow = table.getSelectedRow();
        int selectedColumn = table.getSelectedColumn();
        if (selectedRow >= 0 && selectedRow < tableModel.size())  {
            FinancialTransaction transaction = tableModel.getTransactionAt(selectedRow);
            view.setAssignableInAllEntries(transaction.sizeEntry() > 1 && (selectedColumn == 1 || selectedColumn == 3));
        } else {
            view.setAssignableInAllEntries(false);
        }
    }

    /**
     *
     * @return the last edited transaction, if none the selected transaction date, if none the begin date.
     */
    public Date getUseDate() throws NoFiscalPeriodFoundException {

        FinancialPeriod financialPeriod = (FinancialPeriod)view.getFinancialPeriodComboBox().getSelectedItem();

        if (financialPeriod != null) {
            Date beginDate = financialPeriod.getBeginDate();
            Date endDate = financialPeriod.getEndDate();

            if (lastAssignDate == null
                    || lastAssignDate.before(beginDate)
                    || lastAssignDate.after(endDate) ) {

                FinancialTransaction selectedTransaction = getSelectedFinancialTransaction();
                lastAssignDate = selectedTransaction == null ? beginDate : selectedTransaction.getTransactionDate();
            }
            
        } else {
            throw new NoFiscalPeriodFoundException();
        }


        return lastAssignDate;
    }

    protected FinancialTransaction getSelectedFinancialTransaction() {
        FinancialTransactionTable table = view.getFinancialTransactionTable();
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();
        int indexSelectedRow = table.getSelectedRow();
        FinancialTransaction selectedTransaction = null;
        if (indexSelectedRow != -1) {
            selectedTransaction = tableModel.getTransactionAt(indexSelectedRow);
        }
        return selectedTransaction;
    }

    protected void computePeriodStatusText() {

        if (initializationComplete) {

            FinancialPeriod period = (FinancialPeriod) view.getFinancialPeriodComboBoxModel().getSelectedItem();
            EntryBook entryBook = (EntryBook) view.getEntryBookComboBoxModel().getSelectedItem();

            String subText = "";
            if (period == null) {
                subText = t("lima.financialTransaction.statusTextPeriod.noFiscalPeriodFound");
            }
            if (entryBook == null) {
                String noEntryBookSubText = t("lima.financialTransaction.statusTextPeriod.noEntryBookFound");
                subText = subText.length() > 0 ? subText + "," + noEntryBookSubText : subText + noEntryBookSubText;
            }
            if (period != null && entryBook != null) {
                subText = subText + (isLock ? t("lima.financialTransaction.statusTextPeriod.close", period.getBeginDate(), period.getEndDate(), entryBook.getCode()) : t("lima.financialTransaction.statusTextPeriod.open", period.getBeginDate(), period.getEndDate()));
            }
            String result = String.format(t("lima.financialTransaction.statusTextPeriod"), subText);

            view.getPeriodStatusLabel().setText(result);
        }
    }


    protected void computeBalanceStatusText() {
        String result;
        FinancialTransactionTableModel tableModel = view.getFinancialTransactionTableModel();

        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
        BigDecimal balance;

        for (Entry entry : tableModel.getValues()) {

            BigDecimal amount = entry.getAmount();
            boolean debit = entry.isDebit();
            if (debit) {
                totalDebit = totalDebit.add(amount);
            } else {
                totalCredit = totalCredit.add(amount);
            }
        }

        balance = totalDebit.subtract(totalCredit);
        if (balance.compareTo(BigDecimal.ZERO) == 0) {
            result = t("lima.financialTransaction.statusTextDebitCredit", BigDecimalToString.format(totalDebit), BigDecimalToString.format(totalCredit), BigDecimalToString.format(balance));
        } else {
            result = t("lima.financialTransaction.statusTextDebitCreditError", BigDecimalToString.format(totalDebit), BigDecimalToString.format(totalCredit), BigDecimalToString.format(balance));
        }

        view.getBalanceStatusLabel().setText(result);

    }

    protected void setAddEntryAllowed() {
        view.setAddEntryAllowed(!isLock);
    }

}
