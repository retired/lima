/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.home;

import javax.swing.*;
import javax.swing.event.HyperlinkListener;
import javax.swing.plaf.basic.BasicEditorPaneUI;
import java.awt.*;

/**
 * Common code for all home page panel.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public abstract class AbstractHomePane extends JEditorPane implements HyperlinkListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = -7820679841428370136L;

    protected static final Color RED_BACKGROUND = new Color(0xff, 0xee, 0xee);

    protected static final Color GREEN_BACKGROUND = new Color(0xee, 0xff, 0xee);

    protected HomeView view;

    protected AbstractHomePane(HomeView view) {
        this.view = view;

        //init
        setOpaque(true);
        setContentType("text/html");
        setEditable(false);
        setUI(new BasicEditorPaneUI());
        addHyperlinkListener(this);
    }
}
