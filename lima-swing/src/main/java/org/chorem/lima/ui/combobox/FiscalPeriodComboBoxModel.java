/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.combobox;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.business.LimaServiceFactory;

import javax.swing.*;
import java.util.List;

public class FiscalPeriodComboBoxModel extends AbstractListModel implements ComboBoxModel, ServiceListener {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(FiscalPeriodComboBoxModel.class);

    protected Object selectedFiscalPeriod;

    protected FiscalPeriodService fiscalPeriodService;

    protected List<FiscalPeriod> datasCache;

    //constructor for blocked fiscal period only
    public FiscalPeriodComboBoxModel() {
        fiscalPeriodService =
                LimaServiceFactory.getService(
                        FiscalPeriodService.class);
        LimaServiceFactory.addServiceListener(FiscalPeriodService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);
        datasCache = getDataList();

        if (log.isDebugEnabled()) {
            log.debug("Size of datasCache : " + datasCache.size());
            log.debug("Beginning and end dates of first fiscal period in datasCache: " + ((FiscalPeriod)getElementAt(0)).getBeginDate() + ", " + ((FiscalPeriod)getElementAt(0)).getEndDate());
        }
    }

    @Override
    public int getSize() {
        return datasCache.size();
    }

    @Override
    public Object getElementAt(int index) {
        return datasCache.get(index);
    }


    @Override
    public void setSelectedItem(Object anItem) {

        if (log.isDebugEnabled()) {
            log.debug("Beginning and end dates of first fiscal period : " + ((FiscalPeriod) getElementAt(0)).getBeginDate() + ", " + ((FiscalPeriod) getElementAt(0)).getEndDate());
        }

        selectedFiscalPeriod = anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedFiscalPeriod;
    }

    //get unblocked fiscal periods data list
    public List<FiscalPeriod> getDataList() {
        List<FiscalPeriod> result = fiscalPeriodService.getAllUnblockedFiscalPeriods();

        if (log.isDebugEnabled()) {
            log.debug("Size of result : " + result.size());
            log.debug("Beginning and end dates of first fiscal period : " + result.get(0).getBeginDate() + ", " + result.get(0).getEndDate());
        }

        return result;

    }

    //get all fiscal periods data list
    public List<FiscalPeriod> getAllDataList() {
        List<FiscalPeriod> result = fiscalPeriodService.getAllFiscalPeriods();
        if (log.isDebugEnabled()) {
            log.debug("Size of result : " + result);
            log.debug("Beginning and end dates of first fiscal period : " + result.get(0).getBeginDate() + ", " + result.get(0).getEndDate());
        }

        return result;
    }

    public void refresh() {
        datasCache = getDataList();
        fireContentsChanged(this, 0, datasCache.size());
    }

    public void refreshAll() {
        datasCache = getAllDataList();
        fireContentsChanged(this, 0, datasCache.size());
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        if (serviceName.contains("FiscalPeriod") || methodName.contains("importAll")) {
            refresh();
        }
    }
}
