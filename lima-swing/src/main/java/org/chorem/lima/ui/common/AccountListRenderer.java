/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.common;

import org.chorem.lima.entity.Account;

import javax.swing.*;
import java.awt.*;

public class AccountListRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {

        JLabel label = (JLabel) super.getListCellRendererComponent(list,
                                                                   value,
                                                                   index,
                                                                   isSelected,
                                                                   cellHasFocus
        );
        if (value != null) {
            Account account = (Account) value;
            String accountLabel = account.getLabel();
            label.setText(account.getAccountNumber() + " - " + accountLabel);
        }
        return this;
    }

}
