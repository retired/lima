package org.chorem.lima.ui.celleditor;
/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.LimaSwingConfig;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;

/**
 * @author sletellier <letellier@codelutin.com>
 * @author mallon <mallon@codelutin.com>
 */
public class BigDecimalTableCellEditor extends StringTableCellEditor {

    private static final long serialVersionUID = -3178887048291878246L;
    protected String comma = "";

    private static final Log log = LogFactory.getLog(BigDecimalTableCellEditor.class);

    public BigDecimalTableCellEditor() {
        super();
        getComponent().setHorizontalAlignment(SwingConstants.RIGHT);
        getComponent().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                //nothing to do
            }

            @Override
            public void keyPressed(KeyEvent e) {
                //nothing to do
            }

            @Override
            public void keyReleased(KeyEvent e) {
                limitComma(e);
            }
        });
    }

    /**
     * Split decimal with scale config
     * @return bigDecimal value
     * */
    @Override
    public BigDecimal getCellEditorValue() {
        BigDecimal cellEditorValue;

        String stringValue = super.getCellEditorValue().toString();
        if (StringUtils.isBlank(stringValue)) {
            stringValue = "0";
        }
        stringValue = stringValue.replaceAll(",", ".");

        String valueToConvert;

        int pointIndex = stringValue.indexOf('.');
        if (pointIndex != -1) {
            LimaSwingConfig config = LimaSwingApplicationContext.getContext().getConfig();
            String actualDecimals = stringValue.substring(pointIndex, stringValue.length() - 1);
            if (config.getScale() > actualDecimals.length()) {
                valueToConvert = stringValue;
            } else {
                int decimalLength = config.getScale() + pointIndex + 1;
                String roundedStringValue = stringValue.substring(0, pointIndex) +
                                            stringValue.substring(pointIndex, decimalLength);
                valueToConvert = roundedStringValue;
            }
        } else {
            valueToConvert = stringValue;
        }
        try {

            cellEditorValue = new BigDecimal(valueToConvert);

        } catch (NumberFormatException e) {
            // it cancel current cell value
            // tchemit-2015-02-27 Can't have a null value (NPE in DebitColim.setValueAt)
            cellEditorValue = BigDecimal.ZERO;
        }
        return cellEditorValue;
    }

    /**
     * Limit number of decimalSeparator to one
     * @param e keyEvent starting control of decimalSeparator
     * */
    protected void limitComma(KeyEvent e) {
        if ( (String.valueOf(e.getKeyChar()).matches(",")  || String.valueOf(e.getKeyChar()).matches("\\."))
              &&  comma.equals(",")) {
            getComponent().setText(getComponent().getText().substring(0, getComponent().getText().length()-1));
        } else if( (String.valueOf(e.getKeyChar()).matches(",")  || String.valueOf(e.getKeyChar()).matches("\\."))
                &&  comma.equals("")) {
            comma = ",";
        } else if(!getComponent().getText().matches(",") || !getComponent().getText().matches("\\.") &&
                (!String.valueOf(e.getKeyChar()).matches(",")  || !String.valueOf(e.getKeyChar()).matches("\\."))) {
            comma = "";
        }
    }

}
