/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.identity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.TreasuryService;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.IdentityImpl;
import org.chorem.lima.entity.Treasury;

import static org.nuiton.i18n.I18n.t;

public class IdentityHandler {

    protected IdentityService identityService;

    protected TreasuryService treasuryService;

    protected IdentityForm view;

    private static final Log log = LogFactory.getLog(IdentityHandler.class);


    public IdentityHandler(IdentityForm view) {
        this.view = view;
        identityService = LimaServiceFactory.getService(IdentityService.class);
        treasuryService = LimaServiceFactory.getService(TreasuryService.class);
    }

    public Identity getIdentity() {
        Identity identity = identityService.getIdentity();
        if (identity == null) {
            identity = new IdentityImpl();
        }
        return identity;
    }

    public Treasury getTreasury() {
        Treasury treasury = treasuryService.getTreasury();
        return treasury;
    }

    public void updateIdentity(){
        Identity identity = getIdentity();

        identity.setName(view.getNameTextField().getText());
        identity.setDescription(view.getDescriptionTextField().getText());
        identity.setAddress(view.getAddressTextField().getText());
        identity.setZipCode(view.getZipCodeTextField().getText());
        identity.setCity(view.getCityTextField().getText());
        identity.setBusinessNumber(view.getBusinessNumberTextField().getText());
        identity.setClassificationCode(view.getClassificationCodeTextField().getText());
        identity.setPhoneNumber(view.getPhoneNumberTextField().getText());
        identity.setEmail(view.getEmailTextField().getText());
        identity.setVatNumber(view.getVatNumberTextField().getText());
        identityService.updateIdentity(identity);

        Treasury treasury = treasuryService.getTreasury();
        treasury.setAddress(view.getTreasuryAddressTextField().getText());
        treasury.setCdi(view.getTreasuryCdiTextField().getText());
        treasury.setCity(view.getTreasuryCityTextField().getText());
        treasury.setKey(view.getTreasuryKeyTextField().getText());
        treasury.setServiceCode(view.getTreasuryServiceCodeTextField().getText());
        treasury.setDossierNumber(view.getDossierNumberTextField().getText());
        treasury.setSie(view.getTreasurySieTextField().getText());
        treasury.setZipCode(view.getTreasuryZipCodeTextField().getText());
        treasury.setSystemType(view.getTreasurySystemTypeTextField().getText());

        treasuryService.updateTreasury(treasury);
    }

    public String getIdentityTabTitle() {
        String result = t("lima.identity");
        return result;
    }

    public String getTreasuryTabTitle() {
        String result = t("lima.treasury");
        return result;
    }
}
