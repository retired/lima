package org.chorem.lima.ui.celleditor;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class NumberSeparatorCellRenderer extends DefaultListCellRenderer {

    protected static final Map<Character, String> LABEL_BY_SEPARATOR = ImmutableMap.of(
            ' ', "lima.config.numberSeparator.space",
            ',', "lima.config.numberSeparator.comma",
            '.', "lima.config.numberSeparator.dot",
            ';', "lima.config.numberSeparator.semicolon");

    public static String getSeparatorLabel(Object value) {
        String separatorLabel = "";
        if (value != null) {
            Character separator = (Character) value;
            separatorLabel = LABEL_BY_SEPARATOR.get(separator);
            if (separatorLabel == null) {
                separatorLabel = "";
            } else {
                separatorLabel = t(separatorLabel);
            }
            separatorLabel += " \"" + separator + "\"";
        }
        return separatorLabel;
    }

    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {

        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        label.setText(getSeparatorLabel(value));
        return this;
    }
}
