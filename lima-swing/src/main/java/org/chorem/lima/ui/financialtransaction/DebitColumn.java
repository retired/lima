package org.chorem.lima.ui.financialtransaction;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.Entry;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;

import java.math.BigDecimal;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class DebitColumn extends AbstractColumn<FinancialTransactionTableModel> {

    public DebitColumn() {
        super(BigDecimal.class, t("lima.entry.debit"), true);
    }

    @Override
    public Object getValueAt(int row) {
        Entry entry = tableModel.get(row);
        return entry.isDebit() ? entry.getAmount() : BigDecimal.ZERO;
    }

    @Override
    public boolean setValueAt(Object value, int row) {
        Entry entry = tableModel.get(row);
        BigDecimal debit = (BigDecimal) value;
        boolean update = (debit.signum() > 0 && (!entry.isDebit() || entry.getAmount().compareTo(debit) != 0));
        if (update) {
            BigDecimal previousAmount = entry.getAmount();
            boolean previousDebit = entry.isDebit();
            entry.setAmount(debit);
            entry.setDebit(true);
            if(tableModel.updateEntry(entry)) {
                tableModel.fireTransaction(entry.getFinancialTransaction());
            } else {
                entry.setAmount(previousAmount);
                entry.setDebit(previousDebit);
                update = false;
            }
        }
        return update;
    }
}
