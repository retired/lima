package org.chorem.lima.ui.common;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public abstract class TableModelWithGroup<E> extends AbstractLimaTableModel<E> {

    private static final long serialVersionUID = 2349944168938847283L;

    public int indexInGroup(int row) {
        int index = 0;
        while (row-index > 0 && sameGroup(get(row), get(row-index-1)))  {
            index++;
        }
        return index;

    }

    public abstract boolean sameGroup(E e1, E e2);
}
