/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.accountViewer;

import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.beans.LetteringFilter;
import org.chorem.lima.beans.LetteringFilterImpl;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.ui.combobox.AccountComboBox;
import org.chorem.lima.ui.common.FinancialPeriodComboBoxModel;
import org.chorem.lima.ui.common.FiscalPeriodComboBoxModel;
import org.chorem.lima.util.BigDecimalToString;
import org.chorem.lima.util.ErrorHelper;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;


/**
 * Handler associated with financial transaction view.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class AccountViewerViewHandler {

    public static final String YEAR_DATE_FORMAT = "yyyy";

    protected AccountViewerView view;

    protected AccountService accountService;
    protected EntryBookService entryBookService;
    protected FinancialPeriodService financialPeriodService;
    protected FinancialTransactionService financialTransactionService;
    protected FiscalPeriodService fiscalPeriodService;

    protected AccountViewerTableModel accountViewerTableModel;
    protected AccountViewerSelectionModel accountViewerSelectionModel;
    protected AccountViewerEditModel editModel;
    protected FinancialPeriodComboBoxModel financialPeriodComboBoxModel;
    protected FiscalPeriodComboBoxModel fiscalPeriodComboBoxModel;

    protected ErrorHelper errorHelper;

    protected SelectionMode selectionMode = SelectionMode.LETTERED;

    private static final Log log = LogFactory.getLog(AccountViewerViewHandler.class);

    protected boolean initializationComplete;

    public AccountViewerViewHandler(AccountViewerView view) {

        initializationComplete = false;
        this.view = view;
        initShortCuts();

        accountService = LimaServiceFactory.getService(AccountService.class);
        entryBookService = LimaServiceFactory.getService(EntryBookService.class);
        financialPeriodService = LimaServiceFactory.getService(FinancialPeriodService.class);
        financialTransactionService = LimaServiceFactory.getService(FinancialTransactionService.class);
        fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);

        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
    }

    /**
     * Init all combo box in view.
     */
    public void init() {

        createComponentsModels();

        addModelsToComponents();

        initAccountTableView();

        addListeners();

        initFinancialPeriodDefaultComponent();

        initSoldPeriodView();

        initializationComplete = true;

        updateSoldStatus();
    }

    protected void createComponentsModels() {
        editModel = new AccountViewerEditModel();
        financialPeriodComboBoxModel = new FinancialPeriodComboBoxModel();
        fiscalPeriodComboBoxModel = new FiscalPeriodComboBoxModel();
        accountViewerTableModel = new AccountViewerTableModel();
        accountViewerSelectionModel = new AccountViewerSelectionModel(accountViewerTableModel);
    }

    protected void addModelsToComponents() {
        view.getFinancialPeriodComboBox().setModel(financialPeriodComboBoxModel);
        view.getFiscalPeriodComboBox().setModel(fiscalPeriodComboBoxModel);
        AccountViewerTable accountViewerTable = view.getTable();
        accountViewerTable.setModel(accountViewerTableModel);
        accountViewerTable.setSelectionModel(accountViewerSelectionModel);
    }

    protected void addListeners() {
        accountViewerSelectionModel.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                view.getHandler().balanceAndActions();
                updateSoldStatus();

            }
        });
    }

    protected void initAccountTableView() {
        AccountViewerTable accountViewerTable = view.getTable();
        SwingUtil.fixTableColumnWidth(accountViewerTable, 0, 100);
        SwingUtil.fixTableColumnWidth(accountViewerTable, 1, 60);
        SwingUtil.fixTableColumnWidth(accountViewerTable, 2, 80);
        SwingUtil.fixTableColumnWidth(accountViewerTable, 4, 50);
    }

    protected void initFinancialPeriodDefaultComponent() {
        updateFinancialPeriodView();
        onSelectionModeChanged(selectionMode);
    }

    protected void initShortCuts() {

        InputMap inputMap= view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();
        Object binding;

        //To block reaction of the dual key 'ctrl+a' (Selection of all lines)
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK), "none");

        // refresh
        binding = "refresh";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -7192846839712951680L;

            @Override
            public void actionPerformed(ActionEvent e) {
                updateAllEntries();
            }
        });
    }

    public void onAccountChange(PropertyChangeEvent event) {
        if (initializationComplete) {
            if (event.getPropertyName().equals(AccountComboBox.PROPERTY_SELECTED_ITEM) && event.getNewValue() != null && event.getNewValue() instanceof Account) {
                setAccount((Account) event.getNewValue());
            }
        }
    }

    protected void onPeriodChoiceChanged(PeriodChoice periodChoice) {
        if (initializationComplete && !editModel.getPeriodChoice().equals(periodChoice)) {
            editModel.setPeriodChoice(periodChoice);
            switch (periodChoice) {
                case FINANCIAL_PERIOD:
                    initViewForPeriodChoiceChange(true, true, false);
                    updateAllEntriesForFinancialPeriodChoice();
                    break;
                case FISCAL_PERIOD:
                    initViewForPeriodChoiceChange(true, false, false);
                    updateAllEntriesForFiscalPeriodChoice();
                    break;
                case DATE:
                    initViewForPeriodChoiceChange(false, false, true);
                    updateAllEntriesForDateIntervalChoice();
                    break;
            }
        }
    }

    private void initViewForPeriodChoiceChange(boolean isFiscalPeriodComboBoxVisible, boolean isFinancialPeriodComboBoxVisible, boolean isPeriodPickersVisible) {
        view.getFiscalPeriodComboBox().setVisible(isFiscalPeriodComboBoxVisible);
        view.getFinancialPeriodComboBox().setVisible(isFinancialPeriodComboBoxVisible);
        view.getBeginPeriodPicker().setVisible(isPeriodPickersVisible);
        view.getEndPeriodPicker().setVisible(isPeriodPickersVisible);
    }

    private void updateAllEntriesForDateIntervalChoice() {
        initDateIntervalView();
        updateAllEntries();
    }

    private void updateAllEntriesForFiscalPeriodChoice() {
        initFiscalPeriodView();
        updateAllEntriesForFiscalPeriodChoice(fiscalPeriodService.getLastFiscalPeriod());
    }

    private void updateAllEntriesForFinancialPeriodChoice() {
        updateFinancialPeriodView();
        updateAllEntries();
    }

    /**
     * Select previous value in combo box.
     *
     * @param accountComboBox account combo box
     */
    public void onBackChange(AccountComboBox accountComboBox) {
        if (initializationComplete) {
            JComboBox comboBox = accountComboBox.getCombobox();
            int row = comboBox.getSelectedIndex();

            if (row > 0) {
                comboBox.setSelectedIndex(row - 1);
            }
            view.getTable().getSelectionModel().clearSelection();
        }
    }

    /**
     * Select next value in combo box.
     *
     * @param accountComboBox combo box
     */
    public void onNextChange(AccountComboBox accountComboBox) {
        if (initializationComplete) {
            JComboBox comboBox = accountComboBox.getCombobox();
            int size = comboBox.getItemCount();
            int row = comboBox.getSelectedIndex();

            if (row < size - 1) {
                comboBox.setSelectedIndex(row + 1);
            }
            view.getTable().getSelectionModel().clearSelection();
        }
    }

    protected void onFiscalPeriodChange(ItemEvent event) {
        FiscalPeriod selectedFiscalPeriod = event != null && event.getItem() != null ? (FiscalPeriod) event.getItem() : null;
        if (initializationComplete && selectedFiscalPeriod != null && !selectedFiscalPeriod.equals(editModel.getFiscalPeriod())) {
            if (PeriodChoice.FINANCIAL_PERIOD.equals(editModel.getPeriodChoice())) {
                List<FinancialPeriod> periods = financialPeriodService.getFinancialPeriodsWithBeginDateWithin(selectedFiscalPeriod.getBeginDate(), selectedFiscalPeriod.getEndDate());
                FinancialPeriod selectedFinancialPeriod = periods.get(0);
                financialPeriodComboBoxModel.setObjects(periods);
                financialPeriodComboBoxModel.setSelectedItem(selectedFinancialPeriod);

                setFilter(selectedFiscalPeriod, selectedFinancialPeriod, selectedFinancialPeriod.getBeginDate(), selectedFinancialPeriod.getEndDate(), PeriodChoice.FINANCIAL_PERIOD);
            } else {
                setFilter(selectedFiscalPeriod, null, selectedFiscalPeriod.getBeginDate(), selectedFiscalPeriod.getEndDate(), PeriodChoice.FISCAL_PERIOD);
            }
            updateAllEntries();
        }
    }

    protected void onFinancialPeriodChange(ItemEvent event) {
        FinancialPeriod financialPeriod = (FinancialPeriod) event.getItem();
        if (initializationComplete && editModel.getFinancialPeriod() == null || !editModel.getFinancialPeriod().equals(financialPeriod)) {
            updateAllEntriesForFinancialPeriodChoice(financialPeriod);
        }
    }

    public void onDateStartChange(Date date) {
        if (initializationComplete) {
            if (date != null && !DateUtils.isSameDay(date, editModel.getDateStart())) {
                editModel.setDateStart(date);

                if (editModel.getDateEnd().before(date)) {
                    editModel.setDateEnd(date);
                    view.getEndPeriodPicker().setDate(date);
                }
                updateAllEntriesForDateChange();
            }
        }
    }

    public void onDateEndChange(Date date) {
        if (initializationComplete) {
            if (date != null && !DateUtils.isSameDay(date, editModel.getDateEnd())) {
                editModel.setDateEnd(date);
                if (editModel.getDateStart().after(date)) {
                    editModel.setDateStart(date);
                    view.getBeginPeriodPicker().setDate(date);
                }
                updateAllEntriesForDateChange();
            }
        }
    }

    public void onSelectionModeChanged(SelectionMode selectionMode) {
        AccountViewerSelectionModel accountViewerSelectionModel = (AccountViewerSelectionModel) view.getTable().getSelectionModel();

        switch (selectionMode) {
            case LETTERED:
                accountViewerSelectionModel.setLetteredSelectionMode(true);
                break;
            case MANUAL:
                accountViewerSelectionModel.setLetteredSelectionMode(false);
                break;
            default:
                accountViewerSelectionModel.setLetteredSelectionMode(true);
        }
    }

    public void updateSoldStatus() {
        if (initializationComplete) {
            view.getSelectionDescription().setText(t("lima.accountViewer.selectionDescription"));
            view.getSelectionDebit().setText(BigDecimalToString.format(editModel.getDebit()));
            view.getSelectionCredit().setText(BigDecimalToString.format(editModel.getCredit()));
            view.getSelectionSold().setText(BigDecimalToString.format(editModel.getSold()));
        }
    }


    public void updateAllEntriesForFiscalPeriodChoice(FiscalPeriod fiscalPeriod) {
        if (fiscalPeriod != null) {
            setFilter(fiscalPeriod, null, fiscalPeriod.getBeginDate(), fiscalPeriod.getEndDate(), PeriodChoice.FISCAL_PERIOD);
            updateAllEntries();
        }
    }

    public void updateAllEntriesForFinancialPeriodChoice(FinancialPeriod financialPeriod) {
        setFilter(null, financialPeriod, financialPeriod.getBeginDate(), financialPeriod.getEndDate(), PeriodChoice.FINANCIAL_PERIOD);
        updateAllEntries();
    }

    protected void setFilter(FiscalPeriod fiscalPeriod, FinancialPeriod financialPeriod, Date dateStart, Date dateEnd, PeriodChoice periodChoice) {
        editModel.setFiscalPeriod(fiscalPeriod);
        editModel.setFinancialPeriod(financialPeriod);
        editModel.setDateStart(dateStart);
        editModel.setDateEnd(dateEnd);
        editModel.setPeriodChoice(periodChoice);
    }

    public void updateAllEntriesForDateChange() {
        editModel.setPeriodChoice(PeriodChoice.DATE);
        updateAllEntries();
    }

    public void updateAllEntries() {
        if (initializationComplete
                && editModel.getAccount() != null
                && editModel.getDateStart() != null
                && editModel.getDateEnd() != null) {
            LetteringFilter filter = new LetteringFilterImpl();
            filter.setAccount(editModel.getAccount());
            filter.setDateStart(editModel.getDateStart());
            filter.setDateEnd(editModel.getDateEnd());
            List<Entry> entries = financialTransactionService.getAllEntrieByDatesAndAccountAndLettering(filter);
            AccountViewerTableModel accountViewerTableModel = (AccountViewerTableModel) view.getTable().getModel();
            accountViewerTableModel.setValues(entries);

            updateSummary();
        }

        onBalanceChanged(null);
    }

    protected void updateSummary() {
        LetteringFilter previousPeriodFilter = computePreviousPeriodFilter();
        LetteringFilter actualPeriodFilter = computeActualPeriodFilter();
        setViewPreviousSold(previousPeriodFilter);
        setViewActualSold(actualPeriodFilter);
    }

    protected LetteringFilter computePreviousPeriodFilter() {
        LetteringFilter previousPeriodFilter = null;
        FiscalPeriod previousFiscalPeriod;
        Date dateStart = null, dateEnd = null;
        switch (editModel.getPeriodChoice()) {
            case FINANCIAL_PERIOD:
                Date financialPeriodDate = editModel.getFinancialPeriod().getBeginDate();
                FiscalPeriod fiscalPeriodFiscalPeriod = fiscalPeriodService.getFiscalPeriodForDate(financialPeriodDate);

                if (DateUtils.isSameDay(financialPeriodDate, fiscalPeriodFiscalPeriod.getBeginDate())) {
                    fiscalPeriodFiscalPeriod = fiscalPeriodService.getFiscalPeriodForDate(getDayMinus1Calendar(financialPeriodDate).getTime());
                }

                dateStart = fiscalPeriodFiscalPeriod != null ? fiscalPeriodFiscalPeriod.getBeginDate() : null;
                dateEnd = getDayMinus1Calendar(financialPeriodDate).getTime();
                break;
            case FISCAL_PERIOD:
                previousFiscalPeriod = fiscalPeriodService.getFiscalPeriodForDate(getDayMinus1Calendar(editModel.getFiscalPeriod().getBeginDate()).getTime());
                if (previousFiscalPeriod != null) {
                    dateStart = previousFiscalPeriod.getBeginDate();
                    dateEnd = previousFiscalPeriod.getEndDate();
                }
                break;
            case DATE:
                previousFiscalPeriod = fiscalPeriodService.getFiscalPeriodForDate(editModel.getDateStart());

                // case of starting date is same as current fiscal period one, so we look for previous fiscal period
                if (previousFiscalPeriod != null && DateUtils.isSameDay(previousFiscalPeriod.getBeginDate(), editModel.getDateStart())) {
                    previousFiscalPeriod = fiscalPeriodService.getFiscalPeriodForDate(getDayMinus1Calendar(editModel.getDateStart()).getTime());
                }

                // case of selected date is upper the last fiscal period we look for the last fiscal period
                if (previousFiscalPeriod == null) {
                    FiscalPeriod lastFiscalPeriod = fiscalPeriodService.getLastFiscalPeriod();
                    previousFiscalPeriod = lastFiscalPeriod != null && lastFiscalPeriod.getEndDate().before(editModel.getDateStart()) ? lastFiscalPeriod : null;
                }

                if (previousFiscalPeriod != null) {
                    dateStart = previousFiscalPeriod.getBeginDate();
                    dateEnd = getDayMinus1Calendar(editModel.getDateStart()).getTime();
                }
                break;
        }

        if (dateStart != null && dateEnd != null) {
            previousPeriodFilter = new LetteringFilterImpl();
            previousPeriodFilter.setDateStart(dateStart);
            previousPeriodFilter.setDateEnd(dateEnd);
            previousPeriodFilter.setAccount(editModel.getAccount());
        }

        return previousPeriodFilter;
    }

    private LetteringFilter computeActualPeriodFilter() {
        LetteringFilter actualPeriodFilter = null;
        Date dateStart = null, dateEnd = null;
        switch (editModel.getPeriodChoice()) {
            case FINANCIAL_PERIOD:
                if (editModel.getFinancialPeriod() != null) {
                    Date financialPeriodDate = editModel.getFinancialPeriod().getBeginDate();
                    FiscalPeriod fiscalPeriodFiscalPeriod = fiscalPeriodService.getFiscalPeriodForDate(financialPeriodDate);
                    dateStart = fiscalPeriodFiscalPeriod.getBeginDate();
                    dateEnd = editModel.getFinancialPeriod().getEndDate();
                }
                break;
            case FISCAL_PERIOD:
                dateStart = editModel.getFiscalPeriod() == null ? null : editModel.getFiscalPeriod().getBeginDate();
                dateEnd = editModel.getFiscalPeriod() == null ? null : editModel.getFiscalPeriod().getEndDate();
                break;
            case DATE:
                dateStart = editModel.getFiscalPeriod() == null ? editModel.getDateStart() : editModel.getFiscalPeriod().getBeginDate();
                dateEnd = editModel.getDateEnd();
                break;
        }
        if (dateStart != null && dateEnd != null) {
            actualPeriodFilter = new LetteringFilterImpl();
            actualPeriodFilter.setDateStart(dateStart);
            actualPeriodFilter.setDateEnd(dateEnd);
            actualPeriodFilter.setAccount(editModel.getAccount());
        }
        return actualPeriodFilter;
    }

    public void initFiscalPeriodView() {
        List<FiscalPeriod> fiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        fiscalPeriodComboBoxModel.setObjects(fiscalPeriods);

        FiscalPeriod fiscalPeriod = fiscalPeriodService.getLastFiscalPeriod();
        if (fiscalPeriod != null) {
            setFilter(fiscalPeriod, null, fiscalPeriod.getBeginDate(), fiscalPeriod.getEndDate(), PeriodChoice.FINANCIAL_PERIOD);
            fiscalPeriodComboBoxModel.setSelectedItem(fiscalPeriod);
            view.getFiscalPeriodComboBox().setVisible(true);
        }
    }

    public void initDateIntervalView() {
        FiscalPeriod fiscalPeriod = (FiscalPeriod) view.getFiscalPeriodComboBox().getSelectedItem();
        FinancialPeriod defaultFinancialPeriod;
        if (fiscalPeriod != null) {
            Date today = new Date();
            defaultFinancialPeriod = financialPeriodService.getFinancialPeriodIfExistsByDate(today);

            if (defaultFinancialPeriod == null) {
                defaultFinancialPeriod = financialPeriodService.getFinancialForDate(fiscalPeriod.getEndDate());
            }
            setFilter(null, null, defaultFinancialPeriod.getBeginDate(), defaultFinancialPeriod.getEndDate(), PeriodChoice.DATE);
            view.getBeginPeriodPicker().setDate(defaultFinancialPeriod.getBeginDate());
            view.getEndPeriodPicker().setDate(defaultFinancialPeriod.getEndDate());
        }
    }

    public void updateFinancialPeriodView() {
        FiscalPeriod fiscalPeriod = fiscalPeriodService.getLastFiscalPeriod();
        FinancialPeriod defaultFinancialPeriod = null;
        List<FinancialPeriod> financialPeriods = null;
        if (fiscalPeriod != null) {
            financialPeriodComboBoxModel.removeAllElements();

            financialPeriods = financialPeriodService.getFinancialPeriodsWithBeginDateWithin(fiscalPeriod.getBeginDate(), fiscalPeriod.getEndDate());

            Date today = new Date();
            Iterator<FinancialPeriod> financialPeriodIterator = financialPeriods.iterator();
            while (financialPeriodIterator.hasNext() && defaultFinancialPeriod == null) {
                FinancialPeriod financialPeriod = financialPeriodIterator.next();
                defaultFinancialPeriod = financialPeriod.getBeginDate().before(today) && financialPeriod.getEndDate().after(today) ? financialPeriod : null;
            }

            defaultFinancialPeriod = defaultFinancialPeriod != null ? defaultFinancialPeriod : financialPeriods.get(financialPeriods.size() - 1);

        }

        Date defaultDateBegin, defaultDateEnd;
        Calendar calendar = Calendar.getInstance();
        int firstCurrentMonthDay = calendar.getActualMinimum(Calendar.DATE);
        int lastCurrentMonthDay = calendar.getActualMaximum(Calendar.DATE);
        defaultDateBegin = defaultFinancialPeriod == null ? DateUtils.setDays(new Date(), firstCurrentMonthDay) : defaultFinancialPeriod.getBeginDate();
        defaultDateEnd = defaultFinancialPeriod == null ?  DateUtils.setDays(new Date(), lastCurrentMonthDay) : defaultFinancialPeriod.getEndDate();

        setFilter(fiscalPeriod, defaultFinancialPeriod, defaultDateBegin, defaultDateEnd, PeriodChoice.FINANCIAL_PERIOD);

        initFiscalPeriodCombo();
        updateFinancialPeriodCombo(defaultFinancialPeriod, financialPeriods);

    }

    protected void updateFinancialPeriodCombo(FinancialPeriod defaultFinancialPeriod, List<FinancialPeriod> financialPeriods) {
        if (CollectionUtils.isNotEmpty(financialPeriods)) {
            financialPeriodComboBoxModel.setObjects(financialPeriods);
            financialPeriodComboBoxModel.setSelectedItem(defaultFinancialPeriod);
        }
    }

    protected void initFiscalPeriodCombo() {
        List<FiscalPeriod> fiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        fiscalPeriodComboBoxModel.setObjects(fiscalPeriods);
        if (CollectionUtils.isNotEmpty(fiscalPeriods)) {
            fiscalPeriodComboBoxModel.setSelectedItem(fiscalPeriods.get(fiscalPeriods.size() - 1));
        }
        view.getFiscalPeriodComboBox().setVisible(true);
    }

    protected void initSoldPeriodView() {
        Date ppbd = editModel.getFiscalPeriod() != null ? editModel.getFiscalPeriod().getBeginDate() : new Date();
        Date pped = editModel.getFinancialPeriod() != null ? getDayMinus1Calendar(editModel.getFinancialPeriod().getBeginDate()).getTime() : new Date();
        view.getBeginPeriodSummary().setText(String.format(t("lima.accountingView.summaryLabel"), getDateFormat().format(ppbd), getDateFormat().format(pped)));
        view.getBeginDebit().setText(BigDecimalToString.format(BigDecimal.ZERO));
        view.getBeginCredit().setText(BigDecimalToString.format(BigDecimal.ZERO));
        view.getBeginSold().setText(BigDecimalToString.format(BigDecimal.ZERO));

        Date aped = editModel.getFinancialPeriod() != null ? editModel.getFinancialPeriod().getEndDate() : new Date();
        view.getEndPeriodSummary().setText(String.format(t("lima.accountingView.summaryLabel"), getDateFormat().format(ppbd), getDateFormat().format(aped)));
        view.getEndDebit().setText(BigDecimalToString.format(BigDecimal.ZERO));
        view.getEndCredit().setText(BigDecimalToString.format(BigDecimal.ZERO));
        view.getEndSold().setText(BigDecimalToString.format(BigDecimal.ZERO));
    }

    protected void setViewPreviousSold(LetteringFilter previousPeriodFilter) {
        if (previousPeriodFilter != null) {
            if (editModel.getPeriodChoice().equals(PeriodChoice.FISCAL_PERIOD)) {
                String summaryLabel = t("lima.accountingView.summaryFiscalPeriodLabel");
                SimpleDateFormat yearFormat = new SimpleDateFormat(YEAR_DATE_FORMAT);
                view.getBeginPeriodSummary().setText(String.format(t(summaryLabel), yearFormat.format(previousPeriodFilter.getDateStart())));
            } else {
                String summaryLabel = t("lima.accountingView.summaryLabel");
                view.getBeginPeriodSummary().setText(String.format(t(summaryLabel), getDateFormat().format(previousPeriodFilter.getDateStart()), getDateFormat().format(getDayMinus1Calendar(previousPeriodFilter.getDateEnd()).getTime())));
            }

            List<Object[]> initialDebitCredit = financialTransactionService.getAccountEntriesDebitCreditFromIncludingToExcludingPeriod(previousPeriodFilter);
            DebitCreditSold debitCreditSold = new DebitCreditSold().invoke(initialDebitCredit);
            view.getBeginDebit().setText(BigDecimalToString.format(debitCreditSold.getDebit()));
            view.getBeginCredit().setText(BigDecimalToString.format(debitCreditSold.getCredit()));
            view.getBeginSold().setText(BigDecimalToString.format(debitCreditSold.getSold()));
        } else {
            String summaryLabel = editModel.getPeriodChoice().equals(PeriodChoice.FISCAL_PERIOD) ? t("lima.accountingView.summaryNoFiscalPeriodLabel") : t("lima.accountingView.summaryNoPeriodLabel");
            view.getBeginPeriodSummary().setText(summaryLabel);
            view.getBeginDebit().setText(BigDecimalToString.format(BigDecimal.ZERO));
            view.getBeginCredit().setText(BigDecimalToString.format(BigDecimal.ZERO));
            view.getBeginSold().setText(BigDecimalToString.format(BigDecimal.ZERO));
        }

    }

    protected void setViewActualSold(LetteringFilter actualPeriodFilter) {
        if (editModel.getPeriodChoice().equals(PeriodChoice.FISCAL_PERIOD)) {
            String summaryLabel = t("lima.accountingView.summaryFiscalPeriodLabel");
            SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
            view.getEndPeriodSummary().setText(String.format(t(summaryLabel), yearFormat.format(actualPeriodFilter.getDateEnd())));
        } else {
            String summaryLabel = t("lima.accountingView.summaryLabel");
            view.getEndPeriodSummary().setText(String.format(t(summaryLabel), getDateFormat().format(actualPeriodFilter.getDateStart()), getDateFormat().format(actualPeriodFilter.getDateEnd())));
        }

        List<Object[]> initialDebitCredit = financialTransactionService.getAccountEntriesDebitCreditFromIncludingToIncludingPeriod(actualPeriodFilter);
        DebitCreditSold debitCreditSold = new DebitCreditSold().invoke(initialDebitCredit);
        view.getEndDebit().setText(BigDecimalToString.format(debitCreditSold.getDebit()));
        view.getEndCredit().setText(BigDecimalToString.format(debitCreditSold.getCredit()));
        view.getEndSold().setText(BigDecimalToString.format(debitCreditSold.getSold()));
    }

    public void balanceAndActions() {
        if (log.isDebugEnabled()) {
            log.debug("balanceAndActions");
        }
        onBalanceChanged(null);
        AccountViewerSelectionModel accountViewerSelectionModel = (AccountViewerSelectionModel) view.getTable().getSelectionModel();
        if (view.getTable().getSelectedRows().length > 0 && !letteringNotExist(view.getTable().getSelectedRow()) ||
                view.getTable().getSelectedRows().length > 0 && !accountViewerSelectionModel.isSelectionEmpty()) {
            setValuesForSelectedEntries();
        }
    }

    /**return true if lettering is null, or not null but empty
     * @param row index of the line to test
     * @return boolean
     * */
    public boolean letteringNotExist(int row){
        boolean emptyOrNull = false;
        if (row != -1) {
            AccountViewerTableModel accountViewerTableModel = (AccountViewerTableModel) view.getTable().getModel();
            Entry entry = accountViewerTableModel.get(row);
            String lettering = entry.getLettering();
            emptyOrNull = (lettering==null||lettering.isEmpty());
        }
        return emptyOrNull;
    }

    public void setValuesForSelectedEntries() {
        Entry selectedEntry;
        AccountViewerTableModel accountViewerTableModel = (AccountViewerTableModel) view.getTable().getModel();
        AccountViewerSelectionModel accountViewerSelectionModel = (AccountViewerSelectionModel) view.getTable().getSelectionModel();
        for (int i = 0; i < accountViewerTableModel.getRowCount(); i ++){
            if (accountViewerSelectionModel.isSelectedIndex(i)){
                selectedEntry = accountViewerTableModel.get(i);
                //Set values for calculation (By AccountViewerEditModel) of balance
                onBalanceChanged(selectedEntry);
            }
        }
    }

    public void onBalanceChanged(Entry balance) {
        if (balance == null) {
            editModel.setCredit(BigDecimal.ZERO);
            editModel.setDebit(BigDecimal.ZERO);
            editModel.setSold(BigDecimal.ZERO, false);
        } else {
            selectedEntriesBalanceCalculation(balance.getAmount(), balance.isDebit());
        }
    }

    /**Allow to add / subtract credit / debit and balance
     * @param amount debit or credit
     * @param debit it indicate if amount is debit or not
     * */
    public void selectedEntriesBalanceCalculation(BigDecimal amount, boolean debit){

        BigDecimal debitVal = debit ? amount : BigDecimal.ZERO;
        BigDecimal creditVal = debit ? BigDecimal.ZERO : amount;

        if (log.isDebugEnabled()) {
            log.debug("balance calculation");
        }

        if (debitVal.equals(BigDecimal.ZERO)){

            if (!creditVal.equals(BigDecimal.ZERO)){
                editModel.setCredit(creditVal);
                editModel.setSold(creditVal, true);
            }
        } else if (creditVal.equals(BigDecimal.ZERO)){
            editModel.setDebit(debitVal);
            editModel.setSold(debitVal, false);
        } else{
            onBalanceChanged(null);
        }
    }

    protected class DebitCreditSold {
        private BigDecimal debit;
        private BigDecimal credit;
        private BigDecimal sold;

        public BigDecimal getDebit() {
            return debit;
        }

        public BigDecimal getCredit() {
            return credit;
        }

        public BigDecimal getSold() {
            return sold;
        }

        public DebitCreditSold invoke(List<Object[]> initialDebitCredit) {
            debit = BigDecimal.ZERO;
            credit = BigDecimal.ZERO;
            if (CollectionUtils.isNotEmpty(initialDebitCredit)) {
                int nbAmount = initialDebitCredit.size();
                if (nbAmount == 2) {
                    debit = (BigDecimal) initialDebitCredit.get(0)[1];
                    credit = (BigDecimal) initialDebitCredit.get(1)[1];
                }
                if (nbAmount == 1) {
                    if ((Boolean) initialDebitCredit.get(0)[0]) {
                        debit = (BigDecimal) initialDebitCredit.get(0)[1];
                    } else {
                        credit = (BigDecimal) initialDebitCredit.get(0)[1];
                    }
                }
            }

            sold = debit.subtract(credit);
            return this;
        }
    }

    public void setAccount(Account account) {
        if (editModel != null) {
            editModel.setAccount(account);
            updateAllEntries();
        }
    }

    public Account getAccount() {
        Account account = null;
        if (editModel != null) {
            account = editModel.getAccount();
        }
        return account;
    }

    protected Calendar getDayMinus1Calendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        return cal;
    }


    protected DateFormat getDateFormat() {
            Locale locale = LimaSwingConfig.getInstance().getLocale();
            DateFormat result = DateFormat.getDateInstance(DateFormat.SHORT, locale);
            return result;
    }

    protected Locale getLocale() {
            Locale locale = LimaSwingConfig.getInstance().getLocale();
            return locale;
    }

}
