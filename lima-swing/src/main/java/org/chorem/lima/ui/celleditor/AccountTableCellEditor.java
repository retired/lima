/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.celleditor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.entity.Account;
import org.chorem.lima.ui.combobox.AccountComboBox;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;

/**
 * Editor des cellules de tableau de type "Account".
 * Affiche une wide combo box pour selectionner un compte.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class AccountTableCellEditor extends AbstractCellEditor implements TableCellEditor {

    protected static final Log log = LogFactory.getLog(AccountTableCellEditor.class);

    protected final AccountComboBox accountComboBox;

    protected static final long serialVersionUID = 1L;

    /** constructor */
    public AccountTableCellEditor() {
        accountComboBox = new AccountComboBox();
        accountComboBox.setLeafAccounts(true);
        accountComboBox.setShowDecorator(false);

        final JComboBox comboBox = accountComboBox.getCombobox();
        final ComboBoxEditor editor = comboBox.getEditor();

        editor.getEditorComponent().addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                if (log.isDebugEnabled()) {
                    log.debug("keyReleased:" + e.getKeyCode());
                    log.debug("items:" + comboBox.getItemCount());
                }
                // in case of unique chose available this one is auto-selected
                if (accountComboBox.isEnterToSelectUniqueUniverse() && comboBox.getItemCount() == 1) {


                    if (log.isDebugEnabled()) {
                        log.debug("Auto-select unique result");
                    }
                    e.consume();

                    if (log.isDebugEnabled()) {
                        log.debug("setEditable false");
                    }

                    // set the account into the combobox
                    // disabled edition to avoid user taping other chars and unvolontary start a new research
                    // User can press Tab or Enter to leave the combo or F12 to edit it again
                    comboBox.getItemAt(0);
                    comboBox.setSelectedIndex(0);
                    editor.setItem(comboBox.getSelectedItem());
                    comboBox.setEditable(false);

                }
            }
        });

    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        // focus reach the combo by either coming into it or pressing F12
        // cell is set editable for user to be able to start a new research
        accountComboBox.getCombobox().setEditable(true);
        if (value != null && (value instanceof Account)) {
            if (log.isDebugEnabled()) {
                log.debug("set account:" + ((Account) value).getAccountNumber());
            }
            accountComboBox.setSelectedItem(value);
        }
        return accountComboBox;
    }

    @Override
    public Object getCellEditorValue() {
        return accountComboBox.getSelectedItem();
    }

    /**
     * Vérifie si la cellule peut être éditable :
     * seulement si il y a une frappe au clavier ou un double clic.
     *
     * @param evt the event
     * @return true celle is editable otherwise not.
     */
    @Override
    public boolean isCellEditable(EventObject evt) {
        // Si il y a une frappe au clavier
        if (evt instanceof KeyEvent) {
            final KeyEvent keyEvent = (KeyEvent) evt;
            // Empèche la touche echap
            if (keyEvent.getKeyChar() != KeyEvent.VK_ESCAPE) {
                // Permet de placer le focus sur l'editor de la comboBox
                SwingUtilities.invokeLater(() -> {
                    accountComboBox.getCombobox().getEditor().getEditorComponent().requestFocus();
                    JTextComponent edit = (JTextComponent) accountComboBox.getCombobox().getEditor().getEditorComponent();
                    edit.requestFocus();
                    if (!Character.isIdentifierIgnorable(keyEvent.getKeyChar())) {
                        edit.setText(Character.toString(keyEvent.getKeyChar()));
                    }
                });
            }
        }
        boolean result = !(evt instanceof MouseEvent) || ((MouseEvent) evt).getClickCount() == 1;
        return result;
    }
}
