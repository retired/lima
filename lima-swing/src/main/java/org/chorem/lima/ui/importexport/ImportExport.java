/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.importexport;

import com.google.common.base.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.business.ExportResult;
import org.chorem.lima.business.ImportExportResults;
import org.chorem.lima.business.ImportResult;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.ExportService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.AlreadyExistFinancialStatementException;
import org.chorem.lima.business.exceptions.AlreadyExistVatStatementException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.ImportFileException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoDataToImportException;
import org.chorem.lima.business.exceptions.NoFiscalPeriodFoundException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialStatement;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.VatStatement;
import org.chorem.lima.enums.EncodingEnum;
import org.chorem.lima.enums.ImportExportEnum;
import org.chorem.lima.util.ErrorHelper;
import org.jdesktop.swingx.painter.BusyPainter;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.rmi.server.ExportException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static org.nuiton.i18n.I18n.t;

/**
 * Import export helper.
 * Calling service with specified file to import and displaying wait view to user.
 *
 * @author echatellier
 * Date : 24 avr. 2012
 */
public class ImportExport {

    private static final Log log = LogFactory.getLog(ImportExport.class);
    public static final int BUFFER_SIZE = 1024;
    public static final String CSV = ".csv";
    public static final String EXPORT_DATE_PATTERN = "dd-MM-yyyy HHmmss";

    /** Parent view. */
    protected Component viewComponent;

    protected ImportService importService;

    protected ExportService exportService;

    private ImportExportWaitView waitView;

    protected ErrorHelper errorHelper;

    public static final String JAVA_IO_TMPDIR = System.getProperty("java.io.tmpdir")+"/";
    public static final String TMP_BACKUP_FILE_MODEL = "%s%s-%s" + CSV;
    public static final String EXPORT_ALL_ZIP_FILE_NAME = "%s/LIMA-BACKUP-%s.zip";
    public static final String TEMPORARY_EXTRACT_CSV_FILE_PATH = JAVA_IO_TMPDIR + "%s" + CSV;

    private static String getExportDefaultFileName(ImportExportEnum export) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(EXPORT_DATE_PATTERN);
        return t(ImportExportEnum.class.getName() + "." + export.name()) + "_" + dateFormat.format(new Date());
    }

    private static String getSelectedFileWithExtension(ImportExportEnum export, String filePath) {
        if (ImportExportEnum.CSV_ALL_EXPORT != export && !export.getImportMode()) {
            filePath = filePath == null ? "" : filePath.trim();
            filePath = filePath.endsWith("/") ? filePath + getExportDefaultFileName(export) : filePath;
            String nameLower = (filePath.toLowerCase());
            filePath = nameLower.endsWith(CSV) ? filePath : filePath + CSV;
        }
        return filePath;
    }

    public ImportExport(Component view) {
        viewComponent = view;

        //services
        importService = LimaServiceFactory.getService(ImportService.class);
        exportService = LimaServiceFactory.getService(ExportService.class);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());

        //create the wait dialog panel
        waitView = new ImportExportWaitView();
        waitView.setLocationRelativeTo(view);
        BusyPainter busyPainter = waitView.getBusylabel().getBusyPainter();
        busyPainter.setHighlightColor(new Color(44, 61, 146).darker());
        busyPainter.setBaseColor(new Color(168, 204, 241).brighter());
    }

    /**
     * Call the appropriate method in business service.
     *
     * @param importExportChoice determine what to import/export and export type (CSV, EBP, PDF)
     * @param exportPath file path for import/export file, if null a dialog will ask for it
     * @param verbose if true a dialog show result
     */
    public boolean importExport(final ImportExportEnum importExportChoice, String exportPath, URL importUrl, boolean verbose) {
        final Charset defaultCharset = Charsets.UTF_8;
        boolean done = false;
        if (StringUtils.isBlank(exportPath) && importUrl == null) {
            exportPath = chooseFile(importExportChoice.getImportMode(), importExportChoice);
            if (exportPath != null) { // if null ==> cancel import/export
                File file = new File(exportPath);
                if (file.exists()) {
                    try {
                        importUrl = file.toURI().toURL();
                    } catch (MalformedURLException e) {
                        throw new LimaTechnicalException("Can't open file " + exportPath);
                    }
                } else {
                    throw new LimaTechnicalException("Can't open file from path" + exportPath);
                }
            }
        }
        if (StringUtils.isNotBlank(exportPath) || importUrl != null) {
            processImportExport(verbose, importExportChoice, defaultCharset, exportPath, importUrl);
            done = true;
        }
        return done;
    }

    public void resetInitialAccountabilityLayoutsImport() {
        importService.removeAccountabilityLayouts();
    }

    public void resetInitialImportedEntryBook() {
        importService.removeInitallyImportedEntryBook();
    }

    public void processImportExport(final ImportExportEnum importExportChoice, boolean verbose) {
        switch (importExportChoice) {
            case CSV_ALL_EXPORT:
            case CSV_ACCOUNTCHARTS_EXPORT:
            case CSV_ENTRYBOOKS_EXPORT:
            case CSV_ENTRIES_EXPORT:
            case CSV_FINANCIALSTATEMENTS_EXPORT:
            case CSV_VAT_EXPORT:
            case PDF_VAT_EXPORT:
            case EBP_ACCOUNTCHARTS_EXPORT:
            case EBP_ENTRIES_EXPORT:
            case EBP_ENTRYBOOKS_EXPORT:
                processExport(importExportChoice, verbose);
                break;

            case CSV_ALL_IMPORT:
            case CSV_ACCOUNTCHARTS_IMPORT:
            case CSV_ENTRYBOOKS_IMPORT:
            case CSV_ENTRIES_IMPORT:
            case CSV_FINANCIALSTATEMENTS_IMPORT:
            case CSV_VAT_IMPORT:
            case PDF_VAT_IMPORT:
            case EBP_ACCOUNTCHARTS_IMPORT:
            case EBP_ENTRIES_IMPORT:
            case EBP_ENTRYBOOKS_IMPORT:
                processImport(importExportChoice, verbose);
                break;
        }
    }

    protected void processImport(final ImportExportEnum importExportChoice, boolean verbose) {
        final Charset defaultCharset = Charsets.UTF_8;
        URL importFileUrl;
        String importFilePath = chooseFile(importExportChoice.getImportMode(), importExportChoice);
        if (importFilePath != null) {  // if null ==> cancel import
            File file = new File(importFilePath);
            if (file.exists()) {
                try {
                    importFileUrl = file.toURI().toURL();
                } catch (MalformedURLException e) {
                    throw new LimaTechnicalException("Can't open file " + importFilePath);
                }
            } else {
                throw new LimaTechnicalException("Can't open file from path" + importFilePath);
            }
            log.info("Processing import:" + importFileUrl.getPath() + " " + importFilePath + " " + importFileUrl.getFile());
            processImportExport(verbose, importExportChoice, defaultCharset, null, importFileUrl);
        }
    }

    protected void processExport(final ImportExportEnum importExportChoice, boolean verbose) {
        String exportFilePath = chooseFile(importExportChoice.getImportMode(), importExportChoice);
        if (exportFilePath != null) {  // if null ==> cancel export
            File file = new File(exportFilePath);
            if (file.getParentFile().exists()) {
                log.info("Processing export:" + exportFilePath);
                processImportExport(verbose, importExportChoice, Charsets.UTF_8, exportFilePath, null);
            } else {
                throw new LimaTechnicalException("Targeted directory can not be found.");
            }
        }
    }

    protected void processImportExport(final boolean verboseMode, final ImportExportEnum importExportMethod, final Charset defaultCharset, final String exportFilePath, final URL importURL) {
        //if export cancel
        final Boolean importMode = importExportMethod.getImportMode();

        new SwingWorker<ImportExportResults, Void>() {

            @Override
            protected ImportExportResults doInBackground() {
                ImportExportResults results = new ImportExportResults();
                switch (importExportMethod) {
                    //####################################### CSV ##############################################
                    case CSV_ACCOUNTCHARTS_IMPORT:
                        String content = loadContent(importURL, defaultCharset.name());
                        results.pushImportResults(importService.importAccountAsCSV(content));
                        break;
                    case CSV_ENTRYBOOKS_IMPORT:
                        content = loadContent(importURL, defaultCharset.name());
                        results.pushImportResults(importService.importEntryBooksAsCSV(content));
                        break;
                    case CSV_ENTRIES_IMPORT:
                        content = loadContent(importURL, defaultCharset.name());
                        results.pushImportResults(importService.importEntriesAsCSV(content));
                        break;
                    case CSV_VAT_IMPORT:
                        content = loadContent(importURL, defaultCharset.name());
                        results.pushImportResults(importService.importVATStatementsAsCSV(content));
                        break;
                    case CSV_FINANCIALSTATEMENTS_IMPORT:
                        content = loadContent(importURL, defaultCharset.name());
                        results.pushImportResults(importService.importFinancialStatementsAsCSV(content));
                        break;

                    case CSV_ACCOUNTCHARTS_EXPORT:
                        results.pushExportResults(exportService.exportAccountsAsCSV(defaultCharset.name()));
                        createFile(exportFilePath, defaultCharset.name(), results.getExportResults().get(0).getExportData());
                        break;
                    case CSV_ENTRYBOOKS_EXPORT:
                        results.pushExportResults(exportService.exportEntryBooksAsCSV(defaultCharset.name()));
                        createFile(exportFilePath, defaultCharset.name(), results.getExportResults().get(0).getExportData());
                        break;
                    case CSV_ENTRIES_EXPORT:
                        results.pushExportResults(exportService.exportEntriesAsCSV(defaultCharset.name(), true));
                        createFile(exportFilePath, defaultCharset.name(), results.getExportResults().get(0).getExportData());
                        break;
                    case CSV_ALL_EXPORT:
                        results = exportAll(exportFilePath, defaultCharset.name());
                        break;
                    case CSV_ALL_IMPORT:
                        results = importAllFromZipFile(importURL);
                        break;
                    case CSV_VAT_EXPORT:
                        results.pushExportResults(exportService.exportVatStatements(defaultCharset.name()));
                        createFile(exportFilePath, defaultCharset.name(), results.getExportResults().get(0).getExportData());
                        break;
                    case CSV_FINANCIALSTATEMENTS_EXPORT:
                        results.pushExportResults(exportService.exportFinancialStatements(defaultCharset.name()));
                        createFile(exportFilePath, defaultCharset.name(), results.getExportResults().get(0).getExportData());
                        break;

                    //####################################### EBP ##############################################
                    //For windows ebp so using encoding ISOLATIN1
                    case EBP_ACCOUNTCHARTS_IMPORT:
                        content = loadContent(importURL, EncodingEnum.ISOLATIN1.getEncoding());
                        results.pushImportResults(importService.importAccountFromEbp(content));
                        break;

                    case EBP_ENTRYBOOKS_IMPORT:
                        content = loadContent(importURL, EncodingEnum.ISOLATIN1.getEncoding());
                        results.pushImportResults(importService.importEntryBookFromEbp(content));
                        break;

                    case EBP_ENTRIES_IMPORT:
                        content = loadContent(importURL, EncodingEnum.ISOLATIN1.getEncoding());
                        results.pushImportResults(importService.importEntriesFromEbp(content));
                        break;

                    case EBP_ACCOUNTCHARTS_EXPORT:
                        results.pushExportResults(exportService.exportAccountAsEbp(defaultCharset.name()));
                        createFile(exportFilePath, EncodingEnum.ISOLATIN1.getEncoding(), results.getExportResults().get(0).getExportData());
                        break;

                    case EBP_ENTRYBOOKS_EXPORT:
                        results.pushExportResults(exportService.exportEntryBookAsEbp(defaultCharset.name()));
                        createFile(exportFilePath, EncodingEnum.ISOLATIN1.getEncoding(), results.getExportResults().get(0).getExportData());
                        break;

                    case EBP_ENTRIES_EXPORT:
                        results.pushExportResults(exportService.exportEntriesAsEbp(defaultCharset.name()));
                        createFile(exportFilePath, EncodingEnum.ISOLATIN1.getEncoding(), results.getExportResults().get(0).getExportData());
                        break;

                    default:
                        break;
                }
                return results;
            }

            @Override
            protected void done() {
                try {

                    if (log.isDebugEnabled()) {
                        log.debug("importMode : " + importMode);
                    }

                    // display result dialog
                    if (verboseMode) {
                        ImportExportResults globalResult = get();
                        if (importMode && globalResult == null) {
                            JOptionPane.showMessageDialog(viewComponent, t("lima.import.error"),
                                    t("lima.import.title"), JOptionPane.ERROR_MESSAGE);

                        } else {
                            if (importMode) {
                                List<ImportResult> importResults = globalResult.getImportResults();
                                if (importResults != null) {
                                    ComputeImportResultMessage(importResults);
                                }
                            } else {
                                List<ExportResult> exportResults = globalResult.getExportResults();
                                if (exportResults != null) {
                                    ComputeExportResultMessage(exportResults);
                                }
                            }
                        }
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't get result message", ex);
                    }
                } finally {
                    //hidde wait dialog panel
                    waitView.dispose();
                }
            }
        }.execute();
        waitView.setVisible(true);

    }

    protected void ComputeExportResultMessage(List<ExportResult> exportResults) {
        StringBuilder message = new StringBuilder();
        for (ExportResult result : exportResults) {
            Class fromSource = result.getFromSource();
            String messageFromSource = getFromSourceMessage(fromSource);
            message.append("Export ");
            message.append(messageFromSource);

            List<ExportException> exportExceptions = result.getExportExceptions();
            if (exportExceptions != null && !exportExceptions.isEmpty()) {
                message.append(t("lima.export.exceptions") + "\n");
            }
        }

        displayImportExportResultMessage(message, false);
    }

    protected void ComputeImportResultMessage(List<ImportResult> resultList) {
        StringBuilder message = new StringBuilder();
        String importTerminatedMessage = t("lima.import.terminated") + "\n";
        String noErrorFoundMessage = t("lima.import.report.noErrorFound");
        String someErrorFoundMessage = t("lima.import.report.someErrorFound");
        message.append(importTerminatedMessage);
        for (ImportResult result : resultList) {
            Class fromSource = result.getFromSource();
            message.append("Import ");
            message.append(getFromSourceMessage(fromSource));
            message.append(result.getAllExceptionsByLine().isEmpty() ? noErrorFoundMessage : someErrorFoundMessage);
            message.append(t("lima.import.report", result.getNbCreated(), result.getNbIgnored(), result.getNbUpdated()));

            logErrorMessage(result, fromSource);
        }

        displayImportExportResultMessage(message, true);

    }

    protected void displayImportExportResultMessage(StringBuilder message, Boolean importMode) {
        if (log.isDebugEnabled()){
            log.debug(message.toString());
        }

        String title = importMode ? t("lima.import.terminated") : t("lima.export.terminated");
        JOptionPane.showMessageDialog(
                waitView,
                message.toString(),
                title,
                JOptionPane.INFORMATION_MESSAGE);
    }

    protected void logErrorMessage(ImportResult result,Class fromSource) {
        Map<Integer, LimaException> exceptionsByLine = result.getAllExceptionsByLine();
        if (!exceptionsByLine.isEmpty()) {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.append("\n" + "Import " + getFromSourceMessage(fromSource));
            errorMessage.append("\n" + t("lima.import.report", result.getNbCreated(), result.getNbIgnored(), result.getNbUpdated()));

            for (Map.Entry<Integer, LimaException> e:exceptionsByLine.entrySet()) {
                String message = "\n" + t("lima.import.line", e.getKey()) + "%s";
                String error;
                LimaException importException = e.getValue();
                if (importException instanceof InvalidAccountNumberException) {
                    error = t("lima.account.error.invalidAccountNumber", ((InvalidAccountNumberException) importException).getAccountNumber());
                } else if (importException instanceof NotNumberAccountNumberException) {
                    error = t("lima.account.error.notNumberAccountNumber", ((NotNumberAccountNumberException) importException).getAccountNumber());
                } else if (importException instanceof NotAllowedLabelException) {
                    error = t("lima.error.notAllowedLabel", ((NotAllowedLabelException) importException).getLabel());
                } else if (importException instanceof MoreOneUnlockFiscalPeriodException) {
                    error = t("lima.fiscalPeriod.error.moreOneUnlockFiscalPeriod", ((MoreOneUnlockFiscalPeriodException) importException).getBeginDate(), ((MoreOneUnlockFiscalPeriodException) importException).getEndDate());
                } else if (importException instanceof BeginAfterEndFiscalPeriodException) {
                    error = t("lima.fiscalPeriod.error.beginAfterEndFiscalPeriod", ((BeginAfterEndFiscalPeriodException) importException).getFiscalPeriod().getBeginDate(), ((BeginAfterEndFiscalPeriodException) importException).getFiscalPeriod().getEndDate());
                } else if (importException instanceof NotBeginNextDayOfLastFiscalPeriodException) {
                    error = t("lima.fiscalPeriod.error.notBeginNextDayOfLastFiscalPeriod", ((NotBeginNextDayOfLastFiscalPeriodException) importException).getFiscalPeriod().getBeginDate(), ((NotBeginNextDayOfLastFiscalPeriodException) importException).getFiscalPeriod().getEndDate());
                } else if (importException instanceof LockedFinancialPeriodException) {
                    error = t("lima.fiscalPeriod.error.lockedFinancialPeriod");
                } else if (importException instanceof LockedEntryBookException) {
                    error = t("lima.entryBook.error.lockedEntryBook", ((LockedEntryBookException) importException).getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(), ((LockedEntryBookException) importException).getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate());
                } else if (importException instanceof AlreadyExistFinancialStatementException) {
                    error = t("lima.financialStatement.error.alreadyExistFinancialStatement", ((AlreadyExistFinancialStatementException) importException).getFinancialStatementLabel());
                } else if (importException instanceof AlreadyExistVatStatementException) {
                    error = t("lima.vatStatement.error.alreadyExistVatStatement", ((AlreadyExistVatStatementException) importException).getVatStatementLabel());
                } else if (importException instanceof NoFiscalPeriodFoundException) {
                    error = t("lima.error.noFiscalPeriodFound");
                } else if (importException instanceof BeforeFirstFiscalPeriodException) {
                    error = t("lima.entries.add.transaction.error.beforeFirstFiscalPeriod", ((BeforeFirstFiscalPeriodException) importException).getDate());
                } else if (importException instanceof AfterLastFiscalPeriodException) {
                    error = t("lima.entries.add.transaction.error.afterLastFiscalPeriod", ((AfterLastFiscalPeriodException) importException).getDate());
                } else if (importException instanceof NoDataToImportException) {
                    error = t("lima.import.error.noDataToImport");
                } else if (importException instanceof ImportFileException){
                    error = ((ImportFileException) importException).getDetailMessage();
                } else {
                    error = importException.getMessage();
                }
                if (StringUtils.isNotEmpty(error)) {
                    errorMessage.append(String.format(message, error));
                }
            }
            log.error(errorMessage.toString());
        }
    }

    /**
     * Return with entities have been imported/exported.
     * @param fromSource The imported target class
     * @return the name of the imported/exported entities class.
     */
    protected String getFromSourceMessage(Class fromSource) {
        String message;
        if (fromSource == null) {
            message = "BACKUP"+"\n";
        } else if (fromSource.equals(Account.class)){
            message = t("lima.importExport.account")+"\n";
        } else if (fromSource.equals(EntryBook.class)) {
            message = t("lima.importExport.entryBook")+"\n";
        } else if (fromSource.equals(Entry.class)) {
            message = t("lima.importExport.entry")+"\n";
        } else if (fromSource.equals(FinancialStatement.class)) {
            message = t("lima.importExport.financialStatement")+"\n";
        } else if (fromSource.equals(FinancialTransaction.class)) {
            message = t("lima.importExport.financialTransaction")+"\n";
        } else if (fromSource.equals(FiscalPeriod.class)) {
            message = t("lima.importExport.fiscalPeriod")+"\n";
        } else if (fromSource.equals(Identity.class)) {
            message = t("lima.importExport.identity")+"\n";
        } else if (fromSource.equals(VatStatement.class)) {
            message = t("lima.importExport.vatStatement")+"\n";
        } else {
            throw new LimaTechnicalException("Source not know");
        }
        return message;
    }

    /**
     * open choose file dialog with appropriate file mode view
     * folders for export or folders+files for import
     *
     * @param importMode true = import mode false = export
     * @param importExportMethod Inport/export to process
     * @return the targeted file path
     */
    public String chooseFile(Boolean importMode, ImportExportEnum importExportMethod) {
        String filePath = null;
        JFileChooser chooser = new JFileChooser();
        //Encoding option

        JComboBox comboBox = new JComboBox<>(EncodingEnum.descriptions());

        if (importExportMethod.getEncodingOption()) {
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            panel.add(new JLabel(t("lima.importExport.encoding.choice")), BorderLayout.WEST);
            panel.add(comboBox, BorderLayout.CENTER);
            ((Container) chooser.getComponent(2)).add(panel, BorderLayout.SOUTH);
        }

        String approveButtonText;
        if (importMode) {
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            approveButtonText = t("lima.importExport.import");
            chooser.setDialogTitle(approveButtonText);
            chooser.setApproveButtonText(approveButtonText);
        } else if (importExportMethod.equals(ImportExportEnum.CSV_ALL_EXPORT)) {
            approveButtonText = t("lima.importExport.export");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setDialogTitle(approveButtonText);
            chooser.setApproveButtonText(approveButtonText);
        } else {
            approveButtonText = t("lima.importExport.export");
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setSelectedFile(new File(getExportDefaultFileName(importExportMethod)));
            chooser.setDialogTitle(approveButtonText);
            chooser.setApproveButtonText(approveButtonText);
        }
        if (chooser.showOpenDialog(viewComponent) == JFileChooser.APPROVE_OPTION) {
            filePath = chooser.getSelectedFile().getAbsolutePath();
            filePath = getSelectedFileWithExtension(importExportMethod, filePath);
        }

        return filePath;
    }


    /**
     * Get csv datas in string and write file.
     *
     * @param filePath path to file
     * @param charset charset to use
     * @param data data to write on file
     */
    protected File createFile(String filePath, String charset, String data) {
        File file = new File(filePath);
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), charset));
            out.write(data);
            out.flush();
            out.close();
        } catch (IOException eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't write file " + filePath, eee);
            }
        } finally {
            IOUtils.closeQuietly(out);
        }
        return file;
    }

    /**
     * Open csv file and get his data on a string.
     *
     * @param url url to targeted file
     * @param charset charset to use for import
     * @return file contents
     */
    protected String loadContent(URL url, String charset) {
        String result = null;

        if (url != null) {
            try (InputStream inputStream = url.openStream()) {
                result = IOUtils.toString(inputStream, charset);
                if (log.isInfoEnabled()) {
                    log.info("Loading " + url.getPath());
                }
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't read file " + url.getPath(), e);
                }
            }
        }

        return result;
    }

    protected ImportExportResults exportAll(String path, String charset){
        ImportExportResults streamData = exportService.exportAll(charset);

        ZipOutputStream export = null;
        FileOutputStream result = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(EXPORT_DATE_PATTERN);
            result = new FileOutputStream(String.format(EXPORT_ALL_ZIP_FILE_NAME, path, dateFormat.format(new Date())));
            export = new ZipOutputStream(result);
            List<ExportResult> exportResults = streamData.getExportResults();
            for (ExportResult exportedData : exportResults) {
                if (exportedData != null && StringUtils.isNotBlank(exportedData.getExportData())) {
                    String data = exportedData.getExportData();
                    String entityFileName = exportedData.getFromSource().getSimpleName();
                    File file = createFile(String.format(TEMPORARY_EXTRACT_CSV_FILE_PATH, entityFileName), charset, data);
                    if (file != null) {
                        FileInputStream stream = null;
                        try {
                            ZipEntry ze = new ZipEntry(file.getName());
                            export.putNextEntry(ze);
                            int len;
                            byte[] buffer = new byte[BUFFER_SIZE];
                            stream = new FileInputStream(file);
                            while ((len = stream.read(buffer)) > 0) {
                                export.write(buffer, 0, len);
                            }
                        } finally {
                            IOUtils.closeQuietly(stream);
                            FileUtils.forceDelete(file);
                        }
                    }
                }
            }
            export.flush();

        } catch (IOException e) {
            errorHelper.showErrorMessage(t("lima.export.failed"));
            log.error(t("lima.export.failed"), e);
        } finally {
            IOUtils.closeQuietly(export);
            IOUtils.closeQuietly(result);
        }
        return streamData;
    }

    protected ImportExportResults importAllFromZipFile(URL url) {

        String tmpDir = System.getProperty("java.io.tmpdir")+"/";
        String determinant = String.valueOf(new Date().getTime());
        ImportResult result = extractZipFile(url, tmpDir, determinant);
        ImportExportResults results = processRestoreAll(result, tmpDir, determinant);
        return results;
    }

    protected ImportResult extractZipFile(URL url, String tmpDir, String determinant) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(null);

        ZipInputStream zipInputStream = null;
        InputStream inputStream = null;
        // use to be sure to not load old streams.
        try {
            inputStream = url.openStream();

            zipInputStream = new ZipInputStream(inputStream);

            ZipEntry entry;

            while ((entry = zipInputStream.getNextEntry()) != null) {
                FileOutputStream fileoutputstream = null;
                try {
                    byte[] buffer = new byte[BUFFER_SIZE];
                    String entityName = StringUtils.substring(entry.getName(), 0, -4);
                    String targetFileName= String.format(TMP_BACKUP_FILE_MODEL, tmpDir, entityName, determinant);
                    fileoutputstream = new FileOutputStream(targetFileName);

                    int n;
                    while ((n = zipInputStream.read(buffer, 0, BUFFER_SIZE)) > -1) {
                        fileoutputstream.write(buffer, 0, n);
                    }
                    zipInputStream.closeEntry();
                } finally {
                    IOUtils.closeQuietly(fileoutputstream);
                }
            }
        } catch (Exception e) {
            result.addInitException(new ImportFileException(t("lima.import.error.extractFile", e)));
            log.error(e.getStackTrace());
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(zipInputStream);
            IOUtils.closeQuietly(inputStream);
        }
        return result;
    }

    protected ImportExportResults processRestoreAll(ImportResult result, String tmpDir, String determinant) {
        ImportExportResults results = null;
        if (result.getAllExceptionsByLine().isEmpty()) {

            String pathname = String.format(TMP_BACKUP_FILE_MODEL, tmpDir, EntryBook.class.getSimpleName(), determinant);
            String entryBooksStreamString = extractFile(result, pathname);

            pathname = String.format(TMP_BACKUP_FILE_MODEL, tmpDir, FinancialTransaction.class.getSimpleName(), determinant);
            String transactionsStreamString = extractFile(result, pathname);

            pathname = String.format(TMP_BACKUP_FILE_MODEL, tmpDir, Account.class.getSimpleName(), determinant);
            String accountsStreamString = extractFile(result, pathname);

            pathname = String.format(TMP_BACKUP_FILE_MODEL, tmpDir, Entry.class.getSimpleName(), determinant);
            String entriesStreamString = extractFile(result, pathname);

            pathname = String.format(TMP_BACKUP_FILE_MODEL, tmpDir, FiscalPeriod.class.getSimpleName(), determinant);
            String fiscalPeriodsStreamString = extractFile(result, pathname);

            pathname = String.format(TMP_BACKUP_FILE_MODEL, tmpDir, Identity.class.getSimpleName(), determinant);
            String identityStreamString = extractFile(result, pathname);

            try {
                results = importService.importAll(entryBooksStreamString, transactionsStreamString, fiscalPeriodsStreamString, accountsStreamString, entriesStreamString, identityStreamString);
            } catch (Exception ex) {
                if(log.isInfoEnabled()) {
                    log.info(ex);
                }
                result.addInitException(new ImportFileException(t("lima.import.error.extractFile")));
            }
        }
        return results;
    }

    private String extractFile(ImportResult result, String pathname){
        InputStream stream = null;
        File file = new File(pathname);
        String extractedStream = "";
        if (file.exists()) {
            try {
                stream =  new FileInputStream(file);
                extractedStream = IOUtils.toString(stream);
            } catch (IOException ex) {
                if(log.isInfoEnabled()) {
                    log.info(ex);
                }
                result.addInitException(new ImportFileException(t("lima.import.error.extractFile")));
            } finally {
                IOUtils.closeQuietly(stream);
            }
        }
        return extractedStream;
    }
}
