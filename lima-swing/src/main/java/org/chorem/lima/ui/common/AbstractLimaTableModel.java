package org.chorem.lima.ui.common;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.util.ErrorHelper;

import javax.swing.table.AbstractTableModel;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public abstract class AbstractLimaTableModel<E> extends AbstractTableModel {

    private static final long serialVersionUID = -1125159257823536360L;
    protected List<E> values;

    protected List<Column<AbstractLimaTableModel<E>>> columns;
    
    protected Comparator<E> comparator;

    protected ErrorHelper errorHelper;

    public AbstractLimaTableModel() {

        values = Lists.newArrayList();
        
        columns = Lists.newArrayList();

        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());

        initColumn();

    }

    protected abstract void initColumn();

    public void setComparator(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    public void sort() {
        if (comparator != null) {
            values.sort(comparator);
            fireTableDataChanged();
        }
    }

    public void setValues(Collection<E> values) {
        this.values.clear();
        this.values.addAll(values);
        sort();
        fireTableDataChanged();
    }

    public void setValue(int row, E value) {
        values.set(row, value);
        fireTableRowsUpdated(row, row);
    }
    
    public void addValue(E value) {
        int row = values.size();
        values.add(value);
        fireTableRowsInserted(row, row);
        sort();
    }
    
    public void addAll(Collection<E> values) {
        if (values != null && !values.isEmpty()) {
            int row = this.values.size();
            this.values.addAll(values);
            fireTableRowsInserted(row, row + values.size() - 1);
            sort();
        }
    }

    public void clear() {
        if (!values.isEmpty()) {
            int rowCount = getRowCount();
            values.clear();
            fireTableRowsDeleted(0, rowCount-1);
        }
    }

    public List<E> getValues() {
        return values;
    }

    public void remove(E value) {
        int row = indexOf(value);
        values.remove(value);
        fireTableRowsDeleted(row, row);
    }

    public void removeAll(Collection<E> values) {
        for (E value : values) {
            remove(value);
        }
    }

    public void remove(int row) {
        values.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public E get(int row) {
        return values.get(row);
    }

    public int indexOf(E value) {
        return values.indexOf(value);
    }

    public boolean contains(E value) {
        return values.contains(value);
    }

    public void addColumn(Column column) {
        column.setTableModel(this);
        columns.add(column);
        fireTableStructureChanged();
    }

    public Column getColumn(int column) {
        return columns.get(column);
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public int getRowCount() {
        return size();
    }

    public int size() {
        return values.size();
    }

    @Override
    public Class<?> getColumnClass(int column) {
        Class result = null;
        if (column >= 0 && column < columns.size()) {
            result = columns.get(column).getColumnClass();
        }
        return result;
    }

    @Override
    public String getColumnName(int column) {
        String result = t("lima.noAffect");
        if (column >= 0 && column < columns.size()) {
            result = columns.get(column).getColumnName();
        }
        return  result;
    }

    @Override
    public Object getValueAt(int row, int column) {

        Object result = columns.get(column).getValueAt(row);

        return result;
    }

    /**
     * To set cells editable or not
     *
     */
    @Override
    public boolean isCellEditable(int row, int column) {
        boolean result = columns.get(column).isCellEditable(row);
        return  result;
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        boolean update = columns.get(column).setValueAt(value, row);
        // some modification must update all other
        // first row modification update following rows
        if (update) {
            fireTableCellUpdated(row, column);
        }
    }
}
