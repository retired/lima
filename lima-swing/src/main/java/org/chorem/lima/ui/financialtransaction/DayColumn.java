package org.chorem.lima.ui.financialtransaction;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.ui.celleditor.DayTableCellEditor;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * @author David Cossé <cosse@codelutin.com>
 */
public class DayColumn extends AbstractColumn<FinancialTransactionTableModel> {

    protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd");

    public DayColumn(){
        super(Integer.class,  t("lima.financialTransaction.day"), true);
        setCellEditor(new DayTableCellEditor());
    }

    @Override
    public Object getValueAt(int row) {
        Integer result;
        Entry entry = tableModel.get(row);
        FinancialTransaction transaction = entry.getFinancialTransaction();
        if (row == 0 || tableModel.get(row - 1).getFinancialTransaction() != transaction) {

            result = Integer.valueOf(dateFormat.format(transaction.getTransactionDate())); // date
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public boolean isCellEditable(int row) {
        return row == 0
                || tableModel.get(row).getFinancialTransaction() != tableModel.get(row - 1).getFinancialTransaction();
    }

    @Override
    public boolean setValueAt(Object value, int row) {
        FinancialTransaction transaction = tableModel.get(row).getFinancialTransaction();

        boolean update = false;
        if (value != null) {
            // valid that the day is into month range (date not > to 1rst day of next month)
            Date newDate = validAndGetNewDay(transaction, value);
            if (newDate != null) {
//                Integer newDay = (Integer) value;
                update = (transaction.getTransactionDate().compareTo(newDate) != 0);
                if (update) {
                    transaction.setTransactionDate(newDate);
                    if (!tableModel.updateTransaction(transaction)) {
                        Date previousDate = transaction.getTransactionDate();
                        transaction.setTransactionDate(previousDate);
                        update = false;
                    }
                }
            }
        }

        return update;
    }

    protected Date validAndGetNewDay(FinancialTransaction transaction, Object newDayValue) {
        Date result = null;
        if (newDayValue != null && newDayValue instanceof Integer) {

            Integer newDay = (Integer) newDayValue;

            // valid that the day is into the month range
            if (newDay > 0) {

                Date previousDate = transaction.getTransactionDate();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(previousDate);
                int maximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                if (newDay <= maximum) {

                    calendar.set(Calendar.DAY_OF_MONTH, newDay);
                    result = calendar.getTime();
                } else {
                    result = previousDate;
                }
            }

        }
        return result;
    }
}
