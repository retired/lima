package org.chorem.lima.ui.financialtransaction;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class DateColumn extends AbstractColumn<FinancialTransactionTableModel> {

    public DateColumn(){
        super(Date.class,  t("lima.financialTransaction.date"), true);
    }

    @Override
    public Object getValueAt(int row) {
        Date result;
        Entry entry = tableModel.get(row);
        FinancialTransaction transaction = entry.getFinancialTransaction();
        if (row == 0 || tableModel.get(row - 1).getFinancialTransaction() != transaction) {
            result = transaction.getTransactionDate(); // date
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public boolean isCellEditable(int row) {
        return row == 0
                || tableModel.get(row).getFinancialTransaction() != tableModel.get(row - 1).getFinancialTransaction();
    }

    @Override
    public boolean setValueAt(Object value, int row) {
        FinancialTransaction transaction = tableModel.get(row).getFinancialTransaction();
        boolean update = (transaction.getTransactionDate().compareTo((Date) value) != 0);
        if (update) {
            Date previousDate = transaction.getTransactionDate();
            transaction.setTransactionDate((Date) value);
            if (!tableModel.updateTransaction(transaction)) {
                transaction.setTransactionDate(previousDate);
                update = false;
            }
        }
        return update;
    }
}
