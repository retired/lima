/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialperiod;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.exceptions.NotLockedClosedPeriodicEntryBooksException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.business.utils.FinancialPeriodComparator;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.util.BigDecimalToString;
import org.chorem.lima.util.ErrorHelper;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for FinancialPeriodView.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialPeriodViewHandler implements ServiceListener {

    private static final Log log = LogFactory.getLog(FinancialPeriodViewHandler.class);

    protected FinancialPeriodService financialPeriodService;

    protected FinancialPeriodView view;

    protected ErrorHelper errorHelper;

    protected FinancialPeriodViewHandler(FinancialPeriodView view) {
        this.view = view;
        
        financialPeriodService = LimaServiceFactory.getService(FinancialPeriodService.class);
        LimaServiceFactory.addServiceListener(ImportService.class, this);
        LimaServiceFactory.addServiceListener(FiscalPeriodService.class, this);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
    }

    /**
     * Init view.
     */
    public void init() {

        InputMap inputMap = view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();

        // add action on Ctrl + B
        String binding = "close-FinancialPeriod";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 4933544189561608564L;

            @Override
            public void actionPerformed(ActionEvent e) {
                blockFinancialPeriod();
            }
        });


        // get data from service
        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = financialPeriodService.
                getAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriod();
        closedPeriodicEntryBooks.sort(new FinancialPeriodComparator());
        if (log.isDebugEnabled()) {
            log.debug(String.format("Loaded %d closed periodic entry book", closedPeriodicEntryBooks.size()));
        }

        // notify ui
        view.getFinancialPeriodTableModel().setValues(closedPeriodicEntryBooks);
    }

    /**
     * Block selected period.
     */
    public void blockFinancialPeriod() {
        JXTable financialPeriodeTable = view.getFinancialPeriodTable();
        int selectedRow = financialPeriodeTable.getSelectedRow();
        FinancialPeriodTableModel model = (FinancialPeriodTableModel) view.getFinancialPeriodTable().getModel();

        // blocked it
        ClosedPeriodicEntryBook closedPeriodicEntryBook = model.get(selectedRow);
        int response = JOptionPane.showConfirmDialog(view, t("lima.financialPeriod.block.confirm"),
            t("lima.financialPeriod.block.title"),
            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (response == JOptionPane.YES_OPTION) {
            try {
                closedPeriodicEntryBook = financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);
                model.setValue(selectedRow, closedPeriodicEntryBook);
            } catch (UnbalancedFinancialTransactionsException e) {
                StringBuilder message = new StringBuilder(t("lima.financialPeriod.block.error.unbalanced.main"));
                for (FinancialTransaction transaction : e.getFinancialTransactions()) {
                    message.append("\n    - ");
                    message.append(t("lima.financialPeriod.block.error.unbalanced.transaction",
                            transaction.getTransactionDate(),
                            BigDecimalToString.format(transaction.getAmountDebit()),
                            BigDecimalToString.format(transaction.getAmountCredit())));
                }
                errorHelper.showErrorMessage(message.toString());
            } catch (UnfilledEntriesException e) {
                StringBuilder message = new StringBuilder(t("lima.financialPeriod.block.error.unfilled.main"));
                for (Entry entry : e.getEntries()) {
                    message.append("\n    - ");
                    message.append(t("lima.financialPeriod.block.error.unfilled.entry",
                            entry.getFinancialTransaction().getTransactionDate(),
                            BigDecimalToString.format(entry.getAmount())));
                    if (entry.getAccount() == null) {
                        message.append("\n        - ");
                        message.append(t("lima.financialPeriod.block.error.unfilled.noAccount"));
                    }
                    if (StringUtils.isBlank(entry.getVoucher())) {
                        message.append("\n        - " );
                        message.append(t("lima.financialPeriod.block.error.unfilled.noVoucher"));
                    }
                    if (StringUtils.isBlank(entry.getDescription())) {
                        message.append("\n        - ");
                        message.append(t("lima.financialPeriod.block.error.unfilled.noDescription"));
                    }
                }
                errorHelper.showErrorMessage(message.toString());
            } catch (WithoutEntryBookFinancialTransactionsException e) {
                StringBuilder message = new StringBuilder(t("lima.financialPeriod.block.error.withoutEntryBook.main"));
                for (FinancialTransaction transaction : e.getFinancialTransactions()) {
                    message.append("\n    - ");
                    message.append(t("lima.financialPeriod.block.error.withoutEntryBook.transaction",
                            transaction.getTransactionDate(),
                            BigDecimalToString.format(transaction.getAmountCredit())));
                }
                errorHelper.showErrorMessage(message.toString());
            } catch (NotLockedClosedPeriodicEntryBooksException e) {
                StringBuilder message = new StringBuilder(t("lima.financialPeriod.block.error.noLockedPreviousEntryBook.main"));
                for (ClosedPeriodicEntryBook previousClosedPeriodicEntryBook : e.getClosedPeriodicEntryBooks()) {
                    message.append("\n    - ");
                    message.append(t("lima.financialPeriod.block.error.noLockedPreviousEntryBook.previousEntryBook",
                            previousClosedPeriodicEntryBook.getFinancialPeriod().getBeginDate(),
                            previousClosedPeriodicEntryBook.getFinancialPeriod().getEndDate()));
                }
                errorHelper.showErrorMessage(message.toString());
            }
        }
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        if (serviceName.contains("FiscalPeriod") || methodName.contains("importAll")) {
            init();
        }
    }
}
