/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialtransactionsearch;

import org.chorem.lima.beans.FinancialTransactionCondition;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.ui.Filter.AccountCondition.AccountConditionView;
import org.chorem.lima.ui.Filter.BigDecimalCondition.CreditConditionView;
import org.chorem.lima.ui.Filter.BigDecimalCondition.DebitConditionView;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.Filter.EntryBookCondition.EntryBookConditionView;
import org.chorem.lima.ui.Filter.StringCondition.DescriptionConditionView;
import org.chorem.lima.ui.Filter.StringCondition.LetteringConditionView;
import org.chorem.lima.ui.Filter.StringCondition.VoucherConditionView;
import org.chorem.lima.ui.Filter.dateCondition.DateConditionView;
import org.chorem.lima.ui.Filter.dateIntervalCondition.DateIntervalConditionView;
import org.chorem.lima.ui.Filter.financialPeriodCondition.FinancialPeriodConditionView;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionView;
import org.chorem.lima.ui.Filter.fiscalPeriodCondition.FiscalPeriodConditionView;
import org.chorem.lima.ui.financialtransaction.FinancialTransactionDefaultTable;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Handler associated with financial transaction view.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class FinancialTransactionSearchViewHandler {

    protected FinancialTransactionSearchView view;

    protected FinancialTransactionSearchTableModel tableModel;

    /** Transaction service. */
    protected final FinancialTransactionService financialTransactionService;

    protected FinancialTransactionSearchViewHandler(FinancialTransactionSearchView view) {
        this.view = view;

        /* Services */
        financialTransactionService =
                LimaServiceFactory.getService(
                        FinancialTransactionService.class);

        initShortCuts();

    }
    protected void initShortCuts() {

        InputMap inputMap= view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();
        Object binding;

        // add action on F5
        binding = "refresh";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 5877340693755869036L;

            @Override
            public void actionPerformed(ActionEvent e) {
                refresh();
            }
        });
    }

    protected void addCondition(ConditionHandler conditionHandler) {
        FinancialTransactionConditionView filterView = view.getFinancialTransactionConditionView();
        FinancialTransactionConditionHandler handler = filterView.getHandler();
        handler.addCondition(conditionHandler);

        // cf #1193 La zone des critères de recherche apparait réduite,
        // les critères de recherche ne sont donc pas visibles, il faudrait définir une taille minimale
        JSplitPane financialTransactionSplitpane = view.getFinancialTransactionSplitpane();
        int dividerLocation = financialTransactionSplitpane.getDividerLocation();

        int height = view.getFinancialTransactionConditionScrollPane().getPreferredSize().height + 5;
        if (dividerLocation < height) {
            financialTransactionSplitpane.setDividerLocation(height);
        }

        view.validate();
        view.repaint();
    }

    public void addDateCondition() {
        addCondition(new DateConditionView().getHandler());
    }

    public void addDateIntervalCondition() {
        addCondition(new DateIntervalConditionView().getHandler());
    }

    public void addFinancialPeriodCondition() {
        addCondition(new FinancialPeriodConditionView().getHandler());
    }

    public void addFiscalPeriodCondition() {
        addCondition(new FiscalPeriodConditionView().getHandler());
    }

    public void addEntryBookCondition() {
        addCondition(new EntryBookConditionView().getHandler());
    }

    public void addVoucherCondition() {
        addCondition(new VoucherConditionView().getHandler());
    }

    public void addDescriptionCondition() {
        addCondition(new DescriptionConditionView().getHandler());
    }

    public void addLetteringCondition() {
        addCondition(new LetteringConditionView().getHandler());
    }

    public void addAccountCondition() {
        addCondition(new AccountConditionView().getHandler());
    }

    public void addDebitCondition() {
        addCondition(new DebitConditionView().getHandler());
    }

    public void addCreditCondition() {
        addCondition(new CreditConditionView().getHandler());
    }

    public void refresh() {
        FinancialTransactionDefaultTable table = view.getFinancialTransactionSearchTable();
        table.exit();
        FinancialTransactionConditionView conditionView =  view.getFinancialTransactionConditionView();
        FinancialTransactionConditionHandler conditionHandler = conditionView.getHandler();
        FinancialTransactionCondition condition= conditionHandler.getFilter();
        tableModel = view.getFinancialTransactionSearchTableModel();
        if (tableModel != null) {
            tableModel.refresh(condition);
        }

    }

}
