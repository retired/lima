/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.home;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.ui.MainView;

import javax.swing.event.HyperlinkEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Home view pane that display statistics about unbalanced transactions.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialTransactionsPane extends AbstractHomePane implements ServiceListener {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(FinancialTransactionsPane.class);

    protected FinancialTransactionService financialTransactionService;

    protected FiscalPeriodService fiscalPeriodService;
    
    protected EntryBookService entryBookService;

    public FinancialTransactionsPane(HomeView view) {
        super(view);

        financialTransactionService = LimaServiceFactory.getService(FinancialTransactionService.class);
        fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);
        entryBookService = LimaServiceFactory.getService(EntryBookService.class);
        LimaServiceFactory.addServiceListener(FinancialTransactionService.class, this);
        LimaServiceFactory.addServiceListener(FiscalPeriodService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);

        refresh();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType())) {

            if (e.getDescription().equals("#financialtransactionunbalanced")) {
                MainView ui = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                ui.getHandler().showTransactionUnbalancedView(ui);
            } else if (e.getDescription().equals("#financialtransactionbalanced")) {
                MainView ui = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                ui.getHandler().showTransactionView(ui);
            }
        }
    }

    public void refresh() {
        
        log.debug("Rafraîchissement financial transaction pane");
        
        String htmlBegin = "<font face='sans-serif' size=3>"
            + "<p style=vertical-align:'bottom', horizontal-align:'center'>";
        String htmlEnd = "</p></font>";

        List<FiscalPeriod> unblockedFiscalPeriods =
            fiscalPeriodService.getAllUnblockedFiscalPeriods();
        List<EntryBook> entryBooks = entryBookService.getAllEntryBooks();

        //Au moins un exercice ouvert et un journal ouvert
        if (!unblockedFiscalPeriods.isEmpty() && !entryBooks.isEmpty()) {
            List<FinancialTransaction> financialTransactionsInexact =
                financialTransactionService.
                    getAllInexactFinancialTransactions(unblockedFiscalPeriods.get(0));
            if (!financialTransactionsInexact.isEmpty()) {
                setBackground(RED_BACKGROUND);
                String transactionsString = t("lima.home.entries.error", financialTransactionsInexact.size())
                                            + "<br/><br/><a href='#financialtransactionunbalanced'>"
                                            + t("lima.home.entries.error.modify") + "</a>";
                //set Text
                setText(htmlBegin + transactionsString + htmlEnd);
            } else {
                List<FinancialTransaction> financialTransactionsBal =
                        financialTransactionService.getAllFinancialTransactionsBalanced(unblockedFiscalPeriods.get(0));

                if (!financialTransactionsBal.isEmpty()) {
                    setBackground(GREEN_BACKGROUND);
                    String transactionsString = t("lima.home.entries.balanced", financialTransactionsBal.size())
                                                + "<br/><br/><a href='#financialtransactionbalanced'>"
                                                + t("lima.home.entries.modify") + "</a>";
                    setText(htmlBegin + transactionsString + htmlEnd);
                } else {
                    setBackground(RED_BACKGROUND);
                    String transactionsString = t("lima.home.entries.nothing")
                                                + "<br/><br/><a href='#financialtransactionbalanced'>"
                                                + t("lima.home.entries.create") + "</a>";
                    setText(htmlBegin + transactionsString + htmlEnd);
                }

            }
        }else {
            setBackground(RED_BACKGROUND);
            setText("");
        }
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        log.debug("Nom de la méthode : " + methodName);
        if (methodName.contains("importEntries") || methodName.contains("importAll") || methodName.contains("importAs")) {
            refresh();
        }
    }

}
