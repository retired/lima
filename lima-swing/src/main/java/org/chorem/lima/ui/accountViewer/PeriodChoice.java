package org.chorem.lima.ui.accountViewer;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 15/09/15.
 */
public enum PeriodChoice {

    FINANCIAL_PERIOD, FISCAL_PERIOD, DATE;

    @Override
    public String toString() {

        String result;

        switch (this) {
            case FINANCIAL_PERIOD:
                result = t("lima.filter.condition.periodChoice.financialPeriod");
                break;
            case FISCAL_PERIOD:
                result = t("lima.filter.condition.periodChoice.fiscalPeriod");
                break;
            case DATE:
                result = t("lima.filter.condition.periodChoice.date");
                break;
            default:
                result = t("lima.filter.condition.periodChoice.financialPeriod");
        }

        return  result;

    }
}
