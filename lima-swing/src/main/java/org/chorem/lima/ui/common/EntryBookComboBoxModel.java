/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.common;

import org.chorem.lima.entity.EntryBook;

/**
 * Entry book combo model.
 *
 * @author chatellier
 * @version $Revision$
 */
public class EntryBookComboBoxModel extends GenericComboBoxModel<EntryBook> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 6991293987668268456L;
    
    /*By default, selection of the first entryBook (Because a 
    * transaction without an entrybook is forbidden)
    */
    @Override
    public Object getElementAt(int index) {
        if (getSelectedItem() == null){
            setSelectedItem(objects.get(0));
            return objects.get(0);
        }
        return objects.get(index);
    }
}
