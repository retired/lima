package org.chorem.lima.ui.financialtransaction;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.ui.celleditor.AccountTableCellEditor;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class AccountColumn extends AbstractColumn<FinancialTransactionTableModel> {

    protected static final Log log = LogFactory.getLog(AccountColumn.class);

    public AccountColumn() {
        super(Account.class, t("lima.entry.account"), true);
        setCellEditor(new AccountTableCellEditor());
    }

    @Override
    public Object getValueAt(int row) {
        Entry entry = tableModel.get(row);
        return entry.getAccount();
    }

    @Override
    public boolean setValueAt(Object value, int row) {
        Entry entry = tableModel.get(row);

        Account selectedAccount = getSelectedAccount(value);
        Account previousAccount = entry.getAccount();

        boolean update = isValueChange(selectedAccount, previousAccount);

        if (update) {
            entry.setAccount(selectedAccount);
            boolean updateSucceed = tableModel.updateEntry(entry);
            if (updateSucceed) {
                setDescriptionFieldIfNone(entry, selectedAccount);
            } else {
                update = revertAccountChange(entry, previousAccount);
            }
        }

        return update;
    }

    protected Account getSelectedAccount(Object value) {
        Account newAccount =  null;
        if (value != null && value instanceof Account) {
            newAccount = (Account) value;
        }
        return newAccount;
    }

    protected boolean isValueChange(Account newAccount, Account previousAccount) {
        boolean update;
        if (previousAccount == null) {
            update = newAccount != null;
        } else {
            update = newAccount == null || !previousAccount.getAccountNumber().equals(newAccount.getAccountNumber());
        }
        return update;
    }

    protected void setDescriptionFieldIfNone(Entry entry, Account account) {
        if (StringUtils.isBlank(entry.getDescription()) && account != null && StringUtils.isNotBlank(account.getLabel())) {
            entry.setDescription(account.getLabel());
        }
    }

    protected boolean revertAccountChange(Entry entry, Account previousAccount) {
        entry.setAccount(previousAccount);
        return false;
    }
}
