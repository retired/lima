/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.home;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.ui.MainView;

import javax.swing.event.HyperlinkEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Home view pane that display account plan statistics.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class AccountsPane extends AbstractHomePane implements ServiceListener {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(AccountsPane.class);

    protected AccountService accountService;

    public AccountsPane(HomeView view) {
        super(view);

        accountService = LimaServiceFactory.getService(AccountService.class);
        LimaServiceFactory.addServiceListener(AccountService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);

        refresh();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType())) {
            if (e.getDescription().equals("#accountschart")) {
                MainView ui = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                ui.getHandler().showAccountView(ui);
            }
        }
    }

    public void refresh() {

        log.debug("Rafraîchissement accounts pane");
        
        String htmlBegin = "<font face='sans-serif' size=3>"
                           + "<p style=vertical-align:'bottom', horizontal-align:'center'>";
        String htmlEnd = "</p></font>";

        long accountCount = accountService.getAccountCount();
        if (accountCount > 0) {
            setBackground(GREEN_BACKGROUND);
            String accountsString = t("lima.home.accounts.info", accountCount)
                                    + "<br/><br/><a href='#accountschart'>"
                                    + t("lima.home.accounts.modify") + "</a>";
            //set Text
            setText(htmlBegin + accountsString + htmlEnd);
        } else {
            setBackground(RED_BACKGROUND);
            String accountsString = t("lima.home.accounts.nothing")
                                    + "<br/><br/><a href='#accountschart'>"
                                    + t("lima.home.accounts.create") + "</a>";
            //set Text
            setText(htmlBegin + accountsString + htmlEnd);
        }
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        log.debug("Nom de la méthode : " + methodName);
        if (methodName.contains("Account") || methodName.contains("importAll") || methodName.contains("importAs")) {
            refresh();
        }
    }
}
