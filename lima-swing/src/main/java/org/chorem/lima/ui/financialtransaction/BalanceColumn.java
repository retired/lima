package org.chorem.lima.ui.financialtransaction;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;

import java.math.BigDecimal;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class BalanceColumn extends AbstractColumn<FinancialTransactionTableModel> {

    public BalanceColumn() {
        super(BigDecimal.class, t("lima.table.balance"), false);
    }

    @Override
    public Object getValueAt(int row) {
        BigDecimal balance = BigDecimal.ZERO;
        balance = getRowBalance(row, balance);

        if (row > 0) {
            balance = addPreviousTransactionEntryBalance(row, balance);
        }
        return balance;
    }

    protected BigDecimal getRowBalance(int row, BigDecimal result) {
        Entry currentEntry = tableModel.get(row);
        result = currentEntry.isDebit() ? result.add(currentEntry.getAmount()) : result.subtract(currentEntry.getAmount());
        return result;
    }

    protected BigDecimal addPreviousTransactionEntryBalance(int row, BigDecimal result) {
        FinancialTransaction rowTransaction = tableModel.getTransactionAt(row);

        int i = 1;
        while (isSameTransaction(row, rowTransaction, i)) {
            result = getRowBalance(row - i, result);
            i++;
        }

        return result;
    }

    protected boolean isSameTransaction(int row, FinancialTransaction transaction, int i) {
        return row - i >= 0 && tableModel.get(row - i).getFinancialTransaction() == transaction;
    }

}
