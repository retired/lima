/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialtransactionunbalanced;

import com.google.common.collect.Lists;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.ui.common.FinancialTransactionTableModel;
import org.chorem.lima.ui.financialtransaction.AccountColumn;
import org.chorem.lima.ui.financialtransaction.CreditColumn;
import org.chorem.lima.ui.financialtransaction.DateColumn;
import org.chorem.lima.ui.financialtransaction.DebitColumn;
import org.chorem.lima.ui.financialtransaction.DescriptionColumn;
import org.chorem.lima.ui.financialtransaction.EntryBookColumn;
import org.chorem.lima.ui.financialtransaction.VoucherColumn;

import java.util.List;

/**
 * Basic transaction table model.
 * <p/>
 * Le modele est filtré sur {@link #selectedFiscalPeriod}(montée en charge !).
 *
 * @author ore
 * @author chatellier
 */
public class FinancialTransactionUnbalancedTableModel extends FinancialTransactionTableModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 3914954536809622358L;

    /** selected financial period */
    protected FiscalPeriod selectedFiscalPeriod;

    @Override
    protected void initColumn() {
        addColumn(new DateColumn());
        addColumn(new EntryBookColumn());
        addColumn(new VoucherColumn());
        addColumn(new AccountColumn());
        addColumn(new DescriptionColumn());
        addColumn(new DebitColumn());
        addColumn(new CreditColumn());
    }

    /**
     * Le model est une combinaison de Transaction/entries.
     */
    protected void refresh() {
        clear();

        if (selectedFiscalPeriod != null) {
            List<FinancialTransaction> financialTransactions =
                    financialTransactionService.getAllInexactFinancialTransactions(selectedFiscalPeriod);
            List<Entry> entries = Lists.newLinkedList();

            for (FinancialTransaction transaction : financialTransactions) {
                entries.addAll(transaction.getEntry());
            }

            addAll(entries);
        }
    }

    public void setFiscalPeriod(FiscalPeriod fiscalPeriod) {
        selectedFiscalPeriod = fiscalPeriod;
    }
}
