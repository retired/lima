package org.chorem.lima.ui.opening;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.IdentityImpl;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CreateIdentityPanelHandler {

    protected IdentityService identityService;

    protected CreateIdentityPanel view;


    public CreateIdentityPanelHandler(CreateIdentityPanel view) {
        this.view = view;
        identityService =
                LimaServiceFactory.getService(
                        IdentityService.class);
    }

    public Identity getIdentity() {
        Identity identity = identityService.getIdentity();
        if (identity == null) {
            identity = new IdentityImpl();
        }
        return identity;
    }

    public void updateIdentity(){
        Identity identity = getIdentity();

        identity.setName(view.getNameTextField().getText());
        identity.setDescription(view.getDescriptionTextField().getText());
        identity.setAddress(view.getAddressTextField().getText());
        identity.setZipCode(view.getZipCodeTextField().getText());
        identity.setCity(view.getCityTextField().getText());
        identity.setBusinessNumber(view.getBusinessNumberTextField().getText());
        identity.setVatNumber(view.getVatNumberTextField().getText());
        identity.setClassificationCode(view.getClassificationCodeTextField().getText());
        identity.setPhoneNumber(view.getPhoneNumberTextField().getText());
        identity.setEmail(view.getEmailTextField().getText());

        identityService.updateIdentity(identity);
    }

}
