package org.chorem.lima.ui.Filter.AccountCondition;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.AccountCondition;
import org.chorem.lima.entity.Account;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;
import org.chorem.lima.ui.combobox.AccountComboBox;

import java.awt.event.ItemEvent;
import java.beans.PropertyChangeEvent;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class AccountConditionHandler implements ConditionHandler {

    protected AccountCondition condition;

    protected AccountConditionView view;

    protected FinancialTransactionConditionHandler filterHandler;

    public AccountConditionHandler (AccountConditionView view) {
        this.view = view;
        this.condition = new AccountCondition();
    }

    public Account getAccount() {
        return condition.getAccount();
    }

    public void setAccount(Account account) {
        condition.setAccount(account);
    }

    public void accountSelected(ItemEvent event) {
        condition.setAccount((Account) event.getItem());
    }

    public void delete() {
        filterHandler.removeCondition(this);
    }

    @Override
    public AccountCondition getCondition() {
        return condition;
    }

    @Override
    public AccountConditionView getView() {
        return view;
    }

    @Override
    public void setFilterHandler(FinancialTransactionConditionHandler filterHandler) {
        this.filterHandler = filterHandler;
    }

    public void accountComboBoxChange(PropertyChangeEvent event) {
        if (event.getPropertyName().equals(AccountComboBox.PROPERTY_SELECTED_ITEM)) {
            Account account = null;
            if (event.getNewValue() != null && event.getNewValue() instanceof Account) {
                account = (Account) event.getNewValue();
            }
            setAccount(account);
        }
    }
}
