package org.chorem.lima.ui.common;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.*;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class CellRouteVerticalAction extends CellRouteHorizontalAction {

    private static final long serialVersionUID = -4984679970177484506L;

    public CellRouteVerticalAction(JTable table, boolean nextCell) {
        super(table, nextCell);
    }

    protected void init() {
        route1 = table.getSelectedRow();
        route2 = table.getSelectedColumn();
        nbCell1 = table.getRowCount();
        nbCell2 = table.getColumnCount();
    }

    protected int getRow() {
        return route1;
    }

    protected int getColumn() {
        return route2;
    }

}
