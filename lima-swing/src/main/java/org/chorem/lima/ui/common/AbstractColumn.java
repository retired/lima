package org.chorem.lima.ui.common;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.util.ErrorHelper;

import javax.swing.table.TableCellEditor;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public abstract class AbstractColumn<T extends AbstractLimaTableModel> implements Column<T> {

    protected T tableModel;

    protected Class<?> columnClass;

    protected String columnName;

    protected boolean editable;

    protected TableCellEditor cellEditor;

    protected ErrorHelper errorHelper;

    public AbstractColumn(Class<?> columnClass, String columnName, boolean editable) {
        this.columnClass = columnClass;
        this.columnName = columnName;
        this.editable = editable;
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
    }

    public Class<?> getColumnClass() {
        return columnClass;
    }

    @Override
    public void setTableModel(T tableModel) {
        this.tableModel = tableModel;
    }

    @Override
    public boolean isCellEditable(int row) {
        return editable;
    }

    @Override
    public boolean setValueAt(Object value, int row) {
        return false;
    }

    public void setColumnClass(Class<?> columnClass) {
        this.columnClass = columnClass;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public TableCellEditor getCellEditor() {
        return cellEditor;
    }

    public void setCellEditor(TableCellEditor cellEditor) {
        this.cellEditor = cellEditor;
    }
}
