package org.chorem.lima.ui.lettering;
/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.entity.Entry;

import javax.swing.*;
import java.beans.PropertyChangeSupport;
import java.util.List;

/**
 * @author sletellier <letellier@codelutin.com>
 */
public class LetteringSelectionModel extends DefaultListSelectionModel{

    protected LetteringTableModel letteringTableModel;
    protected Entry entry;
    protected int lineSelected;
    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public LetteringSelectionModel(LetteringTableModel letteringTableModel){
        this.letteringTableModel = letteringTableModel;
    }

    @Override
    public void addSelectionInterval(int row, int column) {
        setSelectionInterval(row, column);
    }

    @Override
    public void setSelectionInterval(int row, int column) {
        if (!letteringNotExist(row)) {

            //lettered entries
            if ( isSelectionEmpty() || !isSelectedIndex(row)){
                clearSelection();
                lineSelected = row;
                String currentLettering = getCurrentLettering();

                //select entries with the same letter of the selected entry
                for(Entry entry : getEntries()){
                    if (StringUtils.isNotBlank(entry.getLettering())){
                        if (entry.getLettering().equals(currentLettering)){
                            int entryToSelect = letteringTableModel.indexOf(entry);
                            super.addSelectionInterval(entryToSelect, entryToSelect);
                        }
                    }
                }
            }
        }
        else {

            //unlettered entries
            //To clear the selection when it changes from lettered entry to unlettered
            for(Entry entry : getEntries()){
                if (!StringUtils.isBlank(entry.getLettering())) {
                    int entryToSelect = letteringTableModel.indexOf(entry);
                    super.removeSelectionInterval(entryToSelect, entryToSelect);
                }
            }

            if (isSelectionEmpty() || !isSelectedIndex(row)){
                super.addSelectionInterval(row, column);
            }else {
                super.removeSelectionInterval(row, column);
            }

        }
    }

    /**return true if lettering is null, or not null but empty
     * @param row index of the line to test
     * @return boolean
     * */
    public boolean letteringNotExist(int row){
        boolean emptyOrNull;
        entry = letteringTableModel.get(row);
        String lettering = entry.getLettering();
        emptyOrNull = (lettering==null||lettering.isEmpty());
        return emptyOrNull;
    }

    public List<Entry> getEntries(){
        return letteringTableModel.getValues();
    }

    public String getCurrentLettering(){
        return getCurrentEntrySelected().getLettering();
    }

    public Entry getCurrentEntrySelected(){
        return letteringTableModel.get(lineSelected);
    }

    /**After rounding one of two entries, selection of its, and of the new entry,
     * resulting of rounding*/
    public void selectRoundedAndNewEntries(int indexFirstRoundedEntry, int indexSecondRoundedEntry, Entry newResultRoundedEntry) {
        if (!isSelectedIndex(indexFirstRoundedEntry)) {
            addSelectionInterval(indexFirstRoundedEntry, indexFirstRoundedEntry);
        }
        if (!isSelectedIndex(indexSecondRoundedEntry)) {
            addSelectionInterval(indexSecondRoundedEntry, indexSecondRoundedEntry);
        }
        /*New entry*/
        int newEntryIndex = letteringTableModel.indexOf(newResultRoundedEntry);
        addSelectionInterval(newEntryIndex, newEntryIndex);
    }

    @Override
    public int getSelectionMode() {
        return MULTIPLE_INTERVAL_SELECTION;
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }
}
