/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.combobox;

import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.business.LimaServiceFactory;

import javax.swing.*;
import java.util.List;

/**
 * Opened financial period combo box model.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class EntryBookComboBoxModel extends AbstractListModel implements ComboBoxModel, ServiceListener {

    private static final long serialVersionUID = 1L;

    protected Object selectedEntryBook;

    protected EntryBookService entryBookService;

    protected List<EntryBook> cacheDatas;

    public EntryBookComboBoxModel() {
        entryBookService =
                LimaServiceFactory.getService(EntryBookService.class);
        LimaServiceFactory.addServiceListener(EntryBookService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);
        cacheDatas = getDataList();
    }

    @Override
    public int getSize() {
        return cacheDatas.size();
    }

    @Override
    public Object getElementAt(int index) {
        return cacheDatas.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedEntryBook = anItem;
        fireContentsChanged(this, -1, -1);
    }

    @Override
    public Object getSelectedItem() {
        return selectedEntryBook;
    }

    /** get the accounts list */
    public List<EntryBook> getDataList() {
        List<EntryBook> result = entryBookService.getAllEntryBooks();
        return result;
    }


    public void refresh() {
        cacheDatas = getDataList();
        fireContentsChanged(this, 0, cacheDatas.size());
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        if (methodName.contains("EntryBook") || methodName.contains("importAll")) {
            refresh();
        }
    }
}
