package org.chorem.lima.ui.Filter.StringCondition;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.AbstractStringCondition;
import org.chorem.lima.beans.Condition;
import org.chorem.lima.beans.VoucherCondition;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class StringConditionHandler implements ConditionHandler {

    protected StringConditionView view;

    protected AbstractStringCondition condition;

    protected FinancialTransactionConditionHandler filterHandler;

    public StringConditionHandler(StringConditionView view) {
        this.view = view;
        this.condition = new VoucherCondition();
    }

    public String getText() {
        return condition.getValue();
    }

    public void setText(String text) {
        condition.setValue(text);
    }

    public void setText(KeyEvent event) {
        String text = view.getTextField().getText();
        setText(text);
    }

    public void setOperand(AbstractStringCondition.Operand operand) {
        condition.setOperand(operand);
    }

    public AbstractStringCondition.Operand getOperand() {
        return condition.getOperand();
    }

    public boolean isSensitiveCase() {
        return condition.isSensitiveCase();
    }

    public void setSensitiveCase(boolean sensitiveCase) {
        condition.setSensitiveCase(sensitiveCase);
    }

    public void setSensitiveCase(ActionEvent event) {
        boolean selected = view.getSensitiveCaseCheckBox().isSelected();
        setSensitiveCase(selected);
    }

    public void delete() {
        filterHandler.removeCondition(this);
    }

    public void operandSelected(ItemEvent event) {
        condition.setOperand((AbstractStringCondition.Operand) event.getItem());
    }

    @Override
    public Condition getCondition() {
        return condition;
    }

    public StringConditionView getView() {
        return view;
    }

    public void setFilterHandler(FinancialTransactionConditionHandler filterHandler) {
        this.filterHandler = filterHandler;
    }
}
