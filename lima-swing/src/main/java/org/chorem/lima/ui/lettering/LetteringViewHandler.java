/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.lettering;

import com.google.common.collect.Lists;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.beans.LetteringFilterImpl;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.UnbalancedEntriesException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.ui.combobox.AccountComboBox;
import org.chorem.lima.util.BigDecimalToString;
import org.chorem.lima.util.ErrorHelper;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;


/**
 * Handler associated with financial transaction view.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class LetteringViewHandler{

    protected LetteringView view;

    /** Transaction service. */
    protected FiscalPeriodService fiscalPeriodService;
    protected FinancialPeriodService financialPeriodService;
    protected AccountService accountService;
    protected FinancialTransactionService financialTransactionService;
    protected EntryBookService entryBookService;

    protected LetteringFilterImpl filter;

    protected BigDecimal debit = BigDecimal.ZERO;
    protected BigDecimal credit = BigDecimal.ZERO;
    protected BigDecimal solde = BigDecimal.ZERO;
    protected LetteringSelectionModel letteringSelectionModel;
    
    protected LetteringEditModel editModel;

    protected List<Entry> unlockedEntries;

    protected ErrorHelper errorHelper;

    protected enum ButtonMode {DELETTERED, LETTERED, EQUALIZED, ALL}
    private static final Log log = LogFactory.getLog(LetteringViewHandler.class);


    protected boolean initializationComplete;

    public LetteringViewHandler(LetteringView view) {
        initializationComplete = false;
        this.view = view;
        initShortCuts();
        financialPeriodService = LimaServiceFactory.getService(FinancialPeriodService.class);
        fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);
        accountService = LimaServiceFactory.getService(AccountService.class);
        financialTransactionService = LimaServiceFactory.getService(FinancialTransactionService.class);
        entryBookService = LimaServiceFactory.getService(EntryBookService.class);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
    }

    /**
     * Init all combo box in view.
     */
    public void init() {
        filter = new LetteringFilterImpl();
        editModel = view.getEditModel();
        letteringSelectionModel = view.getLetteringSelectionModel();
        loadComboAndRows();

        editModel.addPropertyChangeListener(LetteringEditModel.PROPERTY_DEBIT, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updateSoldStatus();
            }
        });

        editModel.addPropertyChangeListener(LetteringEditModel.PROPERTY_CREDIT, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updateSoldStatus();
            }
        });

        editModel.addPropertyChangeListener(LetteringEditModel.PROPERTY_SOLD, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updateSoldStatus();
            }
        });

        initializationComplete = true;
        SwingUtil.fixTableColumnWidth(view.getTable(), 0, 40);

        updateSoldStatus();
        updateAllEntries();


    }

    public void updateSoldStatus() {
        view.getBalanceStatusLabel().setText(t("lima.lettering.balanceStatus",
                BigDecimalToString.format(editModel.getTableDebit()),
                BigDecimalToString.format(editModel.getTableCredit()),
                BigDecimalToString.format(editModel.getTableSold()),
                BigDecimalToString.format(editModel.getDebit()),
                BigDecimalToString.format(editModel.getCredit()),
                BigDecimalToString.format(editModel.getSold())));
    }

    protected void initShortCuts() {

        InputMap inputMap= view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();
        Object binding;

        //To block reaction of the dual key 'ctrl+a' (Selection of all lines)
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK), "none");

        // add action on Ctrl + L
        binding = "lettering";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 397305388204489988L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addLetter();
            }
        });

        // add action on Ctrl + Delete
        binding = "un-lettering";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 6493175994438339351L;

            @Override
            public void actionPerformed(ActionEvent e) {
                removeLetter();
            }
        });

        // add action on Ctrl + B
        binding = "balance";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 5997811877503911744L;

            @Override
            public void actionPerformed(ActionEvent e) {
                roundAndCreateEntry();
            }
        });

        // refresh
        binding = "refresh";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -7192846839712951680L;

            @Override
            public void actionPerformed(ActionEvent e) {
                updateAllEntries();
            }
        });
    }

    public void balanceAndActions() {
        if (log.isDebugEnabled()) {
            log.debug("balanceAndActions");
        }
        if (view.getTable().getSelectedRows().length == 0) {
            onButtonModeChanged(ButtonMode.ALL);
            onBalanceChanged(null);
        } else if (!letteringNotExist(view.getTable().getSelectedRow())) {

            //lettered entries
            onBalanceChanged(null);
            setValuesForSelectedEntries();

            //For U.I. buttons (Lettering and unlettering)
            onButtonModeChanged(ButtonMode.DELETTERED);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("unlettered entries");
            }
            int[] selectedRows = view.getTable().getSelectedRows();
            if (selectedRows.length == 2) {
                if (log.isDebugEnabled()) {
                    log.debug("2 rows selected");
                }
                /*Treatment only if one of values contains decimals*/
                LetteringTableModel tableModel = view.getTableModel();
                Entry firstSelectedEntry = tableModel.get(selectedRows[0]);
                Entry secondSelectedEntry = tableModel.get(selectedRows[1]);

                /*Get decimals*/
                BigDecimal firstSelectedEntryAmount = firstSelectedEntry.getAmount();
                BigDecimal secondSelectedEntryAmount = secondSelectedEntry.getAmount();

                if ( secondSelectedEntry.isDebit() != firstSelectedEntry.isDebit()
                     && (firstSelectedEntryAmount.subtract(secondSelectedEntryAmount).abs().compareTo(BigDecimal.ZERO) >0
                         && firstSelectedEntryAmount.subtract(secondSelectedEntryAmount).abs().compareTo(BigDecimal.ONE) <0) ) {
                    onButtonModeChanged(ButtonMode.EQUALIZED);
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("!2 rows selected");
                }
                onButtonModeChanged(ButtonMode.ALL);
            }

            //Unlettered entries
            onBalanceChanged(null);
            //treatment useful if no rows are selected
            if (!view.getLetteringSelectionModel().isSelectionEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("Rows selected");
                }
                setValuesForSelectedEntries();
                onButtonModeChanged(ButtonMode.LETTERED);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No Rows selected");
                }
                onButtonModeChanged(ButtonMode.ALL);
            }
        }
    }

    /**return true if lettering is null, or not null but empty
     * @param row index of the line to test
     * @return boolean
     * */
    public boolean letteringNotExist(int row){
        boolean emptyOrNull = false;
        if (row != -1) {
            Entry entry = view.getTableModel().get(row);
            String lettering = entry.getLettering();
            emptyOrNull = (lettering==null||lettering.isEmpty());
        }
        return emptyOrNull;
    }

    public void onButtonModeChanged(ButtonMode buttonMode) {

        switch (buttonMode) {
            case DELETTERED:
                editModel.setLettered(false);
                editModel.setUnLettered(true);
                break;
            case LETTERED:
                editModel.setUnLettered(false);
                editModel.setLettered(true);
                break;
            case EQUALIZED:
                editModel.setEqualized(true);
                break;
            default:
                editModel.setLettered(false);
                editModel.setUnLettered(false);
                editModel.setEqualized(false);
        }
    }

    public void setValuesForSelectedEntries() {
        boolean isEditable = true;
        Entry selectedEntry;
        LetteringTableModel tableModel = view.getTableModel();
        for (int i = 0; i < tableModel.getRowCount(); i ++){
            if (view.getLetteringSelectionModel().isSelectedIndex(i)){
                selectedEntry = tableModel.get(i);
                //Set values for calculation (By LetteringEditModel) of balance
                if (!view.getTableModel().getUnlockedEntries().contains(selectedEntry)) {
                    isEditable = false;
                }
                onBalanceChanged(selectedEntry);
            }
        }
        editModel.setEditable(isEditable);
    }

    public void setValuesForEntries() {
        if (editModel != null) {
            Entry resultDebit = new EntryImpl();
            Entry resultCredit = new EntryImpl();
            Entry resultSold = new EntryImpl();

            resultDebit.setAmount(BigDecimal.ZERO);
            resultCredit.setAmount(BigDecimal.ZERO);
            resultSold.setAmount(BigDecimal.ZERO);

            LetteringTableModel tableModel = view.getTableModel();
            for (int i = 0; i < tableModel.getRowCount(); i ++){
                Entry rowEntry = tableModel.get(i);
                if (rowEntry.isDebit()) {
                    resultDebit.setAmount(resultDebit.getAmount().add(rowEntry.getAmount()));
                    resultSold.setAmount(resultSold.getAmount().subtract(rowEntry.getAmount()));
                } else {
                    resultCredit.setAmount(resultCredit.getAmount().add(rowEntry.getAmount()));
                    resultSold.setAmount(resultSold.getAmount().add(rowEntry.getAmount()));
                }
            }
            resultSold.setDebit(resultSold.getAmount().compareTo(BigDecimal.ZERO) < 0);

            editModel.setTableDebit(resultDebit.getAmount());
            editModel.setTableCredit(resultCredit.getAmount());
            editModel.setTableSold(resultSold.getAmount());
            updateSoldStatus();
        }
    }

    public void onBalanceChanged(Entry balance) {
        if (balance == null) {
            editModel.setCredit(BigDecimal.ZERO);
            editModel.setDebit(BigDecimal.ZERO);
            editModel.setSold(BigDecimal.ZERO, false);
        } else {
            balanceCalculation(balance.getAmount(), balance.isDebit());
        }
    }

    /**Allow to add / subtract credit / debit and balance
     * @param amount debit or credit
     * @param debit it indicate if amount is debit or not
     * */
    public void balanceCalculation(BigDecimal amount, boolean debit){

        BigDecimal debitVal = debit ? amount : BigDecimal.ZERO;
        BigDecimal creditVal = debit ? BigDecimal.ZERO : amount;

        if (log.isDebugEnabled()) {
            log.debug("balance calculation");
        }

        if (debitVal.equals(BigDecimal.ZERO)){

            if (!creditVal.equals(BigDecimal.ZERO)){

                editModel.setCredit(creditVal);
                editModel.setSold(creditVal, true);
            }
        } else if (creditVal.equals(BigDecimal.ZERO)){
            editModel.setDebit(debitVal);
            editModel.setSold(debitVal, false);
        } else {
            onBalanceChanged(null);
        }
    }

    public void loadComboAndRows(){

        //By default, we have the beginning of the fiscal period (Or of current
        //date if no fiscal period) and the end of the current date
        FiscalPeriod fiscalPeriod = fiscalPeriodService.getLastFiscalPeriod();
        Date defaultDateBegFiscalPeriod;

        Calendar calendar = Calendar.getInstance();
        int lastCurrentMonthDay = calendar.getActualMaximum(Calendar.DATE);
        int firstCurrentMonthDay = calendar.getActualMinimum(Calendar.DATE);

        if (fiscalPeriod != null){
            defaultDateBegFiscalPeriod = fiscalPeriod.getBeginDate();
        } else{
           defaultDateBegFiscalPeriod = DateUtils.setDays(new Date(), firstCurrentMonthDay);
        }

        Date defaultDateEndCurrent = DateUtils.setDays(new Date(), lastCurrentMonthDay);


        view.getBeginPeriodPicker().setDate(defaultDateBegFiscalPeriod);
        view.getEndPeriodPicker().setDate(defaultDateEndCurrent);

        filter.setDateStart(defaultDateBegFiscalPeriod);
        filter.setDateEnd(defaultDateEndCurrent);

        TypeEntry type = view.getLetteredEntryComboBox().getSelectedItem();
        setTypeEntry(type);
    }

    public void updateAllEntries() {

        if (initializationComplete
                && filter.getAccount() != null
                && filter.getDateStart() != null
                && filter.getDateEnd() != null) {

            List<Entry> entries = financialTransactionService.getAllEntrieByDatesAndAccountAndLettering(filter);
            List<Entry> unlockEntries = financialTransactionService.getAllUnlockEntriesByFilter(filter);
            view.getTableModel().setUnlockedEntries(unlockEntries);
            this.unlockedEntries = unlockEntries;
            view.getTableModel().setValues(entries);
        }

        onBalanceChanged(null);
        setValuesForEntries();
    }

    /**To make the difference between two selected entries and
     * create a new entry with the result (debit or credit).
     * It allow to letter somme entries with different debit and credit
     * */
    public void roundAndCreateEntry() {

        LetteringTableModel tableModel = view.getTableModel();

        int[] selectedRows = view.getTable().getSelectedRows();
        if (editModel.isEqualized() && selectedRows.length == 2) {
            try {
                /*Treatment only if one of values contains decimals*/
                Entry firstSelectedEntry = tableModel.get(selectedRows[0]);
                Entry secondSelectedEntry = tableModel.get(selectedRows[1]);

                Entry[] newEntriesFormEqualizing = financialTransactionService.getEntriesFromEqualizing(firstSelectedEntry, secondSelectedEntry);

                /*Add new entries to the model and the table*/
                Entry newSameAccountEntry = newEntriesFormEqualizing[0];
                Entry newCostOrProductEntry = newEntriesFormEqualizing[1];
                this.unlockedEntries.add(newSameAccountEntry);
                this.unlockedEntries.add(newCostOrProductEntry);

                tableModel.addValue(newSameAccountEntry);
                tableModel.addValue(newCostOrProductEntry);

                /*Re-select the two entries (firstSelectedEntry and secondSelectedEntry)
                * and the new sameAccountEntry
                * */
                view.getLetteringSelectionModel().selectRoundedAndNewEntries(selectedRows[0], selectedRows[1], newSameAccountEntry);
            } catch (LockedFinancialPeriodException e) {
                errorHelper.showErrorMessage(t("lima.lettering.roundAndCreateEntry.error.lockedFinancialPeriod",
                        e.getFinancialPeriod().getBeginDate(),
                        e.getFinancialPeriod().getEndDate()));
            } catch (LockedEntryBookException e) {
                errorHelper.showErrorMessage(t("lima.lettering.roundAndCreateEntry.error.lockedEntryBook",
                        e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                        e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
            } catch (AfterLastFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.afterLastFiscalPeriod",
                        e.getDate()));
            } catch (BeforeFirstFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.beforeFirstFiscalPeriod",
                        e.getDate()));
            }
        }
    }

    public void accountComboBoxChange(PropertyChangeEvent event) {
        if (event.getPropertyName().equals(AccountComboBox.PROPERTY_SELECTED_ITEM) && event.getNewValue() != null && event.getNewValue() instanceof Account) {
            setAccount((Account) event.getNewValue());
        }
    }

    /**
     * Select previous value in combo box.
     *
     * @param accountComboBox account combo box
     */
    public void back(AccountComboBox accountComboBox) {
        JComboBox combobox = accountComboBox.getCombobox();
        int row = combobox.getSelectedIndex();

        if (row > 0) {
            combobox.setSelectedIndex(row - 1);
        }
        view.getLetteringSelectionModel().clearSelection();
    }

    /**
     * Select next value in combo box.
     *
     * @param accountComboBox combo box
     */
    public void next(AccountComboBox accountComboBox) {
        JComboBox combobox = accountComboBox.getCombobox();
        int size = combobox.getItemCount();
        int row = combobox.getSelectedIndex();

        if (row < size - 1) {
            combobox.setSelectedIndex(row + 1);
        }
        view.getLetteringSelectionModel().clearSelection();
    }

    /**Add a group of three letters to n entries*/
    public void addLetter() {
        if (editModel.isLettered()) {
            int[] entriesSelected = view.getTable().getSelectedRows();

            LetteringTableModel tableModel = view.getTableModel();

            List<Entry> entries = Lists.newLinkedList();

            for (int indexEntry : entriesSelected) {

                Entry entry = tableModel.get(indexEntry);

                entries.add(entry);
            }

            try {

                if (!entries.isEmpty()) {

                    entries = financialTransactionService.addLetter(entries);

                    updateEntries(entries);
                }

            } catch (LockedEntryBookException e) {

                errorHelper.showErrorMessage(t("lima.entries.letter.closed.entryBook.error",
                        e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                        e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));

            } catch (UnbalancedEntriesException e) {

                errorHelper.showErrorMessage(t("lima.entries.letter.unbalanced.error"));

            }
            onButtonModeChanged(ButtonMode.DELETTERED);
        }
    }

    /**Remove a group of three letters to n entries*/
    public void removeLetter() {
        if (editModel.isUnLettered()) {

            int[] entriesSelected = view.getTable().getSelectedRows();

            LetteringTableModel tableModel = view.getTableModel();

            if (entriesSelected.length > 0) {

                Entry firstEntry = tableModel.get(entriesSelected[0]);

                String letter = firstEntry.getLettering();

                List<Entry> entries = financialTransactionService.removeLetter(letter);

                updateEntries(entries);
            }

            onButtonModeChanged(ButtonMode.LETTERED);
        }
    }


    protected void updateEntries(List<Entry> entries) {

        LetteringTableModel tableModel = view.getTableModel();

        for (final Entry entry : entries) {

            Entry oldEntry = tableModel.getValues().stream().filter(input -> input.getTopiaId().equals(entry.getTopiaId())).findFirst().orElseGet(() -> null);

            if (oldEntry != null) {
                int indexEntry = tableModel.indexOf(oldEntry);
                tableModel.setValue(indexEntry, entry);
            }
        }
    }


    public void setDateStart(Date date) {
        if (initializationComplete) {
            filter.setDateStart(date);
            updateAllEntries();
        }
    }

    public void setDateEnd(Date date) {
        if (initializationComplete) {
            filter.setDateEnd(date);
            updateAllEntries();
        }
    }

    public void setAccount(Account account) {
        if (filter != null) {
            filter.setAccount(account);
            updateAllEntries();
        }
    }

    public Account getAccount() {
        Account account = null;
        if (filter != null) {
            account = filter.getAccount();
        }
        return account;
    }

    public void setTypeEntry(TypeEntry typeEntry) {
        filter.setDisplayLettered(typeEntry.isLettered());
        filter.setDisplayUnlettred(typeEntry.isNoLettered());
        updateAllEntries();
    }

    protected DateFormat getDateFormat() {
        Locale locale = LimaSwingConfig.getInstance().getLocale();
        DateFormat result = DateFormat.getDateInstance(DateFormat.SHORT, locale);
        return result;
    }

    protected Locale getLocale() {
        Locale locale = LimaSwingConfig.getInstance().getLocale();
        return locale;
    }
}
