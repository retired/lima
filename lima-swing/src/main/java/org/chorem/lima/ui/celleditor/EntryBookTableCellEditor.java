/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.celleditor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.ui.LimaRendererUtil;
import org.chorem.lima.ui.combobox.EntryBookComboBoxModel;
import org.chorem.lima.util.EntryBookToString;
import org.chorem.lima.widgets.JWideComboBox;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;

public class EntryBookTableCellEditor extends AbstractCellEditor implements TableCellEditor {

    protected static final Log log = LogFactory.getLog(EntryBookTableCellEditor.class);

    private final JWideComboBox comboBox;

    private static final long serialVersionUID = 1L;

    //    private static EntryBookTableCellEditor editor;
    private volatile boolean keyPressed;

    /** constructor */
    public EntryBookTableCellEditor() {
        comboBox = new JWideComboBox();
        EntryBookComboBoxModel entryBookComboBoxModel = new EntryBookComboBoxModel();
        comboBox.setModel(entryBookComboBoxModel);
        ListCellRenderer renderer =
                LimaRendererUtil.newDecoratorListCellRenderer(EntryBook.class);
        comboBox.setRenderer(renderer);

        // AutoCompletion
        AutoCompleteDecorator.decorate(comboBox, EntryBookToString.getInstance());

        /**
         * Ajout d'un listener pour la frappe au clavier seulement, permettant
         * de déplacer la sélection (et le caret) du fait que la première touche
         * n'est pas prise en compte.
         */
        comboBox.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                // Vérifie si c'est la première frappe au clavier
                if (!keyPressed) {
                    // Récupère l'editor de la comboBox
                    JTextComponent edit = (JTextComponent) comboBox.getEditor().getEditorComponent();
                    // Met en place le curseur et la selection après la première lettre
                    edit.select(1, edit.getText().length());
                    keyPressed = true;
                }
                // Validate editing with enter key
                if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                    stopCellEditing();
                }
            }
        });

    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof EntryBook) {
            comboBox.setSelectedItem(value);
        }
        return comboBox;
    }

    @Override
    public Object getCellEditorValue() {
        return comboBox.getSelectedItem();
    }


    /**
     * Vérifie si la cellule peut être éditable :
     * seulement si il y a une frappe au clavier ou un double clic.
     *
     * @param evt the event
     * @return true, cell is editable otherwise not.
     */
    @Override
    public boolean isCellEditable(EventObject evt) {
        // Si il y a une frappe au clavier
        if (evt instanceof KeyEvent) {
            final KeyEvent keyEvent = (KeyEvent) evt;
            // Empèche la touche echap
            if (keyEvent.getKeyChar() != KeyEvent.VK_ESCAPE) {
                // Permet de placer le focus sur l'editor de la comboBox
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        comboBox.getEditor().getEditorComponent().requestFocus();
                        JTextComponent edit = (JTextComponent) comboBox.getEditor().getEditorComponent();
                        if (!Character.isIdentifierIgnorable(keyEvent.getKeyChar())) {
                            edit.setText(Character.toString(keyEvent.getKeyChar()));
                        }
                    }
                });
            }
        }
        // Remet à faux pour la premiere lettre tapée au clavier
        keyPressed = false;
        boolean result = !(evt instanceof MouseEvent) || ((MouseEvent) evt).getClickCount() == 1;
        return result;
    }

}
