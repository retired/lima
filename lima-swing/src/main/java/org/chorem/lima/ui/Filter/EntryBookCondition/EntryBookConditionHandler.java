package org.chorem.lima.ui.Filter.EntryBookCondition;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.Condition;
import org.chorem.lima.beans.EntryBookCondition;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.ui.Filter.ConditionHandler;
import org.chorem.lima.ui.Filter.financialTransactionCondition.FinancialTransactionConditionHandler;

import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class EntryBookConditionHandler implements ConditionHandler {

    protected EntryBookConditionView view;

    protected EntryBookCondition condition;

    protected FinancialTransactionConditionHandler filterHandler;

    public EntryBookConditionHandler(EntryBookConditionView view) {
        this.view = view;
        this.condition = new EntryBookCondition();
    }

    @Override
    public Condition getCondition() {
        return condition;
    }

    @Override
    public EntryBookConditionView getView() {
        return view;
    }

    @Override
    public void setFilterHandler(FinancialTransactionConditionHandler filterHandler) {
        this.filterHandler = filterHandler;
    }

    public Object[] getEntryBookList() {
        EntryBookService entryBookService = LimaServiceFactory.getService(EntryBookService.class);
        List<EntryBook> periods = entryBookService.getAllEntryBooks();
        Object[] result = periods.toArray();
        return result;
    }

    public void setEntryBook(ListSelectionEvent event) {
        JList entryBookList = view.getEntryBookList();
        List<EntryBook> selectedValuesList = entryBookList.getSelectedValuesList();
        condition.setEntryBooks(selectedValuesList);
    }

    public void delete() {
        filterHandler.removeCondition(this);
    }
}
