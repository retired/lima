package org.chorem.lima.ui.accountViewer;
/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.entity.Entry;

import javax.swing.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

/**
 * @author sletellier <letellier@codelutin.com>
 */
public class AccountViewerSelectionModel extends DefaultListSelectionModel{

    protected AccountViewerTableModel letteringTableModel;
    protected Entry entry;
    protected int lineSelected;

    protected boolean letteredSelectionMode;
    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public AccountViewerSelectionModel(AccountViewerTableModel letteringTableModel){
        this.letteringTableModel = letteringTableModel;
    }

    @Override
    public void addSelectionInterval(int row, int column) {
        setSelectionInterval(row, column);
    }

    @Override
    public void setSelectionInterval(int row, int column) {
        if (letteredSelectionMode) {
            if (!letteringNotExist(row)) {

                //lettred entries
                if ( isSelectionEmpty() || !isSelectedIndex(row)){
                    clearSelection();
                    lineSelected = row;
                    String currentLettring = getCurrentLettring();

                    //select entries with the same letter of the selected entry
                    for(Entry entry : getEntries()){
                        if (StringUtils.isNotBlank(entry.getLettering())){
                            if (entry.getLettering().equals(currentLettring)){
                                int entryToSelect = letteringTableModel.indexOf(entry);
                                super.addSelectionInterval(entryToSelect, entryToSelect);
                            }
                        }
                    }
                }
            }
            else {

                //unlettred entries
                //To clear the selection when it changes from lettered entry to unlettered
                for(Entry entry : getEntries()){
                    if (!StringUtils.isBlank(entry.getLettering())) {
                        int entryToSelect = letteringTableModel.indexOf(entry);
                        super.removeSelectionInterval(entryToSelect, entryToSelect);
                    }
                }

                if (isSelectionEmpty() || !isSelectedIndex(row)){
                    super.addSelectionInterval(row, column);
                }else {
                    super.removeSelectionInterval(row, column);
                }

            }
        } else {
            super.addSelectionInterval(row, column);
            //super.setSelectionInterval(row, column);
        }

    }

    @Override
    public int getSelectionMode() {
        return MULTIPLE_INTERVAL_SELECTION;
    }

    /**return true if lettering is null, or not null but empty
     * @param row index of the line to test
     * @return boolean
     * */
    public boolean letteringNotExist(int row){
        boolean emptyOrNull;
        entry = letteringTableModel.get(row);
        String lettering = entry.getLettering();
        emptyOrNull = (lettering==null||lettering.isEmpty());
        return emptyOrNull;
    }

    public List<Entry> getEntries(){
        return letteringTableModel.getValues();
    }

    public String getCurrentLettring(){
        return getCurrentEntrySelected().getLettering();
    }

    public Entry getCurrentEntrySelected(){
        return letteringTableModel.get(lineSelected);
    }


    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }

    public void setLetteredSelectionMode(boolean letteredSelectionMode) {
        this.letteredSelectionMode = letteredSelectionMode;
        clearSelection();
    }

    public boolean getLetteredSelectionMode() {
        return letteredSelectionMode;
    }
}
