/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.ui;

import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FiscalPeriod;
import org.nuiton.decorator.DecoratorProvider;

/**
 * To provide decorations for entities.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.5.2
 */
public class LimaDecoratorProvider extends DecoratorProvider {

    @Override
    protected void loadDecorators() {

        registerJXPathDecorator(EntryBook.class, "${code}$s - ${label}$s");
        registerJXPathDecorator(Account.class, "${accountNumber}$s - ${label}$s");

        registerMultiJXPathDecorator(
                FiscalPeriod.class,
                "${beginDate}$td/%1$tm/%1$tY#${endDate}$td/%2$tm/%2$tY", "#"," - ");

        registerMultiJXPathDecorator(
                FinancialPeriod.class,
                "${beginDate}$td/%1$tm/%1$tY#${endDate}$td/%2$tm/%2$tY", "#"," - ");
    }
}
