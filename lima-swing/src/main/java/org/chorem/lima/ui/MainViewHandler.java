/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui;

import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.AboutPanel;
import jaxx.runtime.swing.config.ConfigUIHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.OptionsService;
import org.chorem.lima.enums.ImportExportEnum;
import org.chorem.lima.enums.MainUiRefreshComponent;
import org.chorem.lima.ui.account.AccountView;
import org.chorem.lima.ui.accountViewer.AccountViewerView;
import org.chorem.lima.ui.celleditor.NumberSeparatorCellRenderer;
import org.chorem.lima.ui.celleditor.NumberSeparatorTableCellRenderer;
import org.chorem.lima.ui.entrybook.EntryBookView;
import org.chorem.lima.ui.financialperiod.FinancialPeriodView;
import org.chorem.lima.ui.financialstatementchart.FinancialStatementChartView;
import org.chorem.lima.ui.financialtransaction.FinancialTransactionView;
import org.chorem.lima.ui.financialtransactionsearch.FinancialTransactionSearchView;
import org.chorem.lima.ui.financialtransactionunbalanced.FinancialTransactionUnbalancedView;
import org.chorem.lima.ui.fiscalControlExport.FiscalControlExportView;
import org.chorem.lima.ui.fiscalperiod.FiscalPeriodView;
import org.chorem.lima.ui.home.HomeView;
import org.chorem.lima.ui.identity.IdentityForm;
import org.chorem.lima.ui.importexport.ImportExport;
import org.chorem.lima.ui.lettering.LetteringView;
import org.chorem.lima.ui.vatchart.VatChartView;
import org.nuiton.util.DesktopUtil;
import org.nuiton.widget.SwingSession;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.BorderUIResource;
import java.awt.*;
import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for main view.
 * <p/>
 * Date: 8 nov. 2009
 * Time: 09:59:54
 *
 * @author chemit
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class MainViewHandler {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(MainViewHandler.class);

    protected SwingSession swingSession;

    /**
     * Methode pour initialiser l'ui principale sans l'afficher.
     *
     * @param rootContext le context applicatif
     * @return l'ui instancie et initialisee mais non visible encore
     */
    public MainView initUI(LimaSwingApplicationContext rootContext) {
        // show main ui
        MainView ui = new MainView(rootContext);

        swingSession = new SwingSession(getLimaStateFile(), false);

        LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.setContextValue(rootContext, ui);

        return ui;
    }

    //Get xml lima state file
    protected File getLimaStateFile() {

        LimaSwingConfig limaSwingConfig = LimaSwingConfig.getInstance();

        File limaStateFile = limaSwingConfig.getLimaStateFile();

        return limaStateFile;
    }

    public void changeLanguage(MainView mainUI, Locale newLocale) {
        LimaSwingConfig config = mainUI.getConfig();

        int response = JOptionPane.showConfirmDialog(mainUI,t("lima.restart"));

        if (response == JOptionPane.YES_OPTION) {
            // sauvegarde de la nouvelle locale
            config.setLocale(newLocale);

            // rechargement i18n
            LimaSwingApplicationContext.get().initI18n(config);

            LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.removeContextValue(LimaSwingApplicationContext.get());

            mainUI.dispose();

            mainUI.setVisible(false);

            System.exit(0);
        }
    }

    /**
     * Ferme l'application.
     *
     * @param ui l'ui principale de l'application
     */
    public void close(MainView ui) {
        /*Save Lima state*/
        swingSession.save();
        if (log.isInfoEnabled()) {
            log.info("Lima quitting...");
        }
        boolean canContinue = ensureModification(ui);
        if (!canContinue) {
            return;
        }
        try {
            LimaSwingApplicationContext.get().close();

            Runtime.getRuntime().halt(0);

        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("error while closing " + ex.getMessage(), ex);
            }
            Runtime.getRuntime().halt(1);
        }
    }

    public void showConfig(JAXXContext context) {
        MainView ui = getUI(context);
        final LimaSwingConfig config = ui.getConfig();

        ConfigUIHelper helper = new ConfigUIHelper(config);

        helper.registerCallBack(LimaSwingConfig.Option.COLOR_SELECTION_FOCUS.getKey(), t("lima.config.color.selection.focus"), new ImageIcon(), () -> UIManager.put("Table.focusCellHighlightBorder", new BorderUIResource(new LineBorder(config.getColorSelectionFocus(), 2))));

        helper.addCategory(t("lima.config.category.directories"), t("lima.config.category.directories.description"));
        helper.addOption(LimaSwingConfig.Option.CONFIG_FILE);
        helper.setOptionShortLabel(LimaSwingConfig.Option.CONFIG_FILE.getLabel());

        helper.addCategory(t("lima.config.category.table"), t("lima.config.category.table.description"));
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_SELECTED_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_SELECTED_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_SELECTED_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_SELECTED_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_ERROR_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_ERROR_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_ERROR_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_ERROR_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_ERROR_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_ERROR_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_ERROR_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_ERROR_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_SELECTED_ERROR_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_SELECTED_ERROR_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_SELECTED_ERROR_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_SELECTED_ERROR_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_ERROR_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_ERROR_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_ERROR_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_ERROR_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_MANDATORY_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_MANDATORY_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_MANDATORY_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_MANDATORY_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_MANDATORY_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_MANDATORY_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_MANDATORY_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_MANDATORY_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_SELECTED_MANDATORY_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_SELECTED_MANDATORY_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_SELECTED_MANDATORY_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_SELECTED_MANDATORY_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_MANDATORY_BACKGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_MANDATORY_BACKGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_MANDATORY_FOREGROUND);
        helper.setOptionShortLabel(LimaSwingConfig.Option.TABLE_CELL_PAIR_SELECTED_MANDATORY_FOREGROUND.getLabel());
        helper.addOption(LimaSwingConfig.Option.COLOR_SELECTION_FOCUS);
        helper.setOptionShortLabel(LimaSwingConfig.Option.COLOR_SELECTION_FOCUS.getLabel());


        helper.addCategory(t("lima.config.category.other"), t("lima.config.category.other.description"));
        helper.addOption(LimaSwingConfig.Option.LOCALE);
        helper.setOptionShortLabel(LimaSwingConfig.Option.LOCALE.getLabel());

        JComboBox<Character> comboBoxSeparator = new JComboBox<>(LimaSwingConfig.NUMBER_SEPARATOR.toArray(new Character[LimaSwingConfig.NUMBER_SEPARATOR.size()]));
        DefaultCellEditor separatorEditor = new DefaultCellEditor(comboBoxSeparator);
        comboBoxSeparator.setRenderer(new NumberSeparatorCellRenderer());
        NumberSeparatorTableCellRenderer separatorRenderer = new NumberSeparatorTableCellRenderer();

        OptionsService optionsService = LimaServiceFactory.getService(OptionsService.class);

        helper.addOption(optionsService.getDecimalSeparatorOption());
        helper.setOptionShortLabel(optionsService.getDecimalSeparatorOption().getLabel());
        helper.setOptionRenderer(separatorRenderer);
        helper.setOptionEditor(separatorEditor);

        JComboBox<Integer> comboBoxDecimal = new JComboBox<>(LimaSwingConfig.NUMBER_DECIMALS.toArray(new Integer[LimaSwingConfig.NUMBER_DECIMALS.size()]));
        DefaultCellEditor decimalEditor = new DefaultCellEditor(comboBoxDecimal);
        helper.addOption(optionsService.getScaleOption());
        helper.setOptionShortLabel(optionsService.getScaleOption().getLabel());
        helper.setOptionEditor(decimalEditor);

        helper.addOption(optionsService.getThousandSeparatorOption());
        helper.setOptionShortLabel(optionsService.getThousandSeparatorOption().getLabel());
        helper.setOptionRenderer(separatorRenderer);
        helper.setOptionEditor(separatorEditor);

        helper.addOption(optionsService.getCurrencyOption());
        helper.setOptionShortLabel(optionsService.getCurrencyOption().getLabel());
        /*Pas de 'callBack' sur le changement de comportement lors de l'édition d'une cellule,
        * car les deux éditeurs concernés rappellent la config dans un listener ('focusGained')*/
        helper.addOption(LimaSwingConfig.Option.SELECT_ALL_EDITING_CELL);
        helper.setOptionShortLabel(LimaSwingConfig.Option.SELECT_ALL_EDITING_CELL.getLabel());

        helper.buildUI(context, t("lima.config.category.directories"));
        helper.displayUI(ui, false);
    }

    public void gotoSite(JAXXContext context) {

        LimaSwingConfig config = getUI(context).getConfig();

        URL siteURL = config.getOptionAsURL("application.site.url");
        try {
            DesktopUtil.browse(siteURL.toURI());
        } catch (Exception e) {
            throw new LimaTechnicalException("Can't open lima website at " + siteURL, e);
        }
    }

    protected JEditorPane getJEditorPane() {
        JEditorPane translateArea = new JEditorPane();
        translateArea.setContentType("text/html");
        translateArea.setEditable(false);
        if (translateArea.getFont() != null) {
            translateArea.setFont(translateArea.getFont().deriveFont((float) 11));
        }
        translateArea.setBorder(null);
        return translateArea;
    }

    public void showAbout(JAXXContext context) {
        MainView ui = getUI(context);
        AboutPanel about = new AboutPanel();
        about.setTitle(t("lima.help.about.title"));
        about.setAboutText(t("lima.help.about.title.description"));
        about.setBottomText(ui.getConfig().getCopyrightText());
        about.setIconPath("/icons/lima.png");
        about.setLicenseFile("META-INF/lima-LICENSE.txt");
        about.setThirdpartyFile("META-INF/lima-THIRD-PARTY.txt");

        JEditorPane translateArea = getJEditorPane();
        String text = t("lima.help.about.translate.content");
        translateArea.setText(text);

        JScrollPane component = new JScrollPane();
        component.getViewport().add(translateArea);
        about.getTabs().add(t("lima.help.about.translate.title"), component);
        about.init();
        about.showInDialog(ui, true);
    }

    /**
     * Test if there is some modification on screen,
     *
     * @param rootContext the context
     * @return <code>true</code> if no more modification is detected
     * @throws IllegalArgumentException if rootContext is null
     */
    protected boolean ensureModification(JAXXContext rootContext) throws IllegalArgumentException {
        if (rootContext == null) {
            throw new IllegalArgumentException("rootContext can not be null");
        }
        MainView ui = getUI(rootContext);
        if (ui == null) {
            // no ui, so no modification
            return true;
        }
        // check ui is not modified
        return true;
    }

    protected MainView getUI(JAXXContext context) {
        if (context instanceof MainView) {
            return (MainView) context;
        }
        MainView ui = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(context);
        return ui;
    }

    public void showHomeView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        final HomeView homeView = new HomeView(mainView);
        showTab(mainView, t("lima.home"), homeView, false);
        JTabbedPane contentTabbedPane = mainView.getContentTabbedPane();
        contentTabbedPane.addChangeListener(new OnTabChangeListener(homeView));

    }

    public void showIdentity(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        IdentityForm identityForm = new IdentityForm();
        identityForm.setLocationRelativeTo(mainView);
        identityForm.setVisible(true);
    }

    /**
     * Show account tree table view to create or modify accounts
     *
     * @param rootContext the root application context
     */
    public void showAccountView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        AccountView accountView = new AccountView(mainView);
        showTab(mainView, t("lima.accounts"), accountView);
        swingSession.add(accountView);
    }

    /**
     * Show account tree table view to create or modify accounts
     *
     * @param rootContext the root application context
     */
    public void showEntryBookView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        EntryBookView entryBookView = new EntryBookView(mainView);
        showTab(mainView, t("lima.entryBooks"), entryBookView);
        swingSession.add(entryBookView);
    }

    /**
     * Show fiscal period view to create or block a period
     *
     * @param rootContext the root application context
     */
    public void showFiscalPeriodView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        FiscalPeriodView fiscalPeriodView = new FiscalPeriodView(mainView);
        showTab(mainView, t("lima.fiscalYears"), fiscalPeriodView);
        swingSession.add(fiscalPeriodView);
    }

    /**
     * Show financial period view to create or block a period
     *
     * @param rootContext the root application context
     */
    public void showFinancialPeriodView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        FinancialPeriodView financialPeriodView =
                new FinancialPeriodView(mainView);
        showTab(mainView, t("lima.financialPeriods"), financialPeriodView);
        swingSession.add(financialPeriodView);
    }

    /**
     * Show financial statement view to generate balance sheet and income statement
     *
     * @param rootContext the root application context
     */
    public void showFinancialStatementView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        FinancialStatementChartView financialStatementView =
                new FinancialStatementChartView(mainView);
        showTab(mainView, t("lima.financialStatements"), financialStatementView);
        swingSession.add(financialStatementView);
    }

    /**
     * Show vat chart to generate vat document
     *
     * @param rootContext the root application context
     */
    public void showVatChartView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        VatChartView vatChartView = new VatChartView(mainView);
        showTab(mainView, t("lima.vatStatements"), vatChartView);
        swingSession.add(vatChartView);
    }

    /**
     * Show financial transactions view to create entries
     *
     * @param rootContext the root application context
     */
    public void showTransactionView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        FinancialTransactionView transactionView =
                new FinancialTransactionView(mainView);
        showTab(mainView, t("lima.entries.enter"),
                transactionView);
        swingSession.add(transactionView);
    }

    public void showTransactionUnbalancedView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        FinancialTransactionUnbalancedView searchResultView =
                new FinancialTransactionUnbalancedView(mainView);
        showTab(mainView, t("lima.entries.unbalanced"),
                searchResultView);
        swingSession.add(searchResultView);
    }

    public void showTransactionSearchView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        FinancialTransactionSearchView searchView =
                new FinancialTransactionSearchView(mainView);
        showTab(mainView, t("lima.entries.search"), searchView);
        swingSession.add(searchView);
    }

    public void showLetteringView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        LetteringView letteringView = new LetteringView(mainView);
        showTab(mainView, t("lima.entries.lettering"), letteringView);
        swingSession.add(letteringView);
    }

    public void showAccountViewerView(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        AccountViewerView accountViewerView = new AccountViewerView(mainView);
        showTab(mainView, t("lima.entries.accountViewer"), accountViewerView);
        swingSession.add(accountViewerView);
    }

    public void showImportExportView(JAXXContext rootContext,
                                     ImportExportEnum type) {
        MainView mainView = getUI(rootContext);
        ImportExport importExport = new ImportExport(mainView);
        importExport.processImportExport(type, true);

        switch (type) {
            case CSV_ALL_IMPORT:
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ALL);
                break;
            case EBP_ACCOUNTCHARTS_IMPORT:
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ACCOUNT_PANE);
                break;
            case CSV_ENTRIES_IMPORT:
            case CSV_FINANCIALSTATEMENTS_IMPORT:
            case EBP_ENTRIES_IMPORT:
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FINANCIAL_TRANSACTION_PANE);
                break;
            case CSV_ENTRYBOOKS_IMPORT:
            case EBP_ENTRYBOOKS_IMPORT:
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.ENTRY_BOOK_PANE);
                break;
        }
    }

    public void openLimaHttpUi(MainView ui) {

        OptionsService optionsService = LimaServiceFactory.getService(OptionsService.class);

        String host = optionsService.getLimaHttpHostAddress();
        int port =  optionsService.getLimaHttpPort();

        String url;
        if (port == 80) {
            url = String.format("http://%s/", host);
        } else if (port == 443) {
            url = String.format("https://%s/", host);
        } else {
            url = String.format("http://%s:%d/", host, port);
        }

        if (log.isDebugEnabled()) {
            log.debug("URL : " + url);
        }

        openLink(url);
    }

    public static void openLink(String url) {

        try {
            URI uri = new URI(url);
            DesktopUtil.browse(uri);
        } catch (Exception e) {

            if (log.isErrorEnabled()) {
                log.error("Error while opening link", e);
            }
        }
    }

    /**
     * Show a new closable tab.
     *
     * @param ui        main ui
     * @param name      name of tab to show
     * @param container the tab container
     * @param canClose  {@code false} if tab can't be closed
     */
    protected void showTab(MainView ui,
                           String name,
                           Component container,
                           boolean canClose) {
        final JTabbedPane contentTabbedPane = ui.getContentTabbedPane();

        // if contentTabbedPane does not yet contains tab
        if (contentTabbedPane.indexOfTab(name) == -1) {
            ClosableTabHeader closableHeader = new ClosableTabHeader();
            closableHeader.setTitle(name);
            closableHeader.setCanClose(canClose);
            contentTabbedPane.addTab(name, container);
            contentTabbedPane.setSelectedComponent(container);
            contentTabbedPane.setTabComponentAt(
                    contentTabbedPane.indexOfTab(name), closableHeader);
            closableHeader.getCloseTab().addActionListener(e -> {
                JButton button = (JButton) e.getSource();
                ClosableTabHeader closableTab =
                        (ClosableTabHeader) button.getParent();
                String name1 = closableTab.getTitle();
                contentTabbedPane.remove(contentTabbedPane.indexOfTab(name1));
            });
            // if contentTabbedPane contains tab
        } else {
            contentTabbedPane.setSelectedIndex(contentTabbedPane.indexOfTab(name));
        }
    }

    /**
     * Show a new closable tab.
     *
     * @param ui        main ui
     * @param name      name of tab to show
     * @param container the tab container
     */
    protected void showTab(MainView ui, String name, Component container) {
        showTab(ui, name, container, true);
    }

    public void showFiscalControlExport(JAXXContext rootContext) {
        MainView mainView = getUI(rootContext);
        FiscalControlExportView fiscalControlExportView = new FiscalControlExportView((JAXXContext) mainView);
        fiscalControlExportView.setLocationRelativeTo(mainView);
        fiscalControlExportView.setVisible(true);

    }

    protected static class OnTabChangeListener implements ChangeListener {

        protected final HomeView homeView;

        public OnTabChangeListener(HomeView homeView) {
            this.homeView = homeView;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            JTabbedPane source = (JTabbedPane) e.getSource();
            if (source.getSelectedIndex()==0) {
                MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(homeView);
                MainUiRefreshComponent compo = mainView.getMainUiRefreshComponent();
                homeView.refresh(compo);
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.NONE);
            }
        }

    }

}
