/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.ui.vatchart;

import jaxx.runtime.swing.JAXXWidgetUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.VatStatementService;
import org.chorem.lima.business.exceptions.AlreadyAffectedVatBoxException;
import org.chorem.lima.business.exceptions.AlreadyExistVatStatementException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.entity.VatStatement;
import org.chorem.lima.entity.VatStatementImpl;
import org.chorem.lima.util.ErrorHelper;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import static org.nuiton.i18n.I18n.t;

public class VatChartViewModel extends DefaultTreeTableModel {

    /** log. */
    private static final Log log = LogFactory.getLog(VatChartViewHandler.class);

    /** Services. */
    protected final VatStatementService vatStatementService;

    protected ErrorHelper errorHelper;

    /** Model constructor. Initiate account service used here. */
    public VatChartViewModel(TreeTableNode root) {
        //create root for the tree
        super(root);
        // Gets factory service 
        vatStatementService = LimaServiceFactory.getService(VatStatementService.class);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
    }

    @Override
    public Object getValueAt(Object node, int column) {
        Object result = "n/a";
        MutableTreeTableNode treeNode = (MutableTreeTableNode) node;
        if (treeNode.getUserObject() instanceof VatStatement) {
            VatStatement vatStatement = (VatStatement)treeNode.getUserObject();
            switch (column) {
                case 0:
                    result = vatStatement.getLabel();
                    break;
                case 1:
                    result = vatStatement.getAccounts();
                    break;
                case 2:
                    result = vatStatement.getBoxName();
            }
        } else {
            result = treeNode.getUserObject();
        }


        return result;
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        return column >= 1 && isLeaf(node);
    }

    @Override
    public void setValueAt(Object value, Object objectNode, int column) {

        MutableTreeTableNode node = (MutableTreeTableNode) objectNode;
        VatStatement vatStatement = (VatStatement) node.getUserObject();

        // copy is done in case of exception to keep original object intact
        VatStatement copy = getVatStatementCopy(vatStatement);

        String newValue = (String) value;
        String originalValue = setNewValueToVatStatement(column, copy, newValue);

        if (originalValue != null && !originalValue.contentEquals(newValue)) {
            newValue = persistValue(node, copy, newValue, originalValue);
            super.setValueAt(newValue, node, column);
        }

    }

    /**
     * Add VatStatement(path can be null).
     *
     * @param vatStatement
     */
    public VatStatement addVatStatement(VatStatement parentVatStatementHeader, VatStatement vatStatement) {
        VatStatement result = null;
        try {
            result = vatStatementService.createVatStatement(parentVatStatementHeader, vatStatement);
        } catch (AlreadyExistVatStatementException alreadyExistVatStatement) {
            errorHelper.showErrorMessage(t("lima.financialStatement.error.alreadyExistFinancialStatement",
                    alreadyExistVatStatement.getVatStatementLabel(), alreadyExistVatStatement.getMasterLabel()));
        } catch (NotAllowedLabelException notAllowedLabel) {
            errorHelper.showErrorMessage(t("lima.error.notAllowedLabel", notAllowedLabel.getLabel()));
        } catch (AlreadyAffectedVatBoxException e) {
            errorHelper.showErrorMessage(t("lima.financialStatement.error.alreadyUsedVatBox",
                    e.getBoxId()));
        }
        return result;
    }


    /**
     * Update vatStatement
     *
     * @param vatStatement
     */
    public VatStatement updateVatStatement(VatStatement vatStatement) {
        VatStatement updatedVatStatement = null;
        try {
            updatedVatStatement = vatStatementService.updateVatStatement(vatStatement);
        } catch (AlreadyExistVatStatementException alreadyExistVatStatement) {
            errorHelper.showErrorMessage(t("lima.financialStatement.error.alreadyExistFinancialStatement",
                    alreadyExistVatStatement.getVatStatementLabel(), alreadyExistVatStatement.getMasterLabel()));
        } catch (NotAllowedLabelException notAllowedLabel) {
            errorHelper.showErrorMessage(t("lima.error.notAllowedLabel", notAllowedLabel.getLabel()));
        } catch (AlreadyAffectedVatBoxException e) {
            errorHelper.showErrorMessage(t("lima.financialStatement.error.alreadyUsedVatBox",
                    e.getBoxId()));
        }
        return updatedVatStatement;
    }

    protected String persistValue(MutableTreeTableNode node, VatStatement copy, String newValue, String originalValue) {
        String cellValue;
        VatStatement persistedVatStatement = updateVatStatement(copy);
        if (persistedVatStatement != null) {
            cellValue = newValue;
            node.setUserObject(persistedVatStatement);
        } else {
            cellValue = originalValue;
        }
        return cellValue;
    }

    protected String setNewValueToVatStatement(int column, VatStatement copy, String newValue) {
        String originalValue = null;
        switch (column) {
            case 1:
                originalValue = JAXXWidgetUtil.getStringValue(copy.getAccounts());
                copy.setAccounts(newValue);
                break;
            case 2:
                originalValue = JAXXWidgetUtil.getStringValue(copy.getBoxName());
                copy.setBoxName(newValue);
                break;
            default:
                break;
        }
        return originalValue;
    }

    protected VatStatement getVatStatementCopy(VatStatement vatStatement) {
        VatStatement copy = new VatStatementImpl();
        Binder<VatStatement, VatStatement> binder = BinderFactory.newBinder(VatStatement.class, VatStatement.class);
        binder.copy(vatStatement, copy);
        return copy;
    }


}
