/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialperiod;

import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.ui.common.AbstractColumn;
import org.chorem.lima.ui.common.AbstractLimaTableModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Financial period table model.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialPeriodTableModel extends AbstractLimaTableModel<ClosedPeriodicEntryBook> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 77027335135838258L;

    @Override
    protected void initColumn() {

        addColumn(new AbstractColumn<FinancialPeriodTableModel>(Date.class, t("lima.financialPeriod.begin"), false) {
            @Override
            public Object getValueAt(int row) {
                ClosedPeriodicEntryBook closedPeriodicEntryBook = tableModel.get(row);
                FinancialPeriod financialPeriod = closedPeriodicEntryBook.getFinancialPeriod();
                return financialPeriod.getBeginDate();
            }
        });

        addColumn(new AbstractColumn<FinancialPeriodTableModel>(Date.class, t("lima.financialPeriod.end"), false) {
            @Override
            public Object getValueAt(int row) {
                ClosedPeriodicEntryBook closedPeriodicEntryBook = tableModel.get(row);
                FinancialPeriod financialPeriod = closedPeriodicEntryBook.getFinancialPeriod();
                return financialPeriod.getEndDate();
            }
        });

        addColumn(new AbstractColumn<FinancialPeriodTableModel>(EntryBook.class, t("lima.financialPeriod.entryBook"), false) {
            @Override
            public Object getValueAt(int row) {
                ClosedPeriodicEntryBook closedPeriodicEntryBook = tableModel.get(row);
                return closedPeriodicEntryBook.getEntryBook();
            }
        });

        addColumn(new AbstractColumn<FinancialPeriodTableModel>(String.class, t("lima.financialPeriod.status"), false) {
            @Override
            public Object getValueAt(int row) {
                ClosedPeriodicEntryBook closedPeriodicEntryBook = tableModel.get(row);
                String status = t("lima.financialPeriod.open");
                if (closedPeriodicEntryBook.isLocked()) {
                    status = t("lima.financialPeriod.closed");
                }
                return status;
            }
        });


    }

}
