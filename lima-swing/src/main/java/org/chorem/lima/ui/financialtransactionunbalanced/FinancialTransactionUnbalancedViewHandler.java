/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.financialtransactionunbalanced;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.enums.MainUiRefreshComponent;
import org.chorem.lima.ui.MainView;
import org.chorem.lima.ui.combobox.FiscalPeriodComboBoxModel;
import org.chorem.lima.ui.financialtransaction.FinancialTransactionDefaultTable;
import org.chorem.lima.util.ErrorHelper;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler associated with financial transaction view.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class FinancialTransactionUnbalancedViewHandler {

    /** log. */
    private static final Log log =
            LogFactory.getLog(FinancialTransactionUnbalancedViewHandler.class);

    protected FinancialTransactionUnbalancedView view;

    protected ErrorHelper errorHelper;

    protected FinancialTransactionUnbalancedViewHandler(FinancialTransactionUnbalancedView view) {
        this.view = view;
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());
        initShortCuts();
    }


    /**
     * Init all combo box in view.
     */
    public void init() {

        if (view.getFiscalPeriodComboBox().getModel().getSize() > 0) {
            view.getFiscalPeriodComboBox().setSelectedIndex(0);
        } else {
            refresh();
        }

    }

    protected void initShortCuts() {

        InputMap inputMap= view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = view.getActionMap();
        Object binding;

        // add action on Ctrl + Shift + Delete
        binding = "delete-transaction";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -226644303576583765L;

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSelectedTransaction();
            }
        });

        // add action on Ctrl + N
        binding = "new-entry";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -6435906445215486356L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addEntry();
            }
        });

        // add action on Ctrl + Delete
        binding = "delete-entry";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 4178423543503694499L;

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSelectedEntry();
            }
        });

        // add action on Ctrl + B
        binding = "balance";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 1311386081937821318L;

            @Override
            public void actionPerformed(ActionEvent e) {
                balanceTransaction();
            }
        });

        // add action on F5
        binding = "refresh";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0) , binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -1045374983643252166L;

            @Override
            public void actionPerformed(ActionEvent e) {
                refresh();
            }
        });
    }

    //implement new transaction button
    //add a new entry to the selected transaction
    public void addEntry() {

        FinancialTransactionDefaultTable table = view.getFinancialTransactionUnbalancedTable();
        FinancialTransactionUnbalancedTableModel tableModel = view.getFinancialTransactionUnbalancedTableModel();

        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow != -1) {
            try {
                TableCellEditor cellEditor = table.getCellEditor();
                if (cellEditor != null) {
                    cellEditor.cancelCellEditing();
                }

                Entry entry = tableModel.get(indexSelectedRow);
                //copy + paste the description
                Entry newEntry = new EntryImpl();
                newEntry.setFinancialTransaction(entry.getFinancialTransaction());
                newEntry.setVoucher(entry.getVoucher());
                newEntry.setDescription(entry.getDescription());

                Entry savedNewEntry = tableModel.addEntry(newEntry);
                //select the new line
                int row = tableModel.indexOf(savedNewEntry.getFinancialTransaction()) + savedNewEntry.getFinancialTransaction().sizeEntry() - 1;
                ListSelectionModel selectionModel = table.getSelectionModel();
                selectionModel.setSelectionInterval(
                        row, row);
                table.changeSelection(row, 1, false, false);
                table.editCellAt(row, 1);

                MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FINANCIAL_TRANSACTION_PANE);

            } catch (LockedFinancialPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.lockedFinancialPeriod",
                        e.getFinancialPeriod().getBeginDate(),
                        e.getFinancialPeriod().getEndDate()));
            } catch (AfterLastFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.afterLastFiscalPeriod",
                        e.getDate()));
            } catch (BeforeFirstFiscalPeriodException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.beforeFirstFiscalPeriod",
                        e.getDate()));
            } catch (LockedEntryBookException e) {
                errorHelper.showErrorMessage(t("lima.entries.add.entry.error.lockedEntryBook",
                        e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                        e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                        e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Call addentry selected transaction without selection");
            }
        }
    }

    /**
     * Delete selected row in table (could be transaction or entry).
     * <p/>
     * Called by tableModel.
     */
    public void deleteSelectedEntry() {

        FinancialTransactionDefaultTable table = view.getFinancialTransactionUnbalancedTable();
        FinancialTransactionUnbalancedTableModel tableModel = view.getFinancialTransactionUnbalancedTableModel();

        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow != -1) {

            TableCellEditor cellEditor = table.getCellEditor();
            if (cellEditor != null) {
                cellEditor.cancelCellEditing();
            }

            String message = t("lima.entry.remove.confirm");

            int response = JOptionPane.showConfirmDialog(view, message,
                                                         t("lima.confirmation"), JOptionPane.YES_NO_OPTION);

            if (response == JOptionPane.YES_OPTION) {
                try {
                    tableModel.removeEntry(indexSelectedRow);

                    if (tableModel.size() > 0) {
                        if (indexSelectedRow >= tableModel.size()) {
                            indexSelectedRow = tableModel.size() - 1;
                        }
                        ListSelectionModel selectionModel =
                                table.getSelectionModel();
                        selectionModel.setSelectionInterval(
                                indexSelectedRow, indexSelectedRow);
                        table.changeSelection(indexSelectedRow, 1, false, false);
                        table.editCellAt(indexSelectedRow, 1);
                    }

                    MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                    mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FINANCIAL_TRANSACTION_PANE);

                } catch (LockedFinancialPeriodException e) {
                    errorHelper.showErrorMessage(t("lima.entries.remove.entry.error.lockedFinancialPeriod",
                            e.getFinancialPeriod().getBeginDate(),
                            e.getFinancialPeriod().getEndDate()));
                } catch (LockedEntryBookException e) {
                    errorHelper.showErrorMessage(t("lima.entries.remove.entry.error.lockedEntryBook",
                            e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                            e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                            e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                            e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
                }
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Call delete selected row without selection");
            }
        }
    }

    /**
     * Delete selected row in table (could be transaction or entry).
     * <p/>
     * Called by tableModel.
     */
    public void deleteSelectedTransaction() {

        FinancialTransactionDefaultTable table = view.getFinancialTransactionUnbalancedTable();
        FinancialTransactionUnbalancedTableModel tableModel = view.getFinancialTransactionUnbalancedTableModel();

        int indexSelectedRow = table.getSelectedRow();
        if (indexSelectedRow != -1) {

            TableCellEditor cellEditor = table.getCellEditor();
            if (cellEditor != null) {
                cellEditor.cancelCellEditing();
            }

            String message = t("lima.financialTransaction.remove.confirm");

            int response = JOptionPane.showConfirmDialog(view, message,
                    t("lima.confirmation"), JOptionPane.YES_NO_OPTION);

            if (response == JOptionPane.YES_OPTION) {
                try {
                    tableModel.removeTransaction(indexSelectedRow);
                    //select the upper line
                    if (tableModel.size() > 0) {
                        if (indexSelectedRow >= tableModel.size()) {
                            indexSelectedRow = tableModel.size() - 1;
                        }
                        ListSelectionModel selectionModel =
                                table.getSelectionModel();
                        selectionModel.setSelectionInterval(
                                indexSelectedRow, indexSelectedRow);
                        table.changeSelection(indexSelectedRow, 1, false, false);
                        table.editCellAt(indexSelectedRow, 1);

                        MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                        mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FINANCIAL_TRANSACTION_PANE);
                    }
                } catch (LockedFinancialPeriodException e) {
                    errorHelper.showErrorMessage(t("lima.entries.remove.transaction.error.lockedFinancialPeriod",
                            e.getFinancialPeriod().getBeginDate(),
                            e.getFinancialPeriod().getEndDate()));
                } catch (LockedEntryBookException e) {
                    errorHelper.showErrorMessage(t("lima.entries.remove.transaction.error.lockedEntryBook",
                            e.getClosedPeriodicEntryBook().getEntryBook().getCode(),
                            e.getClosedPeriodicEntryBook().getEntryBook().getLabel(),
                            e.getClosedPeriodicEntryBook().getFinancialPeriod().getBeginDate(),
                            e.getClosedPeriodicEntryBook().getFinancialPeriod().getEndDate()));
                }
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Call delete selected row without selection");
            }
        }
    }

    public void selectionChanged() {
        FinancialTransactionDefaultTable table = view.getFinancialTransactionUnbalancedTable();
        FinancialTransactionUnbalancedTableModel tableModel = view.getFinancialTransactionUnbalancedTableModel();

        int selectedRow = table.getSelectedRow();
        view.setSelectedRow(selectedRow >= 0 && selectedRow < tableModel.size());
        if (selectedRow >= 0 && selectedRow < tableModel.size())  {
            FinancialTransaction transaction = tableModel.getTransactionAt(selectedRow);
            BigDecimal credit = transaction.getAmountCredit();
            BigDecimal debit = transaction.getAmountDebit();
            view.setBalance(credit.compareTo(debit) ==  0);
        } else {
            view.setBalance(true);
        }
    }

    public void balanceTransaction()  {
        FinancialTransactionDefaultTable table = view.getFinancialTransactionUnbalancedTable();
        FinancialTransactionUnbalancedTableModel tableModel = view.getFinancialTransactionUnbalancedTableModel();

        int rowSelected = table.getSelectedRow();
        if (rowSelected != -1) {

            if (table.isEditing()) {
                TableCellEditor editor = table.getCellEditor();
                editor.stopCellEditing();
            }
            Entry entry = tableModel.get(rowSelected);
            FinancialTransaction transaction = entry.getFinancialTransaction();
            BigDecimal credit = transaction.getAmountCredit();
            BigDecimal debit = transaction.getAmountDebit();
            if (entry.isDebit()) {
                debit = debit.subtract(entry.getAmount());
            } else {
                credit = credit.subtract(entry.getAmount());
            }

            BigDecimal balance = credit.subtract(debit);
            BigDecimal previousAmount = entry.getAmount();
            boolean previousDebit = entry.isDebit();
            entry.setAmount(balance.abs());
            entry.setDebit(balance.signum() > 0);
            if (tableModel.updateEntry(entry)) {
                int firstRow = tableModel.indexOf(transaction);
                int lastRow = firstRow + transaction.sizeEntry() - 1;
                tableModel.fireTableRowsUpdated(firstRow, lastRow);
                view.setBalance(true);

                ListSelectionModel selectionModel = table.getSelectionModel();
                selectionModel.setSelectionInterval(rowSelected, rowSelected);
                table.changeSelection(rowSelected, 1, false, false);
                table.editCellAt(rowSelected, 1);

                MainView mainView = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                mainView.setMainUiRefreshComponent(MainUiRefreshComponent.FINANCIAL_TRANSACTION_PANE);

            } else {
                entry.setAmount(previousAmount);
                entry.setDebit(previousDebit);
            }
        }
    }

    public void refresh() {
        FinancialTransactionUnbalancedTableModel tableModel = view.getFinancialTransactionUnbalancedTableModel();
        FinancialTransactionDefaultTable table = view.getFinancialTransactionUnbalancedTable();
        table.exit();

        tableModel.refresh();
        table.clearSelection();

        FiscalPeriodComboBoxModel comboBoxModel = view.getFiscalPeriodComboBoxModel();
        comboBoxModel.refresh();
    }
}
