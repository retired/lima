/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.ui.vatchart;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.LimaSwingConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.api.VatStatementService;
import org.chorem.lima.entity.VatStatement;
import org.chorem.lima.entity.VatStatementImpl;
import org.chorem.lima.enums.ImportExportEnum;
import org.chorem.lima.enums.VatStatementsChartEnum;
import org.chorem.lima.ui.importexport.ImportExport;
import org.chorem.lima.util.ErrorHelper;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

public class VatChartViewHandler implements ServiceListener {

    private transient Highlighter colorLine;

    protected VatStatementService vatStatementService;

    protected VatChartView view;

    protected ErrorHelper errorHelper;

    protected VatChartViewHandler(VatChartView view) {
        this.view = view;

        vatStatementService = LimaServiceFactory.getService(VatStatementService.class);
        LimaServiceFactory.addServiceListener(ImportService.class, this);
        errorHelper = new ErrorHelper(LimaSwingConfig.getInstance());

    }

    public static void DISPLAY_DIALOG(final JDialog dialog, Component view) {
        InputMap inputMap = dialog.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = dialog.getRootPane().getActionMap();
        String binding = "dispose";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.dispose();
            }
        });

        // jaxx constructor don't call super() ?
        dialog.setLocationRelativeTo(view);
        dialog.setVisible(true);
    }

    public void init() {

        InputMap inputMap = view.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        addShortCutEvent(inputMap);

        addDoubleClickEvent();

        addErrorLineColor();

        MutableTreeTableNode rootTreeTable = loadAllVatStatements();

        setUpTreeTableModel(rootTreeTable);

        addTreeTableListener();

    }

    protected void addTreeTableListener() {
        ListSelectionModel model = view.getVatTreeTable().getSelectionModel();
        model.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    JXTreeTable vatTreeTable = view.getVatTreeTable();
                    view.setSelectedRow(vatTreeTable.getSelectedRow() != -1);
                }
            }
        });
    }

    protected void addShortCutEvent(InputMap inputMap) {
        ActionMap actionMap = view.getActionMap();

        // add action on Ctrl + N
        String binding = "new-vatStatement";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVatStatementMovement();
            }
        });

        // add action on Delete
        binding = "remove-vatStatement";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), binding);
        actionMap.put(binding, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeVatStatement();
            }
        });

        // add action on Ctrl + M
        binding = "modify-vatStatement";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateVatStatement();
            }
        });

        // add action on Ctrl + I
        binding = "import-importVatStatementChart";
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK), binding);
        actionMap.put(binding, new AbstractAction() {
            private static final long serialVersionUID = -8953401784332356894L;

            @Override
            public void actionPerformed(ActionEvent e) {
                importVatStatementChart();
            }
        });
    }

    protected void addDoubleClickEvent() {
        JXTreeTable table = view.getVatTreeTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JXTreeTable source = (JXTreeTable) e.getSource();
                if (source.rowAtPoint(e.getPoint()) >= 0 && e.getClickCount() == 2) {
                    updateVatStatement();
                }
            }
        });
    }

    /** Highlights line when line is a leaf and is missing its BoxName parameter */
    protected void addErrorLineColor() {
        if (colorLine != null) {
            view.getVatTreeTable().removeHighlighter(colorLine);
        }
        HighlightPredicate predicate = (renderer, adapter) -> {
            JXTreeTable vatTreeTable = view.getVatTreeTable();
            TreePath treePath = vatTreeTable.getPathForRow(adapter.row);
            MutableTreeTableNode vatStatementNode = (MutableTreeTableNode) treePath.getLastPathComponent();
            VatStatement vatStatement = (VatStatement) vatStatementNode.getUserObject();
            boolean highlighted = false;
            if (!vatStatement.isHeader()) {
                highlighted = StringUtils.isBlank(vatStatement.getBoxName());
            }
            return highlighted;
        };
        colorLine = new ColorHighlighter(predicate, new Color(255, 198, 209), null);
        view.getVatTreeTable().addHighlighter(colorLine);
    }

    public void importVatStatementChart() {
        final VatChartImportForm importVatForm = new VatChartImportForm();
        DISPLAY_DIALOG(importVatForm, view);

        Object value = importVatForm.getChartVatStatementCombo().getSelectedItem();
        // if action confirmed
        if (value != null) {

            if (importVatForm.getDeleteVatStatementChart().isSelected()) {
                vatStatementService.removeAllVatStatement();
            }

            VatStatementsChartEnum vatStatementsEnum = (VatStatementsChartEnum) value;

            ImportExport importExport = new ImportExport(view);
            switch (vatStatementsEnum) {
                case IMPORT:
                    importExport.importExport(ImportExportEnum.CSV_VAT_IMPORT,
                            null,
                            null,
                            false);
                    break;
                default:
                    importExport.importExport(ImportExportEnum.CSV_VAT_IMPORT,
                            null,
                            vatStatementsEnum.getDefaultFileUrl(),
                            true);
            }
        }

        MutableTreeTableNode root = loadAllVatStatements();
        setUpTreeTableModel(root);
    }


    public void addVatStatementMovement() {

        final VatChartMovementForm editVatChartForm = new VatChartMovementForm(view);
        editVatChartForm.setIsUpdate(false);

        VatStatement masterVatStatement = getMasterVatStatement();
        VatStatement newVatChartMovement = new VatStatementImpl();
        newVatChartMovement.setMasterVatStatement(masterVatStatement);
        editVatChartForm.setVatStatement(newVatChartMovement);

        DISPLAY_DIALOG(editVatChartForm, view);
    }

    public void updateVatStatement() {

        JXTreeTable table = view.getVatTreeTable();
        int selectedRow = table.getSelectedRow();
        TreePath treePath = table.getPathForRow(selectedRow); // not null
        MutableTreeTableNode vatStatementNode = (MutableTreeTableNode) treePath.getLastPathComponent();
        VatStatement vatStatement = (VatStatement) vatStatementNode.getUserObject();

        // get current selection path
        final VatChartMovementForm editVatChartForm = new VatChartMovementForm(view);
        editVatChartForm.setIsUpdate(true);
        editVatChartForm.setVatStatement(vatStatement);

        DISPLAY_DIALOG(editVatChartForm, view);

    }

    public void createOrUpdateVatStatementMovement(VatChartMovementForm dialog) {

        JXTreeTable treeTable = view.getVatTreeTable();
        VatChartViewModel treeTableModel = (VatChartViewModel) treeTable.getTreeTableModel();
        VatStatement vatStatement = dialog.getVatStatement();

        try {
            int selectedRow = treeTable.getSelectedRow();
            TreePath treePath = treeTable.getPathForRow(selectedRow);

            if (dialog.isIsUpdate()) {
                VatStatement updatedVatStatement = createOrUpdateVatStatement(treeTableModel, vatStatement);
                if (updatedVatStatement != null) {
                    treeTableModel.valueForPathChanged(treePath, updatedVatStatement);
                }
            } else {
                if (selectedRow != -1) {
                    VatStatement persistedVatStatement = createOrUpdateVatStatement(treeTableModel, vatStatement);

                    if (persistedVatStatement != null) {
                        DefaultMutableTreeTableNode newNode = new DefaultMutableTreeTableNode(persistedVatStatement);
                        DefaultMutableTreeTableNode node = (DefaultMutableTreeTableNode) treePath.getLastPathComponent();

                        treeTableModel.insertNodeInto(newNode, node, node.getChildCount());
                        treeTable.expandPath(new TreePath(treeTableModel.getPathToRoot(node)));
                    }
                }
            }

        } finally {
            dialog.dispose();
        }

    }

    /** Ask for user to remove for selected account, and remove it if confirmed. */
    public void removeVatStatement() {
        JXTreeTable treeTable = view.getVatTreeTable();
        VatChartViewModel treeTableModel =
                (VatChartViewModel) treeTable.getTreeTableModel();

        // Any row selected
        int selectedRow = treeTable.getSelectedRow();
        if (selectedRow != -1) {
            int n = JOptionPane.showConfirmDialog(view,
                                                  t("lima.vatStatement.remove.confirm"),
                                                  t("lima.vatStatement.remove.title"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE);
            if (n == JOptionPane.YES_OPTION) {
                // update view of treetable
                TreePath treePath = treeTable.getPathForRow(selectedRow);
                MutableTreeTableNode child = (MutableTreeTableNode) treePath.getLastPathComponent();
                if (!treeTableModel.isLeaf(child)) {
                    JOptionPane.showMessageDialog(
                            view,
                            t("lima.vatStatement.delete.parent"),
                            t("lima.info"),
                            JOptionPane.INFORMATION_MESSAGE);
                } else {
                    VatStatement vatStatement = (VatStatement) child.getUserObject();
                    vatStatementService.removeVatStatement(vatStatement);
                    treeTableModel.removeNodeFromParent(child);
                }
            }
        }
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {}


    protected void addChildToParentsNode(DefaultMutableTreeTableNode parent, Collection<VatStatement> childs) {
        if (CollectionUtils.isNotEmpty(childs)) {
            for (VatStatement subStatement : childs) {
                DefaultMutableTreeTableNode subNodes = new DefaultMutableTreeTableNode(subStatement);
                parent.add(subNodes);
                if (subStatement.isHeader()) {
                    addChildToParentsNode(subNodes, subStatement.getSubVatStatements());
                }
            }
        }
    }

    protected MutableTreeTableNode loadAllVatStatements() {
        List<VatStatement> rootStatements = vatStatementService.getRootVatStatements();

        DefaultMutableTreeTableNode rootTreeTable = new DefaultMutableTreeTableNode(null);
        for (VatStatement rootStatement : rootStatements) {
            DefaultMutableTreeTableNode rootNode = new DefaultMutableTreeTableNode(rootStatement);
            rootTreeTable.add(rootNode);

            Collection<VatStatement> subVatStatements = rootStatement.getSubVatStatements();
            addChildToParentsNode(rootNode, subVatStatements);
        }
        return  rootTreeTable;
    }

    protected void setUpTreeTableModel(MutableTreeTableNode root) {
        // refreshing tree's model
        DefaultTreeTableModel model = new VatChartViewModel(root);

        model.setColumnIdentifiers(Arrays.asList(t("lima.table.label"), t("lima.table.account"), t("lima.table.boxName")));
        JXTreeTable table = view.getVatTreeTable();
        table.setTreeTableModel(model);
    }

    protected VatStatement getMasterVatStatement() {
        VatStatement masterVatStatement = null;
        JXTreeTable table = view.getVatTreeTable();
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            TreePath treePath = table.getPathForRow(selectedRow);
            MutableTreeTableNode node = (MutableTreeTableNode) treePath.getLastPathComponent();
            masterVatStatement = (VatStatement) node.getUserObject();
        }
        return masterVatStatement;
    }

    protected VatStatement createOrUpdateVatStatement(VatChartViewModel treeTableModel, VatStatement vatStatement) {
        VatStatement persistedVatStatement;

        if (vatStatement.isPersisted()) {
            persistedVatStatement = treeTableModel.updateVatStatement(vatStatement);
        } else {
            persistedVatStatement = treeTableModel.addVatStatement(vatStatement.getMasterVatStatement(), vatStatement);
        }

        return persistedVatStatement;
    }
}
