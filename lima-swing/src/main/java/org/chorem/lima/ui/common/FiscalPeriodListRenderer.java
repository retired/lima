/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.common;

import org.chorem.lima.entity.FiscalPeriod;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;
import java.text.SimpleDateFormat;

/**
 * Fiscal period list renderer.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FiscalPeriodListRenderer extends DefaultListCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -9089182547408397051L;

    protected SimpleDateFormat df = new SimpleDateFormat("yyyy");

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        FiscalPeriod fiscalPeriod = (FiscalPeriod)value;
        Object newValue = fiscalPeriod;

        if (fiscalPeriod != null) {

            String begin = df.format(fiscalPeriod.getBeginDate());
            String end = df.format(fiscalPeriod.getEndDate());

            if (begin.equals(end)) {

                newValue = begin;

            } else {

                newValue = begin + " - " + end;

            }
        }

        return super.getListCellRendererComponent(list, newValue, index, isSelected,
                cellHasFocus);
    }

}
