/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.ui.home;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaSwingApplicationContext;
import org.chorem.lima.business.ServiceListener;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.ui.MainView;

import javax.swing.event.HyperlinkEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Home view pane that display entry books statistics.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class EntryBooksPane extends AbstractHomePane implements ServiceListener {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(EntryBooksPane.class);

    protected EntryBookService entryBookService;

    public EntryBooksPane(HomeView view) {
        super(view);

        entryBookService = LimaServiceFactory.getService(EntryBookService.class);
        LimaServiceFactory.addServiceListener(EntryBookService.class, this);
        LimaServiceFactory.addServiceListener(ImportService.class, this);

        refresh();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType())) {
            if (e.getDescription().equals("#entrybookschart")) {
                MainView ui = LimaSwingApplicationContext.MAIN_UI_ENTRY_DEF.getContextValue(view);
                ui.getHandler().showEntryBookView(ui);
            }
        }
    }

    public void refresh() {

        log.debug("Rafraîchissement entry books pane");
        
        String htmlBegin = "<font face='sans-serif' size=3>"
                           + "<p style=vertical-align:'bottom', horizontal-align:'center'>";
        String htmlEnd = "</p></font>";

        List<EntryBook> entryBooks = entryBookService.getAllEntryBooks();
        int ebSize = entryBooks.size();
        if (ebSize > 0) {
            setBackground(GREEN_BACKGROUND);
            StringBuilder entryBooksString = new StringBuilder();
            if (ebSize == 1) {
                entryBooksString.append(t("lima.home.entryBooks.info.one", ebSize));
                entryBooksString.append("<br/>").append(entryBooks.get(0).getLabel());
                entryBooksString.append("<br/><br/><a href='#entrybookschart'>");
                entryBooksString.append(t("lima.home.entryBooks.modify")).append("</a>");

            } else {
                entryBooksString.append(t("lima.home.entryBooks.info", ebSize)).append("<ul>");
                for (int i = 0; i < ebSize && i < 3; i++) {
//                    for (EntryBook entryBook : entryBooks) {
                    EntryBook entryBook = entryBooks.get(i);
                    entryBooksString.append("<li>").append(entryBook.getCode());
                    entryBooksString.append(" - ").append(entryBook.getLabel()).append("</li>");
                }
                if (ebSize > 3) {
                    entryBooksString.append("<li> ... </li>");
                }
                entryBooksString.append("</ul></p><p horizontal-align:'center'>");
                entryBooksString.append("<a href='#entrybookschart'>");
                entryBooksString.append(t("lima.home.entryBooks.modify")).append("</a>");
            }
            //set Text
            setText(htmlBegin + entryBooksString + htmlEnd);
        } else {
            setBackground(RED_BACKGROUND);
            //set Text
            setText(htmlBegin
                    + t("lima.home.entryBooks.nothing")
                    + "<br/><br/><a href='#entrybookschart'>"
                    + t("lima.home.entryBooks.create") + "</a>" + htmlEnd);
        }
    }

    @Override
    public void notifyMethod(String serviceName, String methodName) {
        log.debug("Nom de la méthode : " + methodName);
        if (methodName.contains("EntryBook") || methodName.contains("importAll")
            || methodName.contains("importAs") || methodName.contains("importEntries")) {
            refresh();
        }
    }
}
