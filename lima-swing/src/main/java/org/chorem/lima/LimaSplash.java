/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.*;

/**
 * Utility class used to manage information on slash screen.
 * <p/>
 * Currently display:
 * <ul>
 * <li>Version
 * <li>Loading information
 * </ul>
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 * @since 0.3.2
 */
public class LimaSplash {

    /** Log. */
    static private Log log = LogFactory.getLog(LimaSplash.class);

    /** Splash screen instance. */
    protected SplashScreen splash;

    /** Splash screen graphics. */
    protected Graphics2D splashGraphics;

    public LimaSplash() {

        splash = SplashScreen.getSplashScreen();
        if (splash != null) {
            splashGraphics = splash.createGraphics();
        }
    }

    /** Init splash (add version). */
    public void initSplash() {
        if (splashGraphics != null) {
            splashGraphics.setColor(Color.GRAY);
            splashGraphics.drawRect(65, 253, 300, 10);
            splashGraphics.fillRect(65, 253, 0, 10);

            splashGraphics.setColor(Color.DARK_GRAY);
            Font font = new Font("Lucida Sans", Font.BOLD, 14);
            splashGraphics.setFont(font);
            splashGraphics.drawString("Lutin Invoice Monitoring and Accounting", 65, 249);

            splash.update();
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Can't find splash screen");
                log.warn("Make sure that lima is launched with -splash:image.png VM argument");
                log.warn("or SplashScreen-Image is present in jar manifest.");
            }
        }
    }

    public void drawVersion(String version) {
        if (splashGraphics != null) {
            splashGraphics.setColor(Color.DARK_GRAY);
            Font font = new Font("Lucida Sans", Font.PLAIN, 12);
            splashGraphics.setFont(font);
            splashGraphics.setColor(Color.BLACK);
            splashGraphics.drawString("Version : " + version, 394, 42);
            splash.update();
        }
    }

    /**
     * Update progression.
     *
     * @param progress progress (between 0.0 and 1.0)
     * @param message  message can be {@code null}
     */
    public void updateProgression(double progress, String message) {
        if (splashGraphics != null && splash.isVisible()) {

            splashGraphics.setColor(Color.GRAY);
            splashGraphics.drawRect(65, 253, 300, 10);
            splashGraphics.fillRect(65, 253, (int) (300.0 * progress), 10);

            if (message != null) {
                splashGraphics.setComposite(AlphaComposite.Clear);
                splashGraphics.fillRect(65, 238, 300, 15);
                splashGraphics.setPaintMode();
                splashGraphics.setColor(Color.DARK_GRAY);
                Font font = new Font("Lucida Sans", Font.BOLD, 14);
                splashGraphics.setFont(font);
                splashGraphics.drawString(message + "...", 65, 249);
            }
            splash.update();
        }
    }

    /** Close splash screen (if needed). */
    public void closeSplash() {
        if (splash != null && splash.isVisible()) {
            splash.close();
        }
    }
}
