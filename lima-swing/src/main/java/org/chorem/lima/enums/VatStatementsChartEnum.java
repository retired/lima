/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.enums;

import org.chorem.lima.beans.Labeled;

import java.net.URL;

import static org.nuiton.i18n.I18n.t;

public enum VatStatementsChartEnum implements Labeled {

    DEFAULT("vat_default.csv", t("lima.vatStatement.default")),
    IMPORT(null, t("lima.vatStatement.import"));

    protected final String filePath;

    protected String label;

    VatStatementsChartEnum(String filePath, String label) {
        this.filePath = filePath;
        this.label = label;
    }

    public URL getDefaultFileUrl() {
        URL url = ImportExportEnum.getFileURL("/import/" + filePath);
        return url;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
