/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.enums;

public enum EncodingEnum {

    UTF8("UTF-8", "UTF-8   Lima"),
    ISOLATIN1("ISO-8859-1", "ISO-Latin-1   Excel / Windows"),
    MACROMAN("MacRoman", "MacRoman Excel / Mac");

    protected final String encoding;

    protected final String description;

    EncodingEnum(String encoding, String description) {
        this.encoding = encoding;
        this.description = description;
    }

    public String getEncoding() {
        return encoding;
    }

    public String getDescription() {
        return description;
    }

    public static String[] descriptions() {
        int nbElts = EncodingEnum.values().length;
        String[] descriptions = new String[nbElts];
        EncodingEnum[] encodingEnums = EncodingEnum.values();
        for (int i = 0; i < nbElts; i++) {
            descriptions[i] = encodingEnums[i].getDescription();
        }
        return descriptions;
    }

}
