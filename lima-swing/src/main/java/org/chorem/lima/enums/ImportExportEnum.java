/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.enums;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaTechnicalException;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/** first Boolean importMode define if it's a import (true) or an export (false)
 *  second one to allow or not choce for encoding
 * */
 public enum ImportExportEnum {

    CSV_ALL_EXPORT(false, false), CSV_ALL_IMPORT(true, true),
    CSV_ACCOUNTCHARTS_EXPORT(false, true), CSV_ACCOUNTCHARTS_IMPORT(true, true),
    CSV_ENTRYBOOKS_EXPORT(false, true), CSV_ENTRYBOOKS_IMPORT(true, true),
    CSV_ENTRIES_EXPORT(false, true),CSV_ENTRIES_IMPORT(true, true),
    CSV_FINANCIALSTATEMENTS_EXPORT(false, true), CSV_FINANCIALSTATEMENTS_IMPORT(true, true),
    CSV_VAT_EXPORT(false, true), CSV_VAT_IMPORT(true, true),

    PDF_VAT_EXPORT(false, true), PDF_VAT_IMPORT(true, true),
    EBP_ACCOUNTCHARTS_EXPORT(false, false), EBP_ACCOUNTCHARTS_IMPORT(true, true),
    EBP_ENTRIES_EXPORT(false, false), EBP_ENTRIES_IMPORT(true, true),
    EBP_ENTRYBOOKS_EXPORT(false, false), EBP_ENTRYBOOKS_IMPORT(true, true);

    private static final Log log = LogFactory.getLog(ImportExportEnum.class);

    private final Boolean importMode;

    private final Boolean encodingOption;

    /**
     * import mode :
     * true if import, false if export
     */
    ImportExportEnum(Boolean importMode, Boolean encodingOption) {
        this.importMode = importMode;
        this.encodingOption = encodingOption;
    }

    protected static URL getFileURL(String filePAth) {
        URL result = ImportExportEnum.class.getResource(filePAth);

        if (result == null) {
            File file = new File(filePAth);
            if (log.isInfoEnabled()) {
                log.info(String.format("Load %s from File path", filePAth));
            }
            try {
                result = file.toURI().toURL();
            } catch (MalformedURLException e) {
                throw new LimaTechnicalException("Could not get url of file: " + file);
            }
        } else {
            if (log.isInfoEnabled()) {
                log.info(String.format("Load %s from resource path", filePAth));
            }
        }

        return result;
    }

    /**
     * encoding choice
     * true if import or export methode let encoding option
     */
    public Boolean getEncodingOption() {
        return encodingOption;
    }

    public Boolean getImportMode() {
        return importMode;
    }
}
