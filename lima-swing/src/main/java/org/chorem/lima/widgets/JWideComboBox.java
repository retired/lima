/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.widgets;

import javax.swing.*;
import java.awt.*;

/**
 * Cette combo box surcharge quelques methodes de l'UI par defaut
 * pour que le composant popup soit plus grand que la combobox
 * elle même.
 *
 * @author ore
 */
public class JWideComboBox extends JComboBox {

    /** serialVersionUID. */
    private static final long serialVersionUID = -6715271536163434385L;

    private boolean layingOut;

    public JWideComboBox() {
        setUI(new ScrollMetalComboUI());
    }

    public JWideComboBox(Object items[]) {
        super(items);
    }

    public JWideComboBox(ComboBoxModel aModel) {
        super(aModel);
    }

    @Override
    public void doLayout() {
        try {
            layingOut = true;
            super.doLayout();
        } finally {
            layingOut = false;
        }
    }

    @Override
    public Dimension getSize() {
        Dimension dim = super.getSize();
        if (!layingOut) {
            int max = Math.max(dim.width, getPreferredSize().width);
            dim.width = max > 600 ? 600 : max;
        }
        return dim;
    }
}
