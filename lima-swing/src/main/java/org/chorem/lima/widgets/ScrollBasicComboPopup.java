/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.widgets;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboPopup;

/**
 * TODO
 *
 * @author tchemit <chemit@codelutin.com>
 * @since TODO
 */ //put it at begin, otherwise jaxx complains again !!!
public class ScrollBasicComboPopup extends BasicComboPopup {

    /** serialVersionUID. */
    private static final long serialVersionUID = 2353523521723292218L;

    public ScrollBasicComboPopup(JComboBox combo) {
        super(combo);
    }

    @Override
    protected JScrollPane createScroller() {
        return new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                               JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }
}
