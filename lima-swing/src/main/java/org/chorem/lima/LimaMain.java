/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima;

import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.server.LimaServer;
import org.chorem.lima.ui.MainView;
import org.chorem.lima.ui.MainViewHandler;
import org.chorem.lima.ui.opening.OpeningView;

import javax.swing.*;
import java.util.Arrays;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Lima GUI main class.
 *
 * @author ore
 * @version $Revision$
 */
public class LimaMain {

    /** Log. */
    private static final Log log = LogFactory.getLog(LimaMain.class);

    /** Lima configuration. */
    protected static LimaSwingConfig config;

    /** splash */
    protected static LimaSplash splash;

    /**
     * Lima main method.
     *
     * @param args program args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {

        if (log.isInfoEnabled()) {
            log.info("Lima starts at " + new Date());
            log.info("Args: " + Arrays.toString(args));
        }

        // init root context
        LimaSwingApplicationContext context = init(args);

        // do actions
        config = context.getConfig();
        config.doAction(LimaSwingConfig.Action.AFTER_INIT_STEP);

        // display main ui
        if (config.isLaunchui()) {

            // override default exception management (after config init)
            Thread.setDefaultUncaughtExceptionHandler(new LimaExceptionHandler());
            System.setProperty("sun.awt.exception.handler", LimaExceptionHandler.class.getName());

            launch(context);
        }
    }

    /**
     * initialisation de l'application :
     * <p/>
     * chargement du context
     *
     * @param args les arguments passes a l'application
     * @return le context de l'application
     * @throws Exception pour toute erreur pendant l'init
     */
    public static LimaSwingApplicationContext init(String... args) throws Exception {

        // update splash
        splash = new LimaSplash();
        splash.initSplash();

        // to enable javassist on webstart, must remove any securityManager,
        // see if this can be dangerous (should not be since jnlp is signed ?)
        // moreover it speeds up the loading :)
        System.setSecurityManager(null);
        Runtime.getRuntime().addShutdownHook(new ShutdownHook());

        // init root context
        LimaSwingApplicationContext context = LimaSwingApplicationContext.init();
        LimaSwingConfig config = context.getConfig();
        config.parse(args);
        context.initI18n(config);

        return context;
    }

    protected static void launch(LimaSwingApplicationContext context) {

        // prepare ui look&feel and load ui properties
        try {
            SwingUtil.initNimbusLoookAndFeel();
        } catch (Exception e) {
            // could not find nimbus look-and-feel
            if (log.isWarnEnabled()) {
                log.warn(t("lima.lookAndFeel.nimbus.warning"));
            }
        }

        splash.drawVersion(config.getVersion());
        splash.updateProgression(0.1, t("lima.launch.services"));

        LimaServer.launch(config);
        LimaSwingConfig.getInstance().saveForUser("topia.persistence.classes");

        // load accounts and test if there is an account plan defined
        // if not, call #loadDefaultAccount()
        AccountService accountService = LimaServiceFactory.getService(AccountService.class);
        long accountCount = accountService.getAccountCount();
        if (accountCount == 0) {
            if (log.isInfoEnabled()) {
                log.info("Propose for default account loading");
            }
            OpeningView openingView = new OpeningView();
            openingView.setSize(800, 400);
            openingView.setLocationRelativeTo(null);
            openingView.setVisible(true);
        } else {
            splash.updateProgression(0.5, t("lima.launch.accounting"));
        }

        splash.updateProgression(0.7, t("lima.launch.webServer"));

        splash.updateProgression(1, t("lima.launch.finished"));

        // do init ui
        MainViewHandler uiHandler = context.getContextValue(MainViewHandler.class);
        final MainView ui = uiHandler.initUI(context);
        ui.setLocationRelativeTo(null);
        // defaut display home view (not closeable, but might be !)
        uiHandler.showHomeView(context);

        // show ui
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ui.setVisible(true);
            }
        });
    }

    public static class ShutdownHook extends Thread {

        public ShutdownHook() {
            super("Shutdown Lima");
        }

        @Override
        public void run() {
            try {
                LimaSwingApplicationContext.get().close();
                LimaServiceFactory.destroy();
                // force to kill main thread

                if (log.isInfoEnabled()) {
                    log.info(t("lima.init.closed", new Date()));
                }
                Runtime.getRuntime().halt(0);
            } catch (Exception ex) {
                if (log.isErrorEnabled()) {
                    log.error(t("lima.init.errorclosing"), ex);
                }
                Runtime.getRuntime().halt(1);
            }
        }
    }

}
