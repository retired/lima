/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.actions;

import org.chorem.lima.LimaMain;
import org.chorem.lima.LimaSwingConfig;

import java.util.Arrays;

import static org.nuiton.i18n.I18n.t;

/**
 * Les actions appellables via {@link LimaMain}.
 *
 * @author tony
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class MiscAction {

    /** La configuration de l'application. */
    protected LimaSwingConfig config;

    public MiscAction(LimaSwingConfig config) {
        this.config = config;
    }

    public void help() {
        System.out.println(t("lima.message.help.usage"));
        for (LimaSwingConfig.Option o : LimaSwingConfig.Option.values()) {
            System.out.println("\t" + o.getKey() + "(" + o.getDefaultValue() + "):" + o.getDescription());
        }

        System.out.println("Actions:");
        for (LimaSwingConfig.Action a : LimaSwingConfig.Action.values()) {
            System.out.println("\t" + Arrays.toString(a.aliases) + "(" + a.action + "):" + a.getDescription());
        }
    }
}
