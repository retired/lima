package org.chorem.lima.ui.account;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author David Cossé
 */
public class AccountViewHandlerTest {

    @Test
    public void testAccountLengthComparator() throws Exception {

//        int result = o1.getAccountNumber().length() - o2.getAccountNumber().length();
//        if (result == 0) {
//            // same length, compare accountNumber
//            result = o1.getAccountNumber().compareTo(o2.getAccountNumber());
//        }

        List<String> testsValues = Lists.newArrayList("", "", "", "A", "A", "A", "0", "0", "0", "00", "00", "00", "%£", "%£", "     ", "     ");

        List<Account> accounts = Lists.newArrayListWithCapacity(testsValues.size());

        for (String testValue : testsValues) {
            Account a = new AccountImpl();
            a.setAccountNumber(testValue);
            accounts.add(a);
        }

        try {
            Collections.sort(accounts, AccountViewHandler.ACCOUNT_LENGTH_COMPARATOR);
        } catch (Exception e) {
            Assert.fail("Unexpected exception: " + e.getMessage());
        }

    }
}
