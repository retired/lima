.. -
.. * #%L
.. * Lima :: Swing
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Packaging windows
-----------------

Le packaging Windows a été effectué avec cette configuration.
Il n'est pas inclut dans le pom en attendant la résoluation de 2 bugs.
Seul le resultat exe est dans l'assembly.

<plugin>
    <groupId>org.bluestemsoftware.open.maven.plugin</groupId>
    <artifactId>launch4j-plugin</artifactId>
    <version>1.0.0.3-SNAPSHOT</version>
    <executions>
      <execution>
        <id>launch4j</id>
        <phase>package</phase>
        <goals>
          <goal>launch4j</goal>
        </goals>
        <configuration>
          <dontWrapJar>true</dontWrapJar>
          <headerType>gui</headerType>
          <outfile>target/lima.exe</outfile>
          <jar>lima.jar</jar>
          <errTitle>${project.name}</errTitle>
          <jre>
            <minVersion>1.6.0</minVersion>
          </jre>
        </configuration>
      </execution>
    </executions>
  </plugin>
