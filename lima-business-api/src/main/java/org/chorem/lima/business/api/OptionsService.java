/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.business.api;

import org.chorem.lima.business.config.LimaConfigOptionDef;
import org.chorem.lima.business.utils.BigDecimalToString;

import java.util.Locale;

/**
 * Service de report des certaines configurations du swing dans business
 * <p/>
 * Actuellement :
 * <ul>
 * <li>scale</li>
 * </ul>
 *
 * @author salaun
 */

public interface OptionsService extends BigDecimalToString.Config {

    void setScale(String scale);

    void setCurrency(boolean currency);

    void setDecimalSeparator(String decimalSeparator);

    void setThousandSeparator(String thousandSeparator);

    LimaConfigOptionDef getScaleOption();

    LimaConfigOptionDef getCurrencyOption();

    LimaConfigOptionDef getDecimalSeparatorOption();

    LimaConfigOptionDef getThousandSeparatorOption();

    String getLimaHttpHostAddress();

    int getLimaHttpPort();

    void setLocal(Locale newLocale);
}
