/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.exceptions;

import javax.ejb.ApplicationException;

/**
 * Lima exception.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
@ApplicationException
public abstract class LimaException extends Exception {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -6876236663940184462L;

    /**
     * Constructs a new exception
     *
     */
    public LimaException() {
        super();
    }

    /**
     * Constructs a new exception with specified message.
     *
     */
    public LimaException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with specified message and cause.
     * @param message
     * @param cause
     */
    public LimaException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new exception with the specified cause.
     *
     * @param cause   cause
     */
    public LimaException(Throwable cause) {
        super(cause);
    }
}
