/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.api;

import org.chorem.lima.beans.BalanceTrial;
import org.chorem.lima.beans.ReportsDatas;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FiscalPeriod;

import java.util.Date;
import java.util.List;

/**
 * Service de generation des rapports.
 * <p/>
 * Actuellement:
 * <ul>
 * <li>balance</li>
 * <li>Bilan</li>
 * <li>TVA</li>
 * </ul>
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */

public interface ReportService {

    /**
     * Generation de la balance.
     *
     * @param beginDate
     * @param endDate
     * @param selectedAccounts
     * @param getEntries
     * @param movementedFilter
     * @return la balance
     * @throws LimaException
     */
    BalanceTrial generateBalanceTrial(Date beginDate,
                                      Date endDate,
                                      String selectedAccounts,
                                      Boolean getEntries,
                                      Boolean movementedFilter);

    /**
     * Generation du grand-livre.
     *
     * @param beginDate
     * @param endDate
     * @param selectedAccounts
     * @param movementedFilter
     * @return
     * @throws LimaException
     */
    BalanceTrial generateLedger(Date beginDate,
                                Date endDate,
                                String selectedAccounts,
                                Boolean movementedFilter);


    /**
     * Generation du rapports des comptes.
     *
     * @param account
     * @param thirdPartAccountsMode
     * @param beginDate
     * @param endDate
     * @return
     * @throws LimaException
     */
    ReportsDatas generateAccountsReports(Account account,
                                         Boolean thirdPartAccountsMode,
                                         Date beginDate,
                                         Date endDate);

    /**
     * Generation du rapports des journaux.
     *
     * @param entryBook
     * @param beginDate
     * @param endDate
     * @return
     * @throws LimaException
     */
    ReportsDatas generateEntryBooksReports(EntryBook entryBook,
                                           Date beginDate,
                                           Date endDate);

    /**
     * Generate VAT.
     *
     * @param fiscalPeriod
     * @return
     * @throws LimaException
     */
    List<Object> generateVat(FiscalPeriod fiscalPeriod);

}
