/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * This class surround a real service, but intercept addServiceListener
 * and removeServiceListener that are not defined on delegated service
 * object.
 * 
 * All other call are forwarded on notified to registered listeners.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ServiceMonitorableHandler implements InvocationHandler {

    private static final Log log =
            LogFactory.getLog(ServiceMonitorableHandler.class);

    /** The proxied service. */
    private Object service;

    /** Listeners on the service. */
    private List<ServiceListener> listeners;

    public ServiceMonitorableHandler(Object service) {
        this.service = service;
        listeners = new ArrayList<>();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        switch (method.getName()) {
            case "addServiceListener":
                addServiceListener((ServiceListener) args[0]);
                break;
            case "removeServiceListener":
                removeServiceListener((ServiceListener) args[0]);
                break;
            default:
                try {
                    result = method.invoke(service, args);
                } catch (InvocationTargetException eee) {
                    throw eee.getCause();
                }
                notifyMethod(method);
                break;
        }
        return result;
    }

    protected void addServiceListener(ServiceListener serviceListener) {
        listeners.add(serviceListener);
    }

    protected void removeServiceListener(ServiceListener serviceListener) {
        listeners.remove(serviceListener);
    }

    protected void notifyMethod(Method method) {
        String methodName = method.getName();
        
        // get is removed to only notify write modification
        if (!methodName.startsWith("get")) {
            for (ServiceListener serviceListener : listeners) {
                serviceListener.notifyMethod(service.toString(), methodName);
                if(log.isDebugEnabled()) {
                    log.debug("proxy : " + service.toString() + " " + methodName);
                }
            }
        }
    }
}
