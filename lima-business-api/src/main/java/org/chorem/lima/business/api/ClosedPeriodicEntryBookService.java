package org.chorem.lima.business.api;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;

import java.util.Date;
import java.util.List;

/**
 * Created by davidcosse on 17/01/14.
 */
public interface ClosedPeriodicEntryBookService {

    ClosedPeriodicEntryBook getByEntryBookAndFinancialPeriod(EntryBook entryBook, FinancialPeriod financialPeriod);

    List<ClosedPeriodicEntryBook> getAllByDates(Date beginDate, Date endDate);

    List<ClosedPeriodicEntryBook> getAllByEntryBookAndDates(EntryBook entryBook, Date beginDate, Date endDate);
    
    ClosedPeriodicEntryBook getByEntryBookAndFinancialPeriod(EntryBook entryBook, Date date);

    List<ClosedPeriodicEntryBook> getForDates(Date from, Date to);
}
