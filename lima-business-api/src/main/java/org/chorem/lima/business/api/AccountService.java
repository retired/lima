/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.api;

import org.chorem.lima.business.exceptions.AlreadyExistAccountException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnexistingAccount;
import org.chorem.lima.business.exceptions.UsedAccountException;
import org.chorem.lima.entity.Account;

import java.util.List;

/**
 * Account service.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface AccountService {

    /**
     * Return account count.
     *
     * @return account count
     */
    long getAccountCount();

    /**
     * Obtain the master account for the given account number.
     * <p/>
     * the master account if the account with the longer account number prefixing the given account number.
     * <p/>
     * Example: for account number 401AB, it could be 4, 40, 401,...
     *
     * @param accountNumber the account number
     * @return the master account number
     * @throws LimaException if any pb while loading account
     */
    Account getMasterAccount(String accountNumber);

    Account getAccountByNumber(String number);

    /**
     * Return all account ordered by account name.
     *
     * @return all account
     * @throws LimaException
     */
    List<Account> getAllAccounts();

    List<Account> getAllLeafAccounts();

    /**
     * Return all {@code account}-s subaccounts.
     * 
     * @param account account to get sub accounts
     * @return sub accounts
     * @throws LimaException
     */
    List<Account> getAllSubAccounts(Account account);

    /**
     * Create or update account
     * @param account the account to create
     * @return true if updated or false if created
     * @throws InvalidAccountNumberException
     */
    boolean createOrUpdateAccount(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException, NotAllowedLabelException;

    /**
     * Create new account. If {@code masterAccount} is not null, {@code account}
     * is added in {@code masterAccount}'s subAccounts.
     *
     * @param account account
     * @throws LimaException
     */
    Account createAccount(Account account) throws AlreadyExistAccountException, InvalidAccountNumberException, NotNumberAccountNumberException;

    Account updateAccount(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException;

    void removeAccount(Account account) throws UsedAccountException, UnexistingAccount;

    List<Account> stringToListAccounts(String selectedAccounts);

    Account findAccountById(String accountId);

    /**
     * Remove all accounts from Lima.
     * It's suppose they have no related Entities
     */
    void removeAllAccounts();
}
