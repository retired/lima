/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.api;

import org.chorem.lima.business.exceptions.AlreadyExistEntryBookException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.UsedEntryBookException;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;

import java.util.List;

/**
 * Entry book service.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public interface EntryBookService {

    /**
     * Find entry book by code.
     *
     * @param code entry book code
     * @return
     * @throws LimaException
     */
    EntryBook getEntryBookByCode(String code);

    /**
     * Get all entry book.
     *
     * @return all entrybook
     * @throws LimaException
     */
    List<EntryBook> getAllEntryBooks();

    /**
     * Create new EntryBook not persisted yet.
     * @return
     */
    EntryBook createNewEntryBook();

    /**
     * Create or update the given entry book
     * @param entryBook
     * @return true if updated, false if created
     */
    boolean createOrUpdateEntryBook(EntryBook entryBook);

    EntryBook createImportedNewEntryBook(EntryBook entryBook, List<FinancialPeriod> financialPeriods);

    /**
     * Create new entry book.
     *
     * @param entryBook
     * @throws LimaException
     */
    EntryBook createEntryBook(EntryBook entryBook) throws AlreadyExistEntryBookException;

    /**
     * Permet de modifier un journal.
     *
     * @param entryBook journal
     * @throws LimaException
     */
    EntryBook updateEntryBook(EntryBook entryBook);

    /**
     * Permet d'effacer un EntryBook dans la base de données.
     * <p/>
     * ATTENTION : si un EntryBook est associé avec des entrées, il est alors
     * impossible de supprimer celui-ci.
     *
     * @param entryBook
     * @throws org.chorem.lima.business.exceptions.UsedEntryBookException
     */
    void removeEntryBook(EntryBook entryBook) throws UsedEntryBookException;

    /**
     * Find all Entry book with code the collection of codes given in parameter
     * @param entryBookCodes collection of entry book codes
     * @return All looked for entry books
     */
    List<EntryBook> findAllEntryBookByEntryBookCodes(List<String> entryBookCodes);

    /**
     * Remove all Lima Entry Books
     * /!\ becarefull they must not be related to other entities
     */
    void removeAllEntryBooks();
}
