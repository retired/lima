/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.api;

import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.AlreadyExistAccountException;
import org.chorem.lima.business.exceptions.AlreadyExistEntryBookException;
import org.chorem.lima.business.exceptions.AlreadyLockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LastUnlockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoEmptyFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoFoundFinancialPeriodException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FiscalPeriod;

import java.util.Date;
import java.util.List;

/**
 * Fiscal period service.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public interface FiscalPeriodService {

    /**
     *
     * @return all Fiscal periods
     */
    List<FiscalPeriod> getAllFiscalPeriods();

    /**
     *
     * @return all locked fiscal periods
     */
    List<FiscalPeriod> getAllBlockedFiscalPeriods();

    /**
     *
     * @return all unlocked fiscal periods
     */
    List<FiscalPeriod> getAllUnblockedFiscalPeriods();

    /**
     *
     * @return first fiscal Period
     */
    FiscalPeriod getFirstFiscalPeriod();

    /**
     *
     * @return last fiscal period
     */
    FiscalPeriod getLastFiscalPeriod();

    /**
     *
     * @param fiscalPeriod the fiscal period to create
     * @return the persisted fiscal period
     * @throws BeginAfterEndFiscalPeriodException
     * @throws NotBeginNextDayOfLastFiscalPeriodException
     * @throws MoreOneUnlockFiscalPeriodException
     */
    FiscalPeriod createFiscalPeriod(FiscalPeriod fiscalPeriod)
            throws BeginAfterEndFiscalPeriodException,
            NotBeginNextDayOfLastFiscalPeriodException,
            MoreOneUnlockFiscalPeriodException;

    /**
     * Block the current fiscal period
     * @param fiscalPeriod the fiscal period to block
     * @return
     * @throws AlreadyLockedFiscalPeriodException
     * @throws LastUnlockedFiscalPeriodException
     */
    FiscalPeriod blockFiscalPeriod(FiscalPeriod fiscalPeriod)
            throws AlreadyLockedFiscalPeriodException, LastUnlockedFiscalPeriodException,
            UnbalancedFinancialTransactionsException, WithoutEntryBookFinancialTransactionsException,
            UnfilledEntriesException;

    /**
     * Remove the given fiscal period
     * @param fiscalPeriod the fiscal period to remove
     * @throws NoEmptyFiscalPeriodException
     */
    void deleteFiscalPeriod(FiscalPeriod fiscalPeriod) throws NoEmptyFiscalPeriodException;

    /**
     * Eventually add retained earnings on last unclosed fiscal period,
     * and in any case, block fiscal period
     * @param fiscalPeriod fiscal period selected, and to block
     * @param entryBook entry book where entries will be save in new fiscal period
     * @return fiscal period blocked
     * */
    FiscalPeriod retainedEarningsAndBlockFiscalPeriod(FiscalPeriod fiscalPeriod, EntryBook entryBook)
            throws LockedFinancialPeriodException, LockedEntryBookException, NoFoundFinancialPeriodException,
            LastUnlockedFiscalPeriodException, AlreadyLockedFiscalPeriodException, AlreadyExistAccountException,
            InvalidAccountNumberException, NotNumberAccountNumberException, NotAllowedLabelException,
            AlreadyExistEntryBookException, UnfilledEntriesException, UnbalancedFinancialTransactionsException,
            WithoutEntryBookFinancialTransactionsException, AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException;

    /**
     * Test if we have retained earnings on a fiscal period
     * @param fiscalPeriod the fiscal period to test from
     * @return true if retained earnings are possible
     * */
    boolean isRetainedEarnings(FiscalPeriod fiscalPeriod);

    /**
     * Update the fiscal period end date
     * @param fiscalPeriod the fiscal period where end date must be updated
     * @return
     */
    FiscalPeriod updateEndDate(FiscalPeriod fiscalPeriod);

    /**
     * Return the FiscalPeriod for the given date
     *
     * @param date fiscal period for this given date
     * @return the FiscalPeriod for {@code date}
     */
    FiscalPeriod getFiscalPeriodForDate(Date date);
}
