/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.api;

import org.chorem.lima.business.ExportResult;
import org.chorem.lima.business.ImportExportResults;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FiscalPeriod;

/**
 * Import export service.
 * <p/>
 * Currently import and export as XML.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public interface ExportService {

    //####################################### CSV ##############################################

    ImportExportResults exportAccountsAsCSV(String charset);

    ImportExportResults exportEntryBooksAsCSV(String charset);

    ImportExportResults exportFiscalPeriodsAsCSV(String charset);

    ImportExportResults exportEntriesAsCSV(String charset, Boolean humanReadable);

    ImportExportResults exportFinancialStatements(String charset);

    ImportExportResults exportVatStatements(String charset);

    ImportExportResults exportAll(String charset);

    //####################################### EBP ##############################################

    ImportExportResults exportAccountAsEbp(String charset);

    ImportExportResults exportEntriesAsEbp(String charset);

    ImportExportResults exportEntryBookAsEbp(String charset);

    //########## Fichier des écritures comptable en cas de contrôle fiscal #####################

    ExportResult exportFiscalControl(FiscalPeriod period, EntryBook entryBookAtNew, String charset);

    String getFiscalControlFileName(FiscalPeriod fiscalPeriod);
}
