package org.chorem.lima.business.api;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;

import java.util.Date;
import java.util.List;

/**
 * Created by davidcosse on 17/01/14.
 */
public interface EntryService {

    Entry createEntry(Entry entry);

    /**
     * Return values of financial transaction related to the given ClosedPeriodicEntryBook
     * @param closedPeriodicEntryBook The ClosedPeriodicEntryBook from where financial transaction are returned
     * @return list of tables of 0: boolean (true:credit), 1: amount
     */
    List<Object[]> findDebitCreditOfTransaction(ClosedPeriodicEntryBook closedPeriodicEntryBook);


    List<Entry> findAllEntriesByDatesForEntryBook(EntryBook entryBook, Date beginDate, Date endDate);

    /**
     * Persist all given entries without any rules checked
     */
    void saveAllEntries(List<Entry> entries);
}
