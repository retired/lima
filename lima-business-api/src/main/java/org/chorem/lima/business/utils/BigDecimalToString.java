package org.chorem.lima.business.utils;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class BigDecimalToString {

    public interface Config {
        int getScale();
        boolean getCurrency();
        char getDecimalSeparator();
        char getThousandSeparator();
    }

    public static DecimalFormat newDecimalFormat(Config config) {
        StringBuilder scale = new StringBuilder();
        for (int i = 0; i < config.getScale(); i++) {
            scale.append("0");
        }
        String currency = "";
        if (config.getCurrency()) {
            currency = " ¤";
        }
        DecimalFormat result = new DecimalFormat("##0." + scale.toString() + currency);
        DecimalFormatSymbols symbol = new DecimalFormatSymbols();
        //set decimalSeparator and thousandSeparator preferences
        symbol.setDecimalSeparator(config.getDecimalSeparator());
        symbol.setMonetaryDecimalSeparator(config.getDecimalSeparator());
        symbol.setGroupingSeparator(config.getThousandSeparator());
        result.setDecimalFormatSymbols(symbol);
        //always set grouping
        result.setGroupingUsed(true);
        result.setGroupingSize(3);
        return result;
    }

}
