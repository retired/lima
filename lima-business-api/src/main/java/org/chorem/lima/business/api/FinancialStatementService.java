/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.api;

import org.chorem.lima.beans.FinancialStatementAmounts;
import org.chorem.lima.business.exceptions.AlreadyExistFinancialStatementException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.FinancialStatement;

import java.util.Date;
import java.util.List;

public interface FinancialStatementService {

    /**
     * Create a new FinancialStatement not persisted yet
     * @return
     */
    FinancialStatement newFinancialStatement();

    FinancialStatement createMasterFinacialStatements(FinancialStatement masterFinancialStatements);

    FinancialStatement createFinancialStatement(FinancialStatement masterFinancialStatement,
                                  FinancialStatement financialStatement) throws AlreadyExistFinancialStatementException, NotAllowedLabelException;

    FinancialStatement updateFinancialStatement(FinancialStatement financialStatement);

    void removeFinancialStatement(FinancialStatement financialStatement);

    void removeAllFinancialStatement();

    List<FinancialStatement> getAllFinancialStatements();

    List<FinancialStatement> getRootFinancialStatements();

    List<FinancialStatementAmounts> financialStatementReport(Date selectedBeginDate,
                                                             Date selectedEndDate);

    /**
     *
     * @return  la liste de compte non utilisés.
     */
    List<Account> checkFinancialStatementChart();

    /**
     * Check if Financial Statement exist according the label given as parameter.
     * @param label the Financial Statement's label
     * @return true if a statement with same label exist otherwise false.
     * @throws LimaException
     */
    boolean checkFinancialStatementExist(String label);

    FinancialStatement getFinancialStatementByLabel(String label);
}
