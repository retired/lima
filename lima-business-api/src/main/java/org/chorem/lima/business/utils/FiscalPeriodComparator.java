/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.utils;

import org.chorem.lima.entity.FiscalPeriod;

import java.io.Serializable;
import java.util.Comparator;

public class FiscalPeriodComparator implements Serializable, Comparator<FiscalPeriod> {

    private static final long serialVersionUID = 1L;

    /** sort by fiscal begin date */
    @Override
    public int compare(FiscalPeriod o1, FiscalPeriod o2) {
        return o1.getBeginDate().compareTo(o2.getBeginDate());
    }

}
