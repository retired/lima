package org.chorem.lima.business.api.report;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sf.jasperreports.engine.JasperReport;
import org.chorem.lima.beans.DocumentReport;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by davidcosse on 19/11/14.
 */
public interface ProvisionalEntryBookReportService {

    /**
     * @param beginDate period from
     * @param endDate period to
     * @param entryBookCodes selected entry books if null all are selected
     * @param entryBooksJasperReport Jasper sub report for entryBooks representation
     * @param financialPeriodsJasperReport Jasper sub report for entryBooks financial periods representation
     * @param transactionsJasperReport Jasper sub report for entry books transactions representation
     * @return model for entry books report
     */
    DocumentReport getEntryBookDocumentReport(Date beginDate, Date endDate, List<String> entryBookCodes, DecimalFormat bigDecimalFormat, JasperReport entryBooksJasperReport, JasperReport financialPeriodsJasperReport, JasperReport transactionsJasperReport);
}
