package org.chorem.lima.business;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.rmi.server.ExportException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidcosse on 04/08/14.
 */
public class ExportResult  implements Serializable {

    private static final long serialVersionUID = -7708723957001648683L;

    protected Class fromSource;

    public ExportResult(Class fromSource) {
        this.fromSource = fromSource;
    }

    /**
     * all exception catch during export
     */
    protected List<ExportException> exportExceptions;

    /**
     * export result: data as String
     */
    protected String exportData;

    public String getExportData() {
        return exportData;
    }

    public void setExportData(String exportData) {
        this.exportData = exportData;
    }

    public List<ExportException> getExportExceptions() {
        return exportExceptions;
    }

    public void addException(ExportException e) {
        exportExceptions = exportExceptions == null ? new ArrayList<ExportException>() : exportExceptions;
        exportExceptions.add(e);
    }

    public Class getFromSource() {
        return fromSource;
    }
}
