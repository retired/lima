/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.business.api;

import org.chorem.lima.beans.VatStatementAmounts;
import org.chorem.lima.business.exceptions.AlreadyAffectedVatBoxException;
import org.chorem.lima.business.exceptions.AlreadyExistVatStatementException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.entity.VatStatement;

import java.util.Date;
import java.util.List;

public interface VatStatementService {

    VatStatement createVatStatement(VatStatement masterVatStatement,
                            VatStatement vatStatement) throws AlreadyExistVatStatementException, NotAllowedLabelException, AlreadyAffectedVatBoxException;

    List<VatStatement> getAllVatStatements();

    List<VatStatement> getAllChildrenVatStatement(VatStatement vatStatement,
                                                  List<VatStatement> result);

    List<VatStatement> getChildrenVatStatement(VatStatement masterVatStatement);

    VatStatement updateVatStatement(VatStatement vatStatement) throws AlreadyExistVatStatementException, AlreadyAffectedVatBoxException, NotAllowedLabelException;

    VatStatementAmounts vatStatementAmounts(VatStatement vatStatement,
                                            Date selectedBeginDate,
                                            Date selectedEndDate);

    VatStatement findVatStatementByLabel(String label);

    List<VatStatementAmounts> vatStatementReport(Date beginDate,
                                                 Date endDate);

    void removeAllVatStatement();

    void removeVatStatement(VatStatement vatStatement);

    /**
     * Check if VAT Statement exist according the label given as parameter.
     * @param label the VAT Statement's label
     * @return true if a VAT statement with same label exist otherwise false.
     * @throws LimaException
     */
    boolean checkVatStatementExist(String label);

    List<VatStatement> getRootVatStatements();

    VatStatement newVatStatement();
}
