/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.utils;

public enum ImportExportEntityEnum {

    ACCOUNT("ACCN"),
    ENTRYBOOK("ENBK"),
    FINANCIALSTATEMENT("FNST"),
    FISCALPERIOD("FSCP"),
    CLOSEDPERIODICENTRYBOOK("CPEB"),
    FINANCIALTRANSACTION("FTRC"),
    ENTRY("NTRY"),
    VATSTATEMENT("VAT"),
    IDENTITY("IDNT"),
    VATPDF("VATPDF");

    private final String label;

    ImportExportEntityEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static ImportExportEntityEnum valueOfLabel(String label) {
        ImportExportEntityEnum value = null;

        for (ImportExportEntityEnum importExportEntityEnum : ImportExportEntityEnum.values()) {
            if (label.equals(importExportEntityEnum.label)) {
                value = importExportEntityEnum;
                break;
            }
        }
        return value;
    }
}


