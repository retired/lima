package org.chorem.lima.business;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by davidcosse on 04/08/14.
 */
public class ImportExportResults implements Serializable{

    private static final long serialVersionUID = -5941835433984457107L;
    List<ImportResult> importResults;

    List<ExportResult> exportResults;

    protected boolean errors;

    public List<ImportResult> getImportResults() {
        return importResults;
    }

    public List<ExportResult> getExportResults() {
        return exportResults;
    }

    public void addImportResult(ImportResult result) {
        importResults = importResults == null ? new ArrayList<ImportResult>() : importResults;
        importResults.add(result);
    }

    public void addAllImportResult(Collection<ImportResult> results) {
        importResults = importResults == null ? new ArrayList<ImportResult>() : importResults;
        importResults.addAll(results);
    }

    protected void addExportResult(ExportResult result) {
        exportResults = exportResults == null ? new ArrayList<ExportResult>() : exportResults;
        exportResults.add(result);
    }

    protected void addAllExportResult(Collection<ExportResult> results) {
        exportResults = exportResults == null ? new ArrayList<ExportResult>() : exportResults;
        exportResults.addAll(results);
    }

    public ImportResult createAddAndGetImportResult(Class fromSource) {
        ImportResult result = new ImportResult(fromSource);
        addImportResult(result);
        return result;
    }

    public ExportResult createAddAndGetExportResult(Class fromSource) {
        ExportResult result = new ExportResult(fromSource);
        addExportResult(result);
        return result;
    }

    /**
     * add all ExportResults from the given ImportExportResults to the current ImportExportResults
     * @param results
     */
    public void pushExportResults(ImportExportResults results) {
        addAllExportResult(results.getExportResults());
    }

    /**
     * add all ExportResults from the given ImportExportResults to the current ImportExportResults
     * @param results
     */
    public void pushImportResults(ImportExportResults results) {
        addAllImportResult(results.getImportResults());
    }

    public void setErrors(boolean errors) {
        this.errors = errors;
    }

    public boolean isErrors() {
        return errors;
    }
}
