/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.api;

import org.chorem.lima.business.exceptions.NotLockedClosedPeriodicEntryBooksException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;

import java.util.Date;
import java.util.List;

/**
 * Financial period service.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */

public interface FinancialPeriodService {

    ClosedPeriodicEntryBook getClosedPeriodicEntryBook(EntryBook entryBook,
                                                       FinancialPeriod financialPeriod);

    List<ClosedPeriodicEntryBook> getAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriod();

    ClosedPeriodicEntryBook blockClosedPeriodicEntryBook(ClosedPeriodicEntryBook closedPeriodicEntryBook)
            throws UnbalancedFinancialTransactionsException,
            UnfilledEntriesException,
            WithoutEntryBookFinancialTransactionsException,
            NotLockedClosedPeriodicEntryBooksException;

    List<FinancialPeriod> getAllFinancialPeriods();

    FinancialPeriod getFinancialForDate(Date date);

    FinancialPeriod getFinancialPeriodIfExistsByDate(Date date);

    List<FinancialPeriod> getFinancialPeriodsWithBeginDateWithin(Date beginDateFirst,
                                                                 Date endDateLast);
}
