package org.chorem.lima.business.exceptions;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class MoreOneUnlockFiscalPeriodException extends LimaException {

    private static final long serialVersionUID = -9215191603583397411L;

    private Date beginDate;
    private Date endDate;
    private Boolean isLocked;
    private long countUnlockFiscalPeriod;

    public MoreOneUnlockFiscalPeriodException(Date beginDate, Date endDate, Boolean isLocked, long countUnlockFiscalPeriod) {
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.isLocked = isLocked;
        this.countUnlockFiscalPeriod = countUnlockFiscalPeriod;

    }

    public Date getBeginDate() {
        return beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Boolean getIsLocked() {
        return isLocked;
    }

    public long getCountUnlockFiscalPeriod() {
        return countUnlockFiscalPeriod;
    }
}
