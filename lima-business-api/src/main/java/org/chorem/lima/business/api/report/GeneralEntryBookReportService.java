package org.chorem.lima.business.api.report;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sf.jasperreports.engine.JasperReport;
import org.chorem.lima.beans.DocumentReport;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * Created by davidcosse on 17/11/14.
 */
public interface GeneralEntryBookReportService {

    /**
     * Generate the necessary beans to make general entry book report.
     *  @param from from date
     * @param to to date
     * @param generalEntryBooksJasperReport Jasper sub report for general entry book's representation
     * @param entriesJasperReport Jasper sub report for general entry book's entries representation
     * */
    DocumentReport getGeneralEntryBookDocumentReport(Date from, Date to, DecimalFormat bigDecimalFormat, JasperReport generalEntryBooksJasperReport, JasperReport entriesJasperReport);
}
