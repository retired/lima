package org.chorem.lima.business;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.exceptions.ImportFileException;
import org.chorem.lima.business.exceptions.LimaException;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by davidcosse on 24/07/14.
 */
public class ImportResult implements Serializable{

    private static final Log log = LogFactory.getLog(ImportResult.class);

    protected int nbCreated;

    protected int nbUpdated;

    protected int nbIgnored;

    protected int lineIndex;

    protected Class fromSource;

    protected Map<Integer, LimaException> allExceptions;

    public ImportResult(Class fromSource) {
        nbCreated = 0;
        nbUpdated = 0;
        nbIgnored = 0;
        lineIndex = 2;// line 1 s header
        allExceptions = Maps.newHashMap();
        this.fromSource = fromSource;
    }

    public int getNbCreated() {
        return nbCreated;
    }

    public int getNbUpdated() {
        return nbUpdated;
    }

    public int getNbIgnored() {
        return nbIgnored;
    }

    public void increaseCreated() {
        nbCreated++;
        lineIndex++;
    }

    public void increaseUpdated() {
        nbUpdated++;
        lineIndex++;
    }

    public void increaseIgnored() {
        nbIgnored++;
        lineIndex++;
    }

    public void addException(LimaException e) {
        // TODO dcosse ref:1395 quick fix
        log.error(e instanceof ImportFileException ? ((ImportFileException) e).getDetailMessage() : e);
        if (allExceptions.size() < 50) {
            allExceptions.put(this.lineIndex, e);
        }
        nbIgnored++;
        lineIndex++;
    }

    public void addInitException(LimaException e) {
        allExceptions.put(1, e);
        nbIgnored++;
        lineIndex++;
    }

    public Map<Integer, LimaException> getAllExceptionsByLine() {
        return allExceptions;
    }

    /**
     *
     * @return the class of the targeted entities
     */
    public Class getFromSource() {
        return fromSource;
    }
}
