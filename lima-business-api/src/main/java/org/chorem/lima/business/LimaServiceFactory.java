/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/** This class is a service factory based on embedded openejb container. */
public class LimaServiceFactory {

    /** Log. */
    private static Log log = LogFactory.getLog(LimaServiceFactory.class);

    /** Single instance. */
    private static LimaServiceFactory instance;

    /** Service cache. */
    protected Map<Class, Object> services = new HashMap<>();

    /** EJB container. */
    protected static EJBContainer container;

    /** Context where to look for bean*/
    protected static Context context;

    /** Suffix to add to interface class name to be able to find out bean. */
    protected static final String EJB_SUFFIX = "Remote";

    /**
     * Init openejb container.
     * 
     * @param config  configuration
     */
    public static void initFactory(ApplicationConfig config) {
        // make a copy of options
        Properties props = new Properties();
        
        // warning, make a clean properties, openejb put all those properties in jvm wide scope
        // introducing strange behaviour in AppConfig loading
        // only copy necessary options
        props.putAll(config.getOptionStartsWith("java.naming"));
        props.putAll(config.getOptionStartsWith("ejbd"));
        props.putAll(config.getOptionStartsWith("openejb"));

        // transmission des options de logging a openejb
        // sinon, il utilise son propre pattern interne
        InputStream log4jFile = null;
        try {
            log4jFile = LimaServiceFactory.class.getResourceAsStream("/log4j.properties");
            Properties propsLog4j = new Properties();
            propsLog4j.load(log4jFile);
            props.putAll(propsLog4j);
        } catch (IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't find log4j properties file", ex);
            }
        } finally {
            IOUtils.closeQuietly(log4jFile);
        }

        // as of openejb 4.6, bean-finder-shaded 3.15 doesn't scan full classpath anymore by default
        // see http://list.chorem.org/pipermail/lima-devel/2014-May/000154.html for more details
        System.setProperty("xbean.finder.use.get-resources", "true");

        // see http://openejb.apache.org/embedded-configuration.html
        // http://openejb.apache.org/properties-listing.html
        // for embedded configuration.
        try {
            context = new InitialContext(props);
        } catch (NamingException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get context", e);
            }
        }
    }

    public static <M> M getService(Class<M> serviceMonitorableClass) {
        LimaServiceFactory factory = getInstance();
        Map<Class, Object> services = factory.getServices();
        M result = (M) services.get(serviceMonitorableClass);
        if (result == null) {
            result = factory.newService(serviceMonitorableClass);
            services.put(serviceMonitorableClass, result);
        }
        return result;
    }

    public static <M> void addServiceListener(Class<M> serviceClass, ServiceListener listener) {
        M service = getService(serviceClass);
        addServiceListener(service, listener);
    }

    public static <M> void addServiceListener(M service, ServiceListener listener) {
        Preconditions.checkArgument(service instanceof ServiceMonitorable, "service " + service + " does not implement " + ServiceMonitorable.class);
        ((ServiceMonitorable) service).addServiceListener(listener);
    }


    public static <M> void removeServiceListener(Class<M> serviceClass, ServiceListener listener) {
        ServiceMonitorable service = (ServiceMonitorable) getService(serviceClass);
        service.removeServiceListener(listener);
    }

    /**
     * Close openejb container.
     */
    public static void destroy() {
        if (container != null) {
            container.close();
        }
    }

    /**
     * Return service factory singleton instance.
     * <p/>
     * Init it at first call.
     *
     * @return singleton instance
     */
    protected static LimaServiceFactory getInstance() {
        if (instance == null) {
            instance = new LimaServiceFactory();
        }
        return instance;
    }

    protected Map<Class, Object> getServices() {
        return services;
    }

    protected <M> M newService(Class<M> serviceMonitorableClass) {
        Object ejbHome;
        String serviceName = serviceMonitorableClass.getSimpleName().replace("Monitorable", "Impl");
        // only usefull during non monitorable transition
        if (!serviceName.endsWith("Impl")) {
            serviceName += "Impl";
        }
        try {
            // TODO DCossé 09/03/15 necessary as it is done as now but not sure it's good practice.
            ejbHome = context.lookup(serviceName + EJB_SUFFIX);
        } catch (NamingException eee) {
            throw new RuntimeException(
                    "Can't lookup for service : " + serviceName, eee);

        }
        InvocationHandler handler = new ServiceMonitorableHandler(ejbHome);
        ClassLoader classLoader = serviceMonitorableClass.getClassLoader();
        M result = (M) Proxy.newProxyInstance(
                classLoader,
                new Class[]{ServiceMonitorable.class, serviceMonitorableClass},
                handler
        );
        return result;
    }
}
