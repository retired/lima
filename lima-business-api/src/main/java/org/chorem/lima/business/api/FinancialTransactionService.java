/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.api;

import org.chorem.lima.beans.FinancialTransactionCondition;
import org.chorem.lima.beans.LetteringFilter;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.UnbalancedEntriesException;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Transaction service.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */

public interface FinancialTransactionService {

    /**
     * Create new FinancialTransaction not persisted yet.
     * @return
     */
    FinancialTransaction createNewFinancialTransaction();

    /**
     * Create new Entry not persisted yet.
     * @return
     */
    Entry createNewEntry();

    Iterable<FinancialTransaction> persistImportedFinancialTransactions(Collection<FinancialTransaction> financialtransactions);

    FinancialTransaction createFinancialTransactionWithEntries(FinancialTransaction financialtransaction, Collection<Entry> financialtransactionEntries)
            throws  AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException;

    /**
     * Persist transaction without any entry
     *
     * @throws AfterLastFiscalPeriodException
     * @throws BeforeFirstFiscalPeriodException
     * @throws LockedFinancialPeriodException
     * @throws LockedEntryBookException
     */
    FinancialTransaction createFinancialTransactionSkeleton(FinancialTransaction financialtransaction, Boolean validAccountingRules)
            throws  AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException;

    void updateFinancialTransaction(FinancialTransaction financialtransaction)
            throws AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException;

    void removeFinancialTransaction(FinancialTransaction financialtransaction)
            throws LockedFinancialPeriodException, LockedEntryBookException;

    FinancialTransaction getFinancialTransactionWithId(String id);

    List<FinancialTransaction> getAllFinancialTransactions(FinancialPeriod period);

    List<FinancialTransaction> getAllFinancialTransactions(Date beginDate, Date endDate);

    List<FinancialTransaction> getAllFinancialTransactions(FinancialPeriod period, EntryBook entryBook);

    List<FinancialTransaction> getAllInexactFinancialTransactions(FiscalPeriod fiscalPeriod);

    List<FinancialTransaction> getAllFinancialTransactionsBalanced(FiscalPeriod fiscalPeriod);

    List<FinancialTransaction> getAllFinancialTransactions(FiscalPeriod period);

    List<FinancialTransaction> searchFinancialTransaction(FinancialTransactionCondition financialTransactionFilter);

    void createdImportedEntry(Entry entry);

    Iterable<Entry> createAllEntries(Collection<Entry> entries, Boolean checkRules) throws LockedFinancialPeriodException, LockedEntryBookException;

    Entry createEntry(Entry entry) throws LockedFinancialPeriodException, LockedEntryBookException;

    void updateEntry(Entry entry) throws LockedEntryBookException, LockedFinancialPeriodException;

    void removeEntry(Entry entry) throws LockedFinancialPeriodException, LockedEntryBookException;

    /** Find the last letter used
    * and increment them
    * @return  a string representing the last letter
    * */
    String getNextLetters();

    /** Find all the letters used
    * @return  a list of string containing all the letters
    * */
    List<String> getAllLetters();

    /**
    * From to selected entries, create one with same account and
    * difference between the amounts, and another one with
    * account 658 or 758 and entrybook Extourne
    * and an amount conversely of the first created
    * @param firstEntrySelected first entry selected
    * @param secondEntrySelected second entry selected
    * @return table of the two new entries
    * */
    Entry[] getEntriesFromEqualizing(Entry firstEntrySelected, Entry secondEntrySelected)
            throws LockedFinancialPeriodException, LockedEntryBookException, AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException;

    /**
    * Retourne toutes les entrées d'une transaction
    * pour un compte et la présence d'un lettrage ou (xor) non
    * @param filter filtre sur les entrees, selon le compte, les dates de debut et de fin, et le lettrage
    * */
    List<Entry> getAllEntrieByDatesAndAccountAndLettering(LetteringFilter filter);

    /**
     *
     * @param filter filter define: starting date, ending date, account
     * @return All Entries related to an unclosed entry book according to the filter.
     */
    List<Entry> getAllUnlockEntriesByFilter(LetteringFilter filter);

    /**
     * Retourne la somme des débits credits des entrées associées à une action pour une prériode définie
     * La période est défine entre DateStart inclus et DateEnd exclus
     * @param filter sur le filter doit être définie la période et le compte cible.
     * @return
     */
    List<Object[]> getAccountEntriesDebitCreditFromIncludingToExcludingPeriod(LetteringFilter filter);

    /**
     * Retourne la somme des débits credits des entrées associées à une action pour une prériode définie
     * La période est défine entre DateStart inclus et DateEnd inclus
     * @param filter sur le filter doit être définie la période et le compte cible.
     * @return
     */
    List<Object[]> getAccountEntriesDebitCreditFromIncludingToIncludingPeriod(LetteringFilter filter);

    /**
     * Retourne la dernière entrée d'une transaction
     * @param financialTransaction transaction sur laquelle la derniere entree est selectionnee
     * */
    Entry getLastEntry(FinancialTransaction financialTransaction);


    /**
     * Retourne la list des ecritures lettrées
     * @param entries la list des ecritures lettrées
     * @throws UnbalancedEntriesException, LockedEntryBookException
     * */
    List<Entry> addLetter(List<Entry> entries) throws UnbalancedEntriesException, LockedEntryBookException;

    List<Entry> removeLetter(String letter);

    void persistImportedEntries(Collection<Entry> values);

    Iterable<FinancialTransaction> updateAllImportedFinancialTransactions(Iterable<FinancialTransaction> savedTransactions);
}
