package org.chorem.lima.business.api;

/*
 * #%L
 * Lima :: business API
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.business.ImportExportResults;

/**
 * Created by davidcosse on 03/06/14.
 */
public interface ImportService {

    //####################################### CSV ##############################################

    ImportExportResults importAccountAsCSV(String contents);

    ImportExportResults importEntryBooksAsCSV(String contents);

    ImportExportResults importFiscalPeriodsAsCSV(String contents);

    ImportExportResults importEntriesAsCSV(String contents);

    ImportExportResults importFinancialStatementsAsCSV(String contents);

    ImportExportResults importVATStatementsAsCSV(String contents);

    ImportExportResults importAll(String entryBooks, String transactions, String fiscalPeriods, String accounts, String entries, String identity);

    //####################################### EBP ##############################################

    ImportExportResults importAccountFromEbp(String datas);

    ImportExportResults importEntryBookFromEbp(String datas);

    ImportExportResults importEntriesFromEbp(String datas);

    /**
     * Remove BCR, Financial Transaction and VAT
     */
    void removeAccountabilityLayouts();

    /**
     * Remove all entry books
     * /!\ be careful, entry books must not be related to other entities
     */
    void removeInitallyImportedEntryBook();

//    String importAsPDF(String data,
//                       ImportExportEntityEnum importExportEntityEnum,
//                       boolean saveMode);
}
