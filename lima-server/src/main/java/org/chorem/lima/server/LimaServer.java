package org.chorem.lima.server;

/*
 * #%L
 * Lima :: Server
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.openejb.client.RemoteInitialContextFactory;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.report.LimaReportConfig;
import org.nuiton.config.ApplicationConfig;

import javax.naming.Context;
import javax.naming.NamingException;

/**
 * Lima server class.
 * <p/>
 * Starts openejb server.
 *
 * @author chatellier
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
public class LimaServer {

    private static final Log log = LogFactory.getLog(LimaServer.class);
    public static final String EJB_URL = "ejbd://%s:%d";
    public static final String LIMA_HOST_ADDRESS = "lima.host.address";

    /** http serveur */
    protected static HttpServerService httpServerService;

    /**
     * To lock
     */
    private static final Object LOCK = new Object();

    /**
     * Lima server.
     *
     * @param args
     * @throws NamingException
     */
    public static void main(String... args) throws NamingException {

        String accountabilityName = null;
        if (args!= null && args.length > 0 && StringUtils.isNotBlank(args[0])) {
            accountabilityName = args[0];
        }

        LimaServerConfig serverConfig = LimaServerConfig.getInstance(accountabilityName);

        launch(serverConfig.getConfig());

        LimaServerConfig.getInstance().getConfig().saveForUser();

        // block main otherwize, main will end
        synchronized (LOCK) {
            try {
                LOCK.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (log.isInfoEnabled()) {
            log.info("LimaServer shutdown");
        }
    }

    public static void launch(ApplicationConfig moduleConfig) {

        boolean isRemoteMode = moduleConfig.getOptionAsBoolean(LimaServerConfig.ServerConfigOption.EJB_REMOTABLE.getKey());
        boolean mustStartServer = StringUtils.isBlank(moduleConfig.getOption(LIMA_HOST_ADDRESS)) || isRemoteMode;

        ApplicationConfig initConfig = moduleConfig;

        LimaBusinessConfig config = LimaBusinessConfig.getInstance(initConfig);
        initConfig = config.getConfig();

        if (mustStartServer) {
            // server or standalone configs
            initConfig = LimaServerConfig.getInstance(initConfig).getConfig();
            initConfig = LimaBusinessConfig.getInstance(initConfig).getConfig();
            initConfig = LimaReportConfig.getInstance(initConfig).getConfig();

            // this is to map key ex from lima.host.ejb.bind to ejbd.bind
            setServeurEjbParams(initConfig);

        } else {
            // client config
            setClientEjbParams(initConfig);
        }

        String url = String.format(EJB_URL, config.getHostEJBAddress(), config.getHostEjbPort());
        initConfig.setOption(Context.PROVIDER_URL, url);

        // start EJB container (either local or remote)
        LimaServiceFactory.initFactory(initConfig);

        // start web server only if it's on server mode
        if (mustStartServer) {
            setHttpServerParams(isRemoteMode, initConfig);
            httpServerService = new HttpServerService();
            httpServerService.start();
        }
    }

    protected static void setClientEjbParams(ApplicationConfig initConfig) {
        initConfig.setOption(LimaBusinessConfig.BusinessConfigOption.HOST_EJB_ADDRESS.getKey(), initConfig.getOption(LIMA_HOST_ADDRESS));
        initConfig.setOption(Context.INITIAL_CONTEXT_FACTORY, RemoteInitialContextFactory.class.getName());
    }

    protected static void setServeurEjbParams(ApplicationConfig initConfig) {
        initConfig.setOption(LimaServerConfig.ServerConfigOption.EJB_BIND.getKey(), initConfig.getOption(LimaBusinessConfig.BusinessConfigOption.HOST_EJB_BIND.getKey()));
        initConfig.setOption(LimaServerConfig.ServerConfigOption.EJB_PORT.getKey(), initConfig.getOption(LimaBusinessConfig.BusinessConfigOption.HOST_EJB_PORT.getKey()));
    }


    protected static void setHttpServerParams(boolean isRemoteMode, ApplicationConfig initConfig) {
        boolean isDefaultHttpAddressUsed = LimaBusinessConfig.getInstance().getHostHttpAddress().contentEquals(LimaBusinessConfig.BusinessConfigOption.HOST_HTTP_ADDRESS.getDefaultValue());
        if (isRemoteMode && isDefaultHttpAddressUsed) {
            // in case of client access to server throw remote ejb mode, and no HTTP address is provied, the HTTP server must not use the default localhost address but the ejb one.
            String httpAddress = initConfig.getOption(LimaBusinessConfig.BusinessConfigOption.HOST_EJB_ADDRESS.getKey());
            LimaBusinessConfig.getInstance().setHostHttpAddress(httpAddress);
        }
    }

    public static HttpServerService getHttpServerService() {
        return httpServerService;
    }

}
