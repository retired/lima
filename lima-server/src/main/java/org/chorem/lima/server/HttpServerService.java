package org.chorem.lima.server;

/*
 * #%L
 * Lima :: Server
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.OptionsService;
import org.chorem.lima.entity.Account;
import org.chorem.lima.report.DocumentsEnum;
import org.chorem.lima.report.service.DocumentService;
import org.chorem.lima.report.service.GeneratedReport;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.hibernate.exception.GenericJDBCException;
import org.nuiton.util.Resource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

public class HttpServerService {

    protected static final Log log = LogFactory.getLog(HttpServerService.class);

    protected static final String DATE_FORMAT = "yyyy-MM-dd";

    protected DocumentService documentService;

    protected Server server;

    protected String limaHttpHost;
    protected int limaHttpPort;

    public HttpServerService() {
        init();
    }

    /** start the server */
    public void start() {
        if (server == null) {
            try {

                InetAddress host = InetAddress.getByName(limaHttpHost);
                InetSocketAddress inetSocketAddress = InetSocketAddress.createUnresolved(host.getHostAddress(), limaHttpPort);
                server = new Server(inetSocketAddress);

                ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
                context.setContextPath("/");
                context.addServlet(new ServletHolder(new MainServlet()),"/");
                server.setHandler(context);
                server.start();

                if (log.isInfoEnabled()) {
                    String urlFormat = "http://%s:%d";
                    log.info(String.format("Web server running on :" + urlFormat, limaHttpHost, limaHttpPort));
                }

            } catch (Exception eee) {
                log.error(String.format("error while booting http server with address %s:%d", limaHttpHost, limaHttpPort), eee);
            }
        }
    }

    protected void init() {

        OptionsService optionsService = LimaServiceFactory.getService(OptionsService.class);

        limaHttpHost = optionsService.getLimaHttpHostAddress();
        limaHttpPort = optionsService.getLimaHttpPort();
        documentService = new DocumentService();
    }

    /**
     * This servlet send a static html page.
     */
    public class MainServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            doGet(req, resp);
        }

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            try {

                if (log.isDebugEnabled()) {
                    log.debug("doGet");
                }

                //get all params
                String imageParam = req.getParameter("img");
                String model = req.getParameter("model");
                String beginDate = req.getParameter("beginDate");
                String endDate = req.getParameter("endDate");
                String account = (StringUtils.isBlank(req.getParameter("account")) ? null : URLDecoder.decode(req.getParameter("account"), "UTF-8"));
                String autocomplete = req.getParameter("autocomplete") == null ? "true" : req.getParameter("autocomplete");
                String isGeneral = req.getParameter("isGeneral") == null ? "true" : req.getParameter("isGeneral");
                if (imageParam != null) {
                    // render image
                    doImage(resp, imageParam);
                } else if (model != null && beginDate != null && endDate != null) {
                    // generate and render report
                    doCreateReport(resp, model, beginDate, endDate, account, isGeneral, autocomplete);
                } else {
                    // render home HTML
                    doHomeHtml(req, resp);
                }

            } catch (GenericJDBCException eee) {
                log.error("Can't read db", eee);
            }
        }

        protected void doImage(HttpServletResponse resp, String imageParam) throws IOException {
            URL image = Resource.getURLOrNull("images/" + imageParam);
            if (image != null) {
                resp.setContentType(FormatsEnum.PNG.getMimeType());
                InputStream in = image.openStream();
                OutputStream out = resp.getOutputStream();
                IOUtils.copy(in, out);
            }
        }

        protected void doCreateReport(HttpServletResponse resp, String model, String beginDate, String endDate,
                                      String account, String isGeneral, String autocomplete) throws IOException {


            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

            try {
                Date beginDateFormat = sdf.parse(beginDate);
                Date endDateFormat = sdf.parse(endDate);

                GeneratedReport generatedReport =
                        documentService.createReport(beginDateFormat, endDateFormat, model, account, isGeneral, autocomplete);

                if (Strings.isNullOrEmpty(generatedReport.getHtmlContent())) {
                    InputStream in = generatedReport.getPdfStream();

                    resp.setContentType(FormatsEnum.PDF.getMimeType());
                    OutputStream out = resp.getOutputStream();
                    IOUtils.copy(in, out);
                } else {
                    String report = generatedReport.getHtmlContent();

                    resp.setContentType(FormatsEnum.HTML.getMimeType());
                    OutputStream out = resp.getOutputStream();
                    IOUtils.write(report, out, Charsets.UTF_8);
                }

            } catch (ParseException pe) {
                throw new LimaTechnicalException("Cannot parse dates", pe);
            }
        }

        protected void doHomeHtml(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
            if (log.isDebugEnabled()) {
                log.debug("Home html");
            }

            resp.setContentType(FormatsEnum.HTML.getMimeType());
            StringBuilder pageContent = new StringBuilder();

            log.info("Page load");

            /** create server Address using static config if exist or dynamical address */
            String urlFormat = "http://%s:%d";
            String serverAddress;
            if (limaHttpHost.equals("")) {
                serverAddress = String.format(urlFormat, req.getServerName(), req.getServerPort());
            } else {
                serverAddress = String.format(urlFormat, limaHttpHost, limaHttpPort);
            }

            Calendar calendar = Calendar.getInstance();
            Date endDatePicker = calendar.getTime();
            Date beginDatePicker = calendar.getTime();
            beginDatePicker =
                    DateUtils.truncate(beginDatePicker, Calendar.YEAR);

            Locale locale = LimaBusinessConfig.getInstance().getLocal();

            pageContent.append("<!DOCTYPE html>\n" + "<html lang=\"");
            pageContent.append(locale.getLanguage());
            pageContent.append("\">\n"
                    + "<head>\n"
                        + "<script type=\"text/javascript\">" +
                    "        function showAccountInput(){" +
                    "          var request = document.getElementById('model');" +
                    "          var requestValue = request == null ? '': request.value;" +
                    "          var isDocumentAccount = requestValue === 'lima_account'; " +
                    "          if (document.getElementById('accountField') != null) {" +
                    "            if (isDocumentAccount) {" +
                    "              document.getElementById('accountField').style.display = 'block';" +
                    "            } else {" +
                    "              document.getElementById('accountField').style.display = 'none';" +
                    "            }" +
                    "          }" +
                    "          var request = document.getElementById('model');" +
                    "          var requestValue = request == null ? '': request.value;" +
                    "          var isDocumentBalance = requestValue === 'lima_balance'; " +
                    "          if (document.getElementById('balanceType') != null) {" +
                    "            if (isDocumentBalance) {" +
                    "              document.getElementById('balanceType').style.display = 'block';" +
                    "            } else {" +
                    "              document.getElementById('balanceType').style.display = 'none';" +
                    "            }" +
                    "          }" +
                    "        }" +
                    "        showAccountInput();" +
                    "      </script>"
                        + "<title>LIMA Documents Report</title>\n" + "<style type=\"text/css\">" + "body { font: 14px sans-serif; }" + "h1 { font: 20px sans-serif; text-align: center; }" + "table.padding td {padding-right:20px;}" + ".tdright {text-align: right;}" + "</style>"
                    + "</head>\n"
                    + "<body>\n" + "<table class=\"padding\">\n" + "<tr><td><img src=\"?img=puzzle_icon_mini.png\"/></td>\n" + "<td><h1>LIMA Documents Report</h1></td></tr>\n" + "<tr><td class=\"tdright\"><img src=\"?img=identity.png\"/></td>\n"
                        + "<td rowspan=3><form method=GET action=")
                    .append(serverAddress).append(">\n")
                    .append(t("lima-business.document.date.begin"))
                    .append("<input value=\"")
                    .append(sdf.format(beginDatePicker))
                    .append("\" type=\"date\" name=\"beginDate\">\n")
                    .append(t("lima-business.document.date.end"))
                    .append("<input value=\"")
                    .append(sdf.format(endDatePicker))
                    .append("\" type=\"date\" name=\"endDate\">\n<br/><br/>");

            pageContent.append("Documents : <select id=\"model\" name=\"model\" onchange='showAccountInput()'>");

            for (DocumentsEnum documentsEnum : DocumentsEnum.values()) {
                pageContent.append("<option value=\"")
                        .append(documentsEnum.getFileName())
                        .append("\">")
                        .append(documentsEnum.getDescription())
                        .append("</option>\n");
            }

            List<Account> accounts = documentService.getAllAccounts();

            StringBuilder optionAccounts = new StringBuilder();
            for (Account account1:accounts) {
                optionAccounts.append("<option value=\"" + account1.getTopiaId() + "\">");
                optionAccounts.append(account1.getAccountNumber() + " - " + account1.getLabel());
                optionAccounts.append("</option>\n");
            }

            pageContent.append(  "</select>"
                               + "<div id='accountField'>Compte : <select id=\"account\" name=\"account\">"
                               +   optionAccounts.toString()
                               + "</select></div>"
                               + "<div id='balanceType'>"
                               + "  <input type=\"radio\" name=\"isGeneral\" value=\"true\" checked>" + t("lima-business.document.generalBalanceReport.choice") + "\n"
                               + "  <br>\n"
                               + "  <input type=\"radio\" name=\"isGeneral\" value=\"false\">" + t("lima-business.document.globalBalanceReport.choice")
                               + "</div>"
                               + "<input type=\"submit\">\n"
                               + "</form></td>"
                               + "<tr><td class=\"tdright\"><img src=\"?img=entries.png\"/></td></tr>\n"
                               + "<tr><td class=\"tdright\"><img src=\"?img=accounts.png\"/></td></tr>\n"
                               + "<tr><td class=\"tdright\"><img src=\"?img=entrybooks.png\"/></td>\n"
                               + "<tr><td class=\"tdright\"><img src=\"?img=fiscalperiods.png\"/></td></tr>\n"
                               + "</table>\n"
                               + "<script type=\"text/javascript\">"
                               + "  showAccountInput();"
                               + "</script>"
                               + "</body>\n"
                               + "</html>");

            resp.getWriter().write(pageContent.toString());
        }

    }
}
