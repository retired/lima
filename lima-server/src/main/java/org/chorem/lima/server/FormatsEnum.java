package org.chorem.lima.server;

/*
 * #%L
 * Lima :: Server
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


public enum FormatsEnum {

    HTML(".html", "text/html", "Page Html"),
    PDF(".pdf", "application/pdf", "Document PDF"),
    PNG(".png", "image/png", "Image PNG");

    private final String extension;

    private final String mimeType;

    private final String description;

    FormatsEnum(String extension, String mimeType, String description) {
        this.extension = extension;
        this.mimeType = mimeType;
        this.description = description;
    }

    public String getExtension() {
        return extension;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getDescription() {
        return description;
    }

    public static FormatsEnum valueOfExtension(String extension) {
        FormatsEnum value = null;

        for (FormatsEnum formatsEnum : FormatsEnum.values()) {
            if (extension.equals(formatsEnum.getExtension())) {
                value = formatsEnum;
                break;
            }
        }
        return value;
    }


}
