package org.chorem.lima.server;

/*
 * #%L
 * Lima :: Server
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.openejb.core.LocalInitialContextFactory;
import org.chorem.lima.LimaTechnicalException;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.config.ConfigOptionDef;

import javax.naming.Context;

import java.util.Properties;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 13/03/15.
 */
public class LimaServerConfig {

    protected static final Log log = LogFactory.getLog(LimaServerConfig.class);

    protected ApplicationConfig config;

    protected static LimaServerConfig instance;

    public static final String DEFAULT_CONFIG_FILE_NAME = "lima-server.config";

    private LimaServerConfig(String configFileName) {
        try {
            ApplicationConfig defaultConfig = new ApplicationConfig(DEFAULT_CONFIG_FILE_NAME);
            defaultConfig.loadDefaultOptions(ServerConfigOption.values());
            defaultConfig.parse();

            if (StringUtils.isNotBlank(configFileName)) {
                Properties flatOptions = defaultConfig.getFlatOptions(false);

                config = new ApplicationConfig(flatOptions, configFileName);
                config.parse();
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("No specific configuration provided, using the default one");
                }
                config = defaultConfig;

            }
            instance = this;
        } catch (ArgumentsParserException ex) {
            throw new LimaTechnicalException("Can't read configuration", ex);
        }
    }

    private LimaServerConfig(final ApplicationConfig config) {
        try {
            ApplicationConfig defaultConfig = new ApplicationConfig(DEFAULT_CONFIG_FILE_NAME);
            defaultConfig.loadDefaultOptions(ServerConfigOption.values());
            defaultConfig.parse();

            if (config != null) {
                Properties flatOptions = defaultConfig.getFlatOptions();
                flatOptions.putAll(config.getFlatOptions(true));
                this.config = new ApplicationConfig(flatOptions, DEFAULT_CONFIG_FILE_NAME);
                this.config.parse();
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("No specific configuration provided, using the default one");
                }
                this.config = defaultConfig;
            }
            instance = this;
        } catch (ArgumentsParserException ex) {
            throw new LimaTechnicalException("Can't read configuration", ex);
        }
    }

    public synchronized static LimaServerConfig getInstance(String configFileName) {
        if (instance == null) {
            instance= new LimaServerConfig(configFileName);
        }
        return instance;
    }

    public synchronized static LimaServerConfig getInstance(ApplicationConfig config) {
        if (instance == null) {
            instance= new LimaServerConfig(config);
        }
        return instance;
    }

    public synchronized static LimaServerConfig getInstance() {
        return getInstance("");
    }

    public ApplicationConfig getConfig() {
        return config;
    }

    public Properties getFlatOptions() {
        return config.getFlatOptions();
    }


    public enum ServerConfigOption implements ConfigOptionDef {

        EJB_INITIAL_CONTEXT_FACTORY(Context.INITIAL_CONTEXT_FACTORY, "", LocalInitialContextFactory.class.getName(), String.class, false, true),
        EJB_REMOTABLE("openejb.embedded.remotable", "", "true", Boolean.class, false, true),
        EJB_PORT("ejbd.port", "", "4202", Integer.class, false, false),
        EJB_BIND("ejbd.bind", "", "0.0.0.0", String.class, false, false);
        private final String key;

        private final String description;

        private String defaultValue;

        private final Class<?> type;

        private boolean transientBoolean;

        private boolean finalBoolean;

        ServerConfigOption(String key, String description, String defaultValue,
                           Class<?> type, boolean transientBoolean, boolean finalBoolean) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.finalBoolean = finalBoolean;
            this.transientBoolean = transientBoolean;
        }

        @Override
        public boolean isFinal() {
            return finalBoolean;
        }

        @Override
        public void setFinal(boolean finalBoolean) {
            this.finalBoolean = finalBoolean;
        }

        @Override
        public boolean isTransient() {
            return transientBoolean;
        }

        @Override
        public void setTransient(boolean transientBoolean) {
            this.transientBoolean = transientBoolean;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public String getDescription() {
            return t(description);
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }
    }
}
