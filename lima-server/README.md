# Configuration du serveur

Éditez le fichier de configuration (lima-server.config) qui se trouve ici :

* Unix-like : $HOME/.config/lima-server.config
* Windows 7 et + : C:\Users\USER_NAME\AppData\Roaming\lima-server.config
* Mac OS : $HOME/Library/Application Support/lima-server.config

Ajoutez la ligne suivantes sur en remplaçant l'IP en exemple (192.168.1.37) par celle du serveur :

* lima.host.ejb.address=192.168.1.37

les lignes suivantes sont optionnelles:

* lima.host.ejb.bind=0.0.0.0            # valeur par défaut
* lima.host.ejbd.port=4202              # valeur par défaut
* lima.host.http.address=192.168.1.137  (si aucune adresse n'est spécifiée celle du serveur ejb est utilisée)
* lima.host.http.port=5462              # valeur par défaut


# Configuration des clients

Pour configurer les clients pour se connecter au serveur, éditez le fichier de configuration de la machine cliente(lima-swing.config) qui se trouve ici :

* Unix-like : $HOME/.config/lima-swing.config
* Windows 7 et + : C:\Users\USER_NAME\AppData\Roaming\lima-swing.config
* Mac OS : $HOME/Library/Application Support/lima-swing.config

Ajoutez la ligne suivante sur en remplaçant l'IP en exemple (192.168.1.37) par celle du serveur :

* lima.host.address=192.168.1.37
