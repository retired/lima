
package org.ofbiz.lima;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericFindException;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;

import java.util.List;
import java.util.Map;

/**
 *
 * @author ore
 */
public class PeriodService {

    /** log */
    private static final Log log = LogFactory.getLog(PeriodService.class);

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map getAllPeriod(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("getAllPeriod : ");

        GenericDelegator delegator = dctx.getDelegator();

        List list = delegator.findByAnd("GlPeriod", UtilMisc.toMap("glPeriodTypeEnumId", "PT_GL"));

        if (list == null) {
            throw new GenericFindException("GlPeriod list can't be found.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service getAllPeriod succeeded.");
        result.put("list", list);
        return result;
    }
}
