
package org.ofbiz.lima;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ofbiz.accounting.transaction.generated.AcctgTransactionItemBase;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.GenericCreateException;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericFindException;
import org.ofbiz.entity.GenericRemoveException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ore
 */
public class TransactionService {

    /** log */
    private static final Log log = LogFactory.getLog(TransactionService.class);

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map getAllTransaction(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("getAllTransaction : ");

        GenericDelegator delegator = dctx.getDelegator();

        List<GenericValue> list = delegator.findByAnd("AcctgTransactionItemAndGlPeriod", UtilMisc.toMap("glPeriodTypeEnumId", "PT_GL"));

        if (list == null) {
            throw new GenericFindException("AcctgTransactionItemAndGlPeriod list is null.");
        }

        /** HACK */
        for (GenericValue value : list) {
            setStatus(value);
        }
        /** */
        
        Map<String, Object> result = ServiceUtil.returnSuccess("Service getAllTransaction succeeded.");
        result.put("list", list);
        return result;
    }

    /**
     * 
     * @param value
     */
    protected static void setStatus(GenericValue value) {
        String aTrStatusStatusId = (String) value.get("aTrStatusStatusId");
        value.set("voucherContentId", aTrStatusStatusId);
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map getTransactionByPeriod(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("getTransactionByPeriod : ");

        GenericDelegator delegator = dctx.getDelegator();
        String idNum = (String) context.get("idSeq");

        // Result list
        List list = new LinkedList();

        // List of AcctgTransactionPeriod
        List listTransPeriod = delegator.findByAnd("AcctgTransactionPeriod", UtilMisc.toMap(
                "glPeriodIdNum", idNum));

        if (listTransPeriod == null) {
            throw new GenericFindException("AcctgTransactionPeriod can't be found.");
        }

        for (Iterator it = listTransPeriod.iterator(); it.hasNext();) {
            GenericValue v = (GenericValue) it.next();
            String atranIdName = (String) v.get("atranIdName");
            List listTransItem = delegator.findByAnd("AcctgTransactionItem", UtilMisc.toMap(
                    "atranIdName", atranIdName));
            if (listTransItem == null) {
                throw new GenericFindException("AcctgTransactionItem can't be found.");
            }

            list.addAll(listTransItem);
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service getTransactionByPeriod succeeded.");
        result.put("list", list);
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     * @throws GenericServiceException 
     */
    public static Map removeTransaction(DispatchContext dctx, Map context) throws GenericEntityException, GenericServiceException {
        log.info("removeTransaction : ");

        LocalDispatcher dispatcher = dctx.getDispatcher();
        GenericDelegator delegator = dctx.getDelegator();

        GenericValue userLogin = delegator.findByPrimaryKey("UserLogin", UtilMisc.toMap("userLoginId", "system"));
        if (userLogin != null) {
            context.put("userLogin", userLogin);
        }

        String idNumPeriod = (String) context.get("idNumPeriod");
        String sequenceId = (String) context.get("sequenceId");
        context.remove("idNumPeriod");

        Map result = dispatcher.runSync("removeAcctgTransactionItem", context);

        if (idNumPeriod != null && sequenceId != null) {
            result = removeAcctgTransactionPeriod(dctx, sequenceId, idNumPeriod);
        }

        if (!ServiceUtil.isError(result)) {
            removeAcctgTransaction(dctx, sequenceId);
        }

        result = ServiceUtil.returnSuccess();
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     * @throws GenericServiceException 
     */
    public static Map updateTransaction(DispatchContext dctx, Map context) throws GenericEntityException, GenericServiceException {
        log.info("updateTransaction : ");

        GenericDelegator delegator = dctx.getDelegator();

        // Primary key
        String atranIdName = (String) context.get("atranIdName");
        log.info("atranIdName : " + atranIdName);
        String sequenceId = (String) context.get("sequenceId");

        // Args to update
        Timestamp entryDate = (Timestamp) context.get("entryDate");
        GenericValue value = delegator.findByPrimaryKey("AcctgTransactionItem", UtilMisc.toMap(
                "atranIdName", atranIdName,
                "sequenceId", sequenceId));

        if (value == null) {
            throw new GenericFindException("AcctgTransactionItem can't be found.");
        }

        value.set("entryDate", entryDate);

        // Description
        String description = (String) context.get("description");
        if (description != null) {
            value.set("description", description);
        }
        // Voucher
        String voucherRef = (String) context.get("voucherRef");
        if (voucherRef != null) {
            value.set("voucherRef", voucherRef);
        }
        // Journal
        String atrnstypIdName = (String) context.get("atrnstypIdName");
        if (atrnstypIdName != null) {
            value.set("atrnstypIdName", atrnstypIdName);
        }
        // Period
        String idNumPeriod = (String) context.get("idNumPeriod");
        String newIdNumPeriod = (String) context.get("newIdNumPeriod");

        if (idNumPeriod != null && newIdNumPeriod != null) {
            Map m;
            log.info("change period : " + idNumPeriod);
            m = removeAcctgTransactionPeriod(dctx, sequenceId, idNumPeriod);

            if (!ServiceUtil.isError(m)) {
                m = createAcctgTransactionPeriod(dctx, sequenceId, newIdNumPeriod);
            } else {
                throw new GenericFindException("AcctgTransactionItem can't be associated to new period.");
            }
        }
        value.store();

        Map<String, Object> result = ServiceUtil.returnSuccess("Service updateTransaction succeeded.");
        result.put("status", value.get("aTrStatusStatusId"));
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map addTransaction(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("addTransaction : ");

        GenericDelegator delegator = dctx.getDelegator();

        String idNumPeriod = (String) context.get("idNumPeriod");
        String atrnstypIdName = (String) context.get("atrnstypIdName");
        Timestamp entryDate = (Timestamp) context.get("entryDate");
        String sequenceId = AcctgTransactionItemBase.getNextSeqSequenceId(delegator);

        // Create AcctgTransaction always
        Map m = createAcctgTransaction(dctx, sequenceId, idNumPeriod);
        if (ServiceUtil.isError(m)) {
            throw new GenericCreateException("AcctgTransaction can't be created.");
        }

        GenericValue value = delegator.create("AcctgTransactionItem", UtilMisc.toMap(
                "atranIdName", sequenceId,
                "sequenceId", sequenceId,
                "atrnstypIdName", atrnstypIdName,
                "entryDate", entryDate));
        if (value == null) {
            throw new GenericCreateException("AcctgTransactionItem can't be created.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service addTransaction succeeded.");
        result.put("value", value);
        return result;
    }

    /**
     * 
     * @param dctx
     * @param atranIdName
     * @param idNumPeriod
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map createAcctgTransaction(DispatchContext dctx, String atranIdName, String idNumPeriod) throws GenericEntityException {
        log.info("createAcctgTransaction : " + atranIdName);

        GenericDelegator delegator = dctx.getDelegator();
        // AcctgTransaction
        GenericValue acctgTransaction = delegator.create("AcctgTransaction",
                UtilMisc.toMap("idName", atranIdName));
        if (acctgTransaction == null) {
            throw new GenericCreateException("AcctgTransaction can't be created.");
        }
        acctgTransaction.set("entryDate", new Timestamp(System.currentTimeMillis()));
        acctgTransaction.set("description", atranIdName + " generated");
        acctgTransaction.set("multiUserAutorized", "Y");
        acctgTransaction.set("aTrStatusStatusId", "ATS_WIP");
        acctgTransaction.set("multiUserAutorized", "Y");
        acctgTransaction.store();

        createAcctgTransactionPeriod(dctx, atranIdName, idNumPeriod);

        Map<String, Object> result = ServiceUtil.returnSuccess("Service createAcctgTransaction succeeded.");
        result.put("value", acctgTransaction);
        return result;
    }

    /**
     * 
     * @param dctx
     * @param atranIdName
     * @param idNumPeriod
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map createAcctgTransactionPeriod(DispatchContext dctx, String atranIdName, String idNumPeriod) throws GenericEntityException {
        log.info("createAcctgTransactionPeriod : " + atranIdName);

        GenericDelegator delegator = dctx.getDelegator();

        // AcctgTransactionPeriod
        GenericValue newAcctgTransactionPeriod = delegator.create("AcctgTransactionPeriod",
                UtilMisc.toMap(
                "atranIdName", atranIdName,
                "glPeriodIdNum", idNumPeriod));
        if (newAcctgTransactionPeriod == null) {
            throw new GenericCreateException("AcctgTransactionPeriod can't be created.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service createAcctgTransactionPeriod succeeded.");
        result.put("value", newAcctgTransactionPeriod);
        return result;
    }

    /**
     * 
     * @param dctx
     * @param atranIdName
     * @param idNumPeriod
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map removeAcctgTransactionPeriod(DispatchContext dctx, String atranIdName, String idNumPeriod) throws GenericEntityException {
        log.info("removeAcctgTransactionPeriod : " + atranIdName);

        GenericDelegator delegator = dctx.getDelegator();

        // AcctgTransactionPeriod
        GenericValue newAcctgTransactionPeriod = delegator.findByPrimaryKey("AcctgTransactionPeriod",
                UtilMisc.toMap(
                "atranIdName", atranIdName,
                "glPeriodIdNum", idNumPeriod));
        if (newAcctgTransactionPeriod == null) {
            log.info("AcctgTransactionPeriod can't be found.");
            Map<String, Object> result = ServiceUtil.returnError("AcctgTransactionPeriod can't be found.");
            return result;
        }
        newAcctgTransactionPeriod.remove();

        Map<String, Object> result = ServiceUtil.returnSuccess("Service removeAcctgTransactionPeriod succeeded.");
        return result;
    }

    /**
     * 
     * @param dctx
     * @param atranIdName
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     * @throws org.ofbiz.service.GenericServiceException
     */
    public static Map removeAcctgTransaction(DispatchContext dctx, String atranIdName) throws GenericEntityException, GenericServiceException {
        log.info("removeAcctgTransaction : " + atranIdName);

        LocalDispatcher dispatcher = dctx.getDispatcher();
        GenericDelegator delegator = dctx.getDelegator();

        GenericValue userLogin = delegator.findByPrimaryKey("UserLogin", UtilMisc.toMap("userLoginId", "system"));
        if (userLogin != null) {
            Map result = dispatcher.runSync("removeAcctgTransaction", UtilMisc.toMap(
                    "idName", atranIdName,
                    "userLogin", userLogin));
            if (ServiceUtil.isError(result)) {
                throw new GenericRemoveException("AcctgTransactionItem can't be removed : " + ServiceUtil.getErrorMessage(result));
            }
            return result;
        }
        Map<String, Object> result = ServiceUtil.returnSuccess("Service removeAcctgTransactionPeriod succeeded.");
        return result;
    }
}
