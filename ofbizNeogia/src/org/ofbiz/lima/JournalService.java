
package org.ofbiz.lima;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.GenericCreateException;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericFindException;
import org.ofbiz.entity.GenericRemoveException;
import org.ofbiz.entity.GenericStoreException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import java.util.List;
import java.util.Map;

/**
 *
 * @author ore
 */
public class JournalService {

    /** log */
    private static final Log log = LogFactory.getLog(JournalService.class);

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map getAllJournal(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("getAllJournal : ");

        GenericDelegator delegator = dctx.getDelegator();

        List<GenericValue> list = delegator.findAll("AcctgTransactionType");
        if (list == null) {
            throw new GenericFindException("AcctgTransactionType list is null.");
        }
        /** HACK */
        for (GenericValue value : list) {
            setPrefix(value);
        }
        /** */
        
        Map<String, Object> result = ServiceUtil.returnSuccess("Service getAllJournal succeeded.");
        result.put("list", list);
        return result;
    }
    
    /**
     * 
     * @param value
     */
    protected static void setPrefix(GenericValue value) {
        String aTrItPrefix = (String) value.get("aTrItPrefix");
        value.set("aentfrmEnumId", aTrItPrefix);
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     * @throws GenericServiceException 
     */
    public static Map removeJournal(DispatchContext dctx, Map context) throws GenericEntityException, GenericServiceException {

        log.info("removeJournal : ");

        LocalDispatcher dispatcher = dctx.getDispatcher();
        GenericDelegator delegator = dctx.getDelegator();

        GenericValue userLogin = delegator.findByPrimaryKey("UserLogin", UtilMisc.toMap("userLoginId", "system"));
        if (userLogin != null) {
            context.put("userLogin", userLogin);
        }
        Map result = dispatcher.runSync("removeAcctgTransactionType", context);

        if (ServiceUtil.isError(result)) {
            throw new GenericRemoveException("AcctgTransactionType can't be removed : " + ServiceUtil.getErrorMessage(result));
        }
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map addJournal(DispatchContext dctx, Map context) throws GenericEntityException {

        log.info("addJournal : ");


        GenericDelegator delegator = dctx.getDelegator();

        String idName = (String) context.get("idName");
        String description = (String) context.get("description");
        String prefix = (String) context.get("prefix");
        String aentfrmEnumId = "AEF_AUXILIARY_TR_IT";

        GenericValue value = delegator.create("AcctgTransactionType", UtilMisc.toMap(
                "idName", idName,
                "description", description,
                "aTrItPrefix", prefix,
                "aentfrmEnumId", aentfrmEnumId));

        if (value == null) {
            throw new GenericCreateException("AcctgTransactionType can't be created.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service addJournal succeeded.");
        return result;

    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map updateJournal(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("updateJournal : ");

        GenericDelegator delegator = dctx.getDelegator();

        String idName = (String) context.get("idName");
        String description = (String) context.get("description");
        String prefix = (String) context.get("prefix");

        GenericValue value = delegator.findByPrimaryKey("AcctgTransactionType", UtilMisc.toMap("idName", idName));

        if (value == null) {
            throw new GenericFindException("AcctgTransactionType can't be found.");
        }

        value.set("description", description);
        value.set("aTrItPrefix", prefix);

        int n = delegator.store(value);

        if (n <= 0) {
            throw new GenericStoreException("AcctgTransactionType can't be stored.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service updateJournal succeeded.");
        return result;
    }
}
