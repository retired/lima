
package org.ofbiz.lima;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ofbiz.accounting.transaction.developed.GlEntry;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.GenericCreateException;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericFindException;
import org.ofbiz.entity.GenericRemoveException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ore
 */
public class EntryService {

    /** log */
    private static final Log log = LogFactory.getLog(EntryService.class);

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map getEntryByTransaction(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("getEntryByTransaction : ");

        GenericDelegator delegator = dctx.getDelegator();

        String idName = (String) context.get("idName");
        String idSeq = (String) context.get("idSeq");

        List list = delegator.findByAnd("GlEntryAndGlEntryAccount", UtilMisc.toMap(
                "trItAtranIdName", idName,
                "trItSequenceId", idSeq));

        if (list == null) {
            throw new GenericFindException("GlEntryAndGlEntryAccount list is null.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service getEntryByTransaction succeeded.");
        result.put("list", list);
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     * @throws GenericServiceException 
     */
    public static Map removeEntry(DispatchContext dctx, Map context) throws GenericEntityException, GenericServiceException {
        log.info("removeEntry : ");

        LocalDispatcher dispatcher = dctx.getDispatcher();
        GenericDelegator delegator = dctx.getDelegator();

                // Primary key
        String trItAtranIdName = (String) context.get("trItAtranIdName");
        String trItSequenceId = (String) context.get("trItSequenceId");
        
        GenericValue userLogin = delegator.findByPrimaryKey("UserLogin", UtilMisc.toMap("userLoginId", "system"));
        if (userLogin != null) {
            context.put("userLogin", userLogin);
        }

        Map res = dispatcher.runSync("removeGlEntry", context);

        if (ServiceUtil.isError(res)) {
            throw new GenericRemoveException("GlEntry can't be removed : " + ServiceUtil.getErrorMessage(res));
        }

        /**
         * Find transaction parent for status
         */
        String status = getParentStatus(dctx, trItAtranIdName, trItSequenceId);
        Map<String, Object> result = ServiceUtil.returnSuccess("Service updateEntry succeeded.");
        result.put("status", status);
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map updateEntry(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("updateEntry : ");

        GenericDelegator delegator = dctx.getDelegator();

        // Primary key
        String trItAtranIdName = (String) context.get("trItAtranIdName");
        String trItSequenceId = (String) context.get("trItSequenceId");
        Long sequenceId = (Long) context.get("sequenceId");

        // Args to update
        Double amount = (Double) context.get("amount");
        String debitCreditEnumId = (String) context.get("debitCreditEnumId");

        GenericValue value = delegator.findByPrimaryKey("GlEntry", UtilMisc.toMap(
                "trItAtranIdName", trItAtranIdName,
                "trItSequenceId", trItSequenceId,
                "sequenceId", sequenceId));

        if (value == null) {
            throw new GenericFindException("GlEntry can't be found.");
        }

        value.set("amount", amount);
        value.set("debitCreditEnumId", debitCreditEnumId);
        // Description
        String description = (String) context.get("description");
        if (description != null) {
            value.set("description", description);
        }
        value.store();

        /**
         * Update account
         */
        String accountAccount = (String) context.get("accountAccount");
        if (accountAccount != null) {
            List<GenericValue> list = delegator.findByAnd("GlEntryAccount", UtilMisc.toMap(
                    "accountATypeEnumId", "AT_GL",
                    "entryTrItAtranIdName", trItAtranIdName,
                    "entryTrItSequenceId", trItSequenceId,
                    "entrySequenceId", sequenceId));
            if (list == null) {
                throw new GenericFindException("GlEntryAccount can't be found.");
            }
            // Removes old link
            for (GenericValue val : list) {
                val.remove();
            }
            // Creates new link
            GenericValue val = delegator.create("GlEntryAccount", UtilMisc.toMap(
                    "accountATypeEnumId", "AT_GL",
                    "accountAccount", accountAccount,
                    "entryTrItAtranIdName", trItAtranIdName,
                    "entryTrItSequenceId", trItSequenceId,
                    "entrySequenceId", sequenceId));

            if (val == null) {
                throw new GenericCreateException("GlEntryAccount can't be created.");
            }
            val.set("reconciled", "N");
            val.set("raReconcileId", "_NA_");
            val.store();
        }

        /**
         * Find transaction parent for status
         */
        String status = getParentStatus(dctx, trItAtranIdName, trItSequenceId);

        Map<String, Object> result = ServiceUtil.returnSuccess("Service updateEntry succeeded.");
        result.put("status", status);
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     * @throws org.ofbiz.service.GenericServiceException
     */
    public static Map addEntry(DispatchContext dctx, Map context) throws GenericEntityException, GenericServiceException {
        log.info("addEntry : ");

        GenericDelegator delegator = dctx.getDelegator();

        String trItAtranIdName = (String) context.get("trItAtranIdName");
        String trItSequenceId = (String) context.get("trItSequenceId");
        Long sequenceId = GlEntry.getNextSeqSequenceId(delegator);
        Double amount = (Double) context.get("amount");
        String voucherRef = (String) context.get("voucherRef");
        Timestamp voucherDate = (Timestamp) context.get("voucherDate");
        String debitCreditEnumId = (String) context.get("debitCreditEnumId");
        GenericValue value = delegator.create("GlEntry", UtilMisc.toMap(
                "trItAtranIdName", trItAtranIdName,
                "trItSequenceId", trItSequenceId,
                "sequenceId", sequenceId,
                "amount", amount));
        if (value == null) {
            throw new GenericCreateException("GlEntry can't be created.");
        }
        value.set("voucherRef", voucherRef);
        value.set("voucherDate", voucherDate);
        value.set("debitCreditEnumId", debitCreditEnumId);
        // Description
        String description = (String) context.get("description");
        if (description != null) {
            value.set("description", description);
        }
        value.store();

        // Account association
        String accountAccount = (String) context.get("accountAccount");
        if (accountAccount != null) {
            // Creates new link
            GenericValue val = delegator.create("GlEntryAccount", UtilMisc.toMap(
                    "accountATypeEnumId", "AT_GL",
                    "accountAccount", accountAccount,
                    "entryTrItAtranIdName", trItAtranIdName,
                    "entryTrItSequenceId", trItSequenceId,
                    "entrySequenceId", sequenceId));

            if (val == null) {
                throw new GenericCreateException("GlEntryAccount can't be created.");
            }
            val.set("reconciled", "N");
            val.set("raReconcileId", "_NA_");

            val.store();
        }

        /**
         * Find transaction parent for status
         */
        String status = getParentStatus(dctx, trItAtranIdName, trItSequenceId);

        Map<String, Object> result = ServiceUtil.returnSuccess("Service addEntry succeeded.");
        result.put("value", value);
        result.put("status", status);
        return result;
    }

    public static String getParentStatus(DispatchContext dctx, String trItAtranIdName, String trItSequenceId) throws GenericEntityException {
        GenericValue parent = dctx.getDelegator().findByPrimaryKey("AcctgTransactionItem", UtilMisc.toMap(
                "atranIdName", trItAtranIdName,
                "sequenceId", trItSequenceId));
        if (parent == null) {
            throw new GenericCreateException("AcctgTransactionItem parent can't be found.");
        }
        return (String) parent.get("aTrStatusStatusId");
    }
}
