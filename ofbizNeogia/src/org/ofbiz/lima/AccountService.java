
package org.ofbiz.lima;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.GenericCreateException;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericFindException;
import org.ofbiz.entity.GenericRemoveException;
import org.ofbiz.entity.GenericStoreException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.ServiceUtil;

import java.util.List;
import java.util.Map;

/**
 *
 * @author ore
 */
public class AccountService {

    /** log */
    private static final Log log = LogFactory.getLog(AccountService.class);

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map getAllAccount(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("getAllAccount : ");

        GenericDelegator delegator = dctx.getDelegator();

        List list = delegator.findAll("NGlAccount");

        if (list == null) {
            throw new GenericFindException("NGlAccount list can't be found.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service getAllAccount succeeded.");
        result.put("list", list);
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map addAccount(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("addAccount : ");


        GenericDelegator delegator = dctx.getDelegator();

        String idNumber = (String) context.get("idNumber");
        String description = (String) context.get("description");
        Long lowLevelCode = (Long) context.get("lowLevelCode");
        String treePath = (String) context.get("treePath");

        GenericValue value = delegator.create("NGlAccount", UtilMisc.toMap(
                "account", idNumber,
                "aTypeEnumId", "AT_GL"));

        if (value == null) {
            throw new GenericCreateException("NGlAccount can't be created.");
        }

        value.set("description", description);
        value.set("discriminator", "NGLACCOUNT");
        value.set("lowLevelCode", lowLevelCode);
        value.set("treePath", treePath);
        value.set("inputAccount", "Y");

        value.store();

        Map<String, Object> result = ServiceUtil.returnSuccess("Service addAccount succeeded.");
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map updateAccount(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("addAccount : ");

        GenericDelegator delegator = dctx.getDelegator();

        String idNumber = (String) context.get("idNumber");
        String description = (String) context.get("description");

        GenericValue value = delegator.findByPrimaryKey("NGlAccount", UtilMisc.toMap(
                "account", idNumber,
                "aTypeEnumId", "AT_GL"));

        if (value == null) {
            throw new GenericFindException("NGlAccount can't be found.");
        }

        value.set("description", description);

        int n = delegator.store(value);

        if (n <= 0) {
            throw new GenericStoreException("NGlAccount can't be stored.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service updateAccount succeeded.");
        return result;
    }

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     * @throws GenericServiceException 
     */
    public static Map removeAccount(DispatchContext dctx, Map context) throws GenericEntityException, GenericServiceException {
        log.info("removeAccount : ");

        GenericDelegator delegator = dctx.getDelegator();

        String account = (String) context.get("account");

        int n = delegator.removeByAnd("NGlAccount", UtilMisc.toMap(
                "account", account,
                "aTypeEnumId", "AT_GL"));

        if (n <= 0) {
            throw new GenericRemoveException("NGlAccount can't be removed.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service removeAccount succeeded.");
        return result;
    }
}
