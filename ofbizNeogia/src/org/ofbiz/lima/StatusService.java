
package org.ofbiz.lima;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericFindException;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;

import java.util.List;
import java.util.Map;

/**
 *
 * @author ore
 */
public class StatusService {

    /** log */
    private static final Log log = LogFactory.getLog(StatusService.class);

    /**
     * 
     * @param dctx
     * @param context
     * @return
     * @throws org.ofbiz.entity.GenericEntityException
     */
    public static Map getAllStatus(DispatchContext dctx, Map context) throws GenericEntityException {
        log.info("getAllStatusItem : ");

        GenericDelegator delegator = dctx.getDelegator();

        List exprs = UtilMisc.toList(
                new EntityExpr("statusTypeId", EntityOperator.EQUALS, "ATRSTATUS"),
                new EntityExpr("statusTypeId", EntityOperator.EQUALS, "PERIODSTATUS"));

        List list = delegator.findByOr("StatusItem", exprs);

        if (list == null) {
            throw new GenericFindException("StatusItem list is null.");
        }

        Map<String, Object> result = ServiceUtil.returnSuccess("Service getAllStatusItem succeeded.");
        result.put("list", list);
        return result;
    }
}
