<?xml version="1.0" encoding="UTF-8"?>
<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<project name="OFBiz - Lima Component" default="jar" basedir=".">

    <!-- ================================================================== -->
    <!-- Initialization of all property settings                            -->
    <!-- ================================================================== -->

    <target name="init">
        <property environment="env"/>
        <property name="desc" value="Lima Component"/>
        <property name="name" value="ofbiz-lima"/>
        <property name="ofbiz.home.dir" value="../.."/>
        <property name="src.dir" value="src"/>
        <property name="dtd.dir" value="dtd"/>
        <property name="lib.dir" value="lib"/>
        <property name="build.dir" value="build"/>
    </target>

    <target name="classpath">
        <path id="local.class.path">
            <fileset dir="../../framework/base/lib" includes="*.jar"/>
            <fileset dir="../../framework/base/lib/commons" includes="*.jar"/>
            <fileset dir="../../framework/base/lib/j2eespecs" includes="*.jar"/>
            <fileset dir="../../framework/base/build/lib" includes="*.jar"/>
            <fileset dir="../../framework/entity/lib" includes="*.jar"/>
            <fileset dir="../../framework/entity/build/lib" includes="*.jar"/>
            <fileset dir="../../framework/security/build/lib" includes="*.jar"/>
            <fileset dir="../../framework/service/lib" includes="*.jar"/>
            <fileset dir="../../framework/service/build/lib" includes="*.jar"/>
            <fileset dir="../../framework/minilang/build/lib" includes="*.jar"/>
            <fileset dir="../../framework/webapp/lib" includes="*.jar"/>
            <fileset dir="../../framework/webapp/build/lib" includes="*.jar"/>
            <fileset dir="../../framework/common/build/lib" includes="*.jar"/>
            <fileset dir="../party/build/lib" includes="*.jar"/>
            <fileset dir="../product/build/lib" includes="*.jar"/>
            <fileset dir="../marketing/build/lib" includes="*.jar"/>
            <fileset dir="../order/build/lib" includes="*.jar"/>
            <!-- <fileset dir="lib/worldpay" includes="*.jar"/> -->
            <!-- <fileset dir="lib/cybersource" includes="*.jar"/> -->
            <fileset dir="lib" includes="*.jar"/>
            <!-- Begin Neogia specific -->
            <fileset dir="../../neogia/integration/build/lib" includes="*.jar"/>
            <!-- End Neogia specific -->
        </path>
    </target>

    <!-- ================================================================== -->
    <!-- Removes all created files and directories                          -->
    <!-- ================================================================== -->

    <!-- Begin Neogia specific -->
    <!--
    <target name="clean" depends="clean-lib">
    -->
    <!--target name="clean" depends="clean-lib,clean-integration"-->
    <target name="clean" depends="clean-lib">
    <!-- End Neogia specific -->
        <delete dir="${build.dir}"/>
    </target>

    <target name="clean-lib" depends="init">
        <delete dir="${build.dir}/lib"/>
        <delete dir="${lib.dir}"/>
    </target>
     <!-- Begin Neogia specific -->    
    <!--target name="clean-integration" unless="do.not.clean.integration">
        <ant antfile="../../neogia/integration/build.xml" target="clean-integration" inheritall="false">
            <property name="package" value="org/ofbiz/lima"/>
        </ant>
    </target-->
     <!-- End Neogia specific -->

    <!-- ================================================================== -->
    <!-- Makes sure the needed directory structure is in place              -->
    <!-- ================================================================== -->

    <target name="prepare" depends="clean-lib">
        <mkdir dir="${build.dir}"/>
        <mkdir dir="${build.dir}/classes"/>
        <mkdir dir="${build.dir}/lib"/>
        <mkdir dir="${lib.dir}"/>
    </target>

    <target name="prepare-docs" depends="init">
        <mkdir dir="${build.dir}/javadocs"/>
    </target>

    <!-- ================================================================== -->
    <!-- Compilation of the source files                                                                                                                         -->
    <!-- ================================================================== -->

     <!-- Begin Neogia specific -->
     <!--
    <target name="classes" depends="prepare,classpath">
        <javac debug="on" source="1.4" deprecation="on" destdir="${build.dir}/classes">
     -->
    <!--target name="integrate" unless="do.not.do.integration"><ant antfile="../../neogia/integration/build.xml" inheritall="false"/></target-->
    <!--target name="classes" depends="prepare,classpath,integrate"-->
    <target name="classes" depends="prepare,classpath">
        <javac debug="on" source="1.5" deprecation="on" destdir="${build.dir}/classes">
     <!-- End Neogia specific -->
            <classpath>
                <path refid="local.class.path"/>
            </classpath>
            <src path="${src.dir}"/>
            <!-- exclude the payment processor packages; comment this out to not exclude if you have libs -->
            <exclude name="org/ofbiz/lima/thirdparty/verisign/**"/>
            <exclude name="org/ofbiz/lima/thirdparty/cybersource/**"/>
            <exclude name="org/ofbiz/lima/thirdparty/worldpay/**"/>
        </javac>
        <!-- also put the DTDs in the jar file... -->
        <copy todir="${build.dir}/classes">
            <!--<fileset dir="${dtd.dir}" includes="*.dtd"/>-->
            <fileset dir="${src.dir}" includes="**/*.properties,**/*.xml,**/*.bsh,**/*.logic,**/*.js,**/*.jacl,**/*.py"/>
        </copy>
        
        <!-- now add the NOTICE and LICENSE files to allow the jar file to be distributed alone -->
        <copy todir="${build.dir}/classes/META-INF">
            <fileset dir="${ofbiz.home.dir}" includes="NOTICE,LICENSE"/>
        </copy>        
    </target>

    <target name="jar" depends="classes">
        <jar jarfile="${build.dir}/lib/${name}.jar" basedir="${build.dir}/classes"/>
    </target>

    <!-- ================================================================== -->
    <!-- Build JavaDoc                                                      -->
    <!-- ================================================================== -->

    <target name="docs" depends="prepare-docs,classpath">
        <javadoc packagenames="org.ofbiz.lima.*"
                 classpathref="local.class.path"
                 destdir="${build.dir}/javadocs"
                 windowtitle="Open for Business - ${desc}">
            <sourcepath path="${src.dir}"/>
        </javadoc>
    </target>

    <target name="all" depends="jar,docs"/>
</project>
