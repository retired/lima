package org.chorem.lima.business.ejb.csv;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 03/06/14.
 */
public class EntryBookModel extends AbstractLimaModel<EntryBook> implements ExportModel<EntryBook> {

    public EntryBookModel() {
        super(';');
        newMandatoryColumn("code", EntryBook.PROPERTY_CODE);
        newOptionalColumn("label", EntryBook.PROPERTY_LABEL);
    }

    @Override
    public Iterable<ExportableColumn<EntryBook, Object>> getColumnsForExport() {
        ModelBuilder<EntryBook> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code", EntryBook.PROPERTY_CODE);
        modelBuilder.newColumnForExport("label", EntryBook.PROPERTY_LABEL);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public EntryBook newEmptyInstance() {
        return new EntryBookImpl();
    }
}
