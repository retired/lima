package org.chorem.lima.business.ejb.ebp;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.business.ejb.csv.AbstractLimaModel;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 01/08/14.
 */
public class AccountEBPModel extends AbstractLimaModel<Account> implements ExportModel<Account> {


    public AccountEBPModel() {
        super(',');
        newMandatoryColumn("Numero", Account.PROPERTY_ACCOUNT_NUMBER);
        newOptionalColumn("Intitule", Account.PROPERTY_LABEL);
        newIgnoredColumn("Type");
        newIgnoredColumn("bLetManuel");
        newIgnoredColumn("bLetAuto");
        newIgnoredColumn("bLetAppro");
        newIgnoredColumn("bCentPhysique");
        newIgnoredColumn("bCentJournal");
        newIgnoredColumn("bCentGLivre");
        newIgnoredColumn("bGeleDebit");
        newIgnoredColumn("bGeleCredit");
        newIgnoredColumn("bNoEchAna");
        newIgnoredColumn("TypeRel");
        newIgnoredColumn("Categorie");
        newIgnoredColumn("CodeTVA");
        newIgnoredColumn("TypeTVA");
        newIgnoredColumn("Lettrage");
        newIgnoredColumn("Encours");
        newIgnoredColumn("Seuil");
        newIgnoredColumn("Remise");
        newIgnoredColumn("Escompte");
        newIgnoredColumn("Devise");
        newIgnoredColumn("RIB0NomBanque");
        newIgnoredColumn("RIB0AdrBanque");
        newIgnoredColumn("RIB0Agence");
        newIgnoredColumn("RIB0Guichet");
        newIgnoredColumn("RIB0Compte");
        newIgnoredColumn("RIB0CleRIB");
        newIgnoredColumn("RIB1NomBanque");
        newIgnoredColumn("RIB1AdrBanque");
        newIgnoredColumn("RIB1Agence");
        newIgnoredColumn("RIB1Guichet");
        newIgnoredColumn("RIB1Compte");
        newIgnoredColumn("RIB1CleRIB");
        newIgnoredColumn("RIB2NomBanque");
        newIgnoredColumn("RIB2AdrBanque");
        newIgnoredColumn("RIB2Agence");
        newIgnoredColumn("RIB2Guichet");
        newIgnoredColumn("RIB2Compte");
        newIgnoredColumn("RIB2CleRIB");
        newIgnoredColumn("AdresseCivilite");
        newIgnoredColumn("AdresseRaiSoc");
        newIgnoredColumn("AdresseAdresse");
        newIgnoredColumn("AdresseCodePostal");
        newIgnoredColumn("AdresseVille");
        newIgnoredColumn("AdressePays");
        newIgnoredColumn("NII");
        newIgnoredColumn("Contact0Interloc");
        newIgnoredColumn("Contact0Fonction");
        newIgnoredColumn("Contact0TelNumTel");
        newIgnoredColumn("Contact0TelNumFax");
        newIgnoredColumn("Contact0Infos");
        newIgnoredColumn("Contact1Interloc");
        newIgnoredColumn("Contact1Fonction");
        newIgnoredColumn("Contact1TelNumTel");
        newIgnoredColumn("Contact1TelNumFax");
        newIgnoredColumn("Contact1Infos");
        newIgnoredColumn("Contact2Interloc");
        newIgnoredColumn("Contact2Fonction");
        newIgnoredColumn("Contact2TelNumTel");
        newIgnoredColumn("Contact2TelNumFax");
        newIgnoredColumn("Contact2Infos");
        newIgnoredColumn("Contact3Interloc");
        newIgnoredColumn("Contact3Fonction");
        newIgnoredColumn("Contact3TelNumTel");
        newIgnoredColumn("Contact3TelNumFax");
        newIgnoredColumn("Contact3Infos");
        newIgnoredColumn("Contact4Interloc");
        newIgnoredColumn("Contact4Fonction");
        newIgnoredColumn("Contact4TelNumTel");
        newIgnoredColumn("Contact4TelNumFax");
        newIgnoredColumn("Contact4Infos");
        newIgnoredColumn("CodeRegle");
        newIgnoredColumn("NbJours");
        newIgnoredColumn("TypeJours");
        newIgnoredColumn("Limite");
        newIgnoredColumn("Grille1");
        newIgnoredColumn("Grille2");
        newIgnoredColumn("Grille3");
        newIgnoredColumn("SectGeo");
        newIgnoredColumn("SectAct");
        newIgnoredColumn("ObjCA");
        newIgnoredColumn("TailleCA");
        newIgnoredColumn("NbEmpl");
        newIgnoredColumn("Commercial");
        newIgnoredColumn("JoursMois");
        newIgnoredColumn("CptTva");
        newIgnoredColumn("CptCharge");
        newIgnoredColumn("bDecouvertAutorise");
        newIgnoredColumn("MntDecouvertAutorise");
        newIgnoredColumn("EMailTiers");
        newIgnoredColumn("WebSiteTiers");
        newIgnoredColumn("NatAchat");
        newIgnoredColumn("NatVente");
        newIgnoredColumn("NatDepense");
        newIgnoredColumn("NatRecette");
        newIgnoredColumn("NumCptBqCH");
        newIgnoredColumn("NumClearing");
        newIgnoredColumn("NumIBAN");
        newIgnoredColumn("LetPart");
    }

    @Override
    public Iterable<ExportableColumn<Account, Object>> getColumnsForExport() {
        ModelBuilder<Account> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Numero", Account.PROPERTY_ACCOUNT_NUMBER);
        modelBuilder.newColumnForExport("Intitule", Account.PROPERTY_LABEL);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public Account newEmptyInstance() {
        return new AccountImpl();
    }
}
