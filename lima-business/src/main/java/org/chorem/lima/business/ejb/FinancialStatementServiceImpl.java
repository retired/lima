/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.FinancialStatementWayEnum;
import org.chorem.lima.beans.Amounts;
import org.chorem.lima.beans.AmountsImpl;
import org.chorem.lima.beans.FinancialStatementAmounts;
import org.chorem.lima.beans.FinancialStatementAmountsImpl;
import org.chorem.lima.beans.FinancialStatementDatas;
import org.chorem.lima.beans.FinancialStatementDatasImpl;
import org.chorem.lima.beans.ReportsDatas;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.FinancialStatementService;
import org.chorem.lima.business.api.ReportService;
import org.chorem.lima.business.exceptions.AlreadyExistFinancialStatementException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountTopiaDao;
import org.chorem.lima.entity.FinancialStatement;
import org.chorem.lima.entity.FinancialStatementTopiaDao;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

@Stateless
@Remote(FinancialStatementService.class)
@TransactionAttribute
public class FinancialStatementServiceImpl extends AbstractLimaService implements FinancialStatementService {

    @EJB
    private ReportService reportService;

    @EJB
    private AccountService accountService;

    protected static final Log log = LogFactory.getLog(FinancialStatementServiceImpl.class);

    protected static final Function<FinancialStatement, String> GET_LABEL = new Function<FinancialStatement, String>() {
        @Override
        public String apply(FinancialStatement input) {
            return input == null ? null : input.getLabel();
        }
    };

    protected void validateNewFinancialStatement(FinancialStatement masterFinancialStatement, FinancialStatement financialStatement) throws AlreadyExistFinancialStatementException, NotAllowedLabelException {
        if (financialStatement.getLabel().contains("/")) {
            throw new NotAllowedLabelException(financialStatement.getLabel());
        }
        if (masterFinancialStatement != null) {
            Collection<FinancialStatement> masterSubFinancialStatements = masterFinancialStatement.getSubFinancialStatements();
            if (masterSubFinancialStatements == null) {
                masterSubFinancialStatements = Lists.newArrayList();
            }
            Map indexedSubFinancialStatements = Maps.uniqueIndex(masterSubFinancialStatements, GET_LABEL);
            if (indexedSubFinancialStatements.get(financialStatement.getLabel()) != null){
                throw new AlreadyExistFinancialStatementException(financialStatement.getLabel(), masterFinancialStatement.getLabel());
            }
        }
    }

    @Override
    public FinancialStatement newFinancialStatement() {
        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();
        FinancialStatement result = financialStatementTopiaDao.newInstance();
        return result;
    }

    @Override
    public FinancialStatement createMasterFinacialStatements(FinancialStatement masterFinancialStatements) {
        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();
        FinancialStatement result = financialStatementTopiaDao.create(masterFinancialStatements);
        return result;
    }

    @Override
    public FinancialStatement createFinancialStatement(FinancialStatement parentFinancialStatement,
                                                        FinancialStatement financialStatement) throws AlreadyExistFinancialStatementException, NotAllowedLabelException {

        validateNewFinancialStatement(parentFinancialStatement, financialStatement);
        Preconditions.checkState(parentFinancialStatement.isPersisted(), "parents statement must be persited");
        Preconditions.checkState(!financialStatement.isPersisted(), " statement to create must be not persited");

        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();

        // refresh parent financialstatement
        parentFinancialStatement = financialStatementTopiaDao.forTopiaIdEquals(parentFinancialStatement.getTopiaId()).findUnique();

        financialStatement = financialStatementTopiaDao.create(financialStatement);

        parentFinancialStatement.addSubFinancialStatements(financialStatement);

        financialStatementTopiaDao.update(parentFinancialStatement);

        return financialStatement;
    }

    @Override
    public void removeFinancialStatement(FinancialStatement financialStatement) {

        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();

        // refresh financialstatement
        FinancialStatement financialStatementToDelete =
                financialStatementTopiaDao.forTopiaIdEquals(financialStatement.getTopiaId()).findUnique();

        financialStatementTopiaDao.delete(financialStatementToDelete);
    }

    @Override
    public void removeAllFinancialStatement() {

        FinancialStatementTopiaDao financialStatementDao = getDaoHelper().getFinancialStatementDao();

        List<FinancialStatement> allRoots = getRootFinancialStatements();

        financialStatementDao.deleteAll(allRoots);
    }

    @Override
    public List<FinancialStatement> getAllFinancialStatements() {
        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();
        List<FinancialStatement>  financialStatements = financialStatementTopiaDao.findAll();

        return financialStatements;
    }

    @Override
    public List<FinancialStatement> getRootFinancialStatements() {
        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();

        List<FinancialStatement> result = financialStatementTopiaDao
                .forMasterFinancialStatementEquals(null)
                .setOrderByArguments(FinancialStatement.PROPERTY_TOPIA_CREATE_DATE)
                .findAll();

        return result;
    }

    @Override
    public FinancialStatement updateFinancialStatement(FinancialStatement financialStatement) {

        // TopiaDao
        FinancialStatementTopiaDao financialStatementHeaderTopiaDao = getDaoHelper().getFinancialStatementDao();
        //update
        financialStatement = financialStatementHeaderTopiaDao.update(financialStatement);

        return financialStatement;
    }


    /**
     * remote methode to get list of financial statement
     */
    @Override
    public List<FinancialStatementAmounts> financialStatementReport(Date selectedBeginDate,
                                                                    Date selectedEndDate) {
        //create list form tree
        List<FinancialStatementAmounts> result = financialStatementReport(null, selectedBeginDate,
                                          selectedEndDate, new FinancialStatementDatasImpl()).getListResult();

        return result;
    }

    /**
     * Créé la liste de postes contenant les calculs de comptes
     *
     * @param financialStatement
     * @param selectedBeginDate
     * @param selectedEndDate
     * @param result
     * @return
     * @throws LimaException
     */
    protected FinancialStatementDatas financialStatementReport(FinancialStatement financialStatement,
                                                            Date selectedBeginDate,
                                                            Date selectedEndDate,
                                                            FinancialStatementDatas result) {

        Collection<FinancialStatement> financialStatements;

        if (financialStatement == null) {
            financialStatements = getRootFinancialStatements();
        } else {
            financialStatements = financialStatement.getSubFinancialStatements();
        }

        BigDecimal grossAmount = BigDecimal.ZERO,
        provisionDeprecationAmount = BigDecimal.ZERO;
        List<FinancialStatementAmounts> subResult = new ArrayList<>();

        for (FinancialStatement subFinancialStatement : financialStatements) {
            FinancialStatementAmounts financialStatementAmounts = financialStatementAmounts(subFinancialStatement, selectedBeginDate, selectedEndDate);
            if (!subFinancialStatement.isHeader()) {
                //on calcul
                grossAmount = grossAmount.add(financialStatementAmounts.getGrossAmount());

                provisionDeprecationAmount = provisionDeprecationAmount.add(
                        financialStatementAmounts.getProvisionDeprecationAmount());

                subResult.add(financialStatementAmounts);

            } else {
                FinancialStatementDatas financialStatementDatas =
                        financialStatementReport(subFinancialStatement,
                                                 selectedBeginDate, selectedEndDate, result);

                grossAmount = grossAmount.add(financialStatementDatas.
                        getFinancialStatementAmounts().getGrossAmount());

                provisionDeprecationAmount = provisionDeprecationAmount.
                        add(financialStatementDatas.getFinancialStatementAmounts().
                                getProvisionDeprecationAmount());

                FinancialStatementAmounts headerfinancialStatementAmounts =
                        financialStatementDatas.getFinancialStatementAmounts();

                //Si sous-total
                if (subFinancialStatement.isSubAmount()) {
                    FinancialStatementAmounts header = new FinancialStatementAmountsImpl();
                    header.setLabel(headerfinancialStatementAmounts.getLabel());
                    header.setLevel(headerfinancialStatementAmounts.getLevel());
                    header.setHeader(true);

                    //ajoute header
                    subResult.add(header);

                    //ajoute liste
                    if (financialStatementDatas.getListResult() != null) {
                        subResult.addAll(financialStatementDatas.getListResult());
                    }

                    //ajoute sstotal
                    FinancialStatementAmounts subAmount = new FinancialStatementAmountsImpl();
                    subAmount.setLabel("TOTAL " + headerfinancialStatementAmounts.getLabel());
                    subAmount.setLevel(headerfinancialStatementAmounts.getLevel());
                    subAmount.setGrossAmount(headerfinancialStatementAmounts.getGrossAmount());
                    subAmount.setProvisionDeprecationAmount(headerfinancialStatementAmounts.getProvisionDeprecationAmount());
                    subAmount.setSubAmount(true);
                    subResult.add(subAmount);
                    //ajoute une ligne vide
                    subResult.add(new FinancialStatementAmountsImpl());

                }//sinon
                else {

                    if (subFinancialStatement.isHeaderAmount()) {
                        subResult.add(headerfinancialStatementAmounts);
                    } else {
                        headerfinancialStatementAmounts.setGrossAmount(new BigDecimal(0));
                        headerfinancialStatementAmounts.setProvisionDeprecationAmount(new BigDecimal(0));
                        subResult.add(headerfinancialStatementAmounts);
                    }
                    //ajoute liste
                    if (financialStatementDatas.getListResult() != null) {
                        subResult.addAll(financialStatementDatas.getListResult());
                    }
                }
            }
        }
        FinancialStatementAmounts financialStatementAmounts = new FinancialStatementAmountsImpl();
        financialStatementAmounts.setGrossAmount(grossAmount);
        financialStatementAmounts.setProvisionDeprecationAmount(provisionDeprecationAmount);
        if (financialStatement != null) {
            financialStatementAmounts.setLabel(financialStatement.getLabel());
            financialStatementAmounts.setHeader(financialStatement.isHeader());
            financialStatementAmounts.setLevel(financialStatement.getLevel());
        }
        result.setFinancialStatementAmounts(financialStatementAmounts);
        result.setListResult(subResult);

        return result;
    }


    /**
     * Permet de calculer le montant de tous les comptes contenu dans un mouvement
     *
     * @param financialStatement
     * @param selectedBeginDate
     * @param selectedEndDate
     * @return
     * @throws LimaException
     */
    protected FinancialStatementAmounts financialStatementAmounts(FinancialStatement financialStatement,
                                                               Date selectedBeginDate,
                                                               Date selectedEndDate)  {
        FinancialStatementAmounts financialStatementAmounts =
                new FinancialStatementAmountsImpl();
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal debitAmount = BigDecimal.ZERO;
        BigDecimal creditAmount = BigDecimal.ZERO;
        BigDecimal provisionDeprecationAmount = BigDecimal.ZERO;


        FinancialStatement masterFinancialStatement = financialStatement.getMasterFinancialStatement();
        FinancialStatementWayEnum financialStatementWayEnum = masterFinancialStatement == null ? FinancialStatementWayEnum.BOTH : masterFinancialStatement.getWay();

        // DEBIT & CREDIT ACCOUNTS LIST
        Amounts amounts;
        String accountsString = financialStatement.getAccounts();
        if (accountsString != null && !accountsString.equals("")) {
            amounts = amountFromAccountList(accountsString, selectedBeginDate, selectedEndDate);
            debitAmount = debitAmount.add(amounts.getDebit());
            creditAmount = creditAmount.add(amounts.getCredit());
        }
        // DEBIT  ACCOUNTS LIST
        String debitAccountsString = financialStatement.getDebitAccounts();
        if (debitAccountsString != null && !debitAccountsString.equals("")) {
            amounts = amountFromAccountList(debitAccountsString, selectedBeginDate, selectedEndDate);
            debitAmount = debitAmount.add(amounts.getDebit());
        }
        //CREDIT ACCOUNTS LIST
        String creditAccountsString = financialStatement.getCreditAccounts();
        if (creditAccountsString != null && !creditAccountsString.equals("")) {
            amounts = amountFromAccountList(creditAccountsString, selectedBeginDate, selectedEndDate);
            creditAmount = creditAmount.add(amounts.getCredit());
        }
        // PROVISION & DEPRECATION
        String provisionDeprecationAccountsString = financialStatement.getProvisionDeprecationAccounts();
        if (provisionDeprecationAccountsString != null && !provisionDeprecationAccountsString.equals("")) {
            amounts = amountFromAccountList(provisionDeprecationAccountsString, selectedBeginDate, selectedEndDate);
            provisionDeprecationAmount = provisionDeprecationAmount.add(amounts.getCredit());
            provisionDeprecationAmount = provisionDeprecationAmount.subtract(amounts.getDebit());
        }
        amount = computeAmount(amount, debitAmount, creditAmount, financialStatementWayEnum);

        financialStatementAmounts.setGrossAmount(amount);
        financialStatementAmounts.setLabel(financialStatement.getLabel());
        financialStatementAmounts.setProvisionDeprecationAmount(provisionDeprecationAmount);
        financialStatementAmounts.setLevel(financialStatement.getLevel());

        return financialStatementAmounts;
    }

    private BigDecimal computeAmount(BigDecimal amount, BigDecimal debitAmount, BigDecimal creditAmount, FinancialStatementWayEnum financialStatementWayEnum) {
        switch (financialStatementWayEnum) {
            case BOTH:
                amount = amount.add(debitAmount);
                amount = amount.subtract(creditAmount);
                amount = amount.abs();
                break;
            case CREDIT:
                amount = amount.add(creditAmount);
                amount = amount.subtract(debitAmount);
                break;
            case DEBIT:
                amount = amount.add(debitAmount);
                amount = amount.subtract(creditAmount);
        }
        return amount;
    }

    protected Amounts amountFromAccountList(String accountsNumberList,
                                         Date selectedBeginDate,
                                         Date selectedEndDate) {
        Amounts amounts = new AmountsImpl();
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal credit = BigDecimal.ZERO;
        Boolean substract = false;

        //Remove Spaces
        String result = StringUtils.deleteWhitespace(accountsNumberList);
        StringTokenizer stQuote = new StringTokenizer(result, "-");
        while (stQuote.hasMoreTokens()) {
            String s = stQuote.nextToken();
            List<Account> accountsList = accountService.stringToListAccounts(s);
            BigDecimal debitTemp = BigDecimal.ZERO;
            BigDecimal creditTemp = BigDecimal.ZERO;
            for (Account account : accountsList) {
                ReportsDatas reportsDatas =
                        reportService.generateAccountsReports(
                                account,
                                true,
                                selectedBeginDate,
                                selectedEndDate);
                if (reportsDatas.isSoldeDebit()) {
                    debitTemp = debitTemp.add(reportsDatas.getAmountSolde());
                } else {
                    creditTemp = creditTemp.add(reportsDatas.getAmountSolde());
                }
            }
            if (!substract) {
                debit = debitTemp;
                credit = creditTemp;
            }
            //compte(s) précédé du signe -
            else {
                debit = debit.subtract(debitTemp);
                credit = credit.subtract(creditTemp);
            }
            substract = true;
        }
        amounts.setCredit(credit);
        amounts.setDebit(debit);
        return amounts;
    }

    @Override
    public List<Account> checkFinancialStatementChart() {

        AccountTopiaDao accountTopiaDao = getDaoHelper().getAccountDao();
        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();

        List<Account> accountsList = accountTopiaDao.findAllLeafAccounts();

        List<FinancialStatement> financialStatementsList = financialStatementTopiaDao.findAll();

        for (FinancialStatement financialStatement : financialStatementsList) {

            accountsList.removeAll(accountService.stringToListAccounts(financialStatement.getAccounts()));

            accountsList.removeAll(accountService.stringToListAccounts(financialStatement.getCreditAccounts()));

            accountsList.removeAll(accountService.stringToListAccounts(financialStatement.getDebitAccounts()));

            accountsList.removeAll(accountService.stringToListAccounts(financialStatement.getProvisionDeprecationAccounts()));
        }

        return accountsList;
    }

    @Override
    public boolean checkFinancialStatementExist(String label) {

        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();
        boolean result = financialStatementTopiaDao.forProperties(FinancialStatement.PROPERTY_LABEL, label).exists();

        return result;
    }

    @Override
    public FinancialStatement getFinancialStatementByLabel(String label) {
        FinancialStatementTopiaDao financialStatementTopiaDao = getDaoHelper().getFinancialStatementDao();
        FinancialStatement result = financialStatementTopiaDao.forLabelEquals(label).findUniqueOrNull();

        return result;
    }
}
