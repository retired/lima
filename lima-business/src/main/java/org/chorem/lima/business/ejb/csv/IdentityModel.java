package org.chorem.lima.business.ejb.csv;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.IdentityImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 03/06/14.
 */
public class IdentityModel extends AbstractLimaModel<Identity> implements ExportModel<Identity> {

    public IdentityModel() {
        super(';');
        newOptionalColumn("name", Identity.PROPERTY_NAME);
        newOptionalColumn("description", Identity.PROPERTY_DESCRIPTION);
        newOptionalColumn("address", Identity.PROPERTY_ADDRESS);
        newOptionalColumn("address2", Identity.PROPERTY_ADDRESS2);
        newOptionalColumn("city", Identity.PROPERTY_CITY);
        newOptionalColumn("phoneNumber", Identity.PROPERTY_PHONE_NUMBER);
        newOptionalColumn("email", Identity.PROPERTY_EMAIL);
        newOptionalColumn("zipCode", Identity.PROPERTY_ZIP_CODE);
        //newOptionalColumn("vatNumber", Identity.PROPERTY_VAT_NUMBER);
        newOptionalColumn("classificationCode", Identity.PROPERTY_CLASSIFICATION_CODE);
        newOptionalColumn("buisinessNumber", Identity.PROPERTY_BUSINESS_NUMBER);
    }

    @Override
    public Iterable<ExportableColumn<Identity, Object>> getColumnsForExport() {
        ModelBuilder<Identity> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("name", Identity.PROPERTY_NAME);
        modelBuilder.newColumnForExport("description", Identity.PROPERTY_DESCRIPTION);
        modelBuilder.newColumnForExport("address", Identity.PROPERTY_ADDRESS);
        modelBuilder.newColumnForExport("address2", Identity.PROPERTY_ADDRESS2);
        modelBuilder.newColumnForExport("city", Identity.PROPERTY_CITY);
        modelBuilder.newColumnForExport("phoneNumber", Identity.PROPERTY_PHONE_NUMBER);
        modelBuilder.newColumnForExport("email", Identity.PROPERTY_EMAIL);
        modelBuilder.newColumnForExport("zipCode", Identity.PROPERTY_ZIP_CODE);
        //modelBuilder.newColumnForExport("vatNumber", Identity.PROPERTY_VAT_NUMBER);
        modelBuilder.newColumnForExport("classificationCode", Identity.PROPERTY_CLASSIFICATION_CODE);
        modelBuilder.newColumnForExport("buisinessNumber", Identity.PROPERTY_BUSINESS_NUMBER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public Identity newEmptyInstance() {
        return new IdentityImpl();
    }
}
