/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.accountingrules;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.AccountingRules;
import org.chorem.lima.business.LimaInterceptor;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LastUnlockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoEmptyFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotLockedClosedPeriodicEntryBooksException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnbalancedEntriesException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.UsedAccountException;
import org.chorem.lima.business.exceptions.UsedEntryBookException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.ClosedPeriodicEntryBookTopiaDao;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryTopiaDao;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialPeriodTopiaDao;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionTopiaDao;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodTopiaDao;
import org.chorem.lima.entity.LimaCallaoTopiaDaoSupplier;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Defaults rules, if no localized rules classes is instantiated
 * this default class contain the strict minimum rules to check the data integrity.
 * 
 * Rule specialization should override this class.
 *
 * @author jpepin
 */
public class DefaultAccountingRules implements AccountingRules {

    protected static final Log log =
            LogFactory.getLog(DefaultAccountingRules.class);

    /**
     * Return Dao helper to use in current thread.
     * Defined by {@link LimaInterceptor}.
     * 
     * @return api helper
     */
    protected LimaCallaoTopiaDaoSupplier getDaoHelper() {
        return LimaInterceptor.DAO_HELPER.get();
    }

    /**
     * Rules to check before create accounts.
     */
    @Override
    public void createAccountRules(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException {
        // Check if the numberaccount is not blank
        if (StringUtils.isBlank(account.getAccountNumber()) || account.getAccountNumber().contains("\\")) {
            throw new InvalidAccountNumberException(account.getAccountNumber());
        }

    }

    @Override
    public void updateAccountRules(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException {
        // Check if the numberaccount is not blank
        if (StringUtils.isBlank(account.getAccountNumber())) {
            throw new InvalidAccountNumberException(account.getAccountNumber());
        }
    }

    /** Rules to check before create fiscals periods */
    @Override
    public List<FinancialPeriod> createFiscalPeriodRules(FiscalPeriod fiscalPeriod)
            throws BeginAfterEndFiscalPeriodException,
            NotBeginNextDayOfLastFiscalPeriodException,
            MoreOneUnlockFiscalPeriodException {
        //check if the enddate period is after the begindate period
        if (fiscalPeriod.getEndDate().before(fiscalPeriod.getBeginDate())) {
            throw new BeginAfterEndFiscalPeriodException(fiscalPeriod);
        }
        return null;
    }

    /** Rules to check before block fiscals periods */
    @Override
    public void blockFiscalPeriodRules(FiscalPeriod fiscalPeriod)
            throws LastUnlockedFiscalPeriodException, UnbalancedFinancialTransactionsException,
            UnfilledEntriesException, WithoutEntryBookFinancialTransactionsException {

        FinancialTransactionTopiaDao financialTransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        EntryTopiaDao entryDao = getDaoHelper().getEntryDao();
        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();

        // reload object in current transaction
        fiscalPeriod = fiscalPeriodTopiaDao.forTopiaIdEquals(fiscalPeriod.getTopiaId()).findUnique();

        // Check if all financial transactions of this fiscal period are equilibrate
        List<FinancialTransaction> allUnbalancedTransaction = financialTransactionTopiaDao.getAllUnbalancedTransaction(
                fiscalPeriod.getBeginDate(),
                fiscalPeriod.getEndDate(),
                null);

        if (!allUnbalancedTransaction.isEmpty()) {
            throw new UnbalancedFinancialTransactionsException(allUnbalancedTransaction);
        }

        // Check if all financial transactions of this  fiscal period are well filled in
        List<Entry> allUnfilledEntry = entryDao.findAllUnfilledEntry(
                fiscalPeriod.getBeginDate(),
                fiscalPeriod.getEndDate());
        if (!allUnfilledEntry.isEmpty()) {
            throw new UnfilledEntriesException(allUnfilledEntry);
        }

        // Check if all financial transactions have EntryBooks
        List<FinancialTransaction> allTransactionWithoutEntryBook = financialTransactionTopiaDao.findAllTransactionWithoutEntryBook(
                fiscalPeriod.getBeginDate(),
                fiscalPeriod.getEndDate());
        if (!allTransactionWithoutEntryBook.isEmpty()) {
            throw new WithoutEntryBookFinancialTransactionsException(allTransactionWithoutEntryBook);
        }



    }

    /** Rules to check before delete fiscals periods */
    @Override
    public void deleteFiscalPeriodRules(FiscalPeriod fiscalPeriod)
            throws NoEmptyFiscalPeriodException {
    }

    /**
     * Rules to check if an account and all his subaccount is not used
     * Recursive function
     */
    @Override
    public void removeAccountRules(Account account) throws UsedAccountException {

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        // Check if account have entries
        if (entryTopiaDao.forAccountEquals(account).exists()) {
            throw new UsedAccountException(account.getAccountNumber());
        }
    }

    /** Check if entrybook have financial transaction */
    @Override
    public void removeEntryBookRules(EntryBook entryBook) throws UsedEntryBookException {
        FinancialTransactionTopiaDao financialTransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();

        // Check if entrybook have entries
        long nbfinancialtransaction = financialTransactionTopiaDao.getCountByEntryBook(entryBook);
        if (nbfinancialtransaction != 0) {
            throw new UsedEntryBookException(entryBook);
        }

    }

    /**
     * Check if all financial transactions of closedperiodicentrybook are equilibrate
     * Check if all financial transactions of this closedperiodicentrybook/financialPeriod are well filled in
     * Check if all financial transactions have EntryBooks
     */
    @Override
    public void blockClosedPeriodicEntryBookRules(ClosedPeriodicEntryBook closedPeriodicEntryBook)
            throws UnbalancedFinancialTransactionsException, UnfilledEntriesException, WithoutEntryBookFinancialTransactionsException, NotLockedClosedPeriodicEntryBooksException {

        FinancialTransactionTopiaDao financialTransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        EntryTopiaDao entryDao = getDaoHelper().getEntryDao();
        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao = getDaoHelper().getClosedPeriodicEntryBookDao();
        // reload object in current transaction
        closedPeriodicEntryBook = closedPeriodicEntryBookTopiaDao.forTopiaIdEquals(closedPeriodicEntryBook.getTopiaId()).findUnique();

        // Check if all financial transactions of closedperiodicentrybook are equilibrate
        FinancialPeriod period = closedPeriodicEntryBook.getFinancialPeriod();
        List<FinancialTransaction> allUnbalancedTransaction = financialTransactionTopiaDao.getAllUnbalancedTransaction(
                period.getBeginDate(),
                period.getEndDate(),
                closedPeriodicEntryBook.getEntryBook());
        if (!allUnbalancedTransaction.isEmpty()) {
            throw new UnbalancedFinancialTransactionsException(allUnbalancedTransaction);
        }

        // Check if all financial transactions of this closedperiodicentrybook/financialPeriod are well filled in
        List<Entry> allUnfilledEntry = entryDao.findAllUnfilledEntry(
                period.getBeginDate(),
                period.getEndDate(),
                closedPeriodicEntryBook.getEntryBook());
        if (!allUnfilledEntry.isEmpty()) {
            throw new UnfilledEntriesException(allUnfilledEntry);
        }

        // Check if all financial transactions have EntryBooks
        List<FinancialTransaction> allTransactionWithoutEntryBook = financialTransactionTopiaDao.findAllTransactionWithoutEntryBook(
                period.getBeginDate(),
                period.getEndDate());
        if (!allTransactionWithoutEntryBook.isEmpty()) {
            throw new WithoutEntryBookFinancialTransactionsException(allTransactionWithoutEntryBook);
        }
    }

    @Override
    public void addLetter(List<Entry> oldEntries) throws LockedEntryBookException, UnbalancedEntriesException {

        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao = getDaoHelper().getClosedPeriodicEntryBookDao();
        FinancialPeriodTopiaDao financialPeriodTopiaDao = getDaoHelper().getFinancialPeriodDao();

        BigDecimal balance = BigDecimal.ZERO;

        for (Entry entry : oldEntries) {

            FinancialPeriod financialPeriod = financialPeriodTopiaDao.findByDate(entry.getFinancialTransaction().getTransactionDate());

            ClosedPeriodicEntryBook closedPeriodicEntryBook =
                    closedPeriodicEntryBookTopiaDao.findByEntryBookAndFinancialPeriod(
                            entry.getFinancialTransaction().getEntryBook(), financialPeriod);
            if (closedPeriodicEntryBook.isLocked()) {
                throw new LockedEntryBookException(closedPeriodicEntryBook);
            }

            if (entry.isDebit()) {
                balance = balance.add(entry.getAmount());
            } else {
                balance = balance.subtract(entry.getAmount());
            }

        }

        if (balance.signum() != 0) {
            throw new UnbalancedEntriesException(oldEntries);
        }

    }

    @Override
    public void createFinancialTransactionRules(FinancialTransaction financialTransaction)
            throws BeforeFirstFiscalPeriodException, AfterLastFiscalPeriodException,
            LockedFinancialPeriodException, LockedEntryBookException {
        checkTransactionDate(financialTransaction);
        checkTransactionBlock(financialTransaction);

    }

    @Override
    public void updateFinancialTransactionRules(FinancialTransaction financialTransaction,
                                                FinancialTransaction financialTransactionOld)
            throws BeforeFirstFiscalPeriodException, AfterLastFiscalPeriodException,
            LockedFinancialPeriodException, LockedEntryBookException {
        checkTransactionBlock(financialTransactionOld);
        checkTransactionDate(financialTransaction);
        checkTransactionBlock(financialTransaction);

    }

    @Override
    public void deleteFinancialTransactionRules(FinancialTransaction financialTransaction)
            throws LockedFinancialPeriodException, LockedEntryBookException {
        checkTransactionBlock(financialTransaction);

    }

    protected void checkTransactionDate(FinancialTransaction financialTransaction)
            throws BeforeFirstFiscalPeriodException, AfterLastFiscalPeriodException {

        Date financialTransactionDate = financialTransaction.getTransactionDate();
        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();
        FiscalPeriod lastFiscalPeriod = fiscalPeriodTopiaDao.getLastFiscalPeriod();
        FiscalPeriod firstFiscalPeriod = fiscalPeriodTopiaDao.getFirstFiscalPeriod();
        Date beginDateFirstFiscalPeriod = firstFiscalPeriod.getBeginDate();
        Date endDateLastFiscalPeriod = lastFiscalPeriod.getEndDate();

        //check if date are on fiscal period (test if date is after begindate of first f.p. and before enddate of last f.p.)

        if (log.isDebugEnabled()) {
            log.debug("Date de transaction modifiée sur l'ui " + financialTransactionDate);
            log.debug("Date de début du premier exercice : " + beginDateFirstFiscalPeriod);
            log.debug("Date de fin du dernier exercice : " + endDateLastFiscalPeriod);
        }

        //1-date not on a fiscal period
        if (financialTransactionDate.before(beginDateFirstFiscalPeriod)) {
            if (log.isDebugEnabled()) {
                log.debug("Date de la transaction avant (antérieure) la date de début du premier exercice");
            }
            throw new BeforeFirstFiscalPeriodException(beginDateFirstFiscalPeriod);

        }

        if (financialTransactionDate.after(endDateLastFiscalPeriod)) {
            if (log.isDebugEnabled()) {
                log.debug("Date de la transaction après (postérieure) la date de fin du dernier exercice");
            }
            throw new AfterLastFiscalPeriodException(endDateLastFiscalPeriod);

        }
    }

    protected void checkTransactionBlock(FinancialTransaction financialTransaction) throws LockedFinancialPeriodException, LockedEntryBookException {
        FinancialPeriodTopiaDao financialPeriodTopiaDao = getDaoHelper().getFinancialPeriodDao();
        FinancialPeriod financialPeriod = financialPeriodTopiaDao.findByDate(financialTransaction.getTransactionDate());

        if (financialPeriod.isLocked()) {
            if (log.isDebugEnabled()) {
                log.debug("Periode financière bloquée");
            }
            throw new LockedFinancialPeriodException(financialPeriod);
        }

        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao = getDaoHelper().getClosedPeriodicEntryBookDao();


        ClosedPeriodicEntryBook closedPeriodicEntryBook =
                closedPeriodicEntryBookTopiaDao.findByEntryBookAndFinancialPeriod(
                        financialTransaction.getEntryBook(), financialPeriod);
        if (closedPeriodicEntryBook.isLocked()) {
            if (log.isDebugEnabled()) {
                log.debug("Journal bloquée");
            }
            throw new LockedEntryBookException(closedPeriodicEntryBook);
        }

    }

    @Override
    public void createEntriesRules(Collection<Entry> entries) throws LockedFinancialPeriodException, LockedEntryBookException {
        Set<FinancialTransaction> transactions = new HashSet<>();
        for (Entry entry : entries) {
            transactions.add(entry.getFinancialTransaction());
        }
        for (FinancialTransaction transaction : transactions) {
            checkTransactionBlock(transaction);
        }
    }

    @Override
    public void createEntryRules(Entry entry) throws LockedFinancialPeriodException, LockedEntryBookException {
        checkTransactionBlock(entry.getFinancialTransaction());

    }

    @Override
    public void updateEntryRules(Entry entry, Entry entryOld) throws LockedFinancialPeriodException, LockedEntryBookException {
        checkTransactionBlock(entry.getFinancialTransaction());
    }

    @Override
    public void deleteEntryRules(Entry entry) throws LockedFinancialPeriodException, LockedEntryBookException {
        checkTransactionBlock(entry.getFinancialTransaction());
    }



    
}
