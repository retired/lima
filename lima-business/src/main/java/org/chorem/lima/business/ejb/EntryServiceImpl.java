package org.chorem.lima.business.ejb;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.business.api.EntryService;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryTopiaDao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.util.Date;
import java.util.List;

/**
 * Created by davidcosse on 17/01/14.
 */
@Stateless
@Remote(EntryService.class)
@TransactionAttribute
public class EntryServiceImpl extends AbstractLimaService implements EntryService {

    @Override
    public Entry createEntry(Entry entry) {

        EntryTopiaDao entryDao = getDaoHelper().getEntryDao();
        Entry result = entryDao.create(entry);

        return result;
    }

    @Override
    public List<Object[]> findDebitCreditOfTransaction(ClosedPeriodicEntryBook closedPeriodicEntryBook) {
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        List<Object[]> result = entryTopiaDao.getDebitCreditOfTransaction(closedPeriodicEntryBook.getEntryBook(),
                closedPeriodicEntryBook.getFinancialPeriod().getBeginDate(),
                closedPeriodicEntryBook.getFinancialPeriod().getEndDate());
        return result;
    }

    @Override
    public List<Entry> findAllEntriesByDatesForEntryBook(EntryBook entryBook, Date beginDate, Date endDate) {
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        List<Entry> result = entryTopiaDao.getAllEntriesByDatesForEntryBook(entryBook,
                beginDate,
                endDate);
        return result;
    }

    @Override
    public void saveAllEntries(List<Entry> entries) {
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        entryTopiaDao.createAll(entries);
    }

}
