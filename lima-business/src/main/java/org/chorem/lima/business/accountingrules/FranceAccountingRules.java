/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.accountingrules;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LastUnlockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoEmptyFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotLockedClosedPeriodicEntryBooksException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.ClosedPeriodicEntryBookTopiaDao;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialPeriodImpl;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionTopiaDao;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodTopiaDao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Surcharge des regles par defaut pour application à la comptabilité française.
 * 
 * @author echatellier
 */
public class FranceAccountingRules extends DefaultAccountingRules {

    protected static final Log log = LogFactory.getLog(FranceAccountingRules.class);

    /**
     * Règles de vérification de la création du PCG, appliquées à la comptabilité française.
     */
    @Override
    public void createAccountRules(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException {

        super.createAccountRules(account);
        validFranceAccountingRules(account);
    }

    @Override
    public void updateAccountRules(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException {

        super.updateAccountRules(account);
        validFranceAccountingRules(account);
    }

    protected void validFranceAccountingRules(Account account) throws NotNumberAccountNumberException, InvalidAccountNumberException {
        String accountNumber = account.getAccountNumber();

        // ledger account must be located in 411 account
        if (!StringUtils.isNumeric(accountNumber) && !accountNumber.startsWith("4")) {
            throw new NotNumberAccountNumberException(account.getAccountNumber(), account.getAccountNumber());
        }

        // Check root account starts with 1 to 8
        if (accountNumber.length() >= 1 && !accountNumber.substring(0, 1).matches("[1-8]")) {
            throw new InvalidAccountNumberException(account.getAccountNumber(), account.getAccountNumber());
        }
    }

    /**
     * Règles de vérification d'ouverture d'un exercice, appliquées à la comptabilité française
     * <p/>
     * Permet de créer un exercice.
     * <p/>
     * Elle a une durée de un an, composées de 12 périodes mensuelles.
     * Elle peut aussi être plus courte ou plus longue si l'entreprise
     * se constitu ou entre en liquidation ou que l'entreprise decide
     * de changer
     * <p/>
     * Elle correspond à l'exercice comptable.
     * Pas plus de deux exercices fiscaux ne peuvent-être ouvert en même temps
     */
    @Override
    public List<FinancialPeriod> createFiscalPeriodRules(FiscalPeriod fiscalPeriod)
            throws NotBeginNextDayOfLastFiscalPeriodException,
            MoreOneUnlockFiscalPeriodException,
            BeginAfterEndFiscalPeriodException {

        super.createFiscalPeriodRules(fiscalPeriod);
        List<FinancialPeriod> financialPeriods = new ArrayList<>();

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();

        // Checks if is not the first fiscal period to create
        if (fiscalPeriodTopiaDao.count() != 0) {

            FiscalPeriod lastFiscalPeriod = fiscalPeriodTopiaDao.getLastFiscalPeriod();

            //check the new fiscal period adjoining the last
            Date dateLastFiscalPeriod = lastFiscalPeriod.getEndDate();
            Date newFiscalPeriodStartingDate = DateUtils.addDays(dateLastFiscalPeriod, 1);
            newFiscalPeriodStartingDate = DateUtils.truncate(newFiscalPeriodStartingDate, Calendar.DATE);

            if (newFiscalPeriodStartingDate.compareTo(fiscalPeriod.getBeginDate()) != 0) {
                throw new NotBeginNextDayOfLastFiscalPeriodException(lastFiscalPeriod);
            }

            // No more than one unlocked fiscal period is allowed
            long unblockedFiscalPeriod = fiscalPeriodTopiaDao.forLockedEquals(false).count();
            if (unblockedFiscalPeriod > 1L) {
                throw new MoreOneUnlockFiscalPeriodException(fiscalPeriod.getBeginDate(), fiscalPeriod.getEndDate(), fiscalPeriod.isLocked(), unblockedFiscalPeriod);
            }
        }

        // FinancialPeriods of 1 month are created
        Date endDate = fiscalPeriod.getEndDate();
        Date loopDate = fiscalPeriod.getBeginDate();
        while (loopDate.before(endDate)) {
            FinancialPeriod financialPeriod = new FinancialPeriodImpl();
            //important for fiscalperiod created from import, it can be locked, so financialperiods must be locked
            financialPeriod.setLocked(fiscalPeriod.isLocked());
            financialPeriod.setBeginDate(loopDate);
            loopDate = DateUtils.addMonths(loopDate, 1);
            loopDate = DateUtils.truncate(loopDate, Calendar.MONTH);
            loopDate = DateUtils.addMilliseconds(loopDate, -1);
            if (loopDate.after(endDate)) {
                financialPeriod.setEndDate(endDate);
            } else {
                financialPeriod.setEndDate(loopDate);
            }
            //create it
            financialPeriods.add(financialPeriod);

            //loop incremente
            loopDate = DateUtils.addMilliseconds(loopDate, 1);
        }
        return financialPeriods;
    }

    /**
     * Règles de vérification de fermeture d'un exercice, appliquées à la comptabilité française.
     * 
     * On ne peut pas clore la dernière periode comptable ouverte.
     */
    @Override
    public void blockFiscalPeriodRules(FiscalPeriod fiscalPeriod)
            throws LastUnlockedFiscalPeriodException, UnbalancedFinancialTransactionsException,
            WithoutEntryBookFinancialTransactionsException, UnfilledEntriesException {

        super.blockFiscalPeriodRules(fiscalPeriod);

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();

        FiscalPeriod lastUnlockedFiscalPeriod =
                fiscalPeriodTopiaDao.getLastUnlockedFiscalPeriod();

        //Check if the fiscal period to block is the oldest
        if (lastUnlockedFiscalPeriod.equals(fiscalPeriod)) {
            throw new LastUnlockedFiscalPeriodException(fiscalPeriod);
        }
    }

    /**
     * Règles de vérification de supression d'un exercice, appliquées à la comptabilité française.
     * 
     * On ne peut supprimer qu'une periode comptable ouverte vide.
     */
    @Override
    public void deleteFiscalPeriodRules(FiscalPeriod fiscalPeriod) throws NoEmptyFiscalPeriodException {

        FinancialTransactionTopiaDao financialTransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();

        //Check if the fiscal period to delete is empty
        List<FinancialTransaction> financialTransactions = financialTransactionTopiaDao.findAllByDates(fiscalPeriod.getBeginDate(), fiscalPeriod.getEndDate());
        if (!financialTransactions.isEmpty()) {
           throw new NoEmptyFiscalPeriodException(financialTransactions);
        }
    }    

    /**
     * Check if all previous financial period for a an entrybook are closed before bock the asked closedperiodicentrybook
     */
    @Override
    public void blockClosedPeriodicEntryBookRules(ClosedPeriodicEntryBook closedPeriodicEntryBook)
            throws UnbalancedFinancialTransactionsException, WithoutEntryBookFinancialTransactionsException,
            UnfilledEntriesException, NotLockedClosedPeriodicEntryBooksException {

        super.blockClosedPeriodicEntryBookRules(closedPeriodicEntryBook);

        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookDao = getDaoHelper().getClosedPeriodicEntryBookDao();

        List<ClosedPeriodicEntryBook> allPreviousClosedPeriodicEntryBooksNotLocked =
                closedPeriodicEntryBookDao.findAllPreviousClosedPeriodicEntryBooksNotLocked(closedPeriodicEntryBook);

        //Check if the fiscal period to block is the oldest
        if (!allPreviousClosedPeriodicEntryBooksNotLocked.isEmpty()) {
            throw new NotLockedClosedPeriodicEntryBooksException(allPreviousClosedPeriodicEntryBooksNotLocked);
        }
    }
}
