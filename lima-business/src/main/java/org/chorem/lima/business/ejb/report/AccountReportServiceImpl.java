package org.chorem.lima.business.ejb.report;

/*
 * #%L
 * Lima :: Business
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sf.jasperreports.engine.JasperReport;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.beans.AccountEntry;
import org.chorem.lima.beans.AccountEntryImpl;
import org.chorem.lima.beans.DocumentReport;
import org.chorem.lima.beans.ReportsDatas;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.ReportService;
import org.chorem.lima.business.api.report.AccountReportService;
import org.chorem.lima.business.ejb.AbstractLimaService;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountTopiaDao;
import org.chorem.lima.entity.Entry;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 26/06/15.
 */
@Stateless
@Remote(AccountReportService.class)
@TransactionAttribute
public class AccountReportServiceImpl extends AbstractLimaService implements AccountReportService {

    @EJB
    protected IdentityService identityService;
    @EJB
    protected ReportService reportService;

    @Override
    public DocumentReport getAccountDocumentReport(String accountId, Date from, Date to, JasperReport accountsEntryJasperReport, DecimalFormat bigDecimalFormat) {
        String reportName = t("lima-business.document.accountReport.title");
        DocumentReport documentReport = CommonsDocumentReport.getDocumentReport(reportName, from, to, accountsEntryJasperReport, bigDecimalFormat, identityService);

        setHeaderColumnTitles(documentReport);

        if (StringUtils.isNotBlank(accountId)) {
            AccountTopiaDao accountTopiaDao = getDaoHelper().getAccountDao();
            Account account = accountTopiaDao.forTopiaIdEquals(accountId).findUniqueOrNull();

            if (from != null && to != null && account != null) {
                String selectedAccounts = account.getAccountNumber();

                Collection<AccountEntry> accountEntries = new ArrayList<>();

                ReportsDatas results = reportService.generateAccountsReports(account, true, from, to);
                List<Entry> entries = results.getListEntry();

                if (CollectionUtils.isNotEmpty(entries)) {
                    for (Entry entry : entries) {
                        if (entry.getAmount() == null || BigDecimal.ZERO.compareTo(entry.getAmount()) == 0) {
                            continue;
                        }

                        String entryAccountNumber = entry.getAccount().getAccountNumber();
                        String code = "";
                        if (entry.getFinancialTransaction().getEntryBook() != null) {
                            code = entry.getFinancialTransaction().getEntryBook().getCode();
                        }

                        AccountEntry accountEntry = new AccountEntryImpl();
                        accountEntry.setAccountNumber(entryAccountNumber);
                        accountEntry.setTransactionDate(entry.getFinancialTransaction().getTransactionDate());
                        accountEntry.setCode(code);
                        accountEntry.setVoucher(entry.getVoucher());
                        accountEntry.setDescription(entry.getDescription());
                        accountEntry.setLettering(entry.getLettering());
                        accountEntry.setDebit(entry.isDebit() ? entry.getAmount() : BigDecimal.ZERO);
                        accountEntry.setCredit(entry.isDebit() ? BigDecimal.ZERO : entry.getAmount());
                        accountEntry.setFormatter(bigDecimalFormat);
                        accountEntry.setDateFormat(LimaBusinessConfig.getInstance().getDateFormat());
                        accountEntry.setSubTotalForLabel(t("lima-business.document.subTotal"));
                        accountEntries.add(accountEntry);
                    }
                }

                documentReport.addAllAccounts(accountEntries);

                if (CollectionUtils.isEmpty(accountEntries)) {
                    selectedAccounts += t("lima-business.document.selectedAccountNoEntryFound");
                } else if (accountEntries.size() == 1){
                    selectedAccounts += t("lima-business.document.selectedAccountAndSubAccount");
                } else {
                    selectedAccounts += t("lima-business.document.selectedAccountAndSubAccounts");
                }

                documentReport.setHeaderSelectedAccounts(selectedAccounts);

            } else if (log.isWarnEnabled()) {
                    log.warn("No account present");
            }

        } else if (log.isWarnEnabled()) {
            log.warn("No account present");
        }

        return documentReport;
    }

    protected void setHeaderColumnTitles(DocumentReport documentReport) {
        documentReport.setColumnAccountTitle(t("lima-business.document.columnAccountTitle"));
        documentReport.setColumnDateTitle(t("lima-business.document.columnDateTitle"));
        documentReport.setColumnEntryBookTitle(t("lima-business.document.columnEntryBookTitle"));
        documentReport.setColumnVoucherTitle(t("lima-business.document.columnVoucherTitle"));
        documentReport.setColumnDescriptionTitle(t("lima-business.document.columnDescriptionTitle"));
        documentReport.setColumnLetterTitle(t("lima-business.document.columnLetterTitle"));
        documentReport.setColumnDebitTitle(t("lima-business.document.columnDebitTitle"));
        documentReport.setColumnCreditTitle(t("lima-business.document.columnCreditTitle"));
    }
}
