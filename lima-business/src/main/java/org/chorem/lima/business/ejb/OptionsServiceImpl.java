/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.business.ejb;

import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.OptionsService;
import org.chorem.lima.business.config.LimaConfigOptionDef;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Locale;

@Stateless
@Remote(OptionsService.class)
public class OptionsServiceImpl extends AbstractLimaService implements OptionsService {

    @Override
    public int getScale() {
        return LimaBusinessConfig.getInstance().getScale();
    }

    @Override
    public boolean getCurrency() {
        return LimaBusinessConfig.getInstance().getCurrency();
    }

    @Override
    public char getDecimalSeparator() {
        return LimaBusinessConfig.getInstance().getDecimalSeparator();
    }

    @Override
    public char getThousandSeparator() {
        return LimaBusinessConfig.getInstance().getThousandSeparator();
    }

    @Override
    public void setScale(String scale) {
        LimaBusinessConfig.getInstance().setScale(scale);
    }

    @Override
    public void setThousandSeparator(String thousandSeparator) {
        LimaBusinessConfig.getInstance().setThousandSeparator(thousandSeparator);
    }

    @Override
    public void setCurrency(boolean currency) {
        LimaBusinessConfig.getInstance().setCurrency(Boolean.toString(currency));
    }

    @Override
    public void setDecimalSeparator(String decimalSeparator) {
        LimaBusinessConfig.getInstance().setDecimalSeparator(decimalSeparator);
    }

    @Override
    public LimaConfigOptionDef getScaleOption() {
        return LimaBusinessConfig.BusinessConfigOption.SCALE;
    }

    @Override
    public LimaConfigOptionDef getCurrencyOption() {
        return LimaBusinessConfig.BusinessConfigOption.CURRENCY;
    }

    @Override
    public LimaConfigOptionDef getDecimalSeparatorOption() {
        return LimaBusinessConfig.BusinessConfigOption.DECIMAL_SEPARATOR;
    }

    @Override
    public LimaConfigOptionDef getThousandSeparatorOption() {
        return LimaBusinessConfig.BusinessConfigOption.THOUSAND_SEPARATOR;
    }

    @Override
    public String getLimaHttpHostAddress() {
        return LimaBusinessConfig.getInstance().getHostHttpAddress();
    }

    @Override
    public int getLimaHttpPort() {
        return LimaBusinessConfig.getInstance().getHostHttpPort();
    }

    @Override
    public void setLocal(Locale local) {
        LimaBusinessConfig.getInstance().setLocal(local);
    }

}
