package org.chorem.lima.business.ejb.ebp;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.EntryEBP;
import org.chorem.lima.beans.EntryEBPImpl;
import org.chorem.lima.business.ejb.csv.AbstractLimaModel;
import org.chorem.lima.entity.Entry;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 03/06/14.
 */
public class EntryEBPModel extends AbstractLimaModel<EntryEBP> implements ExportModel<Entry> {

    public EntryEBPModel() {
        super(',');
        newMandatoryColumn("Journal", EntryEBP.PROPERTY_JOURNAL);
        newMandatoryColumn("Compte", EntryEBP.PROPERTY_COMPTE);
        newMandatoryColumn("DatEcr", EntryEBP.PROPERTY_DAT_ECR, DATE_PARSER);
        newIgnoredColumn("DatVal");
        newIgnoredColumn("DatSai");
        newIgnoredColumn("DatEch");
        newIgnoredColumn("Poste");
        newMandatoryColumn("Piece", EntryEBP.PROPERTY_PIECE);
        newIgnoredColumn("NumDoc");
        newOptionalColumn("Libelle", EntryEBP.PROPERTY_LIBELLE);
        newOptionalColumn("Debit", EntryEBP.PROPERTY_DEBIT, BIG_DECIMAL_WITH_NULL_PARSER);
        newOptionalColumn("Credit", EntryEBP.PROPERTY_CREDIT, BIG_DECIMAL_WITH_NULL_PARSER);
        newIgnoredColumn("Devise");
        newIgnoredColumn("TauxDevise");
        newIgnoredColumn("DevDebit");
        newIgnoredColumn("DevCredit");
        newIgnoredColumn("CDebit");
        newIgnoredColumn("CCredit");
        newOptionalColumn("Lettre", EntryEBP.PROPERTY_LETTRE);
        newIgnoredColumn("Rapp");
        newIgnoredColumn("CodReg");
        newIgnoredColumn("Cheque");
        newIgnoredColumn("CptTVA");
        newIgnoredColumn("MoisTVA");
        newIgnoredColumn("GTypEcr");
        newIgnoredColumn("GNumEcr");
        newIgnoredColumn("DateReleve");
        newIgnoredColumn("DatLet");
        newIgnoredColumn("SoftImport");
        newIgnoredColumn("RefBVR");
        newIgnoredColumn("NumAdhBVR");
        newIgnoredColumn("DatderGen");
        newIgnoredColumn("LetPart");
        newIgnoredColumn("bSaisieKM");
    }

    @Override
    public Iterable<ExportableColumn<Entry, Object>> getColumnsForExport() {
        ModelBuilder<Entry> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Journal", Entry.PROPERTY_FINANCIAL_TRANSACTION, FINANCIAL_TRANSACTION_TO_ENTRY_BOOK_FORMATTER);
        modelBuilder.newColumnForExport("Compte", Entry.PROPERTY_ACCOUNT, ACCOUNT_TO_ACCOUNT_NUMBER_FORMATTER);
        modelBuilder.newColumnForExport("DatEcr", Entry.PROPERTY_FINANCIAL_TRANSACTION, FINANCIAL_TRANSACTION_TO_TRANSACTION_DATE_FORMATTER);
        modelBuilder.newColumnForExport("Piece", Entry.PROPERTY_VOUCHER);
        modelBuilder.newColumnForExport("Libelle", Entry.PROPERTY_DESCRIPTION);
        modelBuilder.newColumnForExport("Debit", ENTRY_DEBIT_GETTER);
        modelBuilder.newColumnForExport("Credit", ENTRY_CREDIT_GETTER);
        modelBuilder.newColumnForExport("Lettre", Entry.PROPERTY_LETTERING);

        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public EntryEBP newEmptyInstance() {
        return new EntryEBPImpl();
    }
}
