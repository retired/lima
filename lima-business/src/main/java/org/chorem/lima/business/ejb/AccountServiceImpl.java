/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import org.apache.commons.lang.StringUtils;
import org.chorem.lima.business.AccountingRules;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.exceptions.AlreadyExistAccountException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnexistingAccount;
import org.chorem.lima.business.exceptions.UsedAccountException;
import org.chorem.lima.business.utils.AccountComparator;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountTopiaDao;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Permet d'implémenter le Plan Comptable Général.
 * Un compte ne peut être supprimé si il contient des écritures comptables.
 * Un compte peut devenir père et avoir des comptes fils. Chaque compte créé doit
 * renseigner si il appartient à un compte père avec le numéro de compte père.
 *
 * @author Rémi Chapelet
 */
@Stateless
@Remote(AccountService.class)
@TransactionAttribute
public class AccountServiceImpl extends AbstractLimaService implements AccountService {

    @Override
    public long getAccountCount() {
        AccountTopiaDao accountTopiaDao = getDaoHelper().getAccountDao();
        long result = accountTopiaDao.count();
        return result;
    }

    @Override
    public boolean createOrUpdateAccount(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException, NotAllowedLabelException {
        // check if account number already exist
        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();

        boolean result  = accountDao.forNaturalId(account.getAccountNumber().toUpperCase().trim()).exists();

        if (result) {
            // update
            updateAccount(account);
        } else {
            createNewAccount(account);
        }
        return result;
    }

    /**
     * Permet de créer un nouveau compte dans le PCG de l'application.
     *
     * @param account account to create
     * @throws LimaException
     */
    @Override
    public Account createAccount(Account account) throws AlreadyExistAccountException, InvalidAccountNumberException, NotNumberAccountNumberException {

        // check if account number already exist
        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();

        if (accountDao.forNaturalId(account.getAccountNumber()).exists()) {
            throw new AlreadyExistAccountException(account.getAccountNumber());
        }

        Account result = createNewAccount(account);
        
        return result;
    }

    protected Account createNewAccount(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException {
        // check rules before create the account
        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();
        accountingRules.createAccountRules(account);

        // force uppercase account number
        account.setAccountNumber(account.getAccountNumber().toUpperCase().trim());

        // check if account number already exist
        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();

        //create it
        Account result = accountDao.create(account);

        return result;
    }

    @Override
    public Account getMasterAccount(String accountNumber) {
        Account account = null;

        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();

        while (account == null && accountNumber.length() > 1) {
            accountNumber = accountNumber.substring(0, accountNumber.length() - 1);
            account = accountDao.forAccountNumberEquals(accountNumber).findUniqueOrNull();
        }
        return account;
    }

    /** Permet d'obtenir un compte suivant son numero */
    @Override
    public Account getAccountByNumber(String accountNumber) {

        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();
        Account account = accountDao.forAccountNumberEquals(accountNumber).findUniqueOrNull();

        return account;
    }


    /** Permet d'obtenir tous les comptes. */
    @Override
    public List<Account> getAllAccounts() {

        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();
        List<Account> accountsList = accountDao.findAll();
        Collections.sort(accountsList, new AccountComparator());
        
        return accountsList;
    }


    /** Permet d'obtenir tout les comptes feuilles */
    @Override
    public List<Account> getAllLeafAccounts() {

        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();
        List<Account> accountsList = accountDao.findAllLeafAccounts();

        return accountsList;
    }

    /*
     * @see org.chorem.lima.business.api.AccountService#getAllSubAccounts(org.chorem.lima.entity.Account)
     */
    @Override
    public List<Account> getAllSubAccounts(Account account) {

        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();
        List<Account> accountsList = accountDao.findAllSubAccounts(account);

        return accountsList;
    }

    /**
     * Permet d'effacer un compte dans la base de données.
     * <p/>
     * Si il existe une entrée comptable associée au numéro de
     * compte ou a un de ses sous-compte, il est alors impossible de supprimer le compte.
     *
     * @param account Le compte à supprimer
     * @throws UsedAccountException, UnexistingAccount
     */
    @Override
    public void removeAccount(Account account) throws UsedAccountException, UnexistingAccount {

        if (!account.isPersisted()) {
            throw new UnexistingAccount(account.getAccountNumber(), "Unsaved Account, this account can not be removed");
        }

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        // Check rules for account if have entries
        accountingRules.removeAccountRules(account);

        // remove account
        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();
        Account accountToDelete = accountDao.forTopiaIdEquals(account.getTopiaId()).findUnique();
        accountDao.delete(accountToDelete);
    }

    /**
     * Permet de modifier un compte sur son label.
     * <p/>
     * Il n'est pas possible de modifier un numéro de compte.
     * Si le compte n'existe pas, il envoie alors un message d'avertissement.
     *
     * @param account compte à modifier
     * @throws InvalidAccountNumberException
     */
    @Override
    public Account updateAccount(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException {

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        // DAO
        AccountTopiaDao accountDao = getDaoHelper().getAccountDao();
        Account originalAccount = accountDao.forAccountNumberEquals(account.getAccountNumber()).findUnique();
        Binder<Account, Account> binder = BinderFactory.newBinder(Account.class, Account.class);
        binder.copy(account, originalAccount, Account.PROPERTY_LABEL, Account.PROPERTY_THIRD_PARTY);
        accountingRules.updateAccountRules(originalAccount);
        Account result = accountDao.update(originalAccount);

        return result;
    }

    // TODO sbavencoff 18/07/2014 revoir la selection des comptes #1046
    @Override
    public List<Account> stringToListAccounts(String selectedAccounts)  {
        Set<Account> accounts = new HashSet<>();
        if (selectedAccounts != null) {
            AccountTopiaDao accountDao = getDaoHelper().getAccountDao();
            //Remove Spaces
            String result = StringUtils.deleteWhitespace(selectedAccounts);

            Boolean first = true;
            // TODO DCossé 12/06/14 separator can be anything as accounts are text field
            StringTokenizer stStar = new StringTokenizer(result, "-");
            while (stStar.hasMoreTokens()) {
                String subString = stStar.nextToken();

                //Split comma
                // TODO DCossé 12/06/14 separator can be anything as accounts are text field
                StringTokenizer stComma = new StringTokenizer(subString, ",");
                while (stComma.hasMoreTokens()) {
                    String s = stComma.nextToken();
                    //if intervall account
                    if (s.contains("..") && !s.endsWith("..")) {
                        //Split ..
                        String stringDoubleDot[] = s.split("\\.\\.");

                        List<Account> resultInterval = accountDao.findIntervalAccountByNumber(stringDoubleDot[0], stringDoubleDot[1]);

                        //if first add accounts, else remove
                        if (first) {
                            accounts.addAll(resultInterval);
                        } else {
                            accounts.removeAll(resultInterval);
                        }
                    }
                    //else one account
                    else {

                        Account account = accountDao.forAccountNumberEquals(s).findUniqueOrNull();

                        //if exist
                        if (account != null) {
                            //if first
                            if (first) {
                                accounts.add(account);
                            } else {
                                accounts.remove(account);
                            }
                        }
                        //search all account start with accountnumber
                        else {
                            List<Account> accountsResult = accountDao.findLeafAccounts(s);
                            if (accountsResult != null) {
                                //if first
                                if (first) {
                                    accounts.addAll(accountsResult);
                                } else {
                                    accounts.removeAll(accountsResult);
                                }
                            }
                        }
                    }
                }
                first = false;
            }
        }
        return new ArrayList<>(accounts);
    }

    @Override
    public Account findAccountById(String accountId) {
        Account account = null;
        if (StringUtils.isNotBlank(accountId)) {
            AccountTopiaDao accountDao = getDaoHelper().getAccountDao();
            account = accountDao.forTopiaIdEquals(accountId).findUniqueOrNull();
        }
        return account;
    }

    @Override
    public void removeAllAccounts() {
        AccountTopiaDao dao = getDaoHelper().getAccountDao();
        List<Account> accounts = dao.findAll();
        dao.deleteAll(accounts);
    }
}
