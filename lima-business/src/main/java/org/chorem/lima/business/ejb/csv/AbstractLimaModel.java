package org.chorem.lima.business.ejb.csv;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.FinancialStatementWayEnum;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.FinancialStatement;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.VatStatement;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueGetter;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.AbstractImportModel;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by davidcosse on 03/06/14.
 */
public abstract class AbstractLimaModel<E> extends AbstractImportModel<E> {

    protected static final String DATE_FORMAT = "dd/MM/yyyy";

    public AbstractLimaModel(char separator) {
        super(separator);
    }

    /**
     * String to integer converter.
     */
    protected static final ValueParser<Integer> INT_PARSER = new ValueParser<Integer>() {
        @Override
        public Integer parse(String value) throws ParseException {
            int result = 0;
            if (!Strings.isNullOrEmpty(value)) {
                result = Integer.valueOf(value);
            }
            return result;
        }
    };

    /**
     * O/N boolean parser.
     */
    protected static final ValueParser<Boolean> O_N_PARSER = new ValueParser<Boolean>() {
        @Override
        public Boolean parse(String value) throws ParseException {
            Boolean result;
            if ("O".equalsIgnoreCase(value)) {
                result = Boolean.TRUE;
            } else {
                result = Boolean.FALSE;
            }
            return result;
        }
    };

    protected static final ValueFormatter<Boolean> O_N_FORMATTER = new ValueFormatter<Boolean>() {
        @Override
        public String format(Boolean value) {
            String result = value ? "O" : "N";
            return result;
        }
    };

    protected static final ValueFormatter<Date> DATE_FORMATTER = new ValueFormatter<Date>() {
        @Override
        public String format(Date value) {
            String result;
            if (value != null) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
                result = simpleDateFormat.format(value);
            } else {
                result = "";
            }
            return result;
        }
    };

    /**
     * Date converter converter.
     */
    protected static final ValueParser<Date> DATE_PARSER = new ValueParser<Date>() {
        @Override
        public Date parse(String value) throws ParseException {
            Date result = null;
            if (!Strings.isNullOrEmpty(value)) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
                result = simpleDateFormat.parse(value);
            }
            return result;
        }
    };





    protected static final ValueParser<BigDecimal> BIG_DECIMAL_WITH_NULL_PARSER =  new ValueParser<BigDecimal>() {
        @Override
        public BigDecimal parse(String value) throws ParseException {
            BigDecimal result = null;
            if (!value.isEmpty()) {
                // " " est un espace insécable, pas un " "
                Double dval = Double.valueOf(value.replace(',', '.').replace(" ", ""));
                result = BigDecimal.valueOf(dval);
            }
            return result;
        }
    };

    protected static final ValueFormatter<BigDecimal> BIG_DECIMAL_FORMATTER = new ValueFormatter<BigDecimal>() {
        @Override
        public String format(BigDecimal value) {
            String result;
            if (value != null) {
                result = String.valueOf(value);
            } else {
                result = "";
            }
            return result;
        }
    };

    protected static final ValueFormatter<Account> ACCOUNT_TO_ACCOUNT_NUMBER_FORMATTER = new ValueFormatter<Account>() {
        @Override
        public String format(Account value) {
            String result;
            if (value != null) {
                result = value.getAccountNumber();
            } else {
                result = "";
            }
            return result;
        }
    };

    protected static final ValueFormatter<FinancialStatementWayEnum> FINANCIAL_STATEMENT_WAY_ENUM_VALUE_FORMATTER = new ValueFormatter<FinancialStatementWayEnum>() {
        @Override
        public String format(FinancialStatementWayEnum value) {
            String result;
            if (value != null) {
                result = value.name();
            } else {
                result = "";
            }
            return result;
        }
    };

    protected static final ValueParser<FinancialStatementWayEnum> FINANCIAL_STATEMENT_WAY_ENUM_VALUE_PARSER = new ValueParser<FinancialStatementWayEnum>() {

        @Override
        public FinancialStatementWayEnum parse(String value) {
            FinancialStatementWayEnum result = null;
            if (StringUtils.isNotBlank(value)) {
                value = StringUtils.trim(value.toUpperCase());
                switch (value) {
                    case "BOTH":
                        result = FinancialStatementWayEnum.BOTH;
                        break;
                    case "DEBIT":
                        result = FinancialStatementWayEnum.DEBIT;
                        break;
                    case "CREDIT":
                        result = FinancialStatementWayEnum.CREDIT;
                        break;
                    default:
                        throw new UnsupportedOperationException("Unsupported statement way" + value);
                }
            }
            return result;
        }
    };

    protected static final ValueFormatter<FinancialStatement> MASTER_FINANCIAL_STATEMENT_PATH_FORMATTER = new ValueFormatter<FinancialStatement>() {

        @Override
        public String format(FinancialStatement value) {
            String result = value == null ? "" : getFinancialStatementFullPath(value);
            return result;
        }
    };

    protected static String getFinancialStatementFullPath(FinancialStatement value) {
        String result = value.getMasterFinancialStatement() != null ? getFinancialStatementFullPath(value.getMasterFinancialStatement()) + "/" + value.getLabel() : value.getLabel();
        return result;
    }

    protected static String getVatFullPath(VatStatement value) {
        String result = value.getMasterVatStatement() != null ? getVatFullPath(value.getMasterVatStatement()) + "/" + value.getLabel() : value.getLabel();
        return result;
    }

    protected static final ValueFormatter<VatStatement> MASTER_VAT_PATH_FORMATTER = new ValueFormatter<VatStatement>() {

        @Override
        public String format(VatStatement value) {
            String result = value == null ? "" : getVatFullPath(value);
            return result;
        }
    };

    protected static final ValueFormatter<FinancialTransaction> FINANCIAL_TRANSACTION_TO_ENTRY_BOOK_FORMATTER = new ValueFormatter<FinancialTransaction>() {
        @Override
        public String format(FinancialTransaction value) {
            String result;
            if (value != null) {
                result = value.getEntryBook().getCode();
            } else {
                result = "";
            }
            return result;
        }
    };

    protected static final ValueFormatter<FinancialTransaction> FINANCIAL_TRANSACTION_TO_TRANSACTION_DATE_FORMATTER = new ValueFormatter<FinancialTransaction>() {
        @Override
        public String format(FinancialTransaction value) {
            String result;
            if (value != null) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
                result = simpleDateFormat.format(value.getTransactionDate());
            } else {
                result = "";
            }
            return result;
        }
    };

    protected static final ValueGetter<Entry, String> ENTRY_DEBIT_GETTER = new ValueGetter<Entry, String>() {
        @Override
        public String get(Entry entry) throws Exception {
            String result = entry.isDebit() ? String.valueOf(entry.getAmount()) : "";
            return result;
        }
    };

    protected static final ValueGetter<Entry, String> ENTRY_CREDIT_GETTER = new ValueGetter<Entry, String>() {
        @Override
        public String get(Entry entry) throws Exception {
            String result = entry.isDebit() ? "" : String.valueOf(entry.getAmount());
            return result;
        }
    };
}
