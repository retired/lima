/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.lima.business.ejb;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.beans.ReportsDatas;
import org.chorem.lima.beans.VatStatementAmounts;
import org.chorem.lima.beans.VatStatementAmountsImpl;
import org.chorem.lima.beans.VatStatementDatas;
import org.chorem.lima.beans.VatStatementDatasImpl;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.ReportService;
import org.chorem.lima.business.api.VatStatementService;
import org.chorem.lima.business.exceptions.AlreadyAffectedVatBoxException;
import org.chorem.lima.business.exceptions.AlreadyExistVatStatementException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.VatStatement;
import org.chorem.lima.entity.VatStatementTopiaDao;
import org.nuiton.topia.persistence.TopiaNonUniqueResultException;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Permet d'implémenter le plan de la déclaration de TVA
 *
 * @author vsalaun
 */
@Stateless
@Remote(VatStatementService.class)
@TransactionAttribute
public class VatStatementServiceImpl extends AbstractLimaService implements VatStatementService {

    @EJB
    private ReportService reportService;

    @EJB
    private AccountService accountService;

    protected static final Function<VatStatement, String> GET_LABEL = new Function<VatStatement, String>() {
        @Override
        public String apply(VatStatement input) {
            return input == null ? null : input.getLabel();
        }
    };

    protected void validateNewVATStatement(VatStatement masterVatStatement, VatStatement vatStatement) throws AlreadyExistVatStatementException, NotAllowedLabelException, AlreadyAffectedVatBoxException {

        if (vatStatement.getLabel()!= null && vatStatement.getLabel().contains("/")) {
            throw new NotAllowedLabelException(vatStatement.getLabel());
        }
        if (masterVatStatement != null) {
            Collection<VatStatement> masterSubVatStatements = masterVatStatement.getSubVatStatements();
            if (masterSubVatStatements == null) {
                masterSubVatStatements = Lists.newArrayList();
            }
            Map<String, VatStatement> indexedSubVatStatements = Maps.uniqueIndex(masterSubVatStatements, GET_LABEL);
            if (indexedSubVatStatements.get(vatStatement.getLabel()) != null && (!vatStatement.isPersisted() || !indexedSubVatStatements.get(vatStatement.getLabel()).getTopiaId().contentEquals(vatStatement.getTopiaId()))){
                throw new AlreadyExistVatStatementException(vatStatement.getLabel(), masterVatStatement.getLabel());
            }
        }

        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();
        try {
            String boxName = StringUtils.trimToNull(vatStatement.getBoxName());
            vatStatement.setBoxName(boxName);

            if (boxName != null) {
                VatStatement vatStatementWithSameBoxName = vatStatementTopiaDao.forBoxNameEquals(vatStatement.getBoxName()).findUniqueOrNull();
                if (vatStatementWithSameBoxName != null && (!vatStatement.isPersisted() || !vatStatementWithSameBoxName.getTopiaId().contentEquals(vatStatement.getTopiaId()))) {
                    throw new AlreadyAffectedVatBoxException(vatStatement.getBoxName());
                }
            }
        } catch (TopiaNonUniqueResultException e) {
            throw new AlreadyAffectedVatBoxException(vatStatement.getBoxName());
        }
    }


    protected void addVatStatementToMaster(VatStatement masterVatStatement,
                                           VatStatement vatStatement) {
        if (masterVatStatement != null) {
            VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();

            masterVatStatement.addSubVatStatements(vatStatement);
            masterVatStatement.setHeader(true);

            if (masterVatStatement.isPersisted()) {
                // update the persisted entity to avoid NonUniqueObjectException: A different object with the same identifier value was already associated with the session
                VatStatement vatStatementToUpdate = vatStatementTopiaDao.forTopiaIdEquals(masterVatStatement.getTopiaId()).findUnique();
                Binder<VatStatement, VatStatement> binder = BinderFactory.newBinder(VatStatement.class, VatStatement.class);
                binder.copy(masterVatStatement, vatStatementToUpdate);
                vatStatementTopiaDao.update(vatStatementToUpdate);
            } else {
                vatStatementTopiaDao.create(masterVatStatement);
            }
        }
    }


    @Override
    public VatStatement createVatStatement(VatStatement masterVatStatement,
                                                 VatStatement vatStatement) throws AlreadyExistVatStatementException, NotAllowedLabelException, AlreadyAffectedVatBoxException {

        VatStatement result = null;
        if (StringUtils.isNotBlank(vatStatement.getLabel()) || StringUtils.isNotBlank(vatStatement.getAccounts()) || StringUtils.isNotBlank(vatStatement.getBoxName())) {
            validateNewVATStatement(masterVatStatement, vatStatement);

            if (!vatStatement.isPersisted()) {
                VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();
                result = vatStatementTopiaDao.create(vatStatement);
            } else {
                result = vatStatement;
            }

            addVatStatementToMaster(masterVatStatement, result);
        }

        return result;
    }

    @Override
    public VatStatement updateVatStatement(VatStatement vatStatement) throws AlreadyExistVatStatementException, AlreadyAffectedVatBoxException, NotAllowedLabelException {
        VatStatement result = null;
        if (StringUtils.isNotBlank(vatStatement.getLabel()) || StringUtils.isNotBlank(vatStatement.getAccounts()) || StringUtils.isNotBlank(vatStatement.getBoxName())) {
            validateNewVATStatement(vatStatement.getMasterVatStatement(), vatStatement);
            // TopiaDao
            VatStatementTopiaDao vatStatementHeaderTopiaDao = getDaoHelper().getVatStatementDao();
            VatStatement originalVatStatement = vatStatementHeaderTopiaDao.forTopiaIdEquals(vatStatement.getTopiaId()).findUnique();
            Binder<VatStatement, VatStatement> binder = BinderFactory.newBinder(VatStatement.class, VatStatement.class);
            binder.copy(vatStatement, originalVatStatement);
            //update
            result = vatStatementHeaderTopiaDao.update(originalVatStatement);
        }

        return result;
    }

    @Override
    public List<VatStatement> getAllVatStatements() {

        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();
        List<VatStatement> vatStatements = vatStatementTopiaDao.findAll();

        return vatStatements;

    }

    @Override
    public List<VatStatement> getAllChildrenVatStatement(
            VatStatement vatStatement, List<VatStatement> result) {

        List<VatStatement> childVatStatements =
                getChildrenVatStatement(vatStatement);
        for (VatStatement childVatStatement : childVatStatements) {
            result.add(childVatStatement);
            getAllChildrenVatStatement(childVatStatement, result);
        }
        return result;

    }

    @Override
    public List<VatStatement> getChildrenVatStatement(VatStatement masterVatStatement)  {

        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();
        List<VatStatement> vatStatements = vatStatementTopiaDao.getChildrenVatStatement(masterVatStatement);

        return vatStatements;

    }

    /**
     * Gives the list of account numbers from a VatStatement.
     *
     * @param vatStatement
     * @return accountNumbersList
     */
    protected String findAccountNumberByVatStatement(VatStatement vatStatement)  {

        String accountNumbersList = null;

        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();
        VatStatement vatStatement2 = vatStatementTopiaDao.findVatStatementByLabel(vatStatement.getLabel());

        if (vatStatement2 != null) {
            accountNumbersList = vatStatement2.getAccounts();
        }

        return accountNumbersList;
    }

    @Override
    public VatStatementAmounts vatStatementAmounts(VatStatement vatStatement,
                                                   Date selectedBeginDate,
                                                   Date selectedEndDate) {
        VatStatementAmounts vatStatementAmounts = new VatStatementAmountsImpl();

        BigDecimal debitTemp = BigDecimal.ZERO;
        BigDecimal creditTemp = BigDecimal.ZERO;

        String accountNumbersList = findAccountNumberByVatStatement(vatStatement);

        List<Account> accountsList = accountService.stringToListAccounts(accountNumbersList);

        for (Account account : accountsList) {
            ReportsDatas reportsDatas = reportService.
                    generateAccountsReports(account, true,
                                                          selectedBeginDate, selectedEndDate);
            if (reportsDatas.isSoldeDebit()) {
                debitTemp = debitTemp.add(reportsDatas.getAmountSolde());
            } else {
                creditTemp = creditTemp.add(reportsDatas.getAmountSolde());
            }
        }
        //return debitTemp.subtract(creditTemp).abs();
        vatStatementAmounts.setAmount(debitTemp.subtract(creditTemp).abs());
        return vatStatementAmounts;

    }

    @Override
    public VatStatement findVatStatementByLabel(String label) {

        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();
        VatStatement vatStatement = vatStatementTopiaDao.findVatStatementByLabel(label);

        return vatStatement;
    }

    /** remote methode to get list of financial statement */
    public List<VatStatementAmounts> vatStatementReport(Date beginDate, Date endDate) {

        //create list form tree
        VatStatementDatas vatStatementDatas = vatStatementReport(null, beginDate, endDate,
                                                                 new VatStatementDatasImpl());
        List<VatStatementAmounts> result = vatStatementDatas.getListResult();

        return result;
    }

    /**
     * Créé la liste de postes contenant les calculs de comptes
     *
     * @throws LimaException
     */
    public VatStatementDatas vatStatementReport(VatStatement vatStatement,
                                                Date selectedBeginDate,
                                                Date selectedEndDate,
                                                VatStatementDatas result)  {
        List<VatStatement> vatStatements =
                getChildrenVatStatement(vatStatement);

        BigDecimal amount = BigDecimal.ZERO;
        List<VatStatementAmounts> subResult =
                new ArrayList<>();
        for (VatStatement subVatStatement : vatStatements) {
            VatStatementAmounts vatStatementAmounts =
                    vatStatementAmounts(subVatStatement,
                                        selectedBeginDate, selectedEndDate);
            vatStatementAmounts.setLabel(subVatStatement.getLabel());
            if (!subVatStatement.isHeader()) {
                //on calcul
                amount =
                        amount.add(vatStatementAmounts.getAmount());
                subResult.add(vatStatementAmounts);
            } else {
                VatStatementDatas vatStatementDatas =
                        vatStatementReport(subVatStatement,
                                           selectedBeginDate, selectedEndDate, result);
                amount = amount.add(vatStatementDatas.
                        getVatStatementAmounts().getAmount());


                VatStatementAmounts headerVatStatementAmounts =
                        vatStatementDatas.getVatStatementAmounts();
                //Si sous-total
                if (subVatStatement.isHeader()) {
                    VatStatementAmounts header =
                            new VatStatementAmountsImpl();
                    header.setLabel(headerVatStatementAmounts.getLabel());
                    header.setLevel(headerVatStatementAmounts.getLevel());
                    header.setHeader(true);
                    //ajoute header
                    subResult.add(header);
                    //ajoute liste
                    if (vatStatementDatas.getListResult() != null) {
                        subResult.addAll(vatStatementDatas.getListResult());
                    }
                    //ajoute une ligne vide
                    subResult.add(new VatStatementAmountsImpl());

                }
            }
        }
        VatStatementAmounts vatStatementAmounts =
                new VatStatementAmountsImpl();
        vatStatementAmounts.setAmount(amount);
        if (vatStatement != null) {
            vatStatementAmounts.setLabel(vatStatement.getLabel());
            vatStatementAmounts.setHeader(vatStatement.isHeader());
            vatStatementAmounts.setLevel(vatStatement.getLevel());
        }
        result.setVatStatementAmounts(vatStatementAmounts);
        result.setListResult(subResult);

        return result;
    }

    @Override
    public void removeVatStatement(VatStatement vatStatement) {

        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();

        // remove vatstatement
        VatStatement vatStatementToDelete =
                vatStatementTopiaDao.forTopiaIdEquals(
                        vatStatement.getTopiaId()).findUnique();
        vatStatementTopiaDao.delete(vatStatementToDelete);

        //get all subVatStatement
        List<VatStatement> vatStatements =
                getAllChildrenVatStatement(vatStatement,
                        new ArrayList<>());

        //if VatStatement have subVatStatement
        if (vatStatements.size() > 0) {
            for (VatStatement subVatStatement : vatStatements) {
                VatStatement subVatStatementToDelete =
                        vatStatementTopiaDao.forTopiaIdEquals(
                                subVatStatement.getTopiaId()).findUnique();
                vatStatementTopiaDao.delete(subVatStatementToDelete);
            }
        }
}

    @Override
    public boolean checkVatStatementExist(String label) {

        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();
        boolean result = vatStatementTopiaDao.forProperties(VatStatement.PROPERTY_LABEL, label).exists();

        return result;
    }

    @Override
    public void removeAllVatStatement() {
        for (VatStatement vatStatement : getChildrenVatStatement(null)) {
            removeVatStatement(vatStatement);
        }
    }

    @Override
    public List<VatStatement> getRootVatStatements() {
        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();

        List<VatStatement> result = vatStatementTopiaDao
                .forMasterVatStatementEquals(null)
                .setOrderByArguments(VatStatement.PROPERTY_TOPIA_CREATE_DATE)
                .findAll();
        return result;
    }

    @Override
    public VatStatement newVatStatement() {
        VatStatementTopiaDao vatStatementTopiaDao = getDaoHelper().getVatStatementDao();
        VatStatement result = vatStatementTopiaDao.newInstance();
        return result;
    }
}
