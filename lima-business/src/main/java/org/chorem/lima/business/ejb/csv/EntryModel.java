package org.chorem.lima.business.ejb.csv;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

import java.util.Map;

/**
 * Created by davidcosse on 03/06/14.
 */
public class EntryModel extends AbstractLimaModel<Entry> implements ExportModel<Entry> {

    public static final String PROPERTY_ENTRY_BOOK = Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_ENTRY_BOOK + "." + EntryBook.PROPERTY_CODE;
    public static final String PROPERTY_DATE = Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_TRANSACTION_DATE;

    protected final FinancialTransactionService financialTransactionService;
    protected final AccountService accountService;
    protected final EntryBookService entryBookService;
    protected final boolean humanReadable;

    public EntryModel(AccountService accountService, EntryBookService entryBookService, FinancialTransactionService financialTransactionService, boolean humanReadable) {
        super(';');
        this.accountService = accountService;
        this.entryBookService = entryBookService;
        this.financialTransactionService = financialTransactionService;
        this.humanReadable = humanReadable;

        newMandatoryColumn("financialTransaction", Entry.PROPERTY_FINANCIAL_TRANSACTION, new FinancialTransactionParser());
        if (humanReadable) {
            newMandatoryColumn("entryBook", Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_ENTRY_BOOK, new EntryBookParser());
            newMandatoryColumn("date", PROPERTY_DATE, DATE_PARSER);
        }
        newMandatoryColumn("account", Entry.PROPERTY_ACCOUNT, new AccountParser());
        newOptionalColumn("amount", Entry.PROPERTY_AMOUNT, BIG_DECIMAL_WITH_NULL_PARSER);
        newOptionalColumn("debit", Entry.PROPERTY_DEBIT, O_N_PARSER);
        newOptionalColumn("detail", Entry.PROPERTY_DETAIL);
        newOptionalColumn("position", Entry.PROPERTY_POSITION);
        newOptionalColumn("voucher", Entry.PROPERTY_VOUCHER);
        newOptionalColumn("description", Entry.PROPERTY_DESCRIPTION);
        newOptionalColumn("lettering", Entry.PROPERTY_LETTERING);
    }

    @Override
    public Iterable<ExportableColumn<Entry, Object>> getColumnsForExport() {
        ModelBuilder<Entry> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("financialTransaction", Entry.PROPERTY_FINANCIAL_TRANSACTION, new financialTransactionFormatter());
        if (humanReadable) {
            modelBuilder.newColumnForExport("entryBook", PROPERTY_ENTRY_BOOK);
            modelBuilder.newColumnForExport("date", PROPERTY_DATE, DATE_FORMATTER);
        }
        modelBuilder.newColumnForExport("account", Entry.PROPERTY_ACCOUNT, ACCOUNT_TO_ACCOUNT_NUMBER_FORMATTER);
        modelBuilder.newColumnForExport("amount", Entry.PROPERTY_AMOUNT, BIG_DECIMAL_FORMATTER);
        modelBuilder.newColumnForExport("debit", Entry.PROPERTY_DEBIT, O_N_FORMATTER);
        modelBuilder.newColumnForExport("detail", Entry.PROPERTY_DETAIL);
        modelBuilder.newColumnForExport("position", Entry.PROPERTY_POSITION);
        modelBuilder.newColumnForExport("voucher", Entry.PROPERTY_VOUCHER);
        modelBuilder.newColumnForExport("description", Entry.PROPERTY_DESCRIPTION);
        modelBuilder.newColumnForExport("lettering", Entry.PROPERTY_LETTERING);

        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public Entry newEmptyInstance() {
        return new EntryImpl();
    }

    protected class FinancialTransactionParser implements ValueParser<FinancialTransaction> {

        final Map<Integer, FinancialTransaction> transactionByNums = Maps.newHashMap();

        @Override
        public FinancialTransaction parse(String value) {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(value));
            FinancialTransaction result;
            if (humanReadable) {
                int num = Integer.parseInt(value);
                result = transactionByNums.get(num);
                if (result == null) {
                    result = new FinancialTransactionImpl();
                    transactionByNums.put(num, result);
                }
            } else {
                result = financialTransactionService.getFinancialTransactionWithId(value);
            }
            return result;
        }
    }

    protected class AccountParser implements ValueParser<Account> {

        @Override
        public Account parse(String value) {
            Account result;
            if (StringUtils.isNotBlank(value)) {
                result = accountService.getAccountByNumber(value);
            } else {
                result = null;
            }
            return result;
        }
    }

    protected class EntryBookParser implements ValueParser<EntryBook> {

        protected final Map<String, EntryBook> entryBookByCode;

        public EntryBookParser() {
            entryBookByCode = Maps.newHashMap();
            for (EntryBook entryBook : entryBookService.getAllEntryBooks()) {
                entryBookByCode.put(entryBook.getCode(), entryBook);
            }
        }

        @Override
        public EntryBook parse(String value) {
            EntryBook result;
            if (StringUtils.isNotBlank(value)) {
                result = entryBookByCode.get(value);
            } else {
                result = null;
            }
            return result;
        }
    }

    protected class financialTransactionFormatter implements ValueFormatter<FinancialTransaction> {

        final Map<FinancialTransaction, Integer> numByTransactions = Maps.newHashMap();

        int nextNum = 0;

        @Override
        public String format(FinancialTransaction value) {
            String result;
            if (value != null) {
                if (humanReadable) {
                    Integer num = numByTransactions.get(value);

                    if (num == null) {

                        num = nextNum++;

                        numByTransactions.put(value, num);

                    }

                    result = num.toString();

                } else {

                    result = value.getTopiaId();

                }
            } else {
                result = "";
            }
            return result;
        }
    }
}
