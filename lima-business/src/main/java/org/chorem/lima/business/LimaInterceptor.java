/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaXAResource;
import org.chorem.lima.entity.LimaCallaoTopiaApplicationContext;
import org.chorem.lima.entity.LimaCallaoTopiaDaoSupplier;
import org.chorem.lima.entity.LimaCallaoTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaApplicationContextCache;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

/**
 * Interceptor for topia context transaction.
 * 
 * http://stackoverflow.com/questions/8608349
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
@Stateless
public class LimaInterceptor {

    private static final Log log = LogFactory.getLog(LimaInterceptor.class);

    public static final ThreadLocal<LimaCallaoTopiaDaoSupplier> DAO_HELPER = new ThreadLocal<>();

    @Resource
    private TransactionManager transactionManager;

    @AroundInvoke
    public Object invoke(InvocationContext context) throws Exception {

        Object result;

        // only open transaction when incoming call just
        // enter in service layer
        // interceptor will be called before each internal
        // ejb call, but opening a new transaction in not required
        LimaCallaoTopiaDaoSupplier supplier = DAO_HELPER.get();
        if (supplier == null) {

            // maybe take care of TransactionAttributeTypes ?
            if (context.getTarget().getClass().getAnnotation(TransactionAttribute.class) != null ||
                    context.getMethod().getAnnotation(TransactionAttribute.class) != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Init new topia transaction for method : " +
                            context.getTarget().getClass() + "#" + context.getMethod().getName());
                }

                LimaCallaoTopiaApplicationContext rootContext = TopiaApplicationContextCache.getContext(
                        LimaBusinessConfig.getRootContextProperties(),
                        LimaConfigurationHelper.getCreateTopiaContextFunction());

                LimaCallaoTopiaPersistenceContext tx = rootContext.newPersistenceContext();

                DAO_HELPER.set(tx);

                Transaction tr = transactionManager.getTransaction();

                // enlist topia xaresource, will commited or rollback
                // by container
                tr.enlistResource(new LimaXAResource(tx));

                try {
                    result = context.proceed();
                } finally {
                    DAO_HELPER.remove();
                }
            } else {
                result = context.proceed();
            }

        } else {
            result = context.proceed();
        }

        return result;
    }

}
