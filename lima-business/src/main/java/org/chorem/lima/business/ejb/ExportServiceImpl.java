/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.business.ExportResult;
import org.chorem.lima.business.ImportExportResults;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.ExportService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FinancialStatementService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.VatStatementService;
import org.chorem.lima.business.ejb.csv.AccountModel;
import org.chorem.lima.business.ejb.csv.EntryBookModel;
import org.chorem.lima.business.ejb.csv.EntryModel;
import org.chorem.lima.business.ejb.csv.FinancialStatementModel;
import org.chorem.lima.business.ejb.csv.FinancialTransactionModel;
import org.chorem.lima.business.ejb.csv.FiscalControlExportModel;
import org.chorem.lima.business.ejb.csv.FiscalPeriodModel;
import org.chorem.lima.business.ejb.csv.IdentityModel;
import org.chorem.lima.business.ejb.csv.VatStatementModel;
import org.chorem.lima.business.ejb.ebp.AccountEBPModel;
import org.chorem.lima.business.ejb.ebp.EntryEBPModel;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountTopiaDao;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryTopiaDao;
import org.chorem.lima.entity.FinancialStatement;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.VatStatement;
import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.nio.charset.Charset;
import java.rmi.server.ExportException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * CSV import export service.
 *
 * @version $Revision$
 *          <p/>
 *          Last update : $Date$
 *          By : $Author$
 */
@Stateless
@Remote(ExportService.class)
@TransactionAttribute
public class ExportServiceImpl extends AbstractLimaService implements ExportService {

    @EJB
    protected EntryBookService entryBookService;

    @EJB
    protected AccountService accountService;

    @EJB
    protected FinancialTransactionService financialTransactionService;

    @EJB
    protected FinancialStatementService financialStatementService;

    @EJB
    protected FinancialPeriodService financialPeriodService;

    @EJB
    protected FiscalPeriodService fiscalPeriodService;

    @EJB
    protected VatStatementService vatStatementService;

    @EJB
    protected IdentityService identityService;

    protected void processExport(String charset, Class exportedClass, ExportResult exportResult, Iterable entities, ExportModel model) {
        try {
            String exportData = Export.exportToString(model, entities, Charset.forName(charset), true);
            exportResult.setExportData(exportData);
        } catch (Exception e) {
            exportResult.addException(new ExportException(String.format("Could not export class %s.", exportedClass), e));
        }
    }

    @Override
    public ImportExportResults exportAccountsAsCSV(String charset) {
        ImportExportResults results = new ImportExportResults();
        Class<Account> exportedClass = Account.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        AccountTopiaDao accountTopiaDao = getDaoHelper().getAccountDao();
        List<Account> entities = accountTopiaDao.findAll();

        AccountModel model = new AccountModel();
        processExport(charset, exportedClass, exportResult, entities, model);
        return results;
    }

    @Override
    public ImportExportResults exportEntryBooksAsCSV(String charset){
        ImportExportResults results = new ImportExportResults();
        Class exportedClass = EntryBook.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<EntryBook> entities = getDaoHelper().getEntryBookDao().findAll();

        EntryBookModel model = new EntryBookModel();
        processExport(charset, exportedClass, exportResult, entities, model);

        return results;
    }

    @Override
    public ImportExportResults exportFiscalPeriodsAsCSV(String charset){
        ImportExportResults results = new ImportExportResults();
        Class<FiscalPeriod> exportedClass = FiscalPeriod.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<FiscalPeriod> entities = fiscalPeriodService.getAllFiscalPeriods();

        FiscalPeriodModel model = new FiscalPeriodModel();
        processExport(charset, exportedClass, exportResult, entities, model);

        return results;
    }

    protected ImportExportResults exportFinancialTransactionsFile(String charset){
        ImportExportResults results = new ImportExportResults();
        Class<FinancialTransaction> exportedClass = FinancialTransaction.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<FinancialTransaction> entities = getDaoHelper().getFinancialTransactionDao().findAll();

        FinancialTransactionModel model = new FinancialTransactionModel(entryBookService);
        processExport(charset, exportedClass, exportResult, entities, model);

        return results;
    }

    @Override
    public ImportExportResults exportEntriesAsCSV(String charset, Boolean humanReadable){
        ImportExportResults results = new ImportExportResults();
        Class<Entry> exportedClass = Entry.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<Entry> entities = getDaoHelper().getEntryDao().findAll();

        EntryModel model = new EntryModel(accountService, entryBookService, financialTransactionService , humanReadable);
        processExport(charset, exportedClass, exportResult, entities, model);

        return results;
    }

    protected void addAllSubFinancialStatements(List<FinancialStatement> result, Collection<FinancialStatement> subFinancialStatements) {
        if (subFinancialStatements != null) {
            for (FinancialStatement subFinancialStatement : subFinancialStatements) {
                result.add(subFinancialStatement);
                addAllSubFinancialStatements(result, subFinancialStatement.getSubFinancialStatements());
            }
        }
    }

    @Override
    public ImportExportResults exportFinancialStatements(String charset){
        ImportExportResults results = new ImportExportResults();
        Class<FinancialStatement> exportedClass = FinancialStatement.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<FinancialStatement> rootFinancialStatements = financialStatementService.getRootFinancialStatements();
        FinancialStatementModel model = new FinancialStatementModel();
        List<FinancialStatement> financialStatements = new ArrayList<>();
        addAllSubFinancialStatements(financialStatements, rootFinancialStatements);
        processExport(charset, exportedClass, exportResult, financialStatements, model);

        return results;
    }

    protected List<VatStatement> getAllSubVATStatements(List<VatStatement> result, Collection<VatStatement> subVATStatements) {
        if (subVATStatements != null) {
            for (VatStatement subVATStatement : subVATStatements) {
                result.add(subVATStatement);
                getAllSubVATStatements(result, subVATStatement.getSubVatStatements());
            }
        }
        return result;
    }

    @Override
    public ImportExportResults exportVatStatements(String charset) {
        ImportExportResults results = new ImportExportResults();
        Class<VatStatement> exportedClass = VatStatement.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<VatStatement> rootVatStatements = vatStatementService.getRootVatStatements();
        VatStatementModel model = new VatStatementModel();
        List<VatStatement> vatStatements = new ArrayList<>();
        getAllSubVATStatements(vatStatements, rootVatStatements);
        processExport(charset, exportedClass, exportResult, vatStatements, model);
        return results;
    }

    protected ImportExportResults exportIdentity(String charset) {
        ImportExportResults results = new ImportExportResults();
        Class<Identity> exportedClass = Identity.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        Identity identity = identityService.getIdentity();

        List<Identity> identities = identity == null ? new ArrayList<>() : Lists.newArrayList(identity);

        IdentityModel model = new IdentityModel();
        processExport(charset, exportedClass, exportResult, identities, model);

        return results;
    }

    @Override
    public ImportExportResults exportAll(String charset) {

        ImportExportResults results = exportAccountsAsCSV(charset);
        results.pushExportResults(exportEntryBooksAsCSV(charset));
        results.pushExportResults(exportFiscalPeriodsAsCSV(charset));
        results.pushExportResults(exportFinancialTransactionsFile(charset));
        results.pushExportResults(exportEntriesAsCSV(charset, false));
        results.pushExportResults(exportIdentity(charset));

        return results;
    }

    //####################################### EBP ##############################################

    @Override
    public ImportExportResults exportAccountAsEbp(String charset) {
        ImportExportResults results = new ImportExportResults();
        Class<Account> exportedClass = Account.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<Account> accounts = accountService.getAllAccounts();

        AccountEBPModel model = new AccountEBPModel();
        processExport(charset, exportedClass, exportResult, accounts, model);
        return results;
    }

    @Override
    public ImportExportResults exportEntriesAsEbp(String charset){
        ImportExportResults results = new ImportExportResults();
        Class<Entry> exportedClass = Entry.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<FinancialTransaction> financialTransactions = getDaoHelper().getFinancialTransactionDao().findAllByDates();
        List<Entry> result = new ArrayList<>();
        for (FinancialTransaction financialTransaction : financialTransactions) {
            result.addAll(financialTransaction.getEntry());
        }
        EntryEBPModel model = new EntryEBPModel();
        processExport(charset, exportedClass, exportResult, result, model);

        return results;
    }

    @Override
    public ImportExportResults exportEntryBookAsEbp(String charset){
        ImportExportResults results = new ImportExportResults();
        Class<EntryBook> exportedClass = EntryBook.class;
        ExportResult exportResult = results.createAddAndGetExportResult(exportedClass);

        List<EntryBook> entryBooks = entryBookService.getAllEntryBooks();
        EntryBookModel model = new EntryBookModel();
        processExport(charset, exportedClass, exportResult, entryBooks, model);
        return results;
    }

    @Override
    public ExportResult exportFiscalControl(FiscalPeriod period, EntryBook entryBookAtNew, String charset) {
        Class<Entry> exportedClass = Entry.class;
        ExportResult exportResult = new ExportResult(exportedClass);

        EntryTopiaDao dao = getDaoHelper().getEntryDao();
        List<Entry> entities = dao.findAllEntryByDate(period.getBeginDate(), period.getEndDate());

        // on place les écritures de report a nouveau au debut
        List<Entry> entriesAtNew = dao.findAllEntryByDateForEntryBook(entryBookAtNew, period.getBeginDate(), period.getBeginDate());
        List<Entry> entries = Lists.newArrayListWithCapacity(entities.size());
        entities.removeAll(entriesAtNew);
        entries.addAll(entriesAtNew);
        entries.addAll(entities);

        FiscalControlExportModel model = new FiscalControlExportModel();
        processExport(charset, exportedClass, exportResult, entities, model);
        return exportResult;
    }

    @Override
    public String getFiscalControlFileName(FiscalPeriod fiscalPeriod) {
        /* cf : http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000027804775&cidTexte=LEGITEXT000006069583&dateTexte=20130802&oldAction=rechCodeArticle
        ** section IX
        */

        Identity identity = identityService.getIdentity();
        String siret= identity.getBusinessNumber();
        String siren = "";
        if (StringUtils.isNotBlank(siret)) {
            siret = siret.replace(" ", "");
            siren = siret.substring(0, 9);
        }

        return String.format("%1$sFEC%2$tY%2$tm%2$td", siren, fiscalPeriod.getEndDate());
    }
}
