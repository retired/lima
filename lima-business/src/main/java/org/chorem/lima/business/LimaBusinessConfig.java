/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.business.accountingrules.FranceAccountingRules;
import org.chorem.lima.business.config.LimaConfigOptionDef;
import org.chorem.lima.entity.LimaFlywayServiceImpl;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.topia.flyway.TopiaFlywayService;

import java.io.File;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Configuration pour le business.
 * <p/>
 * A voir comment le lier avec celui de lima swing.
 *
 * @author chatellier
 * @version $Revision$
 * <p/>
 * Last update : $Date$
 * By : $Author$
 */
public class LimaBusinessConfig {

    protected static final Log log = LogFactory.getLog(LimaBusinessConfig.class);

    protected static AccountingRules accountingRules;

    protected ApplicationConfig config;

    protected Properties rootContextProperties;

    protected static volatile LimaBusinessConfig instance;

    private LimaBusinessConfig(String configFileName) {
        try {
            ApplicationConfig defaultConfig = new ApplicationConfig(BusinessConfigOption.CONFIG_FILE.getDefaultValue());
            defaultConfig.loadDefaultOptions(BusinessConfigOption.values());
            defaultConfig.parse();

            if (StringUtils.isNotBlank(configFileName)) {
                Properties flatOptions = defaultConfig.getFlatOptions(false);

                config = new ApplicationConfig(flatOptions, configFileName);
                config.parse();
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("No specific configuration provided, using the default one");
                }
                config = defaultConfig;
            }
            instance = this;
        } catch (ArgumentsParserException ex) {
            throw new LimaTechnicalException("Can't read configuration", ex);
        }
    }

    private LimaBusinessConfig(final ApplicationConfig config) {
        try {
            ApplicationConfig defaultConfig = new ApplicationConfig(BusinessConfigOption.CONFIG_FILE.getDefaultValue());
            defaultConfig.loadDefaultOptions(BusinessConfigOption.values());
            defaultConfig.parse();

            if (config != null) {
                Properties flatOptions = defaultConfig.getFlatOptions();
                flatOptions.putAll(config.getFlatOptions(true));
                this.config = new ApplicationConfig(flatOptions, BusinessConfigOption.CONFIG_FILE.getDefaultValue());
                this.config.parse();
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("No specific configuration provided, using the default one");
                }
                this.config = defaultConfig;
            }
            instance = this;
        } catch (ArgumentsParserException ex) {
            throw new LimaTechnicalException("Can't read configuration", ex);
        }
    }

    protected static Properties getRootContextProperties() {
        if (getInstance().rootContextProperties == null) {
            Properties result = instance.getFlatOptions();

            // Flyway migration service has been setup during 1.1 development (latest stable was 1.0.6)
            Map<String, String> toAddIfNotPresent = Maps.newLinkedHashMap();
            toAddIfNotPresent.put("topia.service.migration", LimaFlywayServiceImpl.class.getName());
            toAddIfNotPresent.put("topia.service.migration." + TopiaFlywayService.USE_MODEL_VERSION, "true");

            for (Map.Entry<String, String> entry : toAddIfNotPresent.entrySet()) {
                if (!result.containsKey(entry.getKey())) {
                    result.setProperty(entry.getKey(), entry.getValue());
                }
            }
            getInstance().setRootContextProperties(result);

        }
        Properties result = getInstance().rootContextProperties;
        return result;
    }

    public void setRootContextProperties(Properties rootContextProperties) {
        this.rootContextProperties = rootContextProperties;
    }

    public synchronized static LimaBusinessConfig getInstance(String configFileName) {
        if (instance == null) {
            instance= new LimaBusinessConfig(configFileName);
        }
        return instance;
    }

    public static LimaBusinessConfig getInstance() {
        if (instance == null) {
            instance= new LimaBusinessConfig("");
        }
        return instance;
    }

    public static LimaBusinessConfig getInstance(ApplicationConfig config) {
        if (instance == null) {
            instance= new LimaBusinessConfig(config);
        }
        return instance;
    }

    public void setConfig(ApplicationConfig config) {
        this.config = config;
    }

    public ApplicationConfig getConfig() {
        return config;
    }

    /**
     * Instancie la bonne classe de nationalite en fonction du fichier de configuration.
     *
     * L'instance est conservée en cache.
     *
     * @return l'instance de rule
     */
    public AccountingRules getAccountingRules() {

        if (accountingRules == null) {
            loadAccountingRules();
        }

        return accountingRules;
    }

    protected static void loadAccountingRules() {
        Class<?> accountingRulesClass = getInstance().config.getOptionAsClass(BusinessConfigOption.RULES_NATIONALTY.key);
        if (accountingRulesClass == null) {
            if (log.isErrorEnabled()) {
                log.error("No accounting rules defined for:" + BusinessConfigOption.RULES_NATIONALTY.key);
            }
            accountingRules = new FranceAccountingRules();
        } else {
            try {
                accountingRules = (AccountingRules) accountingRulesClass.newInstance();
            } catch (Exception ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't instantiate accounting rules", ex);
                }

            }
        }
    }

    public Properties getFlatOptions() {
        return config.getFlatOptions();
    }

    public String getConfigFile() {
        return config.getOption(BusinessConfigOption.CONFIG_FILE.key);
    }

    public void setConfigFile(String configFile) {
        LimaBusinessConfig.getInstance().config.setOption(BusinessConfigOption.CONFIG_FILE.key, configFile);
    }

    public String getApplicationVersion() {
        return config.getOption(BusinessConfigOption.APPLICATION_VERSION.key);
    }

    public void setAccountingRule(String accountingRule) {
        LimaBusinessConfig.getInstance().config.setOption(BusinessConfigOption.RULES_NATIONALTY.key, accountingRule);
        // clear cache
        loadAccountingRules();
    }

    public Locale getLocal() {
        return config.getOptionAsLocale(BusinessConfigOption.LOCAL.key);
    }

    public void setLocal(Locale local) {
        config.setOption(BusinessConfigOption.LOCAL.key, local.toString());
        saveConfig();
    }

    public int getScale() {
        return config.getOptionAsInt(BusinessConfigOption.SCALE.key);
    }

    public void setScale(String locale) {
        config.setOption(BusinessConfigOption.SCALE.key, locale);
        saveConfig();
    }

    public boolean getCurrency() {
        return config.getOptionAsBoolean(BusinessConfigOption.CURRENCY.key);
    }

    public void setCurrency(String locale) {
        config.setOption(BusinessConfigOption.CURRENCY.key, locale);
        saveConfig();
    }

    public String getDateFormat() {
        return config.getOption(BusinessConfigOption.DATE_FORMAT.key);
    }

    public void setDateFormat(String locale) {
        config.setOption(BusinessConfigOption.DATE_FORMAT.key, locale);
        saveConfig();
    }


    public char getDecimalSeparator() {
        char decimalSeparator = config.getOption(BusinessConfigOption.DECIMAL_SEPARATOR.key).charAt(0);
        return decimalSeparator;
    }

    public void setDecimalSeparator(String locale) {
        config.setOption(BusinessConfigOption.DECIMAL_SEPARATOR.key, locale);
        saveConfig();
    }

    public char getThousandSeparator() {
        return config.getOption(BusinessConfigOption.THOUSAND_SEPARATOR.key).charAt(0);
    }

    public void setThousandSeparator(String locale) {
        config.setOption(BusinessConfigOption.THOUSAND_SEPARATOR.key, locale);
        saveConfig();
    }

    public String getHostEJBAddress() {
        String host = config.getOption(BusinessConfigOption.HOST_EJB_ADDRESS.getKey());
        return host;
    }

    public void setHostEJBAddress(String serverAddress) {
        config.setOption(BusinessConfigOption.HOST_EJB_ADDRESS.key, serverAddress);
        saveConfig();
    }

    public int getHostEjbPort() {
        int port = config.getOptionAsInt(BusinessConfigOption.HOST_EJB_PORT.getKey());
        return port;
    }

    public String getHostHttpAddress() {
        String port = config.getOption(BusinessConfigOption.HOST_HTTP_ADDRESS.getKey());
        return port;
    }

    public void setHostHttpAddress(String address) {
        config.setOption(BusinessConfigOption.HOST_HTTP_ADDRESS.key, address);
    }

    public int getHostHttpPort() {
        int port = config.getOptionAsInt(BusinessConfigOption.HOST_HTTP_PORT.getKey());
        return port;
    }

    public void setHttpPort(int port) {
        config.setOption(BusinessConfigOption.HOST_HTTP_PORT.key, String.valueOf(port));
        saveConfig();
    }

    protected void saveConfig() {
        config.saveForUser(
                BusinessConfigOption.HOST_EJB_ADDRESS.getKey(),
                BusinessConfigOption.HOST_EJB_PORT.getKey(),
                BusinessConfigOption.HOST_EJB_BIND.getKey(),
                BusinessConfigOption.HOST_HTTP_ADDRESS.getKey(),
                BusinessConfigOption.HOST_HTTP_PORT.getKey());
    }

    /**
     * Lima option definition.
     * <p/>
     * Contains all lima configuration key, with defaut value and
     * information for jaxx configuration frame ({@code #type},
     * {@code #transientBoolean}, {@code #finalBoolean}...)
     */
    public enum BusinessConfigOption implements LimaConfigOptionDef {

        CONFIG_FILE(ApplicationConfig.CONFIG_FILE_NAME, n("lima.configFileName.description"), "lima-business.config", String.class, true, true),

        DB_DIALECT("hibernate.dialect","", "org.hibernate.dialect.H2Dialect", String.class, false, false),
        DB_USER_NAME("hibernate.connection.username","", "sa", String.class, false, false),
        DB_PASSWORD("hibernate.connection.password","", "", String.class, false, false),
        DB_DRIVER("hibernate.connection.driver_class","", "org.h2.Driver", String.class, false, false),
        DB_URL("hibernate.connection.url","", "jdbc:h2:file:${lima.data.dir}/limadb", String.class, false, false),
        DB_BATCH_SIZE("hibernate.jdbc.batch_size","", "50", String.class, false, false),

        APPLICATION_VERSION("application.version", n("application.version"), null, String.class, false, false),
        DATA_DIR("lima.data.dir", n("lima.config.data.dir.description"), "${user.home}/.lima", File.class, false, false),
        BACKUP_DIR("lima.reports.dir",n("lima.config.reports.dir.description"),"${lima.data.dir}/backups", File.class, false, false),
        RULES_NATIONALTY("lima.rules", n("lima.config.rulesnationality.description"), FranceAccountingRules.class.getName(), String.class, false, false),

        HOST_EJB_ADDRESS("lima.host.ejb.address", n("lima.host.address.description"), "localhost", String.class, false, false),
        HOST_EJB_BIND("lima.host.ejb.bind", "", "0.0.0.0", String.class, false, false),
        HOST_EJB_PORT("lima.host.ejb.port", n("lima.host.ejb.port.description"), "4202", Integer.class, false, false),

        HOST_HTTP_ADDRESS("lima.host.http.address", n("lima.host.http.address.description"), "localhost", String.class, false, false),
        HOST_HTTP_PORT("lima.host.http.port", n("lima.host.http.port.description"), "5462", Integer.class, false, false),

        LOCAL("lima.config.local", t("lima.config.local.label"), n("lima.config.local.description"), Locale.FRANCE.toString() , Locale.class, false, false),
        SCALE("lima.data.bigDecimal.scale", t("lima.config.scale.label"), n("lima.config.scale.description"), "2", String.class, false, false),
        CURRENCY("lima.config.currency", t("lima.config.currency.label"), n("lima.config.currency.description"), "false", Boolean.class, false, false),
        DATE_FORMAT("lima.config.dateFormat", t("lima.config.dateFormat.label"), n("lima.config.dateFormat.description"), "DD/MM/YYYY", Boolean.class, false, false),
        DECIMAL_SEPARATOR("lima.data.bigDecimal.decimalSeparator", t("lima.config.decimalSeparator.label"), "", ",", Character.class, false, false),
        THOUSAND_SEPARATOR("lima.thousandSeparator", t("lima.config.thousandSeparator.label"), n("lima.config.thousandSeparator.description"), " ", Character.class, false, false);

        private final String key;

        protected final String label;

        private final String description;

        private String defaultValue;

        private final Class<?> type;

        private boolean transientBoolean;

        private boolean finalBoolean;

        BusinessConfigOption(String key, String label, String description, String defaultValue,
                             Class<?> type, boolean transientBoolean, boolean finalBoolean) {
            this.key = key;
            this.label = label;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.finalBoolean = finalBoolean;
            this.transientBoolean = transientBoolean;
        }

        BusinessConfigOption(String key, String description, String defaultValue,
                             Class<?> type, boolean transientBoolean, boolean finalBoolean) {
            this(key, null, description, defaultValue, type, transientBoolean, finalBoolean);
        }

        @Override
        public boolean isFinal() {
            return finalBoolean;
        }

        @Override
        public void setFinal(boolean finalBoolean) {
            this.finalBoolean = finalBoolean;
        }

        @Override
        public boolean isTransient() {
            return transientBoolean;
        }

        @Override
        public void setTransient(boolean transientBoolean) {
            this.transientBoolean = transientBoolean;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public String getDescription() {
            return t(description);
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public String getLabel() {
            return label;
        }
    }

}
