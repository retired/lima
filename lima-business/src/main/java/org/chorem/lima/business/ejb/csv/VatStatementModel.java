package org.chorem.lima.business.ejb.csv;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.VatStatementImport;
import org.chorem.lima.beans.VatStatementImportImpl;
import org.chorem.lima.entity.VatStatement;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 28/07/14.
 */
public class VatStatementModel extends AbstractLimaModel<VatStatementImport> implements ExportModel<VatStatement> {

    public VatStatementModel() {
        super(';');
        newMandatoryColumn("label", VatStatement.PROPERTY_LABEL);
        newOptionalColumn("header", VatStatement.PROPERTY_HEADER, O_N_PARSER);
        newOptionalColumn("accounts", VatStatement.PROPERTY_ACCOUNTS);
        newOptionalColumn("boxName", VatStatement.PROPERTY_BOX_NAME);
        newOptionalColumn("masterVATStatement", VatStatement.PROPERTY_MASTER_VAT_STATEMENT);
    }

    @Override
    public Iterable<ExportableColumn<VatStatement, Object>> getColumnsForExport() {
        ModelBuilder<VatStatement> modelBuilder = new ModelBuilder<>();

        modelBuilder.newColumnForExport("label", VatStatement.PROPERTY_LABEL);
        modelBuilder.newColumnForExport("header", VatStatement.PROPERTY_HEADER, O_N_FORMATTER);
        modelBuilder.newColumnForExport("accounts", VatStatement.PROPERTY_ACCOUNTS);
        modelBuilder.newColumnForExport("boxName", VatStatement.PROPERTY_BOX_NAME);
        modelBuilder.newColumnForExport("masterVATStatement", VatStatement.PROPERTY_MASTER_VAT_STATEMENT, MASTER_VAT_PATH_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public VatStatementImport newEmptyInstance() {
        VatStatementImport vatStatementImport = new VatStatementImportImpl();
        return vatStatementImport;
    }
}
