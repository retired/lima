package org.chorem.lima.business;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.base.Function;
import org.chorem.lima.entity.LimaCallaoTopiaApplicationContext;

import java.util.Properties;

/**
 * Created by davidcosse on 24/09/14.
 */
public class LimaConfigurationHelper {

    public static Function<Properties, LimaCallaoTopiaApplicationContext> getCreateTopiaContextFunction() {
        return input -> {
            LimaCallaoTopiaApplicationContext result = new LimaCallaoTopiaApplicationContext(input);
            return result;
        };
    }
}
