/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import org.chorem.lima.business.AccountingRules;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.exceptions.NotLockedClosedPeriodicEntryBooksException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.ClosedPeriodicEntryBookTopiaDao;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialPeriodTopiaDao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.util.Date;
import java.util.List;

/**
 * Gestion des périodes intermédiaires durant l'exercice.
 * Chaque timeSpan sera fixe, et devra correspondre à un mois complet.
 *
 * @author Rémi Chapelet
 */
@Stateless
@Remote(FinancialPeriodService.class)
@TransactionAttribute
public class FinancialPeriodServiceImpl extends AbstractLimaService implements FinancialPeriodService {

    /** @return all financial period */
    @Override
    public List<FinancialPeriod> getAllFinancialPeriods() {

        FinancialPeriodTopiaDao financialPeriodTopiaDao = getDaoHelper().getFinancialPeriodDao();
        List<FinancialPeriod> result = financialPeriodTopiaDao.findAllOrderByBeginDate();

        return result;
    }

    @Override
    public FinancialPeriod getFinancialForDate(Date date) {

        FinancialPeriodTopiaDao financialPeriodTopiaDao = getDaoHelper().getFinancialPeriodDao();
        FinancialPeriod result = financialPeriodTopiaDao.findByDate(date);

        return result;
    }

    @Override
    public FinancialPeriod getFinancialPeriodIfExistsByDate(Date date) {
        FinancialPeriodTopiaDao financialPeriodTopiaDao = getDaoHelper().getFinancialPeriodDao();
        FinancialPeriod result = financialPeriodTopiaDao.findByDateIfExists(date);

        return result;
    }

    /** @return all financial period from a fiscal period */
    @Override
    public List<FinancialPeriod> getFinancialPeriodsWithBeginDateWithin(Date beginDateFirst, Date endDateLast) {

        FinancialPeriodTopiaDao financialPeriodTopiaDao = getDaoHelper().getFinancialPeriodDao();
        List<FinancialPeriod> result = financialPeriodTopiaDao.findForBeginDateBetween(beginDateFirst, endDateLast);

        return result;
    }

    @Override
    public ClosedPeriodicEntryBook blockClosedPeriodicEntryBook(ClosedPeriodicEntryBook closedPeriodicEntryBook) throws UnbalancedFinancialTransactionsException, UnfilledEntriesException, WithoutEntryBookFinancialTransactionsException, NotLockedClosedPeriodicEntryBooksException {
        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        ClosedPeriodicEntryBook result;
        // check rules before create the account
        accountingRules.blockClosedPeriodicEntryBookRules(closedPeriodicEntryBook);

        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao =
                getDaoHelper().getClosedPeriodicEntryBookDao();

        // reload object in current transaction
        result = closedPeriodicEntryBookTopiaDao.forTopiaIdEquals(
                        closedPeriodicEntryBook.getTopiaId()).findUnique();

        result.setLocked(true);
        result = closedPeriodicEntryBookTopiaDao.update(result);
        
        return result;
    }

    @Override
    public ClosedPeriodicEntryBook getClosedPeriodicEntryBook(EntryBook entryBook,
                                                              FinancialPeriod financialPeriod) {
        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao = getDaoHelper().getClosedPeriodicEntryBookDao();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = closedPeriodicEntryBookTopiaDao.findByEntryBookAndFinancialPeriod(entryBook, financialPeriod);

        return closedPeriodicEntryBook;
    }

    @Override
    public List<ClosedPeriodicEntryBook> getAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriod() {

        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao = getDaoHelper().getClosedPeriodicEntryBookDao();
        List<ClosedPeriodicEntryBook> result = closedPeriodicEntryBookTopiaDao.findAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriod();

        return result;
    }
}
