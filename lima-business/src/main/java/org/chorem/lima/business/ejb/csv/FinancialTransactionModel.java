package org.chorem.lima.business.ejb.csv;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

/**
 * Created by davidcosse on 03/06/14.
 */
public class FinancialTransactionModel extends AbstractLimaModel<FinancialTransaction> implements ExportModel<FinancialTransaction> {

    protected final EntryBookService entryBookService;

    public FinancialTransactionModel(EntryBookService entryBookService) {
        super(';');
        this.entryBookService = entryBookService;
        newMandatoryColumn("id", FinancialTransaction.PROPERTY_TOPIA_ID);
        newOptionalColumn("transactionDate", FinancialTransaction.PROPERTY_TRANSACTION_DATE, DATE_PARSER);
        newOptionalColumn("entryBook", FinancialTransaction.PROPERTY_ENTRY_BOOK, ENTRY_BOOK_CODE_TO_ENTRY_BOOK_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<FinancialTransaction, Object>> getColumnsForExport() {
        ModelBuilder<FinancialTransaction> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("id", FinancialTransaction.PROPERTY_TOPIA_ID);
        modelBuilder.newColumnForExport("transactionDate", FinancialTransaction.PROPERTY_TRANSACTION_DATE, DATE_FORMATTER);
        modelBuilder.newColumnForExport("entryBook", FinancialTransaction.PROPERTY_ENTRY_BOOK, ENTRY_BOOK_TO_ENTRY_BOOK_CODE_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public FinancialTransactionImpl newEmptyInstance() {
        return new FinancialTransactionImpl();
    }

    protected ValueParser<EntryBook> ENTRY_BOOK_CODE_TO_ENTRY_BOOK_PARSER = new ValueParser<EntryBook>() {

        @Override
        public EntryBook parse(String value) {
            EntryBook result;
            if (StringUtils.isNotBlank(value)) {
                result = entryBookService.getEntryBookByCode(value);
            } else {
                result = null;
            }
            return result;
        }
    };

    protected static final ValueFormatter<EntryBook> ENTRY_BOOK_TO_ENTRY_BOOK_CODE_FORMATTER = new ValueFormatter<EntryBook>() {
        @Override
        public String format(EntryBook value) {
            String result;
            if (value != null) {
                result = value.getCode();
            } else {
                result = "";
            }
            return result;
        }
    };

}
