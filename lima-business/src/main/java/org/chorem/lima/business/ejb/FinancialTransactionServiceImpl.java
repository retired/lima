/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.beans.FinancialTransactionCondition;
import org.chorem.lima.beans.LetteringFilter;
import org.chorem.lima.business.AccountingRules;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.UnbalancedEntriesException;
import org.chorem.lima.business.utils.LetteringComparator;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.EntryTopiaDao;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionImpl;
import org.chorem.lima.entity.FinancialTransactionTopiaDao;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.filter.FilterGenerator;
import org.chorem.lima.filter.FinancialTransactionFilter;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

import static org.nuiton.i18n.I18n.t;

/**
 * Cette classe permet la création d'une transaction comptable dans l'application.
 * Toute action sur une transaction entraîne automatiquement une création de log.
 * Une transaction est composée d'entrées comptables.
 * Les actions sur les transactions sont soumises au statut de celle-ci, si elle
 * est dans une période bloquée ou non.
 *
 * @author Rémi Chapelet
 */
@Stateless
@Remote(FinancialTransactionService.class)
@TransactionAttribute
public class FinancialTransactionServiceImpl extends AbstractLimaService implements FinancialTransactionService {

    protected static final Log log = LogFactory.getLog(FinancialTransactionServiceImpl.class);
    public static final String EXTOURNE = "EXT";

    @EJB
    protected AccountService accountService;

    @EJB
    protected EntryBookService entryBookService;

    @EJB
    protected FinancialTransactionService financialTransactionService;

    public static final Function<String, String> LETTERS_AFTER = new Function<String, String>() {

        @Override
        public String apply(String letters) {
            String letterAfter;

        /* "" -> "A"  */
            if (StringUtils.isEmpty(letters)) {

                letterAfter = "A";

            } else {
                char endChar = letters.charAt(letters.length() - 1);

                if (endChar < 'Z') {

                /* si la lettre n'est pas un 'Z' on ajout a la reste de la chaine le caratère suivant */
                    letterAfter = letters.substring(0, letters.length() -1) + ((char) (endChar + 1));
                } else {
                /* si non on réitére sur le reste de la chaine en ajoutant "A" a la nouvelle chaine */
                    letterAfter = LETTERS_AFTER.apply(letters.substring(0, letters.length() - 1)) + 'A';

                }

            }
            return letterAfter;
        }
    };

    @Override
    public FinancialTransaction createNewFinancialTransaction(){
        FinancialTransactionTopiaDao financialtransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        FinancialTransaction financialTransaction = financialtransactionTopiaDao.newInstance();
        financialTransaction.setEntry(Lists.newArrayList());
        return financialTransaction;
    }

    @Override
    public Entry createNewEntry(){
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        Entry result = entryTopiaDao.newInstance();
        return result;
    }

    @Override
    public Iterable<FinancialTransaction> persistImportedFinancialTransactions(Collection<FinancialTransaction> financialTransactions) {
        FinancialTransactionTopiaDao financialtransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();

        Iterable<FinancialTransaction> result = financialtransactionTopiaDao.createAll(financialTransactions);

        return result;
    }

    @Override
    public FinancialTransaction createFinancialTransactionWithEntries(FinancialTransaction transaction, Collection<Entry> financialTransactionEntries) throws AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException {

        checkTransactionIsValid(transaction);

        FinancialTransactionTopiaDao financialtransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();

        FinancialTransaction newTransaction = getNewFinancialTransaction(transaction);

        FinancialTransaction result = financialtransactionTopiaDao.create(newTransaction);

        addNewFinancialTransactionEntries(financialTransactionEntries, result);

        result = financialtransactionTopiaDao.update(result);

        return result;
    }

    protected void checkTransactionIsValid(FinancialTransaction transaction) throws BeforeFirstFiscalPeriodException, AfterLastFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException {
        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();
        //check if the financial period is blocked
        accountingRules.createFinancialTransactionRules(transaction);
    }

    protected FinancialTransaction getNewFinancialTransaction(FinancialTransaction transaction) {
        FinancialTransaction newTransaction = new FinancialTransactionImpl();
        newTransaction.setEntryBook(transaction.getEntryBook());
        newTransaction.setTransactionDate(transaction.getTransactionDate());
        newTransaction.setEntry(Lists.newArrayList());
        return newTransaction;
    }

    protected void addNewFinancialTransactionEntries(Collection<Entry> financialTransactionEntries, FinancialTransaction result) {
        if (financialTransactionEntries == null || financialTransactionEntries.isEmpty()) {
            // Default One
            Entry newEntry = new EntryImpl();
            newEntry.setFinancialTransaction(result);

            List<Entry> savedEntry = new ArrayList<>();
            savedEntry.add(newEntry);
            result.addAllEntry(savedEntry);

        } else {

            List<Entry> newEntries = new ArrayList<>();
            for (Entry entry : financialTransactionEntries) {
                Entry newEntry = new EntryImpl();
                newEntry.setFinancialTransaction(result);
                newEntry.setVoucher(entry.getVoucher());
                newEntry.setAccount(entry.getAccount());
                newEntry.setDescription(entry.getDescription());
                newEntry.setAmount(entry.getAmount());
                newEntry.setDebit(entry.isDebit());
                newEntries.add(newEntry);
            }
            result.addAllEntry(newEntries);
        }
    }

    @Override
    public FinancialTransaction createFinancialTransactionSkeleton(FinancialTransaction financialtransaction, Boolean validAccountingRules)
            throws LockedFinancialPeriodException, LockedEntryBookException, BeforeFirstFiscalPeriodException, AfterLastFiscalPeriodException {

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        if (validAccountingRules) {
            //check if the financial period is blocked
            accountingRules.createFinancialTransactionRules(financialtransaction);
        }

        FinancialTransactionTopiaDao financialtransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();

        if (financialtransaction.getEntry() == null) {
            financialtransaction.setEntry(Lists.newArrayList());
        }

        FinancialTransaction result = financialtransactionTopiaDao.create(financialtransaction);

        return result;
    }

    /**
     * Return the list of all financial transaction from two dates.
     */
    @Override
    public List<FinancialTransaction> getAllFinancialTransactions(
            Date beginDate, Date endDate) {

        FinancialTransactionTopiaDao transactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        List<FinancialTransaction> financialTransactions = transactionTopiaDao.findAllByDates(beginDate, endDate);

        return financialTransactions;
    }

    /** Find the last three letters used
     * and increment them
     * */
    @Override
    public String getNextLetters() {

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();

        List<String> letters = new ArrayList<>(entryTopiaDao.findLetters());
        String lastActualLetters = findLastLetter(letters);

        String nextLetters = LETTERS_AFTER.apply(lastActualLetters);

        return nextLetters;
    }

    @Override
    public List<String> getAllLetters() {

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();

        List<String> letters = new ArrayList<>(entryTopiaDao.findLetters());

        return letters;
    }

    @Override
    public Entry[] getEntriesFromEqualizing(Entry firstEntrySelected, Entry secondEntrySelected)
            throws LockedFinancialPeriodException, LockedEntryBookException, AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException {

        Entry newSameAccountEntry = null;
        Entry newCostOrProductEntry = null;
        BigDecimal firstSelectedEntryAmount = firstEntrySelected.getAmount();
        BigDecimal secondSelectedEntryAmount = secondEntrySelected.getAmount();

        if ( (firstSelectedEntryAmount.compareTo(BigDecimal.ZERO) != 0 && secondSelectedEntryAmount.compareTo(BigDecimal.ZERO) != 0)
                && !firstSelectedEntryAmount.equals(secondSelectedEntryAmount)) {

            /*Calculation of result with it
            * Tab : 0 : debit
            *       1 : credit*/
            BigDecimal[] firstEntryDebitCredit = debitCreditCalculation(firstEntrySelected);
            BigDecimal[] secondEntryDebitCredit = debitCreditCalculation(secondEntrySelected);

            /*Subtraction between debit and credit of first and second selected line*/
            BigDecimal firstEntryDebitSubtract = firstEntryDebitCredit[0].subtract(secondEntryDebitCredit[1]);
            BigDecimal secondEntryDebitSubtract = secondEntryDebitCredit[0].subtract(firstEntryDebitCredit[1]);

            /*Create handles of new futures entries*/
            Entry sameAccountEntry;
            Entry costOrProductEntry;

            /*Set result in the amount of the new entries*/
            FinancialTransaction newFinancialTransaction = createNewExtourneFinantialTransaction(firstEntrySelected, secondEntrySelected);

            if (firstEntryDebitSubtract.compareTo(BigDecimal.ZERO) != 0) {
                sameAccountEntry = copyEntryWithoutAmount(firstEntrySelected, newFinancialTransaction);
                costOrProductEntry = copyEntryWithoutAmount(firstEntrySelected, newFinancialTransaction);
                amountsCalculation(firstEntryDebitSubtract, sameAccountEntry, costOrProductEntry);
            } else {
                sameAccountEntry = copyEntryWithoutAmount(secondEntrySelected, newFinancialTransaction);
                costOrProductEntry = copyEntryWithoutAmount(secondEntrySelected, newFinancialTransaction);
                amountsCalculation(secondEntryDebitSubtract, sameAccountEntry, costOrProductEntry);
            }

            String accountRegularization = t("lima.lettering.accountRegularization");

            if (log.isDebugEnabled()) {
                log.debug("accountRegularization : " + accountRegularization);
            }

            sameAccountEntry.setDescription(accountRegularization);
            costOrProductEntry.setDescription(accountRegularization);

            /*Save in the DB the new entries*/
            newSameAccountEntry = createEntry(sameAccountEntry);
            newCostOrProductEntry = createEntry(costOrProductEntry);

        }

        Entry [] entries = {newSameAccountEntry, newCostOrProductEntry};
        return entries;
    }

    protected FinancialTransaction createNewExtourneFinantialTransaction(Entry firstEntrySelected, Entry secondEntrySelected) throws AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException {
        FinancialTransaction newFinancialTransaction = new FinancialTransactionImpl();
        EntryBook entryBook = entryBookService.getEntryBookByCode(EXTOURNE);
        if (entryBook == null) {
            entryBook = firstEntrySelected.getFinancialTransaction().getEntryBook();
        }
        newFinancialTransaction.setEntryBook(entryBook);

        Date lastTransactionDate = secondEntrySelected.getFinancialTransaction().getTransactionDate().after(firstEntrySelected.getFinancialTransaction().getTransactionDate()) ?
                secondEntrySelected.getFinancialTransaction().getTransactionDate() : firstEntrySelected.getFinancialTransaction().getTransactionDate();
        newFinancialTransaction.setTransactionDate(lastTransactionDate);
        newFinancialTransaction = financialTransactionService.createFinancialTransactionWithEntries(newFinancialTransaction, newFinancialTransaction.getEntry());
        return newFinancialTransaction;
    }

    /**Debit and credit calculation for one entry, with its amount
     * @param entry Entry for calculation
     * @return table of two big decimal, representing respectively debit and credit
     * */
    protected BigDecimal[] debitCreditCalculation(Entry entry) {

        boolean firstEntryDebit = entry.isDebit();
        BigDecimal firstEntryAmount = entry.getAmount();

        BigDecimal debitVal = firstEntryDebit ? firstEntryAmount : BigDecimal.ZERO;
        BigDecimal creditVal = firstEntryDebit ? BigDecimal.ZERO : firstEntryAmount;

        BigDecimal[] debitCredit = {debitVal, creditVal};

        return debitCredit;
    }

    /**Copy parametrised entry, without amount*/
    protected Entry copyEntryWithoutAmount(Entry entryToCopy, FinancialTransaction newFinancialTransaction) {
        Entry copiedEntry = new EntryImpl();

        newFinancialTransaction.addEntry(copiedEntry);
        copiedEntry.setFinancialTransaction(newFinancialTransaction);
        copiedEntry.setAccount(entryToCopy.getAccount());
        copiedEntry.setDescription(entryToCopy.getDescription());
        copiedEntry.setDetail(entryToCopy.getDetail());
        copiedEntry.setVoucher(entryToCopy.getVoucher());

        return  copiedEntry;
    }

    /**
     *Calculation of amount for the new entries created
     * and determine for the second if account is in costs (658)
     * or products (758)
     * @param resultOfFirstSecondEntrySubtraction difference between the two old entries
     * @param sameAccountEntry first new entry created with the same account
     * @param costOrProductEntry second new entry created with account 658 or 758
     * */
    protected void amountsCalculation( BigDecimal resultOfFirstSecondEntrySubtraction, Entry sameAccountEntry, Entry costOrProductEntry) {
        sameAccountEntry.setAmount(resultOfFirstSecondEntrySubtraction.abs());
        costOrProductEntry.setAmount(resultOfFirstSecondEntrySubtraction.abs());

        /*-1 for less than 0 : credit*/
        if (resultOfFirstSecondEntrySubtraction.compareTo(BigDecimal.ZERO) == -1) {
            sameAccountEntry.setDebit(true);
            costOrProductEntry.setDebit(false);
            costOrProductEntry.setAccount(accountService.getAccountByNumber("758"));
        } else { /*Greater than 0 : debit*/
            sameAccountEntry.setDebit(false);
            costOrProductEntry.setDebit(true);
            costOrProductEntry.setAccount(accountService.getAccountByNumber("658"));
        }
    }

    @Override
    public List<Entry> addLetter(List<Entry> entries) throws UnbalancedEntriesException, LockedEntryBookException {

        List<Entry> oldEntries = Lists.newLinkedList();

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();

        for (Entry entry : entries) {

            Entry oldEntry = entryTopiaDao.forTopiaIdEquals(entry.getTopiaId()).findUnique();

            oldEntries.add(oldEntry);

        }

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        //check rules
        accountingRules.addLetter(oldEntries);

        String nextLetters = getNextLetters();

        for (Entry entry : oldEntries) {

            entry.setLettering(nextLetters);

            //update entry
            entryTopiaDao.update(entry);

        }

        return oldEntries;
    }

    @Override
    public List<Entry> removeLetter(String letter) {

        List<Entry> entries = Lists.newArrayList();

        if (StringUtils.isNotBlank(letter)) {

            EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();

            entries = entryTopiaDao.forLetteringEquals(letter).findAll();

            for (Entry entry : entries) {

                entry.setLettering("");

                //update entry
                entryTopiaDao.update(entry);

            }
        }

        return entries;
    }

    @Override
    public void persistImportedEntries(Collection<Entry> entries) {
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        entryTopiaDao.createAll(entries);
    }

    @Override
    public Iterable<FinancialTransaction> updateAllImportedFinancialTransactions(Iterable<FinancialTransaction> savedTransactions) {
        FinancialTransactionTopiaDao transactionDao = getDaoHelper().getFinancialTransactionDao();
        Iterable<FinancialTransaction> result = transactionDao.updateAll(savedTransactions);
        return result;
    }

    public String findLastLetter(List<String> letters) {

        String result;

        /** filtre sur les valeurs potentiellement généré */
        Collection<String> generatedLetters = Collections2.filter(letters, new Predicate<String>() {
            @Override
            public boolean apply(String input) {
                return input != null &&
                        input.matches("[A-Z]+");
            }
        });

        if (generatedLetters.isEmpty()) {
            result = "";

        } else {
            result = Collections.max(generatedLetters, new LetteringComparator());
        }
        return result;
    }

    /**
     * Return the list of all financial transaction of a financial period.
     */
    @Override
    public List<FinancialTransaction> getAllFinancialTransactions(
            FinancialPeriod period) {
        return getAllFinancialTransactions(period, null);
    }

    /**
     * Return the list of all financial transaction of a fiscal period.
     */
    @Override
    public List<FinancialTransaction> getAllFinancialTransactions(
            FiscalPeriod period) {
        FinancialTransactionTopiaDao transactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        List<FinancialTransaction> financialTransactions = transactionTopiaDao.findAllByDates(period.getBeginDate(), period.getEndDate());

        return financialTransactions;
    }

    /**
     * Return the list of all financial transaction of a financial period and an entrybook.
     */
    @Override
    public List<FinancialTransaction> getAllFinancialTransactions(FinancialPeriod financialPeriod, EntryBook entryBook) {

        List<FinancialTransaction> financialTransactions;

        FinancialTransactionTopiaDao transactionTopiaDao = getDaoHelper().getFinancialTransactionDao();

        if (entryBook != null) {
            financialTransactions = transactionTopiaDao.findAllByDates(financialPeriod.getBeginDate(),
                financialPeriod.getEndDate(), entryBook);
        } else {
            financialTransactions = transactionTopiaDao.findAllByDates(financialPeriod.getBeginDate(),
                    financialPeriod.getEndDate());
        }

        return financialTransactions;
    }

    /**
     * Get unbalanced financialtransaction from selected fiscalperiod.
     */
    @Override
    public List<FinancialTransaction> getAllInexactFinancialTransactions(FiscalPeriod fiscalPeriod) {

        FinancialTransactionTopiaDao financialTransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();

        List<FinancialTransaction> result = financialTransactionTopiaDao.getAllIncorrectTransaction(fiscalPeriod.getBeginDate(),
                fiscalPeriod.getEndDate(), null);

        return result;
    }

    /**
     * Get balanced financial transaction from selected fiscal period
     *
     * @param fiscalPeriod The fiscal period for financial transaction
     * @return the balanced financial transaction
     * @throws LimaException
     */
    @Override
    public List<FinancialTransaction> getAllFinancialTransactionsBalanced(FiscalPeriod fiscalPeriod) {

        FinancialTransactionTopiaDao financialTransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();

        List<FinancialTransaction> result = financialTransactionTopiaDao.getAllBalancedTransaction(fiscalPeriod.getBeginDate(),
                fiscalPeriod.getEndDate(), null);

        return result;
    }

     @Override
     public List<Entry> getAllEntrieByDatesAndAccountAndLettering(LetteringFilter filter) {

         EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
         List<Entry> entries = entryTopiaDao.findAllEntryByFilter(filter);

         if (log.isDebugEnabled()) {
             log.debug("Entries size : " + entries.size());
         }

         return entries;
    }

    @Override
    public List<Entry> getAllUnlockEntriesByFilter(LetteringFilter filter) {
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        List<Entry> entries = entryTopiaDao.findAllUnlockEntriesByFilter(filter);

        if (log.isDebugEnabled()) {
            log.debug("Entries size : " + entries.size());
        }

        return entries;
    }

    @Override
    public List<Object[]> getAccountEntriesDebitCreditFromIncludingToExcludingPeriod(LetteringFilter filter) {
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        List<Object[]> result = entryTopiaDao.getAccountEntriesDebitCreditFromIncludingToExcludingPeriod(filter);
        return result;
    }

    @Override
    public List<Object[]> getAccountEntriesDebitCreditFromIncludingToIncludingPeriod(LetteringFilter filter) {
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        List<Object[]> result = entryTopiaDao.getAccountEntriesDebitCreditFromIncludingToIncludingPeriod(filter);
        return result;
    }

    @Override
    public Entry getLastEntry(FinancialTransaction financialTransaction) {

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        Entry lastEntry = entryTopiaDao.getLastEntry(financialTransaction);

        return lastEntry;
    }

    /**
     * Method used by update entry and remove entry for update amounts.
     */
    @Override
    public void updateFinancialTransaction(FinancialTransaction financialTransaction)
            throws AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException {

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        FinancialTransactionTopiaDao transactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        FinancialTransaction financialTransactionOld = transactionTopiaDao.forTopiaIdEquals(financialTransaction.getTopiaId()).findUnique();

        accountingRules.updateFinancialTransactionRules(financialTransaction, financialTransactionOld);

        financialTransactionOld.setEntryBook(financialTransaction.getEntryBook());
        financialTransactionOld.setTransactionDate(financialTransaction.getTransactionDate());
        transactionTopiaDao.update(financialTransactionOld);
    }

    /**
     * delete financial period
     * call accounting rules
     */
    @Override
    public void removeFinancialTransaction(FinancialTransaction financialTransaction)
            throws LockedFinancialPeriodException, LockedEntryBookException {

        // check if the financial period is blocked
        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();
        accountingRules.deleteFinancialTransactionRules(financialTransaction);

        FinancialTransactionTopiaDao transactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        FinancialTransaction financialTransactionOld = transactionTopiaDao.forTopiaIdEquals(financialTransaction.getTopiaId()).findUnique();
        transactionTopiaDao.delete(financialTransactionOld);
    }

    @Override
    public FinancialTransaction getFinancialTransactionWithId(String id) {
        FinancialTransactionTopiaDao transactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        FinancialTransaction result = transactionTopiaDao.forTopiaIdEquals(id).findUnique();
        return result;
    }

    @Override
    public void createdImportedEntry(Entry entry) {
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        entryTopiaDao.create(entry);
    }

    @Override
    public Iterable<Entry> createAllEntries(Collection<Entry> entries, Boolean checkRules) throws LockedFinancialPeriodException, LockedEntryBookException {
        if (checkRules) {
            AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();
            //check if the financial period is blocked
            accountingRules.createEntriesRules(entries);
        }

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        Iterable<Entry> newEntries = entryTopiaDao.createAll(entries);

        if (log.isDebugEnabled()) {
            log.debug("Nex ");
        }
        return newEntries;
    }

    @Override
    public Entry createEntry(Entry entry) throws LockedFinancialPeriodException, LockedEntryBookException {
        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        //check if the financial period is blocked
        accountingRules.createEntryRules(entry);

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        Entry newEntry = entryTopiaDao.create(entry);

        if (log.isDebugEnabled()) {
            log.debug("Nex ");
        }
        return newEntry;
    }

    /**
     * update entry, calculate amount of the financial transaction.
     */
    @Override
    public void updateEntry(Entry entry) throws LockedEntryBookException, LockedFinancialPeriodException {

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();

        Entry entryOld = entryTopiaDao.forTopiaIdEquals(entry.getTopiaId()).findUnique();

        //check rules
        accountingRules.updateEntryRules(entry, entryOld);

        final Binder<Entry, Entry> binder = BinderFactory.newBinder(Entry.class, Entry.class);
        binder.copyExcluding(entry, entryOld, Entry.PROPERTY_TOPIA_ID, Entry.PROPERTY_TOPIA_CREATE_DATE);
        //update entry
        entryTopiaDao.update(entryOld);

    }

    /**
     * Remove entry, update amounts for the financial transaction.
     */
    @Override
    public void removeEntry(Entry entry) throws LockedFinancialPeriodException, LockedEntryBookException {

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        //check if the financial period is blocked
        accountingRules.deleteEntryRules(entry);

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        Entry entryOld = entryTopiaDao.forTopiaIdEquals(entry.getTopiaId()).findUnique();
        entryOld.getFinancialTransaction().removeEntry(entryOld);
        entryTopiaDao.delete(entryOld);

    }

    @Override
    public List<FinancialTransaction> searchFinancialTransaction(FinancialTransactionCondition financialTransactionCondition) {

        FilterGenerator generator = new FilterGenerator();
        financialTransactionCondition.accept(generator);
        FinancialTransactionFilter filter = generator.getFilter();

        // next on financial transaction api
        FinancialTransactionTopiaDao financialTransactionTopiaDao = getDaoHelper().getFinancialTransactionDao();
        List<FinancialTransaction> result = financialTransactionTopiaDao.searchFinancialTransaction(filter);

        if (log.isDebugEnabled()) {
            log.debug("Size of results list : " + result.size());
        }

        return result;
    }

}
