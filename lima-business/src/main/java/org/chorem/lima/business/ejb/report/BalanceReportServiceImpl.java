package org.chorem.lima.business.ejb.report;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.beans.BalanceAccount;
import org.chorem.lima.beans.BalanceAccountImpl;
import org.chorem.lima.beans.BalanceTrial;
import org.chorem.lima.beans.BalanceTrialImpl;
import org.chorem.lima.beans.DocumentReport;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.report.BalanceReportService;
import org.chorem.lima.business.ejb.AbstractLimaService;
import org.chorem.lima.business.utils.AccountComparator;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountTopiaDao;
import org.chorem.lima.entity.EntryTopiaDao;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 04/11/14.
 */
@Stateless
@Remote(BalanceReportService.class)
@TransactionAttribute
public class BalanceReportServiceImpl extends AbstractLimaService implements BalanceReportService {

    public static final String GLOBAL_401_ACCOUNT_NUMBER = "4010000";
    public static final String GLOBAL_411_ACCOUNT_NUMBER = "4110000";
    @EJB
    protected FinancialPeriodService financialPeriodService;

    @EJB
    protected AccountService accountService;

    @EJB
    protected IdentityService identityService;

    protected static final Integer ACCOUNT_NUMBER_SIZE = 6;
    protected static final String GROUP_401 = "401";
    protected static final String GROUP_411 = "411";

    protected static final Predicate<BalanceAccount> HAS_NO_ENTRY = new Predicate<BalanceAccount>() {

        @Override
        public boolean apply(BalanceAccount subClasses) {
            boolean result = BigDecimal.ZERO.equals(subClasses.getAmountDebit()) &&
                    BigDecimal.ZERO.equals(subClasses.getAmountCredit()) &&
                    BigDecimal.ZERO.equals(subClasses.getDebitBalance()) &&
                    BigDecimal.ZERO.equals(subClasses.getCreditBalance());
            return result;
        }
    };

    @Override
    public DocumentReport getGlobalBalanceDocumentReport(Date from, Date to, String selectedAccounts, DecimalFormat bigDecimalFormat, JasperReport mainAccountsJasperReport, JasperReport subAccountsJasperReport) {
        String reportName = t("lima-business.document.globalBalanceReport.title");
        DocumentReport documentReport = getBalanceDocumentReport(from, to, bigDecimalFormat, mainAccountsJasperReport, reportName);

        BalanceGlobalResult balanceGlobalResult = new BalanceGlobalResult().invoke();

        BalanceTrial balanceTrial = new BalanceTrialImpl();
        balanceTrial.setReportsDatas(new ArrayList<>());

        List<Account> accounts = getAccounts(selectedAccounts);

        Collection<BalanceAccount> returnedAccounts = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(accounts)) {
            Collections.sort(accounts, new AccountComparator());

            setDocumentReportHeaderSelectedAccounts(documentReport, accounts);

            HashMap<String, BalanceAccountImpl> accountsByClasses = new HashMap<>();

            //for each account create a balance sheet with a ReportsDatas
            for (Account account : accounts) {
                String accountClass = String.valueOf(account.getAccountNumber().charAt(0));

                // all results for a account classes
                BalanceAccountImpl accountSubAccounts = getBalanceAccountForAccountClass(bigDecimalFormat, subAccountsJasperReport, returnedAccounts, accountsByClasses, accountClass);

                // compute balance for the account classes
                BalanceAccount subClassAccount = computeSubAccountBalance(accountClass, account, from, to, bigDecimalFormat);

                // only add it if there are results
                if (subClassAccount != null) {
                    accountSubAccounts.addSubAccount(subClassAccount);

                    balanceGlobalResult.addSubClassAccounts(subClassAccount);
                }
            }
        }

        Iterables.removeIf(returnedAccounts, HAS_NO_ENTRY);
        documentReport.addAllMainAccounts(returnedAccounts);

        setDocumentReportGlobalBalance(documentReport, balanceGlobalResult);

        return documentReport;
    }

    @Override
    public DocumentReport getGeneralBalanceDocumentReport(Date from, Date to, String selectedAccounts, DecimalFormat bigDecimalFormat, JasperReport mainAccountsJasperReport, JasperReport subAccountsJasperReport) {
        String reportName = t("lima-business.document.generalBalanceReport.title");
        DocumentReport documentReport = getBalanceDocumentReport(from, to, bigDecimalFormat, mainAccountsJasperReport, reportName);

        BalanceGlobalResult balanceGlobalResult = new BalanceGlobalResult().invoke();

        BalanceTrial balanceTrial = new BalanceTrialImpl();
        balanceTrial.setReportsDatas(new ArrayList<>());

        List<Account> accounts = getAccounts(selectedAccounts);

        Collection<BalanceAccount> returnedAccounts = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(accounts)) {
            Collections.sort(accounts, new AccountComparator());

            setDocumentReportHeaderSelectedAccounts(documentReport, accounts);

            HashMap<String, BalanceAccountImpl> accountsByClasses = new HashMap<>();

            BalanceAccount sold401 = null;
            BalanceAccount sold411 = null;

            //for each account create a balance sheet with a ReportsDatas
            for (Account account : accounts) {
                String accountClass = String.valueOf(account.getAccountNumber().charAt(0));

                // all results for a account classes
                BalanceAccountImpl accountSubAccounts = getBalanceAccountForAccountClass(bigDecimalFormat, subAccountsJasperReport, returnedAccounts, accountsByClasses, accountClass);

                // compute balance for the account classes
                BalanceAccount subClassAccount = computeSubAccountBalance(accountClass, account, from, to, bigDecimalFormat);

                // only add it if there are results
                if (subClassAccount != null) {
                    // we group Account 401 and 411 with there sub accounts
                    String accountNb = account.getAccountNumber();

                    BalanceAccount balanceAccount = null;
                    if ((accountNb.startsWith(GROUP_401)) || (accountNb.startsWith(GROUP_411))){
                        if ((accountNb.startsWith(GROUP_401))){
                            if (sold401 == null) {
                                sold401 = getGlobalBalance4x(bigDecimalFormat, t("lima-business.document.balance.globalSupplier"), GLOBAL_401_ACCOUNT_NUMBER);
                                accountSubAccounts.addSubAccount(sold401);
                            }
                            balanceAccount = sold401;
                        } else if((accountNb.startsWith(GROUP_411))) {
                            if (sold411 == null) {
                                sold411 = getGlobalBalance4x(bigDecimalFormat, t("lima-business.document.balance.globalClient"), GLOBAL_411_ACCOUNT_NUMBER);
                                accountSubAccounts.addSubAccount(sold411);
                            }
                            balanceAccount = sold411;
                        }

                        if (balanceAccount != null) {
                            // add amounts to global 4 account
                            addInWith(accountSubAccounts, subClassAccount);

                            // add amounts to 401 or 411 global amount
                            addInWith(balanceAccount, subClassAccount);

                            // add amounts to global result
                            balanceGlobalResult.addSubClassAccounts(subClassAccount);
                        }
                    } else {
                        accountSubAccounts.addSubAccount(subClassAccount);

                        balanceGlobalResult.addSubClassAccounts(subClassAccount);
                    }
                }
            }
        }

        Iterables.removeIf(returnedAccounts, HAS_NO_ENTRY);
        documentReport.addAllMainAccounts(returnedAccounts);

        setDocumentReportGlobalBalance(documentReport, balanceGlobalResult);

        return documentReport;
    }

    protected BalanceAccount getGlobalBalance4x(DecimalFormat bigDecimalFormat, String label, String accountNumber) {
        BalanceAccount sold401 = new BalanceAccountImpl();
        sold401.setLabel(label);
        sold401.setFormatter(bigDecimalFormat);
        sold401.setAccountNumber(accountNumber);
        return sold401;
    }

    protected List<Account> getAccounts(String selectedAccounts) {
        AccountTopiaDao accountTopiaDao = getDaoHelper().getAccountDao();
        List<Account> accounts;

        selectedAccounts = StringUtils.deleteWhitespace(selectedAccounts);
        // find all if none specified
        if (StringUtils.isBlank(selectedAccounts)) {
            accounts = accountTopiaDao.findAll();
        } else {
            accounts = accountService.stringToListAccounts(selectedAccounts);
        }
        return accounts;
    }

    protected void setDocumentReportGlobalBalance(DocumentReport documentReport, BalanceGlobalResult globalBalanceResult) {
        documentReport.setTotalDebit(globalBalanceResult.globalDebit);
        documentReport.setTotalCredit(globalBalanceResult.globalCredit);
        documentReport.setDebitBalance(globalBalanceResult.globalDebitBalance);
        documentReport.setCreditBalance(globalBalanceResult.globalCreditBalance);
    }

    protected DocumentReport getBalanceDocumentReport(Date from, Date to, DecimalFormat bigDecimalFormat, JasperReport mainAccountsJasperReport, String reportName) {
        DocumentReport documentReport = CommonsDocumentReport.getDocumentReport(reportName, from, to, mainAccountsJasperReport, bigDecimalFormat, identityService);

        documentReport.setColumnAccountTitle(t("lima-business.document.columnAccountTitle"));
        documentReport.setColumnBalanceForPeriodTitle(t("lima-business.document.columnBalanceForPeriodTitle"));
        documentReport.setColumnCreditTitle(t("lima-business.document.columnCreditTitle"));
        documentReport.setColumnDateTitle(t("lima-business.document.columnDateTitle"));
        documentReport.setColumnDebitTitle(t("lima-business.document.columnDebitTitle"));
        documentReport.setColumnDescriptionTitle(t("lima-business.document.columnDescriptionTitle"));
        documentReport.setColumnTotalForPeriodTitle(t("lima-business.document.columnTotalForPeriodTitle"));
        documentReport.setColumnVoucherTitle(t("lima-business.document.columnVoucherTitle"));

        documentReport.setLastPageColumnDescription(t("lima-business.document.balance.lastPageColumnDescription"));
        return documentReport;
    }

    protected void addInWith(BalanceAccount target, BalanceAccount toBeAdded) {
        target.setAmountDebit(target.getAmountDebit().add(toBeAdded.getAmountDebit()));
        target.setAmountCredit(target.getAmountCredit().add(toBeAdded.getAmountCredit()));
        target.setDebitBalance(target.getDebitBalance().add(toBeAdded.getDebitBalance()));
        target.setCreditBalance(target.getCreditBalance().add(toBeAdded.getCreditBalance()));
    }

    protected BalanceAccountImpl getBalanceAccountForAccountClass(DecimalFormat bigDecimalFormat, JasperReport subAccountsJasperReport, Collection<BalanceAccount> returnedAccounts, HashMap<String, BalanceAccountImpl> accountsByClasses, String accountClass) {
        BalanceAccountImpl accountSubAccounts = accountsByClasses.get(accountClass);
        if (accountSubAccounts == null) {
            accountSubAccounts = new BalanceAccountImpl();
            accountSubAccounts.setFormatter(bigDecimalFormat);
            accountSubAccounts.setSubReport(subAccountsJasperReport);
            accountSubAccounts.setSubAccounts(new ArrayList<>());
            accountsByClasses.put(accountClass, accountSubAccounts);
            returnedAccounts.add(accountSubAccounts);
        }
        return accountSubAccounts;
    }

    protected void setDocumentReportHeaderSelectedAccounts(DocumentReport documentReport, List<Account> accounts) {
        Account fromAccount = accounts.get(0);
        Account toAccount = accounts.get(accounts.size()-1);
        String fromAccountText = StringUtils.isNotBlank(fromAccount.getLabel()) ? fromAccount.getLabel() + " (" + fromAccount.getAccountNumber() + ")" : fromAccount.getAccountNumber();
        String toAccountText = StringUtils.isNotBlank(toAccount.getLabel()) ? toAccount.getLabel() + " (" + toAccount.getAccountNumber() + ")" : toAccount.getAccountNumber();
        documentReport.setHeaderSelectedAccounts(t("lima-business.document.headerSelectedAccounts", fromAccountText, toAccountText));
    }

    /**
     * Calculate all credit, debit and solde amounts for the balance
     * <p/>
     * Get all entries if true
     * @throws org.nuiton.topia.persistence.TopiaException
     */
    protected BalanceAccount computeSubAccountBalance(String mainAccountLabel, Account account, Date beginDate, Date endDate, DecimalFormat bigDecimalFormat) {

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();

        BigDecimal credit = BigDecimal.ZERO;
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal balance = BigDecimal.ZERO;

        List<Object[]> results = entryTopiaDao.getDebitCreditOfBalancedTransaction(account, beginDate, endDate);

        int nbAmount = results.size();
        if (nbAmount == 2) {
            debit = (BigDecimal) results.get(0)[1];
            credit = (BigDecimal) results.get(1)[1];
        }
        if (nbAmount == 1) {
            if ((Boolean) results.get(0)[0]) {
                debit = (BigDecimal) results.get(0)[1];
            } else {
                credit = (BigDecimal) results.get(0)[1];
            }
        }

        // set the amounts and sold
        //sold = debit - credit
        balance = balance.add(debit);
        balance = balance.subtract(credit);

        BalanceAccount balanceAccount = addSubAccountReport(mainAccountLabel, account, bigDecimalFormat, credit, debit, balance);

        return balanceAccount;
    }

    protected BalanceAccount addSubAccountReport(String mainAccountLabel, Account account, DecimalFormat bigDecimalFormat, BigDecimal credit, BigDecimal debit, BigDecimal balance) {
        BalanceAccount balanceAccount = null;
        if (BigDecimal.ZERO.compareTo(credit) != 0 || BigDecimal.ZERO.compareTo(debit) != 0) {
            balanceAccount = new BalanceAccountImpl();
            balanceAccount.setMainAccountLabel(mainAccountLabel);
            balanceAccount.setFormatter(bigDecimalFormat);

            String accountNumber = account.getAccountNumber();
            accountNumber = StringUtils.rightPad(accountNumber, ACCOUNT_NUMBER_SIZE - accountNumber.length(), '0');
            balanceAccount.setAccountNumber(accountNumber);
            balanceAccount.setLabel(account.getLabel());
            balanceAccount.setSubTotalForLabel(t("lima-business.document.subTotalFor"));
            balanceAccount.setTotalForLabel(t("lima-business.document.totalFor"));

            if (balance.compareTo(BigDecimal.ZERO) == 1) {
                balanceAccount.setDebitBalance(balance.abs());
            } else {
                balanceAccount.setCreditBalance(balance.abs());
            }

            balanceAccount.setAmountCredit(credit);
            balanceAccount.setAmountDebit(debit);
        }
        return balanceAccount;
    }

    private class BalanceGlobalResult {
        private BigDecimal globalDebit;
        private BigDecimal globalCredit;
        private BigDecimal globalDebitBalance;
        private BigDecimal globalCreditBalance;

        public BigDecimal getGlobalDebit() {
            return globalDebit;
        }

        public BigDecimal getGlobalCredit() {
            return globalCredit;
        }

        public BigDecimal getGlobalDebitBalance() {
            return globalDebitBalance;
        }

        public BigDecimal getGlobalCreditBalance() {
            return globalCreditBalance;
        }

        public BalanceGlobalResult invoke() {
            globalDebit = BigDecimal.ZERO;
            globalCredit = BigDecimal.ZERO;
            globalDebitBalance = BigDecimal.ZERO;
            globalCreditBalance = BigDecimal.ZERO;
            return this;
        }

        public void addSubClassAccounts(BalanceAccount subClassAccount) {
            globalDebit = globalDebit.add(subClassAccount.getAmountDebit());
            globalCredit = globalCredit.add(subClassAccount.getAmountCredit());
            globalDebitBalance = globalDebitBalance.add(subClassAccount.getDebitBalance());
            globalCreditBalance = globalCreditBalance.add(subClassAccount.getCreditBalance());
        }

    }
}
