/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.LimaInterceptor;
import org.chorem.lima.entity.LimaCallaoTopiaDaoSupplier;

/**
 * Abstract code for all ejb services (get context, catch, finally...).
 *
 * @author chatellier
 * @version $Revision$
 */
public abstract class AbstractLimaService {

    /** Logger. */
    protected static final Log log = LogFactory.getLog(AbstractLimaService.class);

    /**
     * Return Dao helper to use in current thread.
     * Defined by {@link LimaInterceptor}.
     * 
     * @return dao helper
     */
    protected static LimaCallaoTopiaDaoSupplier getDaoHelper() {
        return LimaInterceptor.DAO_HELPER.get();
    }
}
