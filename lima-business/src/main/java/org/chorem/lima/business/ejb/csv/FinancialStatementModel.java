package org.chorem.lima.business.ejb.csv;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.beans.FinancialStatementImport;
import org.chorem.lima.beans.FinancialStatementImportImpl;
import org.chorem.lima.entity.FinancialStatement;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 28/07/14.
 */
public class FinancialStatementModel extends AbstractLimaModel<FinancialStatementImport> implements ExportModel<FinancialStatement> {

    public FinancialStatementModel() {
        super(';');
        newMandatoryColumn("label", FinancialStatement.PROPERTY_LABEL);
        newOptionalColumn("header", FinancialStatement.PROPERTY_HEADER, O_N_PARSER);
        newOptionalColumn("accounts", FinancialStatement.PROPERTY_ACCOUNTS);
        newOptionalColumn("debitAccounts", FinancialStatement.PROPERTY_DEBIT_ACCOUNTS);
        newOptionalColumn("creditAccounts", FinancialStatement.PROPERTY_CREDIT_ACCOUNTS);
        newOptionalColumn("provisionDeprecationAccounts", FinancialStatement.PROPERTY_PROVISION_DEPRECATION_ACCOUNTS);
        newOptionalColumn("subAmount", FinancialStatement.PROPERTY_SUB_AMOUNT, O_N_PARSER);
        newOptionalColumn("headerAmount", FinancialStatement.PROPERTY_HEADER_AMOUNT, O_N_PARSER);
        newOptionalColumn("masterFinancialStatement", FinancialStatement.PROPERTY_MASTER_FINANCIAL_STATEMENT);
        newOptionalColumn("financialStatementWay", FinancialStatement.PROPERTY_WAY, FINANCIAL_STATEMENT_WAY_ENUM_VALUE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<FinancialStatement, Object>> getColumnsForExport() {
        ModelBuilder<FinancialStatement> modelBuilder = new ModelBuilder<>();

        modelBuilder.newColumnForExport("label", FinancialStatement.PROPERTY_LABEL);
        modelBuilder.newColumnForExport("header", FinancialStatement.PROPERTY_HEADER, O_N_FORMATTER);
        modelBuilder.newColumnForExport("accounts", FinancialStatement.PROPERTY_ACCOUNTS);
        modelBuilder.newColumnForExport("debitAccounts", FinancialStatement.PROPERTY_DEBIT_ACCOUNTS);
        modelBuilder.newColumnForExport("creditAccounts", FinancialStatement.PROPERTY_CREDIT_ACCOUNTS);
        modelBuilder.newColumnForExport("provisionDeprecationAccounts", FinancialStatement.PROPERTY_PROVISION_DEPRECATION_ACCOUNTS);
        modelBuilder.newColumnForExport("subAmount", FinancialStatement.PROPERTY_SUB_AMOUNT, O_N_FORMATTER);
        modelBuilder.newColumnForExport("headerAmount", FinancialStatement.PROPERTY_HEADER_AMOUNT,O_N_FORMATTER);
        modelBuilder.newColumnForExport("masterFinancialStatement", FinancialStatement.PROPERTY_MASTER_FINANCIAL_STATEMENT, MASTER_FINANCIAL_STATEMENT_PATH_FORMATTER);
        modelBuilder.newColumnForExport("financialStatementWay", FinancialStatement.PROPERTY_WAY, FINANCIAL_STATEMENT_WAY_ENUM_VALUE_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public FinancialStatementImport newEmptyInstance() {
        return new FinancialStatementImportImpl();
    }
}
