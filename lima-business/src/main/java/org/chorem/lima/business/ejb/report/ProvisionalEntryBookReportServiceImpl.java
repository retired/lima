package org.chorem.lima.business.ejb.report;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sf.jasperreports.engine.JasperReport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.beans.DocumentReport;
import org.chorem.lima.beans.EntryBookImpl;
import org.chorem.lima.beans.FinancialPeriodImpl;
import org.chorem.lima.beans.Transaction;
import org.chorem.lima.beans.TransactionImpl;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.EntryService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.report.ProvisionalEntryBookReportService;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 19/11/14.
 */
@Stateless
@Remote(ProvisionalEntryBookReportService.class)
@TransactionAttribute
public class ProvisionalEntryBookReportServiceImpl implements ProvisionalEntryBookReportService {
    protected static final Log log = LogFactory.getLog(GeneralEntryBookReportServiceImpl.class);

    @EJB
    protected FinancialPeriodService financialPeriodService;

    @EJB
    protected EntryService entryService;

    @EJB
    protected EntryBookService entryBookService;

    @EJB
    protected IdentityService identityService;

    @EJB
    protected FinancialTransactionService financialTransactionService;

    @Override
    public DocumentReport getEntryBookDocumentReport(Date beginDate, Date endDate, List<String> entryBookCodes, DecimalFormat bigDecimalFormat, JasperReport entryBooksJasperReport, JasperReport financialPeriodsJasperReport, JasperReport transactionsJasperReport) {
        String reportName = t("lima-business.document.provisionalEntryBookReport.title");
        DocumentReport documentReport = CommonsDocumentReport.getDocumentReport(reportName, beginDate, endDate, entryBooksJasperReport, bigDecimalFormat, identityService);

        documentReport.setColumnAccountTitle(t("lima-business.document.columnAccountTitle"));
        documentReport.setColumnCreditTitle(t("lima-business.document.columnCreditTitle"));
        documentReport.setColumnDateTitle(t("lima-business.document.columnDateTitle"));
        documentReport.setColumnDebitTitle(t("lima-business.document.columnDebitTitle"));
        documentReport.setColumnDescriptionTitle(t("lima-business.document.columnDescriptionTitle"));
        documentReport.setColumnTotalForPeriodTitle(t("lima-business.document.columnTotalForPeriodTitle"));
        documentReport.setColumnVoucherTitle(t("lima-business.document.columnVoucherTitle"));

        documentReport.setLastPageColumnDescription(t("lima-business.document.generalEntrybook.lastPageColumnDescription"));

        try {

            if (beginDate != null && endDate != null) {

                SimpleDateFormat dateFormatter = new SimpleDateFormat(LimaBusinessConfig.getInstance().getDateFormat());
                BigDecimal documentReportDebit = BigDecimal.ZERO;
                BigDecimal documentReportCredit = BigDecimal.ZERO;

                List<FinancialPeriod> financialPeriods = financialPeriodService.getFinancialPeriodsWithBeginDateWithin(beginDate, endDate);

                List<EntryBook> entryBooks;

                // if not any entry book has been selected then default is all.
                if (entryBookCodes != null) {
                    entryBooks = entryBookService.findAllEntryBookByEntryBookCodes(entryBookCodes);
                } else {
                    entryBooks = entryBookService.getAllEntryBooks();
                }

                for(EntryBook entryBook:entryBooks) {

                    org.chorem.lima.beans.EntryBook entryBookReport = new EntryBookImpl();
                    entryBookReport.setLabel(entryBook.getLabel());
                    entryBookReport.setCode(entryBook.getCode());
                    entryBookReport.setSubReport(financialPeriodsJasperReport);
                    entryBookReport.setFormatter(bigDecimalFormat);
                    entryBookReport.setEntryBookCodeAndNameText(t("lima-business.document.entryBookCodeAndName"));

                    BigDecimal entryBookSoldeDebit = BigDecimal.ZERO;
                    BigDecimal entryBookSoldeCredit = BigDecimal.ZERO;

                    // for each FinancialPeriod an EntryBookPeriodReport is created
                    for (FinancialPeriod financialPeriod : financialPeriods) {

                        org.chorem.lima.beans.FinancialPeriod financialPeriodReport = new FinancialPeriodImpl();
                        financialPeriodReport.setForMonthText(t("lima-business.document.forMonthText"));
                        financialPeriodReport.setBalanceForEntryBookText(t("lima-business.document.balanceForEntryBook"));
                        financialPeriodReport.setSubReport(transactionsJasperReport);
                        financialPeriodReport.setPeriod(financialPeriod.getBeginDate());

                        BigDecimal financialPeriodDebit = BigDecimal.ZERO;
                        BigDecimal financialPeriodCredit = BigDecimal.ZERO;

                        BigDecimal debit;
                        BigDecimal credit;
                        List<Entry> entries = entryService.findAllEntriesByDatesForEntryBook(entryBook, financialPeriod.getBeginDate(), financialPeriod.getEndDate());
                        for (Entry entry : entries) {
                            if (entry.isDebit()) {
                                debit = entry.getAmount();
                                credit = BigDecimal.ZERO;
                            } else {
                                debit = BigDecimal.ZERO;
                                credit = entry.getAmount();
                            }
                            if (debit.compareTo(BigDecimal.ZERO) != 0 || credit.compareTo(BigDecimal.ZERO) != 0) {
                                Account account =  entry.getAccount();
                                String accountLabel = account != null ? entry.getAccount().getAccountNumber() + " - " + entry.getAccount().getLabel() : "";
                                Transaction transaction = new TransactionImpl();
                                transaction.setSubTotalText(t("lima-business.document.subTotal"));
                                transaction.setSubTotalForText(t("lima-business.document.subTotalFor"));
                                transaction.setTransactionDate(entry.getFinancialTransaction().getTransactionDate());
                                transaction.setAccount(accountLabel);
                                transaction.setDescription(entry.getDescription());
                                transaction.setVoucher(entry.getVoucher());
                                transaction.setDebit(debit);
                                transaction.setCredit(credit);
                                transaction.setFormatter(bigDecimalFormat);
                                transaction.setDateFormat(dateFormatter);

                                financialPeriodDebit = financialPeriodDebit.add(debit);
                                financialPeriodCredit = financialPeriodCredit.add(credit);

                                financialPeriodReport.addTransactions(transaction);
                            }
                        }


                        if (financialPeriodDebit.compareTo(BigDecimal.ZERO) != 0 || financialPeriodCredit.compareTo(BigDecimal.ZERO) != 0) {
                            financialPeriodReport.setEntryBookCode(entryBook.getCode());
                            financialPeriodReport.setDebitBalance(financialPeriodDebit);
                            financialPeriodReport.setCreditBalance(financialPeriodCredit);
                            financialPeriodReport.setFormatter(bigDecimalFormat);
                            financialPeriodReport.setDateFormat(LimaBusinessConfig.getInstance().getDateFormat());

                            entryBookReport.addFinancialPeriods(financialPeriodReport);
                            entryBookSoldeDebit = entryBookSoldeDebit.add(financialPeriodDebit);
                            entryBookSoldeCredit = entryBookSoldeCredit.add(financialPeriodCredit);

                        }
                    }

                    if (entryBookSoldeDebit.compareTo(BigDecimal.ZERO) != 0 || entryBookSoldeCredit.compareTo(BigDecimal.ZERO) != 0) {
                        entryBookReport.setDebitBalance(entryBookSoldeDebit);
                        entryBookReport.setCreditBalance(entryBookSoldeCredit);
                        documentReport.addEntryBooks(entryBookReport);
                        documentReportDebit = documentReportDebit.add(entryBookSoldeDebit);
                        documentReportCredit = documentReportCredit.add(entryBookSoldeCredit);
                    }
                }
                documentReport.setDebitBalance(documentReportDebit);
                documentReport.setCreditBalance(documentReportCredit);
            }
        } catch (Exception ex) {
            log.error("Can't create document", ex);
            throw new LimaTechnicalException("Can't create document", ex);
        }
        return documentReport;
    }
}
