package org.chorem.lima.business.ejb.csv;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialTransaction;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueGetter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 *
 * cf : http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000027804775&cidTexte=LEGITEXT000006069583&dateTexte=20130802&oldAction=rechCodeArticle
 */
public class FiscalControlExportModel implements ExportModel<Entry> {

    protected static final String PROPERTY_ENTRY_BOOK_CODE = Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_ENTRY_BOOK + "." + EntryBook.PROPERTY_CODE;
    protected static final String PROPERTY_ENTRY_BOOK_LABEL = Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_ENTRY_BOOK + "." + EntryBook.PROPERTY_LABEL;
    protected static final String PROPERTY_DATE = Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_TRANSACTION_DATE;
    protected static final String PROPERTY_ACCOUNT_NUMBER = Entry.PROPERTY_ACCOUNT + "." + Account.PROPERTY_ACCOUNT_NUMBER;
    protected static final String PROPERTY_ACCOUNT_LABEL = Entry.PROPERTY_ACCOUNT + "." + Account.PROPERTY_LABEL;


    protected static final ValueGetter<Entry, String> EMPTY_GETTER = new ValueGetter<Entry, String>() {
        @Override
        public String get(Entry object) throws Exception {
            return "";
        }
    };

    protected static final ValueFormatter<Boolean> D_C_FORMATTER = new ValueFormatter<Boolean>() {
        @Override
        public String format(Boolean value) {
            String result = value ? "D" : "C";
            return result;
        }
    };

    protected static final ValueFormatter<Date> DATE_FORMATTER = new ValueFormatter<Date>() {
        @Override
        public String format(Date value) {
            String result;
            if (value != null) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                result = simpleDateFormat.format(value);
            } else {
                result = "";
            }
            return result;
        }
    };

    protected static final ValueGetter<Entry, String> DEBIT_GETTER = new ValueGetter<Entry, String>() {
        @Override
        public String get(Entry entry) throws Exception {
            String result = "";
            if (entry != null && entry.isDebit()) {
                result = String.valueOf(entry.getAmount());
            }
            return result;
        }
    };

    protected static final ValueGetter<Entry, String> CREDIT_GETTER = new ValueGetter<Entry, String>() {
        @Override
        public String get(Entry entry) throws Exception {
            String result = "";
            if (entry != null && !entry.isDebit()) {
                result = String.valueOf(entry.getAmount());
            }
            return result;
        }
    };


    @Override
    public char getSeparator() {
        return '|';
    }

    @Override
    public Iterable<ExportableColumn<Entry, Object>> getColumnsForExport() {
        ModelBuilder<Entry> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("JournalCode", PROPERTY_ENTRY_BOOK_CODE);
        modelBuilder.newColumnForExport("JournalLib ", PROPERTY_ENTRY_BOOK_LABEL);
        modelBuilder.newColumnForExport("EcritureNum ", Entry.PROPERTY_FINANCIAL_TRANSACTION, new FinancialTransactionFormatter());
        modelBuilder.newColumnForExport("EcritureDate ", PROPERTY_DATE, DATE_FORMATTER);
        modelBuilder.newColumnForExport("CompteNum", PROPERTY_ACCOUNT_NUMBER);
        modelBuilder.newColumnForExport("CompteLib", PROPERTY_ACCOUNT_LABEL);
        modelBuilder.newColumnForExport("CompAuxNum", EMPTY_GETTER);
        modelBuilder.newColumnForExport("CompAuxLib", EMPTY_GETTER);
        modelBuilder.newColumnForExport("PieceRef", Entry.PROPERTY_VOUCHER);
        modelBuilder.newColumnForExport("PieceDate", PROPERTY_DATE, DATE_FORMATTER);
        modelBuilder.newColumnForExport("EcritureLib", Entry.PROPERTY_DESCRIPTION);
        modelBuilder.newColumnForExport("Debit ", DEBIT_GETTER);
        modelBuilder.newColumnForExport("Credit ", CREDIT_GETTER);
        modelBuilder.newColumnForExport("EcritureLet", Entry.PROPERTY_LETTERING);
        modelBuilder.newColumnForExport("DateLet", EMPTY_GETTER);
        modelBuilder.newColumnForExport("ValidDate", PROPERTY_DATE, DATE_FORMATTER);
        modelBuilder.newColumnForExport("Montantdevise", EMPTY_GETTER);
        modelBuilder.newColumnForExport("Idevise", EMPTY_GETTER);

        return (Iterable) modelBuilder.getColumnsForExport();
    }



    protected class FinancialTransactionFormatter implements ValueFormatter<FinancialTransaction> {

        final Map<FinancialTransaction, Integer> numByTransactions = Maps.newHashMap();
        int nextNum = 0;

        @Override
        public String format(FinancialTransaction value) {
            Integer num = numByTransactions.get(value);
            if (num == null) {
                num = nextNum++;
                numByTransactions.put(value, num);
            }
            return Integer.toString(num);
        }
    }
}
