/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.IdentityTopiaDao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.util.List;

@Stateless
@Remote(IdentityService.class)
@TransactionAttribute
public class IdentityServiceImpl extends AbstractLimaService implements IdentityService {

    @Override
    public Identity getIdentity() {
        Identity identity = null;

        IdentityTopiaDao identityTopiaDao = getDaoHelper().getIdentityDao();
        List<Identity> identities = identityTopiaDao.findAll();
        if (identities.size() != 0) {
            identity = identities.get(0);
        }
        return identity;
    }

    @Override
    public void updateIdentity(Identity identity) {
        IdentityTopiaDao identityTopiaDao = getDaoHelper().getIdentityDao();
        List<Identity> identities = identityTopiaDao.findAll();
        if (identities.size() != 0) {
            identityTopiaDao.delete(identities.get(0));
        }
        identityTopiaDao.create(identity);
    }

}
