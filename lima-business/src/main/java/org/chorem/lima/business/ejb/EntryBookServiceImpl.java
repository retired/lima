/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import com.google.common.collect.Lists;

import org.chorem.lima.business.AccountingRules;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.exceptions.AlreadyExistEntryBookException;
import org.chorem.lima.business.exceptions.UsedEntryBookException;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.ClosedPeriodicEntryBookImpl;
import org.chorem.lima.entity.ClosedPeriodicEntryBookTopiaDao;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookTopiaDao;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialPeriodTopiaDao;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.util.ArrayList;
import java.util.List;

/**
 * Implémente la fonction multi-EntryBook. Il est possible de créer ici le
 * EntryBook des ventes, le EntryBook des achats, etc.
 *
 * @author Rémi Chapelet
 */
@Stateless
@Remote(EntryBookService.class)
@TransactionAttribute
public class EntryBookServiceImpl extends AbstractLimaService implements EntryBookService {

    @Override
    public EntryBook createNewEntryBook(){
        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();
        EntryBook result = entryBookTopiaDao.newInstance();
        return result;
    }

    @Override
    public boolean createOrUpdateEntryBook(EntryBook entryBook) {
        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();
        EntryBook existingEntryBook = entryBookTopiaDao.forCodeEquals(entryBook.getCode()).findUniqueOrNull();
        boolean result;
        if (existingEntryBook != null) {
            result = true;
            Binder binder = BinderFactory.newBinder(EntryBook.class, EntryBook.class);
            binder.copy(entryBook, existingEntryBook, EntryBook.PROPERTY_LABEL);
            updateEntryBook(existingEntryBook);
        } else {
            result = false;
            createNewEntryBook(entryBook);
        }
        return result;
    }

    @Override
    public EntryBook createImportedNewEntryBook(EntryBook entryBook, List<FinancialPeriod> financialPeriods) {

        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();
        EntryBook result = entryBookTopiaDao.create(entryBook);

        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooksToSave = getClosedPeriodicEntryBooks(result, financialPeriods);
        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao = getDaoHelper().getClosedPeriodicEntryBookDao();
        closedPeriodicEntryBookTopiaDao.createAll(closedPeriodicEntryBooksToSave);

        return result;

    }

    @Override
    public EntryBook createEntryBook(EntryBook entryBook) throws AlreadyExistEntryBookException {
        EntryBook result;
        // check if entrybook with is name already exist
        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();
        EntryBook alreadyExistingEntryBook = entryBookTopiaDao.forCodeEquals(entryBook.getCode()).findUniqueOrNull();
        if (alreadyExistingEntryBook != null) {
            throw new AlreadyExistEntryBookException(entryBook);
        } else {
            // creation du EntryBook
            result = createNewEntryBook(entryBook);
        }
        return result;
    }

    protected EntryBook createNewEntryBook(EntryBook entryBook) {
        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();
        EntryBook result = entryBookTopiaDao.create(entryBook);
        createClosedPeriodicEntryBook(result);
        return result;
    }

    protected void createClosedPeriodicEntryBook(EntryBook entryBook, List<FinancialPeriod> financialPeriods) {
        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooksToSave = getClosedPeriodicEntryBooks(entryBook, financialPeriods);

        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao = getDaoHelper().getClosedPeriodicEntryBookDao();
        // create it
        closedPeriodicEntryBookTopiaDao.createAll(closedPeriodicEntryBooksToSave);
    }

    protected List<ClosedPeriodicEntryBook> getClosedPeriodicEntryBooks(EntryBook entryBook, List<FinancialPeriod> financialPeriods) {
        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooksToSave = new ArrayList<>();
        for (FinancialPeriod financialPeriod : financialPeriods) {

            //new closed periodic entrybook
            ClosedPeriodicEntryBook closedPeriodicEntryBook =
                    new ClosedPeriodicEntryBookImpl();
            // set entrybook
            closedPeriodicEntryBook.setEntryBook(entryBook);
            // set financial period
            closedPeriodicEntryBook.setFinancialPeriod(financialPeriod);

            closedPeriodicEntryBooksToSave.add(closedPeriodicEntryBook);

        } return closedPeriodicEntryBooksToSave;
    }

    protected void createClosedPeriodicEntryBook(EntryBook entryBook) {

        //create ClosedPeriodicEntryBook for all unblocked financial period
        FinancialPeriodTopiaDao financialPeriodTopiaDao = getDaoHelper().getFinancialPeriodDao();

        // for all unblocked financialperiod
        List<FinancialPeriod> financialPeriods = financialPeriodTopiaDao.forProperties(FinancialPeriod.PROPERTY_LOCKED, false).findAll();

        createClosedPeriodicEntryBook(entryBook, financialPeriods);
    }

    @Override
    public List<EntryBook> getAllEntryBooks() {

        // check if entrybook with is name already exist
        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();
        List<EntryBook> entryBooksList = entryBookTopiaDao.newQueryBuilder().setOrderByArguments(EntryBook.PROPERTY_CODE).findAll();

        return entryBooksList;
    }

    @Override
    public EntryBook updateEntryBook(EntryBook entryBook) {

        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();

        // creation du EntryBook
        EntryBook result = entryBookTopiaDao.update(entryBook);

        return result;
    }

    @Override
    public void removeEntryBook(EntryBook entryBook) throws UsedEntryBookException {

        // check rule
        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();
        accountingRules.removeEntryBookRules(entryBook);

        // re-attach to current transaction
        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();
        EntryBook localEntryBook = entryBookTopiaDao.forTopiaIdEquals(entryBook.getTopiaId()).findUnique();

        // delete all ClosedPeriodicEntryBook from this EntryBook
        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao =
                getDaoHelper().getClosedPeriodicEntryBookDao();
        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks =
                closedPeriodicEntryBookTopiaDao.findAllByEntryBook(localEntryBook);
        for (ClosedPeriodicEntryBook closedPeriodicEntryBook : closedPeriodicEntryBooks) {
            if (log.isDebugEnabled()) {
                log.debug("Deleting closed " + closedPeriodicEntryBook.getTopiaId());
            }

            // fix : ObjectDeletedException: deleted object would be re-saved by cascade
            closedPeriodicEntryBook.getEntryBook().removeFinancialPeriodClosedPeriodicEntryBook(closedPeriodicEntryBook);
            closedPeriodicEntryBook.getFinancialPeriod().removeEntryBookClosedPeriodicEntryBook(closedPeriodicEntryBook);

            closedPeriodicEntryBookTopiaDao.delete(closedPeriodicEntryBook);
        }

        // delete entry book
        entryBookTopiaDao.delete(localEntryBook);
    }

    /*
     * @see org.chorem.lima.business.ejbinterface.EntryBookService#getEntryBookByCode(java.lang.String)
     */
    @Override
    public EntryBook getEntryBookByCode(String code) {

        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();

        // creation du EntryBook
        EntryBook entryBook = entryBookTopiaDao.forCodeEquals(code).findUniqueOrNull();

        return entryBook;
    }

    @Override
    public List<EntryBook> findAllEntryBookByEntryBookCodes(List<String> entryBookCodes) {
        List<EntryBook> result;
        //if no filter account
        if (entryBookCodes != null) {
            EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();
            result = entryBookTopiaDao.forCodeIn(entryBookCodes).findAll();
        } else {
            result = Lists.newArrayListWithExpectedSize(0);
        }
        return result;
    }

    @Override
    public void removeAllEntryBooks() {
        EntryBookTopiaDao dao = getDaoHelper().getEntryBookDao();
        List<EntryBook> entryBooks = dao.findAll();
        dao.deleteAll(entryBooks);
    }
}
