package org.chorem.lima.business.ejb.report;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.beans.DocumentReport;
import org.chorem.lima.beans.GeneralEntryBook;
import org.chorem.lima.beans.GeneralEntryBookEntry;
import org.chorem.lima.beans.GeneralEntryBookEntryImpl;
import org.chorem.lima.beans.GeneralEntryBookImpl;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.ClosedPeriodicEntryBookService;
import org.chorem.lima.business.api.EntryService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.report.GeneralEntryBookReportService;
import org.chorem.lima.business.ejb.AbstractLimaService;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.FinancialPeriod;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;


/**
 * Created by davidcosse on 17/11/14.
 */
@Stateless
@Remote(GeneralEntryBookReportService.class)
@TransactionAttribute
public class GeneralEntryBookReportServiceImpl extends AbstractLimaService implements GeneralEntryBookReportService {

    protected static final Log log = LogFactory.getLog(GeneralEntryBookReportServiceImpl.class);

    protected static final Predicate<GeneralEntryBookEntry> HAS_NO_ENTRY = new Predicate<GeneralEntryBookEntry>() {

        @Override
        public boolean apply(GeneralEntryBookEntry generalEntryBookEntry) {
            boolean result = BigDecimal.ZERO.equals(generalEntryBookEntry.getDebit()) &&
                    BigDecimal.ZERO.equals(generalEntryBookEntry.getCredit());
            return result;
        }
    };

    @EJB
    protected FinancialPeriodService financialPeriodService;

    @EJB
    protected ClosedPeriodicEntryBookService closedPeriodicEntryBookService;

    @EJB
    protected EntryService entryService;

    @EJB
    protected IdentityService identityService;

    @Override
    public DocumentReport getGeneralEntryBookDocumentReport(Date beginDate, Date endDate, DecimalFormat bigDecimalFormat, JasperReport generalEntryBooksJasperReport, JasperReport entriesJasperReport) {
        String reportName = t("lima-business.document.generalEntryBookReport.title");
        DocumentReport documentReport = CommonsDocumentReport.getDocumentReport(reportName, beginDate, endDate, generalEntryBooksJasperReport, bigDecimalFormat, identityService);

        documentReport.setColumnEntryBookTitle(t("lima-business.document.columnEntryBookTitle"));
        documentReport.setColumnDescriptionTitle(t("lima-business.document.columnDescriptionTitle"));
        documentReport.setColumnTotalForPeriodTitle(t("lima-business.document.columnTotalForPeriodTitle"));
        documentReport.setColumnDebitTitle(t("lima-business.document.columnDebitTitle"));
        documentReport.setColumnCreditTitle(t("lima-business.document.columnCreditTitle"));
        documentReport.setLastPageColumnDescription(t("lima-business.document.entrybook.lastPageColumnDescription"));

        try {
            if (beginDate != null && endDate != null) {

                List<FinancialPeriod> financialPeriods = financialPeriodService.getFinancialPeriodsWithBeginDateWithin(beginDate, endDate);

                BigDecimal amountDebit = BigDecimal.ZERO;
                BigDecimal amountCredit = BigDecimal.ZERO;

                // for each FinancialPeriod an EntryBookPeriodReport is created
                for (FinancialPeriod financialPeriod : financialPeriods) {

                    List<ClosedPeriodicEntryBook> closedPeriodicEntryBookList =
                            closedPeriodicEntryBookService.getAllByDates(
                                    financialPeriod.getBeginDate(), financialPeriod.getEndDate());

                    BigDecimal subAmountDebit = BigDecimal.ZERO;
                    BigDecimal subAmountCredit = BigDecimal.ZERO;

                    // for all ClosedPeriodicEntryBook with same code we create an GeneralEntryBookEntry
                    Map<String, GeneralEntryBookEntry> generalEntryBookEntryByEntryBookCode = Maps.newHashMap();
                    BigDecimal entryBookDebit = BigDecimal.ZERO;
                    BigDecimal entryBookCredit = BigDecimal.ZERO;
                    for (ClosedPeriodicEntryBook closedPeriodicEntryBook : closedPeriodicEntryBookList) {

                        String entryBookCode = closedPeriodicEntryBook.getEntryBook().getCode();
                        GeneralEntryBookEntry generalEntryBookEntry = generalEntryBookEntryByEntryBookCode.get(entryBookCode);
                        if (generalEntryBookEntry == null) {
                            generalEntryBookEntry = new GeneralEntryBookEntryImpl();
                            generalEntryBookEntry.setFormatter(bigDecimalFormat);
                            generalEntryBookEntry.setCode(closedPeriodicEntryBook.getEntryBook().getCode());
                            generalEntryBookEntry.setDescription(closedPeriodicEntryBook.getEntryBook().getLabel());
                            generalEntryBookEntryByEntryBookCode.put(entryBookCode, generalEntryBookEntry);

                            // init debit and credit
                            entryBookDebit = BigDecimal.ZERO;
                            entryBookCredit = BigDecimal.ZERO;

                            if (log.isDebugEnabled()) {
                                log.debug("Entrybook code added : " + closedPeriodicEntryBook.getEntryBook().getCode());
                            }

                        }

                        List<Object[]> results = entryService.findDebitCreditOfTransaction(closedPeriodicEntryBook);

                        BigDecimal debit = BigDecimal.ZERO;
                        BigDecimal credit = BigDecimal.ZERO;
                        int nbAmount = results.size();
                        if (nbAmount == 2) {
                            debit = (BigDecimal) results.get(0)[1];
                            credit = (BigDecimal) results.get(1)[1];
                        }
                        if (nbAmount == 1) {
                            if ((Boolean) results.get(0)[0]) {
                                debit = (BigDecimal) results.get(0)[1];
                            } else {
                                credit = (BigDecimal) results.get(0)[1];
                            }
                        }
                        entryBookDebit = entryBookDebit.add(debit);
                        entryBookCredit = entryBookCredit.add(credit);

                        subAmountDebit= subAmountDebit.add(entryBookDebit);
                        subAmountCredit = subAmountCredit.add(entryBookCredit);
                        generalEntryBookEntry.setDebit(entryBookDebit);
                        generalEntryBookEntry.setCredit(entryBookCredit);

                    }
                    Collection<GeneralEntryBookEntry> generalEntryBookEntries = generalEntryBookEntryByEntryBookCode.values();
                    Iterables.removeIf(generalEntryBookEntries, HAS_NO_ENTRY);

                    if (subAmountCredit.compareTo(BigDecimal.ZERO) != 0 && subAmountDebit.compareTo(BigDecimal.ZERO) != 0) {
                        GeneralEntryBook generalEntryBook = new GeneralEntryBookImpl();
                        generalEntryBook.setSubTotalForMonthText(t("lima-business.document.subTotalForMonthText"));
                        generalEntryBook.setForMonthText(t("lima-business.document.forMonthText"));
                        generalEntryBook.setFormatter(bigDecimalFormat);
                        generalEntryBook.setDateFormat(LimaBusinessConfig.getInstance().getDateFormat());
                        generalEntryBook.setPeriod(financialPeriod.getBeginDate());
                        generalEntryBook.setSubReport(entriesJasperReport);
                        generalEntryBook.setDebitBalance(subAmountDebit);
                        generalEntryBook.setCreditBalance(subAmountCredit);
                        generalEntryBook.setEntries(Lists.newArrayList(generalEntryBookEntries));
                        documentReport.addGeneralEntryBooks(generalEntryBook);

                        amountCredit = amountCredit.add(subAmountCredit);
                        amountDebit = amountDebit.add(subAmountDebit);
                    }

                }

                documentReport.setDebitBalance(amountDebit);
                documentReport.setCreditBalance(amountCredit);
            }
        } catch (Exception ex) {
            log.error("Can't create document", ex);
            throw new LimaTechnicalException("Can't create document", ex);
        }
        return documentReport;
    }
}
