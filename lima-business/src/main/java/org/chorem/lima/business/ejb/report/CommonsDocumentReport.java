package org.chorem.lima.business.ejb.report;

/*
 * #%L
 * Lima :: Business
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sf.jasperreports.engine.JasperReport;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.beans.DocumentReport;
import org.chorem.lima.beans.DocumentReportImpl;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.entity.Identity;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 29/07/15.
 */
public class CommonsDocumentReport {

    public static final String DATE_FOMRAT ="dd/MM/yyyy";
    public static final String TIME_FORMAT = "HH:mm";

    protected static DocumentReport getDocumentReport(String reportName, Date from, Date to, JasperReport subReport, DecimalFormat bigDecimalFormat, IdentityService identityService) {
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FOMRAT);
        SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT);

        DocumentReport documentReport = new DocumentReportImpl();
        documentReport.setSubReport(subReport);
        documentReport.setFormatter(bigDecimalFormat);

        documentReport.setTitleReport(reportName);

        Identity identity = identityService.getIdentity();
        String companyName = identity == null ? "" : identity.getName();
        String companyAddress = identity == null ? "" : StringUtils.replace(identity.getAddress(), "\n", ", ");
        String companyPostCode = identity == null ? "" : identity.getZipCode();
        String companyCity = identity == null ? "" : identity.getCity();
        String businessNumber = identity == null ? "" : identity.getBusinessNumber();
        String phone = identity == null ? "" : identity.getPhoneNumber();

        documentReport.setTitleCompanyName(companyName);
        documentReport.setTitleCompanyAddress(companyAddress);
        documentReport.setTitleCompanyPostCode(companyPostCode);
        documentReport.setTitleCompanyCity(companyCity);
        documentReport.setTitleCompanySiret(businessNumber);
        documentReport.setTitleCompanyPhone(phone);

        documentReport.setTitleCompanySiretLabel(t("lima-business.document.titleCompanySiretLabel"));
        documentReport.setTitleCompanyPhoneLabel(t("lima-business.document.titleCompanyPhoneLabel"));
        documentReport.setTitleCurrentDate(t("lima-business.document.titleCurrentDate", dateFormat.format(currentDate), timeFormat.format(currentDate)));
        documentReport.setTitleFromToDate(t("lima-business.document.titleFromToDate", dateFormat.format(from), dateFormat.format(to)));

        documentReport.setHeaderSelectedAccountsLabel(t("lima-business.document.headerSelectedAccountsLabel"));
        documentReport.setHeaderCurrencyLabel(t("lima-business.document.headerCurrencyLabel"));
        documentReport.setHeaderCurrency(bigDecimalFormat.getDecimalFormatSymbols().getCurrencySymbol());
        return documentReport;
    }
}
