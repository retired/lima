/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.beans.BalanceTrial;
import org.chorem.lima.beans.BalanceTrialImpl;
import org.chorem.lima.beans.ReportsDatas;
import org.chorem.lima.beans.ReportsDatasImpl;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.ReportService;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.utils.AccountComparator;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountTopiaDao;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryTopiaDao;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FiscalPeriod;
import org.nuiton.topia.persistence.TopiaException;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Stateless
@Remote(ReportService.class)
@TransactionAttribute
public class ReportServiceImpl extends AbstractLimaService implements ReportService {

    @EJB
    protected FinancialPeriodService financialPeriodService;

    @EJB
    protected AccountService accountService;

    protected static final Log log = LogFactory.getLog(ReportServiceImpl.class);
    public static final int MAX_ACCOUNT_NUMBER_DISPLAYED = 3;
    public static final String ACCOUNTS_SEPARATOR = " - ";

    /**
     * Recursiv
     * List entries for a period and an account
     * Calculate the amounts
     * 3 cases:
     * - for subaccount
     * - for a foldaccounts, contains many accounts
     * - for a foldthirdparts accounts
     */
    @Override
    public ReportsDatas generateAccountsReports(Account account, Boolean thirdPartAccountsMode,
            Date beginDate, Date endDate) {

        BigDecimal credit = BigDecimal.ZERO;
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal solde = BigDecimal.ZERO;
        List<Entry> entries = new ArrayList<>();

        ReportsDatas reportsDatas = generateSubAccountReports(account,
                beginDate, endDate);
        debit = debit.add(reportsDatas.getAmountDebit());
        credit = credit.add(reportsDatas.getAmountCredit());
        entries.addAll(reportsDatas.getListEntry());

        //Get allsubaccounts and thirdParts accounts
        List<Account> accounts = accountService.getAllSubAccounts(account);

        for (Account subAccount : accounts) {
            reportsDatas = generateSubAccountReports(subAccount,
                    beginDate, endDate);
            debit = debit.add(reportsDatas.getAmountDebit());
            credit = credit.add(reportsDatas.getAmountCredit());
            entries.addAll(reportsDatas.getListEntry());
        }

        //solde = debit - credit
        solde = solde.add(debit);
        solde = solde.subtract(credit);

        if (solde.compareTo(BigDecimal.ZERO) == 1) {
            reportsDatas.setSoldeDebit(true);
        }
        solde = solde.abs();
        reportsDatas.setAmountCredit(credit);
        reportsDatas.setAmountDebit(debit);
        reportsDatas.setAmountSolde(solde);

        reportsDatas.setListEntry(entries);

        return reportsDatas;
    }

    /**
     * Get list entries
     * Calculate all credit, debit and solde amounts for the accounts reports
     *
     * @param account
     * @param beginDate
     * @param endDate
     * @return
     */
    protected ReportsDatas generateSubAccountReports(Account account,
            Date beginDate, Date endDate) {
        ReportsDatas reportsDatas = new ReportsDatasImpl();
        BigDecimal credit = BigDecimal.ZERO;
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal solde = BigDecimal.ZERO;

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();

        // trouve les entrees associées à ca compte entre les date données
        List<Entry> listEntries = entryTopiaDao.findAllEntryOfBalancedTransaction(account, beginDate, endDate);
        reportsDatas.setListEntry(listEntries);

        // recupere les totaux pour le compte
        List<Object[]> results = entryTopiaDao.getDebitCreditOfBalancedTransaction(account, beginDate, endDate);
        int nbAmount = results.size();
        if (nbAmount == 2) {
            debit = (BigDecimal) results.get(0)[1];
            credit = (BigDecimal) results.get(1)[1];
        }
        if (nbAmount == 1) {
            if ((Boolean) results.get(0)[0]) {
                debit = (BigDecimal) results.get(0)[1];
            } else {
                credit = (BigDecimal) results.get(0)[1];
            }
        }

        // set the amounts and solde
        // solde = debit - credit
        solde = solde.add(debit);
        solde = solde.subtract(credit);

        if (solde.compareTo(BigDecimal.ZERO) == 1) {
            reportsDatas.setSoldeDebit(true);
        }
        solde = solde.abs();

        reportsDatas.setAmountCredit(credit);
        reportsDatas.setAmountDebit(debit);
        reportsDatas.setAmountSolde(solde);

        return reportsDatas;
    }

    /**
     * Calculate all credit, debit and solde amounts for the balance
     * <p/>
     * Get all entries if true
     * @throws TopiaException 
     */
    public ReportsDatas generateSubAccountBalance(Account account,
            Date beginDate, Date endDate, Boolean getEntries) {
        ReportsDatas reportsDatas = new ReportsDatasImpl();
        BigDecimal credit = BigDecimal.ZERO;
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal solde = BigDecimal.ZERO;

        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();

        if (getEntries) {
            List<Entry> listEntries = entryTopiaDao.findAllEntryOfBalancedTransaction(account, beginDate, endDate);
            reportsDatas.setListEntry(listEntries);
        }

        List<Object[]> results = entryTopiaDao.getDebitCreditOfBalancedTransaction(account, beginDate, endDate);
        int nbAmount = results.size();
        if (nbAmount == 2) {
            debit = (BigDecimal) results.get(0)[1];
            credit = (BigDecimal) results.get(1)[1];
        }
        if (nbAmount == 1) {
            if ((Boolean) results.get(0)[0]) {
                debit = (BigDecimal) results.get(0)[1];
            } else {
                credit = (BigDecimal) results.get(0)[1];
            }
        }

        // set the amounts and solde
        //solde = debit - credit
        solde = solde.add(debit);
        solde = solde.subtract(credit);

        if (solde.compareTo(BigDecimal.ZERO) == 1) {
            reportsDatas.setSoldeDebit(true);

        }
        solde = solde.abs();

        reportsDatas.setAmountCredit(credit);
        reportsDatas.setAmountDebit(debit);
        reportsDatas.setAmountSolde(solde);

        return reportsDatas;
    }

    /**
     * Get all entry from an entrybook and a begin-end dates order by financial transaction date
     * <p/>
     * Calculate the amounts and the solde
     * <p/>
     * Just balanced transaction are calculated
     */
    @Override
    public ReportsDatas generateEntryBooksReports(EntryBook entryBook,
            Date beginDate, Date endDate) {
        ReportsDatas reportsDatas = new ReportsDatasImpl();
        BigDecimal credit = BigDecimal.ZERO;
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal solde = BigDecimal.ZERO;

        // Get all entries with a topia query
        EntryTopiaDao entryTopiaDao = getDaoHelper().getEntryDao();
        List<Entry> entries = entryTopiaDao.findAllEntryOfBalancedTransaction(entryBook, beginDate, endDate);
        reportsDatas.setListEntry(entries);

        List<Object[]> results = entryTopiaDao.getDebitCreditOfBalancedTransaction(entryBook, beginDate, endDate);
        int nbAmount = results.size();
        if (nbAmount == 2) {
            debit = (BigDecimal) results.get(0)[1];
            credit = (BigDecimal) results.get(1)[1];
        }
        if (nbAmount == 1) {
            if ((Boolean) results.get(0)[0]) {
                debit = (BigDecimal) results.get(0)[1];
            } else {
                credit = (BigDecimal) results.get(0)[1];
            }
        }

        // set the amounts and solde
        //solde = debit - credit;
        solde = solde.add(debit);
        solde = solde.subtract(credit);
        if (solde.compareTo(BigDecimal.ZERO) == 1) {
            reportsDatas.setSoldeDebit(true);
        }
        solde = solde.abs();

        reportsDatas.setAmountCredit(credit);
        reportsDatas.setAmountDebit(debit);
        reportsDatas.setAmountSolde(solde);

        return reportsDatas;
    }

    /**
     * Get balance trial
     * <p/>
     * Calculate the amounts and the solde for all subaccounts
     * <p/>
     * Can have string selectedAccounts in params for account filter
     * <p/>
     * Boolean Param GetEntries is for get entries in datasreports or not :
     * - GetEntries = false for generate balance
     * - GetEntries = true for generate ledger
     */
    @Override
    public BalanceTrial generateBalanceTrial(Date beginDate,
                                             Date endDate,
                                             String selectedAccounts,
                                             Boolean getEntries,
                                             Boolean movementedFilter) {
        BalanceTrial balanceTrial = new BalanceTrialImpl();
        balanceTrial.setReportsDatas(new ArrayList<>());
        BigDecimal credit = BigDecimal.ZERO;
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal balance = BigDecimal.ZERO;

        AccountTopiaDao accountTopiaDao = getDaoHelper().getAccountDao();
        //for each account create a balance sheet with a ReportsDatas

        List<Account> accounts;

        //Remove Spaces
        String result = StringUtils.deleteWhitespace(selectedAccounts);
        //if no filter account
        if (result == null || result.equals("")) {
            accounts = accountTopiaDao.findAll();
        }
        //build list account from selectedAccounts
        else {
            accounts = accountService.stringToListAccounts(result);
        }

        Collections.sort(accounts, new AccountComparator());

        balanceTrial.setFromToAccountNumber(getFromToAccounts(accounts));

        for (Account account : accounts) {
            ReportsDatas reportsDatas =
                    generateSubAccountBalance(account, beginDate,
                                                             endDate, getEntries);
            reportsDatas.setAccount(account);
            BigDecimal amount = reportsDatas.getAmountSolde();
            if (amount == null) {
                amount = BigDecimal.ZERO;
            }
            if (movementedFilter) {
                if (!reportsDatas.getAmountCredit().equals(BigDecimal.ZERO)
                    || !reportsDatas.getAmountDebit().equals(BigDecimal.ZERO)) {
                    // add balance sheet to balance trial
                    balanceTrial.addReportsDatas(reportsDatas);
                }
            } else {
                // add balance sheet to balance trial
                balanceTrial.addReportsDatas(reportsDatas);
            }

            if (reportsDatas.isSoldeDebit()) {
                debit = debit.add(amount);
            } else {
                credit = credit.add(amount);
            }
        }
        // set the amounts and solde
        //solde = debit - credit;
        balance = balance.add(debit);
        balance = balance.subtract(credit);
        if (balance.compareTo(BigDecimal.ZERO) == 1) {
            balanceTrial.setSoldeDebit(true);
        }
        balance = balance.abs();
        balanceTrial.setAmountCredit(credit);
        balanceTrial.setAmountDebit(debit);
        balanceTrial.setAmountSolde(balance);

        return balanceTrial;
    }

    protected String getFromToAccounts(List<Account> accounts) {
        String fromToAccount = "";
        if (accounts.size() > 0) {
            if (accounts.size() < MAX_ACCOUNT_NUMBER_DISPLAYED) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < MAX_ACCOUNT_NUMBER_DISPLAYED - 1; i++) {
                    stringBuilder.append(accounts.get(i).getAccountNumber());
                    stringBuilder.append(ACCOUNTS_SEPARATOR);
                }
                fromToAccount = stringBuilder.append(accounts.get(MAX_ACCOUNT_NUMBER_DISPLAYED)).toString();
            } else {
                fromToAccount = accounts.get(0).getAccountNumber() + " ... " + accounts.get(accounts.size() -1).getAccountNumber();
            }
        }
        return fromToAccount;
    }


    /**
     * Get Ledger
     * <p/>
     * Calculate the amounts and the solde for all subaccounts
     * Get all entries
     */
    @Override
    public BalanceTrial generateLedger(Date beginDate,
                                       Date endDate,
                                       String selectedAccounts,
                                       Boolean movementedFilter) {
        return generateBalanceTrial(beginDate, endDate, selectedAccounts, true, movementedFilter);
    }

    /**
     * Generate VAT
     *
     * @param fiscalPeriod
     * @throws LimaException, TopiaException
     */
    @Override
    public List<Object> generateVat(FiscalPeriod fiscalPeriod) {
        List<Object> list = new ArrayList<>();

        //lists all accounts
        AccountTopiaDao accountTopiaDao = getDaoHelper().getAccountDao();
        List<Account> accountsList;
        //gets all financial periods from the  fiscal period
        List<FinancialPeriod> financialPeriod = financialPeriodService.getFinancialPeriodsWithBeginDateWithin(
                fiscalPeriod.getBeginDate(), fiscalPeriod.getEndDate());
        for (FinancialPeriod fp : financialPeriod) {
            list.add(fp);
            accountsList = accountTopiaDao.findAll();
            for (Account account : accountsList) {
                //VAT accounts start only with number 44
                //FIXME echatellier 44 is hardcoded and depends on
                //french rule ?
                if (account.getAccountNumber().startsWith("44")) {
                    list.add(generateBalanceTrial(fp.getBeginDate(), fp.getEndDate(), account.getAccountNumber(), true, false));
                }
            }
        }
        return list;
    }
}
