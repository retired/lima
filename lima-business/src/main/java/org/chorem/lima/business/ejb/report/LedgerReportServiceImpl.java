package org.chorem.lima.business.ejb.report;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sf.jasperreports.engine.JasperReport;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.beans.BalanceTrial;
import org.chorem.lima.beans.DocumentReport;
import org.chorem.lima.beans.GeneralLedger;
import org.chorem.lima.beans.GeneralLedgerEntry;
import org.chorem.lima.beans.GeneralLedgerEntryImpl;
import org.chorem.lima.beans.GeneralLedgerImpl;
import org.chorem.lima.beans.ReportsDatas;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.ReportService;
import org.chorem.lima.business.api.report.LedgerReportService;
import org.chorem.lima.business.ejb.AbstractLimaService;
import org.chorem.lima.business.utils.EntryComparator;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 20/11/14.
 */
@Stateless
@Remote(LedgerReportService.class)
@TransactionAttribute
public class LedgerReportServiceImpl extends AbstractLimaService implements LedgerReportService {

    @EJB
    protected ReportService reportService;

    @EJB
    protected IdentityService identityService;

    // grand livre
    @Override
    public DocumentReport getLedgerDocumentReport(Date beginDate, Date endDate, DecimalFormat decimalFormat, JasperReport generalLedgersJasperReport, JasperReport entriesJasperReport) {
        String reportName = t("lima-business.document.ledgerReport.title");
        DocumentReport documentReport = CommonsDocumentReport.getDocumentReport(reportName, beginDate, endDate, generalLedgersJasperReport, decimalFormat, identityService);

        documentReport.setColumnDateTitle(t("lima-business.document.setColumnDateTitle"));
        documentReport.setColumnEntryBookTitle(t("lima-business.document.setColumnEntryBookTitle"));
        documentReport.setColumnVoucherTitle(t("lima-business.document.columnVoucherTitle"));
        documentReport.setColumnDescriptionTitle(t("lima-business.document.columnDescriptionTitle"));
        documentReport.setColumnDebitTitle(t("lima-business.document.columnDebitTitle"));
        documentReport.setColumnCreditTitle(t("lima-business.document.columnCreditTitle"));
        documentReport.setColumnBalanceTitle(t("lima-business.document.columnBalanceTitle"));

        if (beginDate != null && endDate != null) {
            try {

                BalanceTrial balanceTrial = reportService.generateLedger(beginDate, endDate, null, true);
                documentReport.setHeaderSelectedAccounts(balanceTrial.getFromToAccountNumber());

                SimpleDateFormat dateFormatter = new SimpleDateFormat(LimaBusinessConfig.getInstance().getDateFormat());
                if (balanceTrial.getReportsDatas() != null) {
                    for (ReportsDatas reportsDatas : balanceTrial.getReportsDatas()) {

                        List<Entry> entries = reportsDatas.getListEntry();
                        if (entries != null) {
                            Account account = reportsDatas.getAccount();

                            if (account != null) {
                                GeneralLedger generalLedger = new GeneralLedgerImpl();
                                generalLedger.setFormatter(decimalFormat);
                                generalLedger.setTotalForAccountText(t("lima-business.document.totalForAccount"));
                                generalLedger.setAccountNumber(account.getAccountNumber());
                                generalLedger.setLabel(account.getLabel());
                                BigDecimal amountCredit = reportsDatas.getAmountCredit();
                                BigDecimal amountDebit = reportsDatas.getAmountDebit();
                                generalLedger.setDebit(amountDebit);
                                generalLedger.setCredit(amountCredit);
                                generalLedger.setBalance(amountDebit.subtract(amountCredit));
                                generalLedger.setSubReport(entriesJasperReport);
                                generalLedger.setNbEntries(balanceTrial.getReportsDatas().size());
                                documentReport.addGeneralLedgers(generalLedger);

                                Collections.sort(entries, new EntryComparator());

                                BigDecimal balance = BigDecimal.ZERO;
                                for (Entry entry : entries) {
                                    String entryBookCode = "";
                                    EntryBook entryBook = entry.getFinancialTransaction().getEntryBook();
                                    if (entryBook != null) {
                                        entryBookCode = entryBook.getCode();
                                    }
                                    BigDecimal entryAmountDebit = BigDecimal.ZERO, entryAmountCredit = BigDecimal.ZERO;

                                    if (entry.isDebit()) {
                                        entryAmountDebit = entry.getAmount();
                                    } else {
                                        entryAmountCredit = entry.getAmount();
                                    }

                                    GeneralLedgerEntry generalLedgerEntry = new GeneralLedgerEntryImpl();
                                    generalLedgerEntry.setFormatter(decimalFormat);
                                    generalLedgerEntry.setDateFormat(dateFormatter);
                                    generalLedgerEntry.setDate(entry.getFinancialTransaction().getTransactionDate());
                                    generalLedgerEntry.setEntryBook(entryBookCode);
                                    generalLedgerEntry.setVoucher(entry.getVoucher());
                                    generalLedgerEntry.setDescription(entry.getDescription());
                                    generalLedgerEntry.setDebit(entryAmountDebit);
                                    generalLedgerEntry.setCredit(entryAmountCredit);
                                    balance = balance.add(entryAmountDebit.subtract(entryAmountCredit));
                                    generalLedgerEntry.setBalance(balance);
                                    generalLedgerEntry.setNbEntries(entries.size());
                                    generalLedger.addEntries(generalLedgerEntry);
                                }
                            }

                        }
                    }
                }
            } catch (Exception ex) {
                throw new LimaTechnicalException("Can't create document", ex);
            }
        }
        return documentReport;
    }
}
