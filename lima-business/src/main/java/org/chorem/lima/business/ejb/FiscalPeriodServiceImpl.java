/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.ejb;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.time.DateUtils;
import org.chorem.lima.beans.BalanceTrial;
import org.chorem.lima.beans.ReportsDatas;
import org.chorem.lima.business.AccountingRules;
import org.chorem.lima.business.LimaBusinessConfig;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.ReportService;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.AlreadyExistAccountException;
import org.chorem.lima.business.exceptions.AlreadyExistEntryBookException;
import org.chorem.lima.business.exceptions.AlreadyLockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LastUnlockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoEmptyFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoFoundFinancialPeriodException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountImpl;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.ClosedPeriodicEntryBookImpl;
import org.chorem.lima.entity.ClosedPeriodicEntryBookTopiaDao;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookTopiaDao;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialPeriodTopiaDao;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionImpl;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodTopiaDao;
import org.nuiton.topia.persistence.TopiaNoResultException;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Gestion des exercices.
 * Un exercice ne peut être supprimé et débloqué après cloture.
 *
 * @author Rémi Chapelet
 */
@Stateless
@Remote(FiscalPeriodService.class)
@TransactionAttribute
public class FiscalPeriodServiceImpl extends AbstractLimaService implements FiscalPeriodService {

    @EJB
    protected AccountService accountService;

    @EJB
    protected FinancialPeriodService financialPeriodService;

    @EJB
    protected EntryBookService entryBookService;

    @EJB
    protected ReportService reportService;

    @EJB
    protected FinancialTransactionService financialTransactionService;

    @Override
    public FiscalPeriod createFiscalPeriod(FiscalPeriod fiscalPeriod)
            throws BeginAfterEndFiscalPeriodException,
            NotBeginNextDayOfLastFiscalPeriodException,
            MoreOneUnlockFiscalPeriodException {

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        // fix begin date at midnight and end date at 23:59:59.999
        Date beginDate = fiscalPeriod.getBeginDate();
        beginDate = DateUtils.truncate(beginDate, Calendar.DATE);
        Date endDate = fiscalPeriod.getEndDate();
        endDate = DateUtils.addDays(endDate, 1);
        endDate = DateUtils.truncate(endDate, Calendar.DATE);
        endDate = DateUtils.addMilliseconds(endDate, -1);
        fiscalPeriod.setBeginDate(beginDate);
        fiscalPeriod.setEndDate(endDate);
        createFiscalPeriodClosePeriodicEntryBooks(fiscalPeriod, accountingRules);

        FiscalPeriod result = fiscalPeriodTopiaDao.create(fiscalPeriod);

        return result;
    }

    protected void createFiscalPeriodClosePeriodicEntryBooks(FiscalPeriod fiscalPeriod, AccountingRules accountingRules) throws
            BeginAfterEndFiscalPeriodException, NotBeginNextDayOfLastFiscalPeriodException, MoreOneUnlockFiscalPeriodException {

        EntryBookTopiaDao entryBookTopiaDao = getDaoHelper().getEntryBookDao();

        List<FinancialPeriod> financialPeriods;

        financialPeriods = accountingRules.createFiscalPeriodRules(fiscalPeriod);

        // create
        fiscalPeriod.addAllFinancialPeriod(financialPeriods);

        //create all financial period
        for (FinancialPeriod financialPeriod : financialPeriods) {

            List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = Lists.newArrayList();
            //create ClosedPeriodicEntryBook for all entrybook
            for (EntryBook entryBook : entryBookTopiaDao.findAll()) {
                //new closed periodic entrybook
                ClosedPeriodicEntryBook closedPeriodicEntryBook = new ClosedPeriodicEntryBookImpl();
                // set entrybook
                closedPeriodicEntryBook.setEntryBook(entryBook);
                // set financial period
                closedPeriodicEntryBook.setFinancialPeriod(financialPeriod);

                closedPeriodicEntryBooks.add(closedPeriodicEntryBook);
            }
            financialPeriod.addAllEntryBookClosedPeriodicEntryBook(closedPeriodicEntryBooks);

        }
    }

    /**
     * return all fiscal period.
     */
    @Override
    public List<FiscalPeriod> getAllFiscalPeriods() {

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();
        List<FiscalPeriod> result = fiscalPeriodTopiaDao
                .forAll()
                .setOrderByArguments(FiscalPeriod.PROPERTY_BEGIN_DATE)
                .findAll();

        return result;
    }

    /**
     * return all blocked fiscal periods.
     */
    @Override
    public List<FiscalPeriod> getAllBlockedFiscalPeriods() {

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();
        List<FiscalPeriod> result = fiscalPeriodTopiaDao
                .forLockedEquals(true)
                .setOrderByArguments(FiscalPeriod.PROPERTY_BEGIN_DATE)
                .findAll();

        return result;
    }


    /** return all unblocked fiscal periods */
    @Override
    public List<FiscalPeriod> getAllUnblockedFiscalPeriods() {

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();
        List<FiscalPeriod>  result = fiscalPeriodTopiaDao
                .forLockedEquals(false)
                .setOrderByArguments(FiscalPeriod.PROPERTY_BEGIN_DATE)
                .findAll();

        return result;
    }

    @Override
    public FiscalPeriod getFirstFiscalPeriod() {

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();
        //get the last fiscal period
        FiscalPeriod result = fiscalPeriodTopiaDao.forAll().setOrderByArguments(FiscalPeriod.PROPERTY_BEGIN_DATE + " ASC").findFirstOrNull();

        return result;
    }

    @Override
    public FiscalPeriod getLastFiscalPeriod() {

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();
        //get the last fiscal period
        FiscalPeriod result = fiscalPeriodTopiaDao.forAll().setOrderByArguments(FiscalPeriod.PROPERTY_BEGIN_DATE + " DESC").findFirstOrNull();

        return result;
    }

    /**
     * to block a fiscal period
     * <p/>
     * check localized rules before block it
     */
    @Override
    public FiscalPeriod blockFiscalPeriod(FiscalPeriod fiscalPeriod)
            throws AlreadyLockedFiscalPeriodException, LastUnlockedFiscalPeriodException,
            UnbalancedFinancialTransactionsException, WithoutEntryBookFinancialTransactionsException,
            UnfilledEntriesException {

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();

        if (fiscalPeriod.isLocked()) {
            throw new AlreadyLockedFiscalPeriodException(fiscalPeriod);
        }
        //check rules
        accountingRules.blockFiscalPeriodRules(fiscalPeriod);

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();

        // Get the old fiscal period instance for no object conflict
        FiscalPeriod oldFiscalPeriod =
                fiscalPeriodTopiaDao.forTopiaIdEquals(fiscalPeriod.getTopiaId()).findUnique();

        // lock fiscalperiod
        oldFiscalPeriod.setLocked(true);
        // locked all financialperiod of the fiscalperiod
        for (FinancialPeriod financialPeriod : oldFiscalPeriod.getFinancialPeriod()) {
            financialPeriod.setLocked(true);
            for (ClosedPeriodicEntryBook closedPeriodicEntryBook : financialPeriod.getEntryBookClosedPeriodicEntryBook()) {
                closedPeriodicEntryBook.setLocked(true);
            }
        }

        FiscalPeriod result = fiscalPeriodTopiaDao.update(oldFiscalPeriod);

        return result;
    }

    @Override
    public boolean isRetainedEarnings(FiscalPeriod fiscalPeriod) {
        List<FinancialTransaction> financialTransactionsList =
                financialTransactionService.getAllFinancialTransactions(
                        fiscalPeriod.getBeginDate(), fiscalPeriod.getEndDate());
        //check if they are at least one transaction to be report
        for (FinancialTransaction financialTransaction : financialTransactionsList) {
            for (Entry entry : financialTransaction.getEntry()) {
                // TODO DCossé 27/02/15 not sure of what to do if entry.getAccount() == null ?
                if (entry.getAccount() != null && Integer.valueOf(entry.getAccount().getAccountNumber().substring(0, 1)) < 6
                         && !entry.getAmount().equals(BigDecimal.ZERO)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public FiscalPeriod updateEndDate(FiscalPeriod fiscalPeriod) {

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();

        String topiaId = fiscalPeriod.getTopiaId();
        FiscalPeriod fiscalPeriodOld = fiscalPeriodTopiaDao.forTopiaIdEquals(topiaId).findUnique();

        Date endDate = fiscalPeriod.getEndDate();
        fiscalPeriodOld.setEndDate(endDate);

        FiscalPeriod result = fiscalPeriodTopiaDao.update(fiscalPeriodOld);

        return result;
    }

    @Override
    public FiscalPeriod retainedEarningsAndBlockFiscalPeriod(FiscalPeriod fiscalPeriod, EntryBook entryBook)
            throws AlreadyExistEntryBookException, AlreadyExistAccountException, NotAllowedLabelException,
            InvalidAccountNumberException, NotNumberAccountNumberException, NoFoundFinancialPeriodException,
            LockedFinancialPeriodException, LockedEntryBookException, UnfilledEntriesException,
            UnbalancedFinancialTransactionsException, LastUnlockedFiscalPeriodException,
            WithoutEntryBookFinancialTransactionsException, AlreadyLockedFiscalPeriodException,
            AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException {

        if (entryBook != null) {

            // re-attach entities to current session

            FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();
            FiscalPeriod localFiscalPeriod = fiscalPeriodTopiaDao.forTopiaIdEquals(fiscalPeriod.getTopiaId()).findUnique();

            //if entrybook isn't found
            //then create it
            if (!entryBook.isPersisted()) {
                entryBook = entryBookService.createEntryBook(entryBook);
            }

            //Sets accounts, check if they exist, if not create them
            //-> 8 COMPTES SPECIAUX
            //   -> 89 BILAN
            //      -> 890 Bilan d'ouverture
            //      -> 891 Bilan de cloture
            Account endRetainedAccount = createRelated89TreeAccounts();


            //look for the last financial period from the previous fiscal year
            //check if the last financial period isn't blocked
            FinancialPeriod lastFPeriod = getLastFinancialPeriodFromPreviousYear(localFiscalPeriod);

            Date beginDateNextFiscalPeriod = DateUtils.addDays(localFiscalPeriod.getEndDate(), 1);

            FinancialPeriodTopiaDao financialPeriodDao = getDaoHelper().getFinancialPeriodDao();
            FinancialPeriod beginFinancialPeriod = findBeginFinancialPeriod(beginDateNextFiscalPeriod, financialPeriodDao);

            validClosedPeriodicEntryBookNotLocked(entryBook, beginFinancialPeriod);

            //holds entries of all closing transactions
            FinancialTransaction endFinancialTransaction = new FinancialTransactionImpl();
            if (lastFPeriod != null) {// TODO DCossé 07/08/14 if null endFinancialTransaction has no EntryBook and it's not valid
                //Sets the endfinancialTransaction
                endFinancialTransaction.setEntryBook(entryBook);
                endFinancialTransaction.setTransactionDate(localFiscalPeriod.getEndDate());
            }

            //holds entries of all opening transactions
            FinancialTransaction beginfinancialTransaction = new FinancialTransactionImpl();

            //Sets the endfinancialTransaction
            beginfinancialTransaction.setEntryBook(entryBook);
            beginfinancialTransaction.setTransactionDate(beginFinancialPeriod.getBeginDate());

            //Sets date for description, e.g: Report à nouveau (DATE)
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fiscalPeriod.getEndDate());

            //Sets entries
            BalanceTrial results = reportService.generateBalanceTrial(localFiscalPeriod.getBeginDate(),localFiscalPeriod.getEndDate(), null, false, false);

            List<ReportsDatas> reportsDatasList = (List<ReportsDatas>) results.getReportsDatas();

            for (ReportsDatas report : reportsDatasList) {
                processReport(endRetainedAccount, endFinancialTransaction, beginfinancialTransaction, calendar, report);
            }
            // persist
            financialTransactionService.createFinancialTransactionWithEntries(beginfinancialTransaction, beginfinancialTransaction.getEntry());
            financialTransactionService.createFinancialTransactionWithEntries(endFinancialTransaction, endFinancialTransaction.getEntry());
        }

        /*block the antepenultimate fiscalPeriod */
        FiscalPeriod fiscalPeriodBlocked = blockFiscalPeriod(fiscalPeriod);
        return fiscalPeriodBlocked;
    }

    protected void processReport(Account endRetainedAccount, FinancialTransaction endFinancialTransaction, FinancialTransaction beginfinancialTransaction, Calendar calendar, ReportsDatas report) throws LockedFinancialPeriodException, LockedEntryBookException {
        //Account class from 1 to 5 and only non zero amount
        if (Integer.valueOf(report.getAccount().getAccountNumber().substring(0, 1)) < 6
            && !report.getAmountSolde().equals(BigDecimal.ZERO)) {

            //close accounts by removing amounts from all class 1 to 5 accounts
            Entry beginEntry = new EntryImpl();
            beginEntry.setDescription(t("lima-business.financialtransaction.retainedearnings.description") + " (" + calendar.get(Calendar.YEAR) + ")");
            beginEntry.setVoucher(t("lima-business.financialtransaction.retainedearnings.voucher"));
            beginEntry.setAccount(report.getAccount());
            beginEntry.setAmount(report.getAmountSolde().abs());
            beginEntry.setDebit(!report.isSoldeDebit());
            endFinancialTransaction.addEntry(beginEntry);

            //save amounts inside account number 891
            beginEntry = new EntryImpl();
            beginEntry.setDescription(t("lima-business.financialtransaction.retainedearnings.description") + " (" + calendar.get(Calendar.YEAR) + ")");
            beginEntry.setVoucher(t("lima-business.financialtransaction.retainedearnings.voucher"));
            beginEntry.setAccount(endRetainedAccount);
            beginEntry.setAmount(report.getAmountSolde().abs());
            beginEntry.setDebit(report.isSoldeDebit());
            endFinancialTransaction.addEntry(beginEntry);

            //open new year accounts if new year exists and a date has been found for the transaction
            //give back amounts from class 1 to 5 accounts
            beginEntry = new EntryImpl();
            beginEntry.setDescription(t("lima-business.financialtransaction.retainedearnings.description") + " (" + calendar.get(Calendar.YEAR) + ")");
            beginEntry.setVoucher(t("lima-business.financialtransaction.retainedearnings.voucher"));
            beginEntry.setAccount(report.getAccount());
            beginEntry.setAmount(report.getAmountSolde().abs());
            beginEntry.setDebit(report.isSoldeDebit());
            beginfinancialTransaction.addEntry(beginEntry);

            //close account by removing amount from account number 890
            beginEntry = new EntryImpl();
            beginEntry.setDescription(t("lima-business.financialtransaction.retainedearnings.description") + " (" + calendar.get(Calendar.YEAR) + ")");
            beginEntry.setVoucher(t("lima-business.financialtransaction.retainedearnings.voucher"));
            beginEntry.setAccount(endRetainedAccount);
            beginEntry.setAmount(report.getAmountSolde().abs());
            beginEntry.setDebit(!report.isSoldeDebit());
            beginfinancialTransaction.addEntry(beginEntry);
        }
    }


    protected void validClosedPeriodicEntryBookNotLocked(EntryBook entryBook, FinancialPeriod beginFinancialPeriod) throws LockedEntryBookException {
        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookDao = getDaoHelper().getClosedPeriodicEntryBookDao();
        ClosedPeriodicEntryBook closedPeriodicEntryBook = closedPeriodicEntryBookDao.findByEntryBookAndFinancialPeriod(entryBook, beginFinancialPeriod);
        if (closedPeriodicEntryBook.isLocked()) {
            throw  new LockedEntryBookException(closedPeriodicEntryBook);
        }
    }

    protected FinancialPeriod findBeginFinancialPeriod(Date beginDateNextFiscalPeriod, FinancialPeriodTopiaDao financialPeriodDao) throws NoFoundFinancialPeriodException, LockedFinancialPeriodException {
        FinancialPeriod beginFinancialPeriod;
        try {
            beginFinancialPeriod = financialPeriodDao.findByDate(beginDateNextFiscalPeriod);
        } catch (TopiaNoResultException e) {
            throw new NoFoundFinancialPeriodException(beginDateNextFiscalPeriod, e);
        }

        if (beginFinancialPeriod.isLocked()) {
            throw new LockedFinancialPeriodException(beginFinancialPeriod);
        }
        return beginFinancialPeriod;
    }

    protected FinancialPeriod getLastFinancialPeriodFromPreviousYear(FiscalPeriod localFiscalPeriod) {
        Collection<FinancialPeriod> fperiod = localFiscalPeriod.getFinancialPeriod();
        FinancialPeriod lastFPeriod = null;
        for (FinancialPeriod aFperiod : fperiod) {
            lastFPeriod = aFperiod;
        }
        return lastFPeriod;
    }

    protected Account createRelated89TreeAccounts() throws AlreadyExistAccountException, InvalidAccountNumberException, NotNumberAccountNumberException, NotAllowedLabelException {
        //89 BILAN
        Account accountMaster = accountService.getAccountByNumber("89");
        if (accountMaster == null) {
            createAccount("89", "BILAN");
        }

        //890 Bilan d'ouverture
        Account beginRetainedAccount = accountService.getAccountByNumber("890");
        if (beginRetainedAccount == null) {
            createAccount("890", "Bilan d'ouverture");
        }

        //891 Bilan de cloture
        Account endRetainedAccount = accountService.getAccountByNumber("891");
        if (endRetainedAccount == null) {
            endRetainedAccount = createAccount("891", "Bilan de clôture");
        }
        return endRetainedAccount;
    }

    protected Account createAccount(String accountNumber, String label) throws AlreadyExistAccountException, InvalidAccountNumberException, NotNumberAccountNumberException {
        Account endRetainedAccount;
        endRetainedAccount = new AccountImpl();
        endRetainedAccount.setAccountNumber(accountNumber);
        endRetainedAccount.setLabel(label);
        endRetainedAccount = accountService.createAccount(endRetainedAccount);
        return endRetainedAccount;
    }

    /**
     * to delete a fiscal period
     * <p/>
     * check localized rules before delete it
     */
    @Override
    public void deleteFiscalPeriod(FiscalPeriod fiscalPeriod) throws NoEmptyFiscalPeriodException {

        AccountingRules accountingRules = LimaBusinessConfig.getInstance().getAccountingRules();
        //get entities with TopiaDao
        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();

        // re-attach entities to current session
        FiscalPeriod localFiscalPeriod = fiscalPeriodTopiaDao.forTopiaIdEquals(fiscalPeriod.getTopiaId()).findUnique();

        //check rules
        accountingRules.deleteFiscalPeriodRules(localFiscalPeriod);

        //all "ClosedPeriodicEntryBooks" for the fiscal period
        ClosedPeriodicEntryBookTopiaDao closedPeriodicEntryBookTopiaDao = getDaoHelper().getClosedPeriodicEntryBookDao();
        List<ClosedPeriodicEntryBook>  closedPeriodicEntryBookTopiaDaoList = closedPeriodicEntryBookTopiaDao.findAllClosedPeriodicEntryBooksFromFiscalPeriod(localFiscalPeriod);

        //delete closedPeriodicEntryBook before fiscal period
        for (ClosedPeriodicEntryBook closedPeriodicEntryBook : closedPeriodicEntryBookTopiaDaoList) {
            // fix : ObjectDeletedException: deleted object would be re-saved by cascade (Need to delete association between entryBook and financial period via closedPeriodEntityBook)
            closedPeriodicEntryBook.getEntryBook().removeFinancialPeriodClosedPeriodicEntryBook(closedPeriodicEntryBook);
            closedPeriodicEntryBookTopiaDao.delete(closedPeriodicEntryBook);
        }

        fiscalPeriodTopiaDao.delete(localFiscalPeriod);
    }


    @Override
    public FiscalPeriod getFiscalPeriodForDate(Date date) {

        FiscalPeriodTopiaDao fiscalPeriodTopiaDao = getDaoHelper().getFiscalPeriodDao();
        //get the last fiscal period
        FiscalPeriod result = fiscalPeriodTopiaDao.findForDate(date);

        return result;
    }
}
