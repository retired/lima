package org.chorem.lima.business.ejb;

/*
 * #%L
 * Lima :: Business
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.chorem.lima.business.api.TreasuryService;
import org.chorem.lima.entity.Treasury;
import org.chorem.lima.entity.TreasuryTopiaDao;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.util.List;

/**
 * Created by davidcosse on 22/07/15.
 */
@Stateless
@Remote(TreasuryService.class)
@TransactionAttribute
public class TreasuryServiceImpl extends AbstractLimaService implements TreasuryService {

    @Override
    public Treasury getTreasury() {
        Treasury treasury;

        TreasuryTopiaDao dao = getDaoHelper().getTreasuryDao();
        List<Treasury> treasuries = dao.findAll();
        if (treasuries.size() == 0) {
            treasury = dao.create(
                    Treasury.PROPERTY_ADDRESS, "",
                    Treasury.PROPERTY_ZIP_CODE, "",
                    Treasury.PROPERTY_CITY, "",
                    Treasury.PROPERTY_SYSTEM_TYPE, "",
                    Treasury.PROPERTY_DOSSIER_NUMBER, "",
                    Treasury.PROPERTY_KEY, "",
                    Treasury.PROPERTY_CDI, "",
                    Treasury.PROPERTY_SERVICE_CODE, ""
            );
        } else {
            treasury = treasuries.get(0);
        }
        return treasury;
    }

    @Override
    public Treasury updateTreasury(Treasury treasury) {
        Preconditions.checkArgument(treasury != null);

        Treasury originalTreasury = getTreasury();

        bindData(treasury, originalTreasury);

        Treasury updatedTreasury = doUpdate(originalTreasury);

        return updatedTreasury;
    }

    protected Treasury doUpdate(Treasury originalTreasury) {
        TreasuryTopiaDao dao = getDaoHelper().getTreasuryDao();
        return dao.update(originalTreasury);
    }

    protected void bindData(Treasury treasury, Treasury originalTreasury) {
        Binder<Treasury, Treasury> binder = BinderFactory.newBinder(Treasury.class);
        binder.copyExcluding(treasury, originalTreasury,
                Treasury.PROPERTY_TOPIA_ID);
    }
}
