package org.chorem.lima.business.ejb;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.beans.EntryEBP;
import org.chorem.lima.beans.FinancialStatementImport;
import org.chorem.lima.beans.VatStatementImport;
import org.chorem.lima.business.ImportExportResults;
import org.chorem.lima.business.ImportResult;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.EntryService;
import org.chorem.lima.business.api.FinancialStatementService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.api.VatStatementService;
import org.chorem.lima.business.ejb.csv.AccountModel;
import org.chorem.lima.business.ejb.csv.EntryBookModel;
import org.chorem.lima.business.ejb.csv.EntryModel;
import org.chorem.lima.business.ejb.csv.FinancialStatementModel;
import org.chorem.lima.business.ejb.csv.FinancialTransactionModel;
import org.chorem.lima.business.ejb.csv.FiscalPeriodModel;
import org.chorem.lima.business.ejb.csv.IdentityModel;
import org.chorem.lima.business.ejb.csv.VatStatementModel;
import org.chorem.lima.business.ejb.ebp.AccountEBPModel;
import org.chorem.lima.business.ejb.ebp.EntryBookEBPModel;
import org.chorem.lima.business.ejb.ebp.EntryEBPModel;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.AlreadyAffectedVatBoxException;
import org.chorem.lima.business.exceptions.AlreadyExistEntryBookException;
import org.chorem.lima.business.exceptions.AlreadyExistFinancialStatementException;
import org.chorem.lima.business.exceptions.AlreadyExistVatStatementException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.ImportFileException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoDataToImportException;
import org.chorem.lima.business.exceptions.NoFiscalPeriodFoundException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.utils.EntryEBPComparator;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialPeriodTopiaDao;
import org.chorem.lima.entity.FinancialStatement;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodImpl;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.VatStatement;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 03/06/14.
 */
@Stateless
@Remote(ImportService.class)
@TransactionAttribute
public class ImportServiceImpl extends AbstractLimaService implements ImportService {

    @EJB
    protected EntryBookService entryBookService;

    @EJB
    protected AccountService accountService;

    @EJB
    protected FinancialTransactionService financialTransactionService;

    @EJB
    protected FiscalPeriodService fiscalPeriodService;

    @EJB
    protected EntryService entryService;

    @EJB
    protected FinancialStatementService financialStatementService;

    @EJB
    protected VatStatementService vatStatementService;

    @EJB
    protected IdentityService identityService;

    protected static final Function<Account, String> GET_ACCOUNT_NUMBER = new Function<Account, String>() {
        @Override
        public String apply(Account input) {
            return input.getAccountNumber();
        }
    };

    protected static final Function<EntryBook, String> GET_ENTRY_BOOK_CODE = new Function<EntryBook, String>() {
        @Override
        public String apply(EntryBook input) {
            return input.getCode();
        }
    };

    @Override
    public void removeAccountabilityLayouts() {
        // vat
        vatStatementService.removeAllVatStatement();
        // financialstatement
        financialStatementService.removeAllFinancialStatement();
        // accounts
        accountService.removeAllAccounts();
    }

    @Override
    public void removeInitallyImportedEntryBook() {
        entryBookService.removeAllEntryBooks();
    }

    @Override
    public ImportExportResults importAccountAsCSV(String contents) {

        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(Account.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<Account> model = new AccountModel();
                Import<Account> accounts = Import.newImport(model, contentStream);
                // csv line index
                boolean updated;
                for (Account account : accounts) {
                    try {
                        updated = accountService.createOrUpdateAccount(account);
                        if (updated) {
                            result.increaseUpdated();
                        } else {
                            result.increaseCreated();
                        }
                    } catch (InvalidAccountNumberException | NotNumberAccountNumberException | NotAllowedLabelException e) {
                        result.addException(e);
                        results.setErrors(true);
                    }
                }
            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
                results.setErrors(true);
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }
        return results;
    }

    @Override
    public ImportExportResults importEntryBooksAsCSV(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(EntryBook.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            InputStream contentStream = IOUtils.toInputStream(contents);

            try {
                ImportModel<EntryBook> model = new EntryBookModel();
                Import<EntryBook> entryBooks = Import.newImport(model, contentStream);
                for (EntryBook entryBook : entryBooks) {
                    boolean updated = entryBookService.createOrUpdateEntryBook(entryBook);
                    if(updated) {
                        result.increaseUpdated();
                    } else {
                        result.increaseCreated();
                    }
                }
            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
                results.setErrors(true);
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }
        return results;
    }


    @Override
    public ImportExportResults importFiscalPeriodsAsCSV(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(FiscalPeriod.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<FiscalPeriod> model = new FiscalPeriodModel();

                Import<FiscalPeriod> fiscalPeriods = Import.newImport(model, contentStream);

                List<FiscalPeriod> orderedFiscalPeriods = Ordering.from(FiscalPeriodImpl.BEGIN_DATE_COMPARATOR).sortedCopy(fiscalPeriods);
                try {
                    for (FiscalPeriod fiscalPeriod : orderedFiscalPeriods) {
                        fiscalPeriodService.createFiscalPeriod(fiscalPeriod);
                        result.increaseCreated();
                    }
                } catch (MoreOneUnlockFiscalPeriodException | BeginAfterEndFiscalPeriodException | NotBeginNextDayOfLastFiscalPeriodException e) {
                    result.addException(e);
                    results.setErrors(true);
                }
            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
                results.setErrors(true);
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }

        return results;
    }

    /**
     *
     * @param contents financial transaction to import
     * @return
     */
    protected ImportExportResults importFinancialTransactionsAsCSV(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(FinancialTransaction.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            // import and save FinancialTransactions
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<FinancialTransaction> model = new FinancialTransactionModel(entryBookService);

                Import<FinancialTransaction> financialTransactions = Import.newImport(model, contentStream);

                for (FinancialTransaction financialTransaction : financialTransactions) {
                    try {
                        // from backup don't check fiscal period close
                        financialTransactionService.createFinancialTransactionSkeleton(financialTransaction, false);
                        result.increaseCreated();
                    } catch (LockedFinancialPeriodException | LockedEntryBookException | BeforeFirstFiscalPeriodException | AfterLastFiscalPeriodException e) {
                        result.addException(e);
                        results.setErrors(true);
                    }
                }

            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
                results.setErrors(true);
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }
        return results;
    }

    @Override
    public ImportExportResults importEntriesAsCSV(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(Entry.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            // import and save entries
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<Entry> model = new EntryModel(accountService, entryBookService, financialTransactionService, true);
                Import<Entry> entries = Import.newImport(model, contentStream);
                Set<FinancialTransaction> financialTransactions = Sets.newHashSet();

                for (Entry entry : entries) {
                    FinancialTransaction financialTransaction = entry.getFinancialTransaction();
                    financialTransaction.addEntry(entry);
                    financialTransactions.add(financialTransaction);
                    result.increaseCreated();
                }
                for (FinancialTransaction financialTransaction : financialTransactions) {
                    financialTransactionService.createFinancialTransactionWithEntries(financialTransaction, financialTransaction.getEntry());
                }
            } catch (Exception e) {
                if (e instanceof LimaException) {
                    result.addInitException((LimaException) e);
                } else {
                    result.addInitException(new ImportFileException(e.getMessage()));
                }
                results.setErrors(true);
            } finally {
                IOUtils.closeQuietly(contentStream);
            }

        }
        return results;
    }

    protected ImportExportResults importEntriesAsCSVBackUp(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(Entry.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            // import and save entries
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<Entry> model = new EntryModel(accountService, entryBookService, financialTransactionService, false);
                Import<Entry> entries = Import.newImport(model, contentStream);
                List<Entry> entiesToSave = Lists.newArrayList(entries);
                for (int i = 0; i < entiesToSave.size(); i++) {
                    result.increaseCreated();

                }
                entryService.saveAllEntries(entiesToSave);
            } catch (Exception e) {
                result.addInitException(new ImportFileException(e.getMessage()));
                results.setErrors(true);
            } finally {
                IOUtils.closeQuietly(contentStream);
            }

        }
        return results;
    }

    public ImportExportResults importIdentityAsCSV(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(Identity.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            InputStream contentStream = null;
            // import and save identity
            try {
                contentStream = IOUtils.toInputStream(contents);
                ImportModel<Identity> model = new IdentityModel();
                Import<Identity> identities = Import.newImport(model, contentStream);
                for (Identity identity : identities) {
                    identityService.updateIdentity(identity);
                    result.increaseCreated();
                }
                IOUtils.closeQuietly(contentStream);
            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
                results.setErrors(true);
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }
        return results;
    }

    protected FinancialStatement returnFinancialStatement (final FinancialStatement rootFinancialStatement, String subFinancialStatementLabel) throws AlreadyExistFinancialStatementException, NotAllowedLabelException {
        FinancialStatement targetedFinancialStatement = null;
        if (rootFinancialStatement != null) {
            Collection<FinancialStatement> subFinancialStatements = rootFinancialStatement.getSubFinancialStatements();

            // look for financial statement from tree range
            if (subFinancialStatements != null) {
                for (FinancialStatement subFinancialStatement : subFinancialStatements) {
                    if(subFinancialStatement.getLabel().equals(subFinancialStatementLabel)){
                        targetedFinancialStatement = subFinancialStatement;
                        break;
                    }
                }
            }

            // the target financialStatement has not been created yet so we create it now.
            if (targetedFinancialStatement == null) {
                // not found, we need to create it
                targetedFinancialStatement = financialStatementService.newFinancialStatement();
                targetedFinancialStatement.setLabel(subFinancialStatementLabel);
                // create targetedFinancialStatement and rootFinancialStatement if needed
                targetedFinancialStatement = financialStatementService.createFinancialStatement(rootFinancialStatement, targetedFinancialStatement);
                targetedFinancialStatement.getMasterFinancialStatement();
            }
        }

        return targetedFinancialStatement;
    }

    protected FinancialStatement returnRootFinancialStatement(final FinancialStatement financialStatement) {
        FinancialStatement rootFinancialStatement = null;
        FinancialStatement interFinancialStatement = financialStatement;
        if (interFinancialStatement != null) {
            while (rootFinancialStatement == null) {
                if (interFinancialStatement.getMasterFinancialStatement() == null){
                    rootFinancialStatement = interFinancialStatement;
                } else {
                    interFinancialStatement = interFinancialStatement.getMasterFinancialStatement();
                }
            }
        }
        return rootFinancialStatement;
    }

    @Override
    public ImportExportResults importFinancialStatementsAsCSV(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(FinancialStatement.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {

            // import and save FinancialTransactions
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<FinancialStatementImport> model = new FinancialStatementModel();

                Import<FinancialStatementImport> financialStatementImports = Import.newImport(model, contentStream);

                // path, FinancialStatement
                Map<String, FinancialStatement> orderedFinancialStatements = new HashMap<>();

                List<FinancialStatement> rootFinancialStatements = financialStatementService.getRootFinancialStatements();
                for (FinancialStatement rootFinancialStatement : rootFinancialStatements) {
                    orderedFinancialStatements.put(rootFinancialStatement.getLabel(), rootFinancialStatement);
                }

                for (FinancialStatementImport financialStatementBean : financialStatementImports) {
                    processFinancialStatementImport(result, orderedFinancialStatements, financialStatementBean);
                }

            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
                results.setErrors(true);
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }

        return results;
    }

    protected void processFinancialStatementImport(ImportResult result, Map<String, FinancialStatement> orderedFinancialStatements, final FinancialStatementImport financialStatementBean) {
        Binder<FinancialStatementImport, FinancialStatement> binder = BinderFactory.newBinder(FinancialStatementImport.class, FinancialStatement.class);
        FinancialStatement financialStatement = financialStatementService.newFinancialStatement();
        binder.copyExcluding(financialStatementBean, financialStatement, FinancialStatement.PROPERTY_MASTER_FINANCIAL_STATEMENT);

        try{
            // full path to master
            String masterPath = financialStatementBean.getMasterFinancialStatement();

            if (StringUtils.isBlank(masterPath)) {
                // case of financialStatement is root
                // look if root exists
                // It can not have several FinancialStatement with the same from same path
                FinancialStatement rootFinancialStatement = getRootFinancialStatement(orderedFinancialStatements, financialStatementBean, financialStatement);
                orderedFinancialStatements.put(rootFinancialStatement.getLabel(), rootFinancialStatement);
            } else {
                String[] masterNames = masterPath.split("/");
                String rootMasterName = masterNames[0];

                FinancialStatement rootFinancialStatement = orderedFinancialStatements.get(rootMasterName);

                // case of not ordered import and subFinancialStatement is looking for it's master that has not been created yet
                rootFinancialStatement = createRootFinancialStatement(orderedFinancialStatements, rootMasterName, rootFinancialStatement);

                // explore branches to find the financialStatement's master one
                FinancialStatement branchesFinancialStatement = rootFinancialStatement;
                branchesFinancialStatement = getBranchFinancialStatement(masterNames, branchesFinancialStatement);

                // in case it exist (not ordered import and previously created) values are bind to the previously created one excepted
                // the sub financial statements
                boolean alreadyCreated = propagetChangesToExistingFinancialStatement(financialStatement, branchesFinancialStatement, false);

                // if necessary financial statement is created
                if (!alreadyCreated) {
                    createRootFinancialStatement(orderedFinancialStatements, financialStatement, branchesFinancialStatement);
                }

            }
            result.increaseCreated();
        } catch (AlreadyExistFinancialStatementException | NotAllowedLabelException e) {
            result.addException(e);
        }
    }

    private FinancialStatement getRootFinancialStatement(Map<String, FinancialStatement> orderedFinancialStatements, final FinancialStatementImport financialStatementBean, final FinancialStatement financialStatement) {
        FinancialStatement rootFinancialStatement = orderedFinancialStatements.get(financialStatementBean.getLabel());

        if (rootFinancialStatement == null) {
            rootFinancialStatement = financialStatement;
            rootFinancialStatement = financialStatementService.createMasterFinacialStatements(rootFinancialStatement);
        } else {
            // in case it exist (not ordered import and previously created) values are bind to the previously created one excepted
            // the sub financial statements
            Binder<FinancialStatement, FinancialStatement> rootBinder = BinderFactory.newBinder(FinancialStatement.class, FinancialStatement.class);
            rootBinder.copyExcluding(financialStatement, rootFinancialStatement,
                    FinancialStatement.PROPERTY_SUB_FINANCIAL_STATEMENTS,
                    FinancialStatement.PROPERTY_TOPIA_ID,
                    FinancialStatement.PROPERTY_TOPIA_VERSION,
                    FinancialStatement.PROPERTY_TOPIA_CREATE_DATE);
        }
        return rootFinancialStatement;
    }

    private FinancialStatement getBranchFinancialStatement(String[] masterNames, final FinancialStatement branchesFinancialStatement) throws AlreadyExistFinancialStatementException, NotAllowedLabelException {
        FinancialStatement _branchesFinancialStatement = branchesFinancialStatement;
        if (_branchesFinancialStatement != null) {
            // 0 is root, start from 1
            for (int i = 1; i < masterNames.length; i++) {
                String masterName = masterNames[i];
                _branchesFinancialStatement = returnFinancialStatement(_branchesFinancialStatement, masterName);
            }
        }
        return _branchesFinancialStatement;
    }

    protected void createRootFinancialStatement(Map<String, FinancialStatement> orderedFinancialStatements, final FinancialStatement financialStatement, final FinancialStatement branchesFinancialStatement) throws AlreadyExistFinancialStatementException, NotAllowedLabelException {
        // if the master finacial statement has been modified then the current one is replace by the new one.
        FinancialStatement _financialStatement = financialStatementService.createFinancialStatement(branchesFinancialStatement, financialStatement);
        FinancialStatement targetedRootFinancialStatement = returnRootFinancialStatement(_financialStatement);

        // replace modified root financial statement with new one
        if (orderedFinancialStatements.get(targetedRootFinancialStatement.getLabel()) != null) {
            orderedFinancialStatements.put(targetedRootFinancialStatement.getLabel(), targetedRootFinancialStatement);
        }
    }

    protected boolean propagetChangesToExistingFinancialStatement(final FinancialStatement financialStatement, final FinancialStatement branchesFinancialStatement, boolean alreadyCreated) {
        if (branchesFinancialStatement != null && branchesFinancialStatement.getSubFinancialStatements() != null) {
            for (FinancialStatement bfs : branchesFinancialStatement.getSubFinancialStatements()) {
                if (bfs.getLabel().equals(financialStatement.getLabel())){
                    Binder<FinancialStatement, FinancialStatement> rootBinder = BinderFactory.newBinder(FinancialStatement.class, FinancialStatement.class);
                    rootBinder.copyExcluding(bfs, financialStatement, FinancialStatement.PROPERTY_SUB_FINANCIAL_STATEMENTS);
                    alreadyCreated = true;
                    break;
                }
            }
        }
        return alreadyCreated;
    }

    protected FinancialStatement createRootFinancialStatement(Map<String, FinancialStatement> orderedFinancialStatements, String rootMasterName, FinancialStatement rootFinancialStatement) {
        if (rootFinancialStatement == null) {
            rootFinancialStatement = financialStatementService.newFinancialStatement();
            rootFinancialStatement.setLabel(rootMasterName);
            rootFinancialStatement = financialStatementService.createMasterFinacialStatements(rootFinancialStatement);
            orderedFinancialStatements.put(rootMasterName, rootFinancialStatement);
        }
        return rootFinancialStatement;
    }

    protected VatStatement returnVATStatement (final VatStatement rootVATStatement, String subVATStatementLabel) throws AlreadyExistVatStatementException, NotAllowedLabelException, AlreadyAffectedVatBoxException {
        VatStatement targetedVATStatement = null;
        if (rootVATStatement != null) {
            Collection<VatStatement> subVatStatements = rootVATStatement.getSubVatStatements();

            // look for vatStatement from tree range
            if (subVatStatements != null) {
                for (VatStatement subVatStatement : subVatStatements) {
                    if(subVatStatement.getLabel().equals(subVATStatementLabel)){
                        targetedVATStatement = subVatStatement;
                        break;
                    }
                }
            }

            if (targetedVATStatement == null) {
                // not found, we need to create it
                targetedVATStatement = vatStatementService.newVatStatement();
                targetedVATStatement.setLabel(subVATStatementLabel);
                // create targetedVATStatement and rootVATStatement if needed
                targetedVATStatement = vatStatementService.createVatStatement(rootVATStatement, targetedVATStatement);
                targetedVATStatement.getMasterVatStatement();
            }
        }

        return targetedVATStatement;
    }

    protected VatStatement returnRootVATStatement(final VatStatement vatStatement) {
        VatStatement rootVatStatement = null;
        VatStatement _vatStatement = vatStatement;

        if (_vatStatement != null) {
            while (rootVatStatement == null) {
                if (_vatStatement.getMasterVatStatement() == null){
                    rootVatStatement = _vatStatement;
                } else {
                    _vatStatement = _vatStatement.getMasterVatStatement();
                }
            }
        }

        return rootVatStatement;
    }

    @Override
    public ImportExportResults importVATStatementsAsCSV(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(VatStatement.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            // import and save VATStatements
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<VatStatementImport> model = new VatStatementModel();

                Import<VatStatementImport> vatStatementImports = Import.newImport(model, contentStream);

                // path, vatStatement
                Map<String, VatStatement> orderedVATStatements = new HashMap<>();

                orderVATStatements(orderedVATStatements);

                for (VatStatementImport vatStatementBean : vatStatementImports) {
                    importVatStatement(result, orderedVATStatements, vatStatementBean);
                }

            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }
        return results;
    }

    protected void importVatStatement(ImportResult result, Map<String, VatStatement> orderedVATStatements, final VatStatementImport vatStatementBean) {
        Binder<VatStatementImport, VatStatement> binder = BinderFactory.newBinder(VatStatementImport.class, VatStatement.class);
        VatStatement vatStatement = vatStatementService.newVatStatement();
        binder.copyExcluding(vatStatementBean, vatStatement, VatStatement.PROPERTY_MASTER_VAT_STATEMENT);

        try{
            // full path to master
            String masterPath = vatStatementBean.getMasterVatStatement();

            if (StringUtils.isBlank(masterPath)) {
                // case of vatStatement is root
                // look if root exists
                // It can not have several vatStatement with the same from same path
                VatStatement rootVATStatement = getRootVatStatement(orderedVATStatements, vatStatementBean, vatStatement);
                orderedVATStatements.put(rootVATStatement.getLabel(), rootVATStatement);
            } else {
                String[] masterNames = masterPath.split("/");
                String rootMasterName = masterNames[0];

                VatStatement rootVATStatement = orderedVATStatements.get(rootMasterName);

                // case of not ordered import and subVATStatement is looking for it's master that has not been created yet
                rootVATStatement = createRootVATStatement(orderedVATStatements, rootMasterName, rootVATStatement);

                // explore branches to find the vatStatement's master one
                VatStatement branchesVATStatement = rootVATStatement;
                branchesVATStatement = getBrancheVatStatement(masterNames, branchesVATStatement);

                // in case it exist (not ordered import and previously created) values are bind to the previously created one excepted
                // the sub vatStatements
                boolean alreadyCreated = propagateVATChangeToBranche(vatStatement, branchesVATStatement, false);

                // if necessary vatStatement is created
                if (!alreadyCreated) {
                    refreshMasterVATStatement(orderedVATStatements, vatStatement, branchesVATStatement);

                }

            }
            result.increaseCreated();
        } catch (AlreadyExistVatStatementException | AlreadyAffectedVatBoxException | NotAllowedLabelException e) {
            result.addException(e);
        }
    }

    private void refreshMasterVATStatement(Map<String, VatStatement> orderedVATStatements, final VatStatement vatStatement, final VatStatement branchesVATStatement) throws AlreadyExistVatStatementException, NotAllowedLabelException, AlreadyAffectedVatBoxException {
        // if the master vatStatement has been modified then the current one is replace by the new one.
        VatStatement _vatStatement = vatStatementService.createVatStatement(branchesVATStatement, vatStatement);
        VatStatement targetedRootVATStatement = returnRootVATStatement(_vatStatement);

        // replace modified root vatStatement with new one
        if (orderedVATStatements.get(targetedRootVATStatement.getLabel()) != null) {
            orderedVATStatements.put(targetedRootVATStatement.getLabel(), targetedRootVATStatement);
        }
    }

    private boolean propagateVATChangeToBranche(final VatStatement vatStatement, final VatStatement branchesVATStatement, boolean alreadyCreated) {
        if (branchesVATStatement != null && branchesVATStatement.getSubVatStatements() != null) {
            for (VatStatement bfs : branchesVATStatement.getSubVatStatements()) {
                if (bfs.getLabel().equals(vatStatement.getLabel())){
                    Binder<VatStatement, VatStatement> rootBinder = BinderFactory.newBinder(VatStatement.class, VatStatement.class);
                    rootBinder.copyExcluding(bfs, vatStatement, VatStatement.PROPERTY_SUB_VAT_STATEMENTS);
                    alreadyCreated = true;
                    break;
                }
            }
        }
        return alreadyCreated;
    }

    private VatStatement getBrancheVatStatement(String[] masterNames, final VatStatement branchesVATStatement) throws AlreadyExistVatStatementException, NotAllowedLabelException, AlreadyAffectedVatBoxException {
        VatStatement _branchesVATStatement = branchesVATStatement;
        for (int i = 1; i < masterNames.length; i++) {// 0 is root
            String masterName = masterNames[i];
            _branchesVATStatement = returnVATStatement(_branchesVATStatement, masterName);
        }
        return _branchesVATStatement;
    }

    private VatStatement createRootVATStatement(Map<String, VatStatement> orderedVATStatements, String rootMasterName, final VatStatement rootVATStatement) throws AlreadyExistVatStatementException, NotAllowedLabelException, AlreadyAffectedVatBoxException {
        VatStatement result = rootVATStatement;
        if (result == null) {
            result = vatStatementService.newVatStatement();
            result.setLabel(rootMasterName);
            result = vatStatementService.createVatStatement(null, result);
            orderedVATStatements.put(rootMasterName, result);
        }
        return result;
    }

    private VatStatement getRootVatStatement(Map<String, VatStatement> orderedVATStatements, final VatStatementImport vatStatementBean, final VatStatement vatStatement) throws AlreadyExistVatStatementException, NotAllowedLabelException, AlreadyAffectedVatBoxException {

        VatStatement rootVATStatement = null;
        if (vatStatementBean != null) {
            rootVATStatement = orderedVATStatements.get(vatStatementBean.getLabel());

            if (rootVATStatement == null) {
                rootVATStatement = vatStatement;
                rootVATStatement = vatStatementService.createVatStatement(null, rootVATStatement);
            } else {
                // in case it exist (not ordered import and previously created) values are bind to the previously created one excepted
                // the sub vatStatements
                Binder<VatStatement, VatStatement> rootBinder = BinderFactory.newBinder(VatStatement.class, VatStatement.class);
                rootBinder.copyExcluding(vatStatement, rootVATStatement, VatStatement.PROPERTY_SUB_VAT_STATEMENTS);
            }
        }

        return rootVATStatement;
    }

    protected void orderVATStatements(Map<String, VatStatement> orderedVATStatements) {
        List<VatStatement> rootVatStatements = vatStatementService.getRootVatStatements();
        for (VatStatement vatStatement : rootVatStatements) {
            orderedVATStatements.put(vatStatement.getLabel(), vatStatement);
        }
    }

    @Override
    public ImportExportResults importAll(String entryBooks, String financialTransactions, String fiscalPeriods, String accounts, String entries, String identity) {
        ImportExportResults globalResult = importAccountAsCSV(accounts);
        if (globalResult.isErrors()){
            return globalResult;
        }

        ImportExportResults subReport = importEntryBooksAsCSV(entryBooks);
        globalResult.pushImportResults(subReport);
        if (subReport.isErrors()){
            return globalResult;
        }

        subReport = importFiscalPeriodsAsCSV(fiscalPeriods);
        globalResult.pushImportResults(subReport);
        if (subReport.isErrors()){
            return globalResult;
        }

        subReport = importFinancialTransactionsAsCSV(financialTransactions);
        globalResult.pushImportResults(subReport);
        if (subReport.isErrors()){
            return globalResult;
        }

        subReport = importEntriesAsCSVBackUp(entries);
        globalResult.pushImportResults(subReport);
        if (subReport.isErrors()){
            return globalResult;
        }

        subReport = importIdentityAsCSV(identity);
        globalResult.pushImportResults(subReport);
        if (subReport.isErrors()){
            return globalResult;
        }

        return globalResult;
    }

    //####################################### EBP ##############################################


    @Override
    public ImportExportResults importAccountFromEbp(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(Account.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<Account> model = new AccountEBPModel();
                contentStream = IOUtils.toInputStream(contents);
                Import<Account> accounts = Import.newImport(model, contentStream);

                for (Account account : accounts) {
                    boolean updated = false;
                    try {
                        updated = accountService.createOrUpdateAccount(account);
                    } catch (InvalidAccountNumberException | NotNumberAccountNumberException | NotAllowedLabelException e) {
                        result.addException(e);
                    }
                    if (updated) {
                        result.increaseUpdated();
                    } else {
                        result.increaseCreated();
                    }
                }
            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }
        return results;
    }

    @Override
    public ImportExportResults importEntryBookFromEbp(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(EntryBook.class);

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            InputStream contentStream = IOUtils.toInputStream(contents);
            try {
                ImportModel<EntryBook> model = new EntryBookEBPModel();
                Import<EntryBook> entryBooks = Import.newImport(model, contentStream);

                for (EntryBook entryEBP : entryBooks) {
                    boolean updated = entryBookService.createOrUpdateEntryBook(entryEBP);
                    if (updated) {
                        result.increaseUpdated();
                    } else {
                        result.increaseCreated();
                    }
                }
            } catch (ImportRuntimeException e) {
                result.addInitException(new ImportFileException(e.getMessage()));
            } finally {
                IOUtils.closeQuietly(contentStream);
            }
        }

        return results;
    }

    protected void basicEntriesFromEBPValidation(String datas, List<FiscalPeriod> fiscalPeriods) throws NoDataToImportException, NoFiscalPeriodFoundException {
        if (datas.isEmpty()) {
            throw new NoDataToImportException();
        }
        // There are no valid fiscalPeriods -> exception
        if (fiscalPeriods.isEmpty()) {
            throw new NoFiscalPeriodFoundException();
        }
    }

    protected List<EntryEBP> loadDateOrderedEntryEBPbeans(String datas) throws ImportFileException {
        List<EntryEBP> result = new ArrayList<>();
        InputStream contentStream = null;
        // convert file to bean
        try {
            contentStream = IOUtils.toInputStream(datas);
            ImportModel<EntryEBP> model = new EntryEBPModel();
            Import<EntryEBP> importedEntryEBPs = Import.newImport(model, contentStream);
            for (EntryEBP entryEBP : importedEntryEBPs) {
                result.add(entryEBP);
            }
            // sort been by date
            Collections.sort(result, new EntryEBPComparator());
        } catch (RuntimeException e) {
            throw new ImportFileException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(contentStream);
        }
        return result;
    }

    protected boolean validEntry(ImportResult importResult, Date dateEcr, Date fiscalPeriodsBiginDate, Date fiscalPeriodsEndingDate, final Account account, String targetedAccount) {
        boolean result = true;
        // if entry date have fiscalperiod open
        if (dateEcr.compareTo(fiscalPeriodsBiginDate) < 0
                || dateEcr.compareTo(fiscalPeriodsEndingDate) > 0) {
            importResult.addException(new ImportFileException(t("lima.import.error.entriesOutOfDatesRange", dateEcr)));
            importResult.increaseIgnored();
            result = false;
        }
        // if account not exist not export -> exception
        else if (account == null) {
            importResult.addException(new ImportFileException(t("lima.import.error.invalidAccountNumber", targetedAccount)));
            importResult.increaseIgnored();
            result = false;
        }
        return result;
    }

    @Override
    public ImportExportResults importEntriesFromEbp(String contents) {
        ImportExportResults results = new ImportExportResults();
        ImportResult result = results.createAddAndGetImportResult(Entry.class);

        // use for logs
        long before = System.currentTimeMillis();

        if (StringUtils.isBlank(contents)) {
            result.addException(new NoDataToImportException());
        } else {
            // Get all the valid fiscalPeriods Ordered by date.
            List<FiscalPeriod> fiscalPeriods = fiscalPeriodService.getAllUnblockedFiscalPeriods();
            try {
                basicEntriesFromEBPValidation(contents, fiscalPeriods);
            } catch (NoFiscalPeriodFoundException | NoDataToImportException e) {
                result.addException(e);
            }

            try {
                List<Account> accounts = accountService.getAllAccounts();
                accounts = accounts == null ? new ArrayList<>() : accounts;
                Map<String, Account> indexedAccounts = Maps.newHashMap(Maps.uniqueIndex(accounts, GET_ACCOUNT_NUMBER));

                List<EntryBook> entryBooks = entryBookService.getAllEntryBooks();
                entryBooks = entryBooks == null ? new ArrayList<>() : entryBooks;
                Map<String, EntryBook> indexedEntryBooks = Maps.newHashMap(Maps.uniqueIndex(entryBooks, GET_ENTRY_BOOK_CODE));

                List<EntryEBP> entryEBPs = loadDateOrderedEntryEBPbeans(contents);

                // For all entries loaded from the file
                //   the entry is validate (checking for valide FiscalPeriod and existing Account associated to it)
                //   if valid entry
                //     the entry entity is created and the association with it's dependant entites (Account are FinancialTransaction) are created
                Date fiscalPeriodsBeginDate = fiscalPeriods.get(0).getBeginDate();
                Date fiscalPeriodsEndingDate = fiscalPeriods.get(fiscalPeriods.size() - 1).getEndDate();
                Map<EntryBook, Map<Date, FinancialTransaction>> entryBookFinancialTransactionByDate = getEntryBookFinancialTransactionOrderedByDate(fiscalPeriodsBeginDate, fiscalPeriodsEndingDate);

                FinancialPeriodTopiaDao financialPeriodTopiaDao = getDaoHelper().getFinancialPeriodDao();

                // for all unblocked financialperiod
                List<FinancialPeriod> financialPeriods = financialPeriodTopiaDao.forProperties(FinancialPeriod.PROPERTY_LOCKED, false).findAll();

                Map<FinancialTransaction, List<Entry>> financialTransactionsToSave = new HashMap<>();
                Map<FinancialTransaction, List<Entry>> financialTransactionsToUpdate = new HashMap<>();
                for (EntryEBP entryEBP : entryEBPs) {
                    Date dateEcr = entryEBP.getDatEcr();

                    // account loading
                    Account account = indexedAccounts.get(entryEBP.getCompte());

                    if (!validEntry(result, dateEcr, fiscalPeriodsBeginDate, fiscalPeriodsEndingDate, account, entryEBP.getCompte())) {
                        continue;
                    }
                    // create entry
                    else {
                        // creation of the entry (not persisted yet)
                        // initialisation of this attributs
                        // entry is validate don't redo all check
                        Entry entry = createNewEntry(entryEBP, account);

                        try {
                            // find financial transactions for entry period and entrybook
                            FinancialTransaction transaction = getOrCreateImportEntryFinancialTransaction(entryEBP.getJournal(), indexedEntryBooks, entryBookFinancialTransactionByDate, dateEcr, financialPeriods);
                            if (transaction.isPersisted()) {
                                List<Entry> transactionEntriesToSave = financialTransactionsToUpdate.computeIfAbsent(transaction, k -> new ArrayList<>());
                                transactionEntriesToSave.add(entry);
                            } else {
                                List<Entry> transactionEntriesToSave = financialTransactionsToSave.computeIfAbsent(transaction, k -> new ArrayList<>());
                                transactionEntriesToSave.add(entry);
                            }

                        } catch (LockedFinancialPeriodException | LockedEntryBookException | AlreadyExistEntryBookException | AfterLastFiscalPeriodException | BeforeFirstFiscalPeriodException e) {
                            result.addException(e);
                            continue;
                        }

                    }
                    result.increaseCreated();
                }

                Iterable<FinancialTransaction> savedTransactions = financialTransactionService.persistImportedFinancialTransactions(Lists.newArrayList(financialTransactionsToSave.keySet()));
                addEntriesToTransactions(entryBookFinancialTransactionByDate, financialTransactionsToSave, savedTransactions);

                addEntriesToTransactions(entryBookFinancialTransactionByDate, financialTransactionsToUpdate, Lists.newArrayList(financialTransactionsToUpdate.keySet()));

                if (log.isInfoEnabled()) {
                    long after = System.currentTimeMillis();
                    log.info("Imported form EBP : " + entryEBPs.size() + " entries in "
                            + (after - before) + " ms");
                }
            } catch(ImportFileException e) {
                result.addException(e);
            }
        }
        return results;
    }

    protected void addEntriesToTransactions(Map<EntryBook, Map<Date, FinancialTransaction>> entryBookFinancialTransactionByDate, Map<FinancialTransaction, List<Entry>> financialTransactionsToSave, Iterable<FinancialTransaction> savedTransactions) {
        List<Entry> entriesToSaves = new ArrayList<>();
        for (FinancialTransaction transaction : savedTransactions) {

            Map<Date, FinancialTransaction> financialTransactionsByDate = entryBookFinancialTransactionByDate.get(transaction.getEntryBook());

            FinancialTransaction financialTransaction = financialTransactionsByDate.get(transaction.getTransactionDate());

            List<Entry> fEntries = financialTransactionsToSave.get(financialTransaction);
            transaction.addAllEntry(fEntries);
            entriesToSaves.addAll(fEntries);
        }

        financialTransactionService.persistImportedEntries(entriesToSaves);
    }

    protected Entry createNewEntry(EntryEBP entryEBP, final Account account) {
        Entry entry;
        BigDecimal debit;
        entry = financialTransactionService.createNewEntry();

        // the entry has one amount witch can be Debit or Credit
        // regarding the value of the boolean:debit
        debit = entryEBP.getDebit();
        if (debit == null || BigDecimal.ZERO.compareTo(debit)==0) {
            entry.setDebit(false);
            entry.setAmount(entryEBP.getCredit());
        } else {
            entry.setDebit(true);
            entry.setAmount(debit);
        }
        entry.setAccount(account);
        entry.setDescription(entryEBP.getLibelle());
        entry.setVoucher(entryEBP.getPiece());
        entry.setLettering(entryEBP.getLettre());
        return entry;
    }

    protected FinancialTransaction getOrCreateImportEntryFinancialTransaction(String entryBookCode, Map<String, EntryBook> indexedEntryBooks, Map<EntryBook, Map<Date, FinancialTransaction>> entryBookFinancialTransactionByDate, Date dateEcr, List<FinancialPeriod> financialPeriods) throws LockedFinancialPeriodException, LockedEntryBookException, AlreadyExistEntryBookException, AfterLastFiscalPeriodException, BeforeFirstFiscalPeriodException {

        EntryBook entryBook = getOrCreateEntryBook(indexedEntryBooks, entryBookCode, financialPeriods);
        Map<Date, FinancialTransaction> financialTransactionsByDate = entryBookFinancialTransactionByDate.computeIfAbsent(entryBook, k -> new HashMap<>());

        // create transaction
        FinancialTransaction financialTransaction = financialTransactionsByDate.get(dateEcr);
        if (financialTransaction == null
                || !(dateEcr.equals(financialTransaction
                .getTransactionDate()) && entryBook
                .getCode().equals(
                        financialTransaction.getEntryBook()
                                .getCode()))) {
            // create non persisted financial transaction
            financialTransaction = financialTransactionService.createNewFinancialTransaction();
            financialTransaction.setEntryBook(entryBook);
            financialTransaction.setTransactionDate(dateEcr);
            financialTransactionsByDate.put(financialTransaction.getTransactionDate(), financialTransaction);
        }
        return financialTransaction;
    }

    protected EntryBook getOrCreateEntryBook(Map<String, EntryBook> indexedEntryBooks, String entryBookCode, List<FinancialPeriod> financialPeriods) throws AlreadyExistEntryBookException {
        EntryBook entryBook;
        // entryBook loading
        entryBook = indexedEntryBooks.get(entryBookCode);

        // if entrybook not exist create it !
        if (entryBook == null) {
            entryBook = entryBookService.createNewEntryBook();
            entryBook.setCode(entryBookCode);
            //financialTransaction = null;
            // create it
            entryBook = entryBookService.createImportedNewEntryBook(entryBook, financialPeriods);
            indexedEntryBooks.put(entryBook.getCode(), entryBook);
        }
        return entryBook;
    }

    protected Map<EntryBook, Map<Date, FinancialTransaction>> getEntryBookFinancialTransactionOrderedByDate(Date fiscalPeriodsBeginDate, Date fiscalPeriodsEndingDate) {
        Map<EntryBook, Map<Date, FinancialTransaction>> entryBookFinancialTransactionByDate = new HashMap<>();
        List<FinancialTransaction> financialTransactions = financialTransactionService.getAllFinancialTransactions(fiscalPeriodsBeginDate, fiscalPeriodsEndingDate);
        for (FinancialTransaction ft : financialTransactions) {
            EntryBook eb = ft.getEntryBook();
            Map<Date, FinancialTransaction>  entryBooksFTs = entryBookFinancialTransactionByDate.get(eb);
            if (entryBooksFTs == null) {
                entryBooksFTs = new HashMap<>();
                entryBookFinancialTransactionByDate.put(eb, entryBooksFTs);
            }
            // maybe not unique financial transaction for one date.
            // is there a way to know wich one to take ?
            entryBooksFTs.put(ft.getTransactionDate(), ft);
        }
        return entryBookFinancialTransactionByDate;
    }

}
