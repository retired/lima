/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LastUnlockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoEmptyFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotLockedClosedPeriodicEntryBooksException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnbalancedEntriesException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.business.exceptions.UsedAccountException;
import org.chorem.lima.business.exceptions.UsedEntryBookException;
import org.chorem.lima.business.exceptions.WithoutEntryBookFinancialTransactionsException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;

import java.util.Collection;
import java.util.List;

/**
 * Service for localized rules
 *
 * @author jpepin
 */
public interface AccountingRules {

    /**
     * Account rule : check create account.
     *
     * @param account new account
     * @throws org.chorem.lima.business.exceptions.LimaException if rule validation fails
     */
    void createAccountRules(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException;

    void updateAccountRules(Account account) throws InvalidAccountNumberException, NotNumberAccountNumberException;

    void removeAccountRules(Account account) throws UsedAccountException;

    /**
     * Entrybook rules.
     *
     * @param entryBook
     * @throws org.chorem.lima.business.exceptions.LimaException
     */
    void removeEntryBookRules(EntryBook entryBook) throws UsedEntryBookException;

    /**
     * Fiscal Period rules
     *
     * @param fiscalPeriod
     * @throws org.chorem.lima.business.exceptions.LimaException
     */
    List<FinancialPeriod> createFiscalPeriodRules(FiscalPeriod fiscalPeriod)
            throws BeginAfterEndFiscalPeriodException,
            NotBeginNextDayOfLastFiscalPeriodException,
            MoreOneUnlockFiscalPeriodException;

    void blockFiscalPeriodRules(FiscalPeriod fiscalPeriod) throws LastUnlockedFiscalPeriodException, UnbalancedFinancialTransactionsException, UnfilledEntriesException, WithoutEntryBookFinancialTransactionsException;
    
    void deleteFiscalPeriodRules(FiscalPeriod fiscalPeriod) throws NoEmptyFiscalPeriodException;


    void createFinancialTransactionRules(FinancialTransaction financialTransaction)
            throws BeforeFirstFiscalPeriodException, AfterLastFiscalPeriodException,
            LockedFinancialPeriodException, LockedEntryBookException;

    void updateFinancialTransactionRules(FinancialTransaction financialTransaction, FinancialTransaction financialTransactionOld)
            throws BeforeFirstFiscalPeriodException, AfterLastFiscalPeriodException, LockedFinancialPeriodException, LockedEntryBookException;

    void deleteFinancialTransactionRules(FinancialTransaction financialTransaction)
            throws LockedFinancialPeriodException, LockedEntryBookException;

    void createEntriesRules(Collection<Entry> entries) throws LockedFinancialPeriodException, LockedEntryBookException;

    void createEntryRules(Entry entry) throws LockedFinancialPeriodException, LockedEntryBookException;

    void updateEntryRules(Entry entry, Entry entryOld) throws LockedFinancialPeriodException, LockedEntryBookException;

    void deleteEntryRules(Entry entry) throws LockedFinancialPeriodException, LockedEntryBookException;

   /**
     * Check if a financial period can be closed.
     * 
     * @param closedPeriodicEntryBook
     * @throws org.chorem.lima.business.exceptions.UnfilledEntriesException, WithoutEntryBookFinancialTransactionsException, UnbalancedFinancialTransactionsException, NotLockedClosedPeriodicEntryBooksException
     */
    void blockClosedPeriodicEntryBookRules(ClosedPeriodicEntryBook closedPeriodicEntryBook)
            throws UnfilledEntriesException,
            WithoutEntryBookFinancialTransactionsException,
            UnbalancedFinancialTransactionsException,
            NotLockedClosedPeriodicEntryBooksException;

    /**
     * Entry rules
     *
     * @param oldEntries
     * @throws LockedFinancialPeriodException, LockedEntryBookException
     */
    void addLetter(List<Entry> oldEntries) throws LockedEntryBookException, UnbalancedEntriesException;
}
