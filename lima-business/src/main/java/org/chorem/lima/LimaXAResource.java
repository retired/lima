/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaTransaction;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

/**
 * Topia XA ressource containing TopiaContext to commit or rollback.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class LimaXAResource implements XAResource {

    private static final Log log = LogFactory.getLog(LimaXAResource.class);

    protected final TopiaTransaction transaction;

    protected int timeout;

    public LimaXAResource(TopiaTransaction transaction) {
        this.transaction = transaction;
    }

    /*
     * @see javax.transaction.xa.XAResource#commit(javax.transaction.xa.Xid, boolean)
     */
    @Override
    public void commit(Xid arg0, boolean arg1) throws XAException {
        try {
            transaction.commit();
        } catch (TopiaException ex) {
            if (log.isErrorEnabled()) {
                log.error("Error", ex);
            }
            throw new XAException(XAException.XA_HEURCOM);
        }
    }

    /*
     * @see javax.transaction.xa.XAResource#end(javax.transaction.xa.Xid, int)
     */
    @Override
    public void end(Xid arg0, int arg1) throws XAException {
        /*try {
            context.close();
        } catch (TopiaException ex) {
            throw new XAException(XAException.XA_HEURCOM);
        }*/
    }

    /*
     * @see javax.transaction.xa.XAResource#forget(javax.transaction.xa.Xid)
     */
    @Override
    public void forget(Xid xid) throws XAException {

    }

    /*
     * @see javax.transaction.xa.XAResource#getTransactionTimeout()
     */
    @Override
    public int getTransactionTimeout() throws XAException {
        return timeout;
    }

    /*
     * @see javax.transaction.xa.XAResource#isSameRM(javax.transaction.xa.XAResource)
     */
    @Override
    public boolean isSameRM(XAResource xar) throws XAException {
        return false;
    }

    /*
     * @see javax.transaction.xa.XAResource#prepare(javax.transaction.xa.Xid)
     */
    @Override
    public int prepare(Xid xid) throws XAException {
        return XAResource.XA_OK;
    }

    /*
     * @see javax.transaction.xa.XAResource#recover(int)
     */
    @Override
    public Xid[] recover(int arg0) throws XAException {
        return null;
    }

    /*
     * @see javax.transaction.xa.XAResource#rollback(javax.transaction.xa.Xid)
     */
    @Override
    public void rollback(Xid arg0) throws XAException {
        try {
            transaction.rollback();
        } catch (TopiaException ex) {
            throw new XAException(XAException.XA_HEURCOM);
        }
    }

    /*
     * @see javax.transaction.xa.XAResource#setTransactionTimeout(int)
     */
    @Override
    public boolean setTransactionTimeout(int timeout) throws XAException {
        this.timeout = timeout;
        return true;
    }

    /*
     * @see javax.transaction.xa.XAResource#start(javax.transaction.xa.Xid, int)
     */
    @Override
    public void start(Xid arg0, int arg1) throws XAException {
        
    }
}
