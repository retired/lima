/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import org.nuiton.topia.persistence.HqlAndParametersBuilder;

import java.util.Date;

/**
 * Fiscal period entity DAO.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FiscalPeriodTopiaDao extends AbstractFiscalPeriodTopiaDao<FiscalPeriod> {

    /**
     * Get last fiscal period (higher end date).
     * 
     * @return last fiscal period
     */
    public FiscalPeriod getLastFiscalPeriod() {

        FiscalPeriod result = newQueryBuilder()
                .setOrderByArguments(FinancialPeriod.PROPERTY_END_DATE + " DESC")
                .findFirstOrNull();

        return result;
    }
    
    /**
     * Get first fiscal period (higher end date).
     * 
     * @return last fiscal period
     */
    public FiscalPeriod getFirstFiscalPeriod() {
        FiscalPeriod result = newQueryBuilder()
                .setOrderByArguments(FinancialPeriod.PROPERTY_END_DATE)
                .findFirstOrNull();

        return result;
    }
    
    /**
     * Get last non locked fiscal period (higher end date).
     * 
     * @return last fiscal period
     */
    public FiscalPeriod getLastUnlockedFiscalPeriod() {

        FiscalPeriod result = forLockedEquals(false)
                .setOrderByArguments(FinancialPeriod.PROPERTY_END_DATE + " DESC")
                .findFirstOrNull();

        return result;
    }

    /**
     * Return the FiscalPeriod for the given date
     *
     * @param date fiscal period for this given date
     * @return the FiscalPeriod for {@code date}
     */
    public FiscalPeriod findForDate(Date date) {

        HqlAndParametersBuilder<FiscalPeriod> builder = newHqlAndParametersBuilder();
        builder.addLowerOrEquals(FiscalPeriod.PROPERTY_BEGIN_DATE, date);
        builder.addGreaterOrEquals(FiscalPeriod.PROPERTY_END_DATE, date);

        FiscalPeriod fiscalPeriod = findUniqueOrNull(builder.getHql(), builder.getHqlParameters());
        return fiscalPeriod;
    }
}
