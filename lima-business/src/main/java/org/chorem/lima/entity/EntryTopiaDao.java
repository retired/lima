/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import com.google.common.collect.Ordering;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.beans.LetteringFilter;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntryTopiaDao extends AbstractEntryTopiaDao<Entry> {

    private static final Log log = LogFactory.getLog(EntryTopiaDao.class);

    public static final String PROPERTY_TRANSACTION_DATE = Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_TRANSACTION_DATE;

    public static final String PROPERTY_ENTRY_BOOK = Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_ENTRY_BOOK;

    /**
     * Requete generique qui recupere les entrees equilibrées portant entre
     * deux dates.
     *
     */
    protected HqlAndParametersBuilder<Entry> getEquilibredTransactionQuery(Date beginDate, Date endDate) {

        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, endDate);
        builder.addWhereClause(FinancialTransactionTopiaDao.getBalancedClause(builder.getAlias() + "." + Entry.PROPERTY_FINANCIAL_TRANSACTION, true));

        return builder;
    }

    /**
     * Query for find entries for accountsreports and balancereports
     * Just exact and balanced transaction are calculated.
     *
     * @param account account
     * @param beginDate begin date
     * @param endDate end date
     * @return entries
     */
    public List<Entry> findAllEntryOfBalancedTransaction(Account account,
            Date beginDate, Date endDate) {

        HqlAndParametersBuilder<Entry> builder = getEquilibredTransactionQuery(beginDate, endDate);
        builder.addEquals(Entry.PROPERTY_ACCOUNT, account);
        builder.setOrderByArguments(PROPERTY_TRANSACTION_DATE, Entry.PROPERTY_FINANCIAL_TRANSACTION, Entry.PROPERTY_TOPIA_CREATE_DATE);

        List<Entry> entries = findAll(builder.getHql(), builder.getHqlParameters());

        return entries;
    }

    /**
     * Retourne la somme des entrées des transaction equilibrées entre
     * deux dates pour un compte donné.
     *
     * @param account account
     * @param beginDate bebin date
     * @param endDate end date
     * @return list boolean,int (une ligne pour le debit true, une ligne pour le credit)
     */
    public List<Object[]> getDebitCreditOfBalancedTransaction(Account account,
            Date beginDate, Date endDate) {

        HqlAndParametersBuilder<Entry> builder = getEquilibredTransactionQuery(beginDate, endDate);
        builder.addEquals(Entry.PROPERTY_ACCOUNT, account);

        String query = "SELECT " + builder.getAlias() + "." + Entry.PROPERTY_DEBIT + ", " +
                "SUM(" + builder.getAlias() + "." + Entry.PROPERTY_AMOUNT + ") " +
                builder.getHql() +
                " GROUP BY " + builder.getAlias() + "." + Entry.PROPERTY_DEBIT ;

        List<Object[]> result = findAll(query, builder.getHqlParameters());
        return result;
    }

    /**
     * Retourne les entrees des transaction equlibrées entre deux dates pour
     * un journal.
     *
     * @param entryBook entry book
     * @param beginDate begin date
     * @param endDate end date
     */
    public List<Entry> findAllEntryOfBalancedTransaction(EntryBook entryBook, Date beginDate,
            Date endDate) {

        HqlAndParametersBuilder<Entry> builder = getEquilibredTransactionQuery(beginDate, endDate);
        builder.addEquals(PROPERTY_ENTRY_BOOK, entryBook);
        builder.setOrderByArguments(PROPERTY_TRANSACTION_DATE, Entry.PROPERTY_FINANCIAL_TRANSACTION, Entry.PROPERTY_TOPIA_CREATE_DATE);

        List<Entry> entries = findAll(builder.getHql(), builder.getHqlParameters());
        return entries;
    }

    public List<String> findLetters() {

        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addNotNull(Entry.PROPERTY_LETTERING);
        builder.setOrderByArguments(Entry.PROPERTY_LETTERING + " DESC");

        String query = "SELECT DISTINCT " + builder.getAlias() + "." + Entry.PROPERTY_LETTERING + " " +
                builder.getHql();

        List<String> result = findAll(query, builder.getHqlParameters());

        if (log.isDebugEnabled()) {
            log.debug("Size of result : " + result.size());
        }

        return result;
    }

    /**
     * Retourne la somme des entrées des transaction equilibrées entre
     * deux dates pour un journal donné.
     *
     * @param entryBook entry book
     * @param beginDate bebin date
     * @param endDate end date
     * @return list boolean,int (une ligne pour le debit true, une ligne pour le credit)
     */
    public List<Object[]> getDebitCreditOfBalancedTransaction(EntryBook entryBook,
            Date beginDate, Date endDate) {
        HqlAndParametersBuilder<Entry> builder = getEquilibredTransactionQuery(beginDate, endDate);
        builder.addEquals(PROPERTY_ENTRY_BOOK, entryBook);

        String query = "SELECT " + builder.getAlias() + "." + Entry.PROPERTY_DEBIT + ", " +
                "SUM(" + builder.getAlias() + "." + Entry.PROPERTY_AMOUNT + ") " +
                builder.getHql() +
                " GROUP BY " + builder.getAlias() + "." + Entry.PROPERTY_DEBIT ;

        List<Object[]> result = findAll(query, builder.getHqlParameters());
        return result;
    }

    /**
     * Retourne les entrees des transaction entre deux dates pour
     * un journal.
     *
     * @param entryBook entry book
     * @param beginDate begin date
     * @param endDate end date
     */
    public List<Entry> findAllEntryByDateForEntryBook(EntryBook entryBook, Date beginDate,
                                                      Date endDate) {
        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addEquals(PROPERTY_ENTRY_BOOK, entryBook);
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, endDate);
        builder.setOrderByArguments(PROPERTY_TRANSACTION_DATE, Entry.PROPERTY_FINANCIAL_TRANSACTION, Entry.PROPERTY_TOPIA_CREATE_DATE);

        List<Entry> entries = findAll(builder.getHql(), builder.getHqlParameters());
        return entries;
    }

    /**
     * Retourne les entrees des transaction entre deux dates.
     *
     * @param beginDate begin date
     * @param endDate end date
     */
    public List<Entry> findAllEntryByDate(Date beginDate, Date endDate) {
        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, endDate);
        builder.setOrderByArguments(PROPERTY_TRANSACTION_DATE, Entry.PROPERTY_FINANCIAL_TRANSACTION, Entry.PROPERTY_TOPIA_CREATE_DATE);

        List<Entry> entries = findAll(builder.getHql(), builder.getHqlParameters());
        return entries;
    }


    /**
     * Retourne toutes les entrées d'une transaction
     * pour un compte et la présence d'un lettrage ou (xor) non
     * @param filter filtre sur les entrees, selon le compte, les dates de debut et de fin, et le lettrage
     * */
    public List<Entry> findAllEntryByFilter(LetteringFilter filter) {

        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, filter.getDateStart());
        builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, filter.getDateEnd());

        Account account = filter.getAccount();

        if (account == null || account.getTopiaId() != null) {
            builder.addEquals(Entry.PROPERTY_ACCOUNT, account);
        }

        String alias = builder.getAlias();
        if (!filter.isDisplayLettered() && filter.isDisplayUnlettred()){

            builder.addWhereClause( alias + "." + Entry.PROPERTY_LETTERING + " is null " +
                    "OR " + alias + "." + Entry.PROPERTY_LETTERING + " = ''");

        } else if (filter.isDisplayLettered() && !filter.isDisplayUnlettred()){

            builder.addWhereClause( alias + "." + Entry.PROPERTY_LETTERING + " is not null " +
                    "OR " + alias + "." + Entry.PROPERTY_LETTERING + " != ''");

        }
        builder.setOrderByArguments(PROPERTY_TRANSACTION_DATE, Entry.PROPERTY_FINANCIAL_TRANSACTION, Entry.PROPERTY_TOPIA_CREATE_DATE);

        List<Entry> entries = findAll(builder.getHql(), builder.getHqlParameters());

        return entries;
    }

    public List<Entry> findAllUnlockEntriesByFilter(LetteringFilter filter) {
        Map<String, Object> params = new HashMap<>();
        String req = "SELECT E FROM " + Entry.class.getName() + " E, " + ClosedPeriodicEntryBook.class.getName() + " CPEB " +
                "  WHERE E." + PROPERTY_TRANSACTION_DATE + ">= :ds " +
                "  AND E." + PROPERTY_TRANSACTION_DATE + "<= :de ";
                Account account = filter.getAccount();
                if (account == null || account.getTopiaId() != null) {
                    req += "  AND E." + Entry.PROPERTY_ACCOUNT + " = :a";
                    params.put("a", account);
                }
                req +="  AND E." + Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_ENTRY_BOOK + " =  CPEB." + ClosedPeriodicEntryBook.PROPERTY_ENTRY_BOOK +
                "  AND CPEB." + ClosedPeriodicEntryBook.PROPERTY_LOCKED + " = FALSE" +
                "  AND CPEB." + ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD + "." + FinancialPeriod.PROPERTY_BEGIN_DATE + " <= E." + Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_TRANSACTION_DATE +
                "  AND CPEB." + ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD + "." + FinancialPeriod.PROPERTY_END_DATE + " >= E." + Entry.PROPERTY_FINANCIAL_TRANSACTION + "." + FinancialTransaction.PROPERTY_TRANSACTION_DATE;
        params.put("ds", filter.getDateStart());
        params.put("de", filter.getDateEnd());

        List<Entry> entries = findAll(req, params);

        return entries;
    }

    /**
     * Retourne la dernière entrée d'une transaction
     * @param financialTransaction transaction sur laquelle la derniere entree est selectionnee
     * */
    public Entry getLastEntry(FinancialTransaction financialTransaction) {

        Ordering<Entry> o = new Ordering<Entry>() {
            @Override
            public int compare(Entry left, Entry right) {
                return left.getTopiaCreateDate().compareTo(right.getTopiaCreateDate());
            }
        };
        Entry lastEntry = o.max(financialTransaction.getEntry());
        return lastEntry;
    }

    /**
     * Retourne la somme des entrées des transaction entre
     * deux dates pour un journal donné.
     *
     * @param entryBook entry book
     * @param beginDate bebin date
     * @param endDate end date
     * @return list boolean,int (une ligne pour le debit true, une ligne pour le credit)
     */
    public List<Object[]> getDebitCreditOfTransaction(EntryBook entryBook,
            Date beginDate, Date endDate) {

        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, endDate);
        builder.addEquals(PROPERTY_ENTRY_BOOK, entryBook);

        String query = "SELECT " + builder.getAlias() + "." + Entry.PROPERTY_DEBIT + ", " +
                "SUM(" + builder.getAlias() + "." + Entry.PROPERTY_AMOUNT + ") " +
                builder.getHql() +
                " GROUP BY " + builder.getAlias() + "." + Entry.PROPERTY_DEBIT ;

        List<Object[]> result = findAll(query, builder.getHqlParameters());
        return result;
    }

    public List<Object[]> getAccountEntriesDebitCreditFromIncludingToExcludingPeriod(LetteringFilter filter) {
        List<Object[]> result;
        if (filter != null) {
            result = getAccountEntriesDebitCreditFromIncludingToExcludingPeriod(filter.getAccount(), filter.getDateStart(), filter.getDateEnd(), false);
        } else {
            result = new ArrayList<>();
        }
        return result;
    }

    public List<Object[]> getAccountEntriesDebitCreditFromIncludingToIncludingPeriod(LetteringFilter filter) {
        List<Object[]> result;
        if (filter != null) {
            result = getAccountEntriesDebitCreditFromIncludingToExcludingPeriod(filter.getAccount(), filter.getDateStart(), filter.getDateEnd(), true);
        } else {
            result = new ArrayList<>();
        }
        return result;
    }

    protected List<Object[]> getAccountEntriesDebitCreditFromIncludingToExcludingPeriod(Account account, Date beginDate, Date endDate, boolean includeEndDate) {

        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addEquals(Entry.PROPERTY_ACCOUNT, account);

        if (includeEndDate) {
            builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, endDate);
        } else {
            builder.addLowerThan(PROPERTY_TRANSACTION_DATE, endDate);
        }

        String query = "SELECT " + builder.getAlias() + "." + Entry.PROPERTY_DEBIT + ", " +
                "SUM(" + builder.getAlias() + "." + Entry.PROPERTY_AMOUNT + ") " +
                builder.getHql() +
                " GROUP BY " + builder.getAlias() + "." + Entry.PROPERTY_DEBIT ;

        List<Object[]> result = findAll(query, builder.getHqlParameters());
        return result;
    }

    public List<Entry> getAllEntriesByDatesForEntryBook(EntryBook entryBook, Date beginDate, Date endDate) {

        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, endDate);
        builder.addEquals(PROPERTY_ENTRY_BOOK, entryBook);
        builder.setOrderByArguments(PROPERTY_TRANSACTION_DATE, Entry.PROPERTY_FINANCIAL_TRANSACTION, Entry.PROPERTY_TOPIA_CREATE_DATE);

        List<Entry> result = findAll(builder.getHql(), builder.getHqlParameters());
        return result;
    }

    /**
     * get all entries where some field are not filled in.
     *
     * @param beginDate begin date
     * @param endDate end date
     * @param entryBook entry book
     * @return all entries where some field are not filled in
     */
    public List<Entry> findAllUnfilledEntry(Date beginDate, Date endDate, EntryBook entryBook) {

        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addEquals(PROPERTY_ENTRY_BOOK, entryBook);
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, endDate);
        builder.addWhereClause(Entry.PROPERTY_ACCOUNT + " = null " +
                "OR " + Entry.PROPERTY_VOUCHER + " = null " +
                "OR " + Entry.PROPERTY_VOUCHER + " = ''" +
                "OR " + Entry.PROPERTY_DESCRIPTION + " = null " +
                "OR " + Entry.PROPERTY_DESCRIPTION + " = ''");
        builder.setOrderByArguments(PROPERTY_TRANSACTION_DATE, Entry.PROPERTY_FINANCIAL_TRANSACTION, Entry.PROPERTY_TOPIA_CREATE_DATE);

        List<Entry> entries = findAll(builder.getHql(), builder.getHqlParameters());

        return entries;
    }

    /**
     * get all entries where some field are not filled in.
     *
     * @param beginDate begin date
     * @param endDate end date
     * @return all entries where some field are not filled in
     */
    public List<Entry> findAllUnfilledEntry(Date beginDate, Date endDate) {

        HqlAndParametersBuilder<Entry> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_TRANSACTION_DATE, endDate);
        builder.addWhereClause(Entry.PROPERTY_ACCOUNT + " = null " +
                "OR " + Entry.PROPERTY_VOUCHER + " = null " +
                "OR " + Entry.PROPERTY_VOUCHER + " = ''" +
                "OR " + Entry.PROPERTY_DESCRIPTION + " = null " +
                "OR " + Entry.PROPERTY_DESCRIPTION + " = ''");
        builder.setOrderByArguments(PROPERTY_TRANSACTION_DATE, Entry.PROPERTY_FINANCIAL_TRANSACTION, Entry.PROPERTY_TOPIA_CREATE_DATE);

        List<Entry> entries = findAll(builder.getHql(), builder.getHqlParameters());

        return entries;
    }

}
