/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;

import java.util.Date;
import java.util.List;

public class ClosedPeriodicEntryBookTopiaDao extends AbstractClosedPeriodicEntryBookTopiaDao<ClosedPeriodicEntryBook>  {

    protected static final String PROPERTY_BEGIN_DATE = ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD + "." + FinancialPeriod.PROPERTY_BEGIN_DATE;

    protected static final String PROPERTY_END_DATE = ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD + "." + FinancialPeriod.PROPERTY_END_DATE;


    /**
     * Find all ClosedPeriodicEntryBook with common EntryBook.
     *
     * @param entryBook entry book property
     * @return ClosedPeriodicEntryBook list
     */
    public List<ClosedPeriodicEntryBook> findAllByEntryBook(EntryBook entryBook) {
        return forProperties(ClosedPeriodicEntryBook.PROPERTY_ENTRY_BOOK, entryBook).findAll();
    }

    /**
     * Return ClosedPeriodicEntryBook by EntryBook and FinancialPeriod.
     * 
     * @param entryBook 
     * @param financialPeriod 
     * @return ClosedPeriodicEntryBook
     */
    public ClosedPeriodicEntryBook findByEntryBookAndFinancialPeriod(
            EntryBook entryBook, FinancialPeriod financialPeriod) {

        TopiaQueryBuilderAddCriteriaOrRunQueryStep<ClosedPeriodicEntryBook> queryStep = newQueryBuilder();

        if (entryBook != null) {
            queryStep = queryStep.addEquals(ClosedPeriodicEntryBook.PROPERTY_ENTRY_BOOK, entryBook);
        }

        if (financialPeriod != null) {
            queryStep = queryStep.addEquals(ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD, financialPeriod);
        }

        ClosedPeriodicEntryBook closedPeriodicEntryBook = queryStep.findAnyOrNull();

        return closedPeriodicEntryBook;
    }

    /**
     * Return ClosedPeriodicEntryBook by EntryBook and FinancialPeriod.
     *
     * @param entryBook
     * @param date
     * @return ClosedPeriodicEntryBook
     */
    public ClosedPeriodicEntryBook findByEntryBookAndDate(
            EntryBook entryBook, Date date) {

        HqlAndParametersBuilder<ClosedPeriodicEntryBook> builder = newHqlAndParametersBuilder();

        builder.addEquals(ClosedPeriodicEntryBook.PROPERTY_ENTRY_BOOK, entryBook);

        builder.addLowerOrEquals(PROPERTY_BEGIN_DATE, date);

        builder.addGreaterOrEquals(PROPERTY_END_DATE, date);

        ClosedPeriodicEntryBook closedPeriodicEntryBook = findUnique(builder.getHql(), builder.getHqlParameters());

        return closedPeriodicEntryBook;
    }

    public List<ClosedPeriodicEntryBook> findAllForDate(
            Date from, Date date) {

        HqlAndParametersBuilder<ClosedPeriodicEntryBook> builder = newHqlAndParametersBuilder();

        builder.addLowerOrEquals(PROPERTY_BEGIN_DATE, date);

        builder.addGreaterOrEquals(PROPERTY_END_DATE, date);

        builder.setOrderByArguments(PROPERTY_BEGIN_DATE);

        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = findAll(builder.getHql(), builder.getHqlParameters());

        return closedPeriodicEntryBooks;
    }

    /**
     * Retourne toutes les ClosedPeriodicEntryBook par interval de date
     * sur les periodes sur lequelles elles portent ordonnée par journal.
     * 
     * @param beginDate begin date
     * @param endDate end date
     * @return all ClosedPeriodicEntryBook between begin and end
     */
    public List<ClosedPeriodicEntryBook> findAllByDates(Date beginDate,
                                                        Date endDate) {

        HqlAndParametersBuilder<ClosedPeriodicEntryBook> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_BEGIN_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_END_DATE, endDate);
        builder.setOrderByArguments(ClosedPeriodicEntryBook.PROPERTY_ENTRY_BOOK + "." + EntryBook.PROPERTY_CODE, ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD + "." + FinancialPeriod.PROPERTY_BEGIN_DATE);

        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = findAll(builder.getHql(), builder.getHqlParameters());
        return closedPeriodicEntryBooks;
    }
    
    /**
     * Retourne toutes les ClosedPeriodicEntryBook par interval de date
     * sur les periodes sur lequelles elles portent pour un journal.
     * 
     * @param entryBook entry book
     * @param beginDate begin date
     * @param endDate end date
     * @return all ClosedPeriodicEntryBook between begin and end
     */
    public List<ClosedPeriodicEntryBook> findAllByEntryBookAndDates(EntryBook entryBook,
            Date beginDate, Date endDate) {

        HqlAndParametersBuilder<ClosedPeriodicEntryBook> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_BEGIN_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_END_DATE, endDate);
        builder.addEquals(ClosedPeriodicEntryBook.PROPERTY_ENTRY_BOOK, entryBook);

        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = findAll(builder.getHql(), builder.getHqlParameters());
        return closedPeriodicEntryBooks;
    }
    
    /**
     * Retourne toutes les ClosedPeriodicEntryBook par interval de date
     * sur les periodes sur lequelles elles portent pour un journal.
     * 
     * @param entryBook entry book
     * @param beginDate begin date
     * @param endDate end date
     * @return all ClosedPeriodicEntryBook between begin and end
     */
    public List<ClosedPeriodicEntryBook> findAllByEntryBookAndDatesLocked(EntryBook entryBook,
            Date beginDate, Date endDate) {

        HqlAndParametersBuilder<ClosedPeriodicEntryBook> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(PROPERTY_BEGIN_DATE, beginDate);
        builder.addLowerOrEquals(PROPERTY_END_DATE, endDate);
        builder.addEquals(ClosedPeriodicEntryBook.PROPERTY_ENTRY_BOOK, entryBook);
        builder.addEquals(ClosedPeriodicEntryBook.PROPERTY_LOCKED, true);

        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = findAll(builder.getHql(), builder.getHqlParameters());
        return closedPeriodicEntryBooks;
    }

    /**
     * Retourne les ClosedPeriodicEntryBook de toutes les exercices encore ouverts.
     * 
     * @return les ClosedPeriodicEntryBook
     */
    public List<ClosedPeriodicEntryBook> findAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriod() {

        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks =
                forEquals(ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD + "." + FinancialPeriod.PROPERTY_LOCKED, false)
                .setOrderByArguments(PROPERTY_BEGIN_DATE)
                .findAll();

        return closedPeriodicEntryBooks;
    }
    
    /**
     * Retourne les ClosedPeriodicEntryBook d'un exercice ouvert.
     * 
     * @return les ClosedPeriodicEntryBook
     */
    public List<ClosedPeriodicEntryBook> findAllClosedPeriodicEntryBooksFromFiscalPeriod(FiscalPeriod fiscalPeriod) {
        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks =
                forIn(ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD, fiscalPeriod.getFinancialPeriod())
                        .setOrderByArguments(PROPERTY_BEGIN_DATE)
                        .findAll();

        return closedPeriodicEntryBooks;
    }

    /**
     * find all previous ClosedPeriodicEntryBook not close
     *
     * @return previous ClosedPeriodicEntryBook not close
     */
    public List<ClosedPeriodicEntryBook> findAllPreviousClosedPeriodicEntryBooksNotLocked(ClosedPeriodicEntryBook closedPeriodicEntryBook) {
        HqlAndParametersBuilder<ClosedPeriodicEntryBook> builder = newHqlAndParametersBuilder();
        builder.addEquals(ClosedPeriodicEntryBook.PROPERTY_ENTRY_BOOK, closedPeriodicEntryBook.getEntryBook());
        builder.addEquals(ClosedPeriodicEntryBook.PROPERTY_LOCKED, false);
        builder.addLowerThan(
                ClosedPeriodicEntryBook.PROPERTY_FINANCIAL_PERIOD + "." + FinancialPeriod.PROPERTY_END_DATE,
                closedPeriodicEntryBook.getFinancialPeriod().getBeginDate());

        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = findAll(builder.getHql(), builder.getHqlParameters());
        return closedPeriodicEntryBooks;
    }

}
