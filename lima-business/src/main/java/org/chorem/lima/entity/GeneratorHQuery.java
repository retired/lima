package org.chorem.lima.entity;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.clause.AndClause;
import org.chorem.lima.clause.BooleanClause;
import org.chorem.lima.clause.Clause;
import org.chorem.lima.clause.DateClause;
import org.chorem.lima.clause.NotClause;
import org.chorem.lima.clause.NumberClause;
import org.chorem.lima.clause.OrClause;
import org.chorem.lima.clause.SetClause;
import org.chorem.lima.clause.StringClause;
import org.chorem.lima.clause.SubFilterClause;
import org.chorem.lima.clause.VisitorClause;
import org.chorem.lima.filter.AccountFilter;
import org.chorem.lima.filter.EntryBookFilter;
import org.chorem.lima.filter.EntryFilter;
import org.chorem.lima.filter.Filter;
import org.chorem.lima.filter.FinancialTransactionFilter;
import org.chorem.lima.filter.VisitorFilter;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class GeneratorHQuery implements VisitorFilter, VisitorClause {

    protected String fromClause;

    protected String whereClause;

    protected final Map<String, Object> parameters;

    protected final Map<String, Integer> parametersCount;

    protected final Map<Filter, String> aliasMap;

    protected final Map<String, Integer> aliasCount;

    protected final Deque<String> fromClauseStack;
    protected final Deque<String> whereClauseStak;

    protected final Deque<Filter> filterStack;

    public GeneratorHQuery() {
        fromClause = "";
        whereClause = "";
        parameters = new HashMap<>();
        parametersCount = new HashMap<>();
        aliasMap = new HashMap<>();
        aliasCount = new HashMap<>();
        filterStack = new LinkedList<>();
        fromClauseStack = new LinkedList<>();
        whereClauseStak = new LinkedList<>();
    }

    protected String addParameter(Object value) {
        String type = value.getClass().getSimpleName();
        Integer count;
        if (parametersCount.containsKey(type)) {
            count = parametersCount.get(type);
            count++;
        } else {
            count = 1;
        }
        parametersCount.put(type, count);
        String parameter = type + "_" + count;
        parameters.put(parameter, value);
        return type + "_" + count;
    }

    protected String newAlias(Filter filter) {
        String name = filter.getEntityClass().getSimpleName();
        Integer count;
        if (aliasCount.containsKey(name)) {
            count = aliasCount.get(name);
            count++;
        } else {
            count = 1;
        }
        aliasCount.put(name, count);
        String alias = name + count;
        aliasMap.put(filter, alias);
        return alias;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public List<Object> getArguments() {
        List<Object> result = new ArrayList<>();
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            result.add(entry.getKey());
            result.add(entry.getValue());
        }
        return result;
    }

    public String getHQuery() {
        return fromClause + whereClause;
    }

    protected void visitFilter(Filter filter) {
        String alias = newAlias(filter);
        if (fromClause.isEmpty()) {
            fromClause = "SELECT DISTINCT " + alias;
            fromClause += "\nFROM " + filter.getEntityClass().getName() + " AS " + alias;
            if (filter.getClause() != null) {
                whereClause = "\nWHERE ";
            }
        } else {
            fromClause += " AS " + alias;
        }
        if (filter.getClause() != null) {
            filterStack.addFirst(filter);
            filter.getClause().accept(this);
            filterStack.removeFirst();
        }
    }

    @Override
    public void visitAccountFilter(AccountFilter accountFilter) {
        visitFilter(accountFilter);
    }

    @Override
    public void visitEntryBookFilter(EntryBookFilter entryBookFilter) {
        visitFilter(entryBookFilter);
    }

    @Override
    public void visitEntryFilter(EntryFilter entryFilter) {
        visitFilter(entryFilter);
    }

    @Override
    public void visitFinancialTransactionFilter(FinancialTransactionFilter financialTransactionFilter) {
        visitFilter(financialTransactionFilter);
    }

    @Override
    public void visitDateClause(DateClause dateClause) {
        if (dateClause != null
                && dateClause.getValue() != null) {
            String parameter = addParameter(dateClause.getValue());
            Filter filter = filterStack.peek();
            String alias = aliasMap.get(filter);
            String operand = " = " ;
            switch (dateClause.getOperand()) {
                case SAME:
                    operand = " = ";
                    break;
                case PREVIOUS:
                    operand = " <= ";
                    break;
                case AFTER:
                    operand = " >= ";
                    break;
                case DIFFERENT:
                    operand = " != ";
                    break;
            }
            whereClause += alias + "." + dateClause.getProperty() + operand + ":" + parameter;
        } else {
            whereClause += "1 = 1";
        }
    }

    @Override
    public void visitAndClause(AndClause andClause) {
        boolean first = true;
        whereClause += "(";
        for (Clause clause: andClause) {
            if (first) {
                first = false;
            } else  {
                whereClause += "\nAND ";
            }
            clause.accept(this);
        }
        whereClause += ")";
    }

    @Override
    public void visitBooleanClause(BooleanClause booleanClause) {
        String parameter = addParameter(true);
        Filter filter = filterStack.peek();
        String alias = aliasMap.get(filter);
        whereClause += alias + "." + booleanClause.getProperty() + " = :" + parameter;
    }

    @Override
    public void visitNumberClause(NumberClause numberClause) {
        if (numberClause != null && numberClause.getValue() != null) {
            String parameter = addParameter(numberClause.getValue());
            String operand = " = " ;
            Filter filter = filterStack.peek();
            String alias = aliasMap.get(filter);
            switch (numberClause.getOperand()) {
                case EQUAL:
                    operand = " = ";
                    break;
                case NOT_EQUAL:
                    operand = " != ";
                    break;
                case LOWER:
                    operand = " < ";
                    break;
                case LOWER_OR_EQUAL:
                    operand = " <= ";
                    break;
                case UPPER:
                    operand = " > ";
                    break;
                case UPPER_OR_EQUAL:
                    operand = " >= ";
                    break;
            }
            whereClause += alias + "." + numberClause.getProperty() + operand + ":" + parameter;
        } else {
            whereClause  += "1 = 1";
        }
    }

    @Override
    public void visitOrClause(OrClause orClause) {
        boolean first = true;
        whereClause += "(";
        for (Clause clause: orClause) {
            if (first) {
                first = false;
            } else  {
                whereClause += "\nOR ";
            }
            clause.accept(this);
        }
        whereClause += ")";
    }

    @Override
    public void visitNotClause(NotClause notClause) {
        whereClause += "NOT ";
        Clause clause = notClause.getClause();
        clause.accept(this);
    }

    @Override
    public void visitSetClause(SetClause setClause) {
        Filter filter = filterStack.peek();
        String alias = aliasMap.get(filter);
        String operand = " = " ;
        switch (setClause.getOperand()) {
            case IN:
                operand = " IN ";
                break;
            case NOT_IN:
                operand = " NOT IN ";
                break;
        }
        whereClause += alias + "." + setClause.getProperty() + operand + "(";

        boolean first = true;
        for (Object object : setClause) {
            if (first) {
                first = false;
            } else  {
                whereClause += ", ";
            }
            String parameter = addParameter(object);
            whereClause += ":" + parameter;
        }
        whereClause += ")";
    }

    @Override
    public void visitStringClause(StringClause stringClause) {
        String value = stringClause.getValue();
        if (StringUtils.isNotBlank(value)) {
            Filter filter = filterStack.peek();
            String alias = aliasMap.get(filter);
            String operand = " = " ;
            switch (stringClause.getOperand()) {
                case EQUAL:
                    operand = " = ";
                    value = stringClause.getValue();
                    break;
                case NOT_EQUAL:
                    operand = " != ";
                    value = stringClause.getValue();
                    break;
                case BEGIN:
                    operand = " LIKE ";
                    value = stringClause.getValue() + "%";
                    break;
                case ENDING:
                    operand = " LIKE ";
                    value = "%" + stringClause.getValue();
                    break;
                case CONTAIN:
                    operand = " LIKE ";
                    value = "%" + stringClause.getValue()+ "%";
                    break;
            }
            if (stringClause.isSensitiveCase()) {
                whereClause += alias + "." + stringClause.getProperty() + operand + "'" + value + "'";
            } else {
                whereClause += "lower(" + alias + "." + stringClause.getProperty() + ")" + operand + "'" + value.toLowerCase() + "'";
            }
        }
    }

    @Override
    public void visitSubFilterClause(SubFilterClause subFilterClause) {
        Filter filter = filterStack.peek();
        String alias = aliasMap.get(filter);

        Filter subFilter;
        switch (subFilterClause.getMultiplicity()) {
            case ONE:
                fromClauseStack.addFirst(fromClause);
                fromClause = "";
                whereClauseStak.addFirst(whereClause);
                whereClause = "";

                subFilter = subFilterClause.getSubFilter();
                subFilter.accept(this);

                String clause = alias + "." + subFilterClause.getProperty() +
                        " IN (" + fromClause + whereClause + ")";

                fromClause = fromClauseStack.removeFirst();
                whereClause = whereClauseStak.removeFirst() + clause;
                break;
            case MANY:
                fromClause += "\nLEFT JOIN " + alias + "." + subFilterClause.getProperty();
                subFilter = subFilterClause.getSubFilter();
                subFilter.accept(this);
                break;
        }
    }

}
