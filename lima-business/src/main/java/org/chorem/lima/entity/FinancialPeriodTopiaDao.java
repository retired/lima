/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import org.nuiton.topia.persistence.HqlAndParametersBuilder;

import java.util.Date;
import java.util.List;

public class FinancialPeriodTopiaDao extends AbstractFinancialPeriodTopiaDao<FinancialPeriod> {

    /**
     * Return FinancialPeriod by Date.
     * Date is include between financialperiod begin and end date.
     * 
     * @param date period middle date
     * @return FinancialPeriod for {@code date}
     */
    public FinancialPeriod findByDate(Date date) {

        HqlAndParametersBuilder<FinancialPeriod> builder = newHqlAndParametersBuilder();
        builder.addLowerOrEquals(FinancialPeriod.PROPERTY_BEGIN_DATE, date);
        builder.addGreaterOrEquals(FinancialPeriod.PROPERTY_END_DATE, date);

        FinancialPeriod financialPeriod = findUnique(builder.getHql(), builder.getHqlParameters());
        return financialPeriod;
    }

    public FinancialPeriod findByDateIfExists(Date date) {

        HqlAndParametersBuilder<FinancialPeriod> builder = newHqlAndParametersBuilder();
        builder.addLowerOrEquals(FinancialPeriod.PROPERTY_BEGIN_DATE, date);
        builder.addGreaterOrEquals(FinancialPeriod.PROPERTY_END_DATE, date);

        FinancialPeriod financialPeriod = findUniqueOrNull(builder.getHql(), builder.getHqlParameters());
        return financialPeriod;
    }
    
    /**
     * Return all FinancialPeriod with begin date between two given dates.
     * 
     * @param beginDateFirst period begin date
     * @param beginDateLast period end date
     * @return FinancialPeriod for {@code date}
     */
    public List<FinancialPeriod> findForBeginDateBetween(Date beginDateFirst, Date beginDateLast) {

        HqlAndParametersBuilder<FinancialPeriod> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(FinancialPeriod.PROPERTY_BEGIN_DATE, beginDateFirst);
        builder.addLowerOrEquals(FinancialPeriod.PROPERTY_BEGIN_DATE, beginDateLast);
        builder.setOrderByArguments(FinancialPeriod.PROPERTY_BEGIN_DATE);

        List<FinancialPeriod> financialPeriod = findAll(builder.getHql(), builder.getHqlParameters());
        return financialPeriod;
    }

    /**
     * Retourne toutes les periodes ordonnées par date de debut de periode.
     * 
     * @return all period ordered
     */
    public List<FinancialPeriod> findAllOrderByBeginDate() {

        List<FinancialPeriod> financialPeriod = newQueryBuilder()
                .setOrderByArguments(FinancialPeriod.PROPERTY_BEGIN_DATE)
                .findAll();

        return financialPeriod;
    }

}
