/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import java.util.List;


/**
 * Ajout de requetes specifiques aux {@link org.chorem.lima.entity.FinancialStatement} sur le DAO.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialStatementTopiaDao extends AbstractFinancialStatementTopiaDao<FinancialStatement> {

    /**
     * Find all {@code FinancialStatement} ordered by topia create date.
     */
    public List<FinancialStatement> findAllOrderedByCreateDate() {

        List<FinancialStatement> result = newQueryBuilder()
                .setOrderByArguments(FinancialStatement.PROPERTY_TOPIA_CREATE_DATE)
                .findAll();

        return result;
    }
    
    /**
     * Find all {@code FinancialStatement} ordered by topia create date.
     */
    public List<FinancialStatement> findChildrenFinancialStatement(FinancialStatement financialStatement) {

        List<FinancialStatement> result = forMasterFinancialStatementEquals(financialStatement)
                .setOrderByArguments(FinancialStatement.PROPERTY_TOPIA_CREATE_DATE)
                .findAll();

        return result;

    }

} //FinancialStatementDAO
