package org.chorem.lima.entity;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.support.TopiaSqlWork;

import java.util.List;

public class LimaCallaoTopiaPersistenceContext extends AbstractLimaCallaoTopiaPersistenceContext implements TopiaHibernateSupport, TopiaSqlSupport {

    public LimaCallaoTopiaPersistenceContext(AbstractTopiaPersistenceContextConstructorParameter parameter){
        super(parameter);
    }

    @Override
    public Session getHibernateSession() {
        return getHibernateSupport().getHibernateSession();
    }

    @Override
    public SessionFactory getHibernateFactory() {
        return getHibernateSupport().getHibernateFactory();
    }

    @Override
    public Configuration getHibernateConfiguration() {
        return getHibernateSupport().getHibernateConfiguration();
    }

    @Override
    public void executeSql(String sqlScript) {
        getSqlSupport().executeSql(sqlScript);
    }

    @Override
    public void doSqlWork(TopiaSqlWork sqlWork) {
        getSqlSupport().doSqlWork(sqlWork);
    }

    @Override
    public <O> O findSingleResult(TopiaSqlQuery<O> query) {
        return getSqlSupport().findSingleResult(query);
    }

    @Override
    public <O> List<O> findMultipleResult(TopiaSqlQuery<O> query) {
        return getSqlSupport().findMultipleResult(query);
    }

} //LimaCallaoTopiaPersistenceContext
