package org.chorem.lima.entity;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.flyway.TopiaFlywayServiceImpl;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaConfigurationConstants;
import org.nuiton.topia.persistence.TopiaException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class LimaFlywayServiceImpl  extends TopiaFlywayServiceImpl {

    private static final Log log = LogFactory.getLog(LimaFlywayServiceImpl.class);

    protected final static String INITIAL_VERSION = "";
    protected final static String OLD_TABLE_VERSION = "TMS_VERSION";
    protected final static String VERSION_FROM_OLD_TABLE_VERSION = "SELECT VERSION FROM " + OLD_TABLE_VERSION + ";";
    protected final static String FLYWAY_TABLE_VERSION = "schema_version";

    protected Connection getConnection(TopiaApplicationContext topiaApplicationContext) throws SQLException {

        Map<String, String> configuration = topiaApplicationContext.getConfiguration();

        String url = configuration.get(TopiaConfigurationConstants.CONFIG_URL);
        String user = configuration.get(TopiaConfigurationConstants.CONFIG_USER);
        String password = configuration.get(TopiaConfigurationConstants.CONFIG_PASS);

        DriverManager.registerDriver(new org.h2.Driver());
        DriverManager.registerDriver(new org.postgresql.Driver());

        Connection connection = DriverManager.getConnection(url, user, password);

        return connection;

    }

    @Override
    public void initTopiaService(TopiaApplicationContext topiaApplicationContext, Map<String, String> serviceConfiguration) {

        Connection connection = null;
        ResultSet flywayTableVersion = null;
        ResultSet topiaTableVersion = null;
        PreparedStatement preparedStatement = null;
        ResultSet topiaVersion = null;

        try {
            connection = getConnection(topiaApplicationContext);
            flywayTableVersion = connection.getMetaData().getTables(null, null, FLYWAY_TABLE_VERSION, null);

            if (!flywayTableVersion.first()) {
                topiaTableVersion = connection.getMetaData().getTables(null, null, OLD_TABLE_VERSION, null);
                if (topiaTableVersion.first()) {
                    preparedStatement = connection.prepareStatement(VERSION_FROM_OLD_TABLE_VERSION);
                    topiaVersion = preparedStatement.executeQuery();
                    topiaVersion.first();
                    flywayBaselineVersion = topiaVersion.getString(1);
                } else {
                    flywayBaselineVersion = INITIAL_VERSION;
                }
            }

        } catch (SQLException e) {
            throw new TopiaException(e);
        } finally {
            closeQuietly(connection, flywayTableVersion, topiaTableVersion, preparedStatement, topiaVersion);
        }

        super.initTopiaService(topiaApplicationContext, serviceConfiguration);

    }

    @Override
    public void initOnCreateSchema() {
        flywayBaselineVersion = null;
        super.initOnCreateSchema();
    }

    public static void closeQuietly(AutoCloseable... toClose) {
        for (AutoCloseable closeable : toClose) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (Exception e) {
                    if (log.isErrorEnabled()) {
                        log.error("Unable to close", e);
                    }
                }
            }
        }
    }


}
