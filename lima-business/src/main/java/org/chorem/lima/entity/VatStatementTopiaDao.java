/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import org.nuiton.topia.persistence.HqlAndParametersBuilder;

import java.util.List;


/**
 * Ajout de requetes specifiques aux {@code VatStatement} sur le DAO.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class VatStatementTopiaDao extends AbstractVatStatementTopiaDao<VatStatement> {

    /**
     * Find all {@code VatStatement} ordered by topia create date.
     */
    public List<VatStatement> findAllOrderedByCreateDate() {

        List<VatStatement> result = newQueryBuilder()
                .setOrderByArguments(VatStatement.PROPERTY_TOPIA_CREATE_DATE)
                .findAll();

        return result;
    }

    /**
     * Get masterVatStatement children statement.
     * 
     * @param masterVatStatement master VatStatement
     * @return children VatStatement
     */
    public List<VatStatement> getChildrenVatStatement(VatStatement masterVatStatement) {

        List<VatStatement> result = forMasterVatStatementEquals(masterVatStatement)
                .setOrderByArguments(VatStatement.PROPERTY_TOPIA_CREATE_DATE)
                .findAll();

        return result;
    }

    /**
     * Recherche un statement par equivalence de label.
     * 
     * TODO voir a quoi ca sert vraiment.
     * 
     * @param label label
     * @return statement like label
     */
    public VatStatement findVatStatementByLabel(String label) {

        HqlAndParametersBuilder<VatStatement> builder = newHqlAndParametersBuilder();
        builder.addLike(VatStatement.PROPERTY_LABEL, "%" + label + "%");
        VatStatement result = findAnyOrNull(builder.getHql(), builder.getHqlParameters());

        return result;
    }

} //VatStatementDAO
