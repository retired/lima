/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AccountTopiaDao extends AbstractAccountTopiaDao<Account> {
    protected static final Log log = LogFactory.getLog(AccountTopiaDao.class);

    /**
     * Retourne tous les comptes qui n'ont pas eux meme de sous compte.
     * 
     * @return leaf accounts
     */
    public List<Account> findAllLeafAccounts() {
// TODO DCossé 14/08/14 La requête suivante fonctionne mais est moins performante que le traitement java
//        String query = "SELECT a." + Account.PROPERTY_ACCOUNT_NUMBER +" FROM " +
//                Account.class.getName() + " a " +
//                "WHERE LENGTH( a." + Account.PROPERTY_ACCOUNT_NUMBER +")=" +
//                "(SELECT MAX(LENGTH(b." + Account.PROPERTY_ACCOUNT_NUMBER +")) " +
//                "  FROM " + Account.class.getName() + " b" +
//                "  WHERE b." + Account.PROPERTY_ACCOUNT_NUMBER +
//                "  LIKE CONCAT (a." + Account.PROPERTY_ACCOUNT_NUMBER + ",'%')" +
//                ")";
//
//        List<Account> result = findAll(query);

        List<Account> result = findAll();
        Iterator<Account> itAccount = result.iterator();
        while (itAccount.hasNext()) {
            Account acc = itAccount.next();
            for (Account acc2 : result) {
                if (!acc2.getAccountNumber().equals(acc.getAccountNumber()) &&
                        acc2.getAccountNumber().startsWith(acc.getAccountNumber())) {
                    itAccount.remove();
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Find account contained into account number interval.
     * 
     * @param accountNumberLow min account number
     * @param accountNumberHigh max account number
     * @return account list
     */
    public List<Account> findIntervalAccountByNumber(String accountNumberLow,
                                                     String accountNumberHigh) {
        HqlAndParametersBuilder<Account> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(Account.PROPERTY_ACCOUNT_NUMBER, accountNumberLow);
        builder.addLowerOrEquals(Account.PROPERTY_ACCOUNT_NUMBER, accountNumberHigh);
        List<Account> accounts = forHql(builder.getHql(), builder.getHqlParameters()).findAll();
        return accounts;
    }

    /**
     * Retourne tous les comptes dont le numero commence par celui specifié.
     * 
     * @param account parent account
     * @return
     */
    public List<Account> findAllSubAccounts(Account account) {

        HqlAndParametersBuilder<Account> builder = newHqlAndParametersBuilder();
        builder.addLike(Account.PROPERTY_ACCOUNT_NUMBER, account.getAccountNumber() + "_%");
        List<Account> accounts = forHql(builder.getHql(), builder.getHqlParameters()).findAll();
        return accounts;
    }

    public List<Account> findLeafAccounts(String accountNumber) throws TopiaException {
        String query = "FROM " + Account.class.getName() + " a WHERE a NOT IN (" +
                "FROM " + Account.class.getName() + " b WHERE b.accountNumber LIKE a.accountNumber+'%')" +
                " AND a.accountNumber LIKE :accountNumber";
        Map<String, Object> args = Maps.newLinkedHashMap();
        args.put("accountNumber",accountNumber);
        List<Account> accountsResult = findAll(query, args);
        return accountsResult;
    }
}
