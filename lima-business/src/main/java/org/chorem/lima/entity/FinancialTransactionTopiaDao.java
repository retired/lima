/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.filter.Filter;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Ajout de requetes specifiques aux financial transaction sur le DAO.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialTransactionTopiaDao extends AbstractFinancialTransactionTopiaDao<FinancialTransaction> {

    private static final Log log = LogFactory.getLog(FinancialTransactionTopiaDao.class);

    protected static String getBalancedClause(String alias, boolean balanced) {
        String hql = "(SELECT sum(" + Entry.PROPERTY_AMOUNT + ") FROM " + Entry.class.getName() +
                "   WHERE " + Entry.PROPERTY_DEBIT + " = true " +
                "   AND " + Entry.PROPERTY_FINANCIAL_TRANSACTION + " = " + alias + ") " +
                (balanced ? "=" : "!=") + " " +
                "(SELECT sum(" + Entry.PROPERTY_AMOUNT + ") FROM " + Entry.class.getName() +
                "   WHERE " + Entry.PROPERTY_DEBIT + " = false " +
                "   AND " + Entry.PROPERTY_FINANCIAL_TRANSACTION + " = " + alias + ")";

        return hql;
    }

    /**
     * Return how many transaction are found with specified entryBook.
     * 
     * @param entryBook entry book
     * @return transaction referencing entry book
     */
    public long getCountByEntryBook(EntryBook entryBook) {
        long count = forEquals(FinancialTransaction.PROPERTY_ENTRY_BOOK, entryBook).count();
        return count;
    }

    /**
     * Find all transactions between two dates.
     * 
     * @param beginDate begin date
     * @param endDate end date
     * @return unbalanced transactions
     */
    public List<FinancialTransaction> findAllByDates(Date beginDate, Date endDate) {

        HqlAndParametersBuilder<FinancialTransaction> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, endDate);
        builder.setOrderByArguments(FinancialTransaction.PROPERTY_TRANSACTION_DATE, FinancialTransaction.PROPERTY_TOPIA_CREATE_DATE);
        List<FinancialTransaction> transactions = findAll(builder.getHql(), builder.getHqlParameters());
        return transactions;
    }

    /**
     * Find all transactions between two dates in an entry book.
     * 
     * @param beginDate begin date
     * @param endDate end date
     * @param entryBook entry book
     * @return unbalanced transactions
     */
    public List<FinancialTransaction> findAllByDates(Date beginDate, Date endDate, EntryBook entryBook) {

        HqlAndParametersBuilder<FinancialTransaction> builder = newHqlAndParametersBuilder();
        builder.addEquals(FinancialTransaction.PROPERTY_ENTRY_BOOK, entryBook);
        builder.addGreaterOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, endDate);
        builder.setOrderByArguments(FinancialTransaction.PROPERTY_TRANSACTION_DATE, FinancialTransaction.PROPERTY_TOPIA_CREATE_DATE);
        List<FinancialTransaction> transactions = findAll(builder.getHql(), builder.getHqlParameters());
        return transactions;

    }

    /**
     * Find all unbalanced transactions.
     * 
     * @param beginDate beginDate
     * @param endDate endDate
     * @param entryBook entry book (can be null)
     * @return if exist unbalanced transactions
     */
    public List<FinancialTransaction> getAllUnbalancedTransaction(Date beginDate, Date endDate,
            EntryBook entryBook) {

        HqlAndParametersBuilder<FinancialTransaction> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, endDate);
        if (entryBook != null) {
            builder.addEquals(FinancialTransaction.PROPERTY_ENTRY_BOOK, entryBook);
        }
        builder.addWhereClause(getBalancedClause(builder.getAlias(), false));
        builder.setOrderByArguments(FinancialTransaction.PROPERTY_TRANSACTION_DATE, FinancialTransaction.PROPERTY_TOPIA_CREATE_DATE);
        List<FinancialTransaction> transactions = findAll(builder.getHql(), builder.getHqlParameters());
        return transactions;
    }
    
    /**
     * Find all balanced transactions.
     * 
     * @param beginDate beginDate
     * @param endDate endDate
     * @param entryBook entry book (can be null)
     * @return balanced transactions
     */
    public List<FinancialTransaction> getAllBalancedTransaction(Date beginDate, Date endDate,
            EntryBook entryBook) {

        HqlAndParametersBuilder<FinancialTransaction> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, endDate);
        if (entryBook != null) {
            builder.addEquals(FinancialTransaction.PROPERTY_ENTRY_BOOK, entryBook);
        }
        builder.addWhereClause(getBalancedClause(builder.getAlias(), true));
        builder.setOrderByArguments(FinancialTransaction.PROPERTY_TRANSACTION_DATE, FinancialTransaction.PROPERTY_TOPIA_CREATE_DATE);
        List<FinancialTransaction> transactions = findAll(builder.getHql(), builder.getHqlParameters());
        return transactions;
    }
    
    /**
     * Find all unbalanced transactions.
     * 
     * @param beginDate beginDate
     * @param endDate endDate
     * @param entryBook entry book (can be null)
     * @return unbalanced transactions
     */
    public List<FinancialTransaction> getAllIncorrectTransaction(Date beginDate, Date endDate,
            EntryBook entryBook) {
        String query = "SELECT distinct T FROM " + FinancialTransaction.class.getName() + " T"+
                " LEFT JOIN T.entry AS E" +
                " WHERE (" +
                getBalancedClause("T", false) +
                " OR (SELECT sum(amount) FROM " + Entry.class.getName() +
                "   WHERE debit = true AND financialTransaction = T) IS NULL" +
                " OR (SELECT sum(amount) FROM " + Entry.class.getName() +
                "   WHERE debit = false AND financialTransaction = T) IS NULL" +
                " OR E.account = null" +
                " OR E.voucher = null" +
                " OR E.voucher = ''" +
                " OR E.description = null" +
                " OR E.description = '')" +
                " AND :beginDate <= T.transactionDate" +
                " AND T.transactionDate <= :endDate";
        List<FinancialTransaction> result;

        Map<String, Object> args = Maps.newLinkedHashMap();
        args.put("beginDate",beginDate);
        args.put("endDate", endDate);
        if (entryBook != null) {
            args.put("entryBook", entryBook);
            query += " AND T.entryBook = :entryBook";
            result = findAll(query, args);
        } else {
            result = findAll(query, args);
        }

        return result;
    }

    /**
     * find all transactions without entry book.
     * 
     * @param beginDate begin date
     * @param endDate end date
     * @return transactions without entry books
     */
    public List<FinancialTransaction>  findAllTransactionWithoutEntryBook(Date beginDate, Date endDate) {

        HqlAndParametersBuilder<FinancialTransaction> builder = newHqlAndParametersBuilder();
        builder.addGreaterOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, beginDate);
        builder.addLowerOrEquals(FinancialTransaction.PROPERTY_TRANSACTION_DATE, endDate);
        builder.addNull(FinancialTransaction.PROPERTY_ENTRY_BOOK);

        List<FinancialTransaction> transactions = findAll(builder.getHql(), builder.getHqlParameters());
        return transactions;

    }

    /**
     * Search financial transaction.
     * 
     *
     * @param filter filter parameters
     * @return financial transaction
     */
    public List<FinancialTransaction> searchFinancialTransaction(Filter filter) {

        GeneratorHQuery generator = new GeneratorHQuery();
        filter.accept(generator);


        String query = generator.getHQuery();
        Map<String,Object> arguments = generator.getParameters();

        if (log.isDebugEnabled()) {
            Set<String> parameters = arguments.keySet();
            for(String param : parameters) {
                log.debug("Query : \n" + query + "\nArguments: " + param);
            }
        }

        // perform query
        List<FinancialTransaction> result;
        if (arguments.isEmpty()) {
            result = findAll(query);
        } else {
            result = findAll(query, arguments);
        }

        if (log.isDebugEnabled()) {
            log.debug("Number of founded transactions : " + result.size());
        }

        return result;
    }

    public List<FinancialTransaction> findAllByDates() {
        HqlAndParametersBuilder<FinancialTransaction> builder = newHqlAndParametersBuilder();
        builder.setOrderByArguments(FinancialTransaction.PROPERTY_TRANSACTION_DATE, FinancialTransaction.PROPERTY_TOPIA_CREATE_DATE);
        List<FinancialTransaction> transactions = findAll(builder.getHql(), builder.getHqlParameters());
        return transactions;
    }
} //FinancialTransactionDAO
