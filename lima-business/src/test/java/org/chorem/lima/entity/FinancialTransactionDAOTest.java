/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import org.chorem.lima.business.AbstractLimaTest;
import org.chorem.lima.business.exceptions.LimaException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

/**
 * Test for {@link FinancialTransactionTopiaDao}.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialTransactionDAOTest extends AbstractLimaTest {

    @Before
    public void initTest() throws Exception {
        initTestDatabase();
    }

    /**
     * Test la recherche des transactions non equilibrées.
     * 
     * @throws TopiaException
     * @throws LimaException 
     */
    @Test
    public void testFindAllUnbalancedTransactions() throws Exception {

        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
        initTestWithFinancialTransaction();

        FinancialPeriod financialPeriod = financialPeriodService.getAllFinancialPeriods().get(0);
        EntryBook journalDesVentes = entryBookService.getEntryBookByCode("jdv");

        LimaCallaoTopiaPersistenceContext tcontext = context.newPersistenceContext();
        FinancialTransactionTopiaDao ftDAO = tcontext.getFinancialTransactionDao();
        Assert.assertTrue(ftDAO.getAllUnbalancedTransaction(
                financialPeriod.getBeginDate(),
                financialPeriod.getEndDate(),
                journalDesVentes).isEmpty());
    }
}
