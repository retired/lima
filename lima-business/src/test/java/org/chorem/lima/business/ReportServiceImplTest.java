/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import org.chorem.lima.beans.ReportsDatas;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Test le service de génération des reports.
 * 
 * les rapports dans ReportService est commenté.
 */
public class ReportServiceImplTest extends AbstractLimaTest {

    @Before
    public void initTest() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
        initTestWithFinancialTransaction();
    }

    /**
     * Test de génération du rapport des comptes.
     * 
     * @throws Exception 
     */
    @Test
    public void testGenerateAccountsReports() throws Exception {
        
        Account accountBefa = accountService.getAccountByNumber("51");
        Assert.assertNotNull(accountBefa);
        
        Date beginDate = df.parse("April 1, 2012");
        Date endDate = df.parse("May 31, 2012");
        ReportsDatas datas = reportService.generateAccountsReports(accountBefa, false, beginDate, endDate);
        Assert.assertEquals(BigDecimal.valueOf(42), datas.getAmountCredit().stripTrailingZeros());
        Assert.assertEquals(BigDecimal.valueOf(42), datas.getAmountDebit().stripTrailingZeros());
        
        beginDate = df.parse("May 1, 2012");
        datas = reportService.generateAccountsReports(accountBefa, false, beginDate, endDate);
        Assert.assertEquals(BigDecimal.valueOf(0), datas.getAmountCredit().stripTrailingZeros());
        Assert.assertEquals(BigDecimal.valueOf(0), datas.getAmountDebit().stripTrailingZeros());
    }

    /**
     * Test de génération du rapport des comptes, en verifiant que les transactions
     * non équilibrées ne sont pas présentes (modification 0.6).
     * 
     * @throws Exception 
     */
    @Test
    public void testGenerateAccountsReportsUnlalanced() throws Exception {
        EntryBook journalDesVentes = entryBookService.getEntryBookByCode("jdv");
        Account accountVmpVae = accountService.getAccountByNumber("511");
    
        FinancialTransaction transaction1 = new FinancialTransactionImpl();
        transaction1.setTransactionDate(df.parse("April 5, 2012"));
        transaction1.setEntryBook(journalDesVentes);
        transaction1 = financialTransactionService.createFinancialTransactionWithEntries(transaction1, transaction1.getEntry());

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(54.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setFinancialTransaction(transaction1);
        tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucher");
        financialTransactionService.createEntry(tr1Entry1);
        
        Date beginDate = df.parse("April 1, 2012");
        Date endDate = df.parse("May 31, 2012");
        ReportsDatas datas = reportService.generateAccountsReports(accountVmpVae, false, beginDate, endDate);
        Assert.assertEquals(BigDecimal.valueOf(42), datas.getAmountCredit().stripTrailingZeros());
        Assert.assertEquals(BigDecimal.valueOf(42), datas.getAmountDebit().stripTrailingZeros());
    }
}
