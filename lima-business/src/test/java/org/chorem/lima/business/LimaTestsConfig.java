package org.chorem.lima.business;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.topia.flyway.TopiaFlywayService;
import org.nuiton.topia.flyway.TopiaFlywayServiceImpl;

import java.util.Map;
import java.util.Properties;

/**
 * Created by davidcosse on 11/06/14.
 */
public class LimaTestsConfig {

    public LimaTestsConfig(String configFileName, Properties limaTestConfig) {
        LimaBusinessConfig instance = LimaBusinessConfig.getInstance(configFileName);
        Properties standardLimaConfig = instance.getConfig().getFlatOptions();

        for (Map.Entry<Object, Object> entry : limaTestConfig.entrySet()) {
            standardLimaConfig.setProperty((String)entry.getKey(),(String)entry.getValue());
        }
        ApplicationConfig testConfig = new ApplicationConfig(standardLimaConfig);
        instance.setConfig(testConfig);
        setRootContextProperties(instance);
    }

    protected void setRootContextProperties(LimaBusinessConfig instance) {
        Properties result = instance.getFlatOptions();
        // add persistence classes from generated code

        Map<String, String> toAddIfNotPresent = Maps.newLinkedHashMap();
        toAddIfNotPresent.put("topia.service.migration", TopiaFlywayServiceImpl.class.getName());
        toAddIfNotPresent.put("topia.service.migration." + TopiaFlywayService.USE_MODEL_VERSION, "true");

        for (Map.Entry<String, String> entry : toAddIfNotPresent.entrySet()) {
            if (!result.containsKey(entry.getKey())) {
                result.setProperty(entry.getKey(), entry.getValue());
            }
        }
        instance.setRootContextProperties(result);
    }

}
