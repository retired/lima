/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import com.google.common.collect.Lists;
import org.chorem.lima.business.ejb.FinancialTransactionServiceImpl;
import org.chorem.lima.business.exceptions.AfterLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeforeFirstFiscalPeriodException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.LockedEntryBookException;
import org.chorem.lima.business.exceptions.LockedFinancialPeriodException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionImpl;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Test on financial transaction service.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialTransactionServiceImplTest extends AbstractLimaTest {

    @Before
    public void initTest() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
    }

    @Test
    public void createNewFinancialTransactionTest() {

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        Assert.assertNotNull(transaction);
        Assert.assertNull(transaction.getEntryBook());
        Assert.assertNull(transaction.getTransactionDate());
        Assert.assertEquals(0, transaction.getEntry().size());

    }

    @Test
    public void createNewEntryTest() {

        Entry entry = financialTransactionService.createNewEntry();

        Assert.assertNotNull(entry);
        Assert.assertNull(entry.getFinancialTransaction());
        Assert.assertNull(entry.getDescription());
        Assert.assertNull(entry.getDetail());
        Assert.assertNull(entry.getLettering());
        Assert.assertNull(entry.getAccount());
        Assert.assertNull(entry.getLettering());
        Assert.assertNull(entry.getVoucher());
        Assert.assertEquals(BigDecimal.ZERO, entry.getAmount());
        Assert.assertFalse(entry.isDebit());

    }

    @Test
    public void createFinancialTransactionTest() throws Exception {

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBookService.getEntryBookByCode("jdv"));
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        FinancialTransaction transactionSave = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Assert.assertNotNull(transactionSave);
        Assert.assertEquals(transaction.getEntryBook(), transactionSave.getEntryBook());
        Assert.assertEquals(transaction.getTransactionDate(), transactionSave.getTransactionDate());
        Assert.assertTrue(transactionSave.isPersisted());
    }

    @Test(expected = LockedFinancialPeriodException.class)
    public void createFinancialTransactionFailLockedFinancialPeriodeTest() throws Exception {

        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);

        FiscalPeriod nextFiscalPeriod = new FiscalPeriodImpl();
        nextFiscalPeriod.setBeginDate(DateUtil.createDate(1, 1, 2013));
        nextFiscalPeriod.setEndDate(DateUtil.createDate(31, 12, 2013));
        fiscalPeriodService.createFiscalPeriod(nextFiscalPeriod);

        fiscalPeriodService.blockFiscalPeriod(fiscalPeriod);

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBookService.getEntryBookByCode("jdv"));
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

    }

    @Test(expected = LockedEntryBookException.class)
    public void createFinancialTransactionFailLockedEntryBookPeriodeTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");
        List<FinancialPeriod> financialPeriods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

    }

    @Test(expected = AfterLastFiscalPeriodException.class)
    public void createFinancialTransactionFailAfterLastFiscalPeriodeTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2014));

        financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

    }

    @Test(expected = BeforeFirstFiscalPeriodException.class)
    public void createFinancialTransactionFailBeforeFirstFiscalPeriodeTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2010));

        financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

    }

    @Test
    public void updateFinancialTransactionTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        entryBook = entryBookService.getEntryBookByCode("jda");

        transaction.setEntryBook(entryBook);
        Date date = DateUtil.createDate(15, 1, 2012);
        transaction.setTransactionDate(date);

        financialTransactionService.updateFinancialTransaction(transaction);

        List<FinancialTransaction> transactions = financialTransactionService.getAllFinancialTransactions(date, date);

        Assert.assertEquals(1, transactions.size());
        Assert.assertTrue(transactions.contains(transaction));
    }

    @Test(expected = AfterLastFiscalPeriodException.class)
    public void updateFinancialTransactionFailAfterLastFiscalPeriodTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Date date = DateUtil.createDate(15, 1, 2014);
        transaction.setTransactionDate(date);

        financialTransactionService.updateFinancialTransaction(transaction);
    }

    @Test(expected = BeforeFirstFiscalPeriodException.class)
    public void updateFinancialTransactionFailBeforeFirstFiscalPeriodTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Date date = DateUtil.createDate(15, 1, 2010);
        transaction.setTransactionDate(date);

        financialTransactionService.updateFinancialTransaction(transaction);
    }

    @Test(expected = LockedFinancialPeriodException.class)
    public void updateFinancialTransactionFailLockedFinancialPeriodTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");
        Account accountVmpVae = accountService.getAccountByNumber("511");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();
        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(54.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucher");
        transaction.addEntry(tr1Entry1);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);

        FiscalPeriod nextFiscalPeriod = new FiscalPeriodImpl();
        nextFiscalPeriod.setBeginDate(DateUtil.createDate(1, 1, 2013));
        nextFiscalPeriod.setEndDate(DateUtil.createDate(31, 12, 2013));
        fiscalPeriodService.createFiscalPeriod(nextFiscalPeriod);

        fiscalPeriodService.blockFiscalPeriod(fiscalPeriod);

        Date date = DateUtil.createDate(15, 1, 2012);
        transaction.setTransactionDate(date);

        financialTransactionService.updateFinancialTransaction(transaction);
    }

    @Test(expected = LockedEntryBookException.class)
    public void updateFinancialTransactionFailLockedEntryBookOrigTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");
        Account accountVmpVae = accountService.getAccountByNumber("511");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(54.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucher");
        transaction.addEntry(tr1Entry1);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        List<FinancialPeriod> financialPeriods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);

        entryBook = entryBookService.getEntryBookByCode("jda");
        transaction.setEntryBook(entryBook);

        financialTransactionService.updateFinancialTransaction(transaction);
    }

    @Test
    public void removeFinancialTransactionTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        Date date = DateUtil.createDate(12, 1, 2012);
        transaction.setTransactionDate(date);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        financialTransactionService.removeFinancialTransaction(transaction);

        List<FinancialTransaction> transactions = financialTransactionService.getAllFinancialTransactions(date, date);

        Assert.assertEquals(0, transactions.size());
    }

    @Test(expected = LockedFinancialPeriodException.class)
    public void removeFinancialTransactionFailLockedFinancialPeriodTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");
        Account accountVmpVae = accountService.getAccountByNumber("511");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();
        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(54.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucher");
        transaction.addEntry(tr1Entry1);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);

        FiscalPeriod nextFiscalPeriod = new FiscalPeriodImpl();
        nextFiscalPeriod.setBeginDate(DateUtil.createDate(1, 1, 2013));
        nextFiscalPeriod.setEndDate(DateUtil.createDate(31, 12, 2013));
        fiscalPeriodService.createFiscalPeriod(nextFiscalPeriod);

        fiscalPeriodService.blockFiscalPeriod(fiscalPeriod);

        financialTransactionService.removeFinancialTransaction(transaction);
    }

    @Test(expected = LockedEntryBookException.class)
    public void removeFinancialTransactionFailLockedEntryBookTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");
        Account accountVmpVae = accountService.getAccountByNumber("511");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();
        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(54.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucher");
        transaction.addEntry(tr1Entry1);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        List<FinancialPeriod> financialPeriods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);

        financialTransactionService.removeFinancialTransaction(transaction);
    }

    @Test
    public void createEntryTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Entry entry = new EntryImpl();
        entry.setAccount(accountService.getAccountByNumber("511"));
        entry.setVoucher("Voucher");
        entry.setDebit(true);
        entry.setAmount(BigDecimal.valueOf(14.36));
        transaction.addEntry(entry);

        Entry entry1 = financialTransactionService.createEntry(entry);

        Assert.assertTrue(entry1.isPersisted());

    }

    @Test(expected = LockedFinancialPeriodException.class)
    public void createEntryFailLockedFinancialPeriodTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry entry = new EntryImpl();
        entry.setAccount(accountService.getAccountByNumber("511"));
        entry.setVoucher("Voucher");
        entry.setDebit(true);
        entry.setDescription("Test");
        entry.setAmount(BigDecimal.valueOf(14.36));
        transaction.addEntry(entry);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);

        FiscalPeriod nextFiscalPeriod = new FiscalPeriodImpl();
        nextFiscalPeriod.setBeginDate(DateUtil.createDate(1, 1, 2013));
        nextFiscalPeriod.setEndDate(DateUtil.createDate(31, 12, 2013));
        fiscalPeriodService.createFiscalPeriod(nextFiscalPeriod);

        fiscalPeriodService.blockFiscalPeriod(fiscalPeriod);

        Entry entry1 = new EntryImpl();
        entry1.setAccount(accountService.getAccountByNumber("511"));
        entry1.setVoucher("Voucher");
        entry1.setDebit(false);
        entry.setDescription("Test2");
        entry1.setAmount(BigDecimal.valueOf(15.36));
        transaction.addEntry(entry1);

        financialTransactionService.createEntry(entry1);

    }

    @Test(expected = LockedEntryBookException.class)
    public void createEntryFailLockedEntryBookTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry entry = new EntryImpl();
        entry.setAccount(accountService.getAccountByNumber("511"));
        entry.setVoucher("Voucher");
        entry.setDebit(true);
        entry.setAmount(BigDecimal.valueOf(14.36));
        entry.setDescription("Test");
        transaction.addEntry(entry);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        List<FinancialPeriod> financialPeriods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);

        Entry entry2 = new EntryImpl();
        entry2.setAccount(accountService.getAccountByNumber("511"));
        entry2.setVoucher("Voucher");
        entry2.setDebit(true);
        entry2.setAmount(BigDecimal.valueOf(14.36));
        transaction.addEntry(entry2);

        financialTransactionService.createEntry(entry2);

    }

    @Test
    public void updateEntryTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        Date date = DateUtil.createDate(12, 1, 2012);
        transaction.setTransactionDate(date);
        Entry entry = new EntryImpl();
        entry.setAccount(accountService.getAccountByNumber("511"));
        entry.setVoucher("Voucher");
        entry.setDebit(true);
        entry.setDescription("Test");
        entry.setAmount(BigDecimal.valueOf(14.36));
        transaction.addEntry(entry);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Entry entry1 = Lists.newArrayList(transaction.getEntry()).get(0);

        entry1.setAccount(accountService.getAccountByNumber("501"));
        entry1.setVoucher("VoucherBis");
        entry1.setDebit(true);
        entry1.setAmount(BigDecimal.valueOf(15.21));

        financialTransactionService.updateEntry(entry1);

        FinancialTransaction transactionSave = financialTransactionService.getAllFinancialTransactions(date, date).get(0);

        Assert.assertEquals(1, transactionSave.sizeEntry());
        Entry entrySave = transactionSave.getEntry().iterator().next();
        Assert.assertEquals(entry1, entrySave);
        Assert.assertEquals("501", entrySave.getAccount().getAccountNumber());
        Assert.assertEquals("VoucherBis", entrySave.getVoucher());
        Assert.assertEquals(true, entrySave.isDebit());
        Assert.assertEquals(BigDecimal.valueOf(15.21), entrySave.getAmount());

    }

    @Test(expected = LockedFinancialPeriodException.class)
    public void updateEntryFailLockedFinancialPeriodTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry entry = new EntryImpl();
        entry.setAccount(accountService.getAccountByNumber("511"));
        entry.setVoucher("Voucher");
        entry.setDescription("Decription");
        entry.setDebit(true);
        entry.setAmount(BigDecimal.valueOf(14.36));
        transaction.addEntry(entry);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Entry entry1 = Lists.newArrayList(transaction.getEntry()).get(0);
        financialTransactionService.updateFinancialTransaction(transaction);

        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);

        FiscalPeriod nextFiscalPeriod = new FiscalPeriodImpl();
        nextFiscalPeriod.setBeginDate(DateUtil.createDate(1, 1, 2013));
        nextFiscalPeriod.setEndDate(DateUtil.createDate(31, 12, 2013));
        fiscalPeriodService.createFiscalPeriod(nextFiscalPeriod);

        fiscalPeriodService.blockFiscalPeriod(fiscalPeriod);

        entry1.setAccount(accountService.getAccountByNumber("501"));
        entry1.setVoucher("VoucherBis");
        entry.setDescription("Decription");
        entry1.setDebit(true);
        entry1.setAmount(BigDecimal.valueOf(15.21));

        financialTransactionService.updateEntry(entry1);

    }

    @Test(expected = LockedEntryBookException.class)
    public void updateEntryFailLockedEntryBookTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry entry1 = new EntryImpl();
        entry1.setAccount(accountService.getAccountByNumber("511"));
        entry1.setVoucher("Voucher");
        entry1.setDebit(true);
        entry1.setAmount(BigDecimal.valueOf(14.36));
        entry1.setDescription("Description");
        transaction.addEntry(entry1);

        Entry entry2 = new EntryImpl();
        entry2.setAccount(accountService.getAccountByNumber("501"));
        entry2.setVoucher("Voucher");
        entry2.setDebit(false);
        entry2.setAmount(BigDecimal.valueOf(14.36));
        entry2.setDescription("Description");
        transaction.addEntry(entry2);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Entry entrySave1 = Lists.newArrayList(transaction.getEntry()).get(0);

        financialTransactionService.updateFinancialTransaction(transaction);

        List<FinancialPeriod> financialPeriods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);

        entrySave1.setAccount(accountService.getAccountByNumber("501"));
        entrySave1.setVoucher("VoucherBis");
        entrySave1.setDebit(true);
        entrySave1.setAmount(BigDecimal.valueOf(15.21));

        financialTransactionService.updateEntry(entrySave1);

    }

    @Test
    public void removeEntryTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        Date date = DateUtil.createDate(12, 1, 2012);
        transaction.setTransactionDate(date);

        Entry entry = new EntryImpl();
        entry.setAccount(accountService.getAccountByNumber("511"));
        entry.setVoucher("Voucher");
        entry.setDebit(true);
        entry.setAmount(BigDecimal.valueOf(14.36));
        transaction.addEntry(entry);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Entry entrySave1 = Lists.newArrayList(transaction.getEntry()).get(0);

        financialTransactionService.updateFinancialTransaction(transaction);

        financialTransactionService.removeEntry(entrySave1);

        FinancialTransaction transactionSave = financialTransactionService.getAllFinancialTransactions(date, date).get(0);

        Assert.assertEquals(0, transactionSave.sizeEntry());

    }

    @Test(expected = LockedFinancialPeriodException.class)
    public void removeEntryFailLockedFinancialPeriodTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry entry = new EntryImpl();
        entry.setAccount(accountService.getAccountByNumber("511"));
        entry.setVoucher("Voucher");
        entry.setDescription("Decription");
        entry.setDebit(true);
        entry.setAmount(BigDecimal.valueOf(14.36));
        transaction.addEntry(entry);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Entry entry1 = Lists.newArrayList(transaction.getEntry()).get(0);

        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);

        FiscalPeriod nextFiscalPeriod = new FiscalPeriodImpl();
        nextFiscalPeriod.setBeginDate(DateUtil.createDate(1, 1, 2013));
        nextFiscalPeriod.setEndDate(DateUtil.createDate(31, 12, 2013));
        fiscalPeriodService.createFiscalPeriod(nextFiscalPeriod);

        fiscalPeriodService.blockFiscalPeriod(fiscalPeriod);

        financialTransactionService.removeEntry(entry1);

    }

    @Test(expected = LockedEntryBookException.class)
    public void removeEntryFailLockedEntryBookTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction = financialTransactionService.createNewFinancialTransaction();

        transaction.setEntryBook(entryBook);
        transaction.setTransactionDate(DateUtil.createDate(12, 1, 2012));

        Entry entry1 = new EntryImpl();
        entry1.setAccount(accountService.getAccountByNumber("511"));
        entry1.setVoucher("Voucher");
        entry1.setDebit(true);
        entry1.setAmount(BigDecimal.valueOf(14.36));
        entry1.setDescription("Description");
        transaction.addEntry(entry1);

        Entry entry2 = new EntryImpl();
        entry2.setAccount(accountService.getAccountByNumber("501"));
        entry2.setVoucher("Voucher");
        entry2.setDebit(false);
        entry2.setAmount(BigDecimal.valueOf(14.36));
        entry2.setDescription("Description");
        transaction.addEntry(entry2);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        Entry entrySave1 = Lists.newArrayList(transaction.getEntry()).get(0);

        financialTransactionService.updateFinancialTransaction(transaction);

        List<FinancialPeriod> financialPeriods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);

        financialTransactionService.removeEntry(entrySave1);

    }


    /**
     * Test to find all unbalanced transactions.
     * Nothing wrong here.
     * 
     * @throws ParseException 
     * @throws org.chorem.lima.business.exceptions.LimaException
     */
    @Test
    public void testGetInexactTransactionAllGood() throws ParseException, LimaException {
        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);
        List<FinancialTransaction> transactions = financialTransactionService.getAllInexactFinancialTransactions(fiscalPeriod);
        Assert.assertTrue(transactions.isEmpty());
    }

    /**
     * Test to find all unbalanced transactions.
     * 
     * wrong data.
     * 
     * @throws ParseException 
     * @throws LimaException 
     */
    @Test
    public void testGetInexactTransactionNotAllGood() throws ParseException, LimaException {

        EntryBook journalDesVentes = entryBookService.getEntryBookByCode("jdv");
        Account accountVmpVae = accountService.getAccountByNumber("511");

        FinancialTransaction transaction1 = new FinancialTransactionImpl();
        transaction1.setTransactionDate(df.parse("April 5, 2012"));
        transaction1.setEntryBook(journalDesVentes);

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(42.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setFinancialTransaction(transaction1);
        //tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucher");
        transaction1.addEntry(tr1Entry1);

        Entry tr1Entry2 = new EntryImpl();
        tr1Entry2.setAmount(BigDecimal.valueOf(42.0));
        tr1Entry2.setDebit(true);
        tr1Entry2.setAccount(accountVmpVae);
        tr1Entry2.setFinancialTransaction(transaction1);
        tr1Entry2.setDescription("test desc");
        tr1Entry2.setVoucher("voucher");
        transaction1.addEntry(tr1Entry2);

        transaction1 = financialTransactionService.createFinancialTransactionWithEntries(transaction1, transaction1.getEntry());

        // one in period
        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);
        List<FinancialTransaction> transactions = financialTransactionService.getAllInexactFinancialTransactions(fiscalPeriod);
        Assert.assertEquals(1, transactions.size());
    }
    
    /**
     * Test to find all unbalanced transactions.
     * 
     * Test only unbalanced transactions (no data errors : fields).
     * 
     * @throws ParseException 
     * @throws LimaException 
     */
    @Test
    public void testGetUnbalancedTransactionNotAllGood() throws Exception {

        EntryBook journalDesVentes = entryBookService.getEntryBookByCode("jdv");
        Account accountVmpVae = accountService.getAccountByNumber("511");

        FinancialTransaction transaction1 = new FinancialTransactionImpl();
        transaction1.setTransactionDate(df.parse("April 5, 2012"));
        transaction1.setEntryBook(journalDesVentes);

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(54.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setFinancialTransaction(transaction1);
        tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucher");
        transaction1.addEntry(tr1Entry1);

        financialTransactionService.createFinancialTransactionWithEntries(transaction1, transaction1.getEntry());

        // one in period
        FiscalPeriod fiscalPeriod = fiscalPeriodService.getAllFiscalPeriods().get(0);
        List<FinancialTransaction> transactions = financialTransactionService.getAllInexactFinancialTransactions(fiscalPeriod);
        Assert.assertEquals(1, transactions.size());
    }

    @Test
    public void testLettersAfter() {

        Assert.assertEquals("A", FinancialTransactionServiceImpl.LETTERS_AFTER.apply(null));
        Assert.assertEquals("A", FinancialTransactionServiceImpl.LETTERS_AFTER.apply(""));
        Assert.assertEquals("E", FinancialTransactionServiceImpl.LETTERS_AFTER.apply("D"));
        Assert.assertEquals("AA", FinancialTransactionServiceImpl.LETTERS_AFTER.apply("Z"));
        Assert.assertEquals("ASDGUAAAA", FinancialTransactionServiceImpl.LETTERS_AFTER.apply("ASDGTZZZZ"));

    }

    @Test
    public void testFindLastLetter() {

        FinancialTransactionServiceImpl instance = new FinancialTransactionServiceImpl();

        String nextLetter = instance.findLastLetter(Arrays.asList("A", "BZ", "ZZZ", "C", "Z", "E"));
        Assert.assertEquals("ZZZ", nextLetter);

        nextLetter = instance.findLastLetter(Arrays.asList("sfvq", "sfvr"));
        Assert.assertEquals("", nextLetter);

        nextLetter = instance.findLastLetter(Arrays.asList("zzz", "ABC", "DEF"));
        Assert.assertEquals("DEF", nextLetter);

        nextLetter = instance.findLastLetter(new ArrayList<>());
        Assert.assertEquals("", nextLetter);
    }
}
