/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.EntryBookService;
import org.chorem.lima.business.api.EntryService;
import org.chorem.lima.business.api.ExportService;
import org.chorem.lima.business.api.FinancialPeriodService;
import org.chorem.lima.business.api.FinancialStatementService;
import org.chorem.lima.business.api.FinancialTransactionService;
import org.chorem.lima.business.api.FiscalPeriodService;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.ImportService;
import org.chorem.lima.business.api.ReportService;
import org.chorem.lima.business.api.VatStatementService;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountImpl;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookImpl;
import org.chorem.lima.entity.EntryImpl;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FinancialTransactionImpl;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodImpl;
import org.chorem.lima.entity.LimaCallaoEntityEnum;
import org.chorem.lima.entity.LimaCallaoTopiaApplicationContext;
import org.hibernate.cfg.Environment;
import org.junit.Before;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.ClassPathI18nInitializer;
import org.nuiton.topia.persistence.TopiaApplicationContextCache;

import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.UUID;

/**
 * Common initialization code for all lima tests.
 *
 * @author chatellier
 * @version $Revision$
 * <p/>
 * Last update : $Date$
 * By : $Author$
 */
public abstract class AbstractLimaTest {

    protected static final Log log = LogFactory.getLog(AbstractLimaTest.class);
    protected static final Function<Properties, LimaCallaoTopiaApplicationContext> CREATE_CONTEXT_FUNCTION = new Function<Properties, LimaCallaoTopiaApplicationContext>() {
        @Override
        public LimaCallaoTopiaApplicationContext apply(Properties input) {
            return new LimaCallaoTopiaApplicationContext(input);
        }
    };

    protected static final DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH);
    protected static final String LIMA_TEST_DEFAULT_CONF_FILENAME = "lima-test.properties";

    protected AccountService accountService;
    protected EntryBookService entryBookService;
    protected FinancialPeriodService financialPeriodService;
    protected FinancialTransactionService financialTransactionService;
    protected FiscalPeriodService fiscalPeriodService;
    protected ReportService reportService;
    protected EntryService entryService;
    protected ImportService importService;
    protected ExportService exportService;
    protected FinancialStatementService financialStatementService;
    protected VatStatementService vatStatementService;
    protected IdentityService identityService;

    protected LimaCallaoTopiaApplicationContext context;

    /**
     * This is a before class method, but junit will fail to run inherited
     * BeforeClass methods if there is one.
     *
     * @throws Exception
     */
    @Before
    public void initAbstractTest() throws Exception {
        setUpLocale();
        Properties options = getTestConfiguration();
        new LimaTestsConfig(LIMA_TEST_DEFAULT_CONF_FILENAME, options);
        initServices();
        context = createNewTestApplicationContext();
    }

    protected void setUpLocale() {
        I18n.init(new ClassPathI18nInitializer(), Locale.UK);
    }

    /**
     * Init services after i18n#init().
     */
    protected void initServices() {
        if(accountService == null) {
            LimaServiceFactory.initFactory(LimaBusinessConfig.getInstance().getConfig());
            accountService = LimaServiceFactory.getService(AccountService.class);
            entryBookService = LimaServiceFactory.getService(EntryBookService.class);
            financialPeriodService = LimaServiceFactory.getService(FinancialPeriodService.class);
            financialTransactionService = LimaServiceFactory.getService(FinancialTransactionService.class);
            fiscalPeriodService = LimaServiceFactory.getService(FiscalPeriodService.class);
            reportService = LimaServiceFactory.getService(ReportService.class);
            entryService = LimaServiceFactory.getService(EntryService.class);
            financialStatementService = LimaServiceFactory.getService(FinancialStatementService.class);
            vatStatementService = LimaServiceFactory.getService(VatStatementService.class);

            importService = LimaServiceFactory.getService(ImportService.class);
            exportService = LimaServiceFactory.getService(ExportService.class);
            identityService = LimaServiceFactory.getService(IdentityService.class);
        }
    }

    /**
     * Return a new instance of configuration defined on a new database at
     * each call.
     *
     * @return single test config
     */
    protected Properties getTestConfiguration() {

        // do not call parse() method (don't read /etc...)
        // load file manually (lima-business.config)
        Properties testProperties = new Properties();
        // override somes
        String testDir = System.getProperty("java.io.tmpdir") + File.separator + "lima-business-" + UUID.randomUUID().toString();
//        testProperties.setProperty(LimaBusinessConfig.ServiceConfigOption.DATA_DIR.getKey(), testDir);
        testProperties.setProperty(Environment.URL, "jdbc:h2:file:" + testDir + File.separator + "data");
        testProperties.setProperty(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        testProperties.setProperty("topia.persistence.classes", LimaCallaoEntityEnum.getImplementationClassesAsString());
        return testProperties;
    }


    /**
     * Method to use only for class that need a context to be tester.
     * Only for DOA for now.
     *
     * @return a topia context
     */
    protected LimaCallaoTopiaApplicationContext getTestApplicationContext(Properties options) {
        log.info("Opening context to database : " + options.getProperty("hibernate.connection.url"));

        LimaCallaoTopiaApplicationContext result = TopiaApplicationContextCache.getContext(options, CREATE_CONTEXT_FUNCTION);
        return result;
    }

    protected LimaCallaoTopiaApplicationContext createNewTestApplicationContext() {
        Properties options = LimaBusinessConfig.getInstance().getFlatOptions();
        LimaCallaoTopiaApplicationContext result = getTestApplicationContext(options);
        return result;
    }

    /**
     * Create a basic database.
     *
     * Not called by default (<code>@Before</code>) to allow init test with
     * CSV or EBP import.
     *
     */
    protected void initTestDatabase() {
        context.createSchema();
    }

    /**
     * Create a basic account plan.
     *
     * @throws org.chorem.lima.business.exceptions.AlreadyExistAccountException
     * @throws org.chorem.lima.business.exceptions.NotAllowedLabelException
     * @throws org.chorem.lima.business.exceptions.InvalidAccountNumberException
     * @throws org.chorem.lima.business.exceptions.NotNumberAccountNumberException
     */
    protected void initTestWithAccounts() throws Exception {

        // creation d'un plan compatble de test
        Account classFinancier = new AccountImpl();
        classFinancier.setAccountNumber("5");
        classFinancier.setLabel("Comptes financiers");
        accountService.createAccount(classFinancier);

        Account accountVmp = new AccountImpl();
        accountVmp.setAccountNumber("50");
        accountVmp.setLabel("Valeurs mobilières de placement");
        accountService.createAccount(accountVmp);

        Account accountPel = new AccountImpl();
        accountPel.setAccountNumber("501");
        accountPel.setLabel("Parts dans des entreprises liées");
        accountService.createAccount(accountPel);

        Account accountAP = new AccountImpl();
        accountAP.setAccountNumber("502");
        accountAP.setLabel("Actions propres");
        accountService.createAccount(accountAP);

        Account accountBefa = new AccountImpl();
        accountBefa.setAccountNumber("51");
        accountBefa.setLabel("Banques établissements financiers et assimilés");
        accountService.createAccount(accountBefa);

        Account accountVmpVae = new AccountImpl();
        accountVmpVae.setAccountNumber("511");
        accountVmpVae.setLabel("Valeurs à l'encaissement");
        accountService.createAccount(accountVmpVae);

        Account accountBanques = new AccountImpl();
        accountBanques.setAccountNumber("512");
        accountBanques.setLabel("Banques");
        accountService.createAccount(accountBanques);
    }

    /**
     * Create some EntryBooks.
     *
     * @throws org.chorem.lima.business.exceptions.LimaException
     * @throws java.text.ParseException
     */
    protected void initTestWithEntryBooks() throws Exception {
        // creation d'un journal
        EntryBook journalDesVentes = new EntryBookImpl();
        journalDesVentes.setLabel("Journal des ventes");
        journalDesVentes.setCode("jdv");
        entryBookService.createEntryBook(journalDesVentes);

        EntryBook journalDesAchats = new EntryBookImpl();
        journalDesAchats.setLabel("Journal des achats");
        journalDesAchats.setCode("jda");
        entryBookService.createEntryBook(journalDesAchats);

        EntryBook myEntryBook = new EntryBookImpl();
        myEntryBook.setCode("JRN");
        myEntryBook.setLabel("MyJournal");
        entryBookService.createEntryBook(myEntryBook);

    }

    /**
     * Create a FiscalPeriod with an EntryBook.
     *
     * @throws org.chorem.lima.business.exceptions.LimaException
     * @throws java.text.ParseException
     */
    protected void initTestWithFiscalPeriod() throws Exception {
        // creation d'un exercice fiscal
        FiscalPeriod fiscalPeriod = new FiscalPeriodImpl();
        fiscalPeriod.setBeginDate(df.parse("January 1, 2012"));
        fiscalPeriod.setEndDate(df.parse("December 31, 2012"));
        fiscalPeriodService.createFiscalPeriod(fiscalPeriod);
    }

    /**
     * Create FinancialTransaction with 2 entries
     * @throws Exception
     */
    protected void initTestWithFinancialTransaction() throws Exception {

        Account accountVmpVae = accountService.getAccountByNumber("511");
        EntryBook journalDesVentes = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction1 = new FinancialTransactionImpl();
        transaction1.setTransactionDate(df.parse("April 4, 2012"));
        transaction1.setEntryBook(journalDesVentes);
        //transaction1 = financialTransactionService.createFinancialTransactionSkeleton(transaction1);

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(42.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setFinancialTransaction(transaction1);
        tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucher");
        //tr1Entry1 = financialTransactionService.createEntry(tr1Entry1);

        Entry tr1Entry2 = new EntryImpl();
        tr1Entry2.setAmount(BigDecimal.valueOf(42.0));
        tr1Entry2.setDebit(true);
        tr1Entry2.setAccount(accountVmpVae);
        tr1Entry2.setFinancialTransaction(transaction1);
        tr1Entry2.setDescription("test desc");
        tr1Entry2.setVoucher("voucher");
        //tr1Entry2 = financialTransactionService.createEntry(tr1Entry2);

        //transaction1.setEntry(Lists.newArrayList(tr1Entry1, tr1Entry2));
        financialTransactionService.createFinancialTransactionWithEntries(transaction1, Lists.newArrayList(tr1Entry1, tr1Entry2));
    }


    protected void initTestWithSomeFinancialTransaction() throws Exception {

        Account accountVmpVae = accountService.getAccountByNumber("511");
        EntryBook journalDesVentes = entryBookService.getEntryBookByCode("jdv");

        FinancialTransaction transaction1 = new FinancialTransactionImpl();
        transaction1.setTransactionDate(df.parse("April 4, 2012"));
        transaction1.setEntryBook(journalDesVentes);
        //transaction1 = financialTransactionService.createFinancialTransactionSkeleton(transaction1);

        Entry tr1Entry1 = new EntryImpl();
        tr1Entry1.setAmount(BigDecimal.valueOf(42.0));
        tr1Entry1.setAccount(accountVmpVae);
        tr1Entry1.setFinancialTransaction(transaction1);
        tr1Entry1.setDescription("test desc");
        tr1Entry1.setVoucher("voucherA");
//        tr1Entry1 = financialTransactionService.createEntry(tr1Entry1);

        Entry tr1Entry2 = new EntryImpl();
        tr1Entry2.setAmount(BigDecimal.valueOf(42.0));
        tr1Entry2.setDebit(true);
        tr1Entry2.setAccount(accountVmpVae);
        tr1Entry2.setFinancialTransaction(transaction1);
        tr1Entry2.setDescription("test desc");
        tr1Entry2.setVoucher("voucherA");
//        tr1Entry2 = financialTransactionService.createEntry(tr1Entry2);

        financialTransactionService.createFinancialTransactionWithEntries(transaction1, Lists.newArrayList(tr1Entry1, tr1Entry2));

        FinancialTransaction transaction2 = new FinancialTransactionImpl();
        transaction2.setTransactionDate(df.parse("April 4, 2012"));
        transaction2.setEntryBook(journalDesVentes);
        //transaction2 = financialTransactionService.createFinancialTransactionSkeleton(transaction2);

        Entry tr1Entry1b = new EntryImpl();
        tr1Entry1b.setAmount(BigDecimal.valueOf(12.0));
        tr1Entry1b.setAccount(accountVmpVae);
        tr1Entry1b.setFinancialTransaction(transaction2);
        tr1Entry1b.setDescription("test desc");
        tr1Entry1b.setVoucher("voucherB");
//        financialTransactionService.createEntry(tr1Entry1b);

        Entry tr1Entry2b = new EntryImpl();
        tr1Entry2b.setAmount(BigDecimal.valueOf(6.0));
        tr1Entry2b.setDebit(true);
        tr1Entry2b.setAccount(accountVmpVae);
        tr1Entry2b.setFinancialTransaction(transaction2);
        tr1Entry2b.setDescription("test desc");
        tr1Entry2b.setVoucher("voucherB");
//        financialTransactionService.createEntry(tr1Entry2b);

        Entry tr1Entry3b = new EntryImpl();
        tr1Entry3b.setAmount(BigDecimal.valueOf(6.0));
        tr1Entry3b.setDebit(true);
        tr1Entry3b.setAccount(accountVmpVae);
        tr1Entry3b.setFinancialTransaction(transaction2);
        tr1Entry3b.setDescription("test desc");
        tr1Entry3b.setVoucher("voucherB");
//        financialTransactionService.createEntry(tr1Entry3b);
        financialTransactionService.createFinancialTransactionWithEntries(transaction1, Lists.newArrayList(tr1Entry1b, tr1Entry2b, tr1Entry3b));
    }


    protected FinancialTransaction createFinancialTransaction(String entryBookCode, Date date,
                                                              String accountDebitNumber, String accountCreditNumber,
                                                              BigDecimal amount) throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode(entryBookCode);
        Account accountDebit = accountService.getAccountByNumber(accountDebitNumber);
        Account accountCredit = accountService.getAccountByNumber(accountCreditNumber);

        FinancialTransaction transaction = new FinancialTransactionImpl();
        transaction.setTransactionDate(date);
        transaction.setEntryBook(entryBook);

        Entry entry1 = new EntryImpl();
        entry1.setAmount(amount);
        entry1.setDebit(true);
        entry1.setAccount(accountDebit);
        entry1.setDescription("test desc");
        entry1.setVoucher("voucher");
        transaction.addEntry(entry1);

        Entry entry2 = new EntryImpl();
        entry2.setAmount(amount);
        entry2.setDebit(false);
        entry2.setAccount(accountCredit);
        entry2.setDescription("test desc");
        entry2.setVoucher("voucher");
        transaction.addEntry(entry2);

        transaction = financialTransactionService.createFinancialTransactionWithEntries(transaction, transaction.getEntry());

        return transaction;
    }
}