/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import org.chorem.lima.business.exceptions.AlreadyExistEntryBookException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.UsedEntryBookException;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

/**
 * Tests pour la gestion des journaux.
 * <p/>
 * L'application peut gérer plusieurs journaux pour la gestion des transactions.
 * Il est tester ici l'ajout, la recherche.
 *
 * @author Rémi Chapelet
 */
public class EntryBookServiceImplTest extends AbstractLimaTest {

    protected static final String JOURNAL_DES_VENTES = "Journal des ventes";

    @Before
    public void initTest() throws Exception {
        initTestWithEntryBooks();
    }

    @Test
    public void getEntryBookByCodeTest() {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");
        Assert.assertNotNull(entryBook);
        Assert.assertEquals("jdv", entryBook.getCode());
        Assert.assertEquals("Journal des ventes", entryBook.getLabel());

        entryBook = entryBookService.getEntryBookByCode("XXX");
        Assert.assertNull(entryBook);
    }

    @Test
    public void getAllEntryBooksTest() throws LimaException {
        List<EntryBook> allEntryBooks = entryBookService.getAllEntryBooks();

        Assert.assertEquals(3, allEntryBooks.size());

        Iterator<EntryBook> iterator = allEntryBooks.iterator();

        EntryBook entryBook = iterator.next();
        Assert.assertEquals("JRN", entryBook.getCode());
        Assert.assertEquals("MyJournal", entryBook.getLabel());

        entryBook = iterator.next();
        Assert.assertEquals("jda", entryBook.getCode());
        Assert.assertEquals("Journal des achats", entryBook.getLabel());

        entryBook = iterator.next();
        Assert.assertEquals("jdv", entryBook.getCode());
        Assert.assertEquals("Journal des ventes", entryBook.getLabel());
    }

    @Test
    public void createNewEntryBookTest() {
        EntryBook newEntryBook = entryBookService.createNewEntryBook();
        Assert.assertNull(newEntryBook.getCode());
        Assert.assertNull(newEntryBook.getLabel());

        List<EntryBook> allEntryBooks = entryBookService.getAllEntryBooks();

        Assert.assertEquals(3, allEntryBooks.size());
        Assert.assertFalse(allEntryBooks.contains(newEntryBook));
    }

    @Test
    public void createOrUpdateEntryBookTest() {
        EntryBook entryBook = new EntryBookImpl();
        entryBook.setCode("jdm");
        entryBook.setLabel("journal des machins");
        boolean updated = entryBookService.createOrUpdateEntryBook(entryBook);
        Assert.assertFalse(updated);
        Assert.assertEquals(4, entryBookService.getAllEntryBooks().size());
        entryBook = entryBookService.getEntryBookByCode("jdm");
        Assert.assertNotNull(entryBook);
        Assert.assertEquals("jdm", entryBook.getCode());
        Assert.assertEquals("journal des machins", entryBook.getLabel());

        entryBook = new EntryBookImpl();
        entryBook.setCode("jdv");
        entryBook.setLabel("journal des ventes bis");
        updated = entryBookService.createOrUpdateEntryBook(entryBook);
        Assert.assertTrue(updated);
        Assert.assertEquals(4, entryBookService.getAllEntryBooks().size());
        entryBook = entryBookService.getEntryBookByCode("jdv");
        Assert.assertNotNull(entryBook);
        Assert.assertEquals("jdv", entryBook.getCode());
        Assert.assertEquals("journal des ventes bis", entryBook.getLabel());
    }

    @Test
    public void createEntryBookTest() throws AlreadyExistEntryBookException {

        EntryBook entryBook = new EntryBookImpl();
        entryBook.setCode("jdm");
        entryBook.setLabel("journal des machins");

        entryBook = entryBookService.createEntryBook(entryBook);
        Assert.assertEquals("jdm", entryBook.getCode());
        Assert.assertEquals("journal des machins", entryBook.getLabel());

        List<EntryBook> allEntryBooks = entryBookService.getAllEntryBooks();

        Assert.assertEquals(4, allEntryBooks.size());
        Assert.assertTrue(allEntryBooks.contains(entryBook));
    }

    @Test(expected = AlreadyExistEntryBookException.class)
    public void createEntryBookFailAlreadyExistTest() throws AlreadyExistEntryBookException {

        EntryBook entryBook = new EntryBookImpl();
        entryBook.setCode("jdv");
        entryBook.setLabel("journal des ventes bis");

        entryBookService.createEntryBook(entryBook);
    }


    /**
     * Permet de tester la modification d'un journal suivant son préfixe.
     *
     * @throws org.chorem.lima.business.exceptions.LimaException
     */
    @Test
    public void modifyJournalTest() throws LimaException {
        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");
        Assert.assertNotNull(entryBook);
        Assert.assertEquals(JOURNAL_DES_VENTES, entryBook.getLabel());
        entryBook.setLabel("Journal des achats");
        entryBookService.updateEntryBook(entryBook);

        // Recherche du journal dans la bdd
        entryBook = entryBookService.getEntryBookByCode("jdv");
        Assert.assertNotNull(entryBook);
        Assert.assertEquals("Journal des achats", entryBook.getLabel());
    }

    /**
     * Test que la suppression d'un journal utilisé n'est pas possible.
     * 
     * @throws LimaException
     */
    @Test(expected=UsedEntryBookException.class)
    public void deleteUsedEntryBook() throws Exception {
        initTestWithFiscalPeriod();
        initTestWithFinancialTransaction();
        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        entryBookService.removeEntryBook(entryBook);
    }

    /**
     * Un journal tout juste créé doit pouvoir être supprimé, y compris
     * les closed qui sont lié entre le journal et les periodes.
     * 
     * @throws LimaException
     */
    @Test
    public void deleteNonUsedEntryBookTest() throws LimaException {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        entryBookService.removeEntryBook(entryBook);
        List<EntryBook> allEntryBooks = entryBookService.getAllEntryBooks();

        Assert.assertEquals(2, allEntryBooks.size());

        Iterator<EntryBook> iterator = allEntryBooks.iterator();

        entryBook = iterator.next();
        Assert.assertEquals("JRN", entryBook.getCode());
        Assert.assertEquals("MyJournal", entryBook.getLabel());

        entryBook = iterator.next();
        Assert.assertEquals("jda", entryBook.getCode());
        Assert.assertEquals("Journal des achats", entryBook.getLabel());
        
    }
}
