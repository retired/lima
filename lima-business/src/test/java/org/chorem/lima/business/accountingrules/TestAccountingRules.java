/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.accountingrules;

import org.apache.commons.lang3.time.DateUtils;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialPeriodImpl;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodTopiaDao;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Cette classe existe car les tests tourne 2 fois:
 *  - default
 *  - fr rule set
 * 
 * Par contre, dans default, il manque du code et cela ne peut pas fonctionner
 * juste avec default. Donc le code manquant est ajouter dans ce jeux de regles
 * de test et on lance les tests unitaires avec TestAccountingRules un fois
 * et FranceAccountingRules une autre fois.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
@Ignore
public class TestAccountingRules extends DefaultAccountingRules {

    /**
     * Copier/coller de la methode france.
     * Le probleme ici est que le default n'en creer aucune.
     */
    public List<FinancialPeriod> createFiscalPeriodRules(FiscalPeriod fiscalPeriod)
            throws MoreOneUnlockFiscalPeriodException,
            NotBeginNextDayOfLastFiscalPeriodException,
            BeginAfterEndFiscalPeriodException {
        super.createFiscalPeriodRules(fiscalPeriod);
        List<FinancialPeriod> financialPeriods = new ArrayList<>();
        FiscalPeriodTopiaDao fiscalPeriodDAO = getDaoHelper().getFiscalPeriodDao();

        //Checks if is not the first fiscalperiod to create
        if (fiscalPeriodDAO.count() != 0) {

            FiscalPeriod lastFiscalPeriod = fiscalPeriodDAO.getLastFiscalPeriod();

            //check the new fiscal period adjoining the last
            Date dateLastFiscalPeriod = lastFiscalPeriod.getEndDate();
            dateLastFiscalPeriod = DateUtils.addDays(dateLastFiscalPeriod, 1);
            dateLastFiscalPeriod = DateUtils.truncate(dateLastFiscalPeriod, Calendar.DATE);
            Date dateFiscalPeriod = fiscalPeriod.getBeginDate();
            if (dateLastFiscalPeriod.compareTo(dateFiscalPeriod) != 0) {
                throw new NotBeginNextDayOfLastFiscalPeriodException(lastFiscalPeriod);
            }

            //We can create a new fiscal period meantime the last fiscal period was not locked
            //But not the ante periodfiscal
            int unblockedFiscalPeriod = fiscalPeriodDAO.forLockedEquals(false).findAll().size();
            if (unblockedFiscalPeriod > 1) {
                throw new MoreOneUnlockFiscalPeriodException(fiscalPeriod.getBeginDate(), fiscalPeriod.getEndDate(), fiscalPeriod.isLocked(), unblockedFiscalPeriod);
            }
        }

        // FinancialPeriods of 1 month are created
        Date endDate = fiscalPeriod.getEndDate();
        Date loopDate = fiscalPeriod.getBeginDate();
        while (loopDate.before(endDate)) {
            FinancialPeriod financialPeriod = new FinancialPeriodImpl();
            //important for fiscalperiod created from import, it can be locked, so financialperiods must be locked
            financialPeriod.setLocked(fiscalPeriod.isLocked());
            financialPeriod.setBeginDate(loopDate);
            loopDate = DateUtils.addMonths(loopDate, 1);
            loopDate = DateUtils.truncate(loopDate, Calendar.MONTH);
            loopDate = DateUtils.addMilliseconds(loopDate, -1);
            if (loopDate.after(endDate)) {
                financialPeriod.setEndDate(endDate);
            } else {
                financialPeriod.setEndDate(loopDate);
            }
            //create it
            financialPeriods.add(financialPeriod);
            //financialPeriodService.createFinancialPeriodWithTransaction(financialPeriod, transaction);
            //loop incremente
            loopDate = DateUtils.addMilliseconds(loopDate, 1);
        }
        return financialPeriods;
    }
}
