/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.business.exceptions.AlreadyLockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.LastUnlockedFiscalPeriodException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NoEmptyFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountImpl;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookImpl;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Tests pour la gestion des périodes
 * <p/>
 * Fonctions :
 * _ création d'une période
 * _ valider une période (si elle est correcte)
 * _ bloquer une période (ATTENTION : on ne peut débloquer une période !)
 * _ rechercher une période
 *
 * @author Rémi Chapelet
 */
public class FiscalPeriodServiceImplTest extends AbstractLimaTest {

    /** log. */
    private static final Log log = LogFactory
            .getLog(FiscalPeriodServiceImplTest.class);

    @Before
    public void initTest() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
    }

    @Test
    public void createPeriod12MonthsTest() throws Exception {

        FiscalPeriod period = new FiscalPeriodImpl();
        Date bedingDate = DateUtil.createDate(01, 01, 2012);
        period.setBeginDate(bedingDate);
        Date endDate = DateUtil.createDate(31, 12, 2012);
        period.setEndDate(endDate);

        FiscalPeriod periodSave = fiscalPeriodService.createFiscalPeriod(period);

        Assert.assertEquals(true, periodSave.isPersisted());
        Assert.assertEquals(bedingDate, periodSave.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(endDate), periodSave.getEndDate());
        List<FiscalPeriod> allFiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        Assert.assertEquals(1, allFiscalPeriods.size());
        Assert.assertEquals(periodSave, allFiscalPeriods.get(0));

        List<FinancialPeriod> financialPeriods = financialPeriodService.getAllFinancialPeriods();
        Assert.assertEquals(12, financialPeriods.size());
        Assert.assertEquals(DateUtil.createDate(01, 01, 2012), financialPeriods.get(0).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(31, 01, 2012)), financialPeriods.get(0).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 02, 2012), financialPeriods.get(1).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(29, 02, 2012)), financialPeriods.get(1).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 03, 2012), financialPeriods.get(2).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(31, 03, 2012)), financialPeriods.get(2).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 04, 2012), financialPeriods.get(3).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(30, 04, 2012)), financialPeriods.get(3).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 05, 2012), financialPeriods.get(4).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(31, 05, 2012)), financialPeriods.get(4).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 06, 2012), financialPeriods.get(5).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(30, 06, 2012)), financialPeriods.get(5).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 07, 2012), financialPeriods.get(6).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(31, 07, 2012)), financialPeriods.get(6).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 8, 2012), financialPeriods.get(7).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(31, 8, 2012)), financialPeriods.get(7).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 9, 2012), financialPeriods.get(8).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(30, 9, 2012)), financialPeriods.get(8).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 10, 2012), financialPeriods.get(9).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(31, 10, 2012)), financialPeriods.get(9).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 11, 2012), financialPeriods.get(10).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(30, 11, 2012)), financialPeriods.get(10).getEndDate());
        Assert.assertEquals(DateUtil.createDate(01, 12, 2012), financialPeriods.get(11).getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(DateUtil.createDate(31, 12, 2012)), financialPeriods.get(11).getEndDate());

    }

    @Test
    public void createPeriod6MonthsTest() throws Exception {

        FiscalPeriod period = new FiscalPeriodImpl();
        Date bedingDate = DateUtil.createDate(01, 01, 2012);
        period.setBeginDate(bedingDate);
        Date endDate = DateUtil.createDate(31, 06, 2012);
        period.setEndDate(endDate);

        FiscalPeriod periodSave = fiscalPeriodService.createFiscalPeriod(period);

        Assert.assertEquals(true, periodSave.isPersisted());
        Assert.assertEquals(bedingDate, periodSave.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(endDate), periodSave.getEndDate());
        List<FiscalPeriod> allFiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        Assert.assertEquals(1, allFiscalPeriods.size());
        Assert.assertEquals(periodSave, allFiscalPeriods.get(0));

    }

    @Test
    public void createPeriod24MonthsTest() throws Exception {

        FiscalPeriod period = new FiscalPeriodImpl();
        Date bedingDate = DateUtil.createDate(01, 01, 2012);
        period.setBeginDate(bedingDate);
        Date endDate = DateUtil.createDate(31, 12, 2013);
        period.setEndDate(endDate);

        FiscalPeriod periodSave = fiscalPeriodService.createFiscalPeriod(period);

        Assert.assertEquals(true, periodSave.isPersisted());
        Assert.assertEquals(bedingDate, periodSave.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(endDate), periodSave.getEndDate());
        List<FiscalPeriod> allFiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        Assert.assertEquals(1, allFiscalPeriods.size());
        Assert.assertEquals(periodSave, allFiscalPeriods.get(0));

    }



    @Test(expected = BeginAfterEndFiscalPeriodException.class)
    public void createPeriodFailBeginAfterEndFiscalPeriodTest() throws Exception {

        FiscalPeriod period = new FiscalPeriodImpl();
        Date bedingDate = DateUtil.createDate(31, 12, 2012);
        period.setBeginDate(bedingDate);
        Date endDate = DateUtil.createDate(01, 01, 2012);
        period.setEndDate(endDate);

        fiscalPeriodService.createFiscalPeriod(period);
    }



    @Test
    public void createPeriodSecondTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(01, 01, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        FiscalPeriod periodSave2 = fiscalPeriodService.createFiscalPeriod(period2);


        Assert.assertEquals(true, periodSave2.isPersisted());
        Assert.assertEquals(bedingDate2, periodSave2.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(endDate2), periodSave2.getEndDate());
        List<FiscalPeriod> allFiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        Assert.assertEquals(2, allFiscalPeriods.size());
        Assert.assertEquals(periodSave2, allFiscalPeriods.get(1));

    }

    @Test(expected = NotBeginNextDayOfLastFiscalPeriodException.class)
    public void createPeriodSecondFailNotBeginNextDayOfLastFiscalPeriodTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(01, 02, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        fiscalPeriodService.createFiscalPeriod(period2);

    }

    @Test(expected = MoreOneUnlockFiscalPeriodException.class)
    public void createPeriodSecondFailMoreOneUnlockFiscalPeriodTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(01, 01, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        fiscalPeriodService.createFiscalPeriod(period2);

        FiscalPeriod period3 = new FiscalPeriodImpl();
        Date bedingDate3 = DateUtil.createDate(01, 01, 2014);
        period3.setBeginDate(bedingDate3);
        Date endDate3 = DateUtil.createDate(31, 12, 2014);
        period3.setEndDate(endDate3);

        fiscalPeriodService.createFiscalPeriod(period3);

    }

    @Test
    public void blockFiscalPeriodTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(01, 01, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        fiscalPeriodService.createFiscalPeriod(period2);

        fiscalPeriodService.blockFiscalPeriod(periodSave1);

        List<FiscalPeriod> allFiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        periodSave1 = allFiscalPeriods.get(0);
        FiscalPeriod periodSave2 = allFiscalPeriods.get(1);

        Assert.assertEquals(true, periodSave1.isLocked());
        List<FiscalPeriod> blockedFiscalPeriods = fiscalPeriodService.getAllBlockedFiscalPeriods();
        Assert.assertEquals(1, blockedFiscalPeriods.size());
        Assert.assertEquals(true, blockedFiscalPeriods.contains(periodSave1));


        List<FiscalPeriod> unblockedFiscalPeriods = fiscalPeriodService.getAllUnblockedFiscalPeriods();
        Assert.assertEquals(1, unblockedFiscalPeriods.size());
        Assert.assertEquals(true, unblockedFiscalPeriods.contains(periodSave2));

        for (FinancialPeriod financialPeriod : financialPeriodService.getFinancialPeriodsWithBeginDateWithin(periodSave1.getBeginDate(), periodSave1.getEndDate())) {
            Assert.assertEquals(true, financialPeriod.isLocked());
            for (EntryBook entryBook : entryBookService.getAllEntryBooks()) {
                ClosedPeriodicEntryBook closedPeriodicEntryBook =
                        financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriod);

                Assert.assertEquals(true, closedPeriodicEntryBook.isLocked());
            }
        }

    }

    @Test(expected = AlreadyLockedFiscalPeriodException.class)
    public void blockFiscalPeriodFailAlreadyLockedTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(01, 01, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        fiscalPeriodService.createFiscalPeriod(period2);

        fiscalPeriodService.blockFiscalPeriod(periodSave1);

        FiscalPeriod periodBlockSave1 = fiscalPeriodService.getAllBlockedFiscalPeriods().get(0);

        fiscalPeriodService.blockFiscalPeriod(periodBlockSave1);

    }

    @Test(expected = LastUnlockedFiscalPeriodException.class)
    public void blockFiscalPeriodFailNotNextTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        fiscalPeriodService.blockFiscalPeriod(periodSave1);

    }

    @Test(expected = UnbalancedFinancialTransactionsException.class)
    public void blockFiscalPeriodFailUnbalancedFinancialTransactionsTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(01, 01, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        fiscalPeriodService.createFiscalPeriod(period2);

        FinancialTransaction transaction = createFinancialTransaction("jdv", DateUtil.createDate(04, 04, 2012), "511", "501", BigDecimal.valueOf(42.0));
        Entry entry = transaction.getEntry().iterator().next();
        entry.setAmount(BigDecimal.valueOf(12.36));
        financialTransactionService.updateEntry(entry);

        fiscalPeriodService.blockFiscalPeriod(periodSave1);
    }

    @Test(expected = UnfilledEntriesException.class)
    public void blockFiscalPeriodFailUnfilledVoucherTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(01, 01, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        fiscalPeriodService.createFiscalPeriod(period2);

        FinancialTransaction transaction = createFinancialTransaction("jdv", DateUtil.createDate(04, 04, 2012), "511", "501", BigDecimal.valueOf(42.0));
        Entry entry = transaction.getEntry().iterator().next();
        entry.setVoucher("");
        financialTransactionService.updateEntry(entry);

        fiscalPeriodService.blockFiscalPeriod(periodSave1);
    }

    @Test(expected = UnfilledEntriesException.class)
    public void blockFiscalPeriodFailUnfilledDescriptionTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(01, 01, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        fiscalPeriodService.createFiscalPeriod(period2);

        FinancialTransaction transaction = createFinancialTransaction("jdv", DateUtil.createDate(04, 04, 2012), "511", "501", BigDecimal.valueOf(42.0));
        Entry entry = transaction.getEntry().iterator().next();
        entry.setDescription("");
        financialTransactionService.updateEntry(entry);

        fiscalPeriodService.blockFiscalPeriod(periodSave1);
    }

    @Test
    public void deleteFiscalPeriodTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        fiscalPeriodService.deleteFiscalPeriod(periodSave1);

        List<FiscalPeriod> allFiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        Assert.assertEquals(0, allFiscalPeriods.size());

    }

    @Test(expected = NoEmptyFiscalPeriodException.class)
    public void deleteFiscalPeriodFailNoEmptyFiscalPeriodTest() throws Exception {

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        createFinancialTransaction("jdv", DateUtil.createDate(04, 04, 2012), "511", "501", BigDecimal.valueOf(42.0));

        fiscalPeriodService.deleteFiscalPeriod(periodSave1);
    }


    @Test
    public void retainedEarningsAndBlockFiscalPeriod() throws Exception {

        EntryBook atNew = new EntryBookImpl();
        atNew.setCode("an");
        atNew.setCode("A nouveau");

        atNew = entryBookService.createEntryBook(atNew);


        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(1, 1, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        FiscalPeriod period2 = new FiscalPeriodImpl();
        Date bedingDate2 = DateUtil.createDate(1, 1, 2013);
        period2.setBeginDate(bedingDate2);
        Date endDate2 = DateUtil.createDate(31, 12, 2013);
        period2.setEndDate(endDate2);

        fiscalPeriodService.createFiscalPeriod(period2);

        BigDecimal amount = BigDecimal.valueOf(42.14);
        createFinancialTransaction("jdv", DateUtil.createDate(4, 4, 2012), "511", "501", amount);

        fiscalPeriodService.retainedEarningsAndBlockFiscalPeriod(periodSave1, atNew);


        List<FiscalPeriod> allFiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        periodSave1 = allFiscalPeriods.get(0);
        FiscalPeriod periodSave2 = allFiscalPeriods.get(1);

        Assert.assertEquals(true, periodSave1.isLocked());
        List<FiscalPeriod> blockedFiscalPeriods = fiscalPeriodService.getAllBlockedFiscalPeriods();
        Assert.assertEquals(1, blockedFiscalPeriods.size());
        Assert.assertEquals(true, blockedFiscalPeriods.contains(periodSave1));


        List<FiscalPeriod> unblockedFiscalPeriods = fiscalPeriodService.getAllUnblockedFiscalPeriods();
        Assert.assertEquals(1, unblockedFiscalPeriods.size());
        Assert.assertEquals(true, unblockedFiscalPeriods.contains(periodSave2));

        for (FinancialPeriod financialPeriod : financialPeriodService.getFinancialPeriodsWithBeginDateWithin(periodSave1.getBeginDate(), periodSave1.getEndDate())) {
            Assert.assertEquals(true, financialPeriod.isLocked());
            for (EntryBook entryBook : entryBookService.getAllEntryBooks()) {
                ClosedPeriodicEntryBook closedPeriodicEntryBook =
                        financialPeriodService.getClosedPeriodicEntryBook(entryBook, financialPeriod);

                Assert.assertEquals(true, closedPeriodicEntryBook.isLocked());
            }
        }

        List<FinancialTransaction> transactionsAtNew =
                financialTransactionService.getAllFinancialTransactions(endDate1, DateUtil.getEndOfDay(endDate1));
        Assert.assertEquals(1, transactionsAtNew.size());
        FinancialTransaction transactionAtNew = transactionsAtNew.get(0);
        Assert.assertEquals(atNew, transactionAtNew.getEntryBook());
        Assert.assertEquals(4, transactionAtNew.sizeEntry());
        Map<String, BigDecimal> amountByAccount = Maps.newHashMap();

        for (Entry entry : transactionAtNew.getEntry()) {
            BigDecimal amountAccount = amountByAccount.get(entry.getAccount().getAccountNumber());
            if (amountAccount == null) {
                amountAccount = BigDecimal.ZERO;
            }
            if (entry.isDebit()) {
                amountAccount = amountAccount.subtract(entry.getAmount());
            } else {
                amountAccount = amountAccount.add(entry.getAmount());
            }
            amountByAccount.put(entry.getAccount().getAccountNumber(), amountAccount);
        }

        Assert.assertEquals(3, amountByAccount.size());
        Assert.assertEquals(true, amountByAccount.containsKey("501"));
        Assert.assertEquals(0, amount.compareTo(amountByAccount.get("501").multiply(BigDecimal.valueOf(-1))));

        Assert.assertEquals(true, amountByAccount.containsKey("511"));
        Assert.assertEquals(0, amount.compareTo(amountByAccount.get("511")));

        Assert.assertEquals(true, amountByAccount.containsKey("891"));
        Assert.assertEquals(0, BigDecimal.ZERO.compareTo(amountByAccount.get("891")));


        List<FinancialTransaction> transactionsAtNew2 =
                financialTransactionService.getAllFinancialTransactions(bedingDate2, DateUtil.getEndOfDay(bedingDate2));
        Assert.assertEquals(1, transactionsAtNew2.size());
        FinancialTransaction transactionAtNew2 = transactionsAtNew2.get(0);
        Assert.assertEquals(atNew, transactionAtNew2.getEntryBook());
        Assert.assertEquals(4, transactionAtNew2.sizeEntry());
        amountByAccount.clear();

        for (Entry entry : transactionAtNew2.getEntry()) {
            BigDecimal amountAccount = amountByAccount.get(entry.getAccount().getAccountNumber());
            if (amountAccount == null) {
                amountAccount = BigDecimal.ZERO;
            }
            if (entry.isDebit()) {
                amountAccount = amountAccount.subtract(entry.getAmount());
            } else {
                amountAccount = amountAccount.add(entry.getAmount());
            }
            amountByAccount.put(entry.getAccount().getAccountNumber(), amountAccount);
        }

        Assert.assertEquals(3, amountByAccount.size());
        Assert.assertEquals(true, amountByAccount.containsKey("501"));
        Assert.assertEquals(0, amount.compareTo(amountByAccount.get("501")));

        Assert.assertEquals(true, amountByAccount.containsKey("511"));
        Assert.assertEquals(0, amount.compareTo(amountByAccount.get("511").multiply(BigDecimal.valueOf(-1))));

        Assert.assertEquals(true, amountByAccount.containsKey("891"));
        Assert.assertEquals(0, BigDecimal.ZERO.compareTo(amountByAccount.get("891")));
    }

    @Test
    public void isRetainedEarningsTest() throws Exception {

        Account accountAchat = new AccountImpl();
        accountAchat.setAccountNumber("60111");
        accountAchat.setLabel("achat");
        accountService.createAccount(accountAchat);

        Account accountPrestation = new AccountImpl();
        accountPrestation.setAccountNumber("70611");
        accountPrestation.setLabel("prestation");
        accountService.createAccount(accountPrestation);

        FiscalPeriod period1 = new FiscalPeriodImpl();
        Date bedingDate1 = DateUtil.createDate(01, 01, 2012);
        period1.setBeginDate(bedingDate1);
        Date endDate1 = DateUtil.createDate(31, 12, 2012);
        period1.setEndDate(endDate1);

        FiscalPeriod periodSave1 = fiscalPeriodService.createFiscalPeriod(period1);

        Assert.assertEquals(false, fiscalPeriodService.isRetainedEarnings(periodSave1));

        BigDecimal amount = BigDecimal.valueOf(42.14);
        createFinancialTransaction("jdv", DateUtil.createDate(04, 04, 2012), "60111", "70611", amount);

        Assert.assertEquals(false, fiscalPeriodService.isRetainedEarnings(periodSave1));

        createFinancialTransaction("jdv", DateUtil.createDate(04, 04, 2012), "501", "511", amount);

        Assert.assertEquals(true, fiscalPeriodService.isRetainedEarnings(periodSave1));

    }

    /**
     * Permet de tester si différentes périodes sont correctes ou non
     * La création d'une nouvelle période doit remplir ce contrat.
     * On définit plusieurs périodes pour les créer dans Callao. Pour chaque
     * création, la période est testée si elle est correcte ou non.
     */
    @Test
    public void isCorrectPeriodTest() {
        /*// Période déja créée Jan 2009 - Déc 2009 (et non bloquée) (createPeriodTest)
        // debut 1 janvier 2010
        Date beginPeriod = new Date(110, 0, 1);
        // fin 1 février 2010
        Date endPeriod = new Date(110, 1, 1);
        String result;
        // Cette période est non correcte, car il n'y a pas 12 mois complets
        // Et la période précédente est non bloquée.
        result = instance.createPeriod(beginPeriod, endPeriod, false);
        Assert.assertEquals(ServiceHelper.RESPOND_ERROR, result);
        // Création période sur 12 mois, MAIS période précédente non bloquée !
        // fin 31 décembre 2010 pour avoir 12 mois complets
        endPeriod = new Date(110, 11, 31);
        result = instance.createPeriod(beginPeriod, endPeriod, false);
        Assert.assertEquals(ServiceHelper.RESPOND_ERROR, result);
        // Période précédente non bloquée et non collée au niveau des dates 
        // de début et fin
        // debut 1 avril 2009
        beginPeriod = new Date(109, 3, 1);
        // fin 31 mars 2010
        endPeriod = new Date(110, 2, 31);
        result = instance.createPeriod(beginPeriod, endPeriod, false);
        Assert.assertEquals(ServiceHelper.RESPOND_ERROR, result);
        // Création période qui chevauche la période 2009.
        // debut 1 fevrier 2008
        beginPeriod = new Date(108, 1, 1);
        // fin 1 janvier 2009
        endPeriod = new Date(109, 0, 1);
        result = instance.createPeriod(beginPeriod, endPeriod, false);
        Assert.assertEquals(ServiceHelper.RESPOND_ERROR, result);*/
    }

    /**
     * Permet de tester la recherche sur les périodes. Il suffit de donner une
     * date quelconque, il est renvoyé alors la période dont l'intervalle de
     * temps comprend cette date.
     */
    @Test
    public void searchPeriodWithDateTest() {
        /*// Période créée Jan 2009 - Déc 2009 (et non bloquée) (createPeriodTest)
        // Date recherchée 17 avril 2009
        Date dateSearch = new Date(109, 3, 17);
        Period period = instance.searchPeriodWithDate(dateSearch);
        assertTrue(period != null); // Période trouvée
        // Date recherchée 17 septembre 2000
        dateSearch = new Date(100, 8, 17);
        period = instance.searchPeriodWithDate(dateSearch);
        assertTrue(period == null); // Période non trouvée
        // Recherche la période sur la date de debut de période.
        // Date recherchée 1 janvier 2009
        dateSearch = new Date(109, 0, 1);
        period = instance.searchPeriodWithDate(dateSearch);
        assertTrue(period != null); // Période trouvée
        // Recherche la période sur la date de fin de période.
        // Date recherchée 31 décembre 2009
        dateSearch = new Date(109, 11, 31);
        period = instance.searchPeriodWithDate(dateSearch);
        assertTrue(period != null); // Période trouvée*/
    }

    /**
     * Permet de tester si les périodes peuvent être bloquées ou non
     * Test également la fonction permettant de bloquer tous les timespans
     * d'une période.
     */
    @Test
    public void blockPeriodTest() {
        /*// Période créée Jan 2009 - Déc 2009 (et non bloquée) (createPeriodTest)
        // debut 1 janvier 2009
        Date beginTimeSpan = new Date(109, 0, 1);
        String result;
        // On souhaite bloquer la période, MAIS les timeSpans ne le sont pas
        // On recherche la période Jan 2009 - Déc 2009
        Period period = instance.searchPeriodWithDate(beginTimeSpan);
        result = instance.blockPeriod(period);
        Assert.assertEquals(ServiceHelper.PERIOD_TIMESPAN_NOT_BLOCK, result);
        // On bloque tous les timeSpans et la période ensuite
        result = instance.blockAllTimeSpanOfPeriod(period);
        Assert.assertEquals(ServiceHelper.RESPOND_SUCCESS, result);
        // On bloque de nouveau la période
        result = instance.blockPeriod(period);
        Assert.assertEquals(ServiceHelper.RESPOND_SUCCESS, result);*/
    }

    /**
     * Permet de tester les objets DTO pour la période.
     * Création d'une période 2015 avec la création des 12 timeSpans
     */
    @Test
    public void PeriodDTOTest() {
        /*// Création période DTO
        Date dateBegin = new Date(110, 0, 1);
        Date dateEnd = new Date(110, 11, 31);
        PeriodDTO periodDTO = new PeriodDTO();
        periodDTO.setBeginPeriod(dateBegin);
        periodDTO.setEndPeriod(dateEnd);
        // Création BDD
        String result = instance.createPeriod(periodDTO);
        Assert.assertEquals(ServiceHelper.RESPOND_SUCCESS, result);*/
    }

    /**
     * Permet de tester la transformation d'une période en DTO avec ses timeSpans
     * associés.
     */
    @Test
    public void searchPeriodDTOTest() {
        /*// Cherche la période 2009
        Date dateBegin = new Date(109, 0, 1);
        PeriodDTO periodDTO = instance.searchPeriodDTOWithDate(dateBegin);
        Assert.assertEquals(dateBegin, periodDTO.getBeginPeriod());
        // Recherche des timeSpanDTO
        List<TimeSpanDTO> listTimeSpanDTO = periodDTO.getListTimeSpan();
        // Nombre 12 timeSpans mensuels
        Assert.assertEquals(12, listTimeSpanDTO.size());*/
    }
}
