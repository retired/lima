package org.chorem.lima.business;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.business.exceptions.AlreadyExistEntryBookException;
import org.chorem.lima.business.exceptions.BeginAfterEndFiscalPeriodException;
import org.chorem.lima.business.exceptions.ImportEbpException;
import org.chorem.lima.business.exceptions.ImportFileException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.MoreOneUnlockFiscalPeriodException;
import org.chorem.lima.business.exceptions.NotBeginNextDayOfLastFiscalPeriodException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountImpl;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.EntryBookImpl;
import org.chorem.lima.entity.FinancialStatement;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodImpl;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.IdentityImpl;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by davidcosse on 03/06/14.
 */
public class ImportExportServiceTest extends AbstractLimaTest {

    public static final String JAVA_IO_TMPDIR = System.getProperty("java.io.tmpdir")+"/";

    @Test
    public void testExportImportAccounts() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
        // make sure they are some accounts.
        List<Account> accounts = accountService.getAllAccounts();
        Assert.assertTrue(accountService.getAllAccounts().size() > 0);

        // export accounts
        ImportExportResults export = exportService.exportAccountsAsCSV(Charset.defaultCharset().name());

        InputStream stream = IOUtils.toInputStream(export.getExportResults().get(0).exportData);
        FileOutputStream res = new FileOutputStream(JAVA_IO_TMPDIR + "export-accounts.csv");
        IOUtils.copy(stream, res);

        // remove accounts
        int nbEntities = accounts.size();
        for (Account account : accounts) {
            accountService.removeAccount(account);
        }
        Assert.assertEquals(0, accountService.getAllAccounts().size());

        // import accounts
        InputStream contentStream = null;
        try {
            contentStream = new FileInputStream(JAVA_IO_TMPDIR + "export-accounts.csv");
            String inportStream = IOUtils.toString(contentStream);
            ImportExportResults result = importService.importAccountAsCSV(inportStream);

            // make sure all account have been created
            Assert.assertEquals(nbEntities, accountService.getAllAccounts().size());
            Assert.assertEquals(nbEntities, result.getImportResults().get(0).getNbCreated());
            Assert.assertTrue(result.getImportResults().get(0).getAllExceptionsByLine().isEmpty());
        } finally {
            IOUtils.closeQuietly(contentStream);
        }
    }

    @Test
    public void testExportImportAccountsException() throws Exception {
        String inportStream  = IOUtils.toString(ImportExportServiceTest.class.getResourceAsStream("/import/bcr_developed.csv"));
        ImportExportResults result = importService.importAccountAsCSV(inportStream);
        Assert.assertNotNull(result);
        Map<Integer, LimaException> exceptionMap = result.getImportResults().get(0).getAllExceptionsByLine();
        Assert.assertTrue(!exceptionMap.isEmpty());
        Collection<LimaException> exceptions = exceptionMap.values();
        Assert.assertEquals(1, exceptions.size());
        ImportFileException exception = (ImportFileException) exceptions.iterator().next();
        Assert.assertNotNull(exception);
    }

    @Test
    public void testExportImportEntryBooks() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();

        String tmpDir = System.getProperty("java.io.tmpdir")+"/";
        ImportExportResults export = exportService.exportEntryBooksAsCSV(Charset.defaultCharset().name());
        InputStream stream = IOUtils.toInputStream(export.getExportResults().get(0).exportData);
        FileOutputStream res = new FileOutputStream(tmpDir + "export-EntryBooks.csv");
        IOUtils.copy(stream, res);

        List<EntryBook> entryBooks = entryBookService.getAllEntryBooks();
        int nbEntities = entryBooks.size();
        Assert.assertEquals(3, nbEntities);

        for (EntryBook entryBook : entryBooks) {
            entryBookService.removeEntryBook(entryBook);
        }

        Assert.assertEquals(0, entryBookService.getAllEntryBooks().size());

        FileInputStream contentStream = null;
        ImportResult result;
        try {
            contentStream = new FileInputStream(tmpDir + "export-EntryBooks.csv");
            String inportStream = IOUtils.toString(contentStream);
            result = importService.importEntryBooksAsCSV(inportStream).getImportResults().get(0);
        } finally {
            IOUtils.closeQuietly(contentStream);
        }

        Assert.assertEquals(nbEntities, entryBookService.getAllEntryBooks().size());
        Assert.assertEquals(nbEntities, result.getNbCreated());
        Assert.assertTrue(result.getAllExceptionsByLine().isEmpty());
    }

    @Test
    public void testExportImportEntries() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
        initTestWithFinancialTransaction();

        List<FinancialTransaction> financialTransactions = financialTransactionService.getAllFinancialTransactions(df.parse("April 4, 2012"),df.parse("December 31, 2012"));
        List<Entry> entries = Lists.newArrayList();
        Assert.assertFalse(financialTransactions.isEmpty());
        for (FinancialTransaction financialTransaction : financialTransactions) {
            entries.addAll(financialTransaction.getEntry());
        }
        int nbEntities = entries.size();
        Assert.assertEquals(2,nbEntities);

        //test export
        String tmpDir = System.getProperty("java.io.tmpdir")+"/";
        ImportExportResults export = exportService.exportEntriesAsCSV(Charset.defaultCharset().name(), true);
        InputStream stream = IOUtils.toInputStream(export.getExportResults().get(0).getExportData());
        FileOutputStream res = new FileOutputStream(tmpDir + "export-entries.csv");
        IOUtils.copy(stream, res);

        // clear transaction
        for (FinancialTransaction financialTransaction : financialTransactions) {
            financialTransactionService.removeFinancialTransaction(financialTransaction);
        }

        // ake sure all entries have been cleared
        entries.clear();//

        financialTransactions = financialTransactionService.getAllFinancialTransactions(df.parse("April 4, 2012"),df.parse("December 31, 2012"));
        Assert.assertTrue(financialTransactions.isEmpty());

        // test import
        FileInputStream contentStream = null;
        ImportResult result;
        try {
            contentStream = new FileInputStream(tmpDir + "export-entries.csv");
            String inputStream = IOUtils.toString(contentStream);
            result = importService.importEntriesAsCSV(inputStream).getImportResults().get(0);
        } finally {
            IOUtils.closeQuietly(contentStream);
        }

        // valid import
        financialTransactions = financialTransactionService.getAllFinancialTransactions(df.parse("April 4, 2012"),df.parse("December 31, 2012"));
        Assert.assertEquals(1, financialTransactions.size());
        for (FinancialTransaction financialTransaction : financialTransactions) {
            entries.addAll(financialTransaction.getEntry());
        }

        Assert.assertEquals(nbEntities, entries.size());
        Assert.assertEquals(nbEntities, result.getNbCreated());
        Assert.assertTrue(result.getAllExceptionsByLine().isEmpty());
    }

    @Test
    public void testExportImportFiscalPeriodsAsCSV() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();

        String tmpDir = System.getProperty("java.io.tmpdir")+"/";
        String export = exportService.exportFiscalPeriodsAsCSV(Charset.defaultCharset().name()).getExportResults().get(0).getExportData();
        InputStream stream = IOUtils.toInputStream(export);
        FileOutputStream res = new FileOutputStream(tmpDir + "export-fiscal-periods.csv");
        IOUtils.copy(stream, res);

        List<FiscalPeriod> fiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();
        int nbFiscalPeriods = fiscalPeriods.size();
        Assert.assertEquals(1, nbFiscalPeriods);

        initAbstractTest();

        Assert.assertEquals(0, fiscalPeriodService.getAllFiscalPeriods().size());

        FileInputStream contentStream = null;
        ImportResult result;
        try {
            contentStream = new FileInputStream(tmpDir + "export-fiscal-periods.csv");
            String inputStream = IOUtils.toString(contentStream);
            result = importService.importFiscalPeriodsAsCSV(inputStream).getImportResults().get(0);
        } finally {
            IOUtils.closeQuietly(contentStream);
        }

        Assert.assertEquals(nbFiscalPeriods, fiscalPeriodService.getAllFiscalPeriods().size());
        Assert.assertEquals(nbFiscalPeriods, result.getNbCreated());
        Assert.assertTrue(result.getAllExceptionsByLine().isEmpty());
    }

    @Test
    public void exportImportAllAsCSVTest() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
        initTestWithFinancialTransaction();

        Identity identity = new IdentityImpl();
        identity.setName("Code Lutin");
        identity.setAddress("12 Avenue Jules Verne");
        identity.setZipCode("44230");
        identity.setCity("Saint-Sébastien-sur-Loire");
        identityService.updateIdentity(identity);

        ImportExportResults export = exportService.exportAll(Charset.defaultCharset().name());

        String tmpDir = System.getProperty("java.io.tmpdir")+"/TMP_BACKUP.zip";
        createZipFile(export, tmpDir);


        initAbstractTest();

        List<ImportResult> importResults;
        importResults = importAllFromZipFile(tmpDir);

        String[] imported = {"accounts", "entryBooks", "fiscalPeriod", "financialTransactions", "entries", "identity"};
        Assert.assertEquals(6, importResults.size());

        for (int i = 0; i < importResults.size(); i++) {
            ImportResult importResult = importResults.get(i);
            log.info(imported[i] +": created:"+importResult.getNbCreated() + " updated:" + importResult.getNbUpdated() + " ignoded:" + importResult.getNbIgnored());
            Assert.assertTrue(importResult.getNbCreated()>0);
            Assert.assertTrue(importResult.getAllExceptionsByLine().isEmpty());
        }
    }

    protected ImportExportResults createZipFile(ImportExportResults streamData, String path){

        ZipOutputStream export = null;
        FileOutputStream result = null;
        try {
            result = new FileOutputStream(path);
            export = new ZipOutputStream(result);
            for (ExportResult exportedData : streamData.getExportResults()) {
                if (exportedData != null && StringUtils.isNotBlank(exportedData.getExportData())) {
                    String data = exportedData.getExportData();
                    File file = createFile(JAVA_IO_TMPDIR + exportedData.getFromSource().getSimpleName(), Charset.defaultCharset().name(), data);
                    if (file != null) {
                        FileInputStream stream = null;
                        try {
                            ZipEntry ze = new ZipEntry(file.getName());
                            export.putNextEntry(ze);
                            int len;
                            byte[] buffer = new byte[1024];
                            stream = new FileInputStream(file);
                            while ((len = stream.read(buffer)) > 0) {
                                export.write(buffer, 0, len);
                            }
                        } finally {
                            IOUtils.closeQuietly(stream);
                            FileUtils.forceDelete(file);
                        }
                    }
                } else {
                    // export failed
                    break;
                }
            }
            export.flush();

        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            IOUtils.closeQuietly(export);
            IOUtils.closeQuietly(result);
        }
        return streamData;
    }

    protected File createFile(String filePath, String charset, String data) {
        File file = new File(filePath);
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), charset));
            out.write(data);
            out.flush();
            out.close();
        } catch (IOException eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't write file " + filePath, eee);
            }
        } finally {
            IOUtils.closeQuietly(out);
        }
        return file;
    }

    protected List<ImportResult> importAllFromZipFile(String filePath) {
        ZipInputStream zipInputStream = null;
        String tmpDir = System.getProperty("java.io.tmpdir")+"/";
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filePath);

            zipInputStream = new ZipInputStream(inputStream);

            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                byte[] buffer = new byte[2048];
                FileOutputStream fileoutputstream = null;

                if (entry.getName().equalsIgnoreCase("Account")) {
                    fileoutputstream = new FileOutputStream(tmpDir + "accounts.csv");
                } else if (entry.getName().equalsIgnoreCase("EntryBook")) {
                    fileoutputstream = new FileOutputStream(tmpDir + "entryBooks.csv");
                } else if (entry.getName().equalsIgnoreCase("FiscalPeriod")) {
                    fileoutputstream = new FileOutputStream(tmpDir + "fiscalPeriods.csv");
                } else if (entry.getName().equalsIgnoreCase("FinancialTransaction")) {
                    fileoutputstream = new FileOutputStream(tmpDir + "financialTransactions.csv");
                } else if (entry.getName().equalsIgnoreCase("Entry")) {
                    fileoutputstream = new FileOutputStream(tmpDir + "entries.csv");
                } else if (entry.getName().equalsIgnoreCase("Identity")) {
                    fileoutputstream = new FileOutputStream(tmpDir + "identity.csv");
                }
                int n;

                if (fileoutputstream != null) {
                    while ((n = zipInputStream.read(buffer, 0, 2048)) > -1) {
                        fileoutputstream.write(buffer, 0, n);
                    }
                    fileoutputstream.close();
                }

                zipInputStream.closeEntry();
            }
        } catch (Exception e) {
            throw new LimaTechnicalException("could not extract zip file", e);
        } finally {
            IOUtils.closeQuietly(zipInputStream);
            IOUtils.closeQuietly(inputStream);
        }
        InputStream transactionsStream, entryBooksStream, fiscalPeriodsStream, entriesStream, accountsStream, identityStream;
        List<ImportResult> results;
        try {
            entryBooksStream = new FileInputStream(tmpDir + "entryBooks.csv");
            String entryBooksStreamString = IOUtils.toString(entryBooksStream);
            IOUtils.closeQuietly(entryBooksStream);

            // import
            transactionsStream = new FileInputStream(tmpDir + "financialTransactions.csv");
            String transactionsStreamString = IOUtils.toString(transactionsStream);
            IOUtils.closeQuietly(transactionsStream);

            fiscalPeriodsStream = new FileInputStream(tmpDir + "fiscalPeriods.csv");
            String fiscalPeriodsStreamString = IOUtils.toString(fiscalPeriodsStream);
            IOUtils.closeQuietly(fiscalPeriodsStream);

            entriesStream = new FileInputStream(tmpDir + "entries.csv");
            String entriesStreamString = IOUtils.toString(entriesStream);
            IOUtils.closeQuietly(entriesStream);

            accountsStream = new FileInputStream(tmpDir + "accounts.csv");
            String accountsStreamString = IOUtils.toString(accountsStream);
            IOUtils.closeQuietly(accountsStream);

            identityStream = new FileInputStream(tmpDir + "identity.csv");
            String identityStreamString = IOUtils.toString(identityStream);
            IOUtils.closeQuietly(identityStream);

            results =  importService.importAll(entryBooksStreamString, transactionsStreamString, fiscalPeriodsStreamString, accountsStreamString, entriesStreamString, identityStreamString).getImportResults();

        } catch (Exception ex) {
            if(log.isInfoEnabled()) {
                log.info(ex);
            }
            throw new LimaTechnicalException("could not import files", ex);
        }
        return results;
    }


    @Test
    public void testImportFiscalStatementsAsCSV() throws Exception {
        // not ordered csv import file
        String bcr_developed = IOUtils.toString(ImportExportServiceTest.class.getResourceAsStream("/import/bcr_developed.csv"));

        ImportResult result;

        result = importService.importFinancialStatementsAsCSV(bcr_developed).getImportResults().get(0);

        Assert.assertTrue(result.getAllExceptionsByLine().isEmpty());
        Assert.assertEquals(162, result.getNbCreated());
        Assert.assertEquals(162, financialStatementService.getAllFinancialStatements().size());
        FinancialStatement actifImmobiliseStatement = financialStatementService.getFinancialStatementByLabel("ACTIF IMMOBILISÉ");
        Collection<FinancialStatement> subFinancialStatements = actifImmobiliseStatement.getSubFinancialStatements();
        Assert.assertEquals(3, subFinancialStatements.size());
        FinancialStatement bilanActifStatement = financialStatementService.getFinancialStatementByLabel("BILAN ACTIF");
        subFinancialStatements = bilanActifStatement.getSubFinancialStatements();
        Assert.assertEquals(6, subFinancialStatements.size());
    }

    //vat_shortened.csv
    @Test
    public void testImportVATStatementsAsCSV() throws Exception {
        // not ordered csv import file
        String bcr_developed = IOUtils.toString(ImportExportServiceTest.class.getResourceAsStream("/import/vat_shortened.csv"));

        ImportResult result;

        result = importService.importVATStatementsAsCSV(bcr_developed).getImportResults().get(0);

        Assert.assertTrue(result.getAllExceptionsByLine().isEmpty());
        Assert.assertEquals(55, result.getNbCreated());
        Assert.assertEquals(55, vatStatementService.getAllVatStatements().size());
    }

    @Test
    public void testImportEntriesFromEBP() throws Exception {
        // create fiscal period (mandatory for import)
        FiscalPeriod fiscalPeriod = new FiscalPeriodImpl();
        fiscalPeriod.setBeginDate(df.parse("January 1, 2010"));
        fiscalPeriod.setEndDate(df.parse("December 31, 2010"));
        fiscalPeriodService.createFiscalPeriod(fiscalPeriod);

        String [] accountNumbers = {"62610000","40104200","62510000","40100000","401TEEMP","51200000","60630000","411TECLI"};

        for (String accountNumber : accountNumbers) {
            Account account = new AccountImpl();
            account.setAccountNumber(accountNumber);
            account.setLabel("Account " + accountNumber);
            accountService.createAccount(account);
        }

        String [] _31JanuaryFTaccountNumbers = {"44566000","51210000", "40810000", "50300000", "411XAGAG", "65800000", "27180000", "27618000"};
        List<Account> _31JanuaryFTaccounts = new ArrayList<>();
        for (String accountNumber : _31JanuaryFTaccountNumbers) {
            Account account = new AccountImpl();
            account.setAccountNumber(accountNumber);
            account.setLabel("Account " + accountNumber);
            _31JanuaryFTaccounts.add(accountService.createAccount(account));
        }

        String [] entyBookCodes = {"AC", "AN", "BQ", "VE"};
        for (String entryBookCode : entyBookCodes) {
            EntryBook entryBook = entryBookService.createNewEntryBook();
            entryBook.setCode(entryBookCode);
            entryBookService.createEntryBook(entryBook);
        }

        InputStream entriesStream = ImportExportServiceTest.class.getResourceAsStream("/ebp/ecritures.txt");
        String entriesData = IOUtils.toString(entriesStream, "ISO-8859-1");
        ImportResult result = importService.importEntriesFromEbp(entriesData).getImportResults().get(0);
        Assert.assertTrue(result.getAllExceptionsByLine().isEmpty());
        Assert.assertEquals(28, result.getNbCreated());
        entriesStream.close();

         entryBookService.getEntryBookByCode("AN");
        List<FinancialTransaction> _31JanuaryFinancialTransactions = financialTransactionService.getAllFinancialTransactions(df.parse("January 31, 2010"), df.parse("January 31, 2010"));

        Assert.assertEquals(3, _31JanuaryFinancialTransactions.size());

        List<FinancialTransaction> _AN31JanuaryFinancialTransactions = new ArrayList<>();
        for (FinancialTransaction ft : _31JanuaryFinancialTransactions) {
            if (ft.getEntryBook().getCode().equals("AN")){
                _AN31JanuaryFinancialTransactions.add(ft);
            }
        }
        Assert.assertEquals(1, _AN31JanuaryFinancialTransactions.size());
        FinancialTransaction financialTransaction = _31JanuaryFinancialTransactions.get(0);
        Collection<Entry> entries = financialTransaction.getEntry();
        for (Entry entry : entries) {
            Assert.assertNotEquals(-1, _31JanuaryFTaccounts.indexOf(entry.getAccount()));
        }
    }

    @Test
    public void testImportEntryBooksFromEBP() throws IOException, AlreadyExistEntryBookException {
        InputStream entryBookStream = ImportExportServiceTest.class.getResourceAsStream("/ebp/journaux.txt");
        String entryBookData = IOUtils.toString(entryBookStream, "ISO-8859-1");
        entryBookStream.close();

        // set somme already existing entryBooks
        String [] entryBookCodes = {"AC","AN"};
        for (String entryBookCode : entryBookCodes) {
            EntryBook entryBook = new EntryBookImpl();
            entryBook.setCode(entryBookCode);
            entryBookService.createEntryBook(entryBook);
        }

        ImportResult result = importService.importEntryBookFromEbp(entryBookData).getImportResults().get(0);
        Assert.assertEquals(7, result.getNbCreated());
        Assert.assertEquals(2, result.getNbUpdated());
        EntryBook updatedEntryBook = entryBookService.getEntryBookByCode("AC");
        Assert.assertEquals("Achats de marchandises", updatedEntryBook.getLabel());
    }

    @Test
    public void testExportImportEntriesEbp() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
        initTestWithSomeFinancialTransaction();

        List<FinancialTransaction> financialTransactions = financialTransactionService.getAllFinancialTransactions(df.parse("April 4, 2012"),df.parse("December 31, 2012"));
        List<Entry> entries = Lists.newArrayList();
        Assert.assertFalse(financialTransactions.isEmpty());
        for (FinancialTransaction financialTransaction : financialTransactions) {
            entries.addAll(financialTransaction.getEntry());
        }
        Assert.assertTrue(!entries.isEmpty());
        int nbEntities = entries.size();

        //test export
        String tmpDir = System.getProperty("java.io.tmpdir")+"/";
        String export = exportService.exportEntriesAsEbp(Charset.defaultCharset().name()).getExportResults().get(0).getExportData();
        InputStream stream = IOUtils.toInputStream(export);
        FileOutputStream res = new FileOutputStream(tmpDir + "export-entries-EBP.csv");
        IOUtils.copy(stream, res);

        for (Entry entry : entries) {
            FinancialTransaction financialTransaction = entry.getFinancialTransaction();
            financialTransactionService.removeEntry(entry);
            financialTransactionService.updateFinancialTransaction(financialTransaction);
        }

        // ake sure all entries have been cleared
        entries.clear();//

        financialTransactions = financialTransactionService.getAllFinancialTransactions(df.parse("April 4, 2012"),df.parse("December 31, 2012"));
        for (FinancialTransaction financialTransaction : financialTransactions) {
            entries.addAll(financialTransaction.getEntry());
        }
        Assert.assertTrue(entries.isEmpty());

        // test import
        FileInputStream contentStream = null;
        ImportResult result;
        try {
            contentStream = new FileInputStream(tmpDir + "export-entries-EBP.csv");
            String inputStream = IOUtils.toString(contentStream);
            result = importService.importEntriesFromEbp(inputStream).getImportResults().get(0);
        } finally {
            IOUtils.closeQuietly(contentStream);
        }

        // valid import
        financialTransactions = financialTransactionService.getAllFinancialTransactions(df.parse("April 4, 2012"),df.parse("December 31, 2012"));
        Assert.assertEquals(2, financialTransactions.size());
        for (FinancialTransaction financialTransaction : financialTransactions) {
            entries.addAll(financialTransaction.getEntry());
        }

        Assert.assertEquals(nbEntities, entries.size());
        Assert.assertEquals(nbEntities, result.getNbCreated());
        Assert.assertTrue(result.getAllExceptionsByLine().isEmpty());
    }

    protected void importEBPData() throws IOException, ParseException, BeginAfterEndFiscalPeriodException,
            NotBeginNextDayOfLastFiscalPeriodException, MoreOneUnlockFiscalPeriodException, ImportEbpException {

        // create fiscal period (mandatory for import)
        FiscalPeriod fiscalPeriod = new FiscalPeriodImpl();
        fiscalPeriod.setBeginDate(df.parse("January 1, 2012"));
        fiscalPeriod.setEndDate(df.parse("December 31, 2012"));
        fiscalPeriodService.createFiscalPeriod(fiscalPeriod);

        // import files
        InputStream accountStream = null, entryBookStream = null, entriesStream = null;
        try {
            accountStream = ImportExportServiceTest.class.getResourceAsStream("/ebp/comptes.txt");
            entryBookStream = ImportExportServiceTest.class.getResourceAsStream("/ebp/journaux.txt");
            entriesStream = ImportExportServiceTest.class.getResourceAsStream("/ebp/ecritures.txt");

            ImportExportResults importExportResults = importService.importAccountFromEbp(IOUtils.toString(accountStream, "ISO-8859-1"));
            importService.importEntryBookFromEbp(IOUtils.toString(entryBookStream, "ISO-8859-1"));
            importService.importEntriesFromEbp(IOUtils.toString(entriesStream, "ISO-8859-1"));
        }
        finally {
            IOUtils.closeQuietly(accountStream);
            IOUtils.closeQuietly(entryBookStream);
            IOUtils.closeQuietly(entriesStream);
        }
    }

    /**
     * Do some test on imported accounts.
     *
     * @throws Exception
     */
    @Test
    public void testImportAccountsEBP() throws Exception {
        importEBPData();

        Assert.assertEquals(571, accountService.getAllAccounts().size());
        Assert.assertEquals("Créances", accountService.getAccountByNumber("78174000").getLabel());

        // test employe and client
        Assert.assertNotNull(accountService.getAccountByNumber("401TEEMP"));
        Assert.assertNotNull(accountService.getAccountByNumber("411TECLI"));
    }

    /**
     * Test que les comptes tiers sont correctement rattachés à l'arbre du
     * plan comptable.
     *
     * @throws Exception
     */
    @Test
    public void testImportCreateIntermediateAccount() throws Exception {
        importEBPData();

        Account compteTiers = accountService.getAccountByNumber("4");
        Account employeAccount = accountService.getAccountByNumber("401TEEMP");
        Assert.assertNotNull(compteTiers);
        Assert.assertNotNull(employeAccount);
    }

    @Test
    @Ignore
    public void getLimaTestDefaultConfFilename() {
        context.showCreateSchema();
    }
}
