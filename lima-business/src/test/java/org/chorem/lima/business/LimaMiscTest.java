/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import org.chorem.lima.business.accountingrules.DefaultAccountingRules;
import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;

/**
 * Lima misc tests.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class LimaMiscTest extends AbstractLimaTest {

    protected Properties getTestConfiguration() {
        Properties config = super.getTestConfiguration();
        config.setProperty(LimaBusinessConfig.BusinessConfigOption.RULES_NATIONALTY.getKey(), DefaultAccountingRules.class.getName());
        return config;
    }
    /**
     * Test que la regles de nationnalité par defaut est Default pour
     * la majorité des tests.
     */
    @Test
    public void testDefaultRule() {
        Assert.assertTrue(LimaBusinessConfig.getInstance().getAccountingRules()
                instanceof DefaultAccountingRules);
    }

}
