/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import org.chorem.lima.business.exceptions.AlreadyExistAccountException;
import org.chorem.lima.business.exceptions.InvalidAccountNumberException;
import org.chorem.lima.business.exceptions.LimaException;
import org.chorem.lima.business.exceptions.NotAllowedLabelException;
import org.chorem.lima.business.exceptions.NotNumberAccountNumberException;
import org.chorem.lima.business.exceptions.UnexistingAccount;
import org.chorem.lima.business.exceptions.UsedAccountException;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.AccountImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

/**
 * Tests pour la gestion des comptes dans le plan comptable.
 * 
 * Vérification sur l'ajout, la modification et suppression d'un compte.
 * Il existe également des tests sur des actions à partir de DTO. La classe
 * AccountServiceImplTest possède une méthode upload qui, à partir d'un DTO,
 * va mettre à jour automatiquement dans la base de données.
 *
 * @author Rémi Chapelet
 */
public class AccountServiceImplTest extends AbstractLimaTest {

    @Before
    public void initTest() throws Exception {
        initTestWithAccounts();
    }

    @Test
    public void getAccountCountTest() {
        Assert.assertEquals(7, accountService.getAccountCount());
    }

    @Test
    public void getMasterAccountCountTest() {
        Account master = accountService.getMasterAccount("501");
        Assert.assertNotNull(master);
        Assert.assertEquals("50", master.getAccountNumber());

        master = accountService.getMasterAccount("5");
        Assert.assertNull(master);
    }

    @Test
    public void getAccountByNumberTest() {
        Account account = accountService.getAccountByNumber("501");
        Assert.assertNotNull(account);
        Assert.assertEquals("501", account.getAccountNumber());
        Assert.assertEquals("Parts dans des entreprises liées", account.getLabel());

        account = accountService.getAccountByNumber("753");
        Assert.assertNull(account);
    }

    /**
     * Permet de tester la méthode recherchant tous les comptes.
     *
     * @throws LimaException
     */
    @Test
    public void getAllAccountTest() throws LimaException {
        List<Account> listAccount = accountService.getAllAccounts();
        Assert.assertEquals(7, listAccount.size());

        Iterator<Account> iterator = listAccount.iterator();

        Account account = iterator.next();
        Assert.assertEquals("5", account.getAccountNumber());
        Assert.assertEquals("Comptes financiers", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("50", account.getAccountNumber());
        Assert.assertEquals("Valeurs mobilières de placement", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("501", account.getAccountNumber());
        Assert.assertEquals("Parts dans des entreprises liées", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("502", account.getAccountNumber());
        Assert.assertEquals("Actions propres", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("51", account.getAccountNumber());
        Assert.assertEquals("Banques établissements financiers et assimilés", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("511", account.getAccountNumber());
        Assert.assertEquals("Valeurs à l'encaissement", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("512", account.getAccountNumber());
        Assert.assertEquals("Banques", account.getLabel());
    }

    /**
     * Find all leaf account.
     *
     * @throws LimaException
     */
    @Test
    public void getAllLeafAccountTest() throws LimaException {
        List<Account> listAccount = accountService.getAllLeafAccounts();
        Iterator<Account> iterator = listAccount.iterator();

        Account account = iterator.next();
        Assert.assertEquals("501", account.getAccountNumber());
        Assert.assertEquals("Parts dans des entreprises liées", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("502", account.getAccountNumber());
        Assert.assertEquals("Actions propres", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("511", account.getAccountNumber());
        Assert.assertEquals("Valeurs à l'encaissement", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("512", account.getAccountNumber());
        Assert.assertEquals("Banques", account.getLabel());
    }

    /**
     * Test get all subaccounts.
     *
     * @throws LimaException
     */
    @Test
    public void getAllSubAccountsTest() throws LimaException {
        Account parent = accountService.getAccountByNumber("50");
        List<Account> listAccount = accountService.getAllSubAccounts(parent);
        Assert.assertEquals(2, listAccount.size());

        Iterator<Account> iterator = listAccount.iterator();

        Account account = iterator.next();
        Assert.assertEquals("501", account.getAccountNumber());
        Assert.assertEquals("Parts dans des entreprises liées", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("502", account.getAccountNumber());
        Assert.assertEquals("Actions propres", account.getLabel());

        parent = accountService.getAccountByNumber("501");
        listAccount = accountService.getAllSubAccounts(parent);
        Assert.assertEquals(0, listAccount.size());
    }



    @Test
    public void createOrUbdateAccountTest() throws InvalidAccountNumberException, NotNumberAccountNumberException, NotAllowedLabelException {
        Account account = new AccountImpl();
        account.setAccountNumber("503");
        account.setLabel("VMP - Actions");
        boolean updated = accountService.createOrUpdateAccount(account);
        Assert.assertFalse(updated);
        Assert.assertEquals(8, accountService.getAccountCount());
        account = accountService.getAccountByNumber("503");
        Assert.assertNotNull(account);
        Assert.assertEquals("503", account.getAccountNumber());
        Assert.assertEquals("VMP - Actions", account.getLabel());

        account = new AccountImpl();
        account.setAccountNumber("502");
        account.setLabel("Actions propres MAJ");
        updated = accountService.createOrUpdateAccount(account);
        Assert.assertTrue(updated);
        Assert.assertEquals(8, accountService.getAccountCount());
        account = accountService.getAccountByNumber("502");
        Assert.assertEquals("Actions propres MAJ", account.getLabel());
    }

    /**
     * Création d'un compte dans le plan comptable.
     *
     * @throws org.chorem.lima.business.exceptions.AlreadyExistAccountException
     * @throws org.chorem.lima.business.exceptions.NotAllowedLabelException
     * @throws org.chorem.lima.business.exceptions.InvalidAccountNumberException
     * @throws org.chorem.lima.business.exceptions.NotNumberAccountNumberException
     */
    @Test
    public void createAccountTest() throws AlreadyExistAccountException, NotAllowedLabelException, InvalidAccountNumberException, NotNumberAccountNumberException {
        Account myAccount = new AccountImpl();
        myAccount.setAccountNumber("2");
        myAccount.setLabel("Comptes d'immobilisations");
        accountService.createAccount(myAccount);
    }

    /**
     * Création d'un compte dans le plan comptable (echec duplication).
     *
     * @throws org.chorem.lima.business.exceptions.AlreadyExistAccountException
     * @throws org.chorem.lima.business.exceptions.NotAllowedLabelException
     * @throws org.chorem.lima.business.exceptions.InvalidAccountNumberException
     * @throws org.chorem.lima.business.exceptions.NotNumberAccountNumberException
     */
    @Test(expected = AlreadyExistAccountException.class)
    public void createAccountFailAlreadyExistTest() throws AlreadyExistAccountException, NotAllowedLabelException, InvalidAccountNumberException, NotNumberAccountNumberException {
        Account myAccount = new AccountImpl();
        myAccount.setAccountNumber("5");
        myAccount.setLabel("Comptes de capitaux");
        accountService.createAccount(myAccount);
    }

    /**
     * Création d'un compte dans le plan comptable (echec numéro de compte non valide).
     *
     * @throws org.chorem.lima.business.exceptions.AlreadyExistAccountException
     * @throws org.chorem.lima.business.exceptions.NotAllowedLabelException
     * @throws org.chorem.lima.business.exceptions.InvalidAccountNumberException
     * @throws org.chorem.lima.business.exceptions.NotNumberAccountNumberException
     */
    @Test(expected = InvalidAccountNumberException.class)
    public void createAccountFailInvalidAccountNumberTest() throws AlreadyExistAccountException, NotAllowedLabelException, InvalidAccountNumberException, NotNumberAccountNumberException {
        Account myAccount = new AccountImpl();
        myAccount.setAccountNumber("");
        myAccount.setLabel("Comptes de capitaux");
        accountService.createAccount(myAccount);
    }

    /**
     * Création d'un compte dans le plan comptable (echec duplication).
     *
     * @throws org.chorem.lima.business.exceptions.AlreadyExistAccountException
     * @throws org.chorem.lima.business.exceptions.NotAllowedLabelException
     * @throws org.chorem.lima.business.exceptions.InvalidAccountNumberException
     * @throws org.chorem.lima.business.exceptions.NotNumberAccountNumberException
     */
    @Test(expected = NotNumberAccountNumberException.class)
    public void createAccountFailNotNumberAccountNumberTest() throws AlreadyExistAccountException, NotAllowedLabelException, InvalidAccountNumberException, NotNumberAccountNumberException {
        Account myAccount = new AccountImpl();
        myAccount.setAccountNumber("CINQ");
        myAccount.setLabel("Comptes de capitaux");
        accountService.createAccount(myAccount);
    }



    /**
     * Permet de tester l'ajout des comptes sous format DTO. La grande différence
     * entre la création classique, l'applicaion va enregistrer tous les comptes
     * enfants.
     * @throws LimaException
     */
    @Test(expected = LimaException.class)
    public void createAccountWithChildFailureTest() throws LimaException {
        // Création des comptes
        Account classFinancier = new AccountImpl();
        classFinancier.setAccountNumber("5");
        classFinancier.setLabel("Comptes financiers");
        accountService.createAccount(classFinancier);

        Account accountVmp = new AccountImpl();
        accountVmp.setAccountNumber("40");
        accountVmp.setLabel("Fournisseurs et comptes rattachés");
        accountService.createAccount(accountVmp);
    }

    @Test
    public void updateAccountTest() throws NotNumberAccountNumberException, InvalidAccountNumberException {
        Account account = new AccountImpl();
        account.setAccountNumber("502");
        account.setLabel("Actions propres MAJ");
        accountService.updateAccount(account);
        Assert.assertEquals(7, accountService.getAccountCount());
        account = accountService.getAccountByNumber("502");
        Assert.assertEquals("Actions propres MAJ", account.getLabel());
    }

    /**
     * Permet de tester si un compte est bien effacé.
     * 
     * @throws LimaException
     */
    @Test
    public void removeAccountTest() throws LimaException {
        Account accountToRemove = accountService.getAccountByNumber("50");

        // On souhaite supprimer le compte 51. Ce compte existe bien.
        accountService.removeAccount(accountToRemove);

        // Il ne doit rester que 6 compte
        List<Account> listAccount = accountService.getAllAccounts();
        Assert.assertEquals(6, listAccount.size());

        Iterator<Account> iterator = listAccount.iterator();

        Account account = iterator.next();
        Assert.assertEquals("5", account.getAccountNumber());
        Assert.assertEquals("Comptes financiers", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("501", account.getAccountNumber());
        Assert.assertEquals("Parts dans des entreprises liées", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("502", account.getAccountNumber());
        Assert.assertEquals("Actions propres", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("51", account.getAccountNumber());
        Assert.assertEquals("Banques établissements financiers et assimilés", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("511", account.getAccountNumber());
        Assert.assertEquals("Valeurs à l'encaissement", account.getLabel());

        account = iterator.next();
        Assert.assertEquals("512", account.getAccountNumber());
        Assert.assertEquals("Banques", account.getLabel());
    }

    /**
     * On souhaite supprimer le compte 422, ce dernier n'existe pas. La
     * suppression ne peut s'effectuer.
     * 
     * @throws UsedAccountException
     */
    @Test(expected = UnexistingAccount.class)
    public void removeUnexistedAccountTest() throws UsedAccountException, UnexistingAccount {
        Account account4 = new AccountImpl();
        account4.setAccountNumber("422");
        account4.setLabel("Unsaved account");
        accountService.removeAccount(account4);
    }

    /**
     * On souhaite supprimer le compte 501, ce dernier est utilisé dans une ecriture. La
     * suppression ne peut s'effectuer.
     *
     * @throws UsedAccountException
     */
    @Test(expected = UsedAccountException.class)
    public void removeUsedAccountTest() throws Exception {
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
        initTestWithFinancialTransaction();
        Account account = accountService.getAccountByNumber("511");
        accountService.removeAccount(account);
    }



    @Test
    public void testStringToListAccounts() {
        List<Account> accounts = accountService.stringToListAccounts("50..511");
        Assert.assertEquals(5, accounts.size());

        accounts = accountService.stringToListAccounts("60..99");
        Assert.assertEquals(0, accounts.size());

        context.close();
    }
}
