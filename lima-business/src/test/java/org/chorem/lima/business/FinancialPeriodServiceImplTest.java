/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business;

import org.chorem.lima.business.exceptions.NotLockedClosedPeriodicEntryBooksException;
import org.chorem.lima.business.exceptions.UnbalancedFinancialTransactionsException;
import org.chorem.lima.business.exceptions.UnfilledEntriesException;
import org.chorem.lima.entity.ClosedPeriodicEntryBook;
import org.chorem.lima.entity.Entry;
import org.chorem.lima.entity.EntryBook;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FinancialTransaction;
import org.chorem.lima.entity.FiscalPeriod;
import org.chorem.lima.entity.FiscalPeriodImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

/**
 * Tests pour la gestion des timespans
 * <p/>
 * Fonctions :
 * _ création d'un timespan
 * _ bloquer un timespan
 * _ débloquer un timespan
 * _ rechercher un timespan
 * _ rechercher une période à partir d'un timespan
 *
 * @author Rémi Chapelet
 */
public class FinancialPeriodServiceImplTest extends AbstractLimaTest {

    @Before
    public void initTest() throws Exception {
        initTestWithAccounts();
        initTestWithEntryBooks();
        initTestWithFiscalPeriod();
    }

    @Test
    public void getClosedPeriodicEntryBookTest() throws ParseException {
        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        List<FinancialPeriod> periods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, periods.get(0));

        Assert.assertNotNull(closedPeriodicEntryBook);
        Assert.assertEquals(entryBook, closedPeriodicEntryBook.getEntryBook());
        Assert.assertEquals(periods.get(0), closedPeriodicEntryBook.getFinancialPeriod());
        Assert.assertFalse(closedPeriodicEntryBook.isLocked());
    }

    @Test
    public void getAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriodTest() throws Exception {

        List<ClosedPeriodicEntryBook> closedPeriodicEntryBooks = financialPeriodService.getAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriod();

        Assert.assertEquals(36, closedPeriodicEntryBooks.size());

        // creation d'un exercice fiscal
        FiscalPeriod fiscalPeriod = new FiscalPeriodImpl();
        fiscalPeriod.setBeginDate(df.parse("January 1, 2013"));
        fiscalPeriod.setEndDate(df.parse("December 31, 2013"));
        fiscalPeriodService.createFiscalPeriod(fiscalPeriod);

        closedPeriodicEntryBooks = financialPeriodService.getAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriod();

        Assert.assertEquals(72, closedPeriodicEntryBooks.size());

        List<FiscalPeriod> fiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();

        fiscalPeriodService.blockFiscalPeriod(fiscalPeriods.get(0));

        closedPeriodicEntryBooks = financialPeriodService.getAllClosedPeriodicEntryBooksFromUnblockedFiscalPeriod();

        Assert.assertEquals(36, closedPeriodicEntryBooks.size());
    }


    /**
     * Test de la fermeture d'une periode comptable pour un journal donné.
     * 
     * @throws Exception 
     */
    @Test
    public void blockClosedPeriodicEntryBookTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        List<FinancialPeriod> periods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, periods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);

        closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, periods.get(0));

        Assert.assertTrue(closedPeriodicEntryBook.isLocked());
    }

    @Test(expected = UnbalancedFinancialTransactionsException.class)
    public void blockClosedPeriodicEntryBookFailUnbalanceTest() throws Exception {

        FinancialTransaction transaction = createFinancialTransaction("jdv", df.parse("January 4, 2012"), "511", "501", BigDecimal.valueOf(42.0));

        Entry entry = transaction.getEntry().iterator().next();
        entry.setAmount(BigDecimal.valueOf(12));

        financialTransactionService.updateEntry(entry);

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        List<FinancialPeriod> periods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, periods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);
    }

    @Test(expected = UnfilledEntriesException.class)
    public void blockClosedPeriodicEntryBookFailUnfilledEntriesTest() throws Exception {

        FinancialTransaction transaction = createFinancialTransaction("jdv", df.parse("January 4, 2012"), "511", "501", BigDecimal.valueOf(42.0));

        Entry entry = transaction.getEntry().iterator().next();
        entry.setDescription("");

        financialTransactionService.updateEntry(entry);

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        List<FinancialPeriod> periods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, periods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);
    }

    @Test(expected = Exception.class)
    public void blockClosedPeriodicEntryBookFailNoEntryBoockTest() throws Exception {

        FinancialTransaction transaction = createFinancialTransaction("jdv", df.parse("January 4, 2012"), "511", "501", BigDecimal.valueOf(42.0));

        transaction.setEntryBook(null);

        financialTransactionService.updateFinancialTransaction(transaction);

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        List<FinancialPeriod> periods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, periods.get(0));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);
    }

    @Test(expected = NotLockedClosedPeriodicEntryBooksException.class)
    public void blockClosedPeriodicEntryBookFailNoLockedPréviousTest() throws Exception {

        EntryBook entryBook = entryBookService.getEntryBookByCode("jdv");

        List<FinancialPeriod> periods = financialPeriodService.getAllFinancialPeriods();

        ClosedPeriodicEntryBook closedPeriodicEntryBook = financialPeriodService.getClosedPeriodicEntryBook(entryBook, periods.get(4));

        financialPeriodService.blockClosedPeriodicEntryBook(closedPeriodicEntryBook);
    }

    @Test
    public void getAllFinancialPeriodsTest() throws Exception {

        // creation d'un exercice fiscal
        FiscalPeriod fiscalPeriod = new FiscalPeriodImpl();
        fiscalPeriod.setBeginDate(df.parse("January 1, 2013"));
        fiscalPeriod.setEndDate(df.parse("December 31, 2013"));
        fiscalPeriodService.createFiscalPeriod(fiscalPeriod);

        List<FiscalPeriod> fiscalPeriods = fiscalPeriodService.getAllFiscalPeriods();

        fiscalPeriodService.blockFiscalPeriod(fiscalPeriods.get(0));


        List<FinancialPeriod> periods = financialPeriodService.getAllFinancialPeriods();

        Assert.assertEquals(24, periods.size());

        Iterator<FinancialPeriod> iterator = periods.iterator();

        FinancialPeriod period;
        period = iterator.next();
        Assert.assertEquals(df.parse("January 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("January 31, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("February 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("February 29, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("March 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("March 31, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("April 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("April 30, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("May 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("May 31, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("June 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("June 30, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("July 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("July 31, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("August 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("August 31, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("September 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("September 30, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("October 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("October 31, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("November 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("November 30, 2012")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("December 1, 2012"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("December 31, 2012")), period.getEndDate());

        period = iterator.next();
        Assert.assertEquals(df.parse("January 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("January 31, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("February 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("February 28, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("March 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("March 31, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("April 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("April 30, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("May 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("May 31, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("June 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("June 30, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("July 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("July 31, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("August 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("August 31, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("September 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("September 30, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("October 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("October 31, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("November 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("November 30, 2013")), period.getEndDate());
        period = iterator.next();
        Assert.assertEquals(df.parse("December 1, 2013"), period.getBeginDate());
        Assert.assertEquals(DateUtil.getEndOfDay(df.parse("December 31, 2013")), period.getEndDate());
    }


}
