/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.business.accountingrules;

import org.chorem.lima.business.AccountServiceImplTest;
import org.chorem.lima.business.LimaBusinessConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Add configuration to add specific test on French rules set.
 * 
 * (redo all test defined in AccountServiceImplTest).
 * 
 * Plus ajout de test specific à la locale FR.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class AccountServiceRuleFrTest extends AccountServiceImplTest {

    @Before
    public void installFrenchRule() throws Exception {
        LimaBusinessConfig.getInstance().setAccountingRule(FranceAccountingRules.class.getName());
    }

    /**
     * Test une fois que la regles est correctement instanciée car
     * elle peut être mise en cache dans {@link org.chorem.lima.business.LimaBusinessConfig}.
     */
    @Test
    public void testRuleInstance() {
        Assert.assertTrue(LimaBusinessConfig.getInstance().getAccountingRules() instanceof FranceAccountingRules);
    }

}
