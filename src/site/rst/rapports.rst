.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

========
Rapports
========

Les rapports peuvent être obtenus en cliquant dans le menu Ficher -> Documents...

À la suite de cette action votre navigateur ouvrira la page d'acceuil de génération des documents.

Si votre navigateur ne s'ouvre pas automatiquement vous pouvez accéder à la page à cette adresse http://localhost:5462/.
Cette page n'est consultable que si Lima est démarré.

Vous avez le choix
entre:

* **Compte;**
* **Balance;**
* **Journal;**
* **Bilan et Compte de résultat;**
* **Journal Général;**
* **Grand livre;**
* **Déclaration de TVA.**

Emplacement des rapports
========================

Les rapports s'affichent dans une fenêtre de votre navigateur par défaut mais vous pouvez aussi les retrouver dans le dossier 'reports' accéssible selon votre système à cet endroit:

Unix-like : ``$HOME/.lima/reports``

Windows 7\* et + : ``C:\Users\USER_NAME\.lima\reports``

Mac OS : ``$HOME/.lima/reports``

Page d'acceuil des rapport
==========================

.. image:: screens/lima_reports_menu_global_menu.png

Édition d'un compte
===================

Il est possible de visionner au format PDF les mouvements d'un compte sur une période (i.e. un intervalle de date).
Choisissez ensuite le compte à visualiser.
Une foix ces choix effectués et validés, les mouvements du compte sélectionnés
vont alors être générés et affichés dans votre navigateur.

.. image:: screens/lima_reports_accounts.png

Édition de la balance générale ou globale
=========================================

Il est possible de visionner au format PDF la balance sur un exercice, une
période financière ou une période (i.e. un intervalle de date).
Une foix le choix effectué et validé, la balance de la période va alors
être générée et affichée dans votre navigateur.

.. image:: screens/lima_reports_balance.png

Édition du Journal provisoir et Journal général
===============================================

Il est possible de visionner au format PDF les mouvements des
journaux sur une période (i.e. un intervalle de date).

Deux types de rapports vous sont proposés:

* Le journal provisoire, avec le détail, par journal, de chaque écriture par mois.
* Le journal général, contenant le total de chaque journal pour chaque mois.

.. image:: screens/lima_reports_entrybooks.png

Édition du Bilan et Compte de résultat
======================================

Il est possible de visionner au format HTML le Bilan et le Compte de Résultat
sur une période (i.e. un intervalle de date).
Une foix le choix effectué et validé, le Bilan et le Compte de Résultat
de la période vont alors être générés et affichés dans votre navigateur.

.. image:: screens/lima_reports_financialstatement.png

Édition du Grand livre
======================

Il est possible de visionner au format PDF le Grand livre sur une période (i.e. un intervalle de date).
Une foix le choix effectué et validé, le Grand livre  de la période va alors
être généré et affiché dans votre navigateur.

.. image:: screens/lima_reports_ledger.png

Édition de la Déclaration de TVA
================================

Il est possible de visionner au format PDF la déclaration de TVA
sur un un intervalle de date.
Par ailleurs, il est possible de remplir automatiquement ce document,
à condition d'avoir rempli le plan de TVA (Cf. 'Structure->Plan TVA')
ainsi que les données concernant votre centre des finances publiques (Menu Fichier -> Identité -> Onglet Trésor Public).
Une fois le choix effectué et validé, la déclaration de TVA
de la période va alors être générée et affichée dans votre navigateur.

.. image:: screens/lima_reports_vat.png

