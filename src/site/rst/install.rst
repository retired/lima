.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2017 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

-------------------------
Installer et lancer Lima
-------------------------

Pré-requis
----------

**Attention:** Depuis la version 0.6 de Lima nécessite `Java en version 7`_.

**Attention:** Depuis la version 0.8.6 de Lima nécessite `Java en version 8`_.

Téléchargement
--------------

Rendez-vous sur notre `page de téléchargement`_

Pour une utilisation monoposte téléchargez l'archive **lima-VERSION-bin.zip**

Pour une utilisation multi-poste en mode clients serveur téléchargez l'archive **lima-server-VERSION-bin.zip**

Installation/Lancement
----------------------

Décompressez l'archive **lima-VERSION-bin.zip** ou **lima-server-VERSION-bin.zip**

Pour lancer l'application :

- Sous Linux : via le script *lima* ou en ligne de commande *java -jar lima.jar* ;
- Sous Mac OS X : en double-cliquant sur le jar *lima.jar* ;
- Sous Windows : en double-cliquant sur le script *lima.bat*.

.. image:: screens/lima_splashscreen.png


.. _`page de téléchargement`: http://chorem.org/projects/lima/files
.. _`Java en version 7`: http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html
.. _`Java en version 8`: http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html

Paramètres des préférences de Lima
----------------------------------

De nombreux paramètres de Lima sont configurables.
La fenêtre de configuration des préférences est accécible via le menu
Fichier -> Préférences.

Parmis ses paramètres certains concernent le rendu des tableaux et sont accéssibles via l'onglet 'Tableaux'
et d'autres comme la langue de l'interface ou le séparateur de décimales sont accessibles via l'onglet 'Autre'.

.. image:: screens/lima_preferences.png

Internationalisation de lima
----------------------------

Les langues Français et Anglais avec la devise '€' sont supportées de base par Lima via la fenêtre des préférences.

Il est possible de changer la devise pour utiliser une devise autre que '€' mais pour cela il faudra éditer le fichier de configuration 'lima-swing.config'.

Exemple:

Lima est utilisé au sein d'une entreprise Canadienne qui souhaite utiliser l'interface en Aglais avec comme devise le '$',
dans le fichier de configuration 'lima-swing.config' nous rajoutons ou remplaçons la ligne commençant par ``lima.ui.locale=`` avec la valeur suivante:

``lima.ui.locale=en_CA``

Selon votre système vous trouverez ce fichier à cet emplacement (remplacez USER_NAME par le nom de l'utilisateur):

Unix-like : ``$HOME/.config/lima-swing.config``

Windows 7\* et + : ``C:\Users\USER_NAME\AppData\Roaming\lima-swing.config``

Mac OS : ``$HOME/Library/Application Support/lima-swing.config``

Vous trouverez des informations concernant l'internationalisation ici: https://en.wikipedia.org/wiki/Language_localisation

\* Sous Windows le dossier 'AppData' n'est pas visible par défaut dans l'explorateur de fichier, pour le voir il faut se rendre dans les options de l'explorateur de fichiers:
Onglet Organiser -> Options des dossiers et de recherche
Onglet Affichage -> Sous dossier 'Fichiers et dossiers cachés' -> cocher 'Afficher les fichiers, dossiers et lecteurs cachés'.

Base de données Lima
--------------------

Depuis la version 0.8.7 il est possible de définir une base de données Postgresql à la place de la base de données H2 utilisée par défaut.

**Base de données H2**

Le dossier contenant la base de données est le suivant:

Unix-like : ``$HOME/.lima/limadb.h2``

Windows 7\* et + : ``C:\Users\USER_NAME\.lima\limadb.h2``

Mac OS : ``$HOME/.lima/limadb.h2``

**Il est conseillé de sauvegarder régulièrement ce dossier.**

**Base de données Postgresql**

Si vous souhaitez utiliser une base de données Postgresql il vous faudra au préalable créer la base de données.

::

  CREATE DATABASE "lima" OWNER limaUser;

Il vous faudra ensuite indiquer à Lima d'utiliser cette base de données.
Pour ce faire éditez ou créez le fichier ``lima-business.config``:

et ajouter les lignes suivantes (en remplaçant le nom de la base de données, de l'utilisateur et son mot de passe par ceux que vous aurez défini à la création de la base de données):

::

  hibernate.connection.driver_class=org.postgresql.Driver
  hibernate.connection.password=le_mot_de_passe
  hibernate.connection.url=jdbc\:postgresql\://localhost/lima
  hibernate.connection.username=limaUser
  hibernate.dialect=org.hibernate.dialect.PostgreSQL9Dialect


Selon votre système vous trouverez ce fichier à cet emplacement (remplacez USER_NAME par le nom de l'utilisateur):

Unix-like : ``$HOME/.config/lima-business.config``

Windows 7\* et + : ``C:\Users\USER_NAME\AppData\Roaming\lima-business.config``

Mac OS : ``$HOME/Library/Application Support/lima-business.config``

