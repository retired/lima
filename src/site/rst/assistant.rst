.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=========
Assistant
=========

Au premier démarrage de Lima un assistant vous guide pour créer votre comptabilité.
Il est également possible de charger une précédente sauvegarde de Lima au format ZIP.

Les différentes étapes de l'assistant
-------------------------------------

Acceuil
~~~~~~~

Vous avez la possibilité de restaurer une sauvegarde ou continuer le processus de configuration de Lima.

.. image:: screens/lima_open_welcome.png

Fiche d'identité
~~~~~~~~~~~~~~~~

Vous pouvez renseigner les coordonnées de votre entreprise dans les champs
correspondants. Ces informations permettent de personnalisé vos documents lors
de l'impression des rapports PDF ou HTML : journaux, grand-livre, balance, ...
Ces coordonnées sont modifiables par la suite dans le menu Fichier -> Identité  

.. image:: screens/lima_open_identity.png


Plan comptable
~~~~~~~~~~~~~~

Par défaut Lima propose les trois plans comptables de la norme française :

- abrégé ;
- de base ;
- développé.

Choisissez celui qui correspond à votre activité.
Ce plan comptable est modifiable à volonté : ajout, modification, suppression de compte.
Menu Structure -> Plan comptable

Il est également possible d'importer un plan comptable au format Lima CSV, ou provenant
du logiciel EBP. Pour plus d'information sur les imports et les exports Lima, vous pouvez consulter la page `Import/Export`_.

Lors de l'import d'un plan comptable abregé, de base, ou développé. Le générateur
de bilan / compte de résultat, charge le plan de passage des compte au poste
correspondant (appelé BCR dans Lima). Pour un import externe, le plan BCR chargé
par défaut est celui de base.

.. image:: screens/lima_open_account.png

Journaux
~~~~~~~~

Par défaut les quatres journaux les plus courants sont proposés au chargement :

- Achat ;
- Vente ;
- Opérations diverses ;
- Caisse ;
- Trésorerie.

Il est possible d'importer des jouranux depuis un fichier CSV ou encore depuis un fichier d'export généré par EBP.

.. image:: screens/lima_open_entrybook.png

Exercice
~~~~~~~~

Séléctionner la date de début et de fin du nouvel exercice, une fois crée un exercice ne
peut-être supprimé ou sa date de début modifiée.

.. image:: screens/lima_open_fiscalperiod.png

..
.. _`Import/Export`: importexport.html


