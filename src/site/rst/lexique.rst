.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

==========================
Lexique de la comptabilité
==========================

Nous définirons ici les quelques notions de comptabilité d'entreprise nécessaires
à l'appréhension du fonctionnement de Lima.


Transaction
~~~~~~~~~~~

Ce que nous appelons ici *transaction* désigne une opération comptable,
c'est-à-dire l'inscription au journal, à une date donnée, d'un certain nombre
d'écritures au débit et au crédit. Pour que l'opération soit équilibrée, il faut
que la somme des écritures au crédit soit égale à celle des écritures au débit.
L'ensemble compose une transaction entre plusieurs comptes.

Une transaction peut, par exemple, représenter une opération d'achat à un
fournisseur (Crédit du compte de "Achat", débit du compte "Fournisseur"), une
opération de vente à un client, ...

Historiquement, une transaction est inscrite au Journal (ou, pour la comptabilité
multi-journaux au journal concerné : journal des ventes, journal des achats...)
et ses écritures sont reportées au Grand Livre.


Écriture
~~~~~~~~

Une écriture comptable représente tout simplement le débit ou le crédit d'un
compte. Elles sont passées par compte dans le Grand Livre, et rassemblées par
opérations dans le Journal.


Compte
~~~~~~

Un compte est la plus petite unité retenue pour le classement des flux de valeur.
Il sera débité ou crédité pour symboliser certaines opérations. Il existe par
exemple des comptes de charges (Impôts sur les sociétés, Achats, Variations de
stocks...) qui sont débités lors de l'enregistrement d'une charge, des comptes
d'actifs (Immobilisations...), qui sont débités lors de l'enregistrement d'une
acquisition d'actif, etc.

La liste des comptes est données, en France, par le Plan Comptable Général. Ce
dernier définit, pour chaque compte, un numéro d'identifiant normalisé permettant
de l'identifier. On distingue 4 grands types de comptes : d'actif, de passif, de
charge et de produit. L'augmentation d'un compte de charge ou d'actif se traduit
par un débit du compte concerné (et sa diminution par un crédit). De même,
l'augmentation d'un compte de produit ou de passif se traduit par un crédit du
compte concerné (débit pour la diminution).

Ces comptes sont généralement représentés par deux colonnes (débit et crédit),
dans lesquelles sont consignées par ordre chronologique descendant les sommes
passées respectivement au débit et au crédit du compte. Cela leur donne une forme
de T, ce qui a donné naissance à l'appellation de compte en T.


Éxercice
~~~~~~~~

Un exercice est une période, généralement de un an, pour laquelle l'entreprise
établit sa comptabilité. A l'issu de l'exercice, l'entreprise doit publier sa
comptabilité concernant cette période. Une fois cette publication effectuée,
aucun retour en arrière ne peut être effectué. C'est pourquoi on considère qu'un
exercice, une fois fini, doit être clôturé, ce qui interdit alors toutes
modifications sur les informations comptables concernant cet exercice.


Journal
~~~~~~~

Historiquement, le journal est le cahier où l'entreprise consigne par ordre
chronologique la trace de ses opérations comptables (transactions). Parfois,
celle-ci utilise plusieurs journaux (journal des achats, journal des stocks,
journal des ventes, ...) afin de séparer les différents types d'opérations et de
mieux s'y retrouver, ou de diviser la responsabilité de la consignation des
transactions entre différentes personnes, chacune responsable d'un domaine
précis (achat, vente, stock, ...). On parle alors de comptabilité multi-journaux.


Grand Livre
~~~~~~~~~~~

Historiquement, le Grand Livre est le cahier où sont consignés tous les comptes
en T de l'entreprise (tableaux de deux colonnes, débit et crédit, consignant
les diverses écritures passées sur le compte), à raison d'un par page.


Actif
~~~~~

L'actif d'une entreprise est l'ensemble de son patrimoine. Il comporte notamment
la trésorerie, les immobilisations et les créances (sommes dûes par des tiers).


Passif
~~~~~~

Le passif d'une entreprise est l'ensemble des ressources de l'entreprise, sous
la forme de capitaux et de dettes.


Bilan
~~~~~

Le bilan est un état comptable, obligatoirement publié à l'issue de l'exercice
et déduit du Grand Livre. Il présente l'état des actifs (patrimoines, ou emplois
des ressources) et passifs (ressources, sous diverses formes de dettes) de
l'entreprise à une date donnée : la date de clôture de l'exercice.

On peut également établir un bilan provisoire avant la fin de l'exercice.


Compte de résultat
~~~~~~~~~~~~~~~~~~

Le compte de résultat est un état comptable, obligatoirement publié à l'issue de
l'exercice et déduit du Grand Livre. Il oppose la liste des différentes charges
(pertes d'argent ou de valeur, que ce soit par consommation, par paiement de
taxes ou de salaires, par donations, par dépréciation d'actif...) à celle des
différents produits (gains d'argent, généralement par vente de produits finis ou
de marchandises).

Il met également en avant la différence entre les deux, qui représente le
résultat de l'entreprise pour l'exercice. Celui-ci peut être positif ou négatif,
suivant que la somme des produits dépasse celle des charges ou non. S'il est
positif, on parle alors de bénéfice qui pourra être redistribué aux actionnaires
(propriétaires) ou conservé pour financer l'activité.

