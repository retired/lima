.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2017 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

============
Présentation
============

.. image:: extras/puzzle_icon.png

Acronyme de *Lutin Invoice Monitoring and Accounting*, l'application de comptabilité *Lima* est un logiciel libre pensé
pour être le plus ergonomique possible et facile d'accès à tout utilisateur, quelque soit son niveau en comptabilité :
débutant comme confirmé.
La particularité de Lima est qu'il s'agit d'un produit évolutif permettant de répondre aux besoins spécifiques de toute
entreprise ou organisation, tout en garantissant le maintien des données comptables.

Le logiciel est écrit en Java ce qui assure une compatibilité multiplateforme :
Windows, Mac OS X, Linux. Il peut-être installé en fonctionnement client monoposte,
ou en configuration client <–> serveur.
Lors d'une installation en client serveur, le moteur de persistence de données est
installé coté serveur, et l'interface est installée sur autant de postes client que
désiré.

Historique
----------

Lima était initialiement basé sur `OFBiz Néogia`_ qui proposait un système de
comptabilité générale. 

Cependant, `OFBiz Néogia`_ n'est pas seulement un système de comptablitié, mais 
un ERP complet.
Son interface n'étant pas adaptée au besoin d'un logiciel de comptabilité indépendant,
Code Lutin a donc décidé de remplacer cette interface tout en gardant la possibilité
d'utiliser le moteur d'OFBiz Neogia. La première étape a donc été d'adapter une
interface autonome avec la technologie de bibliothèque graphique *Java Swing*.

L'interface de Lima est indépendante du moteur gérant la comptabilité. Il a donc
été décidé de développer un moteur propre à Code Lutin : Callao.

En 2010, l'application a été réécrite en majeur partie pour - d'une part - répondre le
plus fidèlement aux normes comptables, et - d'autre part - correspondre aux normes de
programmation de Code Lutin. Lima s'appuie notamment sur les projets 
`Topia`_, `EUGene`_ et `JAXX`_.

Architecture
------------

Lima est un projet `Maven`_ multi-module, il est en
réalité composé de trois projets Java :

  * lima-business
  * lima-callao
  * lima-swing

Lima-callao
~~~~~~~~~~~

Ce projet permet de définir les entités - appelées objets (données informatiques
regroupant les principales caractéristiques des éléments du monde réel) - nécessaires
au métier du logiciel de comptabilité. Toutes les classes java correspondantes
sont générées à l'aide d'un modèle `UML`_ (Unified Modeling Language). Ce modèle
est contenu dans un seul fichier au format *zargo* éditable avec le logiciel `ArgoUML`_.
Ce principe permet d'utiliser avantageusement la langue UML et ses normes. L'ensemble
du schéma est ensuite généré via les projets internes à Code Lutin :
`EUGene`_ et `Topia`_ grâce à Maven.
Outre la définition des entités, le projet *lima-callao* permet - via `Topia-persistence`_ -
de définir les méthodes associées à la base de données pour le stockage de ces
entités : ajout, modification, suppression.

lima-business
~~~~~~~~~~~~~

Ce projet permet d'implémenter/de définir le métier en utilisant des entités issues
du projet *lima-callao*. Des services EJB qui ont un rôle précis y sont disponibles
et appelés par l'utilisateur lorsqu'il appelle des
fonctions. Les services sont programmés avec un système de session afin de pouvoir
être exécutés en mode client serveur. En effet, les services sont exécutés côté
serveur et ainsi le client appelle les services en ouvrant une transaction.
Il existe une multitude de services relatifs aux besoins du métier :
service des comptes, service des journaux, service des périodes fiscales,
service des périodes financières, service des entrées, services des rapports, etc...
On peut ajouter, modifier et supprimer des services selon les besoins de l'activité.

lima-swing
~~~~~~~~~~

Il s'agit du projet qui permet d'exécuter l'UI (interface utilisateur) de Lima.
Le design de l'application y est défini : fenêtres, boutons, tableaux,...
L'interface est programmée selon la méthode de conception MVC (Modèle-Vue-
Contrôleur). Le modèle est le couple *lima-callao - lima-business*. La partie
vue-contrôleur se trouve donc dans *lima-swing*. La partie vue, ce qui s'affiche à
l'utilisateur, est programmée en `JAXX`_. Toutes les actions des éléments de la vue
(fenêtres, boutons, menus, tableaux) sont définies dans le contrôleur. Le
contrôleur est implémenté sous forme de classes Java qui s'occupent d'afficher
les bons messages, d'appeler les services correspondants, et de retourner les
données, par exemple : le remplissage d'un tableau.

Licence
-------

Lima est développé sous licence GPL :

  GNU GENERAL PUBLIC LICENSE - Version 3, 29 June 2007


.. _`OFBiz Néogia`: http://neogia.org/wiki/index.php/Accueil
.. _`Topia`: http://maven-site.nuiton.org/topia/
.. _`Topia-persistence`: http://maven-site.nuiton.org/topia/topia-persistence/index.html
.. _`EUGene`: http://maven-site.nuiton.org/eugene/
.. _`JAXX`: http://maven-site.nuiton.org/jaxx/
.. _`Maven`: http://maven.apache.org/
.. _`UML`: http://fr.wikipedia.org/wiki/Unified_Modeling_Language
.. _`ArgoUML`: http://argouml.tigris.org/


