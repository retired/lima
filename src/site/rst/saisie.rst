.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

======
Saisie
======

L'étape la plus importante de la comptabilité consiste bien entendu à saisir
les différentes écritures comptables.

Saisir des entrées comptables
-----------------------------

Pour saisir vos entrées comptables, rendez-vous sur
'Traitement->Saisir des écritures'

.. image:: screens/lima_entries.png

Choisissez alors l'exercice, la période et le journal dans lesquels vous souhaitez ajouter
des écritures. Pour ajouter une transaction, cliquez sur 'Ajouter une
transaction'.
Pour ajouter une entrée, sélectionner la transaction dans laquelle vous souhaitez
ajouter l'entrée, puis cliquez sur 'Ajouter entrée'.
Les transactions non équilibrées, ou dont le débit et crédit sont nuls, sont
affichées en rouge dans l'interface de saisie, tandis que les entrées avec
un débit et un crédit à zéro sont en rouge clair.

Assistance à la saisie :

-  Le choix du compte peut se faire de 3 façons différentes:

   1.  Sélection du compte dans la liste déroulante
   2.  Saisie du numéro de compte
   3.  Saisie du nom du compte, pour cela commancez votre saisie par le charactère '*'.

-  Vous pouvez saisir plus rapidement en utilisant la touche 'Tab' pour
   changer de cellule et tapper directement la valeur souhaitée. Si vous appuyez
   sur 'Tab en fin de ligne, soit la transaction est équilibrée alors une nouvelle
   transaction sera créée, soit la transaction n'est pas équilibrée et une nouvelle
   entrée sera ajoutée.

Les entrées et transactions non équilibrées
-------------------------------------------

Vous pouvez toutes les retrouver sur un même écran pour les
corriger en cliquant sur 'Traitement->Entrées incorrectes'.

.. image:: screens/lima_incorrectEntries.png

Choisissez votre Exercice, les entrées incorrectes s'affichent. Vous pouvez soit
supprimer les transactions, soit modifier les entrées pour équilibrer la
transaction. Le rafraichissement n'est pas automatique. 
Une fois que vous avez effectué vos modifications, vous pouvez revérifier vos 
entrées en cliquant sur le bouton de rafraichissement, en haut à gauche du tableau.


Rechercher des écritures
------------------------

Pour accéder à l'écran de recherche, cliquez sur 'Traitement->Rechercher une écriture'.

.. image:: screens/lima_searchEntry.png

Vous pouvez utiliser un ou plusieurs filtres en les combinants. Les filtres
possibles sont :

* Période (Exercice, période comptable ou dates)
* Pièce comptable (Texte de la pièce comptable, ex 'FA-001')
* Description (Texte de la description, ex 'Cartes de visite')
* Compte
* Journal
* Montant (Il est possible de choisir le montant au crédit, au débit ou les
  deux, ainsi que de faire différentes comparaisons sur les chiffres (=, <, <=,
  intervalle…)

Lettrage
--------

Pour lettrer vos entrées comptables, rendez-vous sur 'Traitement->Lettrage'

.. image:: screens/lima_lettering.png

Il est possible de n'afficher que certaines écritures, via trois types de filtres différents :

* Comptes : listes des comptes disponibles. Vous pouvez afficher toutes les écritures
  dont le numéro de compte est exactement celui sélectionner (Par exemple : 410), ou bien
  sélectionner une catégorie de compte (Par exemple : 4), permettant ainsi d'afficher
  toutes celles dont le numéro de compte débute par ce chiffre.
* Périodes : dates de début et de fin entre lesquels les écritures seront sélectionnées.
* Ecritures : vous pourrez afficher soit les écritures déjà lettrées, soit celles ne 
  l'étant pas, ou encore celles des deux catégories.

La zone 'Sélection courante' affiche le total du débit, du crédit, ainsi que la balance des
écritures sélectionnées au sein du tableau.

Il est donc possible de lettrer différentes écritures, à l'aide du bouton 'Lettrer',
mais à condition que celles-ci permettent d'avoir un solde à zéro.
Pour délettrer, il suffit de sélectionner les entrées souhaitées et de cliquer sur le
bouton 'Délettrer'.
Dans le cas où seulement quelques centimes séparent le débit et le crédit de deux entrées, il 
est possible d'utiliser le bouton 'Equilibrer', générant alors deux entrées supplémentaires.
La première correspondra à la différence (En débit ou crédit) entre les deux premières entrées,
permettant ainsi un équilibrage, et donc de pouvoir lettrer les trois écritures.
La seconde entrée créée fera soit partie du compte 658, dans le cas d'une différence sur crédit,
ou bien du compte 758, dans l'autre cas, avec un montant toujours équivalent à la différence.

