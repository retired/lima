.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2017 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

===============
Import / Export
===============

Lima supporte actuellement deux formats d'import:

- le format CSV avec un modèle de donnée spécifique à Lima ;
- le format EBP

et 3 formats d'export:

- FEC,
- CSV,
- EBP.


Format FEC (Fichier des écritures comptables)
---------------------------------------------

Le FEC est recquis par l’administration fiscale Française pour certains contrôles.
Tous les contribuables Français qui tiennent leur comptabilité au moyen de système informatisés doivent pouvoir remettre une copie du fichier des écritures comptables (FEC).


Format CSV
----------

La comptabilité de Lima peut-être sauvegardée/retaurée en intégralité via la fonction
Import -> Tout. Ou seulement le plan des comptes, le plan BCR ou les journaux.

.. image:: screens/lima_import_lima_all.png

+-------------------------------------------------------------------------------------------------------------+
|               Structure des champs correspondant aux imports et export au format CSV Lima                   |
+================+============================================================================================+
| Comptes        | accountNumber ; label ; thirdParty                                                         |
+----------------+--------------------------------------------------------------------------------------------+
| Journaux       | code ; label                                                                               |
+----------------+--------------------------------------------------------------------------------------------+
| Plan BCR       | label ; header ; accounts ; debitAccounts ; creditAccounts ;                               |
|                | provisionDeprecationAccounts ; subAmount ; headerAmount ;                                  |
|                | masterFinancialStatement ; financialStatementWay                                           |
+----------------+--------------------------------------------------------------------------------------------+
| Plan TVA       | label ; header ; accounts ; boxName ; masterVATStatement                                   |
+----------------+--------------------------------------------------------------------------------------------+
| Ecritures      | date ; entryBook ; financialTransaction ; voucher ; account ; description ; amount ; debit |
+----------------+--------------------------------------------------------------------------------------------+

Le format CSV est utilisable à partir d'un tableur, pour une compatibilité étendue
avec les différents systèmes d'exploitation, différent charset au moment de l'import
et de l'export sont supportés : UTF8, ISO Latin 1, MacRoman.

.. image:: screens/lima_import_charset.png
                   
À partir de la fonction Import -> Tout, votre fichier peut contenir que les entités
qui vous intéresse. Ainsi vous pouvez tapez au kilomètre vos écritures dans un tableur et les importer
en respectant bien le système Transaction / Entrées.


Une sauvegarde de démonstration au format CSV est `disponible au téléchargement`_. 


Format EBP
----------

La procédure est identique pour l'export des comptes ou des écritures.


.. image:: screens/export_ebp_1.png

Choissisez l'option Export Paramétrable dans EBP


.. image:: screens/export_ebp_2.png

Choisissez l'export qui vous intéresse : comptes ou écritures.


.. image:: screens/export_ebp_3.png

Entrer un emplacement et un nom au fichier d'export


.. image:: screens/export_ebp_4.png

Cliquer sur le bouton "Tout exporter", tous les champs passe du cadre de droite
à gauche


.. image:: screens/export_ebp_5.png

Choisir la première ligne devra contenir "les noms des champs". Vérifier que le
séparateur est virgule et que les champs de texte sont entourés par rien.


.. image:: screens/export_ebp_6.png

Cliquer sur le bouton "Exporter" pour confirmer.


.. image:: screens/export_ebp_7.png

L'export est effectué !


.. _`disponible au téléchargement`: http://chorem.org/projects/lima/files
