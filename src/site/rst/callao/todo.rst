.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

====
TODO
====

- suppression des DTOs et utilisation directe des entities
- suppression des convertObject qui du coup ne servent plus
- les services devrait plutot lever des exceptions au lieu de retourner des
  codes d'erreur. (donc revoir la gestion des erreurs dans lima)
- les methodes de recherche devrait plutot etre dans les DAO que sur les
  services
- certaine methode de service ne devrait pas exister (modifyAccount qui prend
  en argument les differents champs de l'entity, est a remplacer simplement
  par un store de l'entity)
- potentiellement il ne faudrait qu'un service CallaoService:
  store(Object); load(Object); delete(Object); search(Criteria); search(????);
  getReportList():List<String>; getReport(String name): html/pdf/data?
  pas de: import(); export(); car c le job de lima ce qui uniformise les formats
- a quoi sert les UserSerice (ne faut-il pas plutot utiliser celui de Topia ?)
