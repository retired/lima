.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

============
Contact list
============

Pour contacter les membres du projet ou suivre son évolution, vous pouvez
utiliser les listes de diffusion du projet :

- Liste utilisateurs : (..USERS_) Liste de discussions des utilisateurs de Callao_
- Liste développeurs : (..DEVEL_) Liste de discussions des développeurs de Callao_
- Liste des commits : (..COMMITS_) Liste des modifications du code de Callao_

=======
Support
=======

.. image:: logo/codelutin.png


Code Lutin est une société de services en logiciels libres spécialisée dans les
technologies Java/J2EE, XML, UML. Son offre s'étend à l'audit, au conseil, à la
tierce maintenance applicative et à la formation.

SARL Code Lutin
44 boulevard des Pas Enchantés
44230 Saint-Sébastien-Sur-Loire
France

Contact : Benjamin Poussin, gérant et chef de projet.

Tél : 02 40 50 29 28


Code Lutin participe activement au mouvement du logiciel libre et fait partie du
consortium ObjectWeb, d'Alliance Libre et du réseau Libre-entreprise


Plus_.

.. _Plus: http://www.codelutin.com




----------------
Libre-entreprise
----------------

.. image:: http://www.csquad.org/stages/stage-tech3/img/libre-entreprise.jpeg

Un regroupement de sociétés expertes en solutions libres

Libre-entreprise est un réseau de sociétés de services en logiciel libre. Son
développement est fondé sur celui de la communauté du libre : mutualisation des
compétences et transparence.


SiteLibreEntreprise_.

.. _SiteLibreEntreprise: http://www.libre-entreprise.com/index.php/Accueil


--------------
Alliance Libre
--------------

.. image:: http://www.solago.com/images/logo_AL.png

SiteAllianceLibre_.

.. _SiteAllianceLibre: http://www.alliance-libre.org/

.. _USERS: http://list.chorem.org/cgi-bin/mailman/listinfo/callao-users
.. _DEVEL: http://list.chorem.org/cgi-bin/mailman/listinfo/callao-devel
.. _COMMITS: http://list.chorem.org/cgi-bin/mailman/listinfo/callao-commits
.. _Callao: http://maven-site.chorem.org/callao/
