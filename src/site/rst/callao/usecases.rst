.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=====================
Les Cas d'Utilisation
=====================

| Cette section présente les cas d'utilisation du moteur Callao couplé à Lima.

``Cas d'utilisation``
=====================

``Diagramme global``
--------------------

| Afin d'optimiser la lisibilité des diagrammes, nous avons commencé par décrire les grandes groupes de fonctionnalités, avant d'entrer dans le détail des cas d'utilisation pour chacun.

.. image:: uc/uc-base.png

| On se retrouve donc avec 5 cas d'utilisation globaux :

- Gérer Ecritures : Saisie et modification des écritures comptables.
- Gérer Périodes : Paramétrage et clôturage des exercice, gestion de la semi-clôture des intervalles de temps.
- Gérer Comptes : Ajout et modification des comptes.
- Gérer Fichiers : Import et export des données.
- Générer Etats : Génération d'états comptables sous forme imprimable, ainsi que des journaux et du grand livre.
- Synchronier Chorem : Cette fonctionnalité ne correspond pas vraiment à un cas d'utilisation inhérent à Lima-Callao, mais plus à une possibilité d'accès à certaines données qui doit être prévue pour une synchronisation entre Callao et Chorem via un logiciel tiers (qui est néanmoins détaillé ici).

``Gérer Ecritures``
-------------------
.. image:: uc/uc-journal.png

| Le diagramme parle de lui-même. Il faudra être vigilent à ne pas pouvoir ajouter d'écriture dans un exercice non encore ouvert ou déjà verrouillé. Il en va de même pour les modification et les suppression.



| **Cas d'utilisation** : Ajouter Écriture étend : Ajouter Transaction; Gérer Ecritures
| Acteur principal : Comptable
| Invariants :

- Une écriture (Entry) appartient à une transaction.
- Toute opération (par extension tout ajout) sur une écriture (Entry) est répertoriée dans un Log.
- On ne peut ajouter d'entrée à un exercice (Period) ou un intervalle de temps (TimeSpan) clôturé ou inexistant.

| Scénario
| Cas 1 : Ajout d'une écriture dans une transaction non existante pour un intervalle de temps (TimeSpan) non clôturé :
| Préconditions :
 
- L'entrée à créer n'appartient pas à une transaction déjà existante.
- La date entrée pour cette transaction correspond à l'intervalle de temps en cours ou à un intervalle non clôturé.

| Résultat :

- Une nouvelle entrée est créée et répertoriée dans une nouvelle transaction, elle-même répertoriée dans un journal donné et dans l'intervalle de temps correspondant à la date donnée pour la transaction.
- Cet ajout d'entrée est consigné dans un nouveau Log.

| Description :

- Une nouvelle transaction est créée, avec pour attributs les valeurs de la date et de la référence justificatif entrée dans l'interface.
- Elle est ajoutée au journal depuis lequel l'utilisateur l'a créée (menu déroulant de l'interface), ainsi que dans l'intervalle de temps correspondant à la date donnée.
- L'entrée est créée avec les données saisies (description, montant, débit, lettrage, compte) et ajoutée à la nouvelle transaction. Un Log est également créé, consignant la date courante (date de modif), la date donnée pour la transaction, le type d'opération (en l'occurrence, ajout) et les attributs de l'écriture et de la transaction.

| Cas 2 : Ajout d'une écriture dans une transaction existante pour un intervalle de temps (TimeSpan) non clôturé :
| Préconditions :

- L'entrée à créer appartient à une transaction déjà existante.
- La date entrée pour cette transaction correspond à l'intervalle de temps en cours ou à un intervalle non clôturé.

| Résultat :
 
- Une nouvelle entrée est créée dans une transaction existante. Cet ajout d'entrée est consigné dans un nouveau Log.

| Description :

- L'entrée est créée avec les données saisies (description, montant, débit, lettrage, compte) et ajoutée à la transaction donnée.
- Un Log est également créé, consignant la date courante (date de modif), la date donnée pour la transaction, le type d'opération (en l'occurrence, ajout) et les attributs de l'écriture.

| Exceptions
| Exception1 : Ajout d'une écriture pour un exercice à venir n'existant pas encore.
| Préconditions :

- La date de la transaction correspondant à l'entrée ne correspond à aucun exercice existant.

| Résultat : Rien.
| Description :

- Un message est affiché pour indiquer à l'utilisateur la démarche à suivre pour créer un nouvel exercice.

| Exception 2 : Ajout d'une écriture pour un exercice (Period) ou un intervalle de temps (TimeSpan) clôturé.
| Préconditions :

- La date de la transaction correspondant à l'entrée appartient à un exercice ou un IT clôturé.

| Résultat : Rien.
| Description : 

- Un message d'erreur est affiché, indiquant à l'utilisateur qu'il ne peut effectuer l'opération d'ajout.

| **Cas d'utilisation** : Modifier Ecriture étend : Gérer Ecritures
| Acteur principal : Comptable
| Invariants :

- Une écriture (Entry) ne migre pas d'une transaction à l'autre.
- Toute opération (par extension toute modification sur une écriture (Entry) est répertoriée dans un Log.
- On ne peut migrer une entrée vers un exercice (Period) ou un intervalle de temps (TimeSpan) clôturé ou inexistant.

| Scénario :
| Cas 1 : Modification d'une écriture pour un intervalle de temps (TimeSpan) non clôturé :
| Pré-conditions :

- L'entrée à créer n'appartient pas à une transaction déjà existante.
- La date entrée pour cette transaction correspond à l'intervalle de temps en cours ou à un intervalle non clôturé.

| Résultat :

- Une nouvelle entrée est créée et répertoriée dans une nouvelle transaction, elle-même répertoriée dans un journal donné et dans l'intervalle de temps correspondant à la date donnée pour la transaction.
- Cet ajout d'entrée est consigné dans un nouveau Log.

| Description :

- L'entrée est modifiée avec les données nouvellement saisies (description, montant, débit, lettrage, compte).
- Un Log est également créé, consignant la date courante (date de modif), la date donnée pour la transaction, le type d'opération (en l'occurrence, modification) et les attributs de l'écriture et de la transaction.

| Exceptions
| Exception1 : Modification d'une écriture pour un intervalle de temps clôturé.
| Pré-conditions :

- La date de la transaction correspondant à l'entrée correspond à un intervalle de temps semi-clôturé, voire à un exercice clôturé.

| Résultat : Rien.
| Description :

- Un message d'erreur est affiché, indiquant à l'utilisateur qu'il ne peut effectuer l'opération de modification.

| La suppression d'écriture se comporte de manière analogue à la modification, c'est pourquoi nous ne la détaillerons pas ici. Notons juste que si la dernière écriture restant dans une transaction est supprimée, celle-ci sera également supprimée.

| 
| **Cas d'utilisation** : Modifier Transaction étend : Gérer Transactions
| Acteur principal : Comptable
| Invariants :

- Aucune transaction ne peut être déplacée dans une période clôturée ou un intervalle de temps semi-clôturé, ni dans un exercice n'existant pas.

| scénario
| Cas 1 : Modification du voucherRef ou de la date vers un intervalle de temps existant et non-clôturé :
| Pré-conditions :

- La transaction n'appartient pas à un intervalle de temps clôturé.
- La date n'est pas modifiée pour une date concernant un intervalle de temps non existant ou clôturé.

| Résultat :

- La transaction est modifiée.
- Un nouveau log est créé pour chacune des Entry de la Transaction.

| Description :

- La transaction est modifiée avec les données nouvellement saisies (description, montant, débit, lettrage, compte).
- Pour chacune des écritures de la transaction, un Log est également créé, consignant la date courante (date de modif), la date donnée pour la transaction, le type d'opération (en l'occurrence, modification) et les attributs de l'écriture et de la transaction.

| Exceptions
| Exception1 : Modification d'une transaction dans un intervalle de temps clôturé.
| Pré-conditions :

- La date de la transaction correspond à un intervalle de temps semi-clôturé, voire à un exercice clôturé.

| Résultat : Rien.
| Description :

- Un message d'erreur est affiché, indiquant à l'utilisateur qu'il ne peut effectuer l'opération de modification.

| Exception2 : Modification de la date de la transaction vers un intervalle de temps clôturé.
| Pré-conditions :

- La nouvelle date donnée pour la transaction correspond à un intervalle de temps semi-clôturé, voire à un exercice clôturé.

| Résultat : Rien.
| Description :

- Un message d'erreur est affiché, indiquant à l'utilisateur qu'il ne peut effectuer l'opération de modification.


``Gérer Périodes``
------------------
.. image:: uc/uc-exercice.png

| Lors de l'ouverture d'un exercice, il est automatiquement divisé en TimeSpan (qui représenteront des mois en pratique). Ces TimeSpan sont verrouillables, de manière à accéder à un état proche de la clôture, mais où il reste possible de les déverrouiller, ce afin d'éviter d'éventuelles erreurs de saisie. La clôture d'un exercice entraîne la fermeture (définitive cette fois) de tous les TimeSpan qui le composent. A noter également qu'un TimeSpan ne peut être ouvert qu'à la condition que tous les TS posterieurs le soient aussi (et consecutivement un TS ne peut être semi-cloturé que si les TS anterieurs le sont aussi.
| 
| **Cas d'utilisation** : UC Ouvrir Nouvel Exercice étend l'UC « cloture de periode »
| Acteur principal : Comptable.
| Invariant

- Il ne peut jamais y avoir plus de  deux exercices non clôturer en même temps.

| Scénario
| Cas 1: Création d'un exercice(Period). 
| Pré-condition : 

- L'exercice ne doit pas être déjà créer.

| Description :

- La création d'un nouvel exercice à besoin de connaitre la durée de ce nouvel exercice.
- Par défaut cette période est de 12 mois.
- Pour chaque mois, on crée un intervalle de temps.
- On réalise les différents report à nouveau pour équilibrer l'exercice précèdent et commencer avec les bons chiffre l'exercice nouvellement créer.

| Post-condition :

- L'intégralité de l'exercice est couvert par des intervalles de temps. 
- La date de début d'exercice et la date de fin de l'exercice précédent se suivent.

| Cas d'utilisation : UC Clôturer Exercice étend l'UC « cloture de periode »
| Acteur principal : Comptable.
| Scénario
| Cas 1: Clôture d'un exercice(Period). 
| Pré-condition : 

- L'exercice ne doit pas être déjà clôturé.
- La date de fin d'exercice doit être atteinte pour pouvoir clôturer l'exercice.

| Description :

- La clôture de l'exercice entraine la clôture définitive de l'ensemble des intervalles de temps qui le compose.
- Les opérations de l'exercice ne sont plus éditables. 
- Il est alors possible de publier les documents comptables (ex: bilan, compte de résultat).
- Les reports à nouveau sont calculés et passés sur l'exercice suivant.
- Un backup de l'exercice est alors généré.

| Post-condition :

- L'ensemble des écritures n'est modifiable depuis Callao.


| **Cas d'utilisation** : UC Semi-clôturer Intervalle de Temps (Cet UC est étendu par l'UC « création de période »)
| Acteur principal : Comptable.
| Invariant 

- Toutes les périodes d'un exercice clôturé sont clôturé.
- Un intervalle de temps dure 1 mois

| Scénario

- Cas 1: Clôture d'un intervalle de temps(TimeSpan).

| Pré-condition :

- L'intervalle de temps ne doit pas être déjà clôturé.
- La date de fin de période doit être supérieure à la date de début de la période à clôturer.

| Description :

- Lorsque l'utilisateur clôture l'intervalle, l'ensemble des écritures du mois sont bloquées.
- S'ils ne le sont pas déjà, l'ensemble des mois précédents sont clôturés. 
- L'utilisateur a toujours la possibilité de débloquer cet intervalle tant que
  l'exercice n'est pas clôturé lui même.

| Post-condition :

- L'ensemble des écritures n'est plus modifiable depuis Callao.
- Les périodes précédents cet intervalle sont clôturés.

| Cas 2: Dé-clôturer un intervalle de temps.
| Pré-condition : 

- L'intervalle de temps doit être clôturer.
- L'exercice dont il fait parti ne doit pas être clôturer.

| Description :

- L'intervalle de temps redevient modifiable par l'utilisateur. 
- Le comptable peut alors ajouter, modifier ou supprimer des transactions sur cet intervalle.
- Tout les intervalles de temps qui le suivent sont dé-clôturés, si ils sont déjà clôturés.
- L'intervalle est de nouveau clôturable.

| Post-Condition :

- Les écritures sont de nouveaux modifiables, les périodes suivantes sont dé-clôturés.


``Gérer Comptes``
-----------------
.. image:: uc/uc-comptes.png

| La seule petite particularité de ces cas d'utilisation est qu'on ne pourra pas modifier le numéro ou supprimer un compte sur lequel des écritures ont été passées.

``Gérer Fichiers``
------------------
.. image:: uc/uc-fichiers.png

Génère des fichiers pour sauvegarder ou échanger les données.

| **Cas d'utilisation** : UC Exporter vers Fichiers
| Acteur principal : Comptable.
| Scénario
| Cas 1: Export d'un exercice comptable.
| Description :

- Récupération de l'ensemble des données.
- Un fichier est crée avec l'ensemble des paramètres de l'application. Il comprend : 
- les données relatives à l'entreprise (siret, raison sociale...)
- les comptes de l'entreprise
- les journaux 
- Les exercices de l'entreprise.(Intervalle de temps, transactions, écritures...)

| Post-condition :

- Un fichier XML valide est généré.

| **Cas d'utilisation** : UC Importer depuis Fichiers
| Acteur principal : Comptable.
| Scénario
| Cas 1: Import de données
| Description :

- L'utilisateur spécifie le chemin du fichier à importer.
- Le fichier est parsé, on supprime l'ensemble des données de la base 
- On enregistre dans la base de données les informations récupérer du fichier.

| Cas exceptionnels
| Cas Exceptionnel 1 :	Le fichier spécifier n'est pas valide ou n'existe pas.
| Description : 

- Un message d'erreur est affiché. 
- La base de données n'est pas changer.


``Générer Etats``
-----------------
.. image:: uc/uc-etats.png

| On aura une génération d'états provisoires pour les périodes non clôturées, et définitif pour les périodes clôturées. On prévoit pour l'instant une génération en PDF. Cette génération pourra être effectuée à n'importe quel moment.

| **Cas d'utilisation** : UC Générer état
| Acteur principal : Comptable.
| Invariants

- Un état concerne un seul exercice.
- Un état sera prévisionnel tant que l'exercice concerné ne sera pas clôturé.

| Scénario
| Cas 1: Génération d'un état(Bilan, compte de résultat...). 
| Pré-condition : 

- Il faut que l'exercice existe pour pouvoir générer un état.

| Description :

- On solde les comptes concernés ( ex :produit et charge pour compte de résultat... ).
- On les affectes aux champs de l'état comptable.
- On récupère les informations sur la société ( numéro siren, raison sociale...)
- On exporte dans un fichier au format pdf dont le chemin est prédéfini, l'état concerné.
- Si l'état n'est pas clôturé, le fichier indique que c'est un état prévisionnel.

| Post-condition :

- Un fichier pdf est créé.


``Synchronier Chorem``
----------------------
.. image:: uc/uc-liaison.png

| Cette liaison permet de récupérer des informations de facturation depuis Chorem afin de les passer en écriture dans Lima/Callao, de fait cette "couche de liaison" disposera d'informations persistantes de manière à associer les clients de Chorem à des comptes (client tartenpion = 4111tartenpion etc ...) et de répercuter les domaines tout en sachant aussi éviter les redondances et faciliter les corrections. Une fois les informations éxtraite de Chorem et les données de liaison mises à jour, ces données pourront être poussées dans Lima/Callao.
