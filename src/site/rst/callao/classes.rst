.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

====================
Diagramme de classes
====================

.. image:: class/class-diag.png

``Entry``
=========
``Description :``
-----------------
Une Entry est une écriture comptable, qui traduit le passage d'une somme au débit
ou au crédit d'un compte.

``Attributs :``
---------------

- description : Description textuelle de l'opération.

- amount : Montant de l'opération, stocké sous forme de string.

- debit : Booléen indiquant s'il s'agit d'une opération de type débit ou crédit.

- lettering : Chaîne de caractères employée pour le lettrage

- detail : Chaîne de caractères servant au stockage d'informations caractérisant
  l'écriture en question au format CSV.

- Transaction : Une Entry fait forcément partie d'une Transaction (soit d'une
  Transaction existante, soit une nouvelle Transaction est créée lors de la
  création


``Liens :``
-----------

- Log : Toute opération sur une Entry sera consignée dans un objet Log(RM).

- Account : Une Entry est une opération de débit ou de crédit sur un compte donné.
  Chacune d'elle concerne donc un et un seul compte.



``Transaction``
===============
``Description :``
-----------------
Une Transaction représente une opération comptable, soit un ensemble d'écritures
(Entry). Une Transaction n'est dite équilibrée que si la somme des montants des
écritures de débit qui la composent est égale à celle des écritures de crédit(RM).
Elle fera souvent référence à un document justificatif (par exemple une facture)
qu'elle représentera comptablement. Elle est passée à une date donnée qui concerne
par extension toutes les écritures qu'elle contient.

``Attributs :``
---------------

- entryDate : La date d'entrée de l'opération (saisie par l'utilisateur).

- voucherRef : Si utile, référence du document justificatif.

- description : Description textuelle.


``Liens :``
-----------

- Entry : Une Transaction est une composition d'un certain nombre d'écritures
  (Entry).

- TimeSpan : Une transaction intervient pendant un intervalle de temps. La clôture
  de cet intervalle de temps (TimeSpan) implique que la Transaction n'est pas
  modifiable (règles métier de l'application(RM)).

- Journal : Une Transaction  appartient à un journal donné, selon là où elle a
  été entrée par l'utilisateur.



``TimeSpan``
============
``Description :``
-----------------
Un TimeSpan, ou intervalle de temps, représente une subdivision de l'exercice
comptable (Period) en plusieurs périodes plus petites afin de pouvoir clôturer
son travail au fur et à mesure. La loi impose juste que les exercices soient
clôturés de manière définitive, donc les clôtures de TimeSpan seront
réversibles(RM). Un intervalle de temps ne pourra être verrouillé que si les
précédents intervalles sont également verrouillés(RM). L'idée de cette
fonctionnalité nous a été soumise par notre professeur de comptabilité. Nous
avions commencé par concevoir des intervalles de temps de durée quelconque mis
en place par l'utilisateur avant de s'orienter vers des TimeSpan d'une durée
d'un mois, également sur ses conseils. Les différents TimeSpan (autant que de
mois pour l'exercice) seront générés lors de la création de l'exercice(RM).

``Attributs :``
---------------

- beginTimeSpan : Date de début de l'intervalle de temps.

- endTimeSpan : Date de fin de l'intervalle de temps.

- locked : Booléen indiquant si l'IT est clôturé ou non.


``Liens :``
-----------

- Period : Un intervalle de temps appartient à un exercice (Period donné). En
  fait, il constitue un mois de cet exercice. Les dates de début et de fin de l'IT
  sont donc évidemment comprises dans celles de l'exercice auquel il appartient.

- Transaction : Un ensemble de transactions est effectué pendant un intervalle
  de temps donné, déterminé par la date de transaction. Un intervalle de temps ne
  peut être clôturé que si toutes ses transactions sont équilibrées(RM).



``Period``
==========
``Description :``
-----------------
Il s'agit d'un exercice comptable. Ses dates seront définies par l'utilisateur
dans la limite de certaines règles métier qui permettront de respecter le cadre
légal : un exercice commence là où le précédent se finit; la durée de l'exercice
est fixée à l'avance et ne peut être modifiée; la clôture d'un exercice est
définitive(RM). Il ne peut y avoir plus de deux exercices ouverts en même temps(RM)
et aucun exercice ne peut être clôturé sans que le précédent ait été clôturé(RM).
Lors de la clôture d'un exercice, une opération de report à nouveau est effectuée
sur l'exercice suivant(RM). De fait, il est impossible de clôturer un exercice
sans avoir préalablement créé le suivant(RM).

``Attributs :``
---------------

- beginTimeSpan : Date de début de l'exercice.

- endTimeSpan : Date de fin de l'exercice.

- locked : Booléen indiquant si l'exercice est clôturé ou non.


``Liens :``
-----------

- TimeSpan : Un exercice contient, selon sa longueur, un certain nombre
  d'intervalles de temps qui correspondent à ses différents mois. Les différents
  TimeSpan sont créés en même temps que l'exercice(RM). La clôture de l'exercice
  implique automatiquement la clôture de tous ses TimeSpan(RM).



``Journal``
===========
``Description :``
-----------------
Le logiciel supporte la « comptabilité multi-journal », c'est-à-dire que
l'utilisateur peut saisir ses opérations dans plusieurs journaux différents
(Journal des ventes, journal des achats...).

``Attributs :``
---------------

- label : Libellé du journal.

- prefix : Prefix du journal, utilisé par l'interface.


``Liens :``
-----------

- Transaction : Un journal est composé de transactions, mais peut être vide.



``Log``
=======
``Description :``
-----------------
Chaque ajout/modification/suppression sur une Entry entraine la création d'un
log contenant toutes les nouvelles informations de celui-ci (qui seront vides
s'il s'agit d'une suppression(RM)). La modification d'une Transaction entraine
aussi la création d'un log pour chacune de ses Entry(RM). Dans la mesure où les
logs devraient pouvoir être consultés sans impliquer de traitement, on y réunira
les informations consultant les Entry et les Transaction.

``Attributs :``
---------------

- logDate : Date où le log est créé (déterminée à partir de l'horloge système)

- type : Type de modification. Peut prendre 4 valeurs différentes : add (ajout),
  mod (modification), del (suppression), tmod (modification de transaction).

- transDate : Date de la Transaction.

- voucherRef : Référence du justificatif de la Transaction.

- transDesc : Description de la Transaction.

- entryDesc : Description de l'Entry.

- amount : Montant de l'Entry.

- debit : Booléen indiquant s'il s'agit d'une écriture de débit ou de crédit.

- lettering : Letterage de l'Entry.


``Liens :``
-----------

- Entry : Le log correspond à une Entry qui, elle, peut avoir plusieurs logs (au
  moins un et autant qu'elle a subi de modifications).



``Account``
===========
``Description :``
-----------------
Correspond à un compte du PCG. Les sous-comptes (e.g. 4111) sont considérés
comme étant agrégés dans leur « compte père » (e.g. 411). Un compte ne peut être
supprimé s'il contient des écritures(RM).

``Attributs :``
---------------

- label : Libellé du compte.

- accountNumber : Numéro de compte.

- Entry : Les Entry représentent les différentes opérations effectuées sur les
  comptes. Plusieurs écritures peuvent être passées concernant un compte donné,
  mais celui-ci peut exister sans écriture.


``Liens :``
-----------

- Account : Un compte peut contenir des sous-comptes.


``CallaoUser``
==============
``Description :``
-----------------
Bien que l'interface prévoit une phase d'authentification à son lancement, il
n'est pour l'instant pas prévu d'implémenter une gestion multi-utilisateurs de
Callao. Nous ajoutons néanmoins cette classe afin de pouvoir prévoir cette
gestion dans une implémentation future.

``Attributs :``
---------------

- userID : ID de l'utilisateur.

- login : Login de l'utilisateur.

- password : Mot de passe de l'utilisateur

- right : Code le groupe de droits auquel l'utilisateur a accès



``User``
========
``Description :``
-----------------
Cette classe va permettre de typer des écritures vis à vis des utilisateurs
relatifs qu'elle touche. Ceci permettre d'effectuer des tris d'écriture afin
d'extraire le coût ou produit généré par des individus.
Cette démarche s'inscrit dans l'optique d'une synchronisation des données entre
Chorem et Callao.

``Attributs :``
---------------

- matcher : Le nom d'utilisateur



``Type Presta``
===============
``Description :``
-----------------
Correspond aux propriétés d'une écriture. Permet de définir et classifier les
écritures grâce aux catégories pertinentes afin de mieux analyser les résultats
des différentes activités de l'entreprise. 

``Attributs :``
---------------

- matcher : Description du type de prestation associée à une écriture



``Client``
==========
``Description :``
-----------------
Les catégories de clients sont des informations venant éventuellement se greffer
sur des comptes afin de mieux renseigner les informations sur ces derniers, de
pouvoir éventuellement les regrouper et déduire des généralisations par catégories
de client, anticiper des durées de stockages de créances pour un client lambda
fonction de sa catégorie.
Ces catégories seront cependant le plus souvent associées aux comptes descendants
du compte 411 (client) et assez rarement (voir pas du tout) d'autres comptes
sauf exceptions sur les comptes 416 ou autres cas exceptionnels.
Ce système reste évolutif et permet à un comptable d'associer des catégorie à
n'importe quel compte dans la mesure où il le jugerait utile.

``Attributs :``
---------------

- matcher : Le nom de la catégorie du client.



``Project``
===========
``Description :``
-----------------
Les projets sont un moyen de regrouper des écritures afin de réaliser des
recherches et des calculs de coûts associés à un projet précis. Ainsi un
comptable pourra réaliser une compta-analytique et connaître la rentabilité de
ses différents contrats.
A long terme ces informations relatives à un projet proviendront de Chorem ou
d'autre solutions purement de gestion de projet, ces informations seront importé
afin d'en faciliter l'analyse comptable.

``Attributs :``
---------------

- matcher : Le nom du projet.

- uri : Une adresse vers laquelle sur laquelle pointera le projet Chorem relatif
  à l'écriture ciblée.



``Tax``
=======
``Description :``
-----------------
Permettra d'assigner un type d'impot à une écriture comptable (TVA, IS, Banque,
etc.).

``Attributs :``
---------------

- matcher : Un type de taxe.



``Particularité des classes : User, TypePresta, Client, Project et Tax``
========================================================================
``Explication :``
-----------------
Ces classes s'inscrivent dans une démarche de synchronisation des données entre
Callao et Chorem dans l'objectif de pouvoir catégoriser des données en fonctions
de ces cinq critères, fournis par Chorem.
Ces données seront en fait stockées dans l'attribut detail d'une instance d'Entry
afin dans une structure de données formatée CSV.
Par exemple : "user:dupont;user:martin;typePresta:dev;client:LM;projet:lima"
