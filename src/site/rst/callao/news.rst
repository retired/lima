.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

====
News
====

Quoi de neuf sur le projet Callao ?

Mise en Ligne du site
=====================

``Par :`` Jean ``[le Jeudi 24 septembre 2009]``

Le site du projet est enfin en ligne, il faisait précédemment partie du site de
Lima, il est maintenant autonome et s'étoffera au fur et à mesure de l'avancée
du projet.
