.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

==================
Callao
==================

Issu du projet Chorem_, et créé pour fonctionner en collaboration avec Lima_,
Callao_ est *un moteur de comptabilité générale* respectant les conventions
ainsi que normes imposées par la législation française aux entreprises.

Ce rôle était jusqu'à alors remplis via le moteur de comptabilité d'OfBiz-Neogia,
Callao_ doit pouvoir se substituer à ce moteur mais sans refondre Lima_ afin de
laisser aux utilisateurs de Lima_ le choix du moteur de comptabilité qu'ils
utilisent ou de faciliter les migrations d'un moteur à l'autre [#]_.

  
----

.. [#] A cet effet des fonctions d'import et d'export seront mises en place.
.. _ChoreM: http://chorem.labs.libre-entreprise.org
.. _Lima: http://maven-site.chorem.org/lima
.. _Callao: http://maven-site.chorem.org/callao
