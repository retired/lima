.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=============
Développement
=============

Nous utiliserons cette section pour fournir des informations concernant
l'avancement du développement du moteur Callao.


-----------------------
La génération via ToPIA
-----------------------

A l'heure actuelle, la génération de code concerne uniquement la couche de
données via l'utilisation de ToPIA-persistence_ avec xmi pour la définition du
modèle, H2 pour la base de données, Hibernate pour la couche d'accès aux données,
et Java pour l'implémentation.
La construction et la génération du code du projet s'effectue directement via
Maven à l'aide de la commande de compilation :

``mvn clean compile``

L'ensemble des paramètres commandant cette génération sont définis dans les
fichiers de configuration de Maven du super pom et du pom du module
''callao-entity''. Le modèle quant à lui est stocké dans un fichier ''zargo'',
toujours dans le module ''callao-entity'' et peut être consulté à l'aide
d' ArgoUML_ .
Lima propose un certain nombre de services disponibles via des interfaces, le but
initial de Callao et de pouvoir implémenter ces services de manière locale ou
distante. L'utilisation de Web Services, comme mis en place pour effectuer la
communication entre Lima et OFBiz constitue la solution à cette problématique.



Néanmoins, pour le moment, les implémentation s'effectuent uniquement de manière
locale. Callao s'occupe uniquement d'implémenter les interfaces de Lima situées
dans le module ''lima-service'' de manière locale, et les échanges de Web Services
n'ont pas encore été mis en place.



Cependant, une configuration de la génération de cette couche de communication
par services a tout de même été effectuée via l'utilisation de ToPIA-soa_ . A ce
titre, un modèle des services est présent en parallèle du modèle entité dans le
module ''callao-entity''. Ce modèle décrit l'ensemble des services fournissant
l'application des règles métiers définies pour l'application Callao. Couplé à
ToPIA-soa_ , il permet la génération de cette couche et de ses interfaces associées.
Cependant une problèmatique de connection aux services de Lima reste encore non
résolue.



De même que pour la partie entity, la génération de la couche service est
commandée par Maven via la configuration du fichier pom du module
''callao-service''.



-------------------------------
L'implémentation des interfaces
-------------------------------

Les implémentations des interfaces services de Lima dans Callao représentent les
fragments de code source de l'application qui vont définir les règles métiers.
L'ensemble de ce code source se situent dans le module ''callao-service'' et
s'occupe à l'heure actuelle d'implémenter uniquement localement les interfaces
services de Lima.



L'état d'avancement actuel de ces implémentations est inachevé. En effet, il
reste encore des améliorations à fournir à celles effectuées et à compléter
celles des classes ''Period'' et ''Journal'' pour pouvoir au moins répondre aux
interfaces services existantes de Lima. De plus, notre modèle de services ne
reflétant pas entièrement celui de Lima, il est fort à penser que Lima aura à
intégrer de nouvelles fonctionnalités notamment au niveau de l'intégration à
Chorem, de la gestion des Imports Exports de sauvegardes et de la génération des
états comptables.
    

.. _ToPIA-persistence: http://topia.labs.libre-entreprise.org/topia/topia-persistence/
.. _ToPIA-soa: http://topia.labs.libre-entreprise.org/topia/topia-soa/
.. _ArgoUML: http://argouml.tigris.org/
