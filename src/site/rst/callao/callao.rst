.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

======================
Présentation de Callao
======================

(Computing Active Layer for Lutin's Accounting Organization)

``1. Présentation``
-------------------

La comptabilité est une tâche longue, répétitive, et induisant un nombre
important de contrôles et de calculs.

C'est un excellent exemple de ce que l'automatisation grâce à l'informatique
peut apporter en terme de gain de temps. Longtemps réservée aux grosses
entreprises, l'explosion de l'informatique personnelle (puis portable) à
largement démocratisé la comptabilité par informatique mais toujours par le biais
de solutions malheureusement propriétaires et/ou peu portables.

L'essor récent du logiciel libre permet l'émergence de solutions encore plus
accessibles, débarrassées des traditionnels verrous emprisonnant l'information
comptable, et donc son propriétaire, dans un contexte étriqué d'utilisation.
Ces logiciels nécessitant l'achat de licences spécifiques en cas d'emplois sur
plusieurs postes, et interdisant le partage ou la migration entre applications
"concurrentes".

``2. Naissance``
----------------

Issu du projet Chorem_,
Callao_ est un moteur de comptabilité `libre de droits`_ respectant les conventions
ainsi que les normes comptables imposées aux entreprises par la législation
française.

Bien que créé pour fonctionner avec l'interface graphique Lima_, Callao_ a été
conçu pour pouvoir être utilisé par différentes interfaces graphiques. Il est
donc possible de créer une interface Web de comptabilité reposant sur le moteur
Callao_

``3. Architecture``
-------------------

Les utilisateurs de l'interface utilisateur Lima_ effectuaient jusqu'alors cette gestion
des données gràce au moteur de comptabilité d'OFBiz-Neogia. Callao_ doit pouvoir se
substituer à ce moteur mais sans nécessité une refonte de Lima_ afin de laisser aux
utilisateurs de ce dernier le choix du moteur de comptabilité qu'ils
utilisent ou de faciliter les migrations d'un moteur à l'autre [#]_.

Ainsi notre couche de DTO se substituera à toute la partie exterieure à
l'interface de Lima_ comme suit :

.. image:: resources/schemas/schema-architecture.png



On peut voir que le DTO utilise le modèle définit par le design pattern du même
nom pour décrire le moteur Callao de l’application comptable finale.
L’architecture est elle même composée de deux couches : une de persistance pour
la gestion des données, et une autre métier pour la gestion des flux entre la
persistance et les interfaces d’entrées/sorties véhiculées par des objets
JavaBeans.


----

.. _`libre de droits`: licence.html
.. [#] A cet effet des fonctions d'import et d'export seront mises en place.
.. _ChoreM: http://chorem.org/
.. _Lima: http://maven-site.chorem.org/lima/
.. _Callao: callao.html
