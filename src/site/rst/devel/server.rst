.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2017 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

===========================
Lima en mode Client/Serveur
===========================

Serveur
-------

Télécharger le fichier `lima-server-VERSION-bin.zip`_ puis décompressez le fichier téléchargé et lancez le module serveur de lima.

Client
------

Configurez le client pour se connecter au serveur via le fichier de
configuration : ``lima-swing.config``

Selon votre système vous trouverez ce fichier à cet emplacement (remplacez USER_NAME par le nom de l'utilisateur):

Unix-like : ``$HOME/.config/lima-swing.config``

Windows 7\* et + : ``C:\Users\USER_NAME\AppData\Roaming\lima-swing.config``

Mac OS : ``$HOME/Library/Application Support/lima-swing.config``


Ajouter les lignes suivantes à votre fichier (remplacer '192.168.99.9' par l'adresse de votre serveur)

::

  java.naming.factory.initial=org.apache.openejb.client.RemoteInitialContextFactory
  java.naming.provider.url=ejbd://192.168.99.9:4201


.. _`lima-server-VERSION-bin.zip`: http://www.chorem.org/projects/lima/files