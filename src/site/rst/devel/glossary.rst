.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

======================================
Lexique des objets manipulés dans Lima
======================================

Ce lexique vise à clarifier les termes utilisés dans les objets de Lima (entités
beans...) par rapport à leur traduction française.

+------------------+--------------------------+-------------------------------+
| Anglais          | Français                 | Description                   |
+==================+==========================+===============================+
| Account          | Compte                   |                               |
+------------------+--------------------------+-------------------------------+
| Entry book       | Journal                  |                               |
+------------------+--------------------------+-------------------------------+
| Entry            | Écriture comptable       | Entrée comptable              |
+------------------+--------------------------+-------------------------------+
| Record           | Enregistrement comptable | Composition d'une écriture    |
+------------------+--------------------------+-------------------------------+
| Fiscal period    | Exercice                 |                               |
+------------------+--------------------------+-------------------------------+
| Financial period | Période                  |                               |
+------------------+--------------------------+-------------------------------+
| Balance trial    | Balance                  |                               |
+------------------+--------------------------+-------------------------------+
| Balance Sheet    | Bilan                    |                               |
+------------------+--------------------------+-------------------------------+

