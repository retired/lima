.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=======
OpenEJB
=======


Définition des EJB
------------------

Les implementations des services sont marqués avec l'annotation
@Stateless.

Point à verifier pour les annotations @Webservice et @Local, @Remote
sur les interfaces.


Embedded mode
-------------

Intantanciation et recherche dans un ``InitialContext`` local :

::

  Properties properties = new Properties();
  properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.openejb.client.LocalInitialContextFactory");
  
  InitialContext ctx = new InitialContext(properties);
  
  AccountService ejbHome = (AccountService)ctx.lookup("AccountServiceImplLocal");

Le nom "AccountServiceImplLocal" est ici une convention de nommage.

TODO reference ? (EC : impossible à retrouver)


Server mode
-----------

Tomcat
~~~~~~

Details : http://openejb.apache.org/tomcat.html


OpenEJB server
~~~~~~~~~~~~~~

Details : http://openejb.apache.org/remote-server.html
Testé avec : https://repository.apache.org/content/groups/snapshots/org/apache/openejb/openejb-standalone/3.1.3-SNAPSHOT/openejb-standalone-3.1.3-20100131.202752-9.zip

Procedure:
  - bin/openejb start &
  - bin/openejb deploy lima-business-0.4.0-SNAPSHOT-jar-with-dependencies.jar

Si le déploiment a réussi, openejb devrait afficher :

::

  Ejb(ejb-name=ReportServiceImpl, id=ReportServiceImpl)
    Jndi(name=ReportServiceImplRemote)
  
  Ejb(ejb-name=RecordServiceImpl, id=RecordServiceImpl)
    Jndi(name=RecordServiceImplRemote)
  
  Ejb(ejb-name=FinancialPeriodServiceImpl, id=FinancialPeriodServiceImpl)
    Jndi(name=FinancialPeriodServiceImplRemote)
  
  Ejb(ejb-name=FiscalPeriodServiceImpl, id=FiscalPeriodServiceImpl)
    Jndi(name=FiscalPeriodServiceImplRemote)
  
  Ejb(ejb-name=EntryBookServiceImpl, id=EntryBookServiceImpl)
    Jndi(name=EntryBookServiceImplRemote)
  
  Ejb(ejb-name=TransactionServiceImpl, id=TransactionServiceImpl)
    Jndi(name=TransactionServiceImplRemote)
  
  Ejb(ejb-name=AccountServiceImpl, id=AccountServiceImpl)
    Jndi(name=AccountServiceImplRemote)

Client
~~~~~~

  Properties properties = new Properties();
  properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.RemoteInitialContextFactory");
  properties.put("java.naming.provider.url", "ejbd://127.0.0.1:4201");
  properties.put("java.naming.security.principal", "jonathan");
  properties.put("java.naming.security.credentials", "secret");

  InitialContext ctx = new InitialContext(properties);
  
  AccountService ejbHome = (AccountService)ctx.lookup("AccountServiceImplRemote");

Les identifiants "jonathan/secret" sont ceux par defaut définit dans le fichier
OPENEJB_HOME/conf/users.properties

Les jar "javaee-api-5.0-3-SNAPSHOT.jar" et "openejb-client-3.1.3-SNAPSHOT.jar"
doivent être EXACTEMENT les mêmes entre le serveur et le client (sinon
une NPE exception survient tout le temps).
