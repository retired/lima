.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=============================================
Modification de l'architecture de Lima/Callao
=============================================

Voici le résumé de la modification de l'architecture de Lima/Callao.

Points majeurs :

- Intégration de callao dans Lima en tant que moteur par défaut ;
- Conservation de la possibilité de changer de moteur ou d'interface ;
- Suppression des DTO (utilisation des interfaces des entités métier générées).


Nouvelle architecture
---------------------

Trois couches (voir schéma) :
- UI ;
- Métier (indépendant de ToPIA) ;
- Persistance (généré sur ToPIA).


UI
~~

Dans un premier temps, une interface de type Swing.
Mais une autre interface pourra être utilisée par la suite.

Pas de DTO manipulé dans l'UI, utilisation des interfaces des beans métier
(sans utiliser de spécificités de TopiaEntity).

L'interface graphique contiendrait les fonctionnalités suivantes :

- import/export des données.


Métier
~~~~~~

Se manipule via les interfaces des DAO.
L'implémentation des DAO étant sur ToPIA.

Problèmes liés aux transaction. L'utilisation du TopiaContext n'est
pas envisageable et rendrait trop dépendant de Topia.

Solution 1 : JTA
 Utilisation de JTA et enregistrement de Topia comme
 transaction JTA. Permet de faire des vrai transaction sans utilisation
 directe du topia contexte.

Solution 2 : EJB
 Implémentation via les EJB qui permet en plus
 des fournir directement des webservices.
 Utilisation d'OpenEJB pour pouvoir tourner en mode embarqué (sans
 serveur d'application)

Pas d'intrusion des spécificités EJB sur les DAO. Utilisation d'un
code annexe pour wrapper sur les DAO.

Le métier contiendrait également les fonctionnalités métier suivantes:
- les règles métier / cohérence de comptabilité (???) ;
- génération des bilan ;
- génération des impressions.

Persistance
~~~~~~~~~~~

Pour Callao : génération entièrement sur ToPIA.

Pour ofbiz, SAP : nouvelle implémentation sur les interfaces des DAO.
Les modules pour OFBiz (etc...) dépendront donc de la persistance
générée.

