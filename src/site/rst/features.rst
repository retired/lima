.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

===============
Fonctionnalités
===============

Structure
---------

Cette section permet de définir le cadre de la comptabilité :

Plan comptable
~~~~~~~~~~~~~~

.. image:: screens/lima_charts_accounts.png

Journaux
~~~~~~~~

.. image:: screens/lima_charts_entrybooks.png

Éxercices
~~~~~~~~~

.. image:: screens/lima_charts_fiscalperiod.png

Période comptable
~~~~~~~~~~~~~~~~~

.. image:: screens/lima_charts_financialperiod.png

Plan bilan / compte de résultat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: screens/lima_charts_financialstatement.png

Plan TVA
~~~~~~~~

.. image:: screens/lima_charts_vat.png

Traitement
----------

La partie traitement regroupe les fonctions utiles au quotidien :

Saisie
~~~~~~

.. image:: screens/lima_entries.png

Consultation
~~~~~~~~~~~~

.. image:: screens/lima_accont_viewer.png

Correction
~~~~~~~~~~

.. image:: screens/lima_incorrectEntries.png

Recherche
~~~~~~~~~

.. image:: screens/lima_searchEntry.png

Lettrage
~~~~~~~~

.. image:: screens/lima_lettering.png

Rapports
--------

Permet de visualiser les documents usuels de la comptabilité.
Ils sont atteignables via le menu de Lima 'Fichier -> Documents...' ou directement depuis votre navigateur à l'adresse http://localhost:5462/

Les documents disponibles sont :

* Compte
* Balance générale
* Balance globale
* Journal
* Journal générale
* Bilan et Compte de résultat
* Grand livre
* Déclaration de TVA

*Page de sélection des rapports:*

.. image:: screens/lima_reports_menu_global_menu.png


Import / Export
---------------

Toutes les données de Lima sont exportables et importables à volonté au format CSV.
L'utilisation de ce format permet l'exploitation des données dans un tableur.

Lima permet l'export FEC recquis par l’administration fiscale Française pour certains contrôles.

Actuellement Lima permet l'import et l'export pour EBP. Il est possible d'ajouter
à la demande l'import / export pour des formats de fichiers d'autres logiciels.
