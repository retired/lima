.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2017 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -


Sauvegarde / Restauration
-------------------------

Base de données H2
==================

La base de données utilisée par Lima par défaut est une base de données H2, elle est situé dans le dossier .lima

- Unix-like : ``$HOME/.lima``

- Mac OS : ``$HOME/.lima``

- Windows 7 et + : ``C:\Users\USER_NAME\.lima``

À ce jour Lima n'est pas pourvu de mécanisme de sauvegarde automatique il est donc fortement conseillé de saugarder régulièrement ce dossier.

En cas de problème il suffira alors de restaurer votre sauvegarde.

Base de données Postgresql
==========================

Depuis la version 0.8.7 il est possible de définir une base de données Postgresql.

Dans ce cas utilisez les commandes **pg_dump** et **pg_restore**.
