.. -
.. * #%L
.. * Lima
.. * %%
.. * Copyright (C) 2008 - 2017 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

==============
Contactez-nous
==============

Pour contacter les membres du projet ou suivre son évolution, vous pouvez
utiliser les listes de diffusion du projet :

  * `Liste utilisateurs`_ : Liste de discussions pour les utilisateurs de Lima
  * `Liste développeurs`_ : Liste de discussions autour du développement de Lima
  * `Liste des commits`_ : Liste des modifications du code de Lima

Il est préférable de vous inscrire à la liste qui vous intéresse pour échanger
sur vos questions, mais ce n'est pas obligatoire (vous pouvez simplement envoyer
un email à la(es) liste(s)).


Support
~~~~~~~

`Code Lutin`_ est une `Société de Services en Logiciels Libres`_ spécialiste du logiciel libre, des langages Java et Javascript depuis 2002. Son offre s'étend à l'audit, au conseil, à la
Tierce Maintenance Applicative et à la formation.

  SASPO Code Lutin

  12, avenue Jules Verne

  44230 Saint-Sébastien-Sur-Loire

  France

  Tél : 02 40 50 29 28


Code Lutin participe activement au mouvement du Logiciel Libre et fait partie du
consortium ObjectWeb, d'`Alliance Libre`_ et du réseau `Libre-entreprise`_.


.. _`Liste utilisateurs`: http://list.chorem.org/cgi-bin/mailman/listinfo/lima-users
.. _`Liste développeurs`: http://list.chorem.org/cgi-bin/mailman/listinfo/lima-devel
.. _`Liste des commits`: http://list.chorem.org/cgi-bin/mailman/listinfo/lima-commits
.. _`Code Lutin`: http://www.codelutin.com
.. _`Société de Services en Logiciels Libres`: http://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_de_services_en_logiciels_libres
.. _`Alliance Libre`: http://www.alliance-libre.org/
.. _`Libre-entreprise`: http://www.libre-entreprise.org


