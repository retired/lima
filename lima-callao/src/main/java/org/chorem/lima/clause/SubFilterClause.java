package org.chorem.lima.clause;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.filter.Filter;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SubFilterClause implements Clause {

    protected String property;

    protected Multiplicity multiplicity;

    protected Filter subFilter;

    public SubFilterClause(String property, Filter subFilter, Multiplicity multiplicity) {
        this.property = property;
        this.subFilter = subFilter;
        this.multiplicity = multiplicity;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Filter getSubFilter() {
        return subFilter;
    }

    public void setSubFilter(Filter subFilter) {
        this.subFilter = subFilter;
    }

    @Override
    public void accept(VisitorClause v) {
        v.visitSubFilterClause(this);
    }

    public Multiplicity getMultiplicity() {
        return multiplicity;
    }

    public void setMultiplicity(Multiplicity multiplicity) {
        this.multiplicity = multiplicity;
    }

    public  enum Multiplicity {
        ONE,
        MANY
    }

}
