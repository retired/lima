package org.chorem.lima.clause;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class NumberClause implements Clause {

    protected String property;

    protected Operand operand;

    protected Number value;

    public NumberClause(String property) {
        this.property = property;
        setOperand(Operand.EQUAL);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Operand getOperand() {
        return operand;
    }

    public void setOperand(Operand operand) {
        this.operand = operand;
    }

    public Number getValue() {
        return value;
    }

    public void setValue(Number value) {
        this.value = value;
    }

    @Override
    public void accept(VisitorClause v) {
        v.visitNumberClause(this);
    }

    public  enum Operand {
        EQUAL,
        NOT_EQUAL,
        UPPER,
        UPPER_OR_EQUAL,
        LOWER,
        LOWER_OR_EQUAL
    }
}
