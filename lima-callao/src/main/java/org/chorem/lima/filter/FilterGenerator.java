package org.chorem.lima.filter;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.chorem.lima.beans.AbstractBigDecimalCondition;
import org.chorem.lima.beans.AbstractStringCondition;
import org.chorem.lima.beans.AccountCondition;
import org.chorem.lima.beans.Condition;
import org.chorem.lima.beans.CreditCondition;
import org.chorem.lima.beans.DateCondition;
import org.chorem.lima.beans.DateIntervalCondition;
import org.chorem.lima.beans.DebitCondition;
import org.chorem.lima.beans.DescriptionCondition;
import org.chorem.lima.beans.EntryBookCondition;
import org.chorem.lima.beans.FinancialPeriodCondition;
import org.chorem.lima.beans.FinancialTransactionCondition;
import org.chorem.lima.beans.FiscalPeriodCondition;
import org.chorem.lima.beans.LetteringCondition;
import org.chorem.lima.beans.VisitorCondition;
import org.chorem.lima.beans.VoucherCondition;
import org.chorem.lima.clause.AndClause;
import org.chorem.lima.clause.BooleanClause;
import org.chorem.lima.clause.Clause;
import org.chorem.lima.clause.DateClause;
import org.chorem.lima.clause.NotClause;
import org.chorem.lima.clause.NumberClause;
import org.chorem.lima.clause.OrClause;
import org.chorem.lima.clause.SetClause;
import org.chorem.lima.clause.StringClause;
import org.chorem.lima.clause.SubFilterClause;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.FinancialPeriod;
import org.chorem.lima.entity.FiscalPeriod;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FilterGenerator implements VisitorCondition {

    protected List<Clause> clauses;
    protected List<Clause> entryClauses;
    protected FinancialTransactionFilter filter;
    protected EntryFilter entryFilter;

    public FilterGenerator() {
        clauses = new ArrayList<>();
        entryClauses = new ArrayList<>();
    }


    @Override
    public void visitFinancialTransactionCondition(FinancialTransactionCondition condition) {
        if (condition != null && condition.getConditions() != null) {
            filter = new FinancialTransactionFilter();
            for (Condition subCondition : condition.getConditions()) {
                if (subCondition.validCondition()) {
                    subCondition.accept(this);
                }
            }
            if (clauses.size() == 1 ) {
                filter.setClause(clauses.get(0));
            } else if (clauses.size() > 1) {
                if (condition.isAllConditions()) {
                    AndClause clause = new AndClause();
                    clause.addAll(clauses);
                    filter.setClause(clause);
                } else {
                    OrClause clause = new OrClause();
                    clause.addAll(clauses);
                    filter.setClause(clause);
                }
            }

            if (entryClauses.size() == 1 ) {
                entryFilter.setClause(entryClauses.get(0));
            } else if (entryClauses.size() > 1) {
                if (condition.isAllConditions()) {
                    AndClause clause = new AndClause();
                    clause.addAll(entryClauses);
                    entryFilter.setClause(clause);
                } else {
                    OrClause clause = new OrClause();
                    clause.addAll(entryClauses);
                    entryFilter.setClause(clause);
                }
            }
        }
    }

    @Override
    public void visitDateCondition(DateCondition condition) {
        if (condition != null && condition.getOperand() != null) {
            DateClause dateClause = getFilter().newTransactionDateClause();
            switch (condition.getOperand()) {
                case SAME:
                    dateClause.setOperand(DateClause.Operand.SAME);
                    break;
                case AFTER:
                    dateClause.setOperand(DateClause.Operand.AFTER);
                    break;
                case PREVIOUS:
                    dateClause.setOperand(DateClause.Operand.PREVIOUS);
                    break;
                case DIFFERENT:
                    dateClause.setOperand(DateClause.Operand.DIFFERENT);
                    break;
            }
            dateClause.setValue(condition.getDate());
            clauses.add(dateClause);
        }
    }

    @Override
    public void visitDateIntervalCondition(DateIntervalCondition condition) {
        if (condition != null && condition.getBeginDate() != null && condition.getEndDate() != null) {
            AndClause andClause = new AndClause();

            DateClause beginDateClause = getFilter().newTransactionDateClause();
            beginDateClause.setOperand(DateClause.Operand.AFTER);
            beginDateClause.setValue(condition.getBeginDate());
            andClause.add(beginDateClause);

            DateClause endDateClause = getFilter().newTransactionDateClause();
            endDateClause.setOperand(DateClause.Operand.PREVIOUS);
            endDateClause.setValue(condition.getEndDate());
            andClause.add(endDateClause);

            clauses.add(andClause);
        }
    }

    @Override
    public void visitEntryBookCondition(EntryBookCondition condition) {
        if (condition != null && condition.getEntryBooks() != null) {
            SetClause setClause = getFilter().newEntryBookSetClause();
            setClause.addAll(condition.getEntryBooks());
            clauses.add(setClause);
        }
    }

    @Override
    public void visitFinancialPeriodCondition(FinancialPeriodCondition condition) {
        if (condition != null && condition.getFinancialPeriods() != null) {
            OrClause orClause = new OrClause();
            for (FinancialPeriod period : condition.getFinancialPeriods()) {
                AndClause andClause = new AndClause();

                DateClause beginDateClause = getFilter().newTransactionDateClause();
                beginDateClause.setOperand(DateClause.Operand.AFTER);
                beginDateClause.setValue(period.getBeginDate());
                andClause.add(beginDateClause);

                DateClause endDateClause = getFilter().newTransactionDateClause();
                endDateClause.setOperand(DateClause.Operand.PREVIOUS);
                endDateClause.setValue(period.getEndDate());
                andClause.add(endDateClause);

                orClause.add(andClause);
            }
            if (orClause.size() == 1) {
                clauses.add(orClause.get(0));
            } else {
                clauses.add(orClause);
            }
        }
    }

    @Override
    public void visitFiscalPeriodCondition(FiscalPeriodCondition condition) {
        if (condition != null && condition.getFiscalPeriods()!=null) {
            OrClause orClause = new OrClause();
            for (FiscalPeriod period : condition.getFiscalPeriods()) {
                AndClause andClause = new AndClause();

                DateClause beginDateClause = getFilter().newTransactionDateClause();
                beginDateClause.setOperand(DateClause.Operand.AFTER);
                beginDateClause.setValue(period.getBeginDate());
                andClause.add(beginDateClause);

                DateClause endDateClause = getFilter().newTransactionDateClause();
                endDateClause.setOperand(DateClause.Operand.PREVIOUS);
                endDateClause.setValue(period.getEndDate());
                andClause.add(endDateClause);

                orClause.add(andClause);
            }
            if (orClause.size() == 1) {
                clauses.add(orClause.get(0));
            } else {
                clauses.add(orClause);
            }
        }
    }

    protected void setupEntryFilter() {
        if (entryFilter == null) {
            SubFilterClause subFilterClause = filter.newEntryFilterClause();
            clauses.add(subFilterClause);
            entryFilter = (EntryFilter) subFilterClause.getSubFilter();
        }
    }

    protected StringClause.Operand getStringClauseOperand(AbstractStringCondition.Operand operand) {
        StringClause.Operand result;
        switch (operand) {
           case NOT_EQUAL:
               result = StringClause.Operand.NOT_EQUAL;
                break;
            case BEGIN:
                result = StringClause.Operand.BEGIN;
                break;
            case ENDING:
                result = StringClause.Operand.ENDING;
                break;
            case CONTAIN:
                result = StringClause.Operand.CONTAIN;
                break;
            case EQUAL:
            default:
                result = StringClause.Operand.EQUAL;
                break;
        }
        return result;
    }

    @Override
    public void visitDescriptionCondition(DescriptionCondition descriptionCondition) {
        if (descriptionCondition != null
                && descriptionCondition.getOperand() != null
                && StringUtils.isNotBlank(descriptionCondition.getValue())) {
            setupEntryFilter();
            StringClause stringClause = entryFilter.newDescriptionClause();

            AbstractStringCondition.Operand operand = descriptionCondition.getOperand();
            StringClause.Operand stringClauseOperand = getStringClauseOperand(operand);
            stringClause.setOperand(stringClauseOperand);

            stringClause.setValue(descriptionCondition.getValue());

            stringClause.setSensitiveCase(descriptionCondition.isSensitiveCase());

            entryClauses.add(stringClause);
        }
    }

    @Override
    public void visitVoucherCondition(VoucherCondition voucherCondition) {
        setupEntryFilter();
        StringClause stringClause = entryFilter.newVoucherClause();

        AbstractStringCondition.Operand operand = voucherCondition.getOperand();
        StringClause.Operand stringClauseOperand = getStringClauseOperand(operand);
        stringClause.setOperand(stringClauseOperand);

        String value = voucherCondition.getValue() == null ? "*" : voucherCondition.getValue();
        stringClause.setValue(value);

        stringClause.setSensitiveCase(voucherCondition.isSensitiveCase());

        entryClauses.add(stringClause);
    }

    @Override
    public void visitLetteringCondition(LetteringCondition letteringCondition) {
        if (letteringCondition != null
                && letteringCondition.getOperand() != null
                && StringUtils.isNotBlank(letteringCondition.getValue())){
            setupEntryFilter();
            StringClause stringClause = entryFilter.newLetteringClause();

            AbstractStringCondition.Operand operand = letteringCondition.getOperand();
            StringClause.Operand stringClauseOperand = getStringClauseOperand(operand);
            stringClause.setOperand(stringClauseOperand);

            stringClause.setValue(letteringCondition.getValue());

            stringClause.setSensitiveCase(letteringCondition.isSensitiveCase());

            entryClauses.add(stringClause);
        }

    }

    @Override
    public void visitAccountCondition(AccountCondition accountCondition) {
        if (accountCondition != null
                && accountCondition.getAccount() != null
                && accountCondition.getAccount().getAccountNumber() != null){
            setupEntryFilter();
            SubFilterClause subFilterClause = entryFilter.newAccountFilterClause();
            AccountFilter accountFilter = (AccountFilter) subFilterClause.getSubFilter();
            StringClause stringClause = accountFilter.newAccountNumberClause();
            stringClause.setOperand(StringClause.Operand.BEGIN);
            Account account = accountCondition.getAccount();
            stringClause.setValue(account.getAccountNumber());

            accountFilter.setClause(stringClause);
            entryClauses.add(subFilterClause);
        }
    }

    protected NumberClause.Operand getNumberClauseOperand(AbstractBigDecimalCondition.Operand operand) {
        NumberClause.Operand result = NumberClause.Operand.EQUAL;
        if (operand != null) {
            switch (operand) {
                case NOT_EQUAL:
                    result = NumberClause.Operand.NOT_EQUAL;
                    break;
                case LOWER:
                    result = NumberClause.Operand.LOWER;
                    break;
                case LOWER_OR_EQUAL:
                    result = NumberClause.Operand.LOWER_OR_EQUAL;
                    break;
                case UPPER:
                    result = NumberClause.Operand.UPPER;
                    break;
                case UPPER_OR_EQUAL:
                    result = NumberClause.Operand.UPPER_OR_EQUAL;
                    break;
                case EQUAL:
                default:
                    result = NumberClause.Operand.EQUAL;
                    break;
            }
        }
        return result;
    }

    @Override
    public void visitDebitCondition(DebitCondition debitCondition) {
        if (debitCondition != null
                && debitCondition.getOperand() != null
                && debitCondition.getValue() != null){
            setupEntryFilter();

            BooleanClause debitClause = entryFilter.newDebitClause();

            NumberClause amountClause = entryFilter.newAmountClause();

            AbstractBigDecimalCondition.Operand operand = debitCondition.getOperand();
            NumberClause.Operand clauseOperand = getNumberClauseOperand(operand);
            amountClause.setOperand(clauseOperand);

            amountClause.setValue(debitCondition.getValue());

            AndClause andClause = new AndClause();
            andClause.add(debitClause);
            andClause.add(amountClause);

            entryClauses.add(andClause);
        }
    }

    @Override
    public void visitCreditCondition(CreditCondition creditCondition) {
        if (creditCondition != null && creditCondition.getOperand() != null && creditCondition.getValue() != null) {
            setupEntryFilter();

            BooleanClause debitClause = entryFilter.newDebitClause();
            NotClause creditClause = new NotClause();
            creditClause.setClause(debitClause);

            NumberClause amountClause = entryFilter.newAmountClause();

            AbstractBigDecimalCondition.Operand operand = creditCondition.getOperand();
            NumberClause.Operand clauseOperand = getNumberClauseOperand(operand);
            amountClause.setOperand(clauseOperand);

            amountClause.setValue(creditCondition.getValue());

            AndClause andClause = new AndClause();
            andClause.add(creditClause);
            andClause.add(amountClause);

            entryClauses.add(andClause);
        }

    }

    public FinancialTransactionFilter getFilter() {
        return filter;
    }
}
