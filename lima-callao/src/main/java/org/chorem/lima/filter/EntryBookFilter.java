package org.chorem.lima.filter;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.clause.StringClause;
import org.chorem.lima.entity.EntryBook;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class EntryBookFilter extends AbstractFilter {

    public StringClause newLabelClause() {
        return new StringClause(EntryBook.PROPERTY_LABEL);
    }

    public StringClause newCodeClause() {
        return new StringClause(EntryBook.PROPERTY_CODE);
    }

    @Override
    public void accept(VisitorFilter v) {
        v.visitEntryBookFilter(this);
    }

    @Override
    public Class getEntityClass() {
        return EntryBook.class;
    }

}
