package org.chorem.lima.filter;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.clause.DateClause;
import org.chorem.lima.clause.SetClause;
import org.chorem.lima.clause.SubFilterClause;
import org.chorem.lima.entity.FinancialTransaction;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FinancialTransactionFilter  extends AbstractFilter {

    public DateClause newTransactionDateClause() {
        return new DateClause(FinancialTransaction.PROPERTY_TRANSACTION_DATE);
    }

    public SubFilterClause newEntryBookFilterClause() {
        return new SubFilterClause(FinancialTransaction.PROPERTY_ENTRY_BOOK, new EntryBookFilter(), SubFilterClause.Multiplicity.ONE);
    }

    public SetClause newEntryBookSetClause() {
        return new SetClause(FinancialTransaction.PROPERTY_ENTRY_BOOK);
    }

    public SubFilterClause newEntryFilterClause () {
        return new SubFilterClause(FinancialTransaction.PROPERTY_ENTRY, new EntryFilter(), SubFilterClause.Multiplicity.MANY);
    }

    @Override
    public void accept(VisitorFilter v) {
        v.visitFinancialTransactionFilter(this);
    }

    @Override
    public Class getEntityClass() {
        return FinancialTransaction.class;
    }
}
