package org.chorem.lima.filter;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.clause.BooleanClause;
import org.chorem.lima.clause.NumberClause;
import org.chorem.lima.clause.SetClause;
import org.chorem.lima.clause.StringClause;
import org.chorem.lima.clause.SubFilterClause;
import org.chorem.lima.entity.Entry;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class EntryFilter extends AbstractFilter {

    public NumberClause newAmountClause() {
        return new NumberClause(Entry.PROPERTY_AMOUNT);
    }

    public BooleanClause newDebitClause() {
        return new BooleanClause(Entry.PROPERTY_DEBIT);
    }

    public StringClause newDetailClause() {
        return new StringClause(Entry.PROPERTY_AMOUNT);
    }

    public StringClause newPositionClause() {
        return new StringClause(Entry.PROPERTY_POSITION);
    }

    public StringClause newVoucherClause() {
        return new StringClause(Entry.PROPERTY_VOUCHER);
    }

    public StringClause newDescriptionClause() {
        return new StringClause(Entry.PROPERTY_DESCRIPTION);
    }

    public StringClause newLetteringClause() {
        return new StringClause(Entry.PROPERTY_LETTERING);
    }

    public SubFilterClause newAccountFilterClause() {
        return new SubFilterClause(Entry.PROPERTY_ACCOUNT, new AccountFilter(), SubFilterClause.Multiplicity.ONE);
    }

    public SetClause newAccountSetClause() {
        return new SetClause(Entry.PROPERTY_ACCOUNT);
    }

    public SubFilterClause newFinancialTransactionFilterClause() {
        return new SubFilterClause(Entry.PROPERTY_FINANCIAL_TRANSACTION, new FinancialTransactionFilter(), SubFilterClause.Multiplicity.ONE);
    }

    public SetClause newFinancialTransactionSetClause() {
        return new SetClause(Entry.PROPERTY_FINANCIAL_TRANSACTION);
    }

    @Override
    public void accept(VisitorFilter v) {
        v.visitEntryFilter(this);
    }

    @Override
    public Class getEntityClass() {
        return Entry.class;
    }
}
