package org.chorem.lima.filter;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.clause.StringClause;
import org.chorem.lima.entity.Account;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class AccountFilter extends AbstractFilter {

    public StringClause newLabelClause() {
        return new StringClause(Account.PROPERTY_LABEL);
    }

    public StringClause newAccountNumberClause() {
        return new StringClause(Account.PROPERTY_ACCOUNT_NUMBER);
    }

    public StringClause newThirdPartyClause() {
        return new StringClause(Account.PROPERTY_THIRD_PARTY);
    }


    @Override
    public void accept(VisitorFilter v) {
        v.visitAccountFilter(this);
    }

    @Override
    public Class getEntityClass() {
        return Account.class;
    }
}
