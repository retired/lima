package org.chorem.lima.beans;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class DateCondition implements Condition, Serializable {

    protected Date date;

    protected Operand operand;

    public DateCondition() {
        operand = Operand.SAME;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Operand getOperand() {
        return operand;
    }

    public void setOperand(Operand operand) {
        this.operand = operand;
    }

    @Override
    public boolean validCondition() {
        return date != null;
    }

    @Override
    public void accept(VisitorCondition v) {
        v.visitDateCondition(this);
    }

    public enum Operand implements Labeled {
        SAME(t("lima.enum.date.operand.same")),
        AFTER(t("lima.enum.date.operand.after")),
        PREVIOUS(t("lima.enum.date.operand.previous")),
        DIFFERENT(t("lima.enum.date.operand.different"));

        protected String label;

        Operand(String label) {
            this.label = label;
        }

        @Override
        public String getLabel() {
            return label;
        }
    }

}
