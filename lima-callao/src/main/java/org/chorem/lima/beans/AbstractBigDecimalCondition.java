package org.chorem.lima.beans;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.math.BigDecimal;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public abstract class AbstractBigDecimalCondition implements Condition, Serializable {

    protected BigDecimal value;

    protected Operand operand;

    public AbstractBigDecimalCondition() {
        setOperand(Operand.EQUAL);
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Operand getOperand() {
        return operand;
    }

    public void setOperand(Operand operand) {
        this.operand = operand;
    }

    public enum Operand implements Labeled {
        EQUAL(t("lima.enum.BigDecimal.operand.equal")),
        NOT_EQUAL(t("lima.enum.BigDecimal.operand.notequal")),
        LOWER(t("lima.enum.BigDecimal.operand.lower")),
        LOWER_OR_EQUAL(t("lima.enum.BigDecimal.operand.lowerorequal")),
        UPPER(t("lima.enum.BigDecimal.operand.upper")),
        UPPER_OR_EQUAL(t("lima.enum.BigDecimal.operand.upperorequal"));

        protected String label;

        Operand(String label) {
            this.label = label;
        }

        @Override
        public String getLabel() {
            return label;
        }
    }
}
