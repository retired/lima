package org.chorem.lima.beans;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public abstract class AbstractStringCondition implements Condition, Serializable {

    protected String value;

    protected Operand operand;

    protected boolean sensitiveCase;

    public AbstractStringCondition() {
        setOperand(Operand.EQUAL);
        setSensitiveCase(false);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Operand getOperand() {
        return operand;
    }

    public void setOperand(Operand operand) {
        this.operand = operand;
    }

    public boolean isSensitiveCase() {
        return sensitiveCase;
    }

    public void setSensitiveCase(boolean sensitiveCase) {
        this.sensitiveCase = sensitiveCase;
    }

    public enum Operand implements Labeled {
        EQUAL(t("lima.enum.string.operand.equal")),
        NOT_EQUAL(t("lima.string.date.operand.notequal")),
        BEGIN(t("lima.enum.string.operand.begin")),
        ENDING(t("lima.enum.string.operand.ending")),
        CONTAIN(t("lima.enum.string.operand.contain"));

        protected String label;

        Operand(String label) {
            this.label = label;
        }

        @Override
        public String getLabel() {
            return label;
        }
    }

}
