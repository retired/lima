package org.chorem.lima.beans;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FinancialTransactionCondition implements Condition, Serializable {

    protected boolean allConditions;

    protected List<Condition> conditions;

    public FinancialTransactionCondition() {
        conditions = new ArrayList<>();
    }

    public boolean isAllConditions() {
        return allConditions;
    }

    public void setAllConditions(boolean allConditions) {
        this.allConditions = allConditions;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    @Override
    public boolean validCondition() {
        return true;
    }

    @Override
    public void accept(VisitorCondition v) {
        v.visitFinancialTransactionCondition(this);
    }
}
