package org.chorem.lima.beans;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.chorem.lima.entity.FiscalPeriod;

import java.io.Serializable;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 * @since 0.7
 */
public class FiscalPeriodCondition implements Condition, Serializable {

    private static final long serialVersionUID = 1L;

    protected List<FiscalPeriod> fiscalPeriods;

    public List<FiscalPeriod> getFiscalPeriods() {
        return fiscalPeriods;
    }

    public void setFiscalPeriods(List<FiscalPeriod> fiscalPeriods) {
        this.fiscalPeriods = fiscalPeriods;
    }

    @Override
    public boolean validCondition() {
        return fiscalPeriods != null;
    }

    @Override
    public void accept(VisitorCondition v) {
        v.visitFiscalPeriodCondition(this);
    }
}
