package org.chorem.lima.beans;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.math.BigDecimal;

public class BalanceAccountImpl extends BalanceAccount {

    private static final long serialVersionUID = 1L;

    public BalanceAccountImpl() {
        this.amountDebit = BigDecimal.ZERO;
        this.amountCredit = BigDecimal.ZERO;
        this.debitBalance = BigDecimal.ZERO;
        this.creditBalance =  BigDecimal.ZERO;
    }

    public void addSubAccount(BalanceAccount subAccount) {

        // we only add account that have values
        if (subAccount.getAmountDebit() != null && subAccount.getAmountCredit() != null) {
            getSubAccounts().add(subAccount);

            this.amountDebit = this.amountDebit.add(subAccount.getAmountDebit());
            this.amountCredit = this.amountCredit.add(subAccount.getAmountCredit());
            this.debitBalance = this.debitBalance.add(subAccount.getDebitBalance());
            this.creditBalance = this.creditBalance.add(subAccount.getCreditBalance());
        }

//        firePropertyChange(PROPERTY_SUB_ACCOUNTS, null, subAccount);
    }
}
