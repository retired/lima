/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.entity;

import java.math.BigDecimal;

/**
 * Financial transaction implementation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FinancialTransactionImpl extends FinancialTransactionAbstract {

    private static final long serialVersionUID = 1L;

    /*
     * @see org.chorem.lima.entity.FinancialTransaction#getAmountDebit()
     */
    @Override
    public BigDecimal getAmountDebit() {
        BigDecimal result = BigDecimal.ZERO;
        if (getEntry() != null) {
            for (Entry entryFT : getEntry()) {
                if (entryFT.isDebit()) {
                    result = result.add(entryFT.getAmount());
                }
            }
        }
        
        return result;
    }

    /*
     * @see org.chorem.lima.entity.FinancialTransaction#getAmountCredit()
     */
    @Override
    public BigDecimal getAmountCredit() {
        BigDecimal result = BigDecimal.ZERO;
        if (getEntry() != null) {
            for (Entry entryFT : getEntry()) {
                if (!entryFT.isDebit()) {
                    result = result.add(entryFT.getAmount());
                }
            }
        }
        return result;
    }
}
