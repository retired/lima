package org.chorem.lima.entity;

/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.Comparator;
import java.util.Date;

/**
 * Created by davidcosse on 13/02/15.
 */
public class FiscalPeriodImpl extends FiscalPeriodAbstract {

    private static final long serialVersionUID = 3487305853036673333L;

    public static final Comparator<FiscalPeriod> BEGIN_DATE_COMPARATOR = new BeginDateComparator();

    private static class BeginDateComparator
            implements Comparator<FiscalPeriod>, java.io.Serializable {

        private static final long serialVersionUID = 0L;

        public int compare(FiscalPeriod s1, FiscalPeriod s2) {
            Date d1 = s1.getBeginDate();
            Date d2 = s2.getBeginDate();

            int result = 0;
            if (d1.after(d2)) {
                result = 1;
            } else if (d1.before(d2)){
                result = -1;
            }

            return result;
        }
    }
}
