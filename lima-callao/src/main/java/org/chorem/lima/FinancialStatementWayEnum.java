/*
 * #%L
 * Lima :: callao
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima;

import org.chorem.lima.beans.Labeled;

import static org.nuiton.i18n.I18n.t;

public enum FinancialStatementWayEnum implements Labeled {

    BOTH(t("lima.financialStatement.way.both")),
    DEBIT(t("lima.financialStatement.way.debit")),
    CREDIT(t("lima.financialStatement.way.credit"));
    
    private final String label;
    
    FinancialStatementWayEnum(String label) {
        this.label = label;
    }
    
    public String getLabel() {
        return label;
    }
    
}
