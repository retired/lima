---
-- #%L
-- Lima :: callao
-- %%
-- Copyright (C) 2008 - 2015 CodeLutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
 -- migration Lima data base from 0.6 to 0.8.6

-- migration from h2 to 1.3.166 not supporting index on clob
--CREATE TABLE TREASURY (
--   TOPIAID varchar (255),
--   TOPIAVERSION bigint(19),
--   TOPIACREATEDATE timestamp(23),
--   ADDRESS varchar (255),
--   ADDRESS2 varchar (255),
--   CDI varchar (2),
--   CITY varchar (30),
--   KEY varchar (2),
--   SERVICECODE varchar (3),
--   SIE varchar (7),
--   SYSTEMTYPE varchar (3),
--   VATNUMBER varchar (6),
--   ZIPCODE varchar (5)
--);
CREATE TABLE TREASURY (
   TOPIAID varchar (255),
   TOPIAVERSION bigint(19),
   TOPIACREATEDATE timestamp(23),
   DOSSIERNUMBER varchar (30),
   ADDRESS varchar (255),
   ADDRESS2 varchar (255),
   ZIPCODE varchar (30),
   CITY varchar (30),
   CDI varchar (30),
   KEY varchar (30),
   SERVICECODE varchar (30),
   SIE varchar (30),
   SYSTEMTYPE varchar (30)
);