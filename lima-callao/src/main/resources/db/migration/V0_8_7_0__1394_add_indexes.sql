---
-- #%L
-- Lima :: callao
-- %%
-- Copyright (C) 2008 - 2015 CodeLutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

CREATE INDEX idx_FinancialTransaction_transactionDate ON FINANCIALTRANSACTION (transactionDate);
CREATE INDEX idx_FinancialTransaction_EntryBookTransactionDate ON FINANCIALTRANSACTION (entryBook, transactionDate);
CREATE INDEX idx_FinancialPeriod_beginDate ON FINANCIALPERIOD (beginDate);
CREATE INDEX idx_FinancialPeriod_endDate ON FINANCIALPERIOD (endDate);
CREATE INDEX idx_FiscalPeriod_beginDateendDate ON FINANCIALPERIOD (beginDate,endDate);
CREATE INDEX idx_FinancialStatement_CreateDate ON FinancialStatement (topiaCreateDate);
CREATE INDEX idx_EntryBook_label ON ENTRYBOOK (label);
CREATE INDEX idx_ClosedPeriodicEntryBook_locked ON CLOSEDPERIODICENTRYBOOK (locked);
CREATE INDEX idx_Account_label ON ACCOUNT (label);