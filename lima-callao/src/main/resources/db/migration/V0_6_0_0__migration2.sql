---
-- #%L
-- Lima :: callao
-- %%
-- Copyright (C) 2008 - 2015 CodeLutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
 -- migration Lima data base from 0.5 to 0.6

-- migration from h2 to 1.3.166 not supporting index on clob
Alter table account alter column accountnumber varchar;

-- add new columns
alter table account drop column masteraccount;
alter table account drop column generalledger;
alter table entry add column lettering varchar(30);
alter table financialtransaction drop column financialperiod;

-- perform some migration
update entry e set lettering = (select l.code from Letter l where l.topiaid = e.letter);

-- delete non necessary fields
Alter table FINANCIALTRANSACTION drop column AMOUNTDEBIT;
Alter table FINANCIALTRANSACTION drop column AMOUNTCREDIT;
alter table vatstatement drop column amount;
alter table entry drop column letter;

-- delete tables
drop table record;
drop table letter;
