/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.report.service;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.examples.fdf.SetField;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.beans.DocumentReport;
import org.chorem.lima.beans.FinancialStatementAmounts;
import org.chorem.lima.business.LimaServiceFactory;
import org.chorem.lima.business.api.AccountService;
import org.chorem.lima.business.api.FinancialStatementService;
import org.chorem.lima.business.api.IdentityService;
import org.chorem.lima.business.api.OptionsService;
import org.chorem.lima.business.api.TreasuryService;
import org.chorem.lima.business.api.VatStatementService;
import org.chorem.lima.business.api.report.AccountReportService;
import org.chorem.lima.business.api.report.BalanceReportService;
import org.chorem.lima.business.api.report.GeneralEntryBookReportService;
import org.chorem.lima.business.api.report.LedgerReportService;
import org.chorem.lima.business.api.report.ProvisionalEntryBookReportService;
import org.chorem.lima.business.utils.BigDecimalToString;
import org.chorem.lima.entity.Account;
import org.chorem.lima.entity.Identity;
import org.chorem.lima.entity.Treasury;
import org.chorem.lima.entity.VatStatement;
import org.chorem.lima.report.DocumentsEnum;
import org.chorem.lima.report.LimaReportConfig;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;


public class DocumentService {

    protected static final Log log = LogFactory.getLog(DocumentService.class);

    protected AccountService accountService;

    protected AccountReportService accountReportService;

    protected BalanceReportService balanceReportService;

    protected ProvisionalEntryBookReportService entryBookReportService;

    protected FinancialStatementService financialStatementService;

    protected GeneralEntryBookReportService generalEntryBookReportService;

    protected IdentityService identityService;

    protected LedgerReportService ledgerReportService;

    protected TreasuryService treasuryService;

    protected VatStatementService vatStatementService;

    protected JasperReports jasperReports;

    protected String accountFilePath;
    protected String balanceFilePath;
    protected String generalEntryBookReportPdfFilePath;
    protected String entryBooksReportPdfFilePath;
    protected String ledgerReportPdfFilePath;
    protected String vat_default_formFilePath;

    public DocumentService() {
        accountService = LimaServiceFactory.getService(AccountService.class);
        accountReportService = LimaServiceFactory.getService(AccountReportService.class);
        balanceReportService = LimaServiceFactory.getService(BalanceReportService.class);
        entryBookReportService = LimaServiceFactory.getService(ProvisionalEntryBookReportService.class);
        financialStatementService = LimaServiceFactory.getService(FinancialStatementService.class);
        generalEntryBookReportService = LimaServiceFactory.getService(GeneralEntryBookReportService.class);
        identityService = LimaServiceFactory.getService(IdentityService.class);
        treasuryService = LimaServiceFactory.getService(TreasuryService.class);

        ledgerReportService = LimaServiceFactory.getService(LedgerReportService.class);
        vatStatementService = LimaServiceFactory.getService(VatStatementService.class);

        jasperReports = new JasperReports();

        File reportDir = LimaReportConfig.getInstance().getReportsModelDir();

        try {
            FileUtil.createDirectoryIfNecessary(reportDir);
        } catch (IOException ioe) {
            if (log.isErrorEnabled()) {
                log.error("Cannot create report dir", ioe);
            }
        }

        String reportDirPath = reportDir.getAbsolutePath();

        accountFilePath = reportDirPath + File.separator + DocumentsEnum.ACCOUNT.getFileName() + ".pdf";
        balanceFilePath = reportDirPath + File.separator + DocumentsEnum.BALANCE.getFileName() + ".pdf";
        generalEntryBookReportPdfFilePath = reportDirPath + File.separator + DocumentsEnum.GENERAL_ENTRY_BOOK.getFileName() + ".pdf";
        entryBooksReportPdfFilePath = reportDirPath + File.separator + DocumentsEnum.ENTRY_BOOKS.getFileName() + ".pdf";
        ledgerReportPdfFilePath = reportDirPath + File.separator + DocumentsEnum.LEDGER.getFileName() + ".pdf";
        vat_default_formFilePath = reportDirPath + File.separator + DocumentsEnum.VAT.getFileName() + ".pdf";

        if (log.isDebugEnabled()) {
            log.debug("Path : " + reportDirPath);
        }

    }

    public String createFinancialStatementsDocuments(Date beginDate,
                                                     Date endDate) {

        List<FinancialStatementAmounts> financialStatementAmounts =
                financialStatementService.financialStatementReport(beginDate, endDate);
        String financialReport;

        try {

            financialReport = constructHtmlHeader("");

            //Split list by financialstatement type
            List<List<FinancialStatementAmounts>> listList = new ArrayList<>();
            Boolean first = true;
            int min = 0;
            int size = financialStatementAmounts.size();
            for (int i = 0; i < size; i++) {
                FinancialStatementAmounts fStatementAmounts =
                        financialStatementAmounts.get(i);
                if (fStatementAmounts.getLevel() == 1 && !fStatementAmounts.isSubAmount()) {
                    if (first) {
                        first = false;
                    } else {
                        listList.add(financialStatementAmounts.subList(min, i - 1));
                    }
                    min = i;
                }
            }
            listList.add(financialStatementAmounts.subList(min, size));
            int printedType = -1;

            //create pages
            for (List<FinancialStatementAmounts> list : listList) {
                if (!list.isEmpty()) {
                    String title = list.get(0).getLabel();
                    int i = 0;
                    int n = list.size();
                    printedType++;
                    while (i < n) {

                        financialReport += constructHeaderTitle(title, beginDate, endDate);

                        String boldBegin = "<b>";
                        String boldEnd = "</b>";
                        financialReport += "<table border=\"1\" width=\"100%\" cellpadding=\"3\" cellspacing=\"0\">\n" +
                                "<tr align=\"center\">\n";

                        if (printedType == 0) {
                            String[] columnHeaderTable = {boldBegin + t("lima-business.document.label") + boldEnd, boldBegin + t("lima-business.document.grossamount") + boldEnd,
                                    boldBegin + t("lima-business.document.provisiondeprecationamount") + boldEnd, boldBegin + t("lima-business.document.netamount") + boldEnd};
                            financialReport += constructTableLine(columnHeaderTable);
                        } else {
                            String[] columnHeaderTable = {boldBegin + t("lima-business.document.label") + boldEnd, boldBegin + t("lima-business.document.amount") + boldEnd};
                            financialReport += constructTableLine(columnHeaderTable);
                        }

                        for (FinancialStatementAmounts financialStatementAmount : list) {

                            String label = financialStatementAmount.getLabel();
                            int level = financialStatementAmount.getLevel();
                            BigDecimal grossAmount =
                                    financialStatementAmount.getGrossAmount();
                            if (grossAmount == null) {
                                grossAmount = BigDecimal.ZERO;
                            }
                            BigDecimal provisionDeprecationAmount =
                                    financialStatementAmount.getProvisionDeprecationAmount();
                            if (provisionDeprecationAmount == null) {
                                provisionDeprecationAmount = BigDecimal.ZERO;
                            }

                            if (label == null) {
                                if (printedType == 0) {
                                    String[] emptyColumn = {"", "", "", ""};
                                    financialReport += constructTableLine(emptyColumn);
                                } else {
                                    String[] emptyColumn = {"", ""};
                                    financialReport += constructTableLine(emptyColumn);
                                }
                            } else {
                                //cell1
                                StringBuilder tab = new StringBuilder();
                                for (int k = 0; k < level; k++) {
                                    tab.append("\t");
                                }
                                //Phrase phrase;
                                String tabLabel;
                                if (financialStatementAmount.isHeader()) {
                                    tabLabel = boldBegin + tab + label + boldEnd;
                                } else {
                                    tabLabel = tab + label;
                                }
                                //cell2
                                String grossAmountStr = "";
                                if (!grossAmount.equals(BigDecimal.ZERO)) {
                                    grossAmountStr = grossAmount.toString();
                                }

                                //cell 3
                                String provisionDeprecationAmountStr = "";
                                if (!provisionDeprecationAmount.equals(BigDecimal.ZERO)) {
                                    provisionDeprecationAmountStr = provisionDeprecationAmount.toString();
                                }

                                //cell 4
                                BigDecimal solde = grossAmount;
                                solde = solde.subtract(provisionDeprecationAmount);
                                String soldeStr = "";
                                if (!solde.equals(BigDecimal.ZERO)) {
                                    soldeStr = solde.toString();
                                }

                                if (printedType == 0) {
                                    String[] columns = {tabLabel, grossAmountStr, provisionDeprecationAmountStr, soldeStr};
                                    financialReport += constructTableLine(columns);
                                } else /*if (printedType == 1)*/ {
                                    String[] columns = {tabLabel, soldeStr};
                                    financialReport += constructTableLine(columns);
                                }
                            }
                        }
                        i = i + n;
                        financialReport += "</table>";
                    }
                }
            }
            financialReport += "</body>" +
                    "</html>";

        } catch (Exception ex) {
            throw new LimaTechnicalException("Can't create document", ex);
        }
        return financialReport;
    }

    //##############     VAT      ##############
    public void createVatDocuments(Date beginDate,
                                   Date endDate,
                                   String autocomplete) {

        String path = LimaReportConfig.getInstance().getReportsDir().getAbsolutePath();
        String filePath = path + File.separator
                + DocumentsEnum.VAT.getFileName() + ".pdf";

        PDDocument doc;
        InputStream reportsStream;

        String vatPDFUrl = LimaReportConfig.getInstance().getVatPDFUrl();

        if (vatPDFUrl.equals("default")) {
            reportsStream = DocumentService.class.getResourceAsStream("/reports/vat_form_fr.pdf");
            if (reportsStream == null) {
                throw new LimaTechnicalException("Could not find such file " + "/reports/vat_form_fr.pdf");
            }
        } else {
            try {
                reportsStream = new FileInputStream(vat_default_formFilePath);
            } catch (FileNotFoundException eee) {
                throw new LimaTechnicalException("Could not find such file "
                        + vat_default_formFilePath, eee);
            }
        }

        try {

            // load the document
            doc = PDDocument.load(reportsStream);

            if (autocomplete != null) {
                if (log.isDebugEnabled()) {
                    log.debug("autocomplete: " + autocomplete);
                }
                if (autocomplete.equals("true")) {
                    SetField fields = new SetField();
                    //search for all VAT Statements from the report
                    List<VatStatement> vatStatementsList = vatStatementService.getAllVatStatements();
                    if (log.isDebugEnabled()) {
                        log.debug("vatStatementsList.size() : " + vatStatementsList.size());
                    }
                    for (VatStatement vatStatement : vatStatementsList) {
                        //search for amount to display
                        BigDecimal amount = vatStatementService.vatStatementAmounts(vatStatement, beginDate, endDate).getAmount();
                        //display amount only if it is a child and has a BoxName
                        if (vatStatement.getBoxName() != null && !vatStatement.isHeader()) {
                            if (log.isDebugEnabled()) {
                                log.debug("Set field...");
                            }
                            fields.setField(doc, vatStatement.getBoxName(), amount.toString());
                        }
                    }

                    Identity identity = identityService.getIdentity();
                    Treasury treasury = treasuryService.getTreasury();

                    String ident = Strings.nullToEmpty(identity.getName()) + "\n" +
                            Strings.nullToEmpty(identity.getAddress()) + "\n" + Strings.nullToEmpty(identity.getAddress2()) + "\n" +
                            Strings.nullToEmpty(identity.getZipCode()) + " " +
                            Strings.nullToEmpty(identity.getCity());
                    String treasuryAddress = Strings.nullToEmpty(treasury.getAddress()) + '\n' +
                            Strings.nullToEmpty(treasury.getZipCode()) + " " +
                            Strings.nullToEmpty(treasury.getCity());

                    fields.setField(doc, "a1", t("lima-business.document.vatPeriod1", beginDate));
                    fields.setField(doc, "a2", treasuryAddress);
                    fields.setField(doc, "a4", ident);
                    fields.setField(doc, "a6", Strings.nullToEmpty(treasury.getSie()));
                    fields.setField(doc, "a7", Strings.nullToEmpty(treasury.getDossierNumber()));
                    fields.setField(doc, "a8", Strings.nullToEmpty(treasury.getKey()));
                    fields.setField(doc, "a9", t("lima-business.document.treasuryPeriod", beginDate));
                    fields.setField(doc, "a10", Strings.nullToEmpty(treasury.getCdi()));
                    fields.setField(doc, "a11", Strings.nullToEmpty(treasury.getServiceCode()));
                    fields.setField(doc, "a12", Strings.nullToEmpty(treasury.getSystemType()));
                    fields.setField(doc, "a13", "FR" + Strings.nullToEmpty(identity.getVatNumber()) + Strings.nullToEmpty(identity.getBusinessNumber()));

                }
            }

            // save the updated document to the new file and close
            doc.save(filePath);
            doc.close();

        } catch (Exception ex) {
            throw new LimaTechnicalException("Can't create document", ex);
        }
    }


    protected String constructHtmlHeader(String title) {
        String head = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"UTF-8\" />\n" +
                "\t<title>" + title + "</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\t<h1>" + title + "</h1>\n";
        return head;
    }


    protected String constructTableLine(String... cells) {
        StringBuilder builder = new StringBuilder("\t\t<tr>\n");
        for (String cell : cells) {
            builder.append("\t\t\t<td>").append(cell).append("</td>\n");
        }
        builder.append("\t\t</tr>\n");
        return builder.toString();
    }

    protected String constructHeaderTitle(String title, Date beginDate, Date endDate) {
        String headerTitle;
        Identity identity = identityService.getIdentity();

        headerTitle = "<table>" +
                "<thead> " +
                "<tr><th>" + title +
                "</th></tr>" +
                "</thead>" +
                "<tr> " +
                "<td>" +
                "<table align=\"left\" border=\"1\" cellpadding=\"3\" cellspacing=\"0\" style=\"font-size:13px;\" >\n" +
                "<tr>\n";

        String boldItalicBegin = "<b>" + "<i>";
        String boldItalicEnd = "</i>" + "</b>";

        if (identity != null) {
            String[] columnsNameSociety = {boldItalicBegin + t("lima-business.document.society") + boldItalicEnd,
                    "<i>" + (StringUtils.isNotEmpty(identity.getName()) ? identity.getName() : " - ") + "</i>",};
            headerTitle += constructTableLine(columnsNameSociety);

            String[] columnsDescription = {boldItalicBegin + t("lima-business.document.description") + boldItalicEnd,
                    "<i>" + (StringUtils.isNotEmpty(identity.getDescription()) ? identity.getDescription() : " - ") + "</i>",};
            headerTitle += constructTableLine(columnsDescription);

            String[] columnsAdressOne = {boldItalicBegin + t("lima-business.document.address") + boldItalicEnd,
                    "<i>" + (StringUtils.isNotEmpty(identity.getAddress()) ? identity.getAddress() : " - ") + "</i>",};
            headerTitle += constructTableLine(columnsAdressOne);

            String[] columnsAdressTwo = {boldItalicBegin + t("lima-business.document.addressMore") + boldItalicEnd,
                    "<i>" + (StringUtils.isNotEmpty(identity.getAddress2()) ? identity.getAddress2() : " - ") + "</i>",};
            headerTitle += constructTableLine(columnsAdressTwo);

            String[] columnsZipCode = {boldItalicBegin + t("lima-business.document.zipcode") + boldItalicEnd,
                    "<i>" + (StringUtils.isNotEmpty(identity.getZipCode()) ? identity.getZipCode() : " - ") + "</i>",};
            headerTitle += constructTableLine(columnsZipCode);

            String[] columnsCity = {boldItalicBegin + t("lima-business.document.city") + boldItalicEnd,
                    "<i>" + (StringUtils.isNotEmpty(identity.getCity()) ? identity.getCity() : " - ") + "</i>",};
            headerTitle += constructTableLine(columnsCity);

            String[] columnsBusinessNumber = {boldItalicBegin + t("lima-business.document.businessnumber") +
                    boldItalicEnd, "<i>" + (StringUtils.isNotEmpty(identity.getBusinessNumber()) ? identity.getBusinessNumber() : " - ") + "</i>"};
            headerTitle += constructTableLine(columnsBusinessNumber);

            String[] columnsClassifCode = {boldItalicBegin + t("lima-business.document.classificationcode") +
                    boldItalicEnd, "<i>" + (StringUtils.isNotEmpty(identity.getClassificationCode()) ? identity.getClassificationCode() : " - ") + "</i>"};
            headerTitle += constructTableLine(columnsClassifCode);

//            String[] columnsVatNumber = {boldItalicBegin + t("lima-business.document.vatnumber") + boldItalicEnd,
//                    "<i>" + (StringUtils.isNotEmpty(identity.getVatNumber()) ? identity.getVatNumber() : " - ") + "</i>",};
//            headerTitle += constructTableLine(columnsVatNumber);
        }

        String[] columnsPeriodOne = {boldItalicBegin + t("lima-business.document.period1") + boldItalicEnd, "<i>"
                + t("lima-business.document.period1format", beginDate) + "</i>"};
        headerTitle += constructTableLine(columnsPeriodOne);

        headerTitle += "<tr>\n";
        String[] columnsPeriodTwo = {boldItalicBegin + t("lima-business.document.period2") + boldItalicEnd, "<i>"
                + t("lima-business.document.period2format", endDate) + "</i>"};
        headerTitle += constructTableLine(columnsPeriodTwo);

        headerTitle += "</table>" +
                "</td>" +
                "</table>";

        return headerTitle;
    }

    protected DecimalFormat getDecimalFormat() {
        OptionsService optionsService = LimaServiceFactory.getService(OptionsService.class);
        DecimalFormat result = BigDecimalToString.newDecimalFormat(optionsService);
        return result;
    }

    //##############     account     ##############
    public void createAccountDocument(String account, Date beginDate, Date endDate) {
        JasperReport acountEntryReport = jasperReports.getAccountEntryReport();
        DocumentReport report = accountReportService.getAccountDocumentReport(account, beginDate, endDate, acountEntryReport, getDecimalFormat());
        jasperReports.generatePDFReport(DocumentsEnum.ACCOUNT, accountFilePath, Lists.newArrayList(report));
    }


    //##############     balance     ##############
    public void createBalanceDocuments(Date beginDate, Date endDate, String isBalanceGeneralSt, String fromToAccount) {
        JasperReport balanceMainAccountsReport = jasperReports.getBalanceManAccountsReport();
        JasperReport balanceSubAccountsReport = jasperReports.getBalanceSubAccountsReport();

        DocumentReport report = getDocumentReport(beginDate, endDate, fromToAccount, balanceMainAccountsReport, balanceSubAccountsReport, isBalanceGeneralSt);

        jasperReports.generatePDFReport(DocumentsEnum.BALANCE, balanceFilePath, Lists.newArrayList(report));
    }

    protected DocumentReport getDocumentReport(Date beginDate, Date endDate, String fromToAccount, JasperReport balanceMainAccountsReport, JasperReport balanceSubAccountsReport, String isBalanceGeneralSt) {
        boolean isBalanceGeneral = StringUtils.isBlank(isBalanceGeneralSt) || Boolean.valueOf(isBalanceGeneralSt);

        DocumentReport report;
        if (isBalanceGeneral) {
            report = balanceReportService.getGeneralBalanceDocumentReport(beginDate, endDate, fromToAccount,
                    getDecimalFormat(), balanceMainAccountsReport, balanceSubAccountsReport);
        } else {
            report = balanceReportService.getGlobalBalanceDocumentReport(beginDate, endDate, fromToAccount,
                    getDecimalFormat(), balanceMainAccountsReport, balanceSubAccountsReport);
        }
        return report;
    }

    //##############     EntryBook      #############
    public void createEntryBooksDocuments(Date beginDate, Date endDate, List<String> entryBookCodes) {
        JasperReport entryBookEntryBooksReport = jasperReports.getEntryBookEntryBooksReport();
        JasperReport entryBookFinancialPeriodsReport = jasperReports.getEntryBookFinancialPeriodsReport();
        JasperReport entryBookTransactionsReport = jasperReports.getEntryBookTransactionsReport();
        DocumentReport report = entryBookReportService.getEntryBookDocumentReport(beginDate, endDate, entryBookCodes,
                getDecimalFormat(), entryBookEntryBooksReport, entryBookFinancialPeriodsReport, entryBookTransactionsReport);
        jasperReports.generatePDFReport(DocumentsEnum.ENTRY_BOOKS, entryBooksReportPdfFilePath, Lists.newArrayList(report));
    }

    //##############     General EntryBook      #############
    public void createGeneralEntryBooksDocuments(Date beginDate, Date endDate) {
        JasperReport generalEntryBookGeneralEntryBooksReport = jasperReports.getGeneralEntryBookGeneralEntryBooksReport();
        JasperReport generalEntryBookEntriesReport = jasperReports.getGeneralEntryBookEntriesReport();
        DocumentReport report = generalEntryBookReportService.getGeneralEntryBookDocumentReport(beginDate, endDate,
                getDecimalFormat(), generalEntryBookGeneralEntryBooksReport, generalEntryBookEntriesReport);
        jasperReports.generatePDFReport(DocumentsEnum.GENERAL_ENTRY_BOOK, generalEntryBookReportPdfFilePath, Lists.newArrayList(report));
    }

    //##############     Ledger      #############
    public void createLedgerDocuments(Date beginDate, Date endDate) {
        JasperReport generalLedgerGeneralLedgersReport = jasperReports.getGeneralLedgerGeneralLedgersReport();
        JasperReport generalLedgerEntriesReport = jasperReports.getGeneralLedgerEntriesReport();
        DocumentReport report = ledgerReportService.getLedgerDocumentReport(beginDate, endDate, getDecimalFormat(),
                generalLedgerGeneralLedgersReport, generalLedgerEntriesReport);
        jasperReports.generatePDFReport(DocumentsEnum.LEDGER, ledgerReportPdfFilePath, Lists.newArrayList(report));
    }

    protected InputStream getReportAsPdfStream(String model) throws IOException {

        File reportDir = LimaReportConfig.getInstance().getReportsModelDir();
        String path = reportDir.getAbsolutePath();
        URL doc = new URL("file:" + path + File.separator + model + ".pdf");
        if (log.isDebugEnabled()) {
            log.debug("file:" + path + File.separator + model + ".pdf");
        }
        InputStream inputStream = doc.openStream();
        return inputStream;
    }

    public GeneratedReport createReport(Date beginDate, Date endDate, String model, String account, String isBalanceGeneral, String autocomplete) throws IOException {
        String stringResult = null;

        //create docs
        switch (DocumentsEnum.valueOfLink(model)) {
            case BALANCE:
                createBalanceDocuments(beginDate, endDate, isBalanceGeneral, null);
                break;
            case ACCOUNT:
                createAccountDocument(account, beginDate, endDate);
                break;
            case ENTRY_BOOKS:
                createEntryBooksDocuments(beginDate, endDate, null);
                break;
            case GENERAL_ENTRY_BOOK:
                createGeneralEntryBooksDocuments(beginDate, endDate);
                break;
            case FINANCIAL_STATEMENT:
                stringResult = createFinancialStatementsDocuments(beginDate, endDate);
                break;
            case LEDGER:
                createLedgerDocuments(beginDate, endDate);
                break;
            case VAT:
                if (log.isDebugEnabled()) {
                    log.debug("autocomplete : " + autocomplete);
                }
                createVatDocuments(beginDate, endDate, autocomplete);
                break;
            default:
                throw new UnsupportedOperationException("Unknown document type: " + model);
        }

        if (Strings.isNullOrEmpty(stringResult)) {
            InputStream pdfStream = getReportAsPdfStream(model);
            return GeneratedReport.pdf(pdfStream);
        } else {
            return GeneratedReport.html(stringResult);
        }
    }

    public List<Account> getAllAccounts() {
        // delegate to accountService
        return accountService.getAllAccounts();
    }

}
