package org.chorem.lima.report;

/*
 * #%L
 * Lima :: Report
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaTechnicalException;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.config.ConfigOptionDef;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created by davidcosse on 13/03/15.
 */
public class LimaReportConfig {

    protected static final Log log = LogFactory.getLog(LimaReportConfig.class);

    protected static final String DEFAULT_CONFIG_FILE_NAME = "lima-report.config";

    protected ApplicationConfig config;

    protected static LimaReportConfig instance;

    private LimaReportConfig(ApplicationConfig config) {
        try {
            ApplicationConfig defaultConfig = new ApplicationConfig(DEFAULT_CONFIG_FILE_NAME);
            defaultConfig.loadDefaultOptions(ReportConfigOption.values());
            defaultConfig.parse();

            if (config != null) {
                Properties flatOptions = defaultConfig.getFlatOptions(false);
                flatOptions.putAll(config.getFlatOptions());
                this.config = new ApplicationConfig(flatOptions);
                this.config.parse();
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("No specific configuration provided, using the default one");
                }
                this.config = defaultConfig;
            }
            instance = this;
        } catch (ArgumentsParserException ex) {
            throw new LimaTechnicalException("Can't read configuration", ex);
        }
    }

    public synchronized static LimaReportConfig getInstance(ApplicationConfig config) {
        if (instance == null) {
            instance= new LimaReportConfig(config);
        }
        return instance;
    }

    public synchronized static LimaReportConfig getInstance() {
        return getInstance(null);
    }

    public ApplicationConfig getConfig() {
        return config;
    }

    public Properties getFlatOptions() {
        return config.getFlatOptions();
    }


    public File getDataDir() {
        File datadir = config.getOptionAsFile(ReportConfigOption.DATA_DIR.getKey());
        return datadir;
    }

    // ** REPORT PART **

    public File getReportsModelDir() {
        String reportsDirPath =  config.getOption(ReportConfigOption.REPORTS_MODEL_DIR.key);
        File result = new File(reportsDirPath);
        return result;
    }

    public File getReportsDir() {
        String reportsDirPath =  config.getOption(ReportConfigOption.REPORTS_DIR.getKey());
        File result = new File(reportsDirPath);
        return result;
    }

    public void setReportsModelDir(String url) {
        config.setOption(ReportConfigOption.REPORTS_MODEL_DIR.key, url);
        config.saveForUser();
    }

    public String getVatPDFUrl() {
        String vatPDFUrl = config.getOption(ReportConfigOption.VAT_PDF_URL.getKey());
        return vatPDFUrl;
    }

    public void setVatPDFUrl(String url) {
        config.setOption(ReportConfigOption.VAT_PDF_URL.key, url);
        config.saveForUser();
    }


    public String getAccountReportModelPath() {
        String vatPDFUrl = config.getOption(ReportConfigOption.ACCOUNT_DOCUMENT_REPORT_MODEL_PATH.getKey());
        return vatPDFUrl;
    }

    public void setAccountReportModelPath(String path) {
        config.setOption(ReportConfigOption.ACCOUNT_DOCUMENT_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    protected URL getReportModelUrl(String documentReportKey) {

        String optionValue = config.getOption(documentReportKey);
        File file = new File(optionValue);

        URL result;
        if (file.exists()) {
            try {
                result = file.toURI().toURL();
            } catch (MalformedURLException e) {
                throw new LimaTechnicalException("Could not get url of file: "+file);
            }
        } else {
            result = getClass().getResource(optionValue);
        }

        if (result == null) {
            throw new LimaTechnicalException(String.format("Could not find option: %s", documentReportKey));
        }

        return result;

    }

    public URL getReportModelUrl(DocumentReportTypes documentType) {
        URL jasperSourceFileUrl = null;
        switch (documentType) {
            case ACCOUNT:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.ACCOUNT_DOCUMENT_REPORT_MODEL_PATH.getKey());
                break;
            case ACCOUNT_ENTRY:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.ACCOUNT_ENTRY_REPORT_MODEL_PATH.getKey());
                break;

            case BALANCE:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.BALANCE_DOCUMENT_REPORT_MODEL_PATH.key);
                break;
            case BALANCE_MAIN_ACCOUNTS:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.BALANCE_ACCOUNT_REPORT_MODEL_PATH.key);
                break;
            case BALANCE_SUB_ACCOUNTS:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.BALANCE_SUB_ACCOUNT_REPORT_MODEL_PATH.key);
                break;

            case ENTRY_BOOKS:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.ENTRY_BOOK_DOCUMENT_REPORT_MODEL_PATH.key);
                break;
            case ENTRY_BOOKS_ENTRY_BOOKS:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.ENTRY_BOOK_ENTRY_BOOK_REPORT_MODEL_PATH.key);
                break;
            case ENTRY_BOOKS_FINANCIAL_PERIODS:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.ENTRY_BOOK_FINANCIAL_PERIOD_REPORT_MODEL_PATH.key);
                break;
            case ENTRY_BOOKS_TRANSACTION:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.ENTRY_BOOK_TRANSACTION_REPORT_MODEL_PATH.key);
                break;

            case GENERAL_ENTRY_BOOK:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.GENERAL_ENTRY_BOOK_DOCUMENT_REPORT_MODEL_PATH.key);
                break;
            case GENERAL_ENTRY_BOOK_GENERAL_ENTRY_BOOKS:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.GENERAL_ENTRY_BOOK_REPORT_MODEL_PATH.key);
                break;
            case GENERAL_ENTRY_BOOK_ENTRIES:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.GENERAL_ENTRY_BOOK_ENTRY_REPORT_MODEL_PATH.key);
                break;

            case LEDGER:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.GENERAL_LEDGER_DOCUMENT_REPORT_MODEL_PATH.key);
                break;
            case LEDGER_GENERAL_LEDGERS:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.GENERAL_LEDGER_MODEL_PATH.key);
                break;
            case LEDGER_ENTRIES:
                jasperSourceFileUrl = getReportModelUrl(ReportConfigOption.GENERAL_LEDGER_ENTRY_MODEL_PATH.key);
                break;
        }
        return jasperSourceFileUrl;
    }

    public void setBalanceDocumentReportModelPath(String path) {
        config.setOption(ReportConfigOption.BALANCE_DOCUMENT_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getBalanceAccountReportModelPath() {
        String result = config.getOption(ReportConfigOption.BALANCE_ACCOUNT_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setBalanceAccountReportModelPath(String path) {
        config.setOption(ReportConfigOption.BALANCE_ACCOUNT_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getBalanceSubAccountReportModelPath() {
        String result = config.getOption(ReportConfigOption.BALANCE_SUB_ACCOUNT_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setBalanceSubAccountReportModelPath(String path) {
        config.setOption(ReportConfigOption.BALANCE_SUB_ACCOUNT_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getEntryBookDocumentReportModelPath() {
        String result = config.getOption(ReportConfigOption.ENTRY_BOOK_DOCUMENT_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setEntryBookDocumentReportModelPath(String path) {
        config.setOption(ReportConfigOption.ENTRY_BOOK_DOCUMENT_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }
    public String getEntryBookEntryBookReportModelPath() {
        String result = config.getOption(ReportConfigOption.ENTRY_BOOK_ENTRY_BOOK_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setEntryBookEntryBookReportModelPath(String path) {
        config.setOption(ReportConfigOption.ENTRY_BOOK_ENTRY_BOOK_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getEntryBookFinancialPeriodReportModelPath() {
        String result = config.getOption(ReportConfigOption.ENTRY_BOOK_FINANCIAL_PERIOD_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setEntryBookFinancialPeriodReportModelPath(String path) {
        config.setOption(ReportConfigOption.ENTRY_BOOK_FINANCIAL_PERIOD_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getEntryBookTransactionReportModelPath() {
        String result = config.getOption(ReportConfigOption.ENTRY_BOOK_TRANSACTION_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setEntryBookTransactionReportModelPath(String path) {
        config.setOption(ReportConfigOption.ENTRY_BOOK_TRANSACTION_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getGeneralEntryBookDocumentReportModelPath() {
        String result = config.getOption(ReportConfigOption.GENERAL_ENTRY_BOOK_DOCUMENT_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setGeneralEntryBookDocumentReportModelPath(String path) {
        config.setOption(ReportConfigOption.GENERAL_ENTRY_BOOK_DOCUMENT_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getGeneralEntryBookReportModelPath() {
        String result = config.getOption(ReportConfigOption.GENERAL_ENTRY_BOOK_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setGeneralEntryBookReportModelPath(String path) {
        config.setOption(ReportConfigOption.GENERAL_ENTRY_BOOK_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getGeneralEntryBookEntryReportModelPath() {
        String result = config.getOption(ReportConfigOption.GENERAL_ENTRY_BOOK_ENTRY_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setGeneralEntryBookEntryReportModelPath(String path) {
        config.setOption(ReportConfigOption.GENERAL_ENTRY_BOOK_ENTRY_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getGeneralLedgerDocumentReportModelPath() {
        String result = config.getOption(ReportConfigOption.
                GENERAL_LEDGER_DOCUMENT_REPORT_MODEL_PATH.getKey());
        return result;
    }

    public void setGeneralLedgerDocumentReportModelPath(String path) {
        config.setOption(ReportConfigOption.
                GENERAL_LEDGER_DOCUMENT_REPORT_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getGeneralLedgerModelPath() {
        String result = config.getOption(ReportConfigOption.
                GENERAL_LEDGER_MODEL_PATH.getKey());
        return result;
    }

    public void setGeneralLedgerModelPath(String path) {
        config.setOption(ReportConfigOption.
                GENERAL_LEDGER_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public String getGeneralLedgerEntryModelPath() {
        String result = config.getOption(ReportConfigOption.
                GENERAL_LEDGER_ENTRY_MODEL_PATH.getKey());
        return result;
    }

    public void setGeneralLedgerEntryModelPath(String path) {
        config.setOption(ReportConfigOption.
                GENERAL_LEDGER_ENTRY_MODEL_PATH.key, path);
        config.saveForUser();
    }

    public enum ReportConfigOption implements ConfigOptionDef {

        DATA_DIR("lima.data.dir", n("lima.config.data.dir.description"), "${user.home}/.lima", File.class, false, false),

        REPORTS_MODEL_DIR("lima.reports.dir",n("lima.config.reports.dir.description"),"${lima.data.dir}/reports", File.class, false, false),
        REPORTS_DIR("lima.reports.dir", n("lima.config.reports.dir.description"), "${lima.data.dir}/reports", File.class, false, false),

        ACCOUNT_DOCUMENT_REPORT_MODEL_PATH("lima.config.documentReport.account.documentReportModelPath", n("lima.config.documentReport.account.documentReportModelPath.description"), "/jasperreports/account/DocumentReport.jrxml",String.class, false, false),
        ACCOUNT_ENTRY_REPORT_MODEL_PATH("lima.config.documentReport.account.accountEntryReportModelPath", n("lima.config.documentReport.account.accountEntryReportModelPath.description"), "/jasperreports/account/AccountEntry.jrxml",String.class, false, false),

        BALANCE_DOCUMENT_REPORT_MODEL_PATH("lima.config.documentReport.balance.documentReportModelPath", n("lima.config.documentReport.balance.documentReportModelPath.description"), "/jasperreports/balance/DocumentReport.jrxml", String.class, false, false),
        BALANCE_ACCOUNT_REPORT_MODEL_PATH("lima.config.documentReport.balance.balanceAccountReportModelPath", n("lima.config.documentReport.balance.balanceAccountReportModelPath.description"), "/jasperreports/balance/BalanceReportAccountReport.jrxml", String.class, false, false),
        BALANCE_SUB_ACCOUNT_REPORT_MODEL_PATH("lima.config.documentReport.balance.balanceSubAccountReportModelPath", n("lima.config.documentReport.balance.balanceSubAccountReportModelPath.description"), "/jasperreports/balance/BalanceSubAccountsReport.jrxml", String.class, false, false),

        GENERAL_ENTRY_BOOK_DOCUMENT_REPORT_MODEL_PATH("lima.config.documentReport.generalEntrybook.documentReportModelPath", n("lima.config.documentReport.generalEntrybook.documentReportModelPath.description"), "/jasperreports/generalEntryBook/DocumentReport.jrxml", String.class, false, false),
        GENERAL_ENTRY_BOOK_REPORT_MODEL_PATH("lima.config.documentReport.generalEntrybook.generalEntryBookModelPath", n("lima.config.documentReport.generalEntrybook.generalEntryBookModelPath.description"), "/jasperreports/generalEntryBook/EntryBookPeriodReport.jrxml", String.class, false, false),
        GENERAL_ENTRY_BOOK_ENTRY_REPORT_MODEL_PATH("lima.config.documentReport.generalEntrybook.generalEntryBookEntryModelPath", n("blima.config.documentReport.generalEntrybook.generalEntryBookEntryModelPath.description"), "/jasperreports/generalEntryBook/GeneralEntryBookEntryReport.jrxml", String.class, false, false),

        ENTRY_BOOK_DOCUMENT_REPORT_MODEL_PATH("lima.config.documentReport.entrybook.documentReportModelPath", n("lima.config.documentReport.entrybook.documentReportModelPath.description"), "/jasperreports/entryBook/DocumentReport.jrxml", String.class, false, false),
        ENTRY_BOOK_ENTRY_BOOK_REPORT_MODEL_PATH("lima.config.documentReport.entrybook.entryBookModelPath", n("lima.config.documentReport.entrybook.entryBookModelPath.description"), "/jasperreports/entryBook/EntryBookReport.jrxml", String.class, false, false),
        ENTRY_BOOK_FINANCIAL_PERIOD_REPORT_MODEL_PATH("lima.config.documentReport.entrybook.financialPeriodModelPath", n("lima.config.documentReport.entrybook.financialPeriodModelPath.description"), "/jasperreports/entryBook/FinancialPeriodReport.jrxml", String.class, false, false),
        ENTRY_BOOK_TRANSACTION_REPORT_MODEL_PATH("lima.config.documentReport.entrybook.transactionReportModelPath", n("lima.config.documentReport.entrybook.transactionReportModelPath.description"), "/jasperreports/entryBook/TransactionReport.jrxml", String.class, false, false),

        GENERAL_LEDGER_DOCUMENT_REPORT_MODEL_PATH("lima.config.documentReport.generalLedger.documentReportModelPath", n("lima.config.documentReport.generalLedger.documentReportModelPath.description"), "/jasperreports/generalLedger/DocumentReport.jrxml", String.class, false, false),
        GENERAL_LEDGER_MODEL_PATH("lima.config.documentReport.generalLedger.generalLedgerModelPath", n("lima.config.documentReport.generalLedger.generalLedgerModelPath.description"), "/jasperreports/generalLedger/GeneralLedgerReport.jrxml", String.class, false, false),
        GENERAL_LEDGER_ENTRY_MODEL_PATH("lima.config.documentReport.generalLedger.generalLedgerEntryModelPath", n("lima.config.documentReport.generalLedger.generalLedgerEntryModelPath.description"), "/jasperreports/generalLedger/GeneralLedgerEntryReport.jrxml", String.class, false, false),

        VAT_PDF_URL("lima.report.vatpdfurl", n("lima.config.reportvatpdfurl.description"), "default", String.class, false, false);

        private final String key;

        private final String description;

        private String defaultValue;

        private final Class<?> type;

        private boolean transientBoolean;

        private boolean finalBoolean;

        ReportConfigOption(String key, String description, String defaultValue,
                           Class<?> type, boolean transientBoolean, boolean finalBoolean) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this.finalBoolean = finalBoolean;
            this.transientBoolean = transientBoolean;
        }

        @Override
        public boolean isFinal() {
            return finalBoolean;
        }

        @Override
        public void setFinal(boolean finalBoolean) {
            this.finalBoolean = finalBoolean;
        }

        @Override
        public boolean isTransient() {
            return transientBoolean;
        }

        @Override
        public void setTransient(boolean transientBoolean) {
            this.transientBoolean = transientBoolean;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public String getDescription() {
            return t(description);
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }
    }
}
