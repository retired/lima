/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.lima.report;

import static org.nuiton.i18n.I18n.t;


public enum DocumentsEnum {
    ACCOUNT(t("lima-business.document.account"), "lima_account"),
    BALANCE(t("lima-business.document.balance"), "lima_balance"),
    ENTRY_BOOKS(t("lima-business.document.entrybook"), "lima_entrybooks"),
    FINANCIAL_STATEMENT(t("lima-business.document.financialstatement"), "lima_financialstatements"),
    GENERAL_ENTRY_BOOK(t("lima-business.document.generalentrybook"), "lima_general_entybook"),
    LEDGER(t("lima-business.document.ledger"), "lima_ledger"),
    VAT(t("lima-business.document.vat"), "lima_vat");

    private final String fileName;

    private final String description;

    DocumentsEnum(String description, String fileName) {
        this.description = description;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getDescription() {
        return description;
    }

    public static DocumentsEnum valueOfLink(String label) {
        DocumentsEnum value = null;

        for (DocumentsEnum documentsEnum : DocumentsEnum.values()) {
            if (label.equals(documentsEnum.fileName)) {
                value = documentsEnum;
                break;
            }
        }
        return value;
    }

}
