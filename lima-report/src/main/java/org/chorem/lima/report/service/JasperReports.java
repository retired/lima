package org.chorem.lima.report.service;

/*
 * #%L
 * Lima :: report
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.lima.LimaTechnicalException;
import org.chorem.lima.beans.DocumentReport;
import org.chorem.lima.report.DocumentReportTypes;
import org.chorem.lima.report.DocumentsEnum;
import org.chorem.lima.report.LimaReportConfig;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that contains all JasperReport
 */
public class JasperReports {

    private static final Log log = LogFactory.getLog(JasperReports.class);

    protected JasperReport accountDocumentReport;
    protected JasperReport accountEntryReport;

    protected JasperReport balanceDocumentReport;
    protected JasperReport balanceManAccountsReport;
    protected JasperReport balanceSubAccountsReport;

    protected JasperReport entryBookDocumentReport;
    protected JasperReport entryBookEntryBooksReport;
    protected JasperReport entryBookFinancialPeriodsReport;
    protected JasperReport entryBookTransactionsReport;

    protected JasperReport generalEntryBookDocumentReport;
    protected JasperReport generalEntryBookGeneralEntryBooksReport;
    protected JasperReport generalEntryBookEntriesReport;

    protected JasperReport generalLedgerDocumentReport;
    protected JasperReport generalLedgerGeneralLedgersReport;
    protected JasperReport generalLedgerEntriesReport;

    protected Map<DocumentsEnum, JasperReport> reportsByDocumentType;

    /**
     * Compile all report
     */
    public JasperReports() {

        LimaReportConfig config = LimaReportConfig.getInstance();

        // compile phase
        accountDocumentReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.ACCOUNT));
        accountEntryReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.ACCOUNT_ENTRY));

        balanceDocumentReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.BALANCE));
        balanceManAccountsReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.BALANCE_MAIN_ACCOUNTS));
        balanceSubAccountsReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.BALANCE_SUB_ACCOUNTS));

        entryBookDocumentReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.ENTRY_BOOKS));
        entryBookEntryBooksReport  = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.ENTRY_BOOKS_ENTRY_BOOKS));
        entryBookFinancialPeriodsReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.ENTRY_BOOKS_FINANCIAL_PERIODS));
        entryBookTransactionsReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.ENTRY_BOOKS_TRANSACTION));

        generalEntryBookDocumentReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.GENERAL_ENTRY_BOOK));
        generalEntryBookGeneralEntryBooksReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.GENERAL_ENTRY_BOOK_GENERAL_ENTRY_BOOKS));
        generalEntryBookEntriesReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.GENERAL_ENTRY_BOOK_ENTRIES));

        generalLedgerDocumentReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.LEDGER));
        generalLedgerGeneralLedgersReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.LEDGER_GENERAL_LEDGERS));
        generalLedgerEntriesReport = prepareJasperReport(config.getReportModelUrl(DocumentReportTypes.LEDGER_ENTRIES));

        reportsByDocumentType = Maps.newHashMap();
        reportsByDocumentType.put(DocumentsEnum.ACCOUNT, accountDocumentReport);
        reportsByDocumentType.put(DocumentsEnum.BALANCE, balanceDocumentReport);
        reportsByDocumentType.put(DocumentsEnum.GENERAL_ENTRY_BOOK, generalEntryBookDocumentReport);
        reportsByDocumentType.put(DocumentsEnum.ENTRY_BOOKS, entryBookDocumentReport);
        reportsByDocumentType.put(DocumentsEnum.LEDGER, generalLedgerDocumentReport);

    }

    protected JasperReport prepareJasperReport(URL url) {
        JasperReport jasperReport;
        try (InputStream inputStream = url.openStream()) {
            jasperReport = JasperCompileManager.compileReport(inputStream);
        } catch (IOException e) {
            throw new LimaTechnicalException("Could not close inputStream for " + url, e);
        } catch (JRException e) {
            throw new LimaTechnicalException("Could not compile jaspert report for " + url, e);
        }
        return jasperReport;
    }


    public void generatePDFReport(DocumentsEnum reportType, String path, List<DocumentReport> reports) {

        try {
            JasperReport report = reportsByDocumentType.get(reportType);
            if (report != null) {
                Map<String, Object> parameters = new HashMap<>();
                JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(reports);
                JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, dataSource);
                JasperExportManager.exportReportToPdfFile(jasperPrint, path);
                if (log.isInfoEnabled()) {
                    log.info("Rapport généré:" + path);
                }
            } else {
                throw new LimaTechnicalException("JasperReport not found for " + reportType);
            }

        } catch (JRException e) {
            throw new LimaTechnicalException(e);
        }

    }

    public JasperReport getAccountEntryReport() {
        return accountEntryReport;
    }

    public JasperReport getBalanceManAccountsReport() {
        return balanceManAccountsReport;
    }

    public JasperReport getBalanceSubAccountsReport() {
        return balanceSubAccountsReport;
    }

    public JasperReport getEntryBookEntryBooksReport() {
        return entryBookEntryBooksReport;
    }

    public JasperReport getEntryBookFinancialPeriodsReport() {
        return entryBookFinancialPeriodsReport;
    }

    public JasperReport getEntryBookTransactionsReport() {
        return entryBookTransactionsReport;
    }

    public JasperReport getGeneralEntryBookGeneralEntryBooksReport() {
        return generalEntryBookGeneralEntryBooksReport;
    }

    public JasperReport getGeneralEntryBookEntriesReport() {
        return generalEntryBookEntriesReport;
    }

    public JasperReport getGeneralLedgerGeneralLedgersReport() {
        return generalLedgerGeneralLedgersReport;
    }

    public JasperReport getGeneralLedgerEntriesReport() {
        return generalLedgerEntriesReport;
    }

    public Map<DocumentsEnum, JasperReport> getReportsByDocumentType() {
        return reportsByDocumentType;
    }

}
