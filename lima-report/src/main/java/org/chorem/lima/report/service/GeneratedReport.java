package org.chorem.lima.report.service;

/*
 * #%L
 * Lima :: Report
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.InputStream;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class GeneratedReport {

    protected String htmlContent;
    protected InputStream pdfStream;

    public static GeneratedReport html(String htmlContent) {
        GeneratedReport result = new GeneratedReport();
        result.setHtmlContent(htmlContent);
        return result;
    }

    public static GeneratedReport pdf(InputStream pdfStream) {
        GeneratedReport result = new GeneratedReport();
        result.setPdfStream(pdfStream);
        return result;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public InputStream getPdfStream() {
        return pdfStream;
    }

    public void setPdfStream(InputStream pdfStream) {
        this.pdfStream = pdfStream;
    }

}
