package org.chorem.lima.report;

/*
 * #%L
 * Lima :: business
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * Created by davidcosse on 02/03/15.
 */
public enum DocumentReportTypes {
    ACCOUNT,
    ACCOUNT_ENTRY,
    BALANCE,
    BALANCE_MAIN_ACCOUNTS,
    BALANCE_SUB_ACCOUNTS,
    ENTRY_BOOKS,
    ENTRY_BOOKS_ENTRY_BOOKS,
    ENTRY_BOOKS_FINANCIAL_PERIODS,
    ENTRY_BOOKS_TRANSACTION,
    FINANCIAL_STATEMENT,
    GENERAL_ENTRY_BOOK,
    GENERAL_ENTRY_BOOK_GENERAL_ENTRY_BOOKS,
    GENERAL_ENTRY_BOOK_ENTRIES,
    LEDGER,
    LEDGER_GENERAL_LEDGERS,
    LEDGER_ENTRIES
}
