package org.chorem.lima.report.action;

/*
 * #%L
 * Lima :: Swing
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sf.jasperreports.engine.JRException;
import org.chorem.lima.report.service.DocumentService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by davidcosse on 02/10/14.
 */
public class ReportTest {

    String tmpDir;

    protected static String DATE_FORMAT = "dd/MM/yyyy";

    @Before
    public void initTest() throws Exception {
        tmpDir = System.getProperty("java.io.tmpdir")+"/";
    }

    @Ignore
    @Test
    public void testBalanceReport() throws JRException, IOException, ParseException {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        Date beginDateFormat = df.parse("01/01/2013");
        Date endDateFormat = df.parse("31/12/2014");
        DocumentService documentService = new DocumentService();
        documentService.createBalanceDocuments(beginDateFormat, endDateFormat, null, null);
    }
}
