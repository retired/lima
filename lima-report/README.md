Présentation :

Acronyme de Lutin Invoice Monitoring and Accounting,
l'application de comptabilité Lima est un logiciel libre pensé pour être le plus ergonomique possible et facile d'accès à tout utilisateur,
quelque soit son niveau en comptabilité : débutant comme confirmé.
La particularité de Lima est qu'il s'agit d'un produit évolutif permettant de répondre aux besoins spécifiques de toute entreprise ou organisation,
tout en garantissant le maintien des données comptables.

Le logiciel est écrit en Java ce qui assure une compatibilité multiplateforme : Windows, Mac OS X, Linux.
Il peut-être installé en fonctionnement client monoposte, ou en configuration client <–> serveur.
Lors d'une installation en client serveur, le moteur de persistence de données est installé coté serveur,
et l'interface est installée sur autant de postes client que désiré.

Pour lancer l'application :

    Sous Linux : via le script lima ou en ligne de commande java -jar lima.jar ;

    Sous Mac OS X : en double-cliquant sur le jar lima.jar ;

    Sous Windows : en double-cliquant sur l'éxécutable lima.exe.